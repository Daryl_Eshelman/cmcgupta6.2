.head 0 +  Application Description: Trusteeship Disbursements Report
City of Canton, Ohio
Matt Drechsler - April 1997
.data CLASSPROPSSIZE
0000: 3800
.enddata
.data CLASSPROPS
0000: 4400420047005F00 50004C0041005900 4200410043004B00 5F00470055004900
0020: 4400000000001000 6C7088344B5E2A49 A73B6009B456BC79
.enddata
.head 1 -  Outline Version - 4.0.50
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000AA0000 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE2FFFFFF6E0000 006E000000780200 00B2010000010000 0001000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0000000000
.enddata
.data DT_MAKERUNDLG
0000: 1300001000000000 FFFEFF00FFFEFF0B 5400720064006900 7300620021002E00
0020: 650078006500FFFE FF0B540072006400 6900730062002100 2E0064006C006C00
0040: FFFEFF0B54007200 6400690073006200 21002E0061007000 6300000001010100
0060: 64000000FFFEFF00 FFFEFF00FFFEFF00 0000000000000000 0000FFFEFF0B5400
0080: 7200640069007300 620021002E006100 70006400FFFEFF0B 5400720064006900
00A0: 7300620021002E00 64006C006C00FFFE FF0B540072006400 6900730062002100
00C0: 2E00610070006300 0000010101006400 0000FFFEFF0B5400 7200640069007300
00E0: 620021002E006100 70006C00FFFEFF0B 5400720064006900 7300620021002E00
0100: 64006C006C00FFFE FF0B540072006400 6900730062002100 2E00610070006300
0120: 0000010101006400 0000FFFEFF0B5400 7200640069007300 620021002E006500
0140: 78006500FFFEFF0B 5400720064006900 7300620021002E00 64006C006C00FFFE
0160: FF0B540072006400 6900730062002100 2E00610070006300 0000010101006400
0180: 0000FFFEFF0B5400 7200640069007300 620021002E006400 6C006C00FFFEFF0B
01A0: 5400720064006900 7300620021002E00 64006C006C00FFFE FF0B540072006400
01C0: 6900730062002100 2E00610070006300 0000010101006400 0000FFFEFF0B5400
01E0: 7200640069007300 620021002E006400 6C006C00FFFEFF0B 5400720064006900
0200: 7300620021002E00 64006C006C00FFFE FF0B540072006400 6900730062002100
0220: 2E00610070006300 0000010101006400 00000000000001FF FEFF00FFFEFF00FF
0240: FEFF00FFFEFF00FF FEFF00FFFEFF0000 00000000000000FF FEFF00FFFEFF00FF
0260: FEFF000000000000 0100000001000000 01FFFEFF00010000 0000000000FFFEFF
0280: 0001000010000000 0000000000000000 0005000000000000 0001000000010000
02A0: 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF
02C0: 00FFFEFF00000000 0000000000000000 00FFFEFF00010000 0000000000FFFEFF
02E0: 00
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 0000721FC2120500 1D00FFFF4D61696E
0020: 0029000100040000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000400 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000400 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: -0.01"
.head 3 -  Top: 0.0"
.head 3 -  Width:  8.01"
.head 3 -  Height: 4.967"
.head 2 +  Options Box Location
.data VIEWINFO
0000: D418E306B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 6.5"
.head 3 -  Top: 1.188"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 2 -  Assembly Symbol File:
.head 1 +  Libraries
.head 2 -  File Include: crLogin.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Line Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 3 +  Tree Control
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Navigation Bar
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 -  Pane Separator
.head 3 +  Tab Bar
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Progress Bar
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: yyyy
.head 3 -  Date/Time: M/d/yyyy
.head 3 -  Number: ###000
.head 3 -  Number: ###000
.head 3 -  Number: ###000
.head 3 -  Number: ###000
.head 2 +  External Functions
.head 2 +  External Assemblies
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 3 +  User
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.data RESOURCE 0 0 1 3615499085
0000: 5E0000004B000000 0000000000000000 02000003000000F0 0600001019000000
0020: 0102102009290000 00FE13004A015600 0400000083090700 0004C5000202102B
0040: 00FE4B01DE560068 0D070019000D0199 3600000A00FE6102 6600360400
.enddata
.head 3 -  Boolean: bOk
.head 3 -  Boolean: nRptFlg
.head 3 -  Sql Handle: hSqlTrustee
.head 3 -  String: sRecSelect
.head 3 -  Number: nRow
.head 3 -  Number: nRetCode
.head 3 -  Number: nFetch
.head 3 -  Number: nPos
.head 3 -  Number: nMin
.head 3 -  Number: nMax
.head 2 +  Internal Functions
.head 2 -  Named Exceptions
.head 2 -  Named Toolbars
.head 2 +  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 3677674686
0000: 5A0100000B010000 0000000000000000 0200000300FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F19003C00 00FFFE00FF084300
0040: 72004400006F0063 006B006500007400 2200000001003C00 0019000500010210
0060: 312908000000FE13 7B00C03B00040000 0010018029000001 003900FF63008852
0080: 006570006F0A7274 73009A00C0000005 0000005B190001A2 021D29000002FE05
00A0: 0049001C97680000 0019001604000202 10AF00FEF8060049 82001D00000004F5
00C0: 00022B00FE0700BE 4900602100000004 00FD028A00FE0800 492F002500580000
00E0: 040002BF00E2FE09 00490B0001801900 810002000000FF01 0563004A00610060
0100: 69006C002200F401 0000161900040210 AE3100FE40560053 0004000003
.enddata
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field: cQuickMLField
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Separator:
.head 3 -  Tree Control:
.head 3 -  Navigation Bar:
.head 3 -  Pane Separator:
.head 3 -  Progress Bar:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture: cQuickPicture
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 +  Application Actions
.head 3 +  On SAM_AppStartup
.head 4 +  If strArgArray[1] = ''
.head 5 -  Call SalModalDialog( dlgLogin, hWndNULL, 'Trusteeship Disbursements' )
.head 4 +  Else
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 -  Set SqlUser = strArgArray[1]
.head 5 -  Set SqlPassword =strArgArray[2]
.head 5 -  Set SqlDatabase =strArgArray[3]
.head 5 -  Set bLogin = SqlConnect( hSql )
.head 5 +  If NOT bLogin
.head 6 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalQuit(  )
.head 4 +  If NOT bLogin
.head 5 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 5 -  Call SalWaitCursor( FALSE )
.head 5 -  Call SalQuit(  )
.head 5 -  Return TRUE
.head 4 -  Call GetCourtInfo(  )
.head 4 -  Set nRow=0
.head 4 -  Call SqlConnect( hSqlTrustee )
.head 4 -  Call SalModalDialog( dlgProcess, hWndForm )
.head 3 +  On SAM_AppExit
.head 4 +  If bLogin
.head 5 -  Call SqlDisconnect( hSql )
.head 5 -  Call SqlDisconnect( hSqlTrustee )
.head 1 +  Dialog Box: dlgProcess
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Trusteeship Disbursements
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.063"
.head 4 -  Top: 0.333"
.head 4 -  Width:  5.57"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.777"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Gray
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 5224
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.13"
.head 5 -  Top: 0.125"
.head 5 -  Width:  5.18"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.342"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Arial
.head 4 -  Font Size: 16
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Trusteeship Monthly Disbursements
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Radio Button: rbCalculate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Calculate Disbursement Totals
.head 4 -  Window Location and Size
.head 5 -  Left: 1.27"
.head 5 -  Top: 0.542"
.head 5 -  Width:  2.93"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 3 +  Radio Button: rbDisburse
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print Disbursement Report
.head 4 -  Window Location and Size
.head 5 -  Left: 1.26"
.head 5 -  Top: 0.883"
.head 5 -  Width:  2.97"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbProcess
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Continue
.head 4 -  Window Location and Size
.head 5 -  Left: 1.98"
.head 5 -  Top: 1.308"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: F12
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If rbDisburse
.head 7 -  Call SalCreateWindow ( frmReportInput, hWndNULL )
.head 6 +  Else
.head 7 -  Call SalCreateWindow ( frmDisburse, hWndNULL )
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call SalWaitCursor( FALSE )
.head 3 +  On SAM_Close
.head 4 -  Call SalQuit(  )
.head 1 +  Form Window: frmReportInput
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Report Input
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Maximized
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 0.35"
.head 4 -  Top: 1.15"
.head 4 -  Width:  7.64"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 2.627"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Gray
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Pushbutton: pbDisplayByReceiptNo
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Display by &Name
.head 4 -  Window Location and Size
.head 5 -  Left: 0.29"
.head 5 -  Top: 0.825"
.head 5 -  Width:  1.7"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sSortSeq = 'RECEIPTNO'
.head 6 -  Call fFillTable(  )
.head 3 +  Data Field: dfBeginDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.688"
.head 6 -  Top: 0.396"
.head 6 -  Width:  1.18"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Center
.head 5 -  Format: M/d/yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = SalDateMonthBegin( SalDateCurrent(  ) -10)
.head 6 -  Set dfEndDate = SalDateMonthBegin( MyValue + 32) - 1
.head 3 +  Data Field: dfEndDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.075"
.head 6 -  Top: 0.396"
.head 6 -  Width:  1.18"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Center
.head 5 -  Format: M/d/yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbRpt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Start &Report
.head 4 -  Window Location and Size
.head 5 -  Left: 3.088"
.head 5 -  Top: 0.729"
.head 5 -  Width:  1.7"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: F12
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sSortSeq = 'RECEIPTNO'
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call fFillTable(  )
.head 6 -  Call SalTblQueryScroll( tblHold, nPos, nMin, nMax )
.head 6 +  If nMax < 1
.head 7 -  Call SalMessageBeep( MB_IconStop )
.head 7 -  Call SalMessageBox( 'Nothing to Print for this Date.', 'Report Error', MB_Ok)
.head 6 +  Else
.head 7 -  Set sReportInputs = 'CASENO, FNAME, MNAME, LNAME, 
	CREDITOR, TRBALANCE, CRBALANCE, TOTALAMT, AMOUNT, BegDate, EndDate'
.head 7 -  Set sReportBinds = 'colCaseNo, colFName, colMName, colLName, 
	colDisCreditor, colTrustBalance, colTrustCRBalance, colTotal, colBegBalance, dfBeginDate, dfEndDate'
.head 7 -  Set sReport = 'Journal'
.head 7 -  ! Call SalReportPrint( hWndForm, 'TRUSTDISB!.qrp', 
	sReportBinds, sReportInputs, 0, RPT_PrintAll, 0, 0, nPrintErr )
.head 7 -  Call SalReportView ( hWndForm, hWndNULL,  CV_REPORT_Path || 'TRDISB!.qrp', 
	sReportBinds, sReportInputs, nPrintErr)
.head 7 +  If nPrintErr > 0
.head 8 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 7 -  Call SalWaitCursor( FALSE )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton( hWndItem )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel, Finish
.head 4 -  Window Location and Size
.head 5 -  Left: 5.163"
.head 5 -  Top: 0.729"
.head 5 -  Width:  1.7"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 6 -  Call SalQuit(  )
.head 3 +  Child Table: tblHold
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.025"
.head 6 -  Top: 1.125"
.head 6 -  Width:  9.813"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 4.354"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 9
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 5000
.head 5 -  Discardable? No
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colCaseNo
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Case No
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.05"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colControl
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Control No
.head 6 -  Visible? No
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  1.05"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colFName
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: First
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.25"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colMName
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Middle
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.717"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colLName
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Last
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.75"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colDisCreditor
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Creditor
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.75"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colTrustBalance
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: TR Balance
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  1.283"
.head 6 -  Width Editable? Yes
.head 6 -  Format: ###000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colBegBalance
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Beg Balance
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  1.35"
.head 6 -  Width Editable? Yes
.head 6 -  Format: ###000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colTrustCRBalance
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Balance
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  1.083"
.head 6 -  Width Editable? Yes
.head 6 -  Format: ###000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colTotal
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Total
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  1.15"
.head 6 -  Width Editable? Yes
.head 6 -  Format: ###000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 38660
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 3.025"
.head 5 -  Top: 0.073"
.head 5 -  Width:  3.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.313"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Arial
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Trusteeship Disbursments
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 +  Functions
.head 3 +  Function: fFillTable
.head 4 -  Description: Fills the Disbursement table
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 -  Set nRow = 1
.head 5 -  Set sRecSelect = "SELECT d.CASENO, c.controlno, FNAME, MNAME, LNAME, 
		d.CREDITOR, t.BALANCE, c.amount, c.BALANCE, sum(d.TOTALAMT) 
	FROM TRUSTDISB d, TRUSTEE t, TRUSTCRED c
	WHERE  d.CASENO = t.CASENO  and  d.CASENO = c.CASENO(+)  and d.CREDITOR = c.CREDITOR(+)  and  
       		d.DISBDATE between :dfBeginDate and :dfEndDate and checkstatus not in 'V' 
	Group by d.CASENO, c.controlno, FNAME, MNAME, LNAME, 
		d.CREDITOR, t.BALANCE, c.amount, c.BALANCE
       Union All 
	SELECT d.CASENO, c.controlno, FNAME, MNAME, LNAME, 
		d.CREDITOR, t.BALANCE, c.amount, c.BALANCE, sum(d.TOTALAMT * -1)
	FROM TRUSTDISB d, TRUSTEE t, TRUSTCRED c
	WHERE  d.CASENO = t.CASENO  and  d.CASENO = c.CASENO(+)  and d.CREDITOR = c.CREDITOR(+)  and  
       		d.DISBDATE < :dfBeginDate and returned between :dfBeginDate and :dfEndDate and checkstatus in 'V' 
	Group by d.CASENO, c.controlno, FNAME, MNAME, LNAME, 
		d.CREDITOR, t.BALANCE, c.amount, c.BALANCE
	Order by 1, 2"
.head 5 -  Call SalTblPopulate( tblHold, hSql, sRecSelect, TBL_FillAll )
.head 5 -  !
.head 5 -  Call SalTblSortRows( tblHold, 1, TBL_SortIncreasing )
.head 5 -  Call SalWaitCursor( FALSE )
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  String: sSortSeq
.head 3 -  Boolean: bExist
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call SalEndDialog( dlgProcess, 0 )
.head 3 +  On SAM_ReportStart
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Call SalReportDlgOptions( SalNumberToWindowHandle( wParam ),
'Canton Civil Municipal Court', 'Printing Quarterly Trusteeship Report',
'On Laser Printer', 'Quarterly Trusteeship Disbursements')
.head 4 -  Set nRow = 0
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  While TRUE
.head 5 +  If sReport = 'Journal'
.head 6 +  If SalTblSetContext( tblHold, nRow )
.head 7 -  Call SalTblSetFocusRow( tblHold, nRow )
.head 7 -  Set nRow = nRow + 1
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Set bExist = FALSE
.head 1 +  Form Window: frmDisburse
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Disburse
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Maximized
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 0.28"
.head 4 -  Top: 0.483"
.head 4 -  Width:  7.73"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 3.369"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Gray
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Combo Box: cmbTrustee
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.13"
.head 5 -  Top: 0.067"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.094"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? Yes
.head 4 -  String Type: String
.head 4 -  Maximum Data Length: Default
.head 4 -  Sorted? Yes
.head 4 -  Always Show List? No
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  AutoFill? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  List Initialization
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SalListPopulate( hWndItem, hSqlTrustee, 'Select CASENO from TRUSTEE
where BALANCE > 0 AND INACTCODE IS NULL' )
.head 5 +  On SAM_Click
.head 6 -  Call SalTblPopulate( tblDisb, hSqlTrustee, "Select CRED.CONTROLNO, CRED.CREDITOR, 
	CRED.REFERENCE, CRED.BALANCE, CRED.ADDRESS, CRED.CITY || '  ' || CRED.ZIP 
into :tblDisb.colControlNo, :tblDisb.colCreditor, :tblDisb.colReference, :tblDisb.colBalance, 
	:tblDisb.colPayeeAddr1, :tblDisb.colPayeeAddr2
from TRUSTEE, TRUSTCRED CRED 
where :cmbTrustee = CRED.CASENO
	and TRUSTEE.CASENO = CRED.CASENO
	and CRED.BALANCE > 0
order by CRED.BALANCE", TBL_FillAll )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'select 
BALANCE, LNAME, FNAME
from TRUSTEE
into :dfBalance, :dfLName, :dfFName
where :cmbTrustee = CASENO' )
.head 6 -  Call SqlFetchNext( hSql, nFetch )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select sum(amtPaid) 
	from trustrcpt into :dfPayIn 
	where CASENO = :cmbTrustee and void is null and 
		RcptDate between :dfQtrStart and :dfQtrEnd' )
.head 6 -  Call SqlFetchNext( hSql, nFetch )
.head 6 -  Set dfTotal = SalTblColumnSum( tblDisb, 4, 0,0 )
.head 6 -  Call SalEnableWindow( pbCalc )
.head 6 -  Call SalEnableWindow( pbWriteChecks )
.head 3 +  Pushbutton: pbCalc
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Calc
.head 4 -  Window Location and Size
.head 5 -  Left: 0.11"
.head 5 -  Top: 0.542"
.head 5 -  Width:  0.67"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalTblInsertRow( tblDisb, 0 )
.head 6 -  Set colCreditor = 'City of Canton'
.head 6 -  Set colPayOut = 15
.head 6 -  Set nPayIn = dfPayIn - 15
.head 6 -  Call SalTblQueryScroll( tblDisb, nPos, nMin, nMax )
.head 6 -  Set nPercent = SalNumberTruncate( dfPayIn*.1, 7,2 )
.head 6 -  Set nPayIn = nPayIn - nPercent
.head 6 -  Set nPos = nMin + 1
.head 6 -  Call SalTblSetFocusRow( tblDisb, nPos )
.head 6 -  Call SalTblSetContext( tblDisb, nPos )
.head 6 +  While tblDisb.colBalance < nPercent AND nPos <= nMax AND nPercent != 0
.head 7 -  Set tblDisb.colPayOut = tblDisb.colBalance
.head 7 -  Set nPercent = nPercent - tblDisb.colBalance
.head 7 -  Set nPos = nPos + 1
.head 7 -  Call SalTblSetContext( tblDisb, nPos )
.head 6 -  Set nMin =  nPos + 1
.head 6 -  Set nPayIn = nPayIn + nPercent
.head 6 -  Set nPercent =  nPayIn / (dfTotal-(dfPayIn-nPayIn))
.head 6 -  Set nAdj=0
.head 6 +  While nPos <= nMax AND nPayIn != 0
.head 7 -  Set nAmount = SalNumberTruncate
( tblDisb.colBalance * nPercent, 7, 2)
.head 7 +  If nAmount < 1
.head 8 -  Set nAmount = 0
.head 8 -  Set nAdj = nAdj + tblDisb.colBalance
.head 8 -  Set nPercent =  nPayIn / ( dfTotal-( dfPayIn - nPayIn)- nAdj)
.head 7 +  If nAmount > nPayIn or nPos = nMax
.head 8 -  Set nAmount = nPayIn
.head 7 -  Set tblDisb.colPayOut =  nAmount
.head 7 -  Set nPos = nPos + 1
.head 7 -  Set nPayIn = nPayIn - nAmount
.head 7 -  Call SalTblSetContext( tblDisb, nPos )
.head 6 -  Set dfPayOut = SalTblColumnSum( ( tblDisb), 5, 0,0 )
.head 6 -  Call SalDisableWindow( pbCalc )
.head 3 +  Pushbutton: pbReport
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Report
.head 4 -  Window Location and Size
.head 5 -  Left: 0.85"
.head 5 -  Top: 0.542"
.head 5 -  Width:  0.83"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set nPrintErr = -1
.head 6 -  Set sReportBinds = 'colCreditor, colReference, cmbTrustee, sDef, colBalance, colPayOut, sCourt, sCourtClerk'
.head 6 -  Set sReportInputs = 'Payee, Addr1, CaseNo, Defendant, Balance, DisbAmt, Court, CourtClerk'
.head 6 -  Call SalReportPrint ( hWndForm,  CV_REPORT_Path || 'TrustDis.qrp', 
sReportBinds, sReportInputs, 0, RPT_PrintAll, 0, 0, nPrintErr )
.head 6 +  If nPrintErr > 0
.head 7 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 3 +  Pushbutton: pbWriteChecks
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Checks
.head 4 -  Window Location and Size
.head 5 -  Left: 1.77"
.head 5 -  Top: 0.542"
.head 5 -  Width:  0.83"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDisableWindow( pbWriteChecks )
.head 6 -  Call SalTblQueryScroll( tblDisb, nPos, nMin, nMax )
.head 6 -  Set nPos = nMin
.head 6 -  Call SalTblSetFocusRow( tblDisb, nPos )
.head 6 -  Call SalTblSetContext( tblDisb, nPos )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select MAX(ControlNo) 
	From TrustDisb Into :nControlNo' )
.head 6 -  Call SqlFetchNext( hSql, nFetch )
.head 6 +  While nPos <= nMax
.head 7 +  If tblDisb.colPayOut > 0
.head 8 -  Set nControlNo = nControlNo + 1
.head 8 -  Call SqlPrepareAndExecute( hSqlTrustee, "Insert Into TrustDisb 
(CONTROLNO, CASENO, CREDITOR, REFERENCE, TOTALAMT, CHECKSTATUS, PAYEEADDR1, PAYEEADDR2) VALUES 
(:nControlNo, :cmbTrustee, :colCreditor, :colReference, :colPayOut, 'W', :colPayeeAddr1, :colPayeeAddr2 )" )
.head 8 +  If colControlNo > 0
.head 9 -  Call SqlPrepareAndExecute( hSql, "Update TrustCred 
	Set BALANCE = BALANCE - :colPayOut where CONTROLNO = :colControlNo" )
.head 9 -  Call SqlPrepareAndExecute( hSql, "Update TRUSTEE
	Set BALANCE = BALANCE - :colPayOut where CaseNo = :cmbTrustee" )
.head 7 -  Set nPos = nPos + 1
.head 7 -  Call SalTblSetContext( tblDisb, nPos )
.head 6 -  Call SqlCommit( hSql )
.head 6 -  Call SqlCommit( hSqlTrustee )
.head 3 +  Pushbutton: pbPrint
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print
.head 4 -  Window Location and Size
.head 5 -  Left: 0.89"
.head 5 -  Top: 0.533"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F3
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalPrtPrintForm( frmDisburse )
.head 3 +  Data Field: dfPayIn
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.35"
.head 6 -  Top: 0.354"
.head 6 -  Width:  0.938"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.225"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: ###000;'($'###000')'
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfPayOut
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.338"
.head 6 -  Top: 0.615"
.head 6 -  Width:  0.95"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: ###000;'($'###000')'
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfBalance
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 8.425"
.head 6 -  Top: 0.342"
.head 6 -  Width:  1.313"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: ###000;'($'###000')'
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfTotal
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 8.438"
.head 6 -  Top: 0.617"
.head 6 -  Width:  1.3"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: ###000;'($'###000')'
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfFName
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.22"
.head 6 -  Top: 0.058"
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Gray
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLName
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.43"
.head 6 -  Top: 0.05"
.head 6 -  Width:  1.688"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Gray
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Child Table: tblDisb
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.09"
.head 6 -  Top: 0.983"
.head 6 -  Width:  9.7"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 3.483"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Default
.head 5 -  Discardable? No
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colControlNo
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Number
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 7
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.725"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #0
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colCreditor
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Creditor
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  4.35"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colReference
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Reference
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.138"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colBalance
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Balance
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  0.925"
.head 6 -  Width Editable? Yes
.head 6 -  Format: ###000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colPayOut
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Pay Out
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Right
.head 6 -  Width:  0.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: ###000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_Validate
.head 8 -  Set dfPayOut = SalTblColumnSum( ( tblDisb), 5, 0,0 )
.head 5 +  Column: colPayeeAddr1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address
.head 6 -  Visible? No
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.17"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colPayeeAddr2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: City, State  Zip
.head 6 -  Visible? No
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.09"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 17653
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 7.275"
.head 5 -  Top: 0.658"
.head 5 -  Width:  1.08"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Column Total
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 17654
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 7.013"
.head 5 -  Top: 0.383"
.head 5 -  Width:  1.36"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Total from Table
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 34474
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 3.863"
.head 5 -  Top: 0.396"
.head 5 -  Width:  1.33"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Period Receipts
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfQtrStart
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 7.763"
.head 6 -  Top: 0.052"
.head 6 -  Width:  0.913"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: M/d/yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = SalDateMonthBegin( SalDateCurrent(  ) - 75 )
.head 5 +  On SAM_Validate
.head 6 -  Call SalSendMsg( cmbTrustee, SAM_Click, 0, 0 )
.head 3 +  Data Field: dfQtrEnd
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 8.738"
.head 6 -  Top: 0.05"
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: M/d/yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = SalDateMonthBegin( SalDateCurrent(  ) ) - 1
.head 5 +  On SAM_Validate
.head 6 -  Call SalSendMsg( cmbTrustee, SAM_Click, 0, 0 )
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 34475
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 7.013"
.head 5 -  Top: 0.094"
.head 5 -  Width:  0.675"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Quarter
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 34476
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 3.875"
.head 5 -  Top: 0.646"
.head 5 -  Width:  1.39"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Disb. Total
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nAdj
.head 3 -  Number: nPayIn
.head 3 -  Number: nPercent
.head 3 -  Number: nAmount
.head 3 -  Number: nIndex
.head 3 -  Number: nControlNo
.head 3 -  String: sDef
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call SalEndDialog( dlgProcess, 0 )
.head 3 +  On SAM_ReportStart
.head 4 -  Set nIndex = 0
.head 4 -  Set nRow = SalTblSetRow( tblDisb, TBL_SetFirstRow )
.head 4 +  If nRow >= TBL_MaxRow
.head 5 -  Return FALSE
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If nIndex > 0
.head 5 -  Set nRow = SalTblSetRow( tblDisb, TBL_SetNextRow )
.head 4 +  If nRow >= TBL_MaxRow
.head 5 -  Return FALSE
.head 4 -  Set nIndex = 1
.head 4 -  Set sDef = dfLName || ', ' || dfFName
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return TRUE
