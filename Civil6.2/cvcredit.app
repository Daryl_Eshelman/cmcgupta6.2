.head 0 +  Application Description: Civil Division Credit Reporting Process
.data CLASSPROPSSIZE
0000: 3A00
.enddata
.data CLASSPROPS
0000: 4400420047005F00 50004C0041005900 4200410043004B00 5F00470055004900
0020: 4400000000001000 514654B7DD8DD34C 8C6739707E1CFB01 0000
.enddata
.head 1 -  Outline Version - 4.0.50
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000B10000 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE2FFFFFF160000 0016000000360200 0052010000010000 0001000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0000000000
.enddata
.data DT_MAKERUNDLG
0000: 1300001000000000 FFFEFF00FFFEFF0C 6300760063007200 6500640069007400
0020: 2E00650078006500 FFFEFF0C63007600 6300720065006400 690074002E006400
0040: 6C006C00FFFEFF0C 6300760063007200 6500640069007400 2E00610070006300
0060: 0000010101006400 0000FFFEFF00FFFE FF00FFFEFF000000 0000000000000000
0080: FFFEFF0C63007600 6300720065006400 690074002E006100 70006400FFFEFF0C
00A0: 6300760063007200 6500640069007400 2E0064006C006C00 FFFEFF0C63007600
00C0: 6300720065006400 690074002E006100 7000630000000101 010064000000FFFE
00E0: FF0C630076006300 7200650064006900 74002E0061007000 6C00FFFEFF0C6300
0100: 7600630072006500 6400690074002E00 64006C006C00FFFE FF0C630076006300
0120: 7200650064006900 74002E0061007000 6300000001010100 64000000FFFEFF0C
0140: 6300760063007200 6500640069007400 2E00650078006500 FFFEFF0C63007600
0160: 6300720065006400 690074002E006400 6C006C00FFFEFF0C 6300760063007200
0180: 6500640069007400 2E00610070006300 0000010101006400 0000FFFEFF0C6300
01A0: 7600630072006500 6400690074002E00 64006C006C00FFFE FF0C630076006300
01C0: 7200650064006900 74002E0064006C00 6C00FFFEFF0C6300 7600630072006500
01E0: 6400690074002E00 6100700063000000 0101010064000000 FFFEFF0C63007600
0200: 6300720065006400 690074002E006400 6C006C00FFFEFF0C 6300760063007200
0220: 6500640069007400 2E0064006C006C00 FFFEFF0C63007600 6300720065006400
0240: 690074002E006100 7000630000000101 0100640000000000 000001FFFEFF00FF
0260: FEFF00FFFEFF00FF FEFF00FFFEFF00FF FEFF000000000000 000000FFFEFF00FF
0280: FEFF00FFFEFF0000 0000000001000000 0100000001FFFEFF 0001000000000000
02A0: 00FFFEFF00010000 1000000000000000 0000000000050000 0000000000010000
02C0: 0001000000FFFEFF 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF
02E0: 00FFFEFF00FFFEFF 0000000000000000 0000000000FFFEFF 0001000000000000
0300: 00FFFEFF00
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003001B00 0200000000000000 00003816330E0500 1D00FFFF4D61696E
0020: 0029000100040000 0000000000E91E80 0A00008600FFFF49 6E7465726E616C20
0040: 46756E6374696F6E 7300200001000400 000000000000E91E 800A0000DF00FFFF
0060: 5661726961626C65 73001E0001000400 000000000000F51E 100D0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: 0.0"
.head 3 -  Top: 0.0"
.head 3 -  Width:  8.01"
.head 3 -  Height: 4.792"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C438B707B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 8.213"
.head 3 -  Top: 0.156"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 2 -  Assembly Symbol File:
.head 1 +  Libraries
.head 2 -  File Include: crlogin.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Line Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 3 +  Tree Control
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Navigation Bar
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 -  Pane Separator
.head 3 +  Tab Bar
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Progress Bar
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: #
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: 0000
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM/dd/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 3 -  Number: 0000
.head 3 -  Number: #
.head 3 -  Number: 00000
.head 3 -  Number: 00000
.head 3 -  Date/Time: MM-dd-yyyy
.head 2 +  External Functions
.head 2 +  External Assemblies
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 3 +  User
.head 4 -  Number: INVALID_UsrPswd = 21017
.head 4 -  Number: PM_FindCase = SAM_User + 1
.head 4 -  Number: PM_NextField = SAM_User + 2
.head 4 -  Number: PM_PopulateDocket = SAM_User + 3
.head 4 -  Number: PM_PopulateRecpt = SAM_User + 4
.head 4 -  String: sQuote='"'
.head 4 -  String: sQuoteReturn='"
'
.head 4 -  String: sQuoteComma='","'
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.data RESOURCE 0 0 1 3363324058
0000: 5E0000004A000000 0000000000000000 02000003000000AD 0600001019000000
0020: 0102102009290000 001008005D010B00 04000000A3C60600 04C5000202102B00
0040: 105E01DE0B0068CA 060019000D019936 00000A001061025A 00360400
.enddata
.head 3 -  Sql Handle: hSqlAtty
.head 3 -  Sql Handle: hSqlFind
.head 3 -  File Handle: hFlatFile
.head 3 -  File Handle: hTextFile
.head 3 -  Boolean: bOk
.head 3 -  Number: nFind
.head 3 -  Number: nPos
.head 3 -  Number: nTotalAmt
.head 3 -  Date/Time: dtRecDate
.head 2 +  Internal Functions
.head 3 +  Function: ClearFields
.head 4 -  Description: Clears all data fields on the current form.
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Window Handle: hWndCurrent
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Window Handle: hWndChild
.head 4 +  Actions
.head 5 -  Set hWndChild = SalGetFirstChild( hWndCurrent, TYPE_DataField )
.head 5 +  While NOT hWndChild = hWndNULL
.head 6 -  Call SalClearField( hWndChild )
.head 6 -  Set hWndChild = SalGetNextChild( hWndChild, TYPE_DataField )
.head 5 -  Set hWndChild = SalGetFirstChild( hWndCurrent, TYPE_ChildTable )
.head 5 +  While NOT hWndChild = hWndNULL
.head 6 -  Call SalTblReset (hWndChild)
.head 6 -  Set hWndChild = SalGetNextChild( hWndChild, TYPE_ChildTable )
.head 5 -  Set hWndChild = SalGetFirstChild( hWndCurrent, TYPE_Any )
.head 5 -  Call SalSetFocus( hWndChild )
.head 2 -  Named Exceptions
.head 2 -  Named Toolbars
.head 2 +  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 1216495117
0000: 5A0100000B010000 0000000000000000 0200000300FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F19003C00 00FFFE00FF084300
0040: 72004400006F0063 006B006500007400 2200000001003C00 0019000500010210
0060: 3129080000001008 7B00C03000040000 0010018029000001 003900FF63008852
0080: 006570006F0A7274 73009A00C0000005 0000005B190001A2 021D290000021005
00A0: 003E001C97680000 0019001604000202 10AF0010F806003E 82001D00000004F5
00C0: 00022B00100700BE 3E00602100000004 00FD028A00100800 3E2F002500580000
00E0: 040002BF00E21009 003E0B0001801900 810002000000FF01 0563004A00610060
0100: 69006C002200F401 0000161900040210 AE31001040560048 0004000003
.enddata
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Separator:
.head 3 -  Tree Control:
.head 3 -  Navigation Bar:
.head 3 -  Pane Separator:
.head 3 -  Progress Bar:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture: cQuickPicture
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 +  Application Actions
.head 3 +  On SAM_AppStartup
.head 4 +  If strArgArray[1] = ''
.head 5 -  Call SalModalDialog( dlgLogin, hWndNULL, 'Civil Division Case Reporting Program' )
.head 4 +  Else
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 -  Set SqlUser = strArgArray[1]
.head 5 -  Set SqlPassword = strArgArray[2]
.head 5 -  Set SqlDatabase = strArgArray[3]
.head 5 -  Set bLogin = SqlConnect( hSql )
.head 4 +  If NOT bLogin
.head 5 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 5 -  Call SalWaitCursor( FALSE )
.head 5 -  Call SalQuit(  )
.head 4 +  Else
.head 5 -  Call SalWaitCursor( FALSE )
.head 4 -  Call SqlConnect( hSqlAtty )
.head 4 -  Call SqlConnect( hSqlFind )
.head 4 -  Call GetCourtInfo(  )
.head 4 -  Call GetUserInfo( SqlUser )
.head 4 -  Call SalCreateWindow( frmCVCreditBureau, hWndNULL )
.head 3 +  On SAM_AppExit
.head 4 +  If bLogin
.head 5 -  Call SqlDisconnect(hSql)
.head 5 -  Call SqlDisconnect(hSqlAtty)
.head 5 -  Call SqlDisconnect(hSqlFind)
.head 1 +  Form Window: frmCVCreditBureau
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Credit Bureau Reporting
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Normal
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 1.463"
.head 4 -  Top: 0.198"
.head 4 -  Width:  7.413"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 3.469"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Gray
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Data Field: dfStartDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.888"
.head 6 -  Top: 0.656"
.head 6 -  Width:  1.063"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = SalDateMonthBegin( SalDateCurrent(  ) - 15 )
.head 3 +  Data Field: dfEndDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.888"
.head 6 -  Top: 0.99"
.head 6 -  Width:  1.063"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = SalDateMonthBegin( SalDateCurrent(  ) ) -1
.head 3 +  Pushbutton: pbNew
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Process New Cases
.head 4 -  Window Location and Size
.head 5 -  Left: 1.113"
.head 5 -  Top: 2.292"
.head 5 -  Width:  1.96"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If Not fOpenFile( hNewFile, 'CREDNEW.TXT' )
.head 7 -  Return FALSE
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SalDisableWindow( pbNew )
.head 6 -  Set sStartDate = SalFmtFormatDateTime( dfStartDate, 'MM/dd/yyyy' )
.head 6 -  Set sEndDate = SalFmtFormatDateTime( dfEndDate, 'MM/dd/yyyy' )
.head 6 -  Set sSystemDate = SalFmtFormatDateTime( SalDateCurrent(  ), 'MM/dd/yyyy' )
.head 6 -  Call SqlPrepareAndExecute( hSql, "SELECT C.CASEYR || 'CV' || CASETYPE, C.CASENO, 
	          C.RENTESCROW, C.BOOKDATE, L.NAME, C.PRAYER, C.JUDGMENTDT, L.LITTYPE, L.ALIASTYPE, 
	          ADDR2, L.STRNO || ' ' || L.STRNAME || ' ' || L.STRTYPE || ' ' || L.STRDIR || ' ' || L.APT , L.CITY, L.STATE, L.ZIP, L.ATTNO 
	FROM CASEMASTER C, LITIGANT L 	
	INTO :sCase, :sCaseNo, :sRentEscrow, :dtBookDate, :sName, :nPrayer, :dtJudgmentDate, :sLitType, :sAliasType, 
	          :sAddr2, :sStreet, :sCity, :sState, :sZip, :nAttno 
	WHERE BOOKDATE >= :dfStartDate AND BOOKDATE <= :dfEndDate AND 
	          C.CASEYR = L.CASEYR AND C.CASENO = L.CASENO 
	ORDER BY C.CASEYR, C.CASENO, L.LITTYPE DESC ")
.head 6 -  !
.head 6 -  Set bOk = TRUE
.head 6 -  Set dfFetchRow = 0
.head 6 +  While bOk = TRUE
.head 7 -  Set bOk = SqlFetchNext (hSql, nError)
.head 7 +  If nError != FETCH_Ok
.head 8 -  Set bOk = FALSE
.head 7 +  Else
.head 8 -  Set dfFetchRow = dfFetchRow + 1
.head 8 -  Set nLength = SalStrLength ( sCaseNo )
.head 8 +  While nLength < 5
.head 9 -  Set nLength = nLength + 1
.head 9 -  Set sCaseNo = '0' || sCaseNo 
.head 8 -  Set sCase = sCase || sCaseNo
.head 8 -  Set nLength = SalStrLength ( sRentEscrow )
.head 8 +  While nLength < 4
.head 9 -  Set nLength = nLength + 1
.head 9 -  Set sRentEscrow = '0' || sRentEscrow 
.head 8 -  Set sBookDate = SalFmtFormatDateTime( dtBookDate, 'MM/dd/yyyy' )
.head 8 -  Set sPrayer = SalFmtFormatNumber( nPrayer, '####0.00' )
.head 8 -  Set sJudgmentDate = SalFmtFormatDateTime( dtJudgmentDate, 'MM/dd/yyyy' )
.head 8 -  Set sJudgment = SalFmtFormatNumber( nJudgment, '####0.00' )
.head 8 +  If sAliasType = 'A'  OR sAliasType = 'B' OR sAliasType = 'F'
.head 9 -  Set sLitType = sAliasType
.head 8 +  If nAttno = NUMBER_Null
.head 9 -  Set sAtty = 'Self'
.head 8 +  Else
.head 9 -  Call SqlPrepareAndExecute( hSqlAtty, "SELECT LNAME || ', ' || FNAME || ' ' || MNAME 
	FROM ATTORNEY INTO :sAtty WHERE ATTNO = :nAttno" )
.head 9 -  Call SqlFetchNext( hSqlAtty, nFind )
.head 9 +  If nFind != FETCH_Ok
.head 10 -  Set sAtty = SalNumberToStrX( nAttno, 0 )
.head 8 -  !
.head 8 -  Set sFileData = sQuote || sCase || sQuoteComma || sBookDate || sQuoteComma || 
	sPrayer || sQuoteComma || sName || sQuoteComma || sLitType || sQuoteComma || sAliasType || sQuoteComma || sAtty || sQuoteComma || 
	sAddr2 || sQuoteComma || sStreet || sQuoteComma || sCity || sQuoteComma || sState || sQuoteComma || sZip || sQuoteComma || 
	sStartDate || sQuoteComma || sEndDate || sQuoteComma || sSystemDate || sQuoteReturn
.head 8 +  If SalFileWrite( hNewFile, sFileData, SalStrLength( sFileData ) ) < 1
.head 9 -  Call SalMessageBox( 'Error Writing to Floppy Drive', 'Write Error', MB_Ok )
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 +  If SalFileClose ( hNewFile )
.head 7 -  Call SalMessageBox( 'CREDNEW File successfully closed', 'File Closed', MB_Ok )
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Error Closing CREDNEW', 'Close Error', MB_Ok )
.head 3 +  Pushbutton: pbJudgments
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Process JEF, JD, JP Entries
.head 4 -  Window Location and Size
.head 5 -  Left: 3.4"
.head 5 -  Top: 2.292"
.head 5 -  Width:  2.31"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call fOpenFile( hFlatFile, 'CREDFLAT.TXT' )
.head 6 -  Call fOpenFile( hTextFile, 'CREDTEXT.TXT' )
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SalDisableWindow( pbJudgments )
.head 6 -  Set sStartDate = SalFmtFormatDateTime( dfStartDate, 'MM/dd/yyyy' )
.head 6 -  Set sEndDate = SalFmtFormatDateTime( dfEndDate, 'MM/dd/yyyy' )
.head 6 -  Set sSystemDate = SalFmtFormatDateTime( SalDateCurrent(  ), 'MM/dd/yyyy' )
.head 6 -  Call SqlPrepareAndExecute( hSql, "SELECT C.CASEYR || 'CV' || CASETYPE, C.CASENO, 
	          C.BOOKDATE, L.NAME, C.PRAYER, C.JUDGMENTDT, C.JUDGMENT, L.LITTYPE, L.ALIASTYPE, 
	          L.ADDR2, L.STRNO || ' ' || L.STRNAME || ' ' || L.STRTYPE || ' ' || L.STRDIR || ' ' || L.APT, L.CITY, L.STATE, L.ZIP, L.ATTNO, D.DDATE  
	FROM CASEMASTER C, DOCKET D, LITIGANT L 	
	INTO :sCase, :sCaseNo, :dtBookDate, :sName, :nPrayer, :dtJudgmentDate, :nJudgment, :sLitType, 
	          :sAliasType, :sAddr2, :sStreet, :sCity, :sState, :sZip, :nAttno, :dtDocketDate
	WHERE (D.DDATE >= :dfStartDate AND D.DDATE <= :dfEndDate) AND 
		(D.RATTY IS NULL OR D.RATTY != 9898989) AND 
		(C.CASEYR = D.CASEYR AND C.CASENO = D.CASENO) AND 
	 	(C.CASEYR = L.CASEYR AND C.CASENO = L.CASENO) AND 
		CODE5 IN ( 'JE', 'JEF', 'JD', 'JP', 'CD', 'CD35', 'CD6A', 'SOJ', 'SJ', 'CDM', 'CD40', 'RRWR', 'RRWC', 'RRBC', 'RRF', 'RRA') 
	ORDER BY C.CASEYR, C.CASENO, L.LITTYPE DESC, L.LITNO, D.DDATE")
.head 6 -  !
.head 6 -  Set bOk = TRUE
.head 6 -  Set dfFetchRow = 0
.head 6 -  Set dfLitigantNo = 0
.head 6 +  While bOk = TRUE
.head 7 -  Set bOk = SqlFetchNext (hSql, nError)
.head 7 +  If nError != FETCH_Ok
.head 8 -  Set bOk = FALSE
.head 7 +  Else
.head 8 -  Set dfLitigantNo = dfLitigantNo + 1
.head 8 -  Set nCaseYr = SalStrToNumber( SalStrLeftX( sCase, 4 ) )
.head 8 -  Set nCaseNo = SalStrToNumber( sCaseNo )
.head 8 -  Set nLength = SalStrLength ( sCaseNo )
.head 8 +  While nLength < 5
.head 9 -  Set nLength = nLength + 1
.head 9 -  Set sCaseNo = '0' || sCaseNo 
.head 8 -  Set sCase = sCase || sCaseNo
.head 8 -  Set sBookDate = SalFmtFormatDateTime( dtBookDate, 'MM/dd/yyyy' )
.head 8 -  Set sPrayer = SalFmtFormatNumber( nPrayer, '####0.00' )
.head 8 -  Set sJudgmentDate = SalFmtFormatDateTime( dtJudgmentDate, 'MM/dd/yyyy' )
.head 8 -  Set sJudgment = SalFmtFormatNumber( nJudgment, '####0.00' )
.head 8 +  If nAttno = NUMBER_Null
.head 9 -  Set sAtty = 'Self'
.head 8 +  Else
.head 9 -  Call SqlPrepareAndExecute( hSqlAtty, "SELECT LNAME || ', ' || FNAME || ' ' || MNAME 
	FROM ATTORNEY INTO :sAtty WHERE ATTNO = :nAttno" )
.head 9 -  Call SqlFetchNext( hSqlAtty, nFind )
.head 9 +  If nFind != FETCH_Ok
.head 10 -  Set sAtty = SalNumberToStrX( nAttno, 0 )
.head 8 -  !
.head 8 -  Set sFileData = sQuote || sCase || sQuoteComma || sBookDate || sQuoteComma || 
	sPrayer || sQuoteComma || sName || sQuoteComma || sLitType || sQuoteComma || sAliasType || sQuoteComma || sAtty || sQuoteComma || 
	sAddr2 || sQuoteComma || sStreet || sQuoteComma || sCity || sQuoteComma || sState || sQuoteComma || sZip || sQuoteComma || 
	sStartDate || sQuoteComma || sEndDate || sQuoteComma || sSystemDate || sQuoteReturn
.head 8 +  If (sName != sHoldLitigant) OR (sCase != sHoldCase)
.head 9 -  Call SalFileWrite( hFlatFile, sFileData, SalStrLength( sFileData ) )
.head 9 -  Set sHoldLitigant = sName
.head 8 -  !
.head 8 +  If (sCase != sHoldCase) OR (sCase = sHoldCase AND dtDocketDate != dtHoldDocketDate)
.head 9 -  Call SqlPrepareAndExecute( hSqlFind,  'SELECT ALL D.CODE5, D.DDATE, C.CODE1 || D.ENTRY1 || C.CODE2 || D.ENTRY2 || C.CODE3 || D.ENTRY3 || C.CODE4 || D.ENTRY4, D.MEMO, D.RDATE, D.RAMT
	FROM DOCKET D, CVCODES C  INTO :sDocketCode, :sDocketDate, :sDocketData, :sDocketMemo, :sDocketRDate, :nDocketRAmt 
	WHERE (C.CODE = D.CODE5) AND (D.RATTY IS NULL OR D.RATTY != 9898989 ) AND (D.DDATE = :dtDocketDate AND D.CASEYR = :nCaseYr AND CASENO = :nCaseNo)
	ORDER BY D.DNUM' )
.head 9 -  Set nFind = FETCH_Ok
.head 9 +  While nFind = FETCH_Ok
.head 10 -  Call SqlFetchNext( hSqlFind, nFind )
.head 10 +  If nFind = FETCH_Ok
.head 11 -  Set dfFetchRow = dfFetchRow + 1
.head 11 -  Set sFileData = sQuote || sCase || sQuoteComma || SalFmtFormatDateTime( sDocketDate, 'MM/dd/yyyy' ) || sQuoteComma || sDocketCode || sQuoteComma || 
	sDocketData || sQuoteComma || sDocketMemo || sQuoteComma || SalFmtFormatDateTime( sDocketRDate, 'MM/dd/yyyy' ) || sQuoteComma || 
	SalFmtFormatNumber( nDocketRAmt, '####0.00' )  || sQuoteComma || sJudgment || sQuoteComma || sJudgmentDate || sQuoteComma || 
	sStartDate || sQuoteComma || sEndDate || sQuoteComma || sSystemDate || sQuoteReturn
.head 11 -  Call SalFileWrite( hTextFile, sFileData, SalStrLength( sFileData ) )
.head 9 -  Set sHoldCase = sCase
.head 9 -  Set dtHoldDocketDate = dtDocketDate 
.head 9 -  Set sHoldDocketCode = sDocketCode
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalFileClose( hFlatFile )
.head 6 -  Call SalFileClose( hTextFile )
.head 3 +  Pushbutton: pbGarnishments
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Process Garnishments
.head 4 -  Window Location and Size
.head 5 -  Left: 5.025"
.head 5 -  Top: 2.313"
.head 5 -  Width:  2.31"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalModalDialog( dlgReport, hWndForm, sType )
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Set sStartDate = SalFmtFormatDateTime( dfStartDate, 'MM/dd/yyyy' )
.head 6 -  Set sEndDate = SalFmtFormatDateTime( dfEndDate, 'MM/dd/yyyy' )
.head 6 -  Set sSystemDate = SalFmtFormatDateTime( SalDateCurrent(  ), 'MM/dd/yyyy' )
.head 6 -  Call SqlPrepareAndExecute( hSql, "SELECT C.CASEYR || 'CV' || CASETYPE, C.CASENO, 
	          C.BOOKDATE, L.NAME, C.PRAYER, C.JUDGMENTDT, C.JUDGMENT, L.LITTYPE, L.ALIASTYPE, 
	          L.ADDR2, L.STRNO || ' ' || L.STRNAME || ' ' || L.STRTYPE || ' ' || L.STRDIR || ' ' || L.APT, L.CITY, L.STATE, L.ZIP, L.ATTNO, D.DDATE, D.ENTRY1 || D.ENTRY2, D.ENTRY3, D.MEMO 
	FROM CASEMASTER C, DOCKET D, LITIGANT L 	
	INTO :sCase, :sCaseNo, :dtBookDate, :sName, :nPrayer, :dtJudgmentDate, :nJudgment, :sLitType, 
	          :sAliasType, :sAddr2, :sStreet, :sCity, :sState, :sZip, :nAttno, :dtDocketDate, :sDocketData, :sStringDocketDate, :sDocketMemo 
	WHERE (D.DDATE >= :dfStartDate AND D.DDATE <= :dfEndDate) AND 
		(D.RATTY IS NULL OR D.RATTY != 9898989) AND 
		(C.CASEYR = D.CASEYR AND C.CASENO = D.CASENO) AND 
	 	(C.CASEYR = L.CASEYR AND C.CASENO = L.CASENO AND L.LITTYPE = 'D' AND L.NAME = D.MEMO) AND 
		CODE5 IN ( 'WAN', 'BAN') 
	ORDER BY C.CASEYR, C.CASENO, L.LITTYPE DESC, L.LITNO, D.DDATE")
.head 6 -  !
.head 6 -  Set bOk = TRUE
.head 6 -  Set dfFetchRow = 0
.head 6 -  Set dfLitigantNo = 0
.head 6 +  If sType = 'Report'
.head 7 -  Set sReportBinds = 'sCase, sName, sAddr2, sCity, sDocketData, dtDocketDate, sStringDocketDate, sCourt, sCourtClerk'
.head 7 -  Set sReportInputs ='CaseNumber, PAYNAME, ADDRESS1, ADDRESS2, Employer, FileDate, AnswerDate, Court, CourtClerk'
.head 7 -  Call SalReportPrint( hWndForm, CV_REPORT_Path || 'GarnList.qrp', sReportBinds, sReportInputs, 1, RPT_PrintAll, 0, 0, nPrintErr )
.head 6 +  Else
.head 7 -  Call SalFileOpen( hFlatFile, 'A:\Garn.TXT', OF_Create  | OF_Write )
.head 7 +  While bOk = TRUE
.head 8 -  Set bOk = SqlFetchNext (hSql, nError)
.head 8 +  If nError != FETCH_Ok
.head 9 -  Set bOk = FALSE
.head 8 +  Else
.head 9 -  Set dfLitigantNo = dfLitigantNo + 1
.head 9 -  Set nCaseYr = SalStrToNumber( SalStrLeftX( sCase, 4 ) )
.head 9 -  Set nCaseNo = SalStrToNumber( sCaseNo )
.head 9 -  Set nLength = SalStrLength ( sCaseNo )
.head 9 +  While nLength < 5
.head 10 -  Set nLength = nLength + 1
.head 10 -  Set sCaseNo = '0' || sCaseNo 
.head 9 -  Set sCase = sCase || sCaseNo
.head 9 -  Set sBookDate = SalFmtFormatDateTime( dtBookDate, 'MM/dd/yyyy' )
.head 9 -  Set sPrayer = SalFmtFormatNumber( nPrayer, '####0.00' )
.head 9 -  Set sJudgmentDate = SalFmtFormatDateTime( dtJudgmentDate, 'MM/dd/yyyy' )
.head 9 -  Set sJudgment = SalFmtFormatNumber( nJudgment, '####0.00' )
.head 9 +  If nAttno = NUMBER_Null
.head 10 -  Set sAtty = 'Self'
.head 9 +  Else
.head 10 -  Call SqlPrepareAndExecute( hSqlAtty, "SELECT LNAME || ', ' || FNAME || ' ' || MNAME 
	FROM ATTORNEY INTO :sAtty WHERE ATTNO = :nAttno" )
.head 10 -  Call SqlFetchNext( hSqlAtty, nFind )
.head 10 +  If nFind != FETCH_Ok
.head 11 -  Set sAtty = SalNumberToStrX( nAttno, 0 )
.head 9 -  !
.head 9 -  Set sFileData = sQuote || sCase || sQuoteComma || sBookDate || sQuoteComma || 
	sPrayer || sQuoteComma || sName || sQuoteComma || sLitType || sQuoteComma || sAliasType || sQuoteComma || sAtty || sQuoteComma || 
	sAddr2 || sQuoteComma || sStreet || sQuoteComma || sCity || sQuoteComma || sState || sQuoteComma || sZip || sQuoteComma || 
	SalFmtFormatDateTime( dtDocketDate, 'MM/dd/yyyy' ) || sQuoteComma || sDocketData || sQuoteComma || sStringDocketDate || sQuoteComma || 
	sStartDate || sQuoteComma || sEndDate || sQuoteComma || sSystemDate || sQuoteReturn
.head 9 +  If (sName != sHoldLitigant) OR (sCase != sHoldCase)
.head 10 -  Call SalFileWrite( hFlatFile, sFileData, SalStrLength( sFileData ) )
.head 10 -  Set sHoldLitigant = sName
.head 9 -  !
.head 9 +  ! If (sCase != sHoldCase) OR (sCase = sHoldCase AND dtDocketDate != dtHoldDocketDate)
.head 10 -  Call SqlPrepareAndExecute( hSqlFind,  'SELECT ALL D.CODE5, D.DDATE, C.CODE1 || D.ENTRY1 || C.CODE2 || D.ENTRY2 || C.CODE3 || D.ENTRY3 || C.CODE4 || D.ENTRY4, D.MEMO, D.RDATE, D.RAMT
	FROM DOCKET D, CVCODES C  INTO :sDocketCode, :sDocketDate, :sDocketData, :sDocketMemo, :sDocketRDate, :nDocketRAmt 
	WHERE (C.CODE = D.CODE5) AND (D.RATTY IS NULL OR D.RATTY != 9898989 ) AND (D.DDATE = :dtDocketDate AND D.CASEYR = :nCaseYr AND CASENO = :nCaseNo)
	ORDER BY D.DNUM' )
.head 10 -  Set nFind = FETCH_Ok
.head 10 +  While nFind = FETCH_Ok
.head 11 -  Call SqlFetchNext( hSqlFind, nFind )
.head 11 +  If nFind = FETCH_Ok
.head 12 -  Set dfFetchRow = dfFetchRow + 1
.head 12 -  Set sFileData = sQuote || sCase || sQuoteComma || SalFmtFormatDateTime( sDocketDate, 'MM/dd/yyyy' ) || sQuoteComma || sDocketCode || sQuoteComma || 
	sDocketData || sQuoteComma || sDocketMemo || sQuoteComma || SalFmtFormatDateTime( sDocketRDate, 'MM/dd/yyyy' ) || sQuoteComma || 
	SalFmtFormatNumber( nDocketRAmt, '####0.00' )  || sQuoteComma || sJudgment || sQuoteComma || sJudgmentDate || sQuoteComma || 
	sStartDate || sQuoteComma || sEndDate || sQuoteComma || sSystemDate || sQuoteReturn
.head 12 -  Call SalFileWrite( hTextFile, sFileData, SalStrLength( sFileData ) )
.head 10 -  Set sHoldCase = sCase
.head 10 -  Set dtHoldDocketDate = dtDocketDate 
.head 10 -  Set sHoldDocketCode = sDocketCode
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalFileClose( hFlatFile )
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 29767
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 1.125"
.head 5 -  Top: 0.115"
.head 5 -  Width:  4.76"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.342"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Arial
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Credit Bureau Reporting
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 29768
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.088"
.head 5 -  Top: 0.677"
.head 5 -  Width:  1.21"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Starting Date
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 29769
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.113"
.head 5 -  Top: 1.031"
.head 5 -  Width:  1.21"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Ending Date
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 21249
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.1"
.head 5 -  Top: 1.781"
.head 5 -  Width:  1.61"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Processing Litigant: 
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfFetchRow
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 4
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.3"
.head 6 -  Top: 1.396"
.head 6 -  Width:  0.65"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 45363
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.1"
.head 5 -  Top: 1.469"
.head 5 -  Width:  2.06"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Processing Docket Entry: 
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfLitigantNo
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 4
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.3"
.head 6 -  Top: 1.708"
.head 6 -  Width:  0.65"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  Function: fOpenFile
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Receive File Handle: hFileHandle
.head 5 -  String: sFile
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sDirectory
.head 4 +  Actions
.head 5 -  Set sDirectory = 'I:\\CreditBureau\\' || SalFmtFormatDateTime( dfStartDate, 'yyyy' )
.head 5 -  Call SalFileCreateDirectory( sDirectory )
.head 5 -  Set sDirectory = sDirectory || '\\' || SalFmtFormatDateTime( dfStartDate, 'MMM' )
.head 5 -  Set sDirectory = sDirectory || '\\'
.head 5 -  Call SalFileCreateDirectory( sDirectory )
.head 5 +  If Not SalFileOpen( hFileHandle, sDirectory || sFile, OF_Create  | OF_Write )
.head 6 -  Call SalMessageBox( 'Error Opening ' || sDirectory || sFile, 'Open Error', MB_Ok )
.head 6 -  Return FALSE
.head 5 -  Return TRUE
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Long String: sFileData
.head 3 -  String: sCase
.head 3 -  String: sCaseNo
.head 3 -  String: sHoldCase
.head 3 -  String: sRentEscrow
.head 3 -  String: sBookDate
.head 3 -  Date/Time: dtBookDate
.head 3 -  String: sPrayer
.head 3 -  Number: nPrayer
.head 3 -  String: sName
.head 3 -  String: sJudgmentDate
.head 3 -  Date/Time: dtJudgmentDate
.head 3 -  Number: nJudgment
.head 3 -  String: sJudgment
.head 3 -  String: sAtty
.head 3 -  String: sLitType
.head 3 -  String: sAliasType
.head 3 -  String: sAddr2
.head 3 -  String: sStreet
.head 3 -  String: sCity
.head 3 -  String: sState
.head 3 -  String: sZip
.head 3 -  String: sZip4
.head 3 -  String: sStartDate
.head 3 -  String: sEndDate
.head 3 -  String: sSystemDate
.head 3 -  Number: nLength
.head 3 -  Number: nAttno
.head 3 -  !
.head 3 -  Number: nCaseYr
.head 3 -  Number: nCaseNo
.head 3 -  Date/Time: dtDocketDate
.head 3 -  Date/Time: dtHoldDocketDate
.head 3 -  String: sHoldDocketCode
.head 3 -  String: sHoldLitigant
.head 3 -  String: sDocketCode
.head 3 -  Date/Time: sDocketDate
.head 3 -  Long String: sDocketData
.head 3 -  Long String: sDocketMemo
.head 3 -  String: sStringDocketDate
.head 3 -  Date/Time: sDocketRDate
.head 3 -  Number: nDocketRAmt
.head 3 -  String: sType
.head 3 -  File Handle: hNewFile
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call SalWaitCursor( FALSE )
.head 3 +  On SAM_ReportStart
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  While SqlFetchNext (hSql, nError)
.head 5 -  Set dfLitigantNo = dfLitigantNo + 1
.head 5 -  Set nCaseYr = SalStrToNumber( SalStrLeftX( sCase, 4 ) )
.head 5 -  Set nCaseNo = SalStrToNumber( sCaseNo )
.head 5 -  Set nLength = SalStrLength ( sCaseNo )
.head 5 +  While nLength < 5
.head 6 -  Set nLength = nLength + 1
.head 6 -  Set sCaseNo = '0' || sCaseNo 
.head 5 -  Set sCase = sCase || sCaseNo
.head 5 +  If SalStrTrimX( sStreet ) = ','
.head 6 -  Set sStreet = ''
.head 5 +  If SalStrTrimX( sAddr2 ) = ','
.head 6 -  Set sAddr2 = ''
.head 5 +  If SalStrTrimX( sCity ) = ','
.head 6 -  Set sCity = ''
.head 5 -  Set sCity = sCity || ', ' || sState || '  ' || sZip
.head 5 +  If sAddr2 != ''
.head 6 -  Set sAddr2 = sStreet || '   ' || sAddr2
.head 5 +  Else
.head 6 -  Set sAddr2 = sStreet
.head 5 -  Return TRUE
.head 4 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return TRUE
.head 1 +  Dialog Box: dlgReport
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Report / Disk
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.45"
.head 4 -  Top: 0.167"
.head 4 -  Width:  5.0"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.604"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Gray
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 49704
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.488"
.head 5 -  Top: 0.146"
.head 5 -  Width:  4.075"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.323"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Arial
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Create Garnishment
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton: pbDisk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Disk
.head 4 -  Window Location and Size
.head 5 -  Left: 0.463"
.head 5 -  Top: 0.885"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sType = 'Disk'
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 3 +  Pushbutton: pbReport
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Report
.head 4 -  Window Location and Size
.head 5 -  Left: 3.35"
.head 5 -  Top: 0.885"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sType = 'Report'
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  Receive String: sType
.head 2 -  Window Variables
.head 2 -  Message Actions
