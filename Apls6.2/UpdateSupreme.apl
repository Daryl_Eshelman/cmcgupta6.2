.head 0 +  Application Description: Criminal Software Integrated Library
11-05-2007 - DE
   * Includes 	-Civil Update of the CR_Supreme table used in the Supreme Court Count Process
.head 1 -  Outline Version - 4.0.39
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000A30000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF2C0000 002C0000002F0200 0066010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D01000000075769 6E646F7773
.enddata
.data DT_MAKERUNDLG
0000: 020000000033433A 5C43656E74757261 5C4372696D333220 2D204F5241434C45
0020: 5C4372696D4E6577 2D4D6173735C4372 4C6F67696E2E6578 6533433A5C43656E
0040: 747572615C437269 6D3332202D204F52 41434C455C437269 6D4E65772D4D6173
0060: 735C43724C6F6769 6E2E646C6C33433A 5C43656E74757261 5C4372696D333220
0080: 2D204F5241434C45 5C4372696D4E6577 2D4D6173735C4372 4C6F67696E2E6170
00A0: 6300000101013343 3A5C43656E747572 615C4372696D3332 202D204F5241434C
00C0: 455C4372696D4E65 772D4D6173735C43 724C6F67696E2E72 756E33433A5C4365
00E0: 6E747572615C4372 696D3332202D204F 5241434C455C4372 696D4E65772D4D61
0100: 73735C43724C6F67 696E2E646C6C3343 3A5C43656E747572 615C4372696D3332
0120: 202D204F5241434C 455C4372696D4E65 772D4D6173735C43 724C6F67696E2E61
0140: 7063000001010133 433A5C43656E7475 72615C4372696D33 32202D204F524143
0160: 4C455C4372696D4E 65772D4D6173735C 43724C6F67696E2E 61706433433A5C43
0180: 656E747572615C43 72696D3332202D20 4F5241434C455C43 72696D4E65772D4D
01A0: 6173735C43724C6F 67696E2E646C6C33 433A5C43656E7475 72615C4372696D33
01C0: 32202D204F524143 4C455C4372696D4E 65772D4D6173735C 43724C6F67696E2E
01E0: 6170630000010101 33433A5C43656E74 7572615C4372696D 3332202D204F5241
0200: 434C455C4372696D 4E65772D4D617373 5C43724C6F67696E 2E61706C33433A5C
0220: 43656E747572615C 4372696D3332202D 204F5241434C455C 4372696D4E65772D
0240: 4D6173735C43724C 6F67696E2E646C6C 33433A5C43656E74 7572615C4372696D
0260: 3332202D204F5241 434C455C4372696D 4E65772D4D617373 5C43724C6F67696E
0280: 2E61706300000101 01
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040002001B00 0200000000000000 00003816A00F0500 1D00FFFF4D61696E
0020: 0020000100040000 0000000000F51E81 0F0000DF00FFFF56 61726961626C6573
0040: 0029000100040000 0000000000F51E81 0F00008600FFFF49 6E7465726E616C20
0060: 46756E6374696F6E 73001E0001000400 000000000000F51E 810F0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: 0.01"
.head 3 -  Top: 0.133"
.head 3 -  Width:  7.96"
.head 3 -  Height: 4.425"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C4389105B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 8.325"
.head 3 -  Top: 0.135"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 1 -  Libraries
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MM-dd-yyyy
.head 2 -  External Functions
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 -  System
.head 3 -  User
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 -  Variables
.head 2 +  Internal Functions
.head 3 +  Function: UpdateSupreme
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 5 -  Boolean: bCommit
.head 5 -  String: sProgramLocation
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtToday
.head 5 -  Number: nMaxSeq
.head 5 -  String: sUser_Name
.head 4 +  Actions
.head 5 +  If dtInsDateF = DATETIME_Null
.head 6 -  Set dtToday = Current_Date( 'Date' )
.head 5 +  Else
.head 6 -  Set dtToday =dtInsDateF
.head 5 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	caseyr
			      FROM	cr_supreme
			      WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo and
					type = :sType and
					ins_date = :dtToday and
					line_no = :nLine')
.head 5 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 5 +  If nReturn = FETCH_EOF
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	max(seq)
			     FROM		cr_supreme
			     WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo
			     INTO		:nMaxSeq')
.head 6 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 6 +  If nMaxSeq = NUMBER_Null
.head 7 -  Set nMaxSeq = 0
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	user 
			     FROM		dual 
			     INTO		:sUser_Name')
.head 6 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 6 -  Set nMaxSeq = nMaxSeq + 1
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'INSERT INTO	cr_supreme (caseyr, casety, caseno, seq, ins_date, type, line_no, judge, program_location, user_name)
			             VALUES		   ( :sCaseYr, :sCaseTy, :sCaseNo, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation, :sUser_Name)')
			             				
.head 6 +  If bCommit
.head 7 -  Call SqlCommit(hSqlSup)
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 2 -  Named Menus
.head 2 -  Class Definitions
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 -  Application Actions
