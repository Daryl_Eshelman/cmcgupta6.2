.head 0 +  Application Description: Registry Utilities with CTD 1.5 or higher

This software is meant as a demonstration of how to access the 
Windows registry. There is no guarantee that it is functional and
safe to use. The author is neither responsible for anything related
to this demo code nor is he obliged or bound by law to support the
code.

Author: Joe Meyer (Joe@iceteagroup.com), Ice Tea Group, LLC.

Visit the Ice Tea Group site at http://www.iceteagroup.com
.head 1 -  Outline Version - 4.0.39
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000D40000 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE2FFFFFFFFFFFF FF000000007C0200 004D010000010000 0000000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0004000000FF FEFF0743006C0061 0073007300650073 00FFFEFF0B630042
00A0: 0054005200650067 0069007300740072 007900FFFEFF0946 0075006E00630074
00C0: 0069006F006E0073 00FFFEFF0B570072 0069007400650053 007400720069006E
00E0: 006700
.enddata
.data DT_MAKERUNDLG
0000: 0000000024533A5C 50726F6A656B7465 5C496D616765735C 49636F6E735C5749
0020: 4E464C41472E4943 4F24533A5C4A6F65 5C50726F5075626C 5C52656769737472
0040: 795C526567697374 72792E6578652453 3A5C4A6F655C5072 6F5075626C5C5265
0060: 6769737472795C52 656769737472792E 646C6C24533A5C4A 6F655C50726F5075
0080: 626C5C5265676973 7472795C52656769 737472792E617063 00000101011B533A
00A0: 5C4A6F655C43656E 747572615C526567 69737472792E7275 6E1B533A5C4A6F65
00C0: 5C43656E74757261 5C52656769737472 792E646C6C1B533A 5C4A6F655C43656E
00E0: 747572615C526567 69737472792E6170 6300000101011B53 3A5C4A6F655C4365
0100: 6E747572615C5265 6769737472792E61 70641B533A5C4A6F 655C43656E747572
0120: 615C526567697374 72792E646C6C1B53 3A5C4A6F655C4365 6E747572615C5265
0140: 6769737472792E61 706300000101011B 533A5C4A6F655C43 656E747572615C52
0160: 656769737472792E 61706C1B533A5C4A 6F655C43656E7475 72615C5265676973
0180: 7472792E646C6C1B 533A5C4A6F655C43 656E747572615C52 656769737472792E
01A0: 6170630000010101
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left: -0.013"
.head 3 -  Top: 0.0"
.head 3 -  Width:  8.013"
.head 3 -  Height: 4.969"
.head 2 +  Options Box Location
.data VIEWINFO
0000: 1018B80BB80B2500
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 6.388"
.head 3 -  Top: 0.729"
.head 2 -  Fully Qualified External References? Yes
.head 2 -  Reject Multiple Window Instances? No
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 1 +  Libraries
.head 2 -  File Include: cstructl.apl
.head 2 -  File Include: vt.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: 3D Face Color
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 2 -  Formats
.head 2 +  External Functions
.head 3 -  ! advapi32.dll comes with windows 95
  Some of the functions are commented out, because I don't
  need them right now and they are untested (see win32 api-
  documentation for further information)
.head 3 +  Library name: ADVAPI32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: RegCloseKey
.head 5 -  Description: function RegCloseKey(hKey: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: RegConnectRegistryW
.head 5 -  Description: function RegConnectRegistryW(
lpMachineName: PAnsiChar;
hKey: HKEY;
var phkResult: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 6 -  Number: LONG
.head 6 -  Receive Number: LPLONG
.head 4 +  Function: RegCreateKeyExW
.head 5 -  Description: function RegCreateKeyExW(
  hKey: HKEY;
  lpSubKey: PAnsiChar;
  Reserved: DWORD;
  lpClass: PAnsiChar;
  dwOptions: DWORD;
  samDesired: REGSAM;
  lpSecurityAttributes: PSecurityAttributes;
  var phkResult: HKEY;
  lpdwDisposition: PDWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPLONG
.head 6 -  Receive Number: LPLONG
.head 4 +  Function: RegEnumKeyExW
.head 5 -  Description: function RegEnumKeyExW(
  hKey: HKEY;
  dwIndex: DWORD;
  lpName: PAnsiChar;
  var lpcbName: DWORD;
  lpReserved: Pointer;
  lpClass: PAnsiChar;
  lpcbClass: PDWORD;
  lpftLastWriteTime: PFileTime): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: DWORD
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 6 +  structPointer
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 4 +  Function: RegEnumValueW
.head 5 -  Description: function RegEnumValueW(
  hKey: HKEY; 
  dwIndex: DWORD; 
  lpValueName: PChar;
  var lpcbValueName: DWORD; 
  lpReserved: Pointer; 
  lpType: PDWORD;
  lpData: PByte; 
  lpcbData: PDWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: DWORD
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 4 +  Function: RegFlushKey
.head 5 -  Description: function RegFlushKey(
hKey: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: RegLoadKeyW
.head 5 -  Description: LONG RegLoadKey(  HKEY hKey,        // handle to open key
  LPCTSTR lpSubKey, // address of name of subkey
  LPCTSTR lpFile    // address of filename for registry information);
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  String: LPSTR
.head 4 +  Function: RegSaveKeyW
.head 5 -  Description: LONG RegSaveKey(  HKEY hKey,       // handle to key where save begins
  LPCTSTR lpFile,  // file for saved data 
  LPSECURITY_ATTRIBUTES lpSecurityAttributes );
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Number: LPVOID
.head 4 +  Function: RegRestoreKeyW
.head 5 -  Description: LONG RegRestoreKey(  HKEY hKey,      // handle to key where restore begins
  LPCTSTR lpFile, // filename containing saved tree
  DWORD dwFlags   // optional flags);
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 4 +  Function: RegOpenKeyExW
.head 5 -  Description: function RegOpenKeyExW(
  hKey: HKEY;
  lpSubKey: PAnsiChar;
  ulOptions: DWORD;
  samDesired: REGSAM;
  var phkResult: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Receive Number: LPLONG
.head 4 +  Function: RegQueryInfoKeyW
.head 5 -  Description: function RegQueryInfoKeyW(
  hKey: HKEY; 
  lpClass: PChar;
  lpcbClass: PDWORD; 
  lpReserved: Pointer;
  lpcSubKeys, 
  lpcbMaxSubKeyLen, 
  lpcbMaxClassLen, 
  lpcValues,
  lpcbMaxValueNameLen, 
  lpcbMaxValueLen, 
  lpcbSecurityDescriptor: PDWORD;
  lpftLastWriteTime: PFileTime): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 +  structPointer
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 4 +  Function: RegQueryValueExW
.head 5 -  Description: function RegQueryValueExW(
  hKey: HKEY;
  lpValueName: PAnsiChar;
  lpReserved: Pointer;
  lpType: PDWORD;
  lpData: PByte;
  lpcbData: PDWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 4 +  Function: RegSetValueExW
.head 5 -  Description: function RegSetValueExW(
  hKey: HKEY;
  lpValueName: PAnsiChar;
  Reserved: DWORD;
  dwType: DWORD;
  lpData: Pointer;
  cbData: DWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  String: LPVOID
.head 6 -  Number: DWORD
.head 4 +  Function: RegDeleteKeyW
.head 5 -  Description: function RegDeleteKeyW(
  hKey: HKEY;
  lpSubKey: PAnsiChar): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 4 +  Function: RegDeleteValueW
.head 5 -  Description: function RegDeleteValueW(
  hKey: HKEY;
  lpValueName: PAnsiChar): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 4 +  ! Function: RegEnumKeyA
.head 5 -  Description: function RegEnumKeyA(
  hKey: HKEY;
  dwIndex: DWORD;
  lpName: PAnsiChar;
  cbName: DWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: DWORD
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 4 +  ! Function: RegEnumValueA
.head 5 -  Description: function RegEnumValueA(
  hKey: HKEY;
  dwIndex: DWORD;
  lpValueName: PChar;
  var lpcbValueName: DWORD;
  lpReserved: Pointer;
  lpType: PDWORD;
  lpData: PByte;
  lpcbData: PDWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: DWORD
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPBYTE
.head 6 -  Receive Number: LPDWORD
.head 4 +  ! Function: RegGetKeySecurity
.head 5 -  Description: function RegGetKeySecurity(
  hKey: HKEY;
  SecurityInformation: SECURITY_INFORMATION;
  pSecurityDescriptor: PSecurityDescriptor;
  var lpcbSecurityDescriptor: DWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: DWORD
.head 6 -  Receive String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 4 +  ! Function: RegOpenKeyA
.head 5 -  Description: function RegOpenKeyA(
  hKey: HKEY;
  lpSubKey: PAnsiChar;
  var phkResult: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPLONG
.head 4 +  ! Function: RegQueryInfoKeyA
.head 5 -  Description: function RegQueryInfoKeyA(
  hKey: HKEY;
  lpClass: PChar;
  lpcbClass: PDWORD;
  lpReserved: Pointer;
  lpcSubKeys,
  lpcbMaxSubKeyLen,
  lpcbMaxClassLen,
  lpcValues,
  lpcbMaxValueNameLen,
  lpcbMaxValueLen,
  lpcbSecurityDescriptor: PDWORD;
  lpftLastWriteTime: PFileTime): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 +  structPointer
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 4 +  ! Function: RegQueryValueA
.head 5 -  Description: function RegQueryValueA(
  hKey: HKEY;
  lpSubKey: PAnsiChar;
  lpValue: PAnsiChar;
  var lpcbValue: Longint): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPLONG
.head 4 +  ! Function: RegCreateKeyA
.head 5 -  Description: function RegCreateKeyA(
  hKey: HKEY;
  lpSubKey: PAnsiChar;
  var phkResult: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  Receive Number: LPLONG
.head 3 -  ! strci11.dll comes with CTD and can be found in the 
  centura samples directory (unused functions are commented 
  out)
.head 3 +  ! Library name: STRCI15.DLL
.head 4 +  Function: CStructGetByte
.head 5 -  Description: Extract a byte from a buffer.
nResult = CStructGetByte( strBuffer, nOffset )
.head 5 -  Export Ordinal: 1
.head 5 +  Returns 
.head 6 -  Number: BYTE
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetInt
.head 5 -  Description: Extract an integer from a buffer.
nResult = CStructGetInt( strBuffer, nOffset )
.head 5 -  Export Ordinal: 5
.head 5 +  Returns 
.head 6 -  Number: INT
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructGetWord
.head 5 -  Description: Extract a word from a buffer.
nResult = CStructGetWord( strBuffer, nOffset )
.head 5 -  Export Ordinal: 3
.head 5 +  Returns 
.head 6 -  Number: WORD
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructGetLong
.head 5 -  Description: Extract a long from a buffer.
nResult = CStructGetLong( strBuffer, nOffset )
.head 5 -  Export Ordinal: 5
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructGetFloat
.head 5 -  Description: Extract a float from a buffer.
nResult = CStructGetFloat( strBuffer, nOffset )
.head 5 -  Export Ordinal: 7
.head 5 +  Returns 
.head 6 -  Number: FLOAT
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetDouble
.head 5 -  Description: Extract a double from a buffer.
nResult = CStructGetDouble( strBuffer, nOffset )
.head 5 -  Export Ordinal: 9
.head 5 +  Returns 
.head 6 -  Number: DOUBLE
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructGetBFloat
.head 5 -  Description: Extract a BFloat from a buffer and convert to double.
nResult = CStructGetBFloat( strBuffer, nOffset )
.head 5 -  Export Ordinal: 21
.head 5 +  Returns 
.head 6 -  Number: DOUBLE
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetString
.head 5 -  Description: Extract a string from a buffer.
nLength = CStructGetString( strBuffer, nOffset, nMaxWidth, strExtract )
.head 5 -  Export Ordinal: 11
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 4 +  ! Function: CStructGetFarPointer
.head 5 -  Description: Extract a far pointer from a buffer.
nFarPointer = CStructGetFarPointer( strBuffer, nOffset )
.head 5 -  Export Ordinal: 17
.head 5 +  Returns 
.head 6 -  Number: ULONG
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructPutByte
.head 5 -  Description: Insert a byte into a buffer.
bOK = CStructPutByte( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 2
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: BYTE
.head 4 +  Function: CStructPutInt
.head 5 -  Description: Insert an integer into a buffer.
bOK = CStructPutInt( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 6
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: INT
.head 4 +  ! Function: CStructPutWord
.head 5 -  Description: Insert a word into a buffer.
bOK = CStructPutWord( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 4
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: WORD
.head 4 +  ! Function: CStructPutLong
.head 5 -  Description: Insert a long into a buffer.
bOK = CStructPutLong( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 6
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructPutFloat
.head 5 -  Description: Insert a byte into a buffer.
bOK = CStructPutFloat( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 8
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: FLOAT
.head 4 +  Function: CStructPutDouble
.head 5 -  Description: Insert a double into a buffer.
bOK = CStructPutDouble( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 10
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: DOUBLE
.head 4 +  ! Function: CStructPutBFloat
.head 5 -  Description: Convert double to BFloat and insert it into a buffer.
bOK = CStructPutBFloat( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 22
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: DOUBLE
.head 4 +  Function: CStructPutString
.head 5 -  Description: Insert a string into a buffer.
bOK = CStructPutString( strBuffer, nOffset, nMaxWidth, strInsert )
.head 5 -  Export Ordinal: 12
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 4 +  ! Function: CStructPutFarPointer
.head 5 -  Description: Insert a far pointer into a buffer.
bOK = CStructPutFarPointer( strBuffer, nOffset, nFarPointer )
.head 5 -  Export Ordinal: 18
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructCopyBuffer
.head 5 -  Description: Copy data from one String to another.
bOK = CStructCopyBuffer( strDest, nDestOffset,
                         strSrc, nSrcOffset, nCopyLen )
.head 5 -  Export Ordinal: 19
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructAllocFarMem
.head 5 -  Description: Allocate memory and return a far pointer.
nFarPointer = CStructAllocFarMem( nBytes )
.head 5 -  Export Ordinal: 13
.head 5 +  Returns 
.head 6 -  Number: ULONG
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructFreeFarMem
.head 5 -  Description: Free memory allocated by CStructAllocFarMem.
bOK = CStructFreeFarMem( nFarPointer )
.head 5 -  Export Ordinal: 14
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructCopyToFarMem
.head 5 -  Description: Copy data from far memory to a String.
bOK = CStructCopyToFarMem( nFarPointer, strData, nDataLen  )
.head 5 -  Export Ordinal: 15
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  ! Function: CStructCopyFromFarMem
.head 5 -  Description: Copy from a String to far memory.
bOK = CStructCopyFromFarMem( nFarPointer, strData, nMaxLen )
.head 5 -  Export Ordinal: 16
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 3 -  ! vti11.dll comes with CTD
  cBTRegistry needs only one function which it declares 
  with a unique name (so it doesn't conflict with vt.apl)
  you can substitute all calls to __VisStrSubstitute() with
  VisStrSubstitute(), if you decide to include vt.apl anyway
.head 3 +  Library name: shlwapi.dll
.head 4 -  ThreadSafe: No
.head 4 +  Function: SHDeleteKeyA
.head 5 -  Description: DWORD SHDeleteKey(    HKEY     hkey,    LPCTSTR  pszSubKey    );
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 4 +  Function: SHDeleteValueA
.head 5 -  Description: DWORD SHDeleteValue(HKEY hkey, LPCTSTR pszSubKey, LPCTSTR pszValue);
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  String: LPSTR
.head 4 +  Function: SHDeleteEmptyKeyA
.head 5 -  Description: DWORD SHDeleteEmptyKey(    HKEY     hkey,    LPCTSTR  pszSubKey    );
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 4 -  ! ----- Registry constants ...
.head 4 -  !
.head 4 -  ! HKEY_... are the frequently needed constants
.head 4 -  Number: HKEY_CLASSES_ROOT	= 0x80000000
.head 4 -  Number: HKEY_CURRENT_USER	= 0x80000001
.head 4 -  Number: HKEY_LOCAL_MACHINE	= 0x80000002
.head 4 -  Number: HKEY_USERS		= 0x80000003
.head 4 -  Number: HKEY_PERFORMANCE_DATA	= 0x80000004
.head 4 -  Number: HKEY_CURRENT_CONFIG	= 0x80000005
.head 4 -  Number: HKEY_DYN_DATA		= 0x80000006
.head 4 -  !
.head 4 -  ! internal:
.head 4 -  Number: STANDARD_RIGHTS_ALL	= 0x001F0000
.head 4 -  Number: STANDARD_RIGHTS_READ	= 0x00020000
.head 4 -  !
.head 4 -  Number: REG_OPTION_NON_VOLATILE	= (0x00000000)
.head 4 -  Number: REG_OPTION_VOLATILE	= (0x00000001)
.head 4 -  !
.head 4 -  Number: REG_NONE		= 0
.head 4 -  Number: REG_SZ			= 1
.head 4 -  Number: REG_EXPAND_SZ		= 2
.head 4 -  Number: REG_BINARY		= 3
.head 4 -  Number: REG_DWORD		= 4
.head 4 -  Number: REG_DWORD_LITTLE_ENDIAN	= 4
.head 4 -  Number: REG_DWORD_BIG_ENDIAN	= 5
.head 4 -  Number: REG_LINK		= 6
.head 4 -  Number: REG_MULTI_SZ		= 7
.head 4 -  Number: REG_RESOURCE_LIST	= 8
.head 4 -  Number: REG_FULL_RESOURCE_DESCRIPTOR = 9
.head 4 -  Number: REG_RESOURCE_REQUIREMENTS_LIST = 10
.head 4 -  !
.head 4 -  Number: KEY_QUERY_VALUE		= 0x0001
.head 4 -  Number: KEY_SET_VALUE		= 0x0002
.head 4 -  Number: KEY_CREATE_SUB_KEY	= 0x0004
.head 4 -  Number: KEY_ENUMERATE_SUB_KEYS	= 0x0008
.head 4 -  Number: KEY_NOTIFY		= 0x0010
.head 4 -  Number: KEY_CREATE_LINK		= 0x0020
.head 4 -  Number: KEY_ALL_ACCESS		= STANDARD_RIGHTS_ALL |
  KEY_QUERY_VALUE | KEY_SET_VALUE | KEY_CREATE_SUB_KEY |
  KEY_ENUMERATE_SUB_KEYS | KEY_NOTIFY | KEY_CREATE_LINK
.head 4 -  Number: KEY_ALL_READ		= STANDARD_RIGHTS_READ |
  KEY_QUERY_VALUE | KEY_ENUMERATE_SUB_KEYS | KEY_NOTIFY
.head 4 -  ! ------------------ end of registry constants
.head 3 +  User
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 -  Variables
.head 2 -  Internal Functions
.head 2 -  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 2201593326
0000: 92040000F0020000 0000000000000000 0200000500FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F13013800 0000FFFE00FF0F63
0040: 0044006500007300 6B0074006F008070 004C006900732274 42006F7800084E01
0060: 000B0000000F1900 000001010010CAA7 000082001D000700 1DA0106700000019
0080: 5900190001BBCA00 221D08001D10670B 0032000000D61900 01AECA001DC80900
00A0: 1D106782004B0000 0019B50001CA2B00 1D0A00B21D106700 60640000001900ED
00C0: 01CAAA001D0B1D2C 1067007D00580000 190001BBCA00221D 0C001D10670B0096
00E0: 000000D6190001AE CA001DC80D001D10 678200AF00000019 B50001CA2B001D0E
0100: 00B21D10670060C8 0000001900ED01CA 8A001D0F001D2C10 6700E10058000019
0120: 0001BBCA00661D00 1D670B00FA000000 D6190001AECA001D C811001D10674200
0140: 0180640000600100 0000FF0B2C630069 7200EE4C733F7C00 C00000040000005B
0160: 190001B910CA0028 2000072010676B00 1900D6190001AECA 0020CA082010679A
0180: 00320019B50001CA AB002009B2201067 00664B001900ED01 CAAA00200A202C10
01A0: 67000180044B0000 02000000F6FF0F8B 72540072EA65654C FE73035E00000003
01C0: 00BC000019A50001 00CA8B00230007B2 2310670066190019 00ED01CAAA002308
01E0: 23AC106700325900 190001BBCA002A23 092310670B000180 AF0099000300FF81
0200: 0C6300460069F86C 0065FE7383D60000 0007BC000019A500 0100CA0B00290006
0220: 00B2291067006619 001900ED01CAAA00 290729AC10670032 5900190001BBCA00
0240: 2A29082910676B00 4B00D6190001AECA 0029CA092910679A 00640019B50001CA
0260: AB00290AB2291067 00667D001900ED01 CAAA00290B29AC10 6700965900190001
0280: BBCA002A290C2910 670B0001807D0099 000400FF010D6300 52006100AA64696F
02A0: 4CFB730B789A0000 00F2050000961900 0100AECA004BCA06 4B10679A00190019
02C0: B50001CAAB004B07 B24B106700663200 1900ED01CAAA004B 084BAC1067004B58
02E0: 0000190001BBCA00 2A4B094B10672B00 640000D6190001AE CA004BCA0A4B1067
0300: 0200
.enddata
.head 3 +  Functional Class: cBTRegistry
.head 4 -  Description: This class encapsulates some of the windows registry api's.
You use the routines in the following way:

if OpenKey()
  ...
  Read-/Write-Functions
  ...
  CloseKey()
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  ! these are all strictly internal:
.head 5 -  Number: m_RootKey
.head 5 -  Number: m_CurrentKey
.head 5 -  Number: m_ErrorCode
.head 5 -  String: m_CurrentPath
.head 5 -  Boolean: m_LazyWrite
.head 4 +  Functions
.head 5 -  ! most used functions:
.head 5 +  Function: CloseKey
.head 6 -  Description: Call CloseKey()
Closes the key opened by OpenKey()
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  ! if any key was opened then close it
.head 7 +  If m_CurrentKey != 0
.head 8 -  ! Laze write means, the changes do not get saved immediately
.head 8 +  If not m_LazyWrite
.head 9 -  Call RegFlushKey( m_CurrentKey )
.head 8 -  Call RegCloseKey( m_CurrentKey )
.head 8 -  Set m_CurrentKey = 0
.head 8 -  Set m_CurrentPath = STRING_Null
.head 7 -  Set m_ErrorCode = 0
.head 5 +  Function: DeleteKey
.head 6 -  Description: Set bOk = DeleteKey( sKeyPath )
Deletes a key
sKeyPath	IN	path of the key to be deleted
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sKeyPath
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Relative
.head 6 +  Actions
.head 7 -  Set sKeyPath = VisStrSubstitute( sKeyPath, '/', '\\' )
.head 7 -  Set Relative = IsRelativeKey( sKeyPath )
.head 7 +  If not Relative
.head 8 -  Set sKeyPath = StrDelete( sKeyPath, 0, 1 )
.head 7 -  Return RegDeleteKeyW( GetBaseKey( Relative ), sKeyPath ) = 0
.head 5 +  Function: DeleteValue
.head 6 -  Description: Set bOk = DeleteValue( sKey )
Deletes a key's value
sKey	IN	key name from which to delete
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sKey
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Relative
.head 7 -  Number: DelKey
.head 6 +  Actions
.head 7 -  Return RegDeleteValueW( m_CurrentKey, sKey ) = 0
.head 5 +  Function: EnumKeys
.head 6 -  Description: bOk = EnumKeys( rsKeys[*] )
fills rsKeys with all the key names residing under the
currently open path
rsKeys[*]	OUT	array of key names
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Receive String: Arr [*]
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: Class
.head 7 -  Number: SubValues
.head 7 -  Number: MaxSubKeyLen
.head 7 -  Number: MaxClassLen
.head 7 -  Number: Values
.head 7 -  Number: MaxValueNameLen
.head 7 -  Number: MaxValueLen
.head 7 -  Number: SecurityDescriptor
.head 7 -  Number: LastWriteTime1
.head 7 -  Number: LastWriteTime2
.head 7 -  Number: n
.head 7 -  Number: len
.head 7 -  String: s
.head 6 +  Actions
.head 7 -  ! clear return values
.head 7 -  Call SalArraySetUpperBound( Arr, 1, -1 )
.head 7 -  ! retrieve the number of key names under the current path
.head 7 -  Set m_ErrorCode = RegQueryInfoKeyW( m_CurrentKey, 
STRING_Null, Class, STRING_Null, 
SubValues, MaxSubKeyLen, MaxClassLen, Values, MaxValueNameLen, 
MaxValueLen, SecurityDescriptor, LastWriteTime1, LastWriteTime2 )
.head 7 +  If m_ErrorCode = 0
.head 8 -  ! loop over each one
.head 8 -  Set n = 0
.head 8 +  While n < SubValues
.head 9 -  ! v1.1: Set len = MaxSubKeyLen
.head 9 -  ! v1.2: must have space for the trailing zero
.head 9 -  Set len = MaxSubKeyLen + 1
.head 9 -  ! read the name directly into the return array
.head 9 -  Call SalSetBufferLength( Arr [n], len )
.head 9 -  Call RegEnumKeyExW( m_CurrentKey, 
n, Arr [n], len, STRING_Null, STRING_Null, Class,
LastWriteTime1, LastWriteTime2 )
.head 9 -  Set n = n + 1
.head 7 -  Return (m_ErrorCode = 0)
.head 5 +  Function: EnumValues
.head 6 -  Description: bOk = EnumValues( rsValues[*] )
fills rsValues with all the value names under the
currently open path
rsValues[*]	OUT	array of value names
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Receive String: Arr [*]
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: Class
.head 7 -  Number: SubValues
.head 7 -  Number: MaxSubKeyLen
.head 7 -  Number: MaxClassLen
.head 7 -  Number: Values
.head 7 -  Number: MaxValueNameLen
.head 7 -  Number: MaxValueLen
.head 7 -  Number: SecurityDescriptor
.head 7 -  Number: LastWriteTime1
.head 7 -  Number: LastWriteTime2
.head 7 -  Number: n
.head 7 -  Number: len
.head 7 -  Number: dummy1
.head 7 -  Number: dummy2
.head 7 -  String: s
.head 6 +  Actions
.head 7 -  ! clear return values
.head 7 -  Call SalArraySetUpperBound( Arr, 1, -1 )
.head 7 -  ! retrieve the number of value names under the current path
.head 7 -  Set m_ErrorCode = RegQueryInfoKeyW( m_CurrentKey, 
STRING_Null, Class, STRING_Null, 
SubValues, MaxSubKeyLen, MaxClassLen, Values, MaxValueNameLen, 
MaxValueLen, SecurityDescriptor, LastWriteTime1, LastWriteTime2 )
.head 7 +  If m_ErrorCode = 0
.head 8 -  ! loop over each one
.head 8 -  Set n = 0
.head 8 +  While n < Values
.head 9 -  ! v1.1: Set len = MaxSubKeyLen
.head 9 -  ! v1.2: must have space for the trailing zero
.head 9 -  Set len = MaxValueNameLen + 1
.head 9 -  ! read the name directly into the return array
.head 9 -  Call SalSetBufferLength( Arr [n], len )
.head 9 -  Call RegEnumValueW( m_CurrentKey, 
n, Arr [n], len, STRING_Null, dummy1, STRING_Null, dummy2 )
.head 9 -  Set n = n + 1
.head 7 -  Return (m_ErrorCode = 0)
.head 5 +  Function: FlushKey
.head 6 -  Description: Saves all changes immediately
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_ErrorCode = RegFlushKey( m_CurrentKey )
.head 7 -  Return ( m_ErrorCode = 0 )
.head 5 +  Function: KeyExists
.head 6 -  Description: Set bExists = KeyExists( sPath )
Checks, if a registry path exists
sPath	IN	the key (ex: "/Software/Centura")
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: TempKey
.head 6 +  Actions
.head 7 -  ! try to open the key to see if it exists
.head 7 -  Set TempKey = GetKey(Name)
.head 7 -  ! if it exists, then close it right away
.head 7 +  If TempKey != 0
.head 8 -  Call RegCloseKey( TempKey )
.head 7 -  ! return the result of the open key function
.head 7 -  Return TempKey != 0
.head 5 +  Function: OpenKey
.head 6 -  Description: Set bOk = OpenKey( sPath, bCanCreate )
sPath		Registry-Path (e.g. "/Software/Centura")
bCanCreate	set to TRUE, if Path shall be created
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Key
.head 7 -  Boolean: CanCreate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Relative
.head 7 -  Number: TempKey
.head 7 -  Number: Disposition
.head 6 +  Actions
.head 7 -  ! all forward slashes must be substituted with backslashes
.head 7 -  Set Key = VisStrSubstitute( Key, '/', '\\' )
.head 7 -  ! is it an absolute or relative path?
.head 7 -  Set Relative = IsRelativeKey( Key )
.head 7 +  If not Relative
.head 8 -  Set Key = StrDelete( Key, 0, 1 )
.head 7 -  ! Create the key, if it doesn't exist?
.head 7 +  If not CanCreate or (Key = STRING_Null)
.head 8 -  ! do not create, if not existing
.head 8 -  Set m_ErrorCode = RegOpenKeyExW( GetBaseKey( Relative ),
Key, 0, 0x000E000F, TempKey )
.head 8 +  If m_ErrorCode != 0
.head 9 -  ! maybe, you don't have enough rights so try again in read/only mode
.head 9 -  Set m_ErrorCode = RegOpenKeyExW( GetBaseKey( Relative ),
Key, 0, KEY_ALL_READ, TempKey )
.head 7 +  Else
.head 8 -  ! create, if not existing
.head 8 -  Set m_ErrorCode = RegCreateKeyExW( GetBaseKey(Relative),
  Key, 0, STRING_Null, REG_OPTION_NON_VOLATILE,
  0x000E000F, STRING_Null, TempKey, Disposition )
.head 7 -  ! if ok, then change internal settings
.head 7 +  If m_ErrorCode = 0
.head 8 +  If (m_CurrentKey != 0) and Relative
.head 9 -  Set Key = m_CurrentPath || '\\' || Key
.head 8 -  Call ChangeKey( TempKey, Key )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: SetRootKey
.head 6 -  Description: Set bOk = SetRootKey( nKey )
sets the root for subsequent operations.
see also: HKEY_xxxx constants
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: RootKey
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If RootKey = m_RootKey
.head 8 -  Return TRUE
.head 7 -  Call CloseKey()
.head 7 -  Set m_RootKey = RootKey
.head 5 +  Function: ValueExists
.head 6 -  Description: Set bExists = ValueExists( sValueName )
Checks, if a value exists under the current path
sValueName	IN	name of the value to be checked
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: DataSize
.head 7 -  Number: DataType
.head 7 -  String: buf
.head 6 +  Actions
.head 7 -  Call SalSetBufferLength( buf, 4 )
.head 7 -  Set DataSize = 0
.head 7 -  Set m_ErrorCode = RegQueryValueExW(
m_CurrentKey, Name, STRING_Null, DataType, buf, DataSize )
.head 7 -  Return m_ErrorCode = 0
.head 5 -  ! read...
.head 5 +  Function: ReadBinary
.head 6 -  Description: Set bOk = ReadBinary( sValueName, rsValue, rnValueSize )
sValueName	IN	Name of the Entry
rsValue		OUT	the value (see CStruct-functions)
rnValueSize	IN	size that rsValue can hold
		OUT	number of bytes retrieved
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Receive String: Value
.head 7 -  Receive Number: ValueSize
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: DataType
.head 6 +  Actions
.head 7 -  Call SalSetBufferLength( Value, 1 )
.head 7 -  Set ValueSize = 0
.head 7 -  Set m_ErrorCode = RegQueryValueExW(
m_CurrentKey, Name, STRING_Null, DataType, Value, ValueSize )
.head 7 -  ! Error 234 means, there is more data that doesn't fit into buf
.head 7 +  If (m_ErrorCode = 0) or (m_ErrorCode = 234)
.head 8 -  Call SalSetBufferLength( Value, ValueSize )
.head 8 -  Set m_ErrorCode = RegQueryValueExW( m_CurrentKey, Name, 
STRING_Null, DataType, Value, ValueSize )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: ReadBool
.head 6 -  Description: Set bOk = ReadBool( sValueName, rbValue )
sValueName	IN	name of the entry
rbValue		OUT	retrieved boolean value
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Receive Boolean: Value
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return ReadInteger( Name, Value )
.head 5 +  Function: ReadFloat
.head 6 -  Description: Set bOk = ReadFloat( sValueName, rnValue )
sValueName	IN	name of the entry
rnValue		OUT	retrieved value
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Receive Number: FloatValue
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: DataType
.head 7 -  Number: ValueSize
.head 7 -  String: Value
.head 6 +  Actions
.head 7 -  Set ValueSize = 8
.head 7 -  Call SalSetBufferLength( Value, ValueSize )
.head 7 -  Set m_ErrorCode = RegQueryValueExW( m_CurrentKey, Name, 
STRING_Null, DataType, Value, ValueSize )
.head 7 -  Set FloatValue = CStructGetDouble( Value, 0 )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: ReadInteger
.head 6 -  Description: Set bOk = ReadInteger( sValueName, rnValue )
sValueName	IN	name of the entry
rnValue		OUT	retrieved value
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Receive Number: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: DataSize
.head 7 -  Number: DataType
.head 7 -  String: buf
.head 6 +  Actions
.head 7 -  Call SalSetBufferLength( buf, 4 )
.head 7 -  Set DataSize = 4
.head 7 -  Set m_ErrorCode = RegQueryValueExW(
m_CurrentKey, Name, STRING_Null, DataType, buf, DataSize )
.head 7 +  If m_ErrorCode = 0
.head 8 -  Set Value = CStructGetInt( buf, 0 )
.head 8 -  Return TRUE
.head 7 -  Return FALSE
.head 5 +  Function: ReadString
.head 6 -  Description: Set bOk = ReadString( sValueName, rsValue )
sValueName	IN	name of the entry
rsValue		OUT	retrieved value
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Receive String: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: DataSize
.head 7 -  Number: DataType
.head 7 -  String: buf
.head 6 +  Actions
.head 7 -  Call SalSetBufferLength( buf, 1 )
.head 7 -  Set DataSize = 0
.head 7 -  Set m_ErrorCode = RegQueryValueExW(
m_CurrentKey, Name, STRING_Null, DataType, buf, DataSize )
.head 7 -  ! Error 234 means, there is more data that doesn't fit into buf
.head 7 +  If (m_ErrorCode = 0) or (m_ErrorCode = 234)
.head 8 -  Call SalSetBufferLength( buf, DataSize )
.head 8 -  Set m_ErrorCode = RegQueryValueExW( m_CurrentKey, Name, 
STRING_Null, DataType, buf, DataSize )
.head 8 +  If m_ErrorCode = 0
.head 9 -  Set Value = buf
.head 9 -  Return TRUE
.head 7 -  Return FALSE
.head 5 -  ! write ...
.head 5 +  Function: WriteBinary
.head 6 -  Description: Set bOk = WriteBinary( sValueName, sValue, nSize )
sValueName	IN	name of the entry
sValue		IN	value to write
nSize		IN	size of value
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  String: Value
.head 7 -  Number: ValueSize
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_ErrorCode = RegSetValueExW( m_CurrentKey, Name, 0, REG_BINARY,
Value, ValueSize )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: WriteBool
.head 6 -  Description: Set bOk = WriteBool( sValueName, bValue )
sValueName	IN	name of the entry
bValue		IN	value to write
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Boolean: Value
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return WriteInteger( Name, Value )
.head 5 +  Function: WriteFloat
.head 6 -  Description: Set bOk = WriteFloat( sValueName, nValue )
sValueName	IN	name of the entry
nValue		IN	value to write
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Number: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: buf
.head 6 +  Actions
.head 7 -  Call SalSetBufferLength( buf, 8 )
.head 7 -  Call CStructPutDouble( buf, 0, Value )
.head 7 -  Set m_ErrorCode = RegSetValueExW( m_CurrentKey, Name, 0, REG_BINARY, buf, 8 )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: WriteInteger
.head 6 -  Description: Set bOk = WriteInteger( sValueName, nValue )
sValueName	IN	name of the entry
nValue		IN	value to write
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  Number: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: buf
.head 6 +  Actions
.head 7 -  Call SalSetBufferLength( buf, 4 )
.head 7 -  Call CStructPutInt( buf, 0, Value )
.head 7 -  Set m_ErrorCode = RegSetValueExW( m_CurrentKey, Name, 0, REG_DWORD,
buf, 4 )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: WriteString
.head 6 -  Description: Set bOk = WriteString( sValueName, sValue )
sValueName	IN	name of the entry
sValue		IN	value to write
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Name
.head 7 -  String: Value
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_ErrorCode = RegSetValueExW( m_CurrentKey, Name, 0, REG_SZ,
Value, SalStrLength( Value ) + 1 )
.head 7 -  Return m_ErrorCode = 0
.head 5 -  ! shortcuts to read/write-functions...
.head 5 +  Function: ReadStringAt
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: KeyPath
.head 7 -  String: ValueName
.head 7 -  Receive String: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Ok
.head 6 +  Actions
.head 7 +  If OpenKey( KeyPath, FALSE )
.head 8 -  Set Ok = ReadString( ValueName, Value )
.head 8 -  Call CloseKey()
.head 7 -  Return Ok
.head 5 +  Function: ReadIntegerAt
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: KeyPath
.head 7 -  String: ValueName
.head 7 -  Receive Number: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Ok
.head 6 +  Actions
.head 7 +  If OpenKey( KeyPath, FALSE )
.head 8 -  Set Ok = ReadInteger( ValueName, Value )
.head 8 -  Call CloseKey()
.head 7 -  Return Ok
.head 5 +  Function: WriteStringAt
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: KeyPath
.head 7 -  Boolean: CanCreate
.head 7 -  String: ValueName
.head 7 -  String: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Ok
.head 6 +  Actions
.head 7 +  If OpenKey( KeyPath, CanCreate )
.head 8 -  Set Ok = WriteString( ValueName, Value )
.head 8 -  Call CloseKey()
.head 7 -  Return Ok
.head 5 +  Function: WriteIntegerAt
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: KeyPath
.head 7 -  Boolean: CanCreate
.head 7 -  String: ValueName
.head 7 -  Number: Value
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Ok
.head 6 +  Actions
.head 7 +  If OpenKey( KeyPath, CanCreate )
.head 8 -  Set Ok = WriteInteger( ValueName, Value )
.head 8 -  Call CloseKey()
.head 7 -  Return Ok
.head 5 -  ! others:
.head 5 +  Function: ConnectRemote
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: UNCName
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: Temp
.head 6 +  Actions
.head 7 -  Set m_ErrorCode = RegConnectRegistryW( UNCName, m_RootKey, Temp )
.head 7 +  If m_ErrorCode = 0
.head 8 -  Set m_RootKey = Temp
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: CreateKey
.head 6 -  Description: Set bOk = CreateKey( sKey )
Creates a new key
sKey	IN	name of the key path to be created
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: Key
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Relative
.head 7 -  Number: TempKey
.head 7 -  Number: Disposition
.head 6 +  Actions
.head 7 -  Set Key = VisStrSubstitute( Key, '/', '\\' )
.head 7 -  Set Relative = IsRelativeKey( Key )
.head 7 +  If not Relative
.head 8 -  Set Key = StrDelete( Key, 0, 1 )
.head 7 -  Set m_ErrorCode = RegCreateKeyExW( GetBaseKey(Relative),
  Key, 0, STRING_Null, REG_OPTION_NON_VOLATILE,
  0x000E000F, STRING_Null, TempKey, Disposition )
.head 7 +  If m_ErrorCode = 0
.head 8 -  Call RegCloseKey( TempKey )
.head 7 -  Return m_ErrorCode = 0
.head 5 +  Function: GetBaseKey
.head 6 -  Description: Set nKey = GetBaseKey()
Retrieves the base key handle
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  Boolean: Relative
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If (m_CurrentKey = 0) or not Relative
.head 8 -  Return GetRootKey()
.head 7 -  Return m_CurrentKey
.head 5 +  Function: GetCurrentPath
.head 6 -  Description: Set nKey = GetCurrentPath()
Retrieves the current key handle
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_CurrentPath
.head 5 +  Function: GetLastError
.head 6 -  Description: Set nErrCode = GetLastError()
retrieves the return value of the most
recently executed registry api
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_ErrorCode
.head 5 +  Function: GetRootKey
.head 6 -  Description: Set nRootKey = GetRootKey()
retrieves the number of the root key
see also: HKEY_xxxx constants
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If m_RootKey = 0
.head 8 -  Return HKEY_CURRENT_USER
.head 7 -  Return m_RootKey
.head 5 +  Function: HasSubValues
.head 6 -  Description: Set bHasSubValues = HasSubValues()
return TRUE, if the currently opened key has any values
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: Class
.head 7 -  Number: SubValues
.head 7 -  Number: MaxSubKeyLen
.head 7 -  Number: MaxClassLen
.head 7 -  Number: Values
.head 7 -  Number: MaxValueNameLen
.head 7 -  Number: MaxValueLen
.head 7 -  Number: SecurityDescriptor
.head 7 -  Number: LastWriteTime1
.head 7 -  Number: LastWriteTime2
.head 6 +  Actions
.head 7 -  Set m_ErrorCode = RegQueryInfoKeyW( m_CurrentKey, 
STRING_Null, Class, STRING_Null, 
SubValues, MaxSubKeyLen, MaxClassLen, Values, MaxValueNameLen, 
MaxValueLen, SecurityDescriptor, LastWriteTime1, LastWriteTime2 )
.head 7 +  If m_ErrorCode = 0
.head 8 -  Return (SubValues > 0)
.head 7 -  Return (m_ErrorCode = 0)
.head 5 +  Function: SetLazyWrite
.head 6 -  Description: Call SetLazyWrite( bSwitchOn )
Set the behaviour of CloseKey(). If set to FALSE,
all changed get written back to disk immediately 
as soon as CloseKey() is called
bSwitchOn	IN	TRUE = use RegCloseKey
			FALSE = use RegFlushKey
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: OnOff
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_LazyWrite = OnOff
.head 5 -  ! internals, do not use:
.head 5 +  Function: ChangeKey
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: hKey
.head 7 -  String: NewPath
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call CloseKey()
.head 7 -  Set m_CurrentKey = hKey
.head 7 -  Set m_CurrentPath = NewPath
.head 5 +  Function: IsRelativeKey
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: Value
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If Value != STRING_Null
.head 8 +  If SalStrLeftX( Value, 1 ) != '\\'
.head 9 -  Return TRUE
.head 7 -  Return FALSE
.head 5 +  Function: GetKey
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: Key
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: Relative
.head 7 -  Number: TempKey
.head 7 -  Number: Disposition
.head 6 +  Actions
.head 7 -  Set Key = VisStrSubstitute( Key, '/', '\\' )
.head 7 -  Set Relative = IsRelativeKey( Key )
.head 7 +  If not Relative
.head 8 -  Set Key = StrDelete( Key, 0, 1 )
.head 7 -  Set m_ErrorCode = RegOpenKeyExW( GetBaseKey( Relative ),
Key, 0, KEY_ALL_ACCESS, TempKey )
.head 7 +  If m_ErrorCode != 0
.head 8 -  Set m_ErrorCode = RegOpenKeyExW( GetBaseKey( Relative ),
Key, 0, KEY_ALL_READ, TempKey )
.head 8 +  If m_ErrorCode != 0
.head 9 -  Return 0
.head 7 -  Return TempKey
.head 5 +  Function: StrDelete
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: Source
.head 7 -  Number: At		! beginnend mit 0
.head 7 -  Number: NumChars
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: Len
.head 7 -  String: SL
.head 7 -  String: SR
.head 6 +  Actions
.head 7 -  Set Len = SalStrLength( Source )
.head 7 +  If At >= Len
.head 8 -  Return Source
.head 7 -  Set SL = SalStrLeftX( Source, At )
.head 7 -  Set SR = SalStrRightX( Source, Len - At - NumChars )
.head 7 -  Return SL || SR
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box: cOutlineListBox
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 2 -  Application Actions
.head 1 +  ! Version history
.head 2 -  ! v1.1 19980907 Joe Meyer
	fixed unsufficient rights bug under NT4
.head 2 -  ! v1.2 19981014 Joe Meyer (reported by Joerg Ellinghaus)
	fixed invalid buffer length bug in EnumValues(), EnumKeys()
.head 2 -  ! v1.3 19990923 Joe Meyer (reported by Charles McLouth, Centura)
	fixed handle leak in CloseKey when used with LazyWrite=false
	included FlushKey()
	included RegLoadKey(), RegSaveKey(), RegRestoreKey
	included shlwapi.dll declarations
.head 2 -  ! v1.4 20000119 Joe Meyer (reported by Martin Jussel)
	fixed CreateKey()
