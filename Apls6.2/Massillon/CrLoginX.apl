.head 0 +  Application Description: Criminal Software Integrated Library
3-20-2001 - WRP
   * Includes 	-Login Screen
		-Registry checking( for Public/Non-Public Access)
		-Classes for formating of (Caseyr, caseno, casety, dates)
		-Retains last user to login
		-Selects User information from database
		-Selects Court information from database
		-8-2-2001-WRP-Added "/" ability to cDate class, allows the backslash to be entered to display todays date.
		-9-6-2001-WRP-Added Holiday Date Class to check for holiday and weekends
3-31-2008 - WRP
		-Consolidated functions into "Function Classes"
		-Changed login screen
.head 1 -  Outline Version - 4.0.50
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000040100 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE1FFFFFF2C0000 002C0000002F0200 0066010000010000 0001000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0000000000
.enddata
.data DT_MAKERUNDLG
0000: 020000000033433A 5C43656E74757261 5C4372696D333220 2D204F5241434C45
0020: 5C4372696D4E6577 2D4D6173735C4372 4C6F67696E2E6578 6533433A5C43656E
0040: 747572615C437269 6D3332202D204F52 41434C455C437269 6D4E65772D4D6173
0060: 735C43724C6F6769 6E2E646C6C33433A 5C43656E74757261 5C4372696D333220
0080: 2D204F5241434C45 5C4372696D4E6577 2D4D6173735C4372 4C6F67696E2E6170
00A0: 6300000101013343 3A5C43656E747572 615C4372696D3332 202D204F5241434C
00C0: 455C4372696D4E65 772D4D6173735C43 724C6F67696E2E72 756E33433A5C4365
00E0: 6E747572615C4372 696D3332202D204F 5241434C455C4372 696D4E65772D4D61
0100: 73735C43724C6F67 696E2E646C6C3343 3A5C43656E747572 615C4372696D3332
0120: 202D204F5241434C 455C4372696D4E65 772D4D6173735C43 724C6F67696E2E61
0140: 7063000001010133 433A5C43656E7475 72615C4372696D33 32202D204F524143
0160: 4C455C4372696D4E 65772D4D6173735C 43724C6F67696E2E 61706433433A5C43
0180: 656E747572615C43 72696D3332202D20 4F5241434C455C43 72696D4E65772D4D
01A0: 6173735C43724C6F 67696E2E646C6C33 433A5C43656E7475 72615C4372696D33
01C0: 32202D204F524143 4C455C4372696D4E 65772D4D6173735C 43724C6F67696E2E
01E0: 6170630000010101 33433A5C43656E74 7572615C4372696D 3332202D204F5241
0200: 434C455C4372696D 4E65772D4D617373 5C43724C6F67696E 2E61706C33433A5C
0220: 43656E747572615C 4372696D3332202D 204F5241434C455C 4372696D4E65772D
0240: 4D6173735C43724C 6F67696E2E646C6C 33433A5C43656E74 7572615C4372696D
0260: 3332202D204F5241 434C455C4372696D 4E65772D4D617373 5C43724C6F67696E
0280: 2E61706300000101 01
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040002001B00 0200000000000000 00003816A00F0500 1D00FFFF4D61696E
0020: 0020000100040000 0000000000F51E81 0F0000DF00FFFF56 61726961626C6573
0040: 0029000100040000 0000000000F51E81 0F00008600FFFF49 6E7465726E616C20
0060: 46756E6374696F6E 73001E0001000400 000000000000F51E 810F0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: 0.01"
.head 3 -  Top: 0.133"
.head 3 -  Width:  7.96"
.head 3 -  Height: 4.425"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C4389105B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 8.325"
.head 3 -  Top: 0.135"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? Yes
.head 2 -  Assembly Symbol File:
.head 1 +  Libraries
.head 2 -  ! File Include: Registry.apl
.head 2 -  File Include: vtcal.apl
.head 2 -  ! File Include: qcktabs.apl
.head 2 -  File Include: vttblwin.apl
.head 2 -  File Include: vtfile.apl
.head 2 -  File Include: vtdos.apl
.head 2 -  File Include: mtbl.apl
.head 2 -  File Include: mimg.apl
.head 2 -  File Include: vtstr.apl
.head 2 -  File Include: CertMail.apl
.head 2 -  File Include: qcktabs.apl
.head 2 -  File Include: RegistryW.apl
.head 2 -  File Include: PrintScreen.apl
.head 2 -  File Include: Gtablex.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Line Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 3 +  Tree Control
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Navigation Bar
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 -  Pane Separator
.head 3 +  Tab Bar
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Progress Bar
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MM-dd-yyyy
.head 2 +  External Functions
.head 3 +  Library name: KERNEL32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: AddAtomA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 4 -  !
.head 4 +  Function: Beep
.head 5 -  Description: Used to generate simple sounds
bOk = Beep( nFrequency, nDuration )
Note:	nFrequency:	37 to 323767 hertz. ( N/A in Window 95 )
	nDuration:	In milliseconds  ( N/A in Window 95 )
			-1 to play until function called again
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 -  !
.head 4 +  ! Function: CloseHandle
.head 5 -  Description: The CloseHandle function closes an open object handle.

BOOL CloseHandle(
	HANDLE hObject 	// handle to object to close
	);
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: HANDLE
.head 4 +  Function: ConvertDefaultLocale
.head 5 -  Description: Converts one of the special locale identifiers to a true locale ID.
nActualLocaleID = ConvertDefaultLocale( nLocale )
Note:	nLocale:		LOCALE_*
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: CopyFileA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  Boolean: BOOL
.head 4 +  ! Function: CreateFileA
.head 5 -  Description: 
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  File Handle: HFILE
.head 5 +  Parameters 
.head 6 -  String: LPCSTR
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Window Handle: HWND
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Window Handle: HWND
.head 4 +  Function: CreateMutexA
.head 5 -  Description: The CreateMutex function creates a named or unnamed mutex object.
HANDLE CreateMutex(
    LPSECURITY_ATTRIBUTES lpMutexAttributes,	// pointer to security attributes
    BOOL bInitialOwner,	// flag for initial ownership
    LPCTSTR lpName 	// pointer to mutex-object name
   );
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 +  Number: LPVOID
.head 7 -  ! Security Attributes (always pass 0)
.head 6 -  Boolean: BOOL
.head 6 -  String: LPSTR
.head 4 -  !
.head 4 +  Function: DeleteAtom
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: INT
.head 4 +  Function: DeleteFileA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 4 -  !
.head 4 +  Function: EnumCalendarInfoA
.head 5 -  Description: Enumerates information about the calendars under a given locale
nResult = EnumCalendarInfo(  nAddressOfCalandarFunction, nLocale, nCalandar, nCalandarType )
Note:	nAddressOfCalandarFunction:		Pointer to a function to call for each calandar
					Use the AddressOf operator to obtain the address of a function in a standard module
					Use the ProcAddress property of dwcbkd32.ocx to obtain a function pointer for callbacks
	Calandar:				ENUM_ALL_CALENDARS for all
					1 for localized gregorian
					2 for English Gregorian
					3 for Japanese Era
					4 for Chinese
					5 for Korean
	CalType:				CAL_*
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: EnumDateFormatsA
.head 5 -  Description: Enumerates the short and long date formats available under a given locale
nResult = EnumDateFormats(  nAddressOfDateFunction, nLocale, nFlags )
Note:	nAddressOfDateFunction:		Pointer to a function to call for each calandar
					Use the AddressOf operator to obtain the address of a function in a standard module
					Use the ProcAddress property of dwcbkd32.ocx to obtain a function pointer for callbacks
	Flags:				DATE_SHORTDATE orDATE_LONGDATE
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: EnumSystemCodePagesA
.head 5 -  Description: Enumerates the code pages that are installed or supported by the system
nResult = EnumSystemCodePages(  nAddressOfCodePageFunction, nLocale, nFlags )
Note:	nAddressOfCodePageFunction:		Pointer to a function to call for each calandar
					Use the AddressOf operator to obtain the address of a function in a standard module
					Use the ProcAddress property of dwcbkd32.ocx to obtain a function pointer for callbacks
	Flags:				CP_INSTALLED or CP_SUPPORTED
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: EnumSystemLocalesA
.head 5 -  Description: Enumerates the locales that are installed or supported by the system
nResult = EnumSystemLocales(  nAddressOfLocalesFunction,  nFlags )
Note:	nAddressOfLocalesFunction:	Pointer to a function to call for each calandar
					Use the AddressOf operator to obtain the address of a function in a standard module
					Use the ProcAddress property of dwcbkd32.ocx to obtain a function pointer for callbacks
	Flags:				LCID_INSTALLED or LCID_SUPPORTED
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Number: LONG
.head 4 +  Function: EnumTimeFormatsA
.head 5 -  Description: Enumerates the locales that are installed or supported by the system
nResult = EnumTimeFormats(  nAddressOfTimeFormatsFunction,  nLocale, nFlags )
Note:	nAddressOfTimeFormatsFunction:	Pointer to a function to call for each calandar
					Use the AddressOf operator to obtain the address of a function in a standard module
					Use the ProcAddress property of dwcbkd32.ocx to obtain a function pointer for callbacks
	Flags:				Set to zero
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: ExitProcess
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 -  Number: UINT
.head 4 +  Function: ExpandEnvironmentStringsA
.head 5 -  Description: Expand environment strings, converting variables in the string into values.( e.g "%path%" is expanded to full path )
nSizeOfBufferRequired = ExpandEnvironmentStringsA( sSource, sDestination, nBufferSize )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 -  !
.head 4 +  Function: FindClose
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: FindFirstFileA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  Receive String: LPVOID
.head 4 +  Function: FindNextFileA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 +  structPointer
.head 7 -  Receive Number: DWORD
.head 7 +  struct
.head 8 -  Receive Number: DWORD
.head 8 -  Receive Number: DWORD
.head 7 +  struct
.head 8 -  Receive Number: DWORD
.head 8 -  Receive Number: DWORD
.head 7 +  struct
.head 8 -  Receive Number: DWORD
.head 8 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive String: LPSTR
.head 7 -  Receive String: char[14]
.head 4 +  Function: FreeEnvironmentStringsA
.head 5 -  Description: Free the specified environment block.
bOk = FreeEnvironmentStringsA( sSource)
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 4 +  Function: FreeLibrary
.head 5 -  Description: The FreeLibrary function decrements the reference count of the loaded dynamic-link library (DLL) module.
When the reference count reaches zero, the module is unmapped from the address space of the calling process and the handle is no longer valid.

Parameters:
	hLibModule 		Identifies the loaded library module. The LoadLibrary or GetModuleHandle function returns this handle.

Return Values:
	If the function succeeds, the return value is nonzero.
	If the function fails, the return value is zero.

Remarks:
Each process maintains a reference count for each loaded library module. This reference count is incremented each time LoadLibrary is called and is decremented each time FreeLibrary is called.
A DLL module loaded at process initialization due to load-time dynamic linking has a reference count of one. This count is incremented if the same module is loaded by a call to LoadLibrary.

Before unmapping a library module, the system enables the DLL to detach from the process by calling the DLL’s DllEntryPoint function, if it has one, with the DLL_PROCESS_DETACH value.
Doing so gives the DLL an opportunity to clean up resources allocated on behalf of the current process. After the entry-point function returns, the library module is removed from the address space of the current process.

Calling FreeLibrary does not affect other processes using the same library module.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 4 -  !
.head 4 +  Function: GetACP
.head 5 -  Description: Determines the ANSI code page that is currently in effect
nANSICodePage = GetACP( )
Note:		nANSICodePage:		874:	Thai
					932:	Japanese
					936:	Chinese
					949:	Korean
					950:	Chinese( Taiwan & Hong Kong )
					1200:	Unicode
					1250:	Eastern European
					1251:	Cyrillic
					1252:	US & Western Europe
					1253:	Greek
					1254:	Turkish
					1255:	Hebrew
					1256:	Arabic
					1257:	Baltic
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 -  Parameters
.head 4 +  Function: GetCommandLineA
.head 5 -  Description: Obtains a pointer to the current command line buffer
nAddressInMemory = GetCommandLine()
Note:	Use agGetStringFromPointer in apigid32.dll to retrieve the command line into a string
	i.e agGetStringFromPointer( GetCommandLine() )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 -  Parameters
.head 4 +  Function: GetComputerNameA
.head 5 -  Description: Retrieves the name of this computer
bOk  = GetCommandLine(sAddressInMemory, nBufferSizeCreated)
Note:	Set nBufferSizeCreated to MAX_COMPUTERNAME_LENGTH + 1 before calling this function
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Receive Number: LPLONG
.head 4 +  Function: GetCPInfo
.head 5 -  Description: Retrieves information about the specified code page
bOk  = GetCPInfo (nCodePage, uCPINFO)
Note:	nCodePage:	either ANSI or OEM code page allowed
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 +  structPointer
.head 7 -  Number: LONG
.head 7 -  Number: BYTE
.head 7 -  Number: BYTE
.head 4 +  Function: GetCurrencyFormatA
.head 5 -  Description: Formats a number according to the currency format for the locale specified
nReturn = GetCurrencyFormatA( nLocaleID, nFlags, sNumberToFormat, nCurrencyFormat, sResult, sResultBufferSize )
Note:	nFlags:		zero if nCurrencyFormat specified
			LOCALE_NOUSEROVERRIDE to force system locale to be used even if overriden by a user
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 +  Function: GetCurrentDirectoryA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 6 -  Receive String: LPSTR
.head 4 +  Function: GetCurrentProcess
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 -  Parameters
.head 4 +  Function: GetCurrentProcessId
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 -  Parameters
.head 4 +  Function: GetCurrentThreadId
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 -  Parameters
.head 4 +  Function: GetDateFormatA
.head 5 -  Description: Formats a system date according to the format for the locale specified
nReturn = GetDateFormatA( nLocaleID, nFlags, sDaterToFormat, nDateFormat, sResult, sResultBufferSize )
Note:	nFlags:		zero if sDaterToFormat specified
			LOCALE_NOUSEROVERRIDE to force system locale to be used even if overriden by a user
			use DATE_SHORTDATE or DATE_LONGDATE to choose between date formats
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 +  Function: GetDiskFreeSpaceA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive Number: LPDWORD
.head 4 +  Function: GetDriveTypeA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: UINT
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 4 +  Function: GetEnvironmentStringsA
.head 5 -  Description: Allocates and returns a handle to a block of memory containing all the current environment string settings, seperated by NULLs and
 two consecutive NULLs indicating the end of the list
nMemoryAddress = GetEnvironmentStringsA( )
Be sure to release this block of memory using FreeEnvironmentStringsA function
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 -  Parameters
.head 4 +  Function: GetEnvironmentVariableA
.head 5 -  Description: Retrieves the value of an environment variable
nBufferLength = GetEnvironmentVariableA( sVariableName, sBuffer, nBufferSize )
Be sure to release this block of memory using FreeEnvironmentStringsA function
Superceded GetDosEnvironment
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 +  Function: GetExitCodeProcess
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Receive Number: LPLONG
.head 4 +  Function: GetFileAttributesA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 4 +  Function: GetFullPathNameA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  Number: DWORD
.head 6 -  Receive String: LPSTR
.head 6 -  Receive String: LPSTR
.head 4 +  ! Function: GetLastError
.head 5 -  Description: Obtains the last error for a previously called API function
nError = GetLastError (  )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Number: DWORD
.head 5 -  Parameters 
.head 4 +  Function: GetLocaleInfoA
.head 5 -  Description: Retrieves information for the speciifed locale
nResult = GetLocalesInfoA (  nLocaleID, nInformationType, sResult,  nBufferLength )
Note:	nInformationType:		LOCALE_*
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 +  Function: GetLocalTime
.head 5 -  Description: Loads uSYSTEMTIME witht the local date and time
bOk = GetLocalTime (  uSYSTEMTIME )
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 4 +  Function: GetModuleFileNameA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: WORD
.head 4 +  Function: GetModuleHandleA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 4 +  Function: GetNumberFormatA
.head 5 -  Description: Formats a number format according to the format for the locale specified
nReturn = GetNumberFormatA( nLocaleID, nFlags, sNumberToFormat, nFormat, sResult, sResultBufferSize )
Note:	nFlags:		zero if sNumberToFormat specified
			LOCALE_NOUSEROVERRIDE to force system locale to be used even if overriden by a user
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 +  Function: GetOEMCP
.head 5 -  Description: Determines the windows code page used to translate betwen the OEM and ANSI character sets
nActiveOEMCodePage = GetOEMCP()
Note:		nActiveOEMCodePage:		437:		Default: United States
						708-720:		Arabic
						737:		Greek
						775:		Baltic
						850:		International
						852:		Slavic
						855:		Cyrillic
						857:		Turkish
						860:		Portuguese
						861:		Icelandic
						862:		Hebrew
						863:		French Canadian
						864:		Arabic
						865:		Norway/Denmark
						866:		Russian
						874:		Thai
						932:		Japanese
						936:		Chinese
						949:		Korean
						950:		Chinese ( Taiwan & Hong Kong )
						1361:		Korean
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 -  Parameters
.head 4 +  Function: GetProcAddress
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 6 -  String: LPCSTR
.head 4 +  Function: GetProcessHeap
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 -  Parameters
.head 4 +  Function: GetShortPathNameA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  Receive String: LPSTR
.head 6 -  Number: DWORD
.head 4 +  Function: GetSystemDefaultLangID
.head 5 -  Description: Retrieves the default language ID for the system
nSystemDefaultLangID = GetSystemDefaultLangID(  )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 -  Parameters
.head 4 +  Function: GetSystemDefaultLCID
.head 5 -  Description: Retrieves the default locale ID for the system
nSystemDefaultLCID = GetSystemDefaultLCID(  )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 -  Parameters
.head 4 +  Function: GetSystemDirectoryA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: UINT
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Number: UINT
.head 4 +  Function: GetSystemInfo
.head 5 -  Description: Loads  uSYSTEMINFO with information about the underlying hardware platform
bOk = GetSystemInfo( uSYSTEMINFO )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 4 +  Function: GetSystemTime
.head 5 -  Description: Loads uSYSTEMTIME with the current system time in Coordinated Universal Time ( UTC or GMT )
bOk = GetSystemTime( uSYSTEMTIME )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 4 +  Function: GetSystemTimeAdjustment
.head 5 -  Description: Allow synchronisation to an extenral source by adding an adjustment value ( in 100ns increments ) periodically.
bOk = GetSystemTimeAdjustment( nTimeAdded, nTimeBetweenAdjustments, bDisabled )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Receive Number: LPLONG
.head 6 -  Receive Boolean: LPBOOL
.head 4 +  Function: GetTempFileNameA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: UINT
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 6 -  String: LPSTR
.head 6 -  Number: UINT
.head 6 -  Receive String: LPSTR
.head 4 +  Function: GetTempPathA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 6 -  Receive String: LPSTR
.head 4 +  Function: GetThreadLocale
.head 5 -  Description: Retrieves the locale ID for the current thread
nLocaleID = GetThreadLocale( )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 -  Parameters
.head 4 +  Function: GetTickCount
.head 5 -  Description: The GetTickCount function retrieves the number of milliseconds that have elapsed since Windows was started.

Parameters:
	This function has no parameters.

Return Values:
	If the function succeeds, the return value is the number of milliseconds that have elapsed since Windows was started.

Remarks:
The elapsed time is stored as a DWORD value. Therefore, the time will wrap around to zero if Windows is run continuously for 49.7 days.

Windows NT: To obtain the time elapsed since the computer was started, look up the System Up Time counter in the performance data in the registry key HKEY_PERFORMANCE_DATA. The value returned is an 8 byte value.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 -  Parameters
.head 4 +  Function: GetTimeFormatA
.head 5 -  Description: Formats a system time format according to the format for the locale specified
nReturn = GetTimeFormatA( nLocaleID, nFlags, sTimeToFormat, nFormat, sResult, sResultBufferSize )
Note:	nFlags:		zero if sNumberToFormat specified
			LOCALE_NOUSEROVERRIDE to force system locale to be used even if overriden by a user
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 6 -  Number: LONG
.head 4 +  Function: GetTimeZoneInformation
.head 5 -  Description: Load uTIMEZONEINFORMATION with information about the zone setting for the system
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: INT
.head 7 +  struct
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: INT
.head 7 +  struct
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 7 -  Receive Number: LONG
.head 4 +  Function: GetUserDefaultLangID
.head 5 -  Description: Retrieves the default language ID for the current user
nUserDefaultLangID = GetUserDefaultLangID(  )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 -  Parameters
.head 4 +  Function: GetVersion
.head 5 -  Description: Determines the version of Windows and DOS currently running
nResult = GetVersion()
Note:	nResult:		Low word:	Windows Version
							Low byte: major version number
							High byte: minor version as a two digit decimal number
			Hig word:		Platform information
							High bit: 	0 for Windows NT
								1 for Win32a on Windows for Workgroups
GetVersionExA is the preferred function to use
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 -  Parameters
.head 4 +  ! Function: GetVersionExA
.head 5 -  Description: Loads uOSVERSIONINFO structure with version information about the platform and operating system
bOk = GetVersionEx( uOSVERSIONINFO )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  ! OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive String: LPSTR
.head 4 +  Function: GetVersionExA
.head 5 -  Description: Struct: OSVERSIONINFO
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive String: char[128]
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: BYTE
.head 7 -  Receive Number: BYTE
.head 4 +  Function: GetWindowsDirectoryA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: UINT
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Number: UINT
.head 4 +  Function: GlobalAddAtomA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 4 +  Function: GlobalAlloc
.head 5 -  Description: The GlobalAlloc function allocates the specified number of bytes from the heap. In the linear Win32 API environment,
there is no difference between the local heap and the global heap.

Parameters:
	uFlags 		Specifies how to allocate memory. If zero is specified, the default is GMEM_FIXED. Except for the incompatible combinations that are specifically noted, any combination of the following flags can be used.
			To indicate whether the function allocates fixed or movable memory, specify one of the first four flags:
			Flag  Meaning
 			GMEM_FIXED 		Allocates fixed memory. This flag cannot be combined with the GMEM_MOVEABLE or GMEM_DISCARDABLE flag.
						The return value is a pointer to the memory block. To access the memory, the calling process simply casts the return value to a pointer.
			GMEM_MOVEABLE 	Allocates movable memory. This flag cannot be combined with the GMEM_FIXED flag. The return value is the handle of the memory object.
						The handle is a 32-bit quantity that is private to the calling process. To translate the handle into a pointer, use the GlobalLock function.
			GPTR 			Combines the GMEM_FIXED and GMEM_ZEROINIT flags.
 			GHND 			Combines the GMEM_MOVEABLE and GMEM_ZEROINIT flags.
 			GMEM_DDESHARE 	Allocates memory to be used by the dynamic data exchange (DDE) functions for a DDE conversation. Unlike Windows version 3. x, this memory is not shared globally.
						However, this flag is available for compatibility purposes. It may be used by some applications to enhance the performance of DDE operations and should, therefore,
						be specified if the memory is to be used for DDE. Only processes that use DDE or the clipboard for interprocess communications should specify this flag.
			GMEM_DISCARDABLE 	Allocates discardable memory. This flag cannot be combined with the GMEM_FIXED flag. Some Win32-based applications may ignore this flag.
			GMEM_LOWER 		Ignored. This flag is provided only for compatibility with Windows version 3. x.
			GMEM_NOCOMPACT 	Does not compact or discard memory to satisfy the allocation request.
			GMEM_NODISCARD 	Does not discard memory to satisfy the allocation request.
			GMEM_NOT_BANKED 	Ignored. This flag is provided only for compatibility with Windows version 3. x.
			GMEM_NOTIFY 		Ignored. This flag is provided only for compatibility with Windows version 3. x.
			GMEM_SHARE 		Same as the GMEM_DDESHARE flag.
			GMEM_ZEROINIT 		Initializes memory contents to zero.
	dwBytes 		Specifies the number of bytes to allocate. If this parameter is zero and the uFlags parameter specifies the GMEM_MOVEABLE flag, the function returns a handle to a memory object that is marked as discarded.

Return Values:
	If the function succeeds, the return value is the handle of the newly allocated memory object.
	If the function fails, the return value is NULL.

Remarks:
If the heap does not contain sufficient free space to satisfy the request, GlobalAlloc returns NULL.

Because NULL is used to indicate an error, virtual address zero is never allocated. It is, therefore, easy to detect the use of a NULL pointer.

All memory is created with execute access; no special function is required to execute dynamically generated code.

Memory allocated with this function is guaranteed to be aligned on an 8-byte boundary.

The GlobalAlloc and LocalAlloc functions are limited to a combined total of 65,536 handles for GMEM_MOVEABLE and LMEM_MOVEABLE memory per process. This limitation does not apply to GMEM_FIXED or LMEM_FIXED memory.

If this function succeeds, it allocates at least the amount of memory requested. If the actual amount allocated is greater than the amount requested, the process can use the entire amount.
To determine the actual number of bytes allocated, use the GlobalSize function.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: UINT
.head 6 -  Number: DWORD
.head 4 +  Function: GlobalDeleteAtom
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: INT
.head 4 +  Function: GlobalFree
.head 5 -  Description: The GlobalFree function frees the specified global memory object and invalidates its handle.

Parameters:
	hMem 		Identifies the global memory object. This handle is returned by either the GlobalAlloc or GlobalReAlloc function.

Return Values:
	If the function succeeds, the return value is NULL.
	If the function fails, the return value is equal to the handle of the global memory object. To get extended error information, call GetLastError.

Remarks:
Heap corruption or an access violation exception (EXCEPTION_ACCESS_VIOLATION) may occur if the process tries to examine or modify the memory after it has been freed.

If the hgblMem parameter is NULL, GlobalFree fails and the system generates an access violation exception.

Both GlobalFree and LocalFree will free a locked memory object. A locked memory object has a lock count greater than zero. The GlobalLock function locks a global memory object and increments the lock count by one.
The GlobalUnlock function unlocks it and decrements the lock count by one. To get the lock count of a global memory object, use the GlobalFlags function.

Windows NT: However, if an application is running under a debug (DBG) version of Windows NT, such as the one distributed on the SDK CD-ROM, both GlobalFree and LocalFree enter a breakpoint just before freeing a locked object.
This lets a programmer double-check the intended behavior. Typing G while using the debugger in this situation lets the freeing operation occur.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 4 +  Function: GlobalGetAtomNameA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: INT
.head 6 -  Receive String: LPSTR
.head 6 -  Number: INT
.head 4 +  ! Function: GlobalLock
.head 5 -  Description: The GlobalLock function locks a global memory object and returns a pointer to the first byte of the object’s memory block.
The memory block associated with a locked memory object cannot be moved or discarded. For memory objects allocated with the
GMEM_MOVEABLE flag, the function increments the lock count associated with the memory object.

Parameters:
	hMem 		Identifies the global memory object. This handle is returned by either the GlobalAlloc or GlobalReAlloc function.

Return Values:
	If the function succeeds, the return value is a pointer to the first byte of the memory block.
	If the function fails, the return value is NULL. To get extended error information, call GetLastError.

Remarks:
The internal data structures for each memory object include a lock count that is initially zero. For movable memory objects,
GlobalLock increments the count by one, and the GlobalUnlock function decrements the count by one. For each call that a process
makes to GlobalLock for an object, it must eventually call GlobalUnlock. Locked memory will not be moved or discarded, unless the
memory object is reallocated by using the GlobalReAlloc function. The memory block of a locked memory object remains locked until its
lock count is decremented to zero, at which time it can be moved or discarded.

Memory objects allocated with the GMEM_FIXED flag always have a lock count of zero. For these objects, the value of the returned pointer
is equal to the value of the specified handle.

If the specified memory block has been discarded or if the memory block has a zero-byte size, this function returns NULL.

Discarded objects always have a lock count of zero.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Number: DWORD
.head 5 +  Parameters 
.head 6 -  Number: WORD
.head 4 +  Function: GlobalMemoryStatus
.head 5 -  Description: The GlobalMemoryStatus function obtains information about the computer
system's current usage of both physical and virtual memory.

Parameter: lpBuffer
		Pointer to a MEMORYSTATUS structure.
		The GlobalMemoryStatus function stores information about
		current memory availability into this structure.

dwLength
	The size in bytes of the MEMORYSTATUS data structure. You do not need to set this
	member before calling the GlobalMemoryStatus function; the function sets it.
dwMemoryLoad
	Specifies a number between 0 and 100 that gives a general idea of current memory utilization,
	in which 0 indicates no memory use and 100 indicates full memory use.
dwTotalPhys
	Indicates the total number of bytes of physical memory.
dwAvailPhys
	Indicates the number of bytes of physical memory available.
dwTotalPageFile
	Indicates the total number of bytes that can be stored in the paging file. Note that this
	number does not represent the actual physical size of the paging file on disk.
dwAvailPageFile
	Indicates the number of bytes available in the paging file.
dwTotalVirtual
	Indicates the total number of bytes that can be described in the user mode portion of the
	virtual address space of the calling process.
dwAvailVirtual
	Indicates the number of bytes of unreserved and uncommitted memory in the user mode
	portion of the virtual address space of the calling process.


.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 4 +  Function: GlobalSize
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  ! Function: GlobalUnlock
.head 5 -  Description: The GlobalUnlock function decrements the lock count associated with a memory object that was allocated with the GMEM_MOVEABLE flag.
This function has no effect on memory objects allocated with the GMEM_FIXED flag.

Parameters:
	hMem 		Identifies the global memory object. This handle is returned by either the GlobalAlloc or GlobalReAlloc function.

Return Values:
	If the memory object is still locked after decrementing the lock count, the return value is a nonzero value.
	If the function fails, the return value is zero. To get extended error information, call GetLastError. If GetLastError returns
	NO_ERROR, the memory object is unlocked.

Remarks:
The internal data structures for each memory object include a lock count that is initially zero. For movable memory objects, the GlobalLock
function increments the count by one, and GlobalUnlock decrements the count by one. For each call that a process makes to GlobalLock
for an object, it must eventually call GlobalUnlock. Locked memory will not be moved or discarded, unless the memory object is reallocated
by using the GlobalReAlloc function. The memory block of a locked memory object remains locked until its lock count is decremented to zero,
at which time it can be moved or discarded.

Memory objects allocated with the GMEM_FIXED flag always have a lock count of zero. If the specified memory block is fixed memory,
this function returns TRUE.

If the memory object is already unlocked, GlobalUnlock returns FALSE and GetLastError reports ERROR_NOT_LOCKED.
Memory objects allocated with the LMEM_FIXED flag always have a lock count of zero and cause the ERROR_NOT_LOCKED error.

A process should not rely on the return value to determine the number of times it must subsequently call GlobalUnlock for a memory object.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Number: INT
.head 5 +  Parameters 
.head 6 -  Number: WORD
.head 4 -  !
.head 4 +  Function: HeapAlloc
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LPVOID
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 4 +  Function: HeapFree
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: DWORD
.head 6 -  Number: LPVOID
.head 4 -  !
.head 4 +  Function: IsValidCodePage
.head 5 -  Description: Determines if the specified code page is valid
bValid = IsValidCodePage( nCodePage )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: IsValidLocale
.head 5 -  Description: Determines if a locale identifier is valide
bValid = IsValidLocale( nLocaleID, nFlags )
Note:		nFlags:	LCID_SUPPORTED/LCID_INSTALLED
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 -  !
.head 4 +  Function: LoadLibraryA
.head 5 -  Description: The LoadLibrary function maps the specified executable module into the address space of the calling process.

Parameters:
	lpLibFileName 		Points to a null-terminated string that names the executable module (either a .DLL or .EXE file). The name specified is the filename of the module and is not related to
				the name stored in the library module itself, as specified by the LIBRARY keyword in the module-definition (.DEF) file.

				If the string specifies a path but the file does not exist in the specified directory, the function fails. When specifying a path, be sure to use backslashes (\), not forward slashes (/).

				If a path is not specified and the filename extension is omitted, the default library extension .DLL is appended. However, the filename string can include a trailing point character (.)
				to indicate that the module name has no extension. When no path is specified, the function searches for the file in the following sequence:

				1. The directory from which the application loaded.

				2. The current directory.

				3. Windows 95: The Windows system directory. Use the GetSystemDirectory function to get the path of this directory.

				Windows NT: The 32-bit Windows system directory. Use the GetSystemDirectory function to get the path of this directory. The name of this directory is SYSTEM32.

				4. Windows NT: The 16-bit Windows system directory. There is no Win32 function that obtains the path of this directory, but it is searched. The name of this directory is SYSTEM.

				5. The Windows directory. Use the GetWindowsDirectory function to get the path of this directory.

				6. The directories that are listed in the PATH environment variable.

				The first directory searched is the one directory containing the image file used to create the calling process (for more information, see the CreateProcess function).
				Doing this allows private dynamic-link library (DLL) files associated with a process to be found without adding the process’s installed directory to the PATH environment variable.

				Once the function obtains a fully qualified path to a library module file, the path is compared (case independently) to the full paths of library modules currently loaded into the calling process.
				These libraries include those loaded when the process was starting up as well as those previously loaded by LoadLibrary but not unloaded by FreeLibrary.
				If the path matches the path of an already loaded module, the function just increments the reference count for the module and returns the module handle for that library.

Return Values:
	If the function succeeds, the return value is a handle to the module.
	If the function fails, the return value is NULL.

Remarks:
LoadLibrary can be used to map a DLL module and return a handle that can be used in GetProcAddress to get the address of a DLL function. LoadLibrary can also be used to map other executable modules.
For example, the function can specify an .EXE file to get a handle that can be used in FindResource or LoadResource.

Module handles are not global or inheritable. A call to LoadLibrary by one process does not produce a handle that another process can use ¾ for example, in calling GetProcAddress.
The other process must make its own call to LoadLibrary for the module before calling GetProcAddress.

If the module is a DLL not already mapped for the calling process, the system calls the DLL’s DllEntryPoint function with the DLL_PROCESS_ATTACH value.
If the DLL’s entry-point function does not return TRUE, LoadLibrary fails and returns NULL.

Windows 95: If you are using LoadLibrary to load a module that contains a resource whose numeric identifier is greater than 0x7FFF, LoadLibrary fails.
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 4 +  Function: LoadModule
.head 5 -  Description:
.head 5 -  Export Ordinal: 45
.head 5 +  Returns
.head 6 -  Window Handle: HWND
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 4 +  Function: lstrcmp
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 4 +  Function: lstrcmpi
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 4 +  Function: lstrlen
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 -  !
.head 4 +  Function: MoveFileA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 4 -  !
.head 4 +  Function: OpenProcess
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 6 -  Boolean: BOOL
.head 6 -  Number: DWORD
.head 4 -  !
.head 4 +  Function: TerminateProcess
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: UINT
.head 4 -  !
.head 4 +  Function: SetEnvironmentVariableA
.head 5 -  Description: Sets the value of an environment variable
bOk = SetEnvironmentVariableA( sVariableName, sNewValue )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Receive String: LPSTR
.head 4 +  Function: SetLastError
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LPVOID
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 4 +  Function: SetLocaleInfoA
.head 5 -  Description: Sets information for the specified locale
nResult = SetLocalesInfoA (  nLocaleID, nInformationType, sNewSetting )
Note:	nInformationType:		LOCALE_*
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 4 +  Function: SetLocalTime
.head 5 -  Description: Set the local date and time
bOk = SetLocalTime (  uSYSTEMTIME )
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 7 -  Number: INT
.head 4 +  Function: SetSystemTime
.head 5 -  Description: Sets the current system time in Coordinated Universal Time ( UTC or GMT )
bOk = SetSystemTime( uSYSTEMTIME )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 4 +  Function: SetSystemTimeAdjustment
.head 5 -  Description: Allow synchronisation to an external source by adding an adjustment value ( in 100ns increments ) periodically.
bOk = SetSystemTimeAdjustment( nTimeBetweenAdjustments, bDisabled )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Receive Number: LPLONG
.head 6 -  Receive Boolean: LPBOOL
.head 4 +  Function: SetThreadLocale
.head 5 -  Description: Set the locale ID for the current thread
bOk = GetThreadLocale( nLocaleID)
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: SetTimeZoneInformation
.head 5 -  Description: Set the zone setting for the system
bOk =  SetTimeZoneInformation( uTIMEZONEINFORMATION )
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: INT
.head 7 +  struct
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: INT
.head 7 +  struct
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 7 -  Receive Number: LONG
.head 4 +  Function: Sleep
.head 5 -  Description: The Sleep function suspends the execution of the current thread for a specified interval, in milliseconds.
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 4 +  Function: SystemTimeToTzSpecificLocalTime
.head 5 -  Description: Converts a system time to local time in Coordinated Universal Time ( UTC or GMT )
bOk = SystemTimeToTzSpecificLocalTime ( uTIMEZONEINFORMATION, uSYSTEMTIMESystemTime, uSYSTEMTIMELocalTime )
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: INT
.head 7 +  struct
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: INT
.head 7 +  struct
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 8 -  Receive Number: INT
.head 7 -  Receive Number: LONG
.head 6 +  structPointer
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 6 +  structPointer
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 7 -  Receive Number: INT
.head 4 -  !
.head 4 +  Function: WinExec
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: UINT
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  Number: UINT
.head 4 +  ! Function: WriteFile
.head 5 -  Description: The WriteFile function writes data to a file and is designed for both synchronous and asynchronous operation. The function starts writing data to the file at the position indicated by the file pointer. After the write operation has been completed, the
file pointer is adjusted by the number of bytes actually written, except when the file is opened with FILE_FLAG_OVERLAPPED. If the file handle was created for overlapped input and output (I/O), the application must adjust the position of the file
pointer after the write operation is finished.

BOOL WriteFile(
	HANDLE hFile, 			// handle to file to write to
	LPCVOID lpBuffer, 			// pointer to data to write to file
	DWORD nNumberOfBytesToWrite, 	// number of bytes to write
	LPDWORD lpNumberOfBytesWritten, 	// pointer to number of bytes written
	LPOVERLAPPED lpOverlapped 	// pointer to structure needed for overlapped I/O
	);
The OVERLAPPED structure contains information used in asynchronous input and output (I/O).
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: HANDLE
.head 6 -  String: LPCSTR
.head 6 -  Number: DWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  Number: DWORD
.head 3 +  Library name: ADVAPI32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: GetUserNameA
.head 5 -  Description: The GetUserName function retrieves the user name of the current thread.
This is the name of the user currently logged onto the system.

lpBuffer
	Pointer to the buffer to receive the null-terminated string containing the user's
	logon name. If this buffer is not large enough to contain the entire user name,
	the function fails. A buffer size of (UNLEN + 1) characters will hold the maximum
	length user name including the terminating null character.
nSize
	Pointer to a DWORD variable that, on input, specifies the maximum size, in
	characters, of the buffer specified by the lpBuffer parameter. If the function
	succeeds, the variable receives the number of characters copied to the buffer.
	If the buffer is not large enough, the function fails and the variable receives the
	required buffer size, in characters, including the terminating null character.
Return Values
	If the function succeeds, the return value is nonzero, and the variable pointed to
	by nSize contains the number of characters copied to the buffer specified by
	lpBuffer, including the terminating null character.

	If the function fails, the return value is zero. To get extended error information,
	call GetLastError.


.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Receive String: LPSTR
.head 6 -  Receive Number: LPDWORD
.head 4 +  ! Function: RegCloseKey
.head 5 -  Description: function RegCloseKey(hKey: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 4 +  ! Function: RegOpenKeyExA
.head 5 -  Description: function RegOpenKeyExA(
  hKey: HKEY;
  lpSubKey: PAnsiChar;
  ulOptions: DWORD;
  samDesired: REGSAM;
  var phkResult: HKEY): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 6 -  String: LPCSTR
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Receive Number: LPLONG
.head 4 +  ! Function: RegQueryValueExA
.head 5 -  Description: function RegQueryValueExA(
  hKey: HKEY;
  lpValueName: PAnsiChar;
  lpReserved: Pointer;
  lpType: PDWORD;
  lpData: PByte;
  lpcbData: PDWORD): Longint;
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 6 -  String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 6 -  Receive String: LPVOID
.head 6 -  Receive Number: LPDWORD
.head 3 +  ! Library name: STRCi51.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: CStructGetByte
.head 5 -  Description: Extract a byte from a buffer.
nResult = CStructGetByte( strBuffer, nOffset )
.head 5 -  Export Ordinal: 1
.head 5 +  Returns 
.head 6 -  Number: BYTE
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetInt
.head 5 -  Description: Extract an integer from a buffer.
nResult = CStructGetInt( strBuffer, nOffset )
.head 5 -  Export Ordinal: 5
.head 5 +  Returns 
.head 6 -  Number: INT
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetWord
.head 5 -  Description: Extract a word from a buffer.
nResult = CStructGetWord( strBuffer, nOffset )
.head 5 -  Export Ordinal: 3
.head 5 +  Returns 
.head 6 -  Number: WORD
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetLong
.head 5 -  Description: Extract a long from a buffer.
nResult = CStructGetLong( strBuffer, nOffset )
.head 5 -  Export Ordinal: 5
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetFloat
.head 5 -  Description: Extract a float from a buffer.
nResult = CStructGetFloat( strBuffer, nOffset )
.head 5 -  Export Ordinal: 7
.head 5 +  Returns 
.head 6 -  Number: FLOAT
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetDouble
.head 5 -  Description: Extract a double from a buffer.
nResult = CStructGetDouble( strBuffer, nOffset )
.head 5 -  Export Ordinal: 9
.head 5 +  Returns 
.head 6 -  Number: DOUBLE
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetBFloat
.head 5 -  Description: Extract a BFloat from a buffer and convert to double.
nResult = CStructGetBFloat( strBuffer, nOffset )
.head 5 -  Export Ordinal: 21
.head 5 +  Returns 
.head 6 -  Number: DOUBLE
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetString
.head 5 -  Description: Extract a string from a buffer.
nLength = CStructGetString( strBuffer, nOffset, nMaxWidth, strExtract )
.head 5 -  Export Ordinal: 11
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPSTR
.head 4 +  Function: CStructGetFarPointer
.head 5 -  Description: Extract a far pointer from a buffer.
nFarPointer = CStructGetFarPointer( strBuffer, nOffset )
.head 5 -  Export Ordinal: 17
.head 5 +  Returns 
.head 6 -  Number: ULONG
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructPutByte
.head 5 -  Description: Insert a byte into a buffer.
bOK = CStructPutByte( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 2
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: BYTE
.head 4 +  Function: CStructPutInt
.head 5 -  Description: Insert an integer into a buffer.
bOK = CStructPutInt( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 6
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: INT
.head 4 +  Function: CStructPutWord
.head 5 -  Description: Insert a word into a buffer.
bOK = CStructPutWord( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 4
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: WORD
.head 4 +  Function: CStructPutLong
.head 5 -  Description: Insert a long into a buffer.
bOK = CStructPutLong( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 6
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: CStructPutFloat
.head 5 -  Description: Insert a byte into a buffer.
bOK = CStructPutFloat( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 8
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: FLOAT
.head 4 +  Function: CStructPutDouble
.head 5 -  Description: Insert a double into a buffer.
bOK = CStructPutDouble( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 10
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: DOUBLE
.head 4 +  Function: CStructPutBFloat
.head 5 -  Description: Convert double to BFloat and insert it into a buffer.
bOK = CStructPutBFloat( strBuffer, nOffset, nInsert )
.head 5 -  Export Ordinal: 22
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: DOUBLE
.head 4 +  Function: CStructPutString
.head 5 -  Description: Insert a string into a buffer.
bOK = CStructPutString( strBuffer, nOffset, nMaxWidth, strInsert )
.head 5 -  Export Ordinal: 12
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  String: LPSTR
.head 4 +  Function: CStructPutFarPointer
.head 5 -  Description: Insert a far pointer into a buffer.
bOK = CStructPutFarPointer( strBuffer, nOffset, nFarPointer )
.head 5 -  Export Ordinal: 18
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: CStructCopyBuffer
.head 5 -  Description: Copy data from one String to another.
bOK = CStructCopyBuffer( strDest, nDestOffset,
                         strSrc, nSrcOffset, nCopyLen )
.head 5 -  Export Ordinal: 19
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: CStructAllocFarMem
.head 5 -  Description: Allocate memory and return a far pointer.
nFarPointer = CStructAllocFarMem( nBytes )
.head 5 -  Export Ordinal: 13
.head 5 +  Returns 
.head 6 -  Number: ULONG
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 4 +  Function: CStructFreeFarMem
.head 5 -  Description: Free memory allocated by CStructAllocFarMem.
bOK = CStructFreeFarMem( nFarPointer )
.head 5 -  Export Ordinal: 14
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 4 +  Function: CStructCopyToFarMem
.head 5 -  Description: Copy data from far memory to a String.
bOK = CStructCopyToFarMem( nFarPointer, strData, nDataLen  )
.head 5 -  Export Ordinal: 15
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructCopyFromFarMem
.head 5 -  Description: Copy from a String to far memory.
bOK = CStructCopyFromFarMem( nFarPointer, strData, nMaxLen )
.head 5 -  Export Ordinal: 16
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Number: LONG
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 4 +  Function: CStructGetStringW
.head 5 -  Description: Extract a string from a buffer.
nLength = CStructGetStringW( strBuffer, nOffset, nMaxWidth, strExtract )
.head 5 -  Export Ordinal: 23
.head 5 +  Returns 
.head 6 -  Number: LONG
.head 5 +  Parameters 
.head 6 -  String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  Receive String: LPWSTR
.head 4 +  Function: CStructPutStringW
.head 5 -  Description: Insert a string into a buffer.
bOK = CStructPutStringW( strBuffer, nOffset, nMaxWidth, strInsert )
.head 5 -  Export Ordinal: 24
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 6 -  String: LPWSTR
.head 2 +  External Assemblies
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 3 +  User
.head 4 -  Number: VK_Backslash = 191
.head 4 -  Number: WM_KEYUP = 0x0101
.head 4 -  Number: VK_RETURN=0x0D
.head 4 -  Number: Key_Up           = 0x26
.head 4 -  Number: Key_Down         = 0x28
.head 4 -  String: String_Null         = ''
.head 4 -  String: TAB = '	'
.head 4 -  ! Number: VTM_LeftClick             = 0x0202
.head 4 -  ! String: sProgramINI = 'C:\\Gupta\\Team Developer 5.1\\apps\\crim\\crim.ini'
.head 4 -  ! ! ! - OS Versions
.head 4 -  Number: VERSION_OS_95		= 0
.head 4 -  Number: VERSION_OS_95_OSR1	= 1
.head 4 -  Number: VERSION_OS_95_OSR2	= 2
.head 4 -  Number: VERSION_OS_95_OSR21	= 3
.head 4 -  Number: VERSION_OS_95_OSR25	= 4
.head 4 -  Number: VERSION_OS_98		= 5
.head 4 -  Number: VERSION_OS_98_SE	= 6
.head 4 -  Number: VERSION_OS_NT_351	= 7
.head 4 -  Number: VERSION_OS_NT_4	= 8
.head 4 -  Number: VERSION_OS_2000		= 9
.head 4 -  Number: VERSION_OS_ME		= 10
.head 4 -  Number: VERSION_OS_XP		= 11
.head 4 -  Number: VERSION_OS_NET		= 12
.head 4 -  Number: VER_PLATFORM_WIN32s		 = 0
.head 4 -  Number: VER_PLATFORM_WIN32_WINDOWS	= 1
.head 4 -  Number: VER_PLATFORM_WIN32_NT		= 2
.head 4 -  ! ! !
.head 4 -  Number: VER_NT_WORKSTATION		= 1
.head 4 -  Number: VER_NT_DOMAIN_CONTROLLER	= 2
.head 4 -  Number: VER_NT_SERVER			= 3
.head 4 -  ! ! !
.head 4 -  Number: VER_SUITE_SMALLBUSINESS		= 0x00000001
.head 4 -  Number: VER_SUITE_ENTERPRISE			= 0x00000002
.head 4 -  Number: VER_SUITE_BACKOFFICE			= 0x00000004
.head 4 -  Number: VER_SUITE_COMMUNICATIONS		= 0x00000008
.head 4 -  Number: VER_SUITE_TERMINAL			= 0x00000010
.head 4 -  Number: VER_SUITE_SMALLBUSINESS_RD	= 0x00000020
.head 4 -  Number: VER_SUITE_EMBEDDEDNT			= 0x00000040
.head 4 -  Number: VER_SUITE_DATACENTER			= 0x00000080
.head 4 -  Number: VER_SUITE_SINGLEUSERTS		= 0x00000100
.head 4 -  Number: VER_SUITE_PERSONAL			= 0x00000200
.head 4 -  Number: VER_SUITE_BLADE			= 0x00000400
.head 4 -  String: cSQLErrorLogFile = 'SQLError.log'
.head 4 -  Number: GET_COL_SUM  = 0x0900
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.data RESOURCE 0 0 1 525064137
0000: 2200000022000000 0000000000000000 000001000000F201 0000190000000001
0020: 0210083400000000 FF06610267000400 00000000
.enddata
.head 3 -  FunctionalVar: Startup
.head 4 -  Class: cStartup
.head 3 -  Boolean: bLogin
.head 3 -  Sql Handle: hSql
.head 3 -  Boolean: bChange
.head 3 -  String: sReport
.head 3 -  String: sReportBinds
.head 3 -  String: sReportInputs
.head 3 -  Number: nReturn
.head 3 -  Number: nPrintErr
.head 3 -  Window Handle: hWndTable
.head 3 -  String: sCTABLE_MENU_TYPE
.head 3 -  ! !!!!!  <<CourtInfo Data Fields >>
.head 3 -  String: sCourt
.head 3 -  String: sCourtCity
.head 3 -  String: sCourtClerk
.head 3 -  String: sCourtCounty
.head 3 -  String: sCourtAddr
.head 3 -  String: sCourtAddr2
.head 3 -  String: sCourtCityState
.head 3 -  String: sCourtPhone
.head 3 -  String: sCourtPhone2
.head 3 -  String: sCourtPhone3
.head 3 -  String: sCourtCode
.head 3 -  String: sCourtORI
.head 3 -  Date/Time: dCostsDate
.head 3 -  Number: nVBDFineInc
.head 3 -  Number: nVBDFineIncI
.head 3 -  String: sCHECKCJISJailFlag
.head 3 -  String: sCourtConst
.head 3 -  String: sCourtDirections
.head 3 -  String: sCourtCBailiff
.head 3 -  String: sCourtPJudge
.head 3 -  Date/Time: dtCourt_Change_Password_Date
.head 3 -  ! ! !!!!!  <<User Table Data Fields >>
.head 3 -  String: sUClerk
.head 3 -  String: sUDivision
.head 3 -  String: sUDrawer
.head 3 -  String: sUDepartment
.head 3 -  String: sUFullName
.head 3 -  String: sUWPhone
.head 3 -  String: sUWPhone_Ext
.head 3 -  Number: nUserId
.head 3 -  Number: nULevel
.head 3 -  Number: nUCashier
.head 3 -  Number: nURecCopies
.head 3 -  String: sUChangePassword
.head 3 -  Number: nUGraceLogins
.head 3 -  String: sUserInit
.head 3 -  String: sUJudge
.head 3 -  FunctionalVar: SQL
.head 4 -  Class: cSQL
.head 3 -  FunctionalVar: REG
.head 4 -  Class: cBTRegistryW
.head 3 -  Boolean: bCVCaseInfo
.head 2 +  Internal Functions
.head 3 +  ! Function: ReadRegistryTitle
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/Court', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Title', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: WriteUser
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Write )
.head 6 -  Call SalFileWrite( hFLogin, SqlUser, 9 )
.head 6 -  Call SalFileClose( hFLogin )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  Call REG.WriteStringAt('Software/CJIS/LastUser',TRUE, 'User', SqlUser)
.head 5 -  Call REG.CloseKey( )
.head 3 +  Function: ReadRegistryOpenWith
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 +  Parameters
.head 5 -  String: sParam
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CLASSES_ROOT )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/'||sParam, FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( '', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  ! If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Call REG.SetRootKey( HKEY_CLASSES_ROOT )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/'||s||'/Shell/open/command', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( '', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  ! If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  ! Function: ReadRegistry
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/PUBLIC', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'PUBLIC', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  ! Function: ReadRegistrySelect
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/UserSelect', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Select', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: ReadRegistryLastUser
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/LastUser', FALSE )
.head 6 -  ! Call REG.ERR( 'Error opening key.' )
.head 5 +  ! If not REG.OpenKey( '/Software/CJIS/LastUser', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'User', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: Inc
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive Number: n
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set n = n + 1
.head 5 -  Return n
.head 3 +  Function: Dec
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive Number: n
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set n = n - 1
.head 5 -  Return n
.head 3 +  Function: HolidayCheck	!! Returns TRUE if lands on holiday
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean: bHoliday
.head 4 +  Parameters
.head 5 -  Date/Time: dHearDate
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHoliday
.head 4 +  Actions
.head 5 -  Set sHoliday = STRING_Null
.head 5 -  Set dHearDate = SalDateConstruct( SalDateYear( dHearDate ), SalDateMonth( dHearDate ), SalDateDay( dHearDate ), 0, 0, 0 )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	text
			      FROM	calendar
 			      INTO		:sHoliday
			      WHERE	:dHearDate = holiday')
.head 5 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  If nReturn = FETCH_Ok
.head 6 -  Call SalMessageBeep( MB_IconAsterisk )
.head 6 -  Call SalMessageBox('Date would have fallen on ' || sHoliday || '.', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 -  Return TRUE
.head 5 +  Else If SalDateWeekday( dHearDate ) = 0
.head 6 -  Call SalMessageBeep( MB_IconAsterisk )
.head 6 -  Call SalMessageBox('Date would have fallen on a Saturday', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 -  Return TRUE
.head 5 +  Else If SalDateWeekday( dHearDate ) = 1
.head 6 -  Call SalMessageBeep( MB_IconAsterisk )
.head 6 -  Call SalMessageBox('Date would have fallen on a Sunday', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: CopyRows
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Number: nParam
.head 5 -  Window Handle: hWndTable
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If nParam = 1
.head 6 +  If SalTblCopyRows( hWndTable, 2, 0 )
.head 7 -  Call SalMessageBox( 'Selected Rows copied to Clipboard', 'Process Complete', MB_Ok)
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'No Rows selected to copy to Clipboard, please make selection and try again.', 'Process Incomplete', MB_Ok)
.head 5 +  Else If nParam = 2
.head 6 +  If SalTblCopyRows( hWndTable, 0, 0 )
.head 7 -  Call SalMessageBox( 'All Rows copied to Clipboard', 'Process Complete', MB_Ok)
.head 3 +  Function: Encode_BarCode
.head 4 -  Description: WRP (10-26-2003)
	-This function takes a string parameter and converts it into the code
	 that is understandable to a  barcode reader using CODE 128 font.
	-It returns the encoded text as a string variable
	-This function is compatible with Elfring Font INC
	-This uses Subset C
	-Perfered font is
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: BarTextIn
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: BarTextOut
.head 5 -  String: BarTextInA
.head 5 -  String: TempString
.head 5 -  String: BarTempOut
.head 5 -  String: BarCodeOut
.head 5 -  Number: Sum
.head 5 -  Number: n
.head 5 -  Number: nStrLength
.head 5 -  Number: ThisChar
.head 5 -  Number: CharValue
.head 5 -  Number: CheckSumValue
.head 5 -  String: CheckSum
.head 5 -  Number: Subset
.head 5 -  String: StartChar
.head 5 -  Number: Weighting
.head 5 -  Number: UCC
.head 5 -  String: sCurrentCharReal
.head 4 +  Actions
.head 5 -  ! Initialize input and output strings
.head 5 -  Set Sum = 104
.head 5 -  Set StartChar = '|'
.head 5 -  Set n = 1
.head 5 -  Set nStrLength = SalStrLength(BarTextIn)
.head 5 +  While n <= nStrLength
.head 6 -  ! ThisChar = (Asc(Mid(BarTextIn, II, 1)))
.head 6 -  ! Call SalStrFirstC( 'A', ThisChar )
.head 6 -  ! Call SalStrFirstC( SalStrMidX(BarTextIn, (n-1), 1), ThisChar )
.head 6 -  Set sCurrentCharReal = SalStrMidX(BarTextIn, (n-1), 1)
.head 6 -  Call SalStrFirstC( sCurrentCharReal, ThisChar )
.head 6 +  If ThisChar < 127
.head 7 -  Set CharValue = ThisChar - 32
.head 6 +  Else
.head 7 -  Set CharValue = ThisChar - 103
.head 6 -  Set Sum = Sum + (CharValue * n)
.head 6 +  If SalStrMidX(BarTextIn, (n-1), 1) = " "
.head 7 -  Set BarTextOut = BarTextOut || SalNumberToChar(228)
.head 6 +  Else If ThisChar = 34
.head 7 -  Set BarTextOut = BarTextOut || SalNumberToChar(226)
.head 6 +  Else
.head 7 -  Set BarTextOut = BarTextOut || SalStrMidX(BarTextIn, (n-1), 1)
.head 6 -  Set n = n + 1
.head 5 -  Set CheckSumValue = SalNumberMod( Sum, 103 )
.head 5 +  If CheckSumValue > 90
.head 6 -  Set CheckSum = SalNumberToChar(CheckSumValue + 103)
.head 5 +  Else If CheckSumValue > 0
.head 6 -  Set CheckSum = SalNumberToChar(CheckSumValue + 32)
.head 5 +  Else
.head 6 -  Set CheckSum = SalNumberToChar(228)
.head 5 -  Return (StartChar || BarTextOut || CheckSum || "~ ")
.head 3 +  Function: GetAdminLock
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sAdminLock
.head 4 +  Parameters
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseTy
.head 5 -  String: fCaseNo
.head 5 -  Receive String: fAdminLockComment
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAdminLock
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute(hSql, 'SELECT	admin_lock, admin_lock_comment
		  	      FROM	muni_booking
			      WHERE	caseyr = :fCaseYr and
					casety = :fCaseTy and
					caseno = :fCaseNo and
					(admin_lock_expire >= sysdate or admin_lock_expire is null)
			      INTO		:sAdminLock, :fAdminLockComment')
.head 5 -  Call SqlFetchNext(hSql, nReturn)
.head 5 -  Return sAdminLock
.head 3 +  Function: GetAdminLockCV
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sAdminLock
.head 4 +  Parameters
.head 5 -  Number: fCaseYr
.head 5 -  Number: fCaseNo
.head 5 -  Receive String: fAdminLockComment
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAdminLock
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute(hSql, 'SELECT	admin_lock, admin_lock_comment
		  	      FROM	casemaster
			      WHERE	caseyr = :fCaseYr and
					caseno = :fCaseNo and
					(admin_lock_expire >= sysdate or admin_lock_expire is null)
			      INTO		:sAdminLock, :fAdminLockComment')
.head 5 -  Call SqlFetchNext(hSql, nReturn)
.head 5 -  Return sAdminLock
.head 3 +  Function: TBL_DisplaySum
.head 4 -  Description: WRP - Function will set a windows status bar with the number of rows
          selected, the sum of the a certain column ( hTBL_Col), and the average
          of the certain column.
          Function returns TRUE if it was succesful in setting the Status Text and FALSE if not.
.head 4 +  Returns
.head 5 -  Boolean: bReturn
.head 4 +  Parameters
.head 5 -  Window Handle: hTBL   !Table to use
.head 5 -  Window Handle: hTBL_Col    !Table Column to sum and average * must be a number
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nRowAmount
.head 5 -  Number: nCountRow
.head 5 -  Number: nRow
.head 5 -  String: sText
.head 5 -  Boolean: bReturn
.head 4 +  Actions
.head 5 -  Set nRowAmount = 0
.head 5 -  Set nCountRow = 0
.head 5 -  Set nRow = -1
.head 5 +  While SalTblFindNextRow( hTBL, nRow, ROW_Selected, NUMBER_Null )
.head 6 -  Call SalTblSetFocusRow( hTBL, nRow )
.head 6 -  Set sText =  VisTblGetCell( hTBL, nRow, hTBL_Col )
.head 6 -  Set nRowAmount = nRowAmount + SalStrToNumber( sText)
.head 6 -  Set nCountRow = nCountRow + 1
.head 5 +  If nCountRow > 0
.head 6 -  Set bReturn = SalStatusSetText( hWndForm, SalNumberToStrX(nCountRow, 0)||' Rows Selected - $'||SalNumberToStrX( nRowAmount, 2)||' Rows Average - $'||SalNumberToStrX( nRowAmount/nCountRow, 2))
.head 5 +  Else
.head 6 -  Set bReturn = SalStatusSetText( hWndForm, 'Ready')
.head 5 -  Return bReturn
.head 3 +  Function: TBL_ReturnSum
.head 4 -  Description: WRP - Function will return the number of rows (nRetRows), sum of rows (nRetSumRows) and
          average of rows (nRetAvgRows) of the selected rows in a table (hTBL)
          Function returns TRUE if it was succesful in setting the Status Text and FALSE if not.
.head 4 +  Returns
.head 5 -  Boolean: bReturn
.head 4 +  Parameters
.head 5 -  Window Handle: hTBL   !Table to use
.head 5 -  Window Handle: hTBL_Col    !Table Column to sum and average * must be a number
.head 5 -  Receive Number: nRetRows
.head 5 -  Receive Number: nRetSumRows
.head 5 -  Receive Number: nRetAvgRows
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nRowAmount
.head 5 -  Number: nCountRow
.head 5 -  Number: nRow
.head 5 -  String: sText
.head 5 -  Boolean: bReturn
.head 4 +  Actions
.head 5 -  Set nRowAmount = 0
.head 5 -  Set nCountRow = 0
.head 5 -  Set nRow = -1
.head 5 +  While SalTblFindNextRow( hTBL, nRow, ROW_Selected, NUMBER_Null )
.head 6 -  Set bReturn = SalTblSetFocusRow( hTBL, nRow )
.head 6 -  Set sText =  VisTblGetCell( hTBL, nRow, hTBL_Col )
.head 6 -  Set nRowAmount = nRowAmount + SalStrToNumber( sText)
.head 6 -  Set nCountRow = nCountRow + 1
.head 5 -  Set nRetRows = nCountRow
.head 5 -  Set nRetSumRows = nRowAmount
.head 5 +  If nCountRow > 0
.head 6 -  Set nRetAvgRows = nRowAmount/nCountRow
.head 5 -  Return bReturn
.head 3 +  Function: UpdateSupreme
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 5 -  Boolean: bCommit
.head 5 -  String: sProgramLocation
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtToday
.head 5 -  Number: nMaxSeq
.head 4 +  Actions
.head 5 +  If dtInsDateF = DATETIME_Null
.head 6 -  Set dtToday = Current_Date( '' )
.head 5 +  Else
.head 6 -  Set dtToday =dtInsDateF
.head 5 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	caseyr
			      FROM	cr_supreme
			      WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo and
					type = :sType and
					ins_date = :dtToday and
					line_no = :nLine')
.head 5 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 5 +  If nReturn = FETCH_EOF
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	max(seq)
			     FROM		cr_supreme
			     WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo
			     INTO		:nMaxSeq')
.head 6 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 6 +  If nMaxSeq = NUMBER_Null
.head 7 -  Set nMaxSeq = 0
.head 6 -  Set nMaxSeq = nMaxSeq + 1
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'INSERT INTO	cr_supreme (caseyr, casety, caseno, seq, ins_date, type, line_no, judge, program_location)
			             VALUES		   ( :sCaseYr, :sCaseTy, :sCaseNo, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation)')
			             				
.head 6 +  If bCommit
.head 7 -  Call SqlCommit(hSqlSup)
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  ! Function: ReadRegistryTestUser
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/TestUser', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'TestUser', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: CheckCRS    !Collections
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseTy
.head 5 -  String: fCaseNo
.head 5 -  Sql Handle: hSqlCRS
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nReturnCRS
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSqlCRS, 'SELECT	finish_date
			         FROM	collections
			         WHERE	caseyr = :fCaseYr and
					casety = :fCaseTy and
					caseno = :fCaseNo and
					finish_date is null')
.head 5 -  Call SqlFetchNext( hSqlCRS, nReturnCRS )
.head 5 +  If nReturnCRS = FETCH_EOF
.head 6 -  Return FALSE
.head 5 +  Else
.head 6 -  Return TRUE
.head 3 +  Function: Current_Date
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Parameters
.head 5 -  String: sTimestamp
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Actions
.head 5 +  If sTimestamp = 'Timestamp'
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select sysdate
	from dual into :dCurrentDate' )
.head 6 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  Else
.head 6 -  Set dCurrentDate = SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 )
.head 5 -  Return dCurrentDate
.head 3 +  Function: Workday
.head 4 -  Description: Checks supplied or calulated (+ # days) date for a valid work day
Returns valid work date
.head 4 +  Returns
.head 5 -  Date/Time: dtWork
.head 4 +  Parameters
.head 5 -  Date/Time: dtNow
.head 5 -  Number: nDays
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nDOW	!No od Day of Week 0=Sat, 1=Sun ..6=Fri
.head 5 -  Date/Time: dtTCK	!date To ChecK
.head 5 -  String: sText
.head 5 -  Boolean: bOk
.head 4 +  Actions
.head 5 -  Set dtTCK = dtNow + nDays
.head 5 +  Loop
.head 6 -  Set nDOW = SalDateWeekday( dtTCK )
.head 6 +  If nDOW > 1
.head 7 -  Set sText = ''
.head 7 -  Set bOk =  SqlPrepareAndExecute( hSql, 'select text from calendar  into :sText where holiday = :dtTCK' )
.head 7 +  If sText = ''
.head 8 -  Break
.head 7 +  ! If bOk != FETCH_Ok
.head 8 -  Break
.head 6 -  Set dtTCK = dtTCK + 1
.head 5 -  Call SqlClearImmediate()
.head 5 -  Return dtTCK
.head 3 +  Function: UpdateSupremeCV
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Number: nCaseYr
.head 5 -  String: sCaseTy
.head 5 -  Number: nCaseNo
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 5 -  Boolean: bCommit
.head 5 -  String: sProgramLocation
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtToday
.head 5 -  Number: nMaxSeq
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseNo
.head 4 +  Actions
.head 5 -  Set sCaseYr = SalNumberToStrX( nCaseYr, 0)
.head 5 -  Set sCaseNo = SalNumberToStrX( nCaseNo, 0)
.head 5 +  While SalStrLength( sCaseNo ) < 5
.head 6 -  Set sCaseNo = '0' || sCaseNo
.head 5 +  If dtInsDateF = DATETIME_Null
.head 6 -  Set dtToday = Current_Date( '' )
.head 5 +  Else
.head 6 -  Set dtToday =dtInsDateF
.head 5 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	caseyr
			      FROM	cr_supreme
			      WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo and
					type = :sType and
					ins_date = :dtToday and
					line_no = :nLine')
.head 5 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 5 +  If nReturn = FETCH_EOF
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	max(seq)
			     FROM		cr_supreme
			     WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo
			     INTO		:nMaxSeq')
.head 6 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 6 +  If nMaxSeq = NUMBER_Null
.head 7 -  Set nMaxSeq = 0
.head 6 -  Set nMaxSeq = nMaxSeq + 1
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'INSERT INTO	cr_supreme (caseyr, casety, caseno, seq, ins_date, type, line_no, judge, program_location)
			             VALUES		   ( :sCaseYr, :sCaseTy, :sCaseNo, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation)')
			             				
.head 6 +  If bCommit
.head 7 -  Call SqlCommit(hSqlSup)
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: ExistSupremeCV
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Number: nCaseYr
.head 5 -  String: sCaseTy
.head 5 -  Number: nCaseNo
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 5 -  Boolean: bExcludeUOA
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nReturnSup
.head 5 -  String: sWhere
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseNo
.head 4 +  Actions
.head 5 -  Set sCaseYr = SalNumberToStrX( nCaseYr, 0)
.head 5 -  Set sCaseNo = SalNumberToStrX( nCaseNo, 0)
.head 5 +  While SalStrLength( sCaseNo ) < 5
.head 6 -  Set sCaseNo = '0' || sCaseNo
.head 5 +  If nLine != NUMBER_Null
.head 6 -  Set sWhere = ' and line_no = :nLine '
.head 5 +  If sType != ''
.head 6 -  Set sWhere = sWhere||' and type = :sType '
.head 5 +  If dtInsDateF != DATETIME_Null
.head 6 -  Set sWhere = sWhere||' and ins_date = :dtInsDateF '
.head 5 +  If sJudge != ''
.head 6 -  Set sWhere = sWhere||' and judge = :sJudge '
.head 5 +  If bExcludeUOA
.head 6 -  Set sWhere = sWhere||' and line_no != 11 '
.head 5 -  Call SqlPrepareAndExecute( hSqlSup, 'SELECT		*
			               FROM		cr_supreme
				 WHERE		caseyr = :sCaseYr and
						casety = :sCaseTy and
						caseno = :sCaseNo '||
				    sWhere)
.head 5 -  Call SqlFetchNext( hSqlSup, nReturnSup )
.head 5 +  If nReturnSup = FETCH_Ok
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: GetDestPrinter
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: fReportType
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sComputerName
.head 5 -  String: sReturnDefault
.head 4 +  Actions
.head 5 -  Set sComputerName = SalSysGetComputerName(  )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	printer_name
			       FROM	report_prt_custom
			       WHERE	report_type = :fReportType and
					computername = :sComputerName
			       INTO		:sReturnDefault')
.head 5 -  Call SqlFetchNext( hSql, nReturn)
.head 5 +  If nReturn = FETCH_EOF
.head 6 -  Call SqlPrepareAndExecute( hSql, 'SELECT	printer_name
			       FROM	report_prt_default
			       WHERE	report_type = :fReportType
			       INTO		:sReturnDefault')
.head 6 -  Call SqlFetchNext( hSql, nReturn)
.head 5 -  Return sReturnDefault
.head 3 +  Function: SalSysGetComputerName
.head 4 -  Description: author: ???
date:     ???
version: 1.00
Returns the computername

example:

Set sComputername =  SalSysGetComputerName()
.head 4 +  Returns
.head 5 -  String:
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nSize
.head 5 -  String: sCOMPUTERNAME
.head 4 +  Actions
.head 5 -  Set nSize = 250
.head 5 -  Call SalSetBufferLength( sCOMPUTERNAME, nSize + 1 )
.head 5 -  Call GetComputerNameA( sCOMPUTERNAME, nSize )
.head 5 -  Set sCOMPUTERNAME = SalStrLeftX( sCOMPUTERNAME, nSize )
.head 5 -  Return sCOMPUTERNAME
.head 3 +  Function: DisableField
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Window Handle: hWndDis
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalDisableWindow( hWndDis )
.head 5 -  Call SalColorSet( hWndDis, COLOR_IndexWindowText, COLOR_Black )
.head 5 -  Call SalColorSet( hWndDis, COLOR_IndexWindow, COLOR_LightGray )
.head 3 +  Function: EnableField
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Window Handle: hWndDis
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalEnableWindow( hWndDis )
.head 5 -  Call SalColorSet( hWndDis, COLOR_IndexWindowText, COLOR_Black )
.head 5 -  Call SalColorSet( hWndDis, COLOR_IndexWindow, COLOR_White )
.head 3 +  Function: SalGetItemNameX
.head 4 -  Description: author: 	tl
date:	2000
version:	1.00

.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  Window Handle: phWndItem
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: lsDummy
.head 4 +  Actions
.head 5 -  Call SalGetItemName (phWndItem, lsDummy)
.head 5 -  Return lsDummy
.head 3 +  Function: LoadPDF
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: fFileName
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sApp
.head 4 +  Actions
.head 5 -  ! Call SalLoadApp( , )
.head 5 -  ! Call SalLoadApp( ReadRegistryOpenWith( '.pdf' ),  Image.GetImagePath('CASE', hSql) || dfRCaseYear ||'\\'|| dfRCaseType ||'\\'|| dfRCaseNo  ||'\\ REC_'|| SalNumberToStrX(
dfReprintNo, 0) ||'.pdf')
.head 5 -  Set sApp = ReadRegistryOpenWith( '.pdf' )
.head 5 -  Set sApp = VisStrSubstitute( sApp, '%1', fFileName )
.head 5 -  Call SalLoadApp( sApp, '')
.head 3 +  Function: IsBackup
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sText
.head 4 +  Actions
.head 5 -  ! This function will determine if the user has selected to connect
.head 5 -  ! to a Test database.  The standard database should be named CRIM.  Anything else is going
.head 5 -  ! to be seen as a TEST database.  This function will then change the Window
.head 5 -  ! title of each screen.
.head 5 -  ! !  ! - Will return TRUE if connection is to a backup DB
.head 5 +  If (SalStrUpperX(SqlDatabase) != 'CRIM' and SalStrUpperX(SqlDatabase) != 'CIVIL')
.head 6 -  Call SalGetWindowText( hWndForm, sText, 100 )
.head 6 -  Call SalSetWindowText( hWndForm, sText||' ****Connected to TEST Database - '||SqlDatabase )
.head 6 -  Call SalColorSet( hWndForm, COLOR_IndexWindow, COLOR_Cyan )
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: GetImagePath
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: fType
.head 5 -  Sql Handle: hSqlImage
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nReturnImage
.head 5 -  String: sLocation
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT	path
				   FROM		cr_image_location
				    WHERE	type = :fType
				    INTO		:sLocation')
.head 5 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 5 -  Return sLocation
.head 2 -  Named Exceptions
.head 2 -  Named Toolbars
.head 2 +  Named Menus
.head 3 +  Menu: CTABLE_MENU
.head 4 -  Resource Id: 5490
.head 4 -  Picture File Name:
.head 4 -  Title:
.head 4 -  Description:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Export Table Data
.head 5 -  Resource Id: 5491
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Save to Excel and Load
.head 5 -  Resource Id: 45269
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT_SAVE_LOAD'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Print Table Report
.head 5 -  Resource Id: 45267
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'PRINT_TABLE'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &View Table Report
.head 5 -  Resource Id: 45268
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'VIEW_TABLE'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Copy Selected Rows to Clipboard
.head 5 -  Resource Id: 5492
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Copy All Rows to Clipboard
.head 5 -  Resource Id: 5493
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Find
.head 5 -  Resource Id: 5494
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'FIND'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Auto Size Columns
.head 5 -  Resource Id: 27346
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'AUTO_SIZE'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Popup Menu: Font Setting
.head 5 -  Resource Id: 49277
.head 5 -  Picture File Name:
.head 5 -  Enabled when:
.head 5 -  Status Text:
.head 5 -  Menu Item Name:
.head 5 +  Popup Menu: Font Size
.head 6 -  Resource Id: 49285
.head 6 -  Picture File Name:
.head 6 -  Enabled when:
.head 6 -  Status Text:
.head 6 -  Menu Item Name:
.head 6 +  Menu Item: &6
.head 7 -  Resource Id: 49284
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_6'
.head 8 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &8
.head 7 -  Resource Id: 49283
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_8'
.head 8 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &10
.head 7 -  Resource Id: 49282
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_10'
.head 8 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &12
.head 7 -  Resource Id: 49281
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_12'
.head 8 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &14
.head 7 -  Resource Id: 49280
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_14'
.head 8 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &16
.head 7 -  Resource Id: 49279
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_16'
.head 8 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 5 +  Menu Item: &Font Dialog...
.head 6 -  Resource Id: 49278
.head 6 -  Picture File Name:
.head 6 -  Keyboard Accelerator: (none)
.head 6 -  Status Text:
.head 6 +  Menu Settings
.head 7 -  Enabled when:
.head 7 -  Checked when:
.head 6 +  Menu Actions
.head 7 -  Set sCTABLE_MENU_TYPE = 'FONT_DIALOG'
.head 7 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 6 -  Menu Item Name:
.head 4 +  Menu Item: &Remove Line Colors
.head 5 -  Resource Id: 49286
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'REMOVE_COLOR'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 3 +  Menu: CGRID_MENU
.head 4 -  Resource Id: 62173
.head 4 -  Picture File Name:
.head 4 -  Title:
.head 4 -  Description:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Export Table Data
.head 5 -  Resource Id: 62191
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Save to Excel and Load
.head 5 -  Resource Id: 62190
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT_SAVE_LOAD'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Print Table Report
.head 5 -  Resource Id: 62189
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'PRINT_TABLE'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &View Table Report
.head 5 -  Resource Id: 62188
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'VIEW_TABLE'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Copy Selected Rows to Clipboard
.head 5 -  Resource Id: 62187
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Copy All Rows to Clipboard
.head 5 -  Resource Id: 62186
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Find
.head 5 -  Resource Id: 62185
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'FIND'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Auto Size Columns
.head 5 -  Resource Id: 62184
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'AUTO_SIZE'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Popup Menu: Font Setting
.head 5 -  Resource Id: 62183
.head 5 -  Picture File Name:
.head 5 -  Enabled when:
.head 5 -  Status Text:
.head 5 -  Menu Item Name:
.head 5 +  Popup Menu: Font Size
.head 6 -  Resource Id: 62182
.head 6 -  Picture File Name:
.head 6 -  Enabled when:
.head 6 -  Status Text:
.head 6 -  Menu Item Name:
.head 6 +  Menu Item: &6
.head 7 -  Resource Id: 62181
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_6'
.head 8 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &8
.head 7 -  Resource Id: 62180
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_8'
.head 8 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &10
.head 7 -  Resource Id: 62179
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_10'
.head 8 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &12
.head 7 -  Resource Id: 62178
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_12'
.head 8 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &14
.head 7 -  Resource Id: 62177
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_14'
.head 8 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 6 +  Menu Item: &16
.head 7 -  Resource Id: 62176
.head 7 -  Picture File Name:
.head 7 -  Keyboard Accelerator: (none)
.head 7 -  Status Text:
.head 7 +  Menu Settings
.head 8 -  Enabled when:
.head 8 -  Checked when:
.head 7 +  Menu Actions
.head 8 -  Set sCTABLE_MENU_TYPE = 'FONT_SIZE_16'
.head 8 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 7 -  Menu Item Name:
.head 5 +  Menu Item: &Font Dialog...
.head 6 -  Resource Id: 62175
.head 6 -  Picture File Name:
.head 6 -  Keyboard Accelerator: (none)
.head 6 -  Status Text:
.head 6 +  Menu Settings
.head 7 -  Enabled when:
.head 7 -  Checked when:
.head 6 +  Menu Actions
.head 7 -  Set sCTABLE_MENU_TYPE = 'FONT_DIALOG'
.head 7 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 6 -  Menu Item Name:
.head 4 +  Menu Item: &Remove Line Colors
.head 5 -  Resource Id: 62174
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'REMOVE_COLOR'
.head 6 -  Call SalSendMsg( hWndTable, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 3832953711
0000: 9E0000008F000000 0000000000000000 0200000200FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F19003C00 00FFFE00FF084300
0040: 72004400006F0063 006B006500007400 2200000001003C00 0019000500010210
0060: F03708000000FF06 7E00C02100040000 0010018019000001 001100FFFE056300
0080: 004A00610069006C 0066220001006F19 00F10402100200FF 065A003600340400
00A0: 00
.enddata
.head 3 +  Data Field Class: cCaseYr
.head 4 -  Data
.head 5 -  Maximum Data Length: 4
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalStrLength( MyValue ) = 1
.head 7 -  Set MyValue = '200' || MyValue
.head 6 +  If SalStrLength( MyValue ) = 2
.head 7 +  If MyValue > '49'
.head 8 -  Set MyValue = '19' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  Else
.head 8 -  Set MyValue = '20' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If SalStrLength( MyValue ) = 2 and (MyValue != '19' and MyValue !='20')
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'CRA'
.head 6 +  Else If MyValue = 'B'
.head 7 -  Set MyValue = 'CRB'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'TRC'
.head 6 +  Else If MyValue = 'D'
.head 7 -  Set MyValue = 'TRD'
.head 6 -  If MyValue = 'CRA' OR MyValue = 'CRB'
.head 6 -  Else If MyValue = 'TRC' OR MyValue = 'TRD'
.head 6 -  Else If MyValue = 'N'
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseVTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'CRA'
.head 6 +  Else If MyValue = 'B'
.head 7 -  Set MyValue = 'CRB'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'TRC'
.head 6 +  Else If MyValue = 'D'
.head 7 -  Set MyValue = 'TRD'
.head 6 -  If MyValue = 'CRA' OR MyValue = 'CRB' or MyValue = 'TRC' or MyValue = 'TRD' or
	MyValue = 'VBD' OR MyValue = 'VBB' or MyValue = 'N'
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseNo
.head 4 -  Data
.head 5 -  Maximum Data Length: 5
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.8"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If Not SalIsNull( MyValue )
.head 7 +  While SalStrLength( MyValue ) < 5
.head 8 -  Set MyValue = '0' || MyValue
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cDate
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_Backslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ('' )
.head 7 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If wParam = Key_Up
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue + 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 6 +  Else If wParam = Key_Down
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue - 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 3 +  Data Field Class: cDateDOB
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalDateYear( MyValue ) > SalDateYear( SalDateCurrent(  ) )
.head 7 -  Set MyValue = SalDateConstruct( SalDateYear( MyValue ) - 100, SalDateMonth( MyValue ), SalDateDay( MyValue ), SalDateHour( MyValue ), SalDateMinute( MyValue ), 0 )
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  ! If MyValue > SalDateCurrent( ) - 6480
.head 8 +  If IDOK =  SalMessageBox( 'Birth Date is Less than 18 Years, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else 
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  If MyValue > SalDateConstruct( SalDateYear( SalDateCurrent( ) ) - 18, SalDateMonth( SalDateCurrent( ) ), SalDateDay( SalDateCurrent( ) ), SalDateHour( SalDateCurrent( ) ), SalDateMinute( SalDateCurrent( ) ), SalDateSecond( SalDateCurrent( ) ) )
.head 8 -  If IDOK =  SalMessageBox( 'Birth Date is Less than 18 Years, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 3 +  Data Field Class: cLoginUser
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: String
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Unformatted
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Window Handle: hWndNext
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Read )
.head 7 -  Call SalFileRead( hFLogin, sLogIn, 9 )
.head 7 -  Set SqlUser = SalStrTrimX( SalStrLeftX( sLogIn, 9 ) )
.head 7 -  Call SalFileClose( hFLogin )
.head 6 -  ! Set MyValue = SqlUser
.head 6 +  ! If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 6 -  Set SqlUser = Startup.sLastUser_S
.head 6 -  Set MyValue = SqlUser
.head 6 +  If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 3 +  Functional Class: CrDocket
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 4 +  Functions
.head 5 +  Function: D_Insert_Docket
.head 6 -  Description: Adds the Requested Data to the Cr_Docket
	- String (4):		Case No
	- String (3):		Case Ty
	- String (5):		Case Yr
	- Date/Time:	Docket Date
	- String (6):		Docket Code
	- String (250):	Docket Data
	- Number:		Seq (If NULL, will find max seq from Docket)
	- Number:		Seq2 (Always NULL, except on charge entry)
	- String:		Variable Mask
	- String:		Commit confirmation
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  Date/Time: DDate
.head 7 -  String: DCode
.head 7 -  String: DData
.head 7 -  Number: nSeq
.head 7 -  Number: nSeq2
.head 7 -  String: DMask
.head 7 -  String: sCommit
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnSeq
.head 6 +  Actions
.head 7 -  Set nReturnSeq =  D_Decode_Mask( DCaseYr, DCaseTy, DCaseNo, DCode, DMask)
.head 7 +  If nReturnSeq != 0
.head 8 -  Set nSeq = nReturnSeq
.head 7 +  If nSeq = 0 or nSeq = NUMBER_Null
.head 8 -  Call SalMessageBox( 'Warning: ' || DCode || ' Docket Code is being inserted without a sequence number', 'Docket Warning', MB_IconInformation | MB_Ok )
.head 7 +  If SalStrLength( DData ) > 250
.head 8 -  Call SalMessageBox( 'Warning - Docket Data is too large for column of 250 characters.  Docket Entry will be truncated.  Please Review.
ORIGINAL - ' || DData ||'
TRUNCATED - '||  SalStrLeftX( DData, 249 ) ||'~', 'Docket Warning', MB_IconInformation | MB_Ok )
.head 8 -  Set DData = SalStrLeftX( DData, 249 ) ||'~'
.head 7 -  Call SqlPrepareAndExecute( hSql, 'INSERT INTO	cr_docket
						(caseyr, casety, caseno, dock_date, seq,
						 seq2 ,casecode, data, userid, ratnum,Accesskey)
				VALUES		(:DCaseYr, :DCaseTy, :DCaseNo, :DDate, :nSeq,
						 :nSeq2, :DCode, :DData, :nUserId, cr_docket_ratnum_seq.nextval,sysdate )')
.head 7 +  If sCommit != 'N'
.head 8 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Docket_Exists
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute (hSql, 'SELECT	caseno
			      FROM		cr_docket '||fWhere)
.head 7 -  Call SqlFetchNext( hSql, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: D_Delete_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSql,'DELETE		cr_docket '||fWhere)
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Update_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 7 -  String: fDData
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSql,'UPDATE	cr_docket
			    SET		data =:fDData '||fWhere)
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Get_Max_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 6 +  Static Variables
.head 7 -  Number: fReturnSeq
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	max(seq)
			             FROM	cr_docket
			             WHERE	caseno =:DCaseNo and
					casety =:DCaseTy and
					caseyr =:DCaseYr
			              INTO	:fReturnSeq' )
.head 7 +  If Not SqlFetchNext( hSql, nReturn )
.head 8 -  Set fReturnSeq = 1
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	seq
			     FROM		new_codes
			     INTO		:fReturnSeq
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Decode_Mask
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: DCode
.head 7 -  String: DMask
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 +  ! If DMask = ''
.head 8 -  Set fReturnSeq = D_Get_Max_Seq( DCaseNo, DCaseTy, DCaseYr)
.head 8 -  Set fReturnSeq = fReturnSeq + 4
.head 7 +  If DMask = '#'
.head 8 -  Set fReturnSeq = D_Get_Seq(DCode)
.head 7 +  Else If DMask = ''
.head 8 -  Set fReturnSeq = fReturnSeq
.head 7 +  Else
.head 8 -  Set DMask = SalStrReplaceX( DMask, 0, 1, '')
.head 8 +  If SalStrIsValidNumber( DMask )
.head 9 +  If SalStrToNumber(DMask) > 0
.head 10 -  Set fReturnSeq = D_Get_Max_Seq( DCaseYr, DCaseTy, DCaseNo)
.head 10 -  Set fReturnSeq = fReturnSeq + SalStrToNumber(DMask)
.head 8 +  Else
.head 9 -  Call SalMessageBox('Error reading Mask string', 'Invalid Mask', MB_Ok|MB_IconExclamation)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Data
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	description
			     FROM		new_codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnData
.head 5 +  Function: D_Get_Costs
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	costs
			     FROM		new_codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnData
.head 3 +  Data Field Class: cDateHoliday
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  If HolidayCheck(MyValue)
.head 8 -  Call SalSetFocus(MyValue)
.head 7 +  Else
.head 8 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_Backslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ('' )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If wParam = Key_Up
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue + 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 6 +  Else If wParam = Key_Down
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue - 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 3 +  Child Table Class: CTable
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.2"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.833"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 8
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? Class Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 100000
.head 5 -  Discardable? No
.head 4 -  Next Class Child Key: 0
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? Class Default
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Description:
.head 4 +  Derived From
.head 5 -  Class: CWindow
.head 4 -  Contents
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Boolean: bDisableC
.head 5 -  ! ! !! << EXPORT VAR >>
.head 5 -  String: fGlobTitle
.head 5 -  String: fGlobFileName
.head 4 +  Functions
.head 5 +  Function: SAM_CreateTBL
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call MTblSubClass( hWndForm )
.head 7 -  Call MTblEnableMWheelScroll( hWndForm, TRUE )
.head 7 +  If NOT bDisableC
.head 8 -  Call SalTblDefineRowHeader( hWndForm, '?', 20, 41, hWndNULL )
.head 7 -  Set hWndTable = hWndForm
.head 7 -  Call SalTblSetTableFlags( hWndForm, TBL_Flag_SelectableCols, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_TEXT, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_COMPLETETEXT, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_IMAGE, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_BTN, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_COLHDR, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_COLHDRGRP, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_ROWHDR, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CORNER, TRUE )
.head 7 -  Call MTblSetTipOpacity( hWndForm, TBL_Error, MTBL_TIP_DEFAULT, 220 )
.head 7 -  Call MTblSetTipFadeInTime( hWndForm, TBL_Error, MTBL_TIP_DEFAULT, 150 )
.head 5 +  Function: PrintTable
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Window Handle: hWndTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nError
.head 7 -  Number: nParamArray[*]
.head 6 +  Actions
.head 7 -  Call SalReportTableCreate( 'PrnTbl.qrp', hWndTable, nError )
.head 7 +  If fType = 'PRINTER'
.head 8 -  Call SalReportTablePrint( hWndTable, 'PrnTbl.qrp', nParamArray , nError )
.head 7 +  Else If fType = 'SCREEN'
.head 8 -  Call SalReportTableView( hWndTable, hWndNULL, 'PrnTbl.qrp' , nError )
.head 7 -  Call VisFileDelete(  'PrnTbl.qrp' )
.head 5 +  Function: AutoSize
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bFound
.head 7 -  Number: nColID
.head 7 -  Window Handle: hWndColumn
.head 6 +  Actions
.head 7 -  Set bFound = FALSE
.head 7 -  Set nColID = 1
.head 7 -  Set hWndColumn = SalTblGetColumnWindow( hTable, nColID, COL_GetID )
.head 7 +  While hWndColumn
.head 8 +  If SalTblQueryColumnFlags( hWndColumn, COL_Selected )
.head 9 -  Call VisTblAutoSizeColumn( hTable, hWndColumn )
.head 9 -  Set bFound = TRUE
.head 9 -  Break
.head 8 -  Set nColID = nColID + 1
.head 8 -  Set hWndColumn = SalTblGetColumnWindow( hTable, nColID, COL_GetID )
.head 7 +  If Not bFound
.head 8 -  Call VisTblAutoSizeColumn( hTable, hWndNULL )
.head 5 +  Function: ChangeFont
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 7 -  Number: nNewFont
.head 7 -  String: fType
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sFont
.head 7 -  Number: nSize
.head 7 -  Number: nEnh
.head 7 -  Number: nColor
.head 6 +  Actions
.head 7 +  If fType = 'DIALOG'
.head 8 +  If SalDlgChooseFont( hWndForm, sFont, nSize, nEnh, nColor )
.head 9 -  Call SalFontSet( hTable, sFont, nSize, nEnh )
.head 7 +  Else If fType = 'FONT_SIZE'
.head 8 -  Call SalFontSet( hTable, 'Arial', nNewFont, 0 )
.head 5 +  Function: RemoveColor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nRow 
.head 7 -  Boolean: bOk
.head 6 +  Actions
.head 7 -  Call SalWaitCursor( TRUE )
.head 7 -  Set nRow = 0
.head 7 -  Set bOk = SalTblSetContext( hTable, nRow)
.head 7 +  While bOk
.head 8 -  Call MTblSetRowBackColor( hTable, nRow, COLOR_White, MTSC_REDRAW)
.head 8 -  Call Inc(nRow)
.head 8 -  Set bOk = SalTblSetContext( hTable, nRow)
.head 7 -  Call SalWaitCursor( FALSE )
.head 4 +  Message Actions
.head 5 +  On SAM_CacheFull
.head 6 -  Call AddMsgToLog( "SAM_CacheFull", wParam, lParam )
.head 5 +  On SAM_ColumnSelectClick
.head 6 -  Call AddMsgToLog( "SAM_ColumnSelectClick", wParam, lParam )
.head 5 +  On SAM_CornerClick
.head 6 -  Call AddMsgToLog( "SAM_CornerClick", wParam, lParam )
.head 6 +  If NOT bDisableC
.head 7 -  Call SalTrackPopupMenu(hWndForm, 'CTABLE_MENU', TPM_CursorX | TPM_CursorY, 0, 0 )
.head 5 +  On SAM_CornerDoubleClick
.head 6 -  Call AddMsgToLog( "SAM_CornerDoubleClick", wParam, lParam )
.head 5 +  On SAM_CountRows
.head 6 -  Call AddMsgToLog( "SAM_CountRows", wParam, lParam )
.head 5 +  On SAM_EndCellTab
.head 6 -  Call AddMsgToLog( "SAM_EndCellTab", wParam, lParam )
.head 5 +  On SAM_FetchDone
.head 6 -  Call AddMsgToLog( "SAM_FetchDone", wParam, lParam )
.head 5 +  On SAM_FetchRow
.head 6 -  Call AddMsgToLog( "SAM_FetchRow", wParam, lParam )
.head 5 +  On SAM_FetchRowDone
.head 6 -  Call AddMsgToLog( "SAM_FetchRowDone", wParam, lParam )
.head 5 +  On SAM_RowSetContext
.head 6 -  Call AddMsgToLog( "SAM_RowSetContext", wParam, lParam )
.head 5 +  On SAM_RowValidate
.head 6 -  Call AddMsgToLog( "SAM_RowValidate", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_First
.head 6 -  Call AddMsgToLog( "MTM_First", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_AreaLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_AreaLBtnDown", wParam, lParam )
.head 5 +  On MTM_AreaLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_AreaLBtnUp", wParam, lParam )
.head 5 +  On MTM_AreaLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_AreaLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_AreaRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_AreaRBtnDown", wParam, lParam )
.head 5 +  On MTM_AreaRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_AreaRBtnUp", wParam, lParam )
.head 5 +  On MTM_AreaRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_AreaRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_CornerLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_CornerLBtnDown", wParam, lParam )
.head 5 +  On MTM_CornerLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_CornerLBtnUp", wParam, lParam )
.head 5 +  On MTM_CornerLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_CornerLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_CornerRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_CornerRBtnDown", wParam, lParam )
.head 5 +  On MTM_CornerRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_CornerRBtnUp", wParam, lParam )
.head 5 +  On MTM_CornerRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_CornerRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_ColHdrLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrLBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrLBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_ColHdrRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrRBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrRBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_ColHdrSepLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepLBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrSepLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepLBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrSepLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepLBtnDblClk", wParam, lParam )
.head 6 -  Call MTblAutoSizeColumn( hWndForm, SalNumberToWindowHandle( wParam ), MTASC_SPLITROWS )
.head 5 +  On MTM_ColHdrSepRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepRBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrSepRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepRBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrSepRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_RowHdrLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_RowHdrLBtnDown", wParam, lParam )
.head 5 +  On MTM_RowHdrLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_RowHdrLBtnUp", wParam, lParam )
.head 5 +  On MTM_RowHdrLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_RowHdrLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_RowHdrRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_RowHdrRBtnDown", wParam, lParam )
.head 5 +  On MTM_RowHdrRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_RowHdrRBtnUp", wParam, lParam )
.head 5 +  On MTM_RowHdrRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_RowHdrRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_SplitBarLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_SplitBarLBtnDown", wParam, lParam )
.head 5 +  On MTM_SplitBarLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_SplitBarLBtnUp", wParam, lParam )
.head 5 +  On MTM_SplitBarLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_SplitBarLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_SplitBarRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_SplitBarRBtnDown", wParam, lParam )
.head 5 +  On MTM_SplitBarRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_SplitBarRBtnUp", wParam, lParam )
.head 5 +  On MTM_SplitBarRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_SplitBarRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_ColMoved
.head 6 -  Call AddMsgToLog( "MTM_ColMoved", wParam, lParam )
.head 5 +  On MTM_ColSized
.head 6 -  Call AddMsgToLog( "MTM_ColSized", wParam, lParam )
.head 5 +  On MTM_Reset
.head 6 -  Call AddMsgToLog( "MTM_Reset", wParam, lParam )
.head 5 +  On MTM_RowDeleted
.head 6 -  Call AddMsgToLog( "MTM_RowDeleted", wParam, lParam )
.head 5 +  On MTM_RowInserted
.head 6 -  Call AddMsgToLog( "MTM_RowInserted", wParam, lParam )
.head 5 +  On MTM_RowsSwapped
.head 6 -  Call AddMsgToLog( "MTM_RowsSwapped", wParam, lParam )
.head 5 +  On MTM_SplitBarMoved
.head 6 -  Call AddMsgToLog( "MTM_SplitBarMoved", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_CollapseRow
.head 6 -  Call AddMsgToLog( "MTM_CollapseRow", wParam, lParam )
.head 5 +  On MTM_CollapseRowDone
.head 6 -  Call AddMsgToLog( "MTM_CollapseRowDone", wParam, lParam )
.head 5 +  On MTM_ExpandRow
.head 6 -  Call AddMsgToLog( "MTM_ExpandRow", wParam, lParam )
.head 5 +  On MTM_ExpandRowDone
.head 6 -  Call AddMsgToLog( "MTM_ExpandRowDone", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_QuerySortValue
.head 6 -  Return SalSendMsg( SalNumberToWindowHandle( wParam ), MTM_QuerySortValue, 0, lParam )
.head 5 -  !
.head 5 +  On MTM_BtnClick
.head 6 -  Call AddMsgToLog( "MTM_BtnClick", wParam, lParam )
.head 5 +  On MTM_HyperlinkClick
.head 6 -  Call AddMsgToLog( "MTM_HyperlinkClick", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_Paint
.head 6 -  Call AddMsgToLog( "MTM_Paint", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_LoadChildRows
.head 6 -  Call AddMsgToLog( "MTM_LoadChildRows", wParam, lParam )
.head 5 +  On SAM_User
.head 6 +  If sCTABLE_MENU_TYPE = 'EXPORT'
.head 7 -  Call SalModalDialog( dlgExport, hWndForm, hWndItem, fGlobTitle, fGlobFileName, '', '', '')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'EXPORT_SAVE_LOAD'
.head 7 -  Call SalModalDialog( dlgExport, hWndForm, hWndItem, fGlobTitle, fGlobFileName, 'EXCEL', '', '')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'PRINT_TABLE'
.head 7 -  Call PrintTable( 'PRINTER' , hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'VIEW_TABLE'
.head 7 -  Call PrintTable( 'SCREEN' , hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 7 -  Call CopyRows( 1, hWndItem )
.head 6 +  Else If sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 7 -  Call CopyRows( 2, hWndItem )
.head 6 +  Else If sCTABLE_MENU_TYPE = 'FIND'
.head 7 -  Call SalCreateWindow( frmCTable_Find, hWndForm, hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'AUTO_SIZE'
.head 7 -  Call AutoSize( hWndItem  )
.head 6 +  Else If SalStrLeftX( sCTABLE_MENU_TYPE, 9) = 'FONT_SIZE'
.head 7 -  Call ChangeFont( hWndItem, SalStrToNumber( VisStrSubstitute( sCTABLE_MENU_TYPE, 'FONT_SIZE_', '' ) ), 'FONT_SIZE')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'FONT_DIALOG'
.head 7 -  Call ChangeFont( hWndItem, 0, 'DIALOG')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'REMOVE_COLOR'
.head 7 -  Call RemoveColor( hWndItem )
.head 3 +  General Window Class: CWindow
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 +  Functions
.head 5 +  Function: AddMsgToLog
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: spMsg
.head 7 -  Number: npwParam
.head 7 -  Number: nplParam
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sItemName
.head 6 +  Actions
.head 7 +  ! If frmMain.whWndMsgLog
.head 8 -  Call SalGetItemName( hWndItem, sItemName )
.head 8 -  Return dlgMsgLog.AddToLog( spMsg || "[" || sItemName || "], wParam: " || SalNumberToStrX( npwParam, 0 ) || ", lParam: " || SalNumberToStrX( nplParam, 0 ) )
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call AddMsgToLog( "SAM_Click", wParam, lParam )
.head 5 +  On SAM_ContextMenu
.head 6 -  Call AddMsgToLog( "SAM_ContextMenu", wParam, lParam )
.head 5 +  On SAM_DoubleClick
.head 6 -  Call AddMsgToLog( "SAM_DoubleClick", wParam, lParam )
.head 5 +  On SAM_DropDown
.head 6 -  Call AddMsgToLog( "SAM_DropDown", wParam, lParam )
.head 5 +  On SAM_KillFocus
.head 6 -  Call AddMsgToLog( "SAM_KillFocus", wParam, lParam )
.head 5 +  On SAM_SetFocus
.head 6 -  Call AddMsgToLog( "SAM_SetFocus", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_KeyDown
.head 6 -  Call AddMsgToLog( "MTM_KeyDown", wParam, lParam )
.head 5 -  !
.head 5 +  ! On WM_KEYDOWN
.head 6 -  Call AddMsgToLog( "WM_KEYDOWN", wParam, lParam )
.head 5 +  ! On WM_LBUTTONDOWN
.head 6 -  Call AddMsgToLog( "WM_LBUTTONDOWN", wParam, lParam )
.head 5 +  ! On WM_LBUTTONUP
.head 6 -  Call AddMsgToLog( "WM_LBUTTONUP", wParam, lParam )
.head 3 +  Column Class: CColumn
.head 4 -  Title:
.head 4 -  Visible? Class Default
.head 4 -  Editable? Class Default
.head 4 -  Maximum Data Length: Class Default
.head 4 -  Data Type: Class Default
.head 4 -  Justify: Class Default
.head 4 -  Width:  Class Default
.head 4 -  Width Editable? Class Default
.head 4 -  Format: Class Default
.head 4 -  Country: Class Default
.head 4 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Cell Options
.head 5 -  Cell Type? Class Default
.head 5 -  Multiline Cell? Class Default
.head 5 -  Cell DropDownList
.head 6 -  Sorted? Class Default
.head 6 -  Vertical Scroll? Class Default
.head 6 -  Auto Drop Down? Class Default
.head 6 -  Allow Text Editing? Class Default
.head 5 -  Cell CheckBox
.head 6 -  Check Value:
.head 6 -  Uncheck Value:
.head 6 -  Ignore Case? Class Default
.head 4 -  ToolTip:
.head 4 -  Column Aggregate Type: Class Default
.head 4 -  Flow Direction: Class Default
.head 4 -  Description:
.head 4 +  Derived From
.head 5 -  Class: CEditWindow
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Boolean: bSortTblCol
.head 4 +  Functions
.head 5 +  Function: OnQuerySortValue
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dt
.head 7 -  Number: nColType
.head 7 -  Number: nDataType
.head 7 -  String: sText
.head 6 +  Actions
.head 7 +  If SalTblQueryColumnCellType( hWndItem, nColType )
.head 8 +  If nColType = COL_CellType_DropDownList
.head 9 -  Return SalListQuerySelection( hWndItem )
.head 7 -  !
.head 7 -  Set nDataType = SalGetDataType( hWndItem )
.head 7 -  Call SalTblGetColumnText( hWndForm, SalTblQueryColumnID( hWndItem ), sText )
.head 7 +  Select Case nDataType
.head 8 +  Case DT_String
.head 9 -  Return SalStrLength( sText )
.head 8 +  Case DT_DateTime
.head 9 -  Set dt = SalStrToDate( sText )
.head 9 -  Return dt - 0000-01-01
.head 8 +  Case DT_Number
.head 9 -  Return SalStrToNumber( sText )
.head 8 +  Default
.head 9 -  Return 0
.head 4 -  List Values
.head 4 +  Message Actions
.head 5 +  On MTM_QuerySortValue
.head 6 -  Return OnQuerySortValue( lParam )
.head 5 +  On SAM_ColumnSelectClick
.head 6 +  If bSortTblCol = FALSE
.head 7 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( hWndItem ), TBL_SortIncreasing )
.head 7 -  Set bSortTblCol = TRUE
.head 6 +  Else
.head 7 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( hWndItem ), TBL_SortDecreasing )
.head 7 -  Set bSortTblCol =FALSE
.head 3 +  General Window Class: CEditWindow
.head 4 -  Description:
.head 4 +  Derived From
.head 5 -  Class: CWindow
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 -  Call AddMsgToLog( "SAM_AnyEdit", wParam, lParam )
.head 5 +  On SAM_Validate
.head 6 -  Call AddMsgToLog( "SAM_Validate", wParam, lParam )
.head 3 +  Functional Class: cImage
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  ! String: sImageFileName
.head 5 -  String: sPrtDeviceSave
.head 5 -  String: sPrtDriverSave
.head 5 -  String: sPrtPortSave
.head 5 -  String: sScriptFileName
.head 5 -  String: sFileSaveName
.head 5 -  String: sFileSavePath
.head 4 +  Functions
.head 5 +  Function: Construct
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fFileName
.head 7 -  String: fDirName
.head 7 -  Boolean: bSetToDefault   !If the qrp needs to print to default, this will set TIFF to the default
.head 7 -  Boolean: bOverwrite  !Usually true, if False, will append
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bSetToDefault
.head 8 -  Call SalPrtGetDefault( sPrtDeviceSave, sPrtDriverSave, sPrtPortSave )
.head 8 -  Call SalPrtSetDefault( 'TIFF', 'winspool', 'NUL' )
.head 8 -  ! Version 8 below
.head 8 -  ! Call SalPrtSetDefault( 'TIFF', 'winspool', 'PNTIF8' )
.head 7 -  Set sScriptFileName = 'PNTIF9S.INI'
.head 7 -  ! Version 8 below
.head 7 -  ! Set sScriptFileName = 'PNETIF8S.INI'
.head 7 -  !
.head 7 +  If NOT CreateINI( fFileName, fDirName, bOverwrite)
.head 8 -  Call SalMessageBox( 'Errors occurred while creating INI File.', 'Error', MB_Ok)
.head 5 +  Function: Destruct
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bSetToDefault   !If the qrp needs to print to default, this will set TIFF to the default
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bSetToDefault
.head 8 -  Call SalPrtSetDefault( sPrtDeviceSave, sPrtDriverSave, sPrtPortSave )
.head 5 +  Function: CreateINI
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: fFileName
.head 7 -  String: fDirectory
.head 7 -  Boolean: bOverwrite  !Usually true, if False, will append
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sTempDir
.head 7 -  String: sOverwrite
.head 7 -  String: sAppend
.head 6 +  Actions
.head 7 -  ! Get Temp Directory
.head 7 -  Set sTempDir = VisDosGetEnvString ('TEMP')
.head 7 -  ! Write Script Strings
.head 7 +  If sTempDir = ''
.head 8 -  Call SalMessageBox( 'Error in retrieving user temp directory - eTiff Creation failed to write ini file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Output directory', fDirectory, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Output directory

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Output filename', fFileName, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Output filename

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Prompt', '0', sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Prompt

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If bOverwrite
.head 8 -  Set sOverwrite = '1'
.head 8 -  Set sAppend = '0'
.head 7 +  Else
.head 8 -  Set sOverwrite = '0'
.head 8 -  Set sAppend = '1'
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Overwrite',sOverwrite, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Overwrite

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Append', sAppend, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Append

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 -  Return TRUE
.head 5 +  Function: GetImagePath
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Sql Handle: hSqlImage
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnImage
.head 7 -  String: sLocation
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT	path
				   FROM		cr_image_location
				    WHERE	type = :fType
				    INTO		:sLocation')
.head 7 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 7 -  Return sLocation
.head 5 +  Function: MoveFiles
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: fPath1
.head 7 -  String: fPath2
.head 7 -  Boolean: bShowErrors ! Report errors interactively during processing.  If FALSE, function will only return a TRUE or FALSE when complete.
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sPath1
.head 7 -  String: sPath2
.head 7 -  Number: nReturn
.head 7 -  Number: nFilesCopied
.head 6 +  Actions
.head 7 +  If SalStrRightX( fPath1, 1 ) != '\\'
.head 8 -  Set fPath1 = fPath1 ||'\\'
.head 7 +  If SalStrRightX( fPath2, 1 ) != '\\'
.head 8 -  Set fPath2 = fPath2 ||'\\'
.head 7 +  If NOT VisDosExist( fPath1 )
.head 8 +  If bShowErrors
.head 9 -  Call SalMessageBox( 'Original Path - '|| fPath1 ||' does not exist.  Please verify and try again.', 'Path not found', MB_IconStop|MB_Ok)
.head 8 -  Return -1
.head 7 +  If VisDosExist( fPath2 )
.head 8 +  If bShowErrors
.head 9 -  If SalMessageBox( 'Copy to Path - '|| fPath2 ||' already exists.
If you continue, you will overwrite any files within this directory that have the same name as the Original directory  Would you like to continue coping?', 'Copy to Path already exists',
MB_IconQuestion|MB_YesNo) = IDYES
.head 9 +  Else
.head 10 -  Return -2
.head 7 +  Else
.head 8 +  If NOT SalFileCreateDirectory( fPath2 )
.head 9 +  If bShowErrors
.head 10 -  Call SalMessageBox( 'An error occurred while creating Copy to path. Please contact suprevisor.', 'Copy to directory not created', MB_IconStop|MB_Ok)
.head 9 -  Return -3
.head 7 -  ! Set sCommand = 'c:\\windows\\system32\\xcopy.exe '|| fPath1 ||' '|| fPath2 ||' /Y/D/E/I/C//K'
.head 7 -  ! 
xcopy E:\MyDocu~1\email G:\Backup\MyDocuments\email /Y/D/E
.head 7 -  ! If SalLoadAppAndWait( sCommand, Window_NotVisible, nReturn )
.head 7 -  Set sPath1 = fPath1||'*.*'
.head 7 -  Set sPath2 = fPath2||'*.*'
.head 7 -  Set nFilesCopied = VisFileCopy(sPath1, fPath2 )
.head 7 +  If nFilesCopied < 1
.head 8 +  If bShowErrors
.head 9 -  Call SalMessageBox( 'An error occurred while coping files, or no files exist in Original directory.  Please contact suprevisor.', 'Files not copied', MB_IconStop|MB_Ok)
.head 8 -  Return -4
.head 7 +  Else
.head 8 -  Set nReturn = VisFileDelete( sPath1 )
.head 8 +  If nReturn < 1
.head 9 +  If bShowErrors
.head 10 -  Call SalMessageBox( 'An error occurred while deleting files from Original directory.  Please contact suprevisor.', 'Files not deleted', MB_IconStop|MB_Ok)
.head 9 -  Return -5
.head 8 +  If SalFileRemoveDirectory(  fPath1 ) = FALSE
.head 9 +  If bShowErrors
.head 10 -  Call SalMessageBox( 'An error occurred while removing Original directory.  Please contact suprevisor.', 'Original Direcotry not removed', MB_IconStop|MB_Ok)
.head 9 -  Return -6
.head 8 +  Else
.head 9 -  Return nFilesCopied
.head 5 +  Function: InsertLog
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Sql Handle: hSqlImage
.head 7 -  String: fCaseYr
.head 7 -  String: fCaseTy
.head 7 -  String: fCaseNo
.head 7 -  String: fOriginalPath
.head 7 -  String: fNewPath
.head 7 -  Number: fErrorCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nNumCopied
.head 6 +  Actions
.head 7 +  If fErrorCode > 0
.head 8 -  Set nNumCopied = fErrorCode
.head 8 -  Set fErrorCode = NUMBER_Null
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'INSERT INTO	cr_image_log
						(caseyr,casety, caseno, original_path, new_path, error_code, num_file_copied)
				     values	(:fCaseYr, :fCaseTy, :fCaseNo, :fOriginalPath, :fNewPath, :fErrorCode, :nNumCopied)')
.head 7 -  Call SqlCommit( hSqlImage )
.head 3 +  Functional Class: cImagePDF
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sFileSaveName
.head 5 -  String: sFileSavePath
.head 4 +  Functions
.head 5 +  Function: Construct
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fFileName
.head 7 -  String: fDirName
.head 7 -  Boolean: bOverwrite  !Usually true, if False, will append
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sFileSaveName = fFileName
.head 7 -  Set sFileSavePath = fDirName
.head 7 +  If VisDosExist( sFileSavePath||sFileSaveName )
.head 8 -  If bOverwrite  !! Do nothing
.head 8 +  Else
.head 9 -  Set sFileSaveName = sFileSaveName || SalFmtFormatDateTime( SalDateCurrent(  ), 'MM-dd-yyyy-hh.mm.ss AMPM' ) || '.pdf'
.head 5 +  Function: GetImagePath
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Sql Handle: hSqlImage
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnImage
.head 7 -  String: sLocation
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT	path
				   FROM		cr_image_location
				    WHERE	type = :fType
				    INTO		:sLocation')
.head 7 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 7 -  Return sLocation
.head 5 +  Function: CreatePath
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnErr
.head 6 +  Actions
.head 7 +  If NOT VisDosExist( sFileSavePath )
.head 8 -  Set nReturnErr = VisDosMakeAllDir( sFileSavePath ) 
.head 7 +  If nReturnErr != VTERR_Ok
.head 8 -  Call SalMessageBox( 'Error creating the following directory:
'||  sFileSavePath ||' for file '|| sFileSaveName ||'
Contact MIS', 'Error, can not save PDF', MB_Ok| MB_IconStop)
.head 8 -  Return FALSE
.head 7 -  Return TRUE
.head 3 +  Functional Class: cStartup
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sLastUser_S
.head 5 -  String: sCourtTitle_S
.head 5 -  String: sTestUser_S
.head 5 -  String: sTestDB_S
.head 5 -  String: sSecurePC_S
.head 5 -  String: sDB_S
.head 5 -  String: sSelect_S
.head 5 -  String: sPublic_S
.head 5 -  String: sProgramINI
.head 5 -  String: sProgramINI_Blank
.head 5 -  String: sRemoteUser  !!! USED FOR ONCALL , INI SET REMOTE = TRUE
.head 4 +  Functions
.head 5 +  Function: GetStartupParam
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sReturn
.head 6 +  Actions
.head 7 -  ! Set sProgramINI_Blank = 'O:\\admin_3.1\\blank.ini'
.head 7 -  Set sProgramINI_Blank = 'O:\\apps\\5.2\\admin\\blank.ini'
.head 7 -  Set sProgramINI = VisDosGetEnvString ('USERPROFILE')
.head 7 -  Set sProgramINI = sProgramINI||'\\crim.ini'
.head 7 +  If NOT VisDosExist( sProgramINI )
.head 8 -  Call VisFileCopy( sProgramINI_Blank, sProgramINI)
.head 7 +  If NOT VisDosExist( sProgramINI )
.head 8 -  Call SalMessageBox( 'An error occurred while coping the blank INI file to the following directory.  Please manually copy this file there and try again.', 'Error while copy', MB_IconStop)
.head 8 -  Return FALSE
.head 7 -  Call SalGetProfileString( 'USER', 'LastUser', '', sLastUser_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Title', '', sCourtTitle_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Database', '', sDB_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Select', '', sSelect_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Public', '', sPublic_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Test', '', sTestUser_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'TestDB', '', sTestDB_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Secure', '', sSecurePC_S,  sProgramINI)
.head 7 -  Call SalGetProfileString( 'USER', 'Remote', '', sRemoteUser,  sProgramINI)
.head 5 +  Function: WriteParam
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sSection
.head 7 -  String: sEntry   !LastUser, Title, Database, Select, Public
.head 7 -  String: sValue
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If sSection = ''
.head 8 -  Set sSection = 'USER'
.head 7 -  Return SalSetProfileString( sSection, sEntry, sValue, sProgramINI)
.head 5 +  Function: GetUserInfo
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Receive String: SqlUser
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nFetch
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT	userid, seclevel, division, drawer, cashier, sname, name, rec_copies, wphone, wphone_ext, department, judge, change_password, gracelogins, user_initials
			      FROM		users
			      INTO		:nUserId, :nULevel, :sUDivision, :sUDrawer, :nUCashier, :sUClerk, :sUFullName, :nURecCopies, :sUWPhone, :sUWPhone_Ext, :sUDepartment, :sUJudge, :sUChangePassword, :nUGraceLogins, 
:sUserInit
			      WHERE '|| cStartup.sSelect_S)
.head 7 -  Call SqlFetchNext( hSql, nFetch )
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: GetCourtInfo
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nFetch
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT	Title, Court, Clerk, County, Address, Address2, CityState, Phone,Phone2,
					Phone3, Code, VBDIncDate, VBDIncFine, VBDIncFineI, CourtConst, ori, 
					change_password_date, PJudge,Directions, CBailiff, CHECK_CJIS_JAIL
			               INTO	:sCourt, :sCourtCity, :sCourtClerk, :sCourtCounty, :sCourtAddr, :sCourtAddr2, :sCourtCityState, :sCourtPhone,:sCourtPhone2,
					:sCourtPhone3, :sCourtCode, :dCostsDate, :nVBDFineInc, :nVBDFineIncI, :sCourtConst, :sCourtORI, 
					:dtCourt_Change_Password_Date , :sCourtPJudge, :sCourtDirections, :sCourtCBailiff , :sCHECKCJISJailFlag
			               FROM	CourtInfo' )
.head 7 -  Call SqlFetchNext( hSql, nFetch )
.head 7 -  Call SqlCommit( hSql )
.head 7 +  If sCourtConst != 'CMC' and sCourtConst != 'MMC'
.head 8 -  Call SalMessageBox('Error reading Court Title from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 8 -  Call SalQuit( )
.head 7 -  Set sCourtCity = SalStrProperX( sCourtCity )
.head 5 +  Function: Check_Change_Password
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dtChange_Password_Date
.head 7 -  Number: nError
.head 6 +  Actions
.head 7 +  If (dtCourt_Change_Password_Date <= SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nUGraceLogins > 0 and sUChangePassword = 'Y')
.head 8 +  If sUChangePassword = 'Y'
.head 9 -  Set bChange = TRUE
.head 9 -  Call SalMessageBox( 'PASSWORD HAS EXPIRED,  You have '||SalNumberToStrX(nUGraceLogins, 0 )||' grace logins left.  Please change your password.  NOTE:  Password must be unique, not used before for your login!', 'Warning', 
MB_Ok|MB_IconExclamation )
.head 9 -  Call SalModalDialog( dlgPasswordAPL, hWndForm )
.head 7 +  Else If (dtCourt_Change_Password_Date < SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nUGraceLogins = 0 and sUChangePassword = 'Y')
.head 8 -  Call SalWaitCursor( FALSE )
.head 8 -  Call SalMessageBox(  'Your user login has been disabled, the grace logins of 5 have been used.  Please Contact System Administrator', 'NO GRACE LOGINS', MB_Ok|MB_IconExclamation )
.head 8 -  Call SalQuit(  )
.head 5 +  Function: GetLockInfo
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sStatus
.head 7 -  String: sTitle
.head 7 -  Sql Handle: hSqlCheck
.head 7 -  Number: nReturnCheck
.head 7 -  String: sSaveUser
.head 7 -  String: sSavePassword
.head 6 +  Actions
.head 7 +  ! When SqlError
.head 8 -  Call SQL.SetClassVar( 'LOG', TRUE, hWndForm, 'Login')
.head 8 -  Call SQL.GetError( )
.head 8 -  Return FALSE
.head 7 -  Set sSaveUser = SqlUser
.head 7 -  Set sSavePassword = SqlPassword
.head 7 -  Set SqlPassword = 'CRPUB'
.head 7 -  Set SqlUser = 'CRPUB'
.head 7 -  Set SqlPassword = 'CRPUB'
.head 7 -  Set SQL.sFunctionName = 'cStartup.GetLockInfo'
.head 7 -  Set SQL.sProgramComment = 'Startup class function to determine if user account is locked.'
.head 7 +  If SqlConnect( hSqlCheck )
.head 8 -  Call SqlSetResultSet( hSqlCheck, TRUE )
.head 8 -  Call SqlPrepareAndExecute( hSqlCheck, 'SELECT	account_status
			         FROM	dba_user_lock
			         WHERE	username = :sSaveUser
			         INTO	:sStatus')
.head 8 -  Call SqlFetchNext( hSqlCheck, nReturnCheck)
.head 8 -  Set SqlUser = sSaveUser
.head 8 -  Set SqlPassword = sSavePassword
.head 8 -  Call SQL.CleanUpVar(  )
.head 8 -  Return sStatus
.head 7 +  Else
.head 8 -  Call SQL.CleanUpVar(  )
.head 8 -  Return 'ERROR'
.head 7 -  ! Set dfAccountStatus = 'ACCOUNT IS '|| sStatus
.head 7 +  ! If sStatus = 'LOCKED'
.head 8 -  Call SalColorSet( dfAccountStatus, COLOR_IndexWindow, COLOR_Red)
.head 7 +  ! Else
.head 8 -  Call SalColorSet( dfAccountStatus, COLOR_IndexWindow, COLOR_3DFace)
.head 3 +  Functional Class: cJail
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Date/Time: dtJailPhotoDate
.head 4 +  Functions
.head 5 +  Function: SetJailPic
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Window Handle: hWndPic
.head 7 -  String: fSSN
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dJPhotoDate
.head 7 -  String: sJPhotoFile
.head 7 -  String: sSSNoDash
.head 7 -  String: sJPhotoName
.head 7 -  String: sJPhoto
.head 6 +  Actions
.head 7 -  Call SalPicClear( hWndPic)
.head 7 -  Set sSSNoDash = VisStrSubstitute( fSSN, '-', '' )
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	p.idate, p.ifile
			       FROM	inmate I, jail_image p
			       INTO	:dJPhotoDate, :sJPhotoFile
			       WHERE	i.id = p.id and i.ssn = :sSSNoDash and p.itype like 'F%'
			       ORDER BY	p.idate desc")
.head 7 +  If SqlFetchNext(hSql, nReturn)
.head 8 -  Call SqlPrepareAndExecute( hSql, "SELECT	path
				   FROM		cr_image_location
				    WHERE	type = 'JAIL_IMAGE'
				    INTO		:sJPhotoName")
.head 8 -  Call SqlFetchNext( hSql, nReturn)
.head 8 -  ! Set sJPhotoName = 'O:\\Images\\Jail_Images\\Front\\s\\'
.head 8 -  ! Set sJPhotoName = sJPhotoName|| sJPhotoFile||'.jpg'
.head 8 -  Set sJPhotoName = sJPhotoName|| SalStrRightX(sJPhotoFile, 2) ||'\\'
.head 8 -  Set sJPhotoName = sJPhotoName|| sJPhotoFile||'.jpg'
.head 8 -  Set dtJailPhotoDate = dJPhotoDate
.head 8 +  If VisDosExist( sJPhotoName )
.head 9 +  If hWndPic = hWndNULL
.head 10 -  Return TRUE
.head 9 +  If SalPicSetFile( hWndPic, sJPhotoName)
.head 10 +  If SalPicGetString( hWndPic, PIC_FormatObject, sJPhoto ) > 0
.head 11 -  Return TRUE
.head 10 +  Else
.head 11 -  Call SalPicClear( hWndPic)
.head 11 -  Return FALSE
.head 8 +  Else
.head 9 -  Call SalPicClear( hWndPic)
.head 9 -  Return FALSE
.head 7 +  Else
.head 8 -  Call SalPicClear( hWndPic)
.head 8 -  Return FALSE
.head 5 +  Function: Check_Jail
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: sPassCaseYr
.head 7 -  String: sPassCaseTy
.head 7 -  String: sPassCaseNo
.head 7 -  String: sPassFirstName
.head 7 -  String: sPassLastName
.head 7 -  Date/Time: sPassDOB
.head 7 -  String: sPassSSNo
.head 7 -  Receive String: sHoldJailMsg
.head 7 -  Sql Handle: hSqlJail1
.head 7 -  Sql Handle: hSqlJail2
.head 7 -  Boolean: bCheckCJISRep   !If TRUE - Will look at cjis.roster_date_tab@CJIS & Jail.rep_date_tab@CJIS, 
			If FALSE - Will only look at local DBA_SNAPSHOT table for status
				-Use FALSE, except when running bring back list
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nJailReturn
.head 7 -  Number: nReturnJail1
.head 6 +  Actions
.head 7 +  If sPassFirstName = '' or sPassLastName = '' or sPassDOB = DATETIME_Null or sPassSSNo = ''
.head 8 -  Call SqlPrepareAndExecute( hSqlJail1, 'SELECT	lname, fname, dob, ssno
			         FROM	cr_parties
			         WHERE	caseyr = :sPassCaseYr and
					 casety = :sPassCaseTy and
					caseno = :sPassCaseNo
			         INTO	:sPassLastName, :sPassFirstName, :sPassDOB, :sPassSSNo')
.head 8 -  Call SqlFetchNext( hSqlJail1, nReturnJail1)
.head 7 -  Set nJailReturn = Check_Jail_Names ( sPassFirstName, sPassLastName, sPassDOB, sPassSSNo, sHoldJailMsg, hSqlJail2,  bCheckCJISRep)
.head 7 +  If nJailReturn
.head 8 -  Return TRUE
.head 7 +  Else If nJailReturn = 2
.head 8 -  Return nJailReturn
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute (hSqlJail1, 'SELECT alname, afname, adob, assno
	FROM cr_alias  INTO  :sPassLastName, :sPassFirstName, :sPassDOB, :sPassSSNo
	WHERE caseyr = :sPassCaseYr and casety = :sPassCaseTy and caseno = :sPassCaseNo')
.head 8 +  While SqlFetchNext( hSqlJail1, nReturnJail1 )
.head 9 +  If Check_Jail_Names ( sPassFirstName, sPassLastName, sPassDOB, sPassSSNo, sHoldJailMsg, hSqlJail2, bCheckCJISRep )
.head 10 -  Return TRUE
.head 5 +  Function: Check_Jail_Names
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: sPassFirstName
.head 7 -  String: sPassLastName
.head 7 -  Date/Time: sPassDOB
.head 7 -  String: sPassSSNo
.head 7 -  Receive String: sHoldJailMsg
.head 7 -  Sql Handle: hSqlJail2
.head 7 -  Boolean: bCheckCJISRep
.head 6 +  Static Variables
.head 7 -  Date/Time: dJail_Date
.head 6 +  Local variables
.head 7 -  String: sHoldJailId
.head 7 -  String: sKeepFirstThree
.head 7 -  String: sSSNoDash
.head 7 -  Number: nReturnJail2
.head 7 -  String: sCourtName
.head 6 +  Actions
.head 7 -  Set sKeepFirstThree = SalStrLeftX( sPassFirstName, 3) || '%'
.head 7 -  Set sSSNoDash = SalStrMidX( sPassSSNo, 0, 3 ) || SalStrMidX( sPassSSNo, 4, 2 ) || SalStrMidX( sPassSSNo, 7, 4 )
.head 7 -  Call SqlPrepareAndExecute( hSqlJail2, "Select i.id
	from Inmate i  into  :sHoldJailId
	where (i.ssn=:sSSNoDash or (i.lname=:sPassLastName and i.fname like :sKeepFirstThree and i.dob=:sPassDOB)) and i.stat in 'JAIL' ")
.head 7 +  If SqlFetchNext (hSqlJail2, nReturnJail2)
.head 8 -  Call SqlPrepareAndExecute( hSqlJail2, "Select 'Defendant in Jail on Case ' || c.amcca || ' ' || nvl(cpcase, mcasnum), court
	from Charge c  into :sHoldJailMsg, :sCourtName
	where c.id = :sHoldJailId  and  (c.expdate > sysdate or c.expdate is null)
	order by c.bookid desc")
.head 8 +  If Not SqlFetchNext (hSqlJail2, nReturnJail2)
.head 9 -  Set sHoldJailMsg = 'Defendant in Jail'
.head 8 +  If sCourtName != ''
.head 9 -  Set sHoldJailMsg = sHoldJailMsg || '(' || sCourtName || ')'
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 +  If dJail_Date < SalDateCurrent(  ) - 3/24
.head 9 +  When SqlError
.head 10 -  Set dJail_Date = SalDateConstruct( 1900, 1, 1, 0, 0, 0 )
.head 10 -  Return TRUE
.head 9 +  If bCheckCJISRep
.head 10 -  Call SqlPrepareAndExecute( hSqlJail2, "Select roster_date
		from cjis.roster_date_tab@CJIS
	Union
	        Select rep_date
		from Jail.rep_date_tab@CJIS
	Union
	        Select distinct last_refresh
		from dba_snapshots
		where owner='CRIM' and name in ('INMATE', 'JAIL_ROSTER')
	        into :dJail_Date
	        Order by 1" )
.head 9 +  Else
.head 10 -  Call SqlPrepareAndExecute( hSqlJail2, "SELECT	distinct last_refresh
				  FROM		dba_snapshots
				  WHERE	owner='CRIM' and name in ('INMATE', 'JAIL_ROSTER')
			                INTO		:dJail_Date
			                ORDER BY	1" )
.head 9 -  Call SqlFetchNext( hSqlJail2, nReturnJail2 )
.head 9 +  If dJail_Date < SalDateCurrent(  ) - 3/24
.head 10 +  If dJail_Date = SalDateConstruct( 1900, 1, 1, 0, 0, 0 )
.head 11 -  Set sHoldJailMsg = 'Jail Roster not available - CJIS Down'
.head 10 +  Else
.head 11 -  Set sHoldJailMsg = 'Jail Roster not updated since ' || SalFmtFormatDateTime( dJail_Date, 'MM-dd-yy hh:mm AMPM' )
.head 10 -  Return 2
.head 8 -  Return FALSE
.head 5 +  Function: CheckJailReplication
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Date/Time:
.head 6 +  Parameters
.head 7 -  Boolean: bCheckCJISRep
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Sql Handle: hSqlCheck
.head 7 -  Number: nReturnCheck
.head 7 -  Date/Time: dJail_Date
.head 6 +  Actions
.head 7 -  Call SqlConnect( hSqlCheck )
.head 7 -  Call SqlSetResultSet( hSqlCheck, TRUE)
.head 7 +  If bCheckCJISRep
.head 8 -  Call SqlPrepareAndExecute( hSqlCheck, "SELECT	roster_date
		                                 FROM	cjis.roster_date_tab@CJIS
					Union
				     SELECT	rep_date
				     FROM	Jail.rep_date_tab@CJIS
					Union
			                   SELECT	distinct last_refresh
				     FROM	dba_snapshots
			                   WHERE	owner='CRIM' and name in ('INMATE', 'JAIL_ROSTER')
		                                 INTO		:dJail_Date
			                   ORDER BY	1" )
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute( hSqlCheck, "SELECT	distinct last_refresh
				  FROM		dba_snapshots
				  WHERE	owner='CRIM' and name in ('INMATE', 'JAIL_ROSTER')
			                INTO		:dJail_Date
			                ORDER BY	1" )
.head 7 -  Call SqlFetchNext( hSqlCheck, nReturnCheck)
.head 7 -  Call SqlCommit( hSqlCheck )
.head 7 -  Call SqlDisconnect( hSqlCheck )
.head 7 -  Return dJail_Date
.head 3 +  Pushbutton Class: cLoadApp
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left:
.head 5 -  Top:
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Class Default
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Keyboard Accelerator: Class Default
.head 4 -  Font Name: Class Default
.head 4 -  Font Size: Class Default
.head 4 -  Font Enhancement: Class Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: Class Default
.head 4 -  Image Style: Class Default
.head 4 -  Text Color: Class Default
.head 4 -  Background Color: #ffe9e9
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Button Appearance: Class Default
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Class Default
.head 4 -  Text Alignment: Class Default
.head 4 -  Text Image Relation: Class Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Description: This class can be used to load an application.  It will check the server to determin
	that the local copy of the application is the most current
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sAppName
.head 5 -  Number: nSecurityLevel
.head 5 -  String: sUserExeceptionRights
.head 5 -  String: sApplication_Type 
.head 5 -  String: sAppNameLocal
.head 5 -  String: sAppDriveL
.head 5 -  String: sAppDirL
.head 5 -  String: sAppFileL
.head 5 -  String: sAppExtL
.head 5 -  String: spFileNoExe
.head 4 +  Functions
.head 5 +  Function: GetImagePath
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Sql Handle: hSqlImage
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnImage
.head 7 -  String: sLocation
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT	path
				   FROM		cr_image_location
				    WHERE	type = :fType
				    INTO		:sLocation')
.head 7 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 7 -  Return sLocation
.head 5 +  Function: CheckVersion
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bCopy  !tells function to copy if not the same
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sServerApp
.head 7 -  Date/Time: dtLocalDate
.head 7 -  Date/Time: dtServerDate
.head 7 -  String: sServerDepend
.head 7 -  String: sLocalDepend
.head 7 -  String: sDependName
.head 7 -  String: sAppFileCheck
.head 7 -  Boolean: bSQLConnect
.head 6 +  Actions
.head 7 +  ! If bCheckVersion = TRUE
.head 8 -  Set sServerApp = GetImagePath( sApplication_Type , hSql) || sAppFileL
.head 8 -  Set sServerDepend = GetImagePath( sApplication_Type , hSql) 
.head 8 -  Set sLocalDepend = GetImagePath( sApplication_Type||'_LOCAL' , hSql) 
.head 8 -  Call SalFileGetDateTime( sAppName, dtLocalDate )
.head 8 -  If SalFileGetDateTime( sServerApp, dtServerDate ) = FALSE !Could not connect to server, don't try to copy
.head 8 +  Else 
.head 9 +  If bCopy
.head 10 +  If dtLocalDate != dtServerDate
.head 11 +  If NOT VisDosExist( GetImagePath( sApplication_Type||'_LOCAL' , hSql)  )
.head 12 -  Call SalFileCreateDirectory( GetImagePath( sApplication_Type||'_LOCAL' , hSql)  )
.head 11 +  If NOT VisDosExist( sAppName )
.head 12 -  Call VisFileCopy( sServerApp, sAppName )
.head 12 -  Set sAppFileCheck = VisStrSubstitute( sAppFileL, '.EXE', '.APP')
.head 12 -  Set sAppFileCheck = SalStrUpperX( sAppFileCheck )
.head 12 -  Call SqlPrepareAndExecute( hSql, 'SELECT	dep_name
			       FROM	app_depend
			       INTO		:sDependName
			       WHERE	UPPER(app_name) = :sAppFileCheck')
.head 12 +  While SqlFetchNext( hSql, nReturn)
.head 13 -  Call VisFileCopy( sServerDepend|| sDependName, sLocalDepend|| sDependName )
.head 11 +  Else 
.head 12 -  Call VisFileCopy( sServerApp, sAppName )
.head 7 +  If hSql = hWndNULL
.head 8 -  Set bSQLConnect = SqlConnect( hSql )
.head 7 -  Set sServerApp = GetImagePath( sApplication_Type , hSql)
.head 7 +  If SalStrRightX( sServerApp, 1 ) = '\\'
.head 8 -  Set sServerApp = sServerApp || sAppFileL
.head 7 +  Else
.head 8 -  Set sServerApp = sServerApp || '\\' || sAppFileL
.head 7 -  Set sServerDepend = GetImagePath( sApplication_Type , hSql)
.head 7 -  Set sLocalDepend = GetImagePath( sApplication_Type || '_LOCAL' , hSql)
.head 7 -  Call SalFileGetDateTime( sAppName, dtLocalDate )
.head 7 -  If SalFileGetDateTime( sServerApp, dtServerDate ) = FALSE !Could not connect to server, don't try to copy
.head 7 +  Else
.head 8 +  If bCopy
.head 9 +  If dtLocalDate != dtServerDate
.head 10 +  If NOT VisDosExist( GetImagePath( sApplication_Type || '_LOCAL' , hSql)  )
.head 11 -  Call SalFileCreateDirectory( GetImagePath( sApplication_Type || '_LOCAL' , hSql)  )
.head 10 +  If NOT VisDosExist( sAppName )
.head 11 -  Call VisFileCopy( sServerApp, sAppName )
.head 11 -  Set sAppFileCheck = VisStrSubstitute( sAppFileL, '.exe', '.app')
.head 11 -  Set sAppFileCheck = sAppFileCheck
.head 11 -  Call SqlPrepareAndExecute( hSql, 'SELECT dep_name
		   		    FROM app_depend
		   		    INTO :sDependName
		   		    WHERE app_name = :sAppFileCheck')
.head 11 +  While SqlFetchNext( hSql, nReturn)
.head 12 -  Call VisFileCopy( sServerDepend || sDependName, sLocalDepend || sDependName )
.head 10 +  Else
.head 11 -  Call VisFileCopy( sServerApp, sAppName )
.head 7 +  If bSQLConnect
.head 8 -  Call SqlDisconnect ( hSql )
.head 5 +  ! Function: GetSecurityLevel
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: 
.head 6 +  Parameters 
.head 7 -  String: fAppName
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nReturnSecLevel
.head 6 +  Actions 
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT	seclevel
				   FROM		app_rights
				    WHERE	app_name = Upper(:fAppName)
				    INTO		:nReturnSecLevel')
.head 7 -  Call SqlFetchNext( hSql, nReturn)
.head 7 +  If nReturnSecLevel = NUMBER_Null
.head 8 -  Set nReturnSecLevel = 0
.head 7 -  Return nReturnSecLevel
.head 5 +  Function: GetSecurityLevel
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: fAppName
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: spdTitle
.head 7 -  Number: nReturnSecLevel
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT seclevel
	FROM app_rights into :nReturnSecLevel
	WHERE app_name = Upper(:fAppName) ')
.head 7 +  If Not SqlFetchNext( hSql, nReturn)
.head 8 -  Call SalGetWindowText( hWndItem, spdTitle, 30 )
.head 8 -  Call SalMessageBox( 'No Security Entry for Push Button ' || spdTitle || ' - ' || fAppName, 'Missing Entry in the app_rights table', MB_Ok )
.head 8 -  Call SalDisableWindow( hWndItem )
.head 8 -  Return TRUE
.head 7 +  If nReturnSecLevel = NUMBER_Null
.head 8 -  Set nReturnSecLevel = 0
.head 7 -  Return nReturnSecLevel
.head 5 +  ! Function: Constructor
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: sPAppName
.head 7 -  Number: nPSecurityLevel
.head 7 -  String: sPApplication_Type   !CRIMINAL_APPLICATIONS_5.1, CRIMINAL_J_APPLICATIONS_5.1, 
			CIVIL_APPLICATIONS_5.1, CIVIL_J_APPLICATIONS_5.1, 
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 -  Set sAppName = GetImagePath(sPApplication_Type||'_LOCAL', hSql )||'\\'|| sPAppName
.head 7 -  Set nSecurityLevel = nPSecurityLevel
.head 7 -  Set sAppName = SalStrUpperX( sAppName )
.head 7 +  If SalStrScan( sAppName, '\\') > 0
.head 8 -  Call VisDosSplitPath( sAppName, sAppDriveL, sAppDirL, sAppFileL, sAppExtL)
.head 8 -  Set sAppFileL = sAppFileL||sAppExtL
.head 7 +  Else 
.head 8 -  Set sAppFileL = sAppName
.head 7 -  Set sApplication_Type = sPApplication_Type
.head 7 +  If nSecurityLevel = NUMBER_Null
.head 8 -  Set nSecurityLevel = GetSecurityLevel( sAppFileL )
.head 7 -  Set sUserExeceptionRights = GetExecption( sAppFileL )
.head 7 +  If nULevel < nSecurityLevel
.head 8 -  Call SalDisableWindow( hWndItem )
.head 7 +  If sUserExeceptionRights = 'E'
.head 8 -  Call SalEnableWindow( hWndItem )
.head 5 +  Function: Constructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sPAppName
.head 7 -  Number: nPSecurityLevel
.head 7 -  String: sPApplication_Type   !CRIMINAL_APPLICATIONS_5.1, CRIMINAL_J_APPLICATIONS_5.1, CIVIL_APPLICATIONS_5.1, CIVIL_J_APPLICATIONS_5.1,
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sAppName = GetImagePath(sPApplication_Type || '_LOCAL', hSql ) || '\\' || sPAppName
.head 7 -  Set nSecurityLevel = nPSecurityLevel
.head 7 -  ! Set sAppName = sAppName
.head 7 +  If SalStrUpperX( SalStrRightX( sAppName, 4 )) = '.EXE'
.head 8 -  Set sAppName = SalStrUpperX( sAppName )
.head 8 +  If SalStrScan( sAppName, '\\') > 0
.head 9 -  Call VisDosSplitPath( sAppName, sAppDriveL, sAppDirL, sAppFileL, sAppExtL)
.head 9 -  Set sAppFileL = sAppFileL || sAppExtL
.head 8 +  Else
.head 9 -  Set sAppFileL = sAppName
.head 8 -  Set sApplication_Type = sPApplication_Type
.head 7 +  Else
.head 8 -  Set sAppFileL = sPAppName
.head 7 +  If nSecurityLevel = NUMBER_Null
.head 8 -  Set nSecurityLevel = GetSecurityLevel( sAppFileL )
.head 8 -  Set sUserExeceptionRights = GetExecption( sAppFileL )
.head 7 -  !
.head 7 +  If nULevel < nSecurityLevel
.head 8 +  If sUserExeceptionRights != 'E'
.head 9 -  Call SalDisableWindow( hWndItem )
.head 7 +  Else If sUserExeceptionRights = 'D'
.head 8 -  Call SalDisableWindow( hWndItem )
.head 5 +  Function: GetExecption
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fAppName
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sRights
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT	rights
				   FROM		APP_RIGHTS_EXEPTIONS
				    WHERE	app_name = Upper(:fAppName) and
						user_name = :SqlUser
				    INTO		:sRights')
.head 7 -  Call SqlFetchNext( hSql, nReturn)
.head 7 -  Return sRights
.head 4 +  Message Actions
.head 5 +  ! On SAM_Click
.head 6 +  If IsBackup(  ) = FALSE  !Don't copy down live version if running is "TEST" mode
.head 7 -  Call CheckVersion( TRUE )
.head 6 -  Call SalDisableWindow( MyValue )
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  If NOT VisDosExist( sAppName )
.head 7 -  Call SalMessageBox( 'Error - Could not find the following executable file:
'|| sAppName||'
Contact Supervisor', 'Executable Not Found', MB_IconStop| MB_Ok)
.head 6 +  Else 
.head 7 -  Call SalLoadApp(sAppName, SqlUser ||  ' ' || SqlPassword|| ' ' || SqlDatabase )
.head 6 -  Call SalEnableWindow( MyValue )
.head 6 -  Call SalWaitCursor( FALSE )
.head 5 +  On SAM_Click
.head 6 -  ! If IsBackup(  ) = FALSE  !Don't copy down live version if running is "TEST" mode
.head 6 +  If SalStrUpperX(SalStrRightX( sAppName, 4 )) = '.EXE'
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, 1, SalStrLength( sAppName ) - 4 )
.head 7 +  If nULevel < 11
.head 8 +  ! If SalAppFind( spFileNoExe, TRUE ) != hWndNULL
.head 9 -  Return TRUE
.head 8 -  ! Disabled above, 
.head 6 +  Else If SalStrScan( sAppName, '.dlg') > 0
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, SalStrScan( sAppName, '.dlg') + 1, 30 )
.head 7 -  Call SalModalDialogFromStr( spFileNoExe, hWndForm )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrScan( sAppName, '.frm') > 0
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, SalStrScan( sAppName, '.frm') + 1, 30 )
.head 7 -  Call SalCreateWindow( spFileNoExe, hWndForm )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrScan( sAppName, '.mdi') > 0
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, SalStrScan( sAppName, '.mdi') + 1, 30 )
.head 7 -  Call SalCreateWindow( spFileNoExe, hWndForm )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrScan( sAppName, '.tbl') > 0
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, SalStrScan( sAppName, '.tbl') + 1, 30 )
.head 7 -  Call SalCreateWindow( spFileNoExe, hWndForm )
.head 7 -  Return TRUE
.head 6 +  If Startup.sRemoteUser != 'TRUE'
.head 7 -  Call CheckVersion( TRUE )
.head 6 -  Call SalDisableWindow( MyValue )
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  If NOT VisDosExist( sAppName )
.head 7 -  Call SalMessageBox( 'Error - Could not find the following executable file:
		'|| sAppName||'
		Contact Supervisor', 'Executable Not Found', MB_IconStop| MB_Ok)
.head 6 +  Else
.head 7 -  Call SalLoadApp(sAppName, SqlUser || ' ' || SqlPassword|| ' ' || SqlDatabase )
.head 7 -  Call SalEnableWindow( MyValue )
.head 6 -  Call SalWaitCursor( FALSE )
.head 3 +  Functional Class: cSQL
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sOperation   ! DB - Insert into Database error
		! LOG - Write to local log file
.head 5 -  Boolean: bShowMSG
.head 5 -  Window Handle: hWndParent
.head 5 -  String: sFunctionName
.head 5 -  Sql Handle: hSqlUse
.head 5 -  String: sProgramComment
.head 5 -  String: fFunctionModDt_Comment
.head 5 -  Sql Handle: hCheckSql  !Handle that we are checking
.head 5 -  Boolean: bSetParamCalled
.head 5 -  String: sComputerName
.head 5 -  String: sWindowsVersion
.head 5 -  Date/Time: dtFileModDate
.head 5 -  String: sOSUserName
.head 4 +  Functions
.head 5 +  Function: GetError
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nErrorNumber
.head 7 -  String: sErrorText
.head 7 -  String: sSqlStatement
.head 7 -  Number: nPos
.head 6 +  Actions
.head 7 -  Call SqlExtractArgs ( wParam, lParam, hCheckSql, nErrorNumber, nPos )
.head 7 +  If sOperation = ''
.head 8 -  Set sOperation = 'DB'
.head 7 +  If bSetParamCalled = FALSE
.head 8 -  Set bShowMSG = TRUE
.head 7 -  Set nErrorNumber = SqlError ( hCheckSql )
.head 7 -  Set sErrorText = SqlGetErrorTextX ( nErrorNumber )
.head 7 -  Set sSqlStatement = SqlGetLastStatement(  )
.head 7 +  If sOperation = 'DB'
.head 8 -  Call InsertError( nErrorNumber, sErrorText, sSqlStatement )
.head 7 +  If sOperation = 'LOG'
.head 8 -  Call InsertError( nErrorNumber, sErrorText, sSqlStatement )
.head 7 +  If bShowMSG
.head 8 -  ! Call SalMessageBox ( 'SQL Error: ' || sErrorText || '
Statement: '|| sSqlStatement, 'Invalid SQL', MB_Ok )
.head 8 -  Call SalModalDialog( dlgSqlErrorReport, hWndForm, sSqlStatement, sErrorText, nErrorNumber )
.head 7 -  Call CleanUpVar( )
.head 5 +  Function: SqlIsConnected
.head 6 -  Description: author: 	Markus Glück
date:	??
version:	V1.00

description:
Checks if an handle is connected

description:
If SqlIsConnected (hSql)
   Call SqlDisconnect( hSql)

.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Sql Handle: p_hSql
.data INHERITPROPS
0000: 0100
.enddata
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: l_bReturn
.data INHERITPROPS
0000: 0100
.enddata
.head 6 +  Actions
.head 7 -  Set l_bReturn = ( SqlGetCursor( p_hSql ) != 0xFFFF )
.head 7 -  Return l_bReturn
.head 5 +  Function: GetWindowsVersion
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  ! String: sBuffer
.head 7 -  Number: nVersion
.head 7 -  String: sOSVersion
.head 7 -  String: sProductType
.head 7 -  String: sReturnVersion
.head 7 -  String: sOSString
.head 7 -  ! Number: nSize
.head 6 +  Actions
.head 7 -  Set nVersion = GetOSVersion( sOSVersion, sProductType)
.head 7 +  Select Case nVersion
.head 8 +  Case VERSION_OS_95
.head 9 -  Set sReturnVersion = "Windows 95 retail, OEM"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_95_OSR1
.head 9 -  Set sReturnVersion = "Windows 95 OEM OSR 1"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_95_OSR2
.head 9 -  Set sReturnVersion = "Windows 95 OEM OSR 2"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_95_OSR21
.head 9 -  Set sReturnVersion = "Windows 95 OEM OSR 2.1"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_95_OSR25
.head 9 -  Set sReturnVersion = "Windows 95 OEM OSR 2.5"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_98
.head 9 -  Set sReturnVersion = "Windows 98 retail, OEM"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_98_SE
.head 9 -  Set sReturnVersion = "Windows 98 Second Edition"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_NT_351
.head 9 -  Set sReturnVersion = "Windows NT 3.51"
.head 9 -  Set sOSString = "Windows NT"
.head 9 -  Break
.head 8 +  Case VERSION_OS_NT_4
.head 9 -  Set sReturnVersion = "Windows NT 4.0"
.head 9 -  Set sOSString = "Windows NT"
.head 9 -  Break
.head 8 +  Case VERSION_OS_2000
.head 9 -  Set sReturnVersion = "Windows 2000"
.head 9 -  Set sOSString = "Windows NT"
.head 9 -  Break
.head 8 +  Case VERSION_OS_ME
.head 9 -  Set sReturnVersion = "Windows Me"
.head 9 -  Set sOSString = "Windows"
.head 9 -  Break
.head 8 +  Case VERSION_OS_XP
.head 9 -  Set sReturnVersion = "Windows XP"
.head 9 -  Set sOSString = "Windows NT"
.head 9 -  Break
.head 8 +  Case VERSION_OS_NET
.head 9 -  Set sReturnVersion = "Windows .NET Server 2003 family"
.head 9 -  Set sOSString = "Windows NT"
.head 9 -  Break
.head 7 -  Set sReturnVersion = sReturnVersion || " (Ver " || sOSVersion || ")"
.head 7 -  Return sReturnVersion
.head 5 +  Function: SalSysGetComputerName
.head 6 -  Description: author: ???
date:     ???
version: 1.00
Returns the computername

example:

Set sComputername =  SalSysGetComputerName()
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nSize
.head 7 -  String: sCOMPUTERNAME
.head 6 +  Actions
.head 7 -  Set nSize = 250
.head 7 -  Call SalSetBufferLength( sCOMPUTERNAME, nSize + 1 )
.head 7 -  Call GetComputerNameA( sCOMPUTERNAME, nSize )
.head 7 -  Set sCOMPUTERNAME = SalStrLeftX( sCOMPUTERNAME, nSize )
.head 7 -  Return sCOMPUTERNAME
.head 5 +  Function: InsertError
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: fErrorNumber
.head 7 -  String: fErrorText
.head 7 -  String: fSqlStatement
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturn
.head 7 -  String: sInsert
.head 7 -  String: sForm
.head 7 -  Long String: sData
.head 7 -  File Handle: fFile
.head 6 +  Actions
.head 7 -  Set sComputerName = SalSysGetComputerName(  )
.head 7 -  Set sWindowsVersion = GetWindowsVersion(  )
.head 7 -  Set sOSUserName = GetOSUserName(  )
.head 7 -  Set sForm = SalGetItemNameX( hWndParent )
.head 7 -  Call SalFileGetDateTime( strArgArray[0], dtFileModDate )
.head 7 +  If sOperation = 'DB'
.head 8 +  If NOT SqlIsConnected( hSqlUse )
.head 9 -  Call SqlConnect(hSqlUse )
.head 9 -  Call SqlSetResultSet( hSqlUse, TRUE)
.head 8 -  Set sInsert ="INSERT INTO SQL_APP_ERRORS
		(COMPUTERNAME, OSVERSION, APPNAME, OSUSERNAME,
		 SQLERROR, SQLERRORTEXT, SQLSTATEMENT, APP_CODE_AREA, APP_CODE_FUNCTION, APP_COMMENT, APP_FUNCTION_MODDT_COMMENT, APP_MOD_DATE)
	     VALUES(:sComputerName, :sWindowsVersion, :strArgArray[0], :sOSUserName,  :fErrorNumber, :fErrorText, :fSqlStatement, :sForm, :sFunctionName, :sProgramComment, :fFunctionModDt_Comment, :dtFileModDate)"
.head 8 -  Call SqlPrepareAndExecute( hSqlUse, sInsert)
.head 8 -  Call SqlCommit( hSqlUse )
.head 7 +  Else If sOperation = 'LOG'
.head 8 +  If NOT VisDosExist( cSQLErrorLogFile )
.head 9 -  Call VisFileOpen( fFile, cSQLErrorLogFile, OF_Read | OF_Write )
.head 8 +  Else
.head 9 -  Call VisFileOpen( fFile, cSQLErrorLogFile, OF_Append)
.head 8 -  Set sData  =  '*******************************************************************************
DATE: ' || SalFmtFormatDateTime( SalDateCurrent(  ),  'MM-dd-yyyy hh.mm.ss AMPM')||'
Computer Name: '|| sComputerName||'
OS Version: '|| sWindowsVersion ||'
OS User: '|| sOSUserName ||'
DB User: '|| SqlUser ||'
Application Name: '|| strArgArray[0] ||'
Application Modified Date: '||  SalFmtFormatDateTime( dtFileModDate,  'MM-dd-yyyy')||'
Windows Form: '|| sForm ||'
Function: '|| sFunctionName ||'
Comment: '|| sProgramComment ||'
                  '|| fFunctionModDt_Comment ||'

SQL Error: '|| fErrorText || ' ('|| SalNumberToStrX( fErrorNumber, 0) ||') 
SQL Statement: '|| fSqlStatement
.head 8 -  Call VisFileWriteString( fFile, sData)
.head 8 -  Call VisFileClose( fFile )
.head 5 +  Function: GetOSUserName
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sReturnUserName
.head 7 -  Number: nSize
.head 7 -  String: sBuffer
.head 6 +  Actions
.head 7 -  ! ! - User Name
.head 7 -  Set nSize = 50
.head 7 -  Call SalSetBufferLength( sBuffer, nSize )
.head 7 -  Call GetUserNameA( sBuffer, nSize )
.head 7 -  Set sReturnUserName = sBuffer
.head 7 -  Return sReturnUserName
.head 5 +  Function: SalGetItemNameX
.head 6 -  Description: author: 	tl
date:	2000
version:	1.00

.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  Window Handle: phWndItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: lsDummy
.head 6 +  Actions
.head 7 -  Call SalGetItemName (phWndItem, lsDummy)
.head 7 -  Return lsDummy
.head 5 +  Function: SetClassVar
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fOp   ! DB - Insert into Database error
		! LOG - Write to local log file
.head 7 -  Boolean: fShowMSG
.head 7 -  Window Handle: fWndParent
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sOperation = fOp
.head 7 -  Set bShowMSG = fShowMSG
.head 7 -  Set hWndParent = fWndParent
.head 7 -  Set bSetParamCalled = TRUE
.head 5 +  Function: GetOSVersion
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nVersion
.head 6 +  Parameters
.head 7 -  Receive String: sVersion
.head 7 -  Receive String: sProductType
.head 7 -  ! Receive String: sServicePack
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nStructSize
.head 7 -  Number: nMajorVersion
.head 7 -  Number: nMinorVersion
.head 7 -  Number: nBuildNumber
.head 7 -  Number: nPlatformId
.head 7 -  String: sCSDVersion
.head 7 -  String: sCSD
.head 7 -  Number: nServicePackMajor
.head 7 -  Number: nServicePackMinor
.head 7 -  Number: nSuiteMask
.head 7 -  Number: nProductType
.head 7 -  Number: nReserved
.head 6 +  Actions
.head 7 -  Set nStructSize = 156
.head 7 -  Call SalSetBufferLength( sCSDVersion,128 )
.head 7 +  If not GetVersionExA( nStructSize, nMajorVersion, nMinorVersion, nBuildNumber, nPlatformId, sCSDVersion, nServicePackMajor, nServicePackMinor, nSuiteMask, nProductType, nReserved )
.head 8 -  Set nStructSize = 148
.head 8 -  Call SalSetBufferLength( sCSDVersion,128 )
.head 8 -  Call GetVersionExA( nStructSize, nMajorVersion, nMinorVersion, nBuildNumber, nPlatformId, sCSDVersion, nServicePackMajor, nServicePackMinor, nSuiteMask, nProductType, nReserved )
.head 7 -  ! Set sServicePack = SalNumberToStrX( nServicePackMajor, 0 ) || "." || SalNumberToStrX( nServicePackMinor, 0 )
.head 7 -  Set sVersion =SalNumberToStrX( nMajorVersion, 0 )  || "." || SalStrRepeatX( "0", 2-SalStrLength(SalNumberToStrX( nMinorVersion, 0 ) )) ||
 SalNumberToStrX( nMinorVersion, 0 ) || "." || SalNumberToStrX( nBuildNumber & 0xFFFF, 0 )
.head 7 -  Call SalSetBufferLength( sCSD,129 )
.head 7 -  Call CStructGetString( sCSDVersion, 0,  SalStrLength(sCSDVersion) + 1, sCSD )
.head 7 -  Set sCSDVersion =SalStrTrimX ( sCSDVersion)
.head 7 -  Set sVersion = sVersion ||  sCSD
.head 7 +  If nProductType = VER_NT_WORKSTATION
.head 8 +  If nMajorVersion = 4
.head 9 -  Set sProductType = "Workstation 4.0"
.head 8 +  Else If ( nSuiteMask & VER_SUITE_PERSONAL )
.head 9 -  Set sProductType = "Home Edition"
.head 8 +  Else
.head 9 -  Set sProductType = "Professional"
.head 7 +  Else If nProductType = VER_NT_SERVER
.head 8 +  If (nMajorVersion = 5 and nMinorVersion = 2 )
.head 9 +  If (nSuiteMask & VER_SUITE_DATACENTER )
.head 10 -  Set sProductType = "Datacenter Edition"
.head 9 +  Else If ( nSuiteMask & VER_SUITE_ENTERPRISE )
.head 10 -  Set sProductType = "Enterprise Edition"
.head 9 +  Else If ( nSuiteMask = VER_SUITE_BLADE )
.head 10 -  Set sProductType = "Web Edition"
.head 9 +  Else
.head 10 -  Set sProductType = "Standard Edition"
.head 8 +  Else If ( nMajorVersion = 5 and  nMinorVersion = 0 )
.head 9 +  If (nSuiteMask & VER_SUITE_DATACENTER )
.head 10 -  Set sProductType = "Datacenter Server"
.head 9 +  Else If ( nSuiteMask & VER_SUITE_ENTERPRISE )
.head 10 -  Set sProductType = "Advanced Server"
.head 9 +  Else
.head 10 -  Set sProductType = "Server"
.head 8 +  Else
.head 9 +  If ( nSuiteMask & VER_SUITE_ENTERPRISE )
.head 10 -  Set sProductType = "Server 4.0, Enterprise Edition"
.head 9 +  Else
.head 10 -  Set sProductType = "Server 4.0"
.head 7 +  If nPlatformId = VER_PLATFORM_WIN32_NT
.head 8 +  If nMajorVersion = 3 and nMinorVersion = 51
.head 9 -  Return VERSION_OS_NT_351
.head 8 +  Else If nMajorVersion  = 4 and nMinorVersion = 0
.head 9 -  Return VERSION_OS_NT_4
.head 8 +  Else If nMajorVersion  = 5 and nMinorVersion = 0
.head 9 -  Return VERSION_OS_2000
.head 8 +  Else If nMajorVersion  = 5 and nMinorVersion = 1
.head 9 -  Return VERSION_OS_XP
.head 8 +  Else If nMajorVersion  = 5 and nMinorVersion = 2
.head 9 -  Return VERSION_OS_NET
.head 7 +  Else If nPlatformId = VER_PLATFORM_WIN32_WINDOWS
.head 8 +  If nMajorVersion = 4  and nMinorVersion = 0
.head 9 +  If sCSDVersion = "A"
.head 10 -  Return VERSION_OS_95_OSR1
.head 9 +  Else If sCSDVersion = "B"
.head 10 -  ! If SalNumberLow(nTemp1) = 1111
.head 10 -  Return VERSION_OS_95_OSR2
.head 9 +  Else
.head 10 -  Return VERSION_OS_95
.head 8 +  Else If nMajorVersion = 4  and nMinorVersion = 3
.head 9 +  If sCSDVersion = "B"
.head 10 -  Return VERSION_OS_95_OSR21
.head 9 +  Else If sCSDVersion = "C"
.head 10 -  Return VERSION_OS_95_OSR25
.head 8 +  Else If nMajorVersion = 4  and nMinorVersion = 10
.head 9 +  If sCSDVersion = "A"
.head 10 -  Return VERSION_OS_98_SE
.head 9 +  Else
.head 10 -  Return VERSION_OS_98
.head 8 +  Else If nMajorVersion = 4  and nMinorVersion = 90
.head 9 -  Return VERSION_OS_ME
.head 5 +  Function: CleanUpVar
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sFunctionName = ''
.head 7 -  Set sProgramComment = ''
.head 7 -  Set fFunctionModDt_Comment = ''
.head 7 -  Set bSetParamCalled = FALSE
.head 7 -  Set hCheckSql = 0
.head 3 +  Functional Class: Costs
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 5 -  Sql Handle: hSqlCost
.head 5 -  Boolean: bCostClass
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseNo
.head 5 -  String: fCaseTy
.head 5 -  Date/Time: dtHB562Date
.head 4 +  Functions
.head 5 +  Function: C_Insert_Costs
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fCode
.head 7 -  Number: fCount
.head 7 -  String: fCity_State_Code	!!! This allows only 1 part of the group to be inserted
.head 7 -  Boolean: bCommit
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dCurrentDate
.head 7 -  String: sCodeAmt
.head 7 -  String: sCodeDesc
.head 7 -  String: sInsertCost
.head 7 -  String: sWhereC2S2Code   !!! Used to only select 1 code out of the group when inserting
.head 7 -  String: sCost_Type
.head 6 +  Actions
.head 7 -  Set fCode = SalStrTrimX( fCode )
.head 7 -  Set dCurrentDate = Current_Date( '' )
.head 7 -  Set sCodeAmt = C_Get_Primary_Charge_Type( )
.head 7 -  Set sCost_Type = C_Get_Cost_Type(fCode )  !! See if cost to be inserted is a "BOOKING" cost
.head 7 +  If sCodeAmt = 'C'
.head 8 -  Set sCodeAmt = 'STATE'
.head 8 -  Set sCodeDesc = 'DESCRIPTION2'
.head 8 +  If fCount = NUMBER_Null
.head 9 -  Set fCount = C_Count_State ( )
.head 7 +  Else If sCodeAmt = 'D'
.head 8 -  Set sCodeAmt = 'CITY'
.head 8 -  Set sCodeDesc = 'DESCRIPTION'
.head 8 +  If fCount = NUMBER_Null
.head 9 -  Set fCount = C_Count_City ( )
.head 7 +  Else
.head 8 -  Call C_Error( 'Primary charge not found.  There was either a problem finding a primary charge for this case, or the type is not correct.

  The type should be a "C" or a "D"

Contact a supervisor.' )
.head 8 -  Return FALSE
.head 7 +  If fCity_State_Code != ''
.head 8 -  Set sWhereC2S2Code = ' and '||sCodeAmt||" = '"||fCity_State_Code||"' "
.head 7 +  Else
.head 8 -  Set sWhereC2S2Code = ' '
.head 7 -  Set sInsertCost = "INSERT INTO		cr_costs
				(caseyr, casety, caseno, description,
				 timestamped, title, costs, userid, ratnum, dock_codes)
			SELECT 		:fCaseYr, :fCaseTy, :fCaseNo, "|| sCodeDesc || ",
					:dCurrentDate ," || sCodeAmt || ", ((1 *cost) + ((:fCount - 1) * costx)), :nUserId, cr_costs_ratnum_seq.nextval, :sCost_Type
			 FROM		dock_code_costs
			 WHERE		code =:fCode and
					((effective_end_date is null and effective_beg_date <= sysdate) or
                                			(effective_end_date >= sysdate and effective_beg_date < sysdate))"||' '
					||sWhereC2S2Code
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, sInsertCost)
.head 7 +  If bCommit
.head 8 -  Call C_Commit( )
.head 5 +  Function: C_Get_Primary_Charge_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'C'    -      STATE
		 'D'   -        CITY
		 '~'   -         NO PRIMARY CHARGE EXISTS
.head 6 +  Returns
.head 7 -  String: sChargeType
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sChargeType
.head 6 +  Actions
.head 7 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	cr_charge
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  Set sChargeType = '~'
.head 7 -  Return sChargeType
.head 5 +  Function: C_Get_Cost_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'BOOK' - for initial booking costs (BOOK, CITY...STATE...)
.head 6 +  Returns
.head 7 -  String: sChargeType
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fCostType
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	cost_type
			               FROM	dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCostType')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  ! Set fCostType = '~'
.head 7 -  Return fCostType
.head 5 +  Function: C_Get_Cost_Amt
.head 6 -  Description: Function  to return the cost amount of a given dock_code_cost Code
	Returns:      amount
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: fCode
.head 7 -  String: fCity
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnCost
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	cost
			               FROM	dock_code_costs
			               WHERE	code =:fCode and city = :fCity and
					((effective_end_date is null and effective_beg_date <= sysdate) or
                                			(effective_end_date >= sysdate and effective_beg_date < sysdate))
   			               INTO	:fReturnCost')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 -  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 7 -  Return fReturnCost
.head 5 +  Function: C_Error
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fErrorType
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SalMessageBox( fErrorType, 'Error in Inserting Cost', MB_Ok )
.head 5 +  Function: C_Count_City
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountCity
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCity
.head 6 +  Actions
.head 7 -  Set sSelect = "SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 -  Return nCountCity
.head 5 +  Function: C_Count_State
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountState
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountState
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountState
.head 5 +  Function: C_Count_All
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountAll
.head 7 -  String: fCondition
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	condition
			               FROM	dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCondition')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If fCondition = ''
.head 8 -  Set fCondition = '1=1'
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 +  If nCountAll < 1
.head 8 -  ! Call C_Error( 'No Charges found for Case.' )
.head 8 -  Return FALSE
.head 7 +  Else
.head 8 -  Return nCountAll
.head 5 +  Function: C_Count_Charge
.head 6 -  Description: -Count # of specific type of charge
	-Parameter is the type of charge ('MM', 'M1',...)
	-Returns count
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fChargeType
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCharge
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountCharge
.head 5 +  Function: Costs_Constructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: CaseYr
.head 7 -  String: CaseTy
.head 7 -  String: CaseNo
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bLogin = TRUE
.head 8 +  If bCostClass = FALSE
.head 9 -  Set bCostClass = SqlConnect( hSqlCost )
.head 9 -  Call SqlSetResultSet( hSqlCost, TRUE )
.head 8 -  Set nFetchResult = 0
.head 8 -  Set dtHB562Date = SalStrToDate( '9-22-2008')
.head 8 -  Set sSelect = ''
.head 8 -  Set fCaseYr = CaseYr
.head 8 -  Set fCaseNo = CaseNo
.head 8 -  Set fCaseTy = CaseTy
.head 5 +  Function: Costs_Destructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bCostClass = TRUE
.head 8 -  Call SqlDisconnect( hSqlCost )
.head 5 +  Function: C_Is_Moving
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: sMoving
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sMoving
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT 	s2.moving
	       FROM		cr_charge s1,
			crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 7 -  ! Check reduced table
.head 7 -  Set sSelect = 'SELECT 	s2.moving
	       FROM		cr_reduced s1,
			crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 5 +  Function: C_Get_Book_Date
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Date/Time:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dtBookDate
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT 	BOOKDATE
	       FROM	muni_booking
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo
	       INTO		:dtBookDate'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult)
.head 7 -  Return dtBookDate
.head 5 +  Function: C_Cost_Exists
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fCode	!!! Cost Code to Check for
		Pass in the code with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('N%', '', NUMBER_Null)
				The above will find all costs for the case where the
				title is like 'N%'
.head 7 -  String: fDesc	!!! Cost Description to check for
		  Pass in the description with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('NG', ':%TRIAL', NUMBER_Null)
				The above will find all costs for the case where the
				title is 'NG' and the description is like '%TRIAL'
.head 7 -  Number: fCost	!!! Cost Amount to check for
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :fCaseNo and
			casety = :fCaseTy and
			caseyr = :fCaseYr  '
.head 7 +  If fCode != ''
.head 8 +  If SalStrLeftX(fCode, 1) = ':'
.head 9 -  Set fCode = SalStrReplaceX(fCode, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and title like :fCode '
.head 8 +  Else
.head 9 -  Set fWhere = fWhere ||' and title = :fCode '
.head 7 +  If fDesc != ''
.head 8 +  If SalStrLeftX(fDesc, 1) = ':'
.head 9 -  Set fDesc = SalStrReplaceX(fDesc, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and description like :fDesc '
.head 8 +  Else
.head 9 -  Set fWhere = fWhere ||' and description = :fDesc '
.head 7 +  If fCost != NUMBER_Null
.head 8 -  Set fWhere = fWhere ||' and costs = :fCost '
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, 'SELECT	caseno
			      FROM		cr_costs '||fWhere)
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: C_Commit
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bReturn
.head 6 +  Actions
.head 7 -  Set bReturn  = SqlCommit( hSqlCost )
.head 7 -  Return bReturn
.head 3 +  Functional Class: cEmailBlat
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sSMTP_Server
.head 5 -  String: sGlobEmail1
.head 5 -  String: sGlobFileName
.head 5 -  String: sGlobFilePath
.head 5 -  String: sGlobFromEmail
.head 5 -  String: sTitle
.head 5 -  String: sMessage
.head 5 -  String: sGlobBlatPath
.head 4 +  Functions
.head 5 +  Function: GetSMTPServer
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sReturnServer
.head 7 -  Number: nFetch
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT	SMTP_SERVER
			               INTO	:sReturnServer
			               FROM	CourtInfo' )
.head 7 -  Call SqlFetchNext( hSql, nFetch )
.head 7 -  Return sReturnServer
.head 5 +  Function: GetSendEmail
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sReturn
.head 7 -  Number: nFetch
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT	EMAIL_RPT_ADDRESS
			               INTO	:sReturn
			               FROM	CourtInfo' )
.head 7 -  Call SqlFetchNext( hSql, nFetch )
.head 7 -  Return sReturn
.head 5 +  Function: SendEmail
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  Boolean: bViewOnly   !TRUE if only want to return parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sParam
.head 7 -  String: sReplace
.head 7 -  File Handle: hFile
.head 6 +  Actions
.head 7 -  Call SalWaitCursor( TRUE )
.head 7 -  !
.head 7 -  ! Need to write "Body" to text file
.head 7 -  Call SalFileOpen( hFile, 'temp.txt',OF_Create| OF_Write)
.head 7 -  Call SalFilePutStr( hFile, sMessage )
.head 7 -  Call SalFileClose( hFile)
.head 7 -  !
.head 7 -  !
.head 7 -  Set sParam = '*'|| sGlobBlatPath ||'*  *temp.txt* -subject *'||sTitle||'* -to *'|| sGlobEmail1 ||'*  -server '|| sSMTP_Server ||'  -f '||  sGlobFromEmail 
.head 7 +  If sGlobFilePath != ''
.head 8 -  Set sParam = sParam|| '  -attach '|| sGlobFilePath ||'\\#.#'
.head 7 -  Set sReplace = '"'
.head 7 -  Set sParam = VisStrSubstitute( sParam, '*', sReplace )
.head 7 -  Set sReplace = '*'
.head 7 -  Set sParam = VisStrSubstitute( sParam, '#', sReplace )
.head 7 +  If bViewOnly
.head 8 -  Return sParam
.head 7 +  Else
.head 8 -  Call SalLoadAppAndWait( sParam, Window_NotVisible, nReturn)
.head 7 -  Call SalWaitCursor( FALSE )
.head 5 +  Function: GetAddresses
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: sList
.head 6 +  Parameters
.head 7 -  String: fProcessName
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sList
.head 7 -  String: sAddress
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	address
			       FROM	cr_report_email
			       INTO	:sAddress
			       WHERE	process_name = :fProcessName
			       ORDER BY	seq')
.head 7 +  While SqlFetchNext(hSql, nReturn)
.head 8 +  If sList != ''
.head 9 -  Set sList = sList||','|| sAddress
.head 8 +  Else
.head 9 -  Set sList = sAddress
.head 7 -  Return sList
.head 5 +  Function: GetImagePath
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Sql Handle: hSqlImage
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnImage
.head 7 -  String: sLocation
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT	path
				   FROM		cr_image_location
				    WHERE	type = :fType
				    INTO		:sLocation')
.head 7 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 7 -  Return sLocation
.head 5 +  Function: Construct
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sSMTP_Server = GetSMTPServer( )
.head 7 -  Set sGlobFromEmail = GetSendEmail( )
.head 7 -  Set sGlobBlatPath = GetImagePath('BLAT', hSql)
.head 3 +  Child Grid Class: cGrid
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  Class Default
.head 6 -  Width Editable? Yes
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Black
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? Class Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 100000
.head 5 -  Discardable? No
.head 4 -  Next Class Child Key: 0
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? Class Default
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Contents
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Boolean: bDisableC
.head 5 -  ! ! ! !! << EXPORT VAR >>
.head 5 -  String: fGlobTitle
.head 5 -  String: fGlobFileName
.head 5 -  Number: nColSelectCount
.head 5 -  Number: nColTotal
.head 4 +  Functions
.head 5 +  Function: PrintTable
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Window Handle: hWndTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nError
.head 7 -  Number: nParamArray[*]
.head 6 +  Actions
.head 7 -  Call SalReportTableCreate( 'PrnTbl.qrp', hWndTable, nError )
.head 7 +  If fType = 'PRINTER'
.head 8 -  Call SalReportTablePrint( hWndTable, 'PrnTbl.qrp', nParamArray , nError )
.head 7 +  Else If fType = 'SCREEN'
.head 8 -  Call SalReportTableView( hWndTable, hWndNULL, 'PrnTbl.qrp' , nError )
.head 7 -  Call VisFileDelete(  'PrnTbl.qrp' )
.head 5 +  Function: AutoSize
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bFound
.head 7 -  Number: nColID
.head 7 -  Window Handle: hWndColumn
.head 6 +  Actions
.head 7 -  Set bFound = FALSE
.head 7 -  Set nColID = 1
.head 7 -  Set hWndColumn = SalTblGetColumnWindow( hTable, nColID, COL_GetID )
.head 7 +  While hWndColumn
.head 8 +  If SalTblQueryColumnFlags( hWndColumn, COL_Selected )
.head 9 -  Call VisTblAutoSizeColumn( hTable, hWndColumn )
.head 9 -  Set bFound = TRUE
.head 9 -  Break
.head 8 -  Set nColID = nColID + 1
.head 8 -  Set hWndColumn = SalTblGetColumnWindow( hTable, nColID, COL_GetID )
.head 7 +  If Not bFound
.head 8 -  Call VisTblAutoSizeColumn( hTable, hWndNULL )
.head 5 +  Function: ChangeFont
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 7 -  Number: nNewFont
.head 7 -  String: fType
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sFont
.head 7 -  Number: nSize
.head 7 -  Number: nEnh
.head 7 -  Number: nColor
.head 6 +  Actions
.head 7 +  If fType = 'DIALOG'
.head 8 +  If SalDlgChooseFont( hWndForm, sFont, nSize, nEnh, nColor )
.head 9 -  Call SalFontSet( hTable, sFont, nSize, nEnh )
.head 7 +  Else If fType = 'FONT_SIZE'
.head 8 -  Call SalFontSet( hTable, 'Arial', nNewFont, 0 )
.head 5 +  Function: RemoveColor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nRow
.head 7 -  Boolean: bOk
.head 6 +  Actions
.head 7 -  Call SalWaitCursor( TRUE )
.head 7 -  Set nRow = 0
.head 7 -  Set bOk = SalTblSetContext( hTable, nRow)
.head 7 +  While bOk
.head 8 -  ! Call MTblSetRowBackColor( hTable, nRow, COLOR_White, MTSC_REDRAW)
.head 8 -  Call VisTblSetRowColorEx(hTable, 1, COLOR_3DFace, COLOR_White)
.head 8 -  Call Inc(nRow)
.head 8 -  Set bOk = SalTblSetContext( hTable, nRow)
.head 7 -  Call SalWaitCursor( FALSE )
.head 5 +  Function: GetColTotals
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nTotal
.head 6 +  Parameters
.head 7 -  Number: nTableID
.head 7 -  Number: nColID
.head 7 -  Receive Number: nRowSelectCount
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nRow
.head 7 -  Boolean: bOk
.head 7 -  Number: nTotal
.head 7 -  String: sColHeader
.head 7 -  String: sColText
.head 7 -  Window Handle: hTblHndl
.head 7 -  Window Handle: hColHndl
.head 7 -  Number: nRowSave
.head 6 +  Actions
.head 7 -  Set nRowSelectCount = NUMBER_Null
.head 7 -  !
.head 7 -  Set hTblHndl = SalNumberToWindowHandle (nTableID)
.head 7 -  Set hColHndl = SalNumberToWindowHandle (nColID)
.head 7 -  !
.head 7 -  Call SalTblGetColumnTitle( hColHndl, sColHeader, 100 )
.head 7 -  !
.head 7 -  Set nRowSave = SalTblQueryContext( hTblHndl )
.head 7 -  Set nRow = 0
.head 7 -  Set bOk = SalTblSetContext( hTblHndl, nRow)
.head 7 +  While bOk
.head 8 +  If SalTblQueryRowFlags( hTblHndl, nRow, ROW_Selected) = TRUE
.head 9 -  Set nTotal = nTotal + SalStrToNumber( VisTblGetCell( hTblHndl, nRow, hColHndl ) )
.head 9 -  Set nRowSelectCount = nRowSelectCount + 1
.head 8 -  Call Inc(nRow)
.head 8 -  Set bOk = SalTblSetContext( hTblHndl, nRow)
.head 7 -  Call SalTblSetContext( hTblHndl, nRowSave)
.head 7 -  Return nTotal
.head 4 +  Message Actions
.head 5 +  On SAM_CreateComplete
.head 6 -  Call SalTblDefineRowHeader( hWndForm, '?', 20, 41, hWndNULL )
.head 6 -  Set hWndTable = hWndForm
.head 5 +  On SAM_CornerClick
.head 6 +  If NOT bDisableC
.head 7 -  Call SalTrackPopupMenu(hWndForm, 'CGRID_MENU', TPM_CursorX | TPM_CursorY, 0, 0 )
.head 5 +  On SAM_User
.head 6 +  If sCTABLE_MENU_TYPE = 'EXPORT'
.head 7 -  Call SalModalDialog( dlgExport, hWndForm, hWndItem, fGlobTitle, fGlobFileName, '', '', '')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'EXPORT_SAVE_LOAD'
.head 7 -  Call SalModalDialog( dlgExport, hWndForm, hWndItem, fGlobTitle, fGlobFileName, 'EXCEL', '', '')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'PRINT_TABLE'
.head 7 -  Call PrintTable( 'PRINTER' , hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'VIEW_TABLE'
.head 7 -  Call PrintTable( 'SCREEN' , hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 7 -  Call CopyRows( 1, hWndItem )
.head 6 +  Else If sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 7 -  Call CopyRows( 2, hWndItem )
.head 6 +  Else If sCTABLE_MENU_TYPE = 'FIND'
.head 7 -  Call SalCreateWindow( frmCTable_Find, hWndForm, hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'AUTO_SIZE'
.head 7 -  Call AutoSize( hWndItem  )
.head 6 +  Else If SalStrLeftX( sCTABLE_MENU_TYPE, 9) = 'FONT_SIZE'
.head 7 -  Call ChangeFont( hWndItem, SalStrToNumber( VisStrSubstitute( sCTABLE_MENU_TYPE, 'FONT_SIZE_', '' ) ), 'FONT_SIZE')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'FONT_DIALOG'
.head 7 -  Call ChangeFont( hWndItem, 0, 'DIALOG')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'REMOVE_COLOR'
.head 7 -  Call RemoveColor( hWndItem )
.head 5 +  On GET_COL_SUM
.head 6 -  Set nColTotal = GetColTotals( wParam, lParam , nColSelectCount )
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Separator:
.head 3 -  Tree Control:
.head 3 -  Navigation Bar:
.head 3 -  Pane Separator:
.head 3 -  Progress Bar:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 +  Application Actions
.head 3 +  On SAM_AppStartup
.head 4 -  Call SalModalDialog( dlgLogin, hWndNULL, 'Login Test' )
.head 3 +  ! On SAM_SqlError
.head 4 -  Call SQL.GetError( )
.head 4 -  Return FALSE
.head 1 +  Dialog Box: dlgLogin
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Criminal Program Login Screen
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.713"
.head 4 -  Top: 1.5"
.head 4 -  Width:  8.643"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 2.115"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: #ff8c57
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description: This window is the initial
login screen
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 28676
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.543"
.head 5 -  Top: 0.875"
.head 5 -  Width:  1.113"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.219"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: #7b7b08
.head 4 -  Title: UserName:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 28677
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.543"
.head 5 -  Top: 1.156"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: #7b7b08
.head 4 -  Title: Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfUser
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cLoginUser
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.7"
.head 6 -  Top: 0.823"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: White
.head 5 -  Input Mask: Class Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( dfUser ) = 10
.head 7 -  Call SalSetFocus( dfPass )
.head 3 +  Data Field: dfPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.7"
.head 6 -  Top: 1.125"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 5861
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.543"
.head 5 -  Top: 1.49"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: #c3c3aa
.head 4 -  Title: Database:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfDB
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.7"
.head 6 -  Top: 1.458"
.head 6 -  Width:  2.071"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Uppercase
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 5.971"
.head 5 -  Top: 1.583"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.26"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If dfUser != '' and dfPass != ''
.head 7 -  Call Login( )
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Missing required information.  Please check that the User Name and Password are entered and try again.', 'Missing Info', MB_IconStop)
.head 7 -  Return FALSE
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton( pbOk )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 7.3"
.head 5 -  Top: 1.583"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.26"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalQuit(  )
.head 3 +  Data Field: dfCourt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.229"
.head 6 -  Top: 0.156"
.head 6 -  Width:  6.357"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.625"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Copperplate Gothic Light
.head 5 -  Font Size: 18
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: White
.head 5 -  Background Color: #7b7b08
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChangeDATABASE
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F9
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SqlUser = 'RYANP' or SqlUser = 'SUZIE'
.head 7 -  Call SalModalDialog(dlgDatasource,  hWndForm, SqlDatabase)
.head 7 -  Set dfDB = SqlDatabase
.head 3 +  Picture: pic1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: -0.014"
.head 5 -  Top: -0.01"
.head 5 -  Width:  8.614"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.073"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name: O:\apps\images\LoginM.bmp
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Scale
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 3 +  Check Box: cbTestDB
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Connect To Test DB
.head 4 -  Window Location and Size
.head 5 -  Left: 3.729"
.head 5 -  Top: 1.708"
.head 5 -  Width:  2.057"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: #c3c3aa
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  Function: Login
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAccountStatus
.head 4 +  Actions
.head 5 -  ! Call SQL.SetClassVar( 'LOG', TRUE, hWndForm)
.head 5 +  ! When SqlError
.head 6 -  ! Call SQL.SetClassVar( 'LOG', TRUE)
.head 6 -  Call SQL.GetError( )
.head 6 -  Return FALSE
.head 5 -  Set dfPass = SalStrTrimX( dfPass)
.head 5 -  Set dfUser = SalStrTrimX( dfUser)
.head 5 -  Set dfPass = SalStrUpperX( dfPass )
.head 5 -  Set SqlDatabase = dfDB
.head 5 +  If SqlDatabase = ''
.head 6 -  Set SqlDatabase = 'CRIM'
.head 5 -  Set SqlUser = dfUser
.head 5 -  Set SqlPassword = dfPass
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 +  When SqlError
.head 6 -  Return FALSE
.head 5 -  Set bLogin = SqlConnect (hSql)
.head 5 +  If Not bLogin
.head 6 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 6 -  ! Call SQL.GetError( )
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalSetFocus( dfUser )
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Call SqlSetResultSet( hSql, TRUE )
.head 6 +  If SalStrLeftX( SqlDatabase, 4) = 'CRIM'
.head 7 -  Call Startup.GetCourtInfo(  )
.head 7 -  Call Startup.GetUserInfo( SqlUser )
.head 7 -  Call Startup.Check_Change_Password(  )
.head 5 -  Call Startup.WriteParam('', 'LastUser', SqlUser)
.head 5 +  If cbTestDB
.head 6 -  Call Startup.WriteParam( '', 'Test', 'Y')
.head 6 -  Call Startup.WriteParam( '', 'TestDB', dfDB)
.head 5 +  Else
.head 6 -  Call Startup.WriteParam( '', 'Test', '')
.head 6 -  Call Startup.WriteParam( '', 'Database', dfDB)
.head 5 -  Call SalEndDialog( dlgLogin, 0 )
.head 5 -  ! Set sAccountStatus = Startup.GetLockInfo(  )
.head 5 +  ! If SalStrLeftX( sAccountStatus, 6) = 'LOCKED'
.head 6 -  ! Call SalMessageBox( 'Error Connecting to Database - USER ACCOUNT LOCKED DUE TO MAX INVALID LOGIN ATTEMPTS REACHED.',
'Contact Administrator', MB_IconExclamation|MB_IconHand )
.head 6 -  ! Call SalWaitCursor( FALSE )
.head 6 -  ! Call SalSetFocus( dfUser )
.head 6 -  ! Return TRUE
.head 5 -  ! Else If sAccountStatus = 'ERROR'
.head 5 -  ! Else
.head 2 +  Window Parameters
.head 3 -  String: sWindowTitle
.head 2 +  Window Variables
.head 3 -  String: sLoginTitle
.head 3 -  Number: nFetch
.head 3 -  String: sPublic
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow( dlgLogin )
.head 4 -  Call Startup.GetStartupParam(  )
.head 4 +  If Startup.sSelect_S = ''
.head 5 -  Call SalMessageBox('Error reading Where Clause from INI File,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 5 -  Call SalQuit( )
.head 4 +  If Startup.sPublic_S = 'Y'
.head 5 -  Call SalMessageBox( 'Access has been denied', 'Error', MB_Ok|MB_IconExclamation )
.head 5 -  Call SalQuit(  )
.head 4 -  Set dfCourt = Startup.sCourtTitle_S
.head 4 +  If sWindowTitle != STRING_Null
.head 5 -  Call SalSetWindowText( hWndForm, sWindowTitle )
.head 4 -  Call IsBackup( )
.head 4 +  If Startup.sTestUser_S= 'Y'
.head 5 +  ! If SqlDatabase != ''
.head 6 -  Set SqlDatabase = SqlDatabase || '_ba'
.head 5 +  ! Else
.head 6 -  Set SqlDatabase = 'Crim_ba'
.head 5 -  Set dfDB = Startup.sTestDB_S
.head 5 -  Set dfUser = Startup.sLastUser_S
.head 5 -  Set dfPass = 'courts'
.head 5 -  Set cbTestDB = TRUE
.head 5 -  ! Call SalSendMsg( pbOk, SAM_Click, 0, 0 )
.head 4 +  Else
.head 5 -  Set dfDB = Startup.sDB_S
.head 5 -  Set cbTestDB = FALSE
.head 4 +  If dfDB = ''
.head 5 -  Set dfDB = 'CRIM'
.head 3 +  On SAM_CreateComplete
.head 4 +  If dfUser = 'RYANP' or dfUser = 'SUZIE'
.head 5 -  Call SalShowWindowAndLabel( dfDB )
.head 5 -  Call SalShowWindow( cbTestDB )
.head 4 +  Else
.head 5 -  Call SalHideWindowAndLabel( dfDB )
.head 5 -  Call SalHideWindow( cbTestDB )
.head 4 -  Call SalSetFocus( dfPass )
.head 1 +  Dialog Box: dlgLoginR  !!!! Remote Login Screen
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Criminal Program Login Screen
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 5.05"
.head 4 -  Top: 2.333"
.head 4 -  Width:  6.971"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.625"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: #ff8c57
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description: This window is the initial
login screen
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 24494
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.229"
.head 5 -  Top: 0.469"
.head 5 -  Width:  1.113"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.219"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Title: UserName:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 24495
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.229"
.head 5 -  Top: 0.75"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Title: Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfUser
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cLoginUser
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.386"
.head 6 -  Top: 0.417"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: White
.head 5 -  Input Mask: Class Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( dfUser ) = 10
.head 7 -  Call SalSetFocus( dfPass )
.head 3 +  Data Field: dfPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.386"
.head 6 -  Top: 0.719"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 24496
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.229"
.head 5 -  Top: 1.083"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Title: Database:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfDB
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.386"
.head 6 -  Top: 1.052"
.head 6 -  Width:  2.071"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Uppercase
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 3.657"
.head 5 -  Top: 1.177"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.26"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If dfUser != '' and dfPass != ''
.head 7 -  Call Login( )
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Missing required information.  Please check that the User Name and Password are entered and try again.', 'Missing Info', MB_IconStop)
.head 7 -  Return FALSE
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton( pbOk )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 4.986"
.head 5 -  Top: 1.177"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.26"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalQuit(  )
.head 3 +  Pushbutton: pbChangeDATABASE
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F9
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SqlUser = 'RYANP' or SqlUser = 'SUZIE'
.head 7 -  Call SalModalDialog(dlgDatasource,  hWndForm, SqlDatabase)
.head 7 -  Set dfDB = SqlDatabase
.head 3 +  Picture: pic1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: -0.014"
.head 5 -  Top: -0.01"
.head 5 -  Width:  8.614"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.073"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name:
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Scale
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 3 +  Data Field: dfCourt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.7"
.head 6 -  Top: 0.021"
.head 6 -  Width:  6.357"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.365"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Copperplate Gothic Light
.head 5 -  Font Size: 18
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: White
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Check Box: cbTestDB
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Connect To Test DB
.head 4 -  Window Location and Size
.head 5 -  Left: 1.414"
.head 5 -  Top: 1.302"
.head 5 -  Width:  2.057"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  Function: Login
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAccountStatus
.head 4 +  Actions
.head 5 -  ! Call SQL.SetClassVar( 'LOG', TRUE, hWndForm)
.head 5 +  ! When SqlError
.head 6 -  ! Call SQL.SetClassVar( 'LOG', TRUE)
.head 6 -  Call SQL.GetError( )
.head 6 -  Return FALSE
.head 5 -  Set dfPass = SalStrTrimX( dfPass)
.head 5 -  Set dfUser = SalStrTrimX( dfUser)
.head 5 -  Set dfPass = SalStrUpperX( dfPass )
.head 5 -  Set SqlDatabase = dfDB
.head 5 +  If SqlDatabase = ''
.head 6 -  Set SqlDatabase = 'CRIM'
.head 5 -  Set SqlUser = dfUser
.head 5 -  Set SqlPassword = dfPass
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 +  When SqlError
.head 6 -  Return FALSE
.head 5 -  Set bLogin = SqlConnect (hSql)
.head 5 +  If Not bLogin
.head 6 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 6 -  ! Call SQL.GetError( )
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalSetFocus( dfUser )
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Call SqlSetResultSet( hSql, TRUE )
.head 6 +  If SalStrLeftX( SqlDatabase, 4) = 'CRIM'
.head 7 -  Call Startup.GetCourtInfo(  )
.head 7 -  Call Startup.GetUserInfo( SqlUser )
.head 7 -  Call Startup.Check_Change_Password(  )
.head 5 -  Call Startup.WriteParam('', 'LastUser', SqlUser)
.head 5 +  If cbTestDB
.head 6 -  Call Startup.WriteParam( '', 'Test', 'Y')
.head 6 -  Call Startup.WriteParam( '', 'TestDB', dfDB)
.head 5 +  Else
.head 6 -  Call Startup.WriteParam( '', 'Test', '')
.head 6 -  Call Startup.WriteParam( '', 'Database', dfDB)
.head 5 -  Call SalEndDialog( dlgLoginR, 0 )
.head 5 -  ! Set sAccountStatus = Startup.GetLockInfo(  )
.head 5 +  ! If SalStrLeftX( sAccountStatus, 6) = 'LOCKED'
.head 6 -  ! Call SalMessageBox( 'Error Connecting to Database - USER ACCOUNT LOCKED DUE TO MAX INVALID LOGIN ATTEMPTS REACHED.',
'Contact Administrator', MB_IconExclamation|MB_IconHand )
.head 6 -  ! Call SalWaitCursor( FALSE )
.head 6 -  ! Call SalSetFocus( dfUser )
.head 6 -  ! Return TRUE
.head 5 -  ! Else If sAccountStatus = 'ERROR'
.head 5 -  ! Else
.head 2 +  Window Parameters
.head 3 -  String: sWindowTitle
.head 2 +  Window Variables
.head 3 -  String: sLoginTitle
.head 3 -  Number: nFetch
.head 3 -  String: sPublic
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow( dlgLoginR )
.head 4 -  Call Startup.GetStartupParam(  )
.head 4 +  If Startup.sSelect_S = ''
.head 5 -  Call SalMessageBox('Error reading Where Clause from INI File,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 5 -  Call SalQuit( )
.head 4 +  If Startup.sPublic_S = 'Y'
.head 5 -  Call SalMessageBox( 'Access has been denied', 'Error', MB_Ok|MB_IconExclamation )
.head 5 -  Call SalQuit(  )
.head 4 -  Set dfCourt = Startup.sCourtTitle_S
.head 4 +  If sWindowTitle != STRING_Null
.head 5 -  Call SalSetWindowText( hWndForm, sWindowTitle )
.head 4 -  Call IsBackup( )
.head 4 +  If Startup.sTestUser_S= 'Y'
.head 5 +  ! If SqlDatabase != ''
.head 6 -  Set SqlDatabase = SqlDatabase || '_ba'
.head 5 +  ! Else
.head 6 -  Set SqlDatabase = 'Crim_ba'
.head 5 -  Set dfDB = Startup.sTestDB_S
.head 5 -  Set dfUser = Startup.sLastUser_S
.head 5 -  Set dfPass = 'courts'
.head 5 -  Set cbTestDB = TRUE
.head 5 -  ! Call SalSendMsg( pbOk, SAM_Click, 0, 0 )
.head 4 +  Else
.head 5 -  Set dfDB = Startup.sDB_S
.head 5 -  Set cbTestDB = FALSE
.head 4 +  If dfDB = ''
.head 5 -  Set dfDB = 'CRIM'
.head 3 +  On SAM_CreateComplete
.head 4 +  If dfUser = 'RYANP' or dfUser = 'SUZIE'
.head 5 -  Call SalShowWindowAndLabel( dfDB )
.head 5 -  Call SalShowWindow( cbTestDB )
.head 4 +  Else
.head 5 -  Call SalHideWindowAndLabel( dfDB )
.head 5 -  Call SalHideWindow( cbTestDB )
.head 4 -  Call SalSetFocus( dfPass )
.head 1 +  Dialog Box: dlgPasswordAPL
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Change Password
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.163"
.head 4 -  Top: 1.813"
.head 4 -  Width:  4.06"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.921"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Data Field: dfCurrentPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.242"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfNewPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.575"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfVerifyPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.908"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChange
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Change Password
.head 4 -  Window Location and Size
.head 5 -  Left: 0.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalStrLength( dfNewPass ) < 6
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('A Password Must be at Least 6 Charaters Long', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX(dfNewPass) != SalStrUpperX(dfVerifyPass)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Passwords do not Match', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX(dfCurrentPass) != SalStrUpperX(SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Current Password must be Re-entered before Password can be Changed','Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX( dfVerifyPass) =SalStrUpperX(  SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox( 'Password Must be UNIQUE, please re-enter a valid password', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  ! Else If nCheckIllegal > -1
.head 7 -  Call SalMessageBox( "Password can not contain illegal characters such as
 	(. ; : ! @ # $ % ^ & * ( ) - _ + = ~ ` ' \" ? / \ | < > , ).
Please re-enter a valid password", 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SqlConnect (hSql)
.head 6 +  If SqlPLSQLCommand(hSql, 'change_pwd(SqlUser,dfNewPass)' )
.head 7 -  Call SalMessageBox( 'Password Successfully Changed', 'Change Password',MB_Ok|MB_IconExclamation)
.head 7 -  Call SqlPrepareAndExecute( hSql,"Update users set change_password = 'N' ,gracelogins = 5 where "|| Startup.sSelect_S)
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Set SqlPassword = dfNewPass
.head 7 -  Set bChange = FALSE
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Password Change failed - - Contact Programmer', 'Change Password',MB_Ok|MB_IconStop)
.head 6 -  ! Do Civil
.head 6 -  ! Call SqlDisconnect( hSql )
.head 6 -  ! Set SqlDatabase = 'CIVIL'
.head 6 -  ! Set SqlPassword = dfCurrentPass
.head 6 -  ! Call SqlConnect (hSql)
.head 6 +  ! If SqlPLSQLCommand(hSql, 'change_pwd(SqlUser,dfNewPass)' )
.head 7 -  Call SalMessageBox( 'Password Successfully Changed', 'Change Password',MB_Ok|MB_IconExclamation)
.head 7 -  Call SqlPrepareAndExecute( hSql,"Update users set change_password = 'N' ,gracelogins = 5 where "|| Startup.sSelect_S)
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Set SqlPassword = dfNewPass
.head 7 -  Set bChange = FALSE
.head 6 +  ! Else
.head 7 -  Call SalMessageBox( 'Password Change failed - - Contact Programmer', 'Change Password',MB_Ok|MB_IconStop)
.head 6 -  ! Call SqlDisconnect( hSql )
.head 6 -  ! Change Database Back
.head 6 -  Set SqlDatabase = 'CRIM'
.head 6 -  Call SqlConnect (hSql)
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton(pbChange)
.head 6 -  Call SalSetFocus(dfCurrentPass)
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 2.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 52744
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.258"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Current Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd4
.head 4 -  Resource Id: 52745
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.592"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: New Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 52746
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.925"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Retype Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nErrorCnt
.head 3 -  Boolean: bOk
.head 3 -  Number: nCheckIllegal
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set nErrorCnt = 0
.head 4 -  ! Call CSCDisableNonEditable( hWndForm )
.head 4 -  Call SalWaitCursor( FALSE )
.head 3 +  On SAM_Destroy
.head 4 +  If bChange = TRUE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'update users set gracelogins = gracelogins - 1 where username = :SqlUser' )
.head 5 -  Call SqlCommit( hSql )
.head 5 -  Set bChange = FALSE
.head 1 +  Form Window: frmCTable_Find
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Find Entry
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Normal
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 2.225"
.head 4 -  Top: 2.24"
.head 4 -  Width:  6.243"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.489"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 52524
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.386"
.head 5 -  Top: 0.188"
.head 5 -  Width:  1.038"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Find:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfCTable_FindText
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.886"
.head 6 -  Top: 0.125"
.head 6 -  Width:  3.471"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 -  Set bCTableFound = FALSE
.head 3 +  Pushbutton: pbCTable_Find
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Find Next
.head 4 -  Window Location and Size
.head 5 -  Left: 4.371"
.head 5 -  Top: 0.135"
.head 5 -  Width:  0.957"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.24"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalTblSetRowFlags( hWndTbl, nLastFind - 1, ROW_Selected, FALSE )
.head 6 +  If rbCTableAll
.head 7 -  Set nRowFind =  VisTblFindString( hWndTbl,nLastFind, hWndNULL, dfCTable_FindText )
.head 7 -  Call SalTblSetRowFlags( hWndTbl, nRowFind, ROW_Selected, TRUE )
.head 6 +  Else
.head 7 -  Set nRowFind =  VisTblFindString( hWndTbl,nLastFind, CTable_GetSelected(  ), dfCTable_FindText )
.head 7 -  Call SalTblSetRowFlags( hWndTbl, nRowFind, ROW_Selected, TRUE )
.head 6 +  If nRowFind < 0
.head 7 +  If bCTableFound =  FALSE
.head 8 -  Call SalMessageBox('Finished searching Table, no matches found', 'Search Results', MB_Ok|MB_IconInformation)
.head 8 -  Set nRowFind = 0
.head 8 -  Set nLastFind = 0
.head 7 +  Else
.head 8 -  Call SalTblSetFocusRow( hWndTbl, nRowFind )
.head 8 -  Set nLastFind = nRowFind + 1
.head 8 -  Set bCTableFound = TRUE
.head 6 +  Else
.head 7 -  Call SalTblSetFocusRow( hWndTbl, nRowFind )
.head 7 -  Set nLastFind = nRowFind + 1
.head 7 -  Set bCTableFound = TRUE
.head 6 -  Call SalSetFocus( pbCTable_Find )
.head 3 +  Radio Button: rbCTableAll
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: All Columns
.head 4 -  Window Location and Size
.head 5 -  Left: 0.271"
.head 5 -  Top: 0.563"
.head 5 -  Width:  1.257"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDisableWindow( cmbCTable_Col)
.head 3 +  Radio Button: rbCTableSelect
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Select Column
.head 4 -  Window Location and Size
.head 5 -  Left: 1.543"
.head 5 -  Top: 0.563"
.head 5 -  Width:  1.386"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEnableWindow( cmbCTable_Col)
.head 6 -  Call SalSetFocus( cmbCTable_Col)
.head 3 +  Combo Box: cmbCTable_Col
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.957"
.head 5 -  Top: 0.573"
.head 5 -  Width:  2.729"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.875"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? Yes
.head 4 -  String Type: String
.head 4 -  Maximum Data Length: Default
.head 4 -  Sorted? No
.head 4 -  Always Show List? No
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  AutoFill? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  List Initialization
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call CTable_GetSelected( )
.head 6 -  Set bCTableFound = FALSE
.head 3 +  Pushbutton: pbExitHide
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Exit Hide
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDestroyWindow( frmCTable_Find)
.head 2 +  Functions
.head 3 +  Function: CTable_FillCmb
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 4 +  Actions
.head 5 -  Set nCol = 1
.head 5 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 5 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 6 +  If sText != ''
.head 7 -  Call SalListAdd( cmbCTable_Col, '('|| SalNumberToStrX(nCol, 0) ||') '||sText )
.head 5 +  While sText != ''
.head 6 -  Set nCol = nCol + 1
.head 6 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 6 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 7 +  If sText != ''
.head 8 -  Call SalListAdd( cmbCTable_Col, '('|| SalNumberToStrX(nCol, 0) ||') '||sText )
.head 3 +  Function: CTable_GetSelected
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Window Handle: hWndCol
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nOffSet
.head 5 -  Number: nColSelected
.head 5 -  Window Handle: hWndCol
.head 4 +  Actions
.head 5 -  Set nOffSet = SalStrScan( cmbCTable_Col, ')')
.head 5 -  Set nOffSet = nOffSet -  1
.head 5 -  Set nColSelected = SalStrToNumber(SalStrMidX( cmbCTable_Col, 1, nOffSet))
.head 5 -  Set hWndCol = SalTblGetColumnWindow( hWndTbl, nColSelected, COL_GetPos )
.head 5 -  Return hWndCol
.head 2 +  Window Parameters
.head 3 -  Window Handle: hWndTbl
.head 2 +  Window Variables
.head 3 -  Number: nRowFind
.head 3 -  Number: nLastFind
.head 3 -  Boolean: bCTableFound
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CTable_FillCmb(  )
.head 4 -  Call SalDisableWindow( cmbCTable_Col)
.head 4 -  Set rbCTableAll = TRUE
.head 4 -  Call SalSetDefButton( pbCTable_Find)
.head 1 +  Dialog Box: dlgAbout
.head 2 -  Class: cQuickTabsDialog
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Class Default
.head 2 -  Display Settings
.head 3 -  Display Style? Class Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Class Default
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.8"
.head 4 -  Top: 1.604"
.head 4 -  Width:  8.4"
.head 4 -  Width Editable? Class Default
.head 4 -  Height: 3.857"
.head 4 -  Height Editable? Class Default
.head 3 -  Absolute Screen Location? Class Default
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 8
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Class Default
.head 3 -  Background Color: Class Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Class Default
.head 4 -  Location? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Size: Class Default
.head 4 -  Size Editable? Class Default
.head 4 -  Docking Toolbar? Class Default
.head 4 -  Toolbar Docking Orientation: Class Default
.head 4 -  Font Name: Class Default
.head 4 -  Font Size: Class Default
.head 4 -  Font Enhancement: Class Default
.head 4 -  Text Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  Resizable? Class Default
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Picture: picTabs
.data CLASSPROPS
0000: 5400610062005400 6F0070004D006100 7200670069006E00 0000FFFE04003000
0020: 0000540061006200 46006F0072006D00 5000610067006500 73000000FFFE0600
0040: 0900090000005400 6100620044007200 6100770053007400 79006C0065000000
0060: FFFE160057006900 6E00390035005300 740079006C006500 0000540061006200
0080: 42006F0074007400 6F006D004D006100 7200670069006E00 0000FFFE04003000
00A0: 0000540061006200 5000610067006500 43006F0075006E00 74000000FFFE0400
00C0: 3100000054006100 62004C0061006200 65006C0073000000 FFFE4E004D006100
00E0: 69006E0009004900 6E0063006C007500 6400650020005200 650070006F007200
0100: 7400730009005000 7200650076006900 6F00750073002000 5200650076006900
0120: 730069006F006E00 0000540061006200 4E0061006D006500 73000000FFFE3000
0140: 740062004D006100 69006E0009007400 6200520065007000 6F00720074007300
0160: 0900740062005000 7200650076000000 5400610062005200 6900670068007400
0180: 4D00610072006700 69006E000000FFFE 0400300000005400 6100620043007500
01A0: 7200720065006E00 74000000FFFE0E00 7400620050007200 6500760000005400
01C0: 610062004C006500 660074004D006100 7200670069006E00 0000FFFE04003000
01E0: 0000000000000000 0000000000000000 000000000000
.enddata
.data CLASSPROPSSIZE
0000: F601
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 1
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cQuickTabsDialog
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: Class Default
.head 5 -  Top: Class Default
.head 5 -  Width:  Class Default
.head 5 -  Width Editable? Class Default
.head 5 -  Height: Class Default
.head 5 -  Height Editable? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Editable? Class Default
.head 4 -  File Name:
.head 4 -  Storage: Class Default
.head 4 -  Picture Transparent Color: Class Default
.head 4 -  Fit: Class Default
.head 4 -  Scaling
.head 5 -  Width:  Class Default
.head 5 -  Height:  Class Default
.head 4 -  Corners: Class Default
.head 4 -  Border Style: Class Default
.head 4 -  Border Thickness: Class Default
.head 4 -  Tile To Parent? Class Default
.head 4 -  Border Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 7.033"
.head 5 -  Top: 3.19"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgAbout, 1)
.head 3 -  Background Text: bkgd3
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50572
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.619"
.head 5 -  Width:  1.3"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Program Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd4
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50573
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.976"
.head 5 -  Width:  1.233"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Revision Date:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50574
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 1.333"
.head 5 -  Width:  1.467"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Revision Number:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfAboutName
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 0.619"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAboutRDate
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 0.976"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: DateTime
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAboutRNumber
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 1.333"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  List Box: lbReportFile
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620052006500 70006F0072007400 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.488"
.head 5 -  Width:  7.517"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.571"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Multiple selection? No
.head 4 -  Sorted? Yes
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Horizontal Scroll? No
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  List Initialization
.head 4 -  Message Actions
.head 3 +  Child Table: tblPrev
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 7400620050007200 650076000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.233"
.head 6 -  Top: 0.476"
.head 6 -  Width:  7.683"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 2.667"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Yes
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: 8
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Class Default
.head 5 -  Discardable? Class Default
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colPrevDate
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Date
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Left
.head 6 -  Width:  0.9"
.head 6 -  Width Editable? Yes
.head 6 -  Format: MM-dd-yy
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colPrev
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  5.983"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colProblem
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 500
.head 6 -  Data Type: Long String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colNote
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 500
.head 6 -  Data Type: Long String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalMessageBox(  'REVISION DATE: '|| SalFmtFormatDateTime( tblPrev.colPrevDate, 'MM-dd-yyyy' ) ||'

PROBLEM: '||  tblPrev.colProblem ||'

NOTE/FIX: '|| tblPrev.colNote, tblPrev.colPrev, MB_Ok|MB_IconInformation)
.head 5 +  On SAM_Create
.head 6 -  Call SAM_CreateTBL( )
.head 3 +  Picture: pic2
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.183"
.head 5 -  Top: 1.607"
.head 5 -  Width:  7.867"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.988"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Editable? No
.head 4 -  File Name: LoginM.bmp
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Scale
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 1
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  Function: FillReportFile
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMax
.head 5 -  Number: n
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 -  Call SalListPopulate( dlgAbout.lbReportFile, hSql, 'SELECT	dep_name
			      FROM	app_depend
			      WHERE	UPPER(app_name) like UPPER(:sAppName)')
.head 3 +  Function: FillPrev
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 -  Call SalTblPopulate( tblPrev, hSql, 'SELECT	mod_label, note, problem, timestamp
			       FROM	app_mod
			       INTO		:tblPrev.colPrev, :tblPrev.colNote, :tblPrev.colProblem, :tblPrev.colPrevDate
			       WHERE	UPPER(app_name) like UPPER( :sAppName)
			       ORDER BY	timestamp desc', TBL_FillAll)
.head 3 +  Function: FillMain
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 +  If sAboutRNumber != ''
.head 6 -  Set dfAboutRDate = dtAboutRDate
.head 6 -  Set dfAboutRNumber = sAboutRNumber
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSql, 'SELECT	timestamp, seq_no
			       FROM	app_mod
			       INTO		:dlgAbout.dfAboutRDate, :dlgAbout.dfAboutRNumber
			       WHERE	UPPER(app_name) like UPPER( :sAppName)
			       ORDER BY	seq_no desc')
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 2 +  Window Parameters
.head 3 -  String: sAboutName
.head 3 -  Date/Time: dtAboutRDate
.head 3 -  String: sAboutRNumber
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow( hWndForm )
.head 4 -  Set dfAboutName = sAboutName
.head 4 -  Call FillMain( )
.head 4 -  Call SalSetWindowText( hWndForm, 'About '||sAboutName )
.head 4 -  Call FillReportFile( )
.head 4 -  Call FillPrev( )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call picTabs.BringToTop( 0, TRUE)
.head 1 +  Dialog Box: dlgExport
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.15"
.head 4 -  Top: 0.167"
.head 4 -  Width:  10.329"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 5.563"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Child Table: tblTable
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 0.344"
.head 6 -  Width:  5.6"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 2.74"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Class Default
.head 5 -  Discardable? Class Default
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colColName
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Column
.head 6 -  Visible? Class Default
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: String
.head 6 -  Justify: Class Default
.head 6 -  Width:  3.5"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Class Default
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colExport
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Export?
.head 6 -  Visible? Class Default
.head 6 -  Editable? Class Default
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: String
.head 6 -  Justify: Class Default
.head 6 -  Width:  0.95"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Check Box
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value: 1
.head 8 -  Uncheck Value: 0
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_AnyEdit
.head 8 -  Call SalTblSetRowFlags( tblTable, SalTblQueryContext( tblTable ), ROW_Edited, FALSE )
.head 8 -  Call FillArray(  )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bSetTRUEE = TRUE
.head 9 -  Set bSetTRUEE = FALSE
.head 8 +  Else If bSetTRUEE = FALSE
.head 9 -  Set bSetTRUEE = TRUE
.head 8 -  Call CheckRows(2, bSetTRUEE)
.head 8 -  Call FillArray(  )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 5 +  Column: colUseSurr
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Use
Surrounder
.head 6 -  Visible? Class Default
.head 6 -  Editable? Class Default
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: String
.head 6 -  Justify: Class Default
.head 6 -  Width:  1.267"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Check Box
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value: 1
.head 8 -  Uncheck Value: 0
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_AnyEdit
.head 8 -  Call FillArray(  )
.head 8 -  Call SalTblSetRowFlags( tblTable, SalTblQueryContext( tblTable ), ROW_Edited, FALSE )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bSetTRUES = TRUE
.head 9 -  Set bSetTRUES = FALSE
.head 8 +  Else If bSetTRUES = FALSE
.head 9 -  Set bSetTRUES = TRUE
.head 8 -  Call CheckRows(3, bSetTRUES)
.head 8 -  Call FillArray(  )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 5 +  Column: colColID
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Class Default
.head 6 -  Width:  Default
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Class Default
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call MTblSubClass( hWndForm )
.head 6 -  Call MTblDefineRowLines( tblTable,0, 0 )
.head 6 -  Call MTblDefineColLines( tblTable,1, COLOR_Black )
.head 6 -  Call SalTblDefineRowHeader( tblTable, '', 20, 33, hWndNULL )
.head 6 -  Call SalTblSetTableFlags( tblTable, TBL_Flag_SelectableCols, TRUE )
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 8.814"
.head 5 -  Top: 0.396"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.229"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call ExportTable(cbIncludeVisible )
.head 6 -  Call SalEndDialog(dlgExport, 1 )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 8.814"
.head 5 -  Top: 0.646"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.208"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgExport, 0 )
.head 3 +  Check Box: cbIncludeVisible
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Include Only Visible Columns
.head 4 -  Window Location and Size
.head 5 -  Left: 5.957"
.head 5 -  Top: 0.958"
.head 5 -  Width:  2.629"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( cbIncludeVisible)
.head 3 +  Check Box: cbIncludeTitle
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Include Title
.head 4 -  Window Location and Size
.head 5 -  Left: 5.957"
.head 5 -  Top: 1.208"
.head 5 -  Width:  1.286"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( cbIncludeVisible)
.head 3 +  Check Box: cbIncludeHeader
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Create a Header
.head 4 -  Window Location and Size
.head 5 -  Left: 5.871"
.head 5 -  Top: 2.24"
.head 5 -  Width:  1.514"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If MyValue = TRUE
.head 7 -  Call ResizeWindow('HEADER')
.head 7 -  Call SalSetFocus(mlHeader)
.head 6 +  Else
.head 7 -  Call ResizeWindow('HEADER_NO')
.head 3 +  Check Box: cbLoadExcel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Load In Excel After Creation
.head 4 -  Window Location and Size
.head 5 -  Left: 7.457"
.head 5 -  Top: 2.24"
.head 5 -  Width:  2.586"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 3 -  Group Box: grp2
.head 4 -  Resource Id: 45270
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 5.857"
.head 5 -  Top: 0.302"
.head 5 -  Width:  2.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.188"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Export Format:
.head 4 -  Line Thickness: 1
.head 4 -  Line Color: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Group Box: grp3
.head 4 -  Resource Id: 45271
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 5.871"
.head 5 -  Top: 1.49"
.head 5 -  Width:  2.843"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.75"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Rows from Parent TBL to Export:
.head 4 -  Line Thickness: 1
.head 4 -  Line Color: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 45272
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 7.643"
.head 5 -  Top: 1.948"
.head 5 -  Width:  0.243"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.188"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: to
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 45273
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 7.371"
.head 5 -  Top: 0.531"
.head 5 -  Width:  0.7"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.333"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Seperate Fields:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfFieldSep
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 8.143"
.head 6 -  Top: 0.573"
.head 6 -  Width:  0.529"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample(cbIncludeVisible  )
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 45274
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 5.914"
.head 5 -  Top: 0.531"
.head 5 -  Width:  0.814"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.365"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Surround Values:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfFieldSurr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 6.757"
.head 6 -  Top: 0.573"
.head 6 -  Width:  0.529"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 45275
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.329"
.head 5 -  Top: 0.49"
.head 5 -  Width:  0.814"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.177"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Dataset:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfDS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.243"
.head 6 -  Top: 0.448"
.head 6 -  Width:  1.657"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.229"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 45276
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.314"
.head 5 -  Top: 0.74"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.177"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Row Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfRS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.243"
.head 6 -  Top: 0.698"
.head 6 -  Width:  1.686"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Data Field: dfAppPath
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.857"
.head 6 -  Top: 2.448"
.head 6 -  Width:  4.257"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Data Field: dfRowStart
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 6.943"
.head 6 -  Top: 1.906"
.head 6 -  Width:  0.671"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Data Field: dfRowEnd
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 7.871"
.head 6 -  Top: 1.906"
.head 6 -  Width:  0.671"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Multiline Field: mlSample
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 20000
.head 5 -  String Type: Long String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Border? Yes
.head 5 -  Word Wrap? No
.head 5 -  Vertical Scroll? Yes
.head 5 -  Window Location and Size
.head 6 -  Left: 0.071"
.head 6 -  Top: 3.125"
.head 6 -  Width:  10.043"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 2.344"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Radio Button: rbAll
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &All Rows
.head 4 -  Window Location and Size
.head 5 -  Left: 6.057"
.head 5 -  Top: 1.656"
.head 5 -  Width:  1.057"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDisableWindow( dfRowStart)
.head 6 -  Call SalDisableWindow( dfRowEnd)
.head 3 +  Radio Button: rbRange
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Range
.head 4 -  Window Location and Size
.head 5 -  Left: 6.057"
.head 5 -  Top: 1.938"
.head 5 -  Width:  0.843"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEnableWindow( dfRowStart)
.head 6 -  Call SalEnableWindow( dfRowEnd)
.head 6 -  Call SalSetFocus(dfRowStart)
.head 3 +  Multiline Field: mlHeader
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2000
.head 5 -  String Type: Long String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Border? Yes
.head 5 -  Word Wrap? Yes
.head 5 -  Vertical Scroll? Yes
.head 5 -  Window Location and Size
.head 6 -  Left: 0.071"
.head 6 -  Top: 2.688"
.head 6 -  Width:  8.386"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.688"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Radio Button: rbCSV
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: CSV
.head 4 -  Window Location and Size
.head 5 -  Left: 2.286"
.head 5 -  Top: 0.031"
.head 5 -  Width:  0.657"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  ! Set dfFieldSep = '}'
.head 6 +  If sFieldSep != ''
.head 7 -  Set dfFieldSep = sFieldSep
.head 6 +  Else
.head 7 -  Set dfFieldSep = '}'
.head 6 +  If sFieldSurr != ''
.head 7 -  Set dfFieldSurr = sFieldSurr
.head 6 +  Else
.head 7 -  Set dfFieldSurr = '"'
.head 6 -  Call SalShowWindowAndLabel( dfFieldSep )
.head 6 -  Call SalShowWindowAndLabel( dfFieldSurr )
.head 6 -  Call SalHideWindowAndLabel( dfDS )
.head 6 -  Call SalHideWindowAndLabel( dfRS )
.head 6 -  Call SalHideWindow( cbLoadExcel )
.head 6 -  Call SalHideWindow ( dfAppPath)
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Radio Button: rbExcel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Excel Format
.head 4 -  Window Location and Size
.head 5 -  Left: 3.1"
.head 5 -  Top: 0.031"
.head 5 -  Width:  1.286"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set dfFieldSep = TAB
.head 6 -  Call SalHideWindowAndLabel( dfFieldSep )
.head 6 -  Call SalShowWindowAndLabel( dfFieldSurr )
.head 6 -  Call SalShowWindow( cbLoadExcel )
.head 6 -  Call SalShowWindow ( dfAppPath)
.head 6 -  Call SalHideWindowAndLabel( dfDS )
.head 6 -  Call SalHideWindowAndLabel( dfRS )
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Radio Button: rbXML
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: XML
.head 4 -  Window Location and Size
.head 5 -  Left: 4.429"
.head 5 -  Top: 0.042"
.head 5 -  Width:  0.743"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalHideWindowAndLabel( dfFieldSep )
.head 6 -  Call SalHideWindowAndLabel( dfFieldSurr )
.head 6 -  Call SalShowWindow( cbLoadExcel )
.head 6 -  Call SalSetWindowText( cbLoadExcel, 'Load In IE After Creation' )
.head 6 -  Call SalHideWindow ( dfAppPath)
.head 6 -  Call SalShowWindowAndLabel( dfDS )
.head 6 -  Call SalShowWindowAndLabel( dfRS )
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 2 +  Functions
.head 3 +  Function: ExportTable
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bIncludeVisible
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 5 -  String: sLineText
.head 5 -  Boolean: bOk
.head 5 -  Number: nRow
.head 5 -  ! Save Variables
.head 5 -  String: saFilter[*]
.head 5 -  Number: nIndex
.head 5 -  Number: nFilters
.head 5 -  File Handle: fhFile
.head 5 -  String: sPath
.head 5 -  String: sFile
.head 5 -  String: sColTitle
.head 5 -  String: sRS
.head 5 -  String: sDS
.head 4 +  Actions
.head 5 -  ! ! SAVE AS...
.head 5 +  If rbCSV
.head 6 -  Set saFilter[0] = "CSV File"
.head 6 -  Set saFilter[1] = "*.csv"
.head 6 -  Set nFilters = 2
.head 6 -  Set nIndex = 1
.head 5 +  Else If rbExcel
.head 6 -  Set saFilter[0] = "Excel File"
.head 6 -  Set saFilter[1] = "*.xls"
.head 6 -  Set nFilters = 2
.head 6 -  Set nIndex = 1
.head 5 +  Else If rbXML
.head 6 -  Set saFilter[0] = "XML File"
.head 6 -  Set saFilter[1] = "*.xml"
.head 6 -  Set nFilters = 2
.head 6 -  Set nIndex = 1
.head 5 -  ! Set sFile = sTableName
.head 5 -  ! !
.head 5 -  ! !
.head 5 +  If sOptionalFileName != ''
.head 6 -  Set sFile = sOptionalFileName
.head 6 -  Set sPath = sFile
.head 5 +  Else
.head 6 -  Call SalDlgSaveFile ( hWndForm, "Save Export File", saFilter, nFilters, nIndex, sFile, sPath )
.head 5 +  If sFile != ''
.head 6 +  If SalFileOpen( fhFile, sPath, OF_Create | OF_Text | OF_Write )
.head 7 -  Call SalWaitCursor( TRUE )
.head 7 +  If rbXML
.head 8 -  Set sLineText = ''
.head 8 -  Set sViewXML1 = ''
.head 8 -  Set sViewXML2 = ''
.head 8 -  Set sDS = dfDS
.head 8 -  Set sRS = dfRS
.head 8 -  Call SalFilePutStr( fhFile, '<?xml version="1.0" encoding="ISO-8859-1" ?> ')
.head 8 -  Call SalFilePutStr( fhFile, '<'|| sDS ||'>'  )
.head 8 -  Set nRow = 0
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 +  While bOk
.head 9 -  Set nCol = 1
.head 9 -  Set sText = ''
.head 9 -  Set sColTitle = ''
.head 9 +  ! If bIncludeVisible
.head 10 -  Set nCOL_COUNT = nCOL_COUNT_SHOWN
.head 9 +  While nCol <= nCOL_COUNT
.head 10 +  If bIncludeVisible
.head 11 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 12 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 12 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 12 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 12 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 12 +  If sText != '' and sColTitle != ''
.head 13 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 12 +  If sColTitle != ''
.head 13 +  If nRow = 1
.head 14 -  Set sViewXML1 = sViewXML1||'
<th>'||sColTitle||'</th>'
.head 14 -  Set sViewXML2 = sViewXML2||'
<td><span datafld="'||sColTitle||'"></span></td>'
.head 10 +  Else
.head 11 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 11 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 11 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 11 +  If sText != '' and sColTitle != ''
.head 12 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 11 +  If sColTitle != ''
.head 12 +  If nRow = 1
.head 13 -  Set sViewXML1 = sViewXML1||'
<th>'||sColTitle||'</th>'
.head 13 -  Set sViewXML2 = sViewXML2||'
<td><span datafld="'||sColTitle||'"></span></td>'
.head 10 -  Set nCol = nCol + 1
.head 9 +  If sLineText != ''
.head 10 -  Set sLineText = '<'||sRS ||'>'  ||sLineText || '</'|| sRS ||'>'
.head 9 -  Call SalFilePutStr( fhFile, sLineText )
.head 9 -  Set sLineText = ''
.head 9 -  Set sText = ''
.head 9 -  Set nRow = nRow +1
.head 9 +  If rbRange = TRUE
.head 10 +  If nRow > dfRowEnd
.head 11 -  Set bOk = FALSE
.head 9 +  Else
.head 10 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 -  Call SalFilePutStr( fhFile, '</'|| sDS ||'>'  )
.head 8 -  Call SalFileClose( fhFile )
.head 8 +  If cbLoadExcel = TRUE
.head 9 -  Call CreateXMLLoader( sPath, sViewXML1, sViewXML2)
.head 8 -  Call SalWaitCursor( FALSE )
.head 7 +  Else
.head 8 +  If cbIncludeHeader = TRUE
.head 9 +  If mlHeader != ''
.head 10 -  Call SalFilePutStr( fhFile, mlHeader )
.head 8 -  Set nIndex = 0
.head 8 -  ! ! GET COLUMN TITLES
.head 8 -  Set nCol = 1
.head 8 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 8 +  If CheckArray('EXPORT', nCol)
.head 9 +  If bIncludeVisible
.head 10 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText||dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText||dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 8 +  While sText != ''
.head 9 -  Set nCol = nCol + 1
.head 9 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 9 +  If CheckArray('EXPORT', nCol)
.head 10 +  If bIncludeVisible
.head 11 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 12 +  If sText != ''
.head 13 +  If sLineText != ''
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sLineText||sText||dfFieldSep
.head 13 +  Else
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText||dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 8 -  Call SalFilePutStr( fhFile, sLineText )
.head 8 -  Set sLineText = ''
.head 8 +  If rbRange = TRUE
.head 9 -  Set nRow = dfRowStart
.head 8 +  Else
.head 9 -  Set nRow = 0
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 +  While bOk
.head 9 -  Set nCol = 1
.head 9 +  While nCol <= nCOL_COUNT
.head 10 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 10 +  If CheckArray('EXPORT', nCol)
.head 11 +  If bIncludeVisible
.head 12 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 13 +  If sLineText != ''
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sLineText||sText||dfFieldSep
.head 13 +  Else
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText||dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 10 -  Set nCol = nCol + 1
.head 9 -  Call SalFilePutStr( fhFile, sLineText )
.head 9 -  Set sLineText = ''
.head 9 -  Set sText = ''
.head 9 -  Set nRow = nRow +1
.head 9 +  If rbRange = TRUE
.head 10 +  If nRow > dfRowEnd
.head 11 -  Set bOk = FALSE
.head 9 +  Else
.head 10 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 -  Call SalFileClose( fhFile )
.head 8 -  Call SalWaitCursor( FALSE )
.head 8 +  If cbLoadExcel = TRUE
.head 9 -  Call SalLoadApp( dfAppPath, '"'||sPath||'"' )
.head 8 +  Else
.head 9 -  Call SalMessageBox( "Export File  <" || sPath || "> saved.", "Save File", 0 )
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bIncludeVisible
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 5 -  Number: nTBLRow
.head 4 +  Actions
.head 5 -  Call SalTblReset( tblTable)
.head 5 -  Set nCol = 1
.head 5 -  Set nCOL_COUNT_SHOWN = 1
.head 5 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 5 +  If bIncludeVisible
.head 6 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 7 +  If sText != ''
.head 8 -  Call FillRow( sText , nTBLRow, nCol)
.head 7 -  Set nCOL_COUNT_SHOWN = nCOL_COUNT_SHOWN + 1
.head 5 +  Else
.head 6 +  If sText != ''
.head 7 -  Call FillRow( sText , nTBLRow, nCol)
.head 5 +  While sText != ''
.head 6 -  Set nCol = nCol + 1
.head 6 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 6 +  If bIncludeVisible
.head 7 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 8 +  If sText != ''
.head 9 -  Call FillRow( sText , nTBLRow, nCol)
.head 8 -  Set nCOL_COUNT_SHOWN = nCOL_COUNT_SHOWN + 1
.head 6 +  Else
.head 7 +  If sText != ''
.head 8 -  Call FillRow( sText , nTBLRow, nCol)
.head 5 -  Set nCOL_COUNT = nCol
.head 5 -  Call FillArray( )
.head 5 -  Call ShowSample(  cbIncludeVisible)
.head 3 +  Function: FillRow
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sText
.head 5 -  Receive Number: nTBLRow
.head 5 -  Number: nColID
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalTblInsertRow( tblTable, nTBLRow  )
.head 5 -  Call SalTblSetContext( tblTable, nTBLRow  )
.head 5 -  Call SalTblSetRowFlags( tblTable, nTBLRow, ROW_New, FALSE )
.head 5 -  Set tblTable.colColName = sText
.head 5 -  Set tblTable.colExport= '1'
.head 5 -  Set tblTable.colUseSurr = '1'
.head 5 -  Set tblTable.colColID = nColID
.head 5 +  If SalNumberMod(nTBLRow, 2) = 0
.head 6 -  Call MTblSetRowBackColor( tblTable, nTBLRow, COLOR_LightGray, 0)
.head 5 -  Call Inc( nTBLRow )
.head 3 +  Function: ShowSample
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bIncludeVisible
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 5 -  String: sLineText
.head 5 -  Boolean: bOk
.head 5 -  Number: nRow
.head 5 -  String: sColTitle
.head 5 -  String: sRS
.head 5 -  String: sDS
.head 4 +  Actions
.head 5 -  Set mlSample = ''
.head 5 +  If rbXML
.head 6 -  Set sLineText = ''
.head 6 -  Set sDS = dfDS
.head 6 -  Set sRS = dfRS
.head 6 -  ! ! GET 10 LINES OF SAMPLE DATA
.head 6 -  Set nRow = 0
.head 6 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 6 +  While bOk
.head 7 -  Set nCol = 1
.head 7 +  ! If bIncludeVisible
.head 8 -  Set nCOL_COUNT = nCOL_COUNT_SHOWN
.head 7 +  While nCol <= nCOL_COUNT
.head 8 +  If bIncludeVisible
.head 9 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 10 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 10 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 10 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 10 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 10 +  If sText != '' and sColTitle != ''
.head 11 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 8 +  Else
.head 9 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 9 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 9 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 9 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 9 +  If sText != '' and sColTitle != ''
.head 10 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 8 -  Set nCol = nCol + 1
.head 7 +  If sLineText != ''
.head 8 -  Set sLineText = '<'||sRS ||'>'  ||sLineText || '</'|| sRS ||'>'
.head 7 -  Set mlSample = mlSample ||'
'||sLineText
.head 7 -  Set sLineText = ''
.head 7 -  Set sText = ''
.head 7 -  Set nRow = nRow +1
.head 7 +  If nRow = 10
.head 8 -  Set bOk = FALSE
.head 7 +  Else
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 6 +  If mlSample != ''
.head 7 -  Set mlSample = '<'||sDS ||'>'  || mlSample || '</'|| sDS ||'>'
.head 7 -  Set mlSample = '<?xml version="1.0" encoding="ISO-8859-1" ?> '||
mlSample
.head 5 +  Else
.head 6 -  ! ! GET COLUMN TITLES
.head 6 +  If cbIncludeTitle
.head 7 -  Set nCol = 1
.head 7 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 7 +  If CheckArray('EXPORT', nCol)
.head 8 +  If bIncludeVisible
.head 9 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sLineText != ''
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 8 +  Else
.head 9 +  If sText != ''
.head 10 +  If sLineText != ''
.head 11 +  If CheckArray('SURR', nCol)
.head 12 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If CheckArray('SURR', nCol)
.head 12 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sLineText != ''
.head 11 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 10 +  Else
.head 11 -  Set sLineText = sText|| dfFieldSep
.head 7 +  While sText != ''
.head 8 -  Set nCol = nCol + 1
.head 8 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 8 +  If CheckArray('EXPORT', nCol)
.head 9 +  If bIncludeVisible
.head 10 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If sLineText != ''
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sLineText != ''
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 7 -  Set mlSample = sLineText
.head 6 -  Set sLineText = ''
.head 6 -  ! ! GET 10 LINES OF SAMPLE DATA
.head 6 -  Set nRow = 0
.head 6 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 6 +  While bOk
.head 7 -  Set nCol = 1
.head 7 +  While nCol <= nCOL_COUNT
.head 8 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 8 +  If CheckArray('EXPORT', nCol)
.head 9 +  If bIncludeVisible
.head 10 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If sLineText != ''
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sLineText != ''
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 8 -  Set nCol = nCol + 1
.head 7 -  Set mlSample = mlSample ||'
'||sLineText
.head 7 -  Set sLineText = ''
.head 7 -  Set sText = ''
.head 7 -  Set nRow = nRow +1
.head 7 +  If nRow = 10
.head 8 -  Set bOk = FALSE
.head 7 +  Else
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 3 +  Function: FillArray
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: n
.head 5 -  Number: nRow
.head 5 -  String: sText
.head 4 +  Actions
.head 5 -  Call ResetArray( sUseExp)
.head 5 -  Call ResetArray( sUseSurr)
.head 5 -  Set n = 0
.head 5 -  Set nRow = 0
.head 5 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 2, COL_GetPos )  )
.head 5 +  If sText = '1'
.head 6 -  Set sUseExp[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 6 -  Call Inc(n)
.head 5 +  While sText != ''
.head 6 -  Call Inc(nRow)
.head 6 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 2, COL_GetPos )  )
.head 6 +  If sText = '1'
.head 7 -  Set sUseExp[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 7 -  Call Inc(n)
.head 5 -  Set n = 0
.head 5 -  Set nRow = 0
.head 5 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 3, COL_GetPos )  )
.head 5 +  If sText = '1'
.head 6 -  Set sUseSurr[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 6 -  Call Inc(n)
.head 5 +  While sText != ''
.head 6 -  Call Inc(nRow)
.head 6 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 3, COL_GetPos )  )
.head 6 +  If sText = '1'
.head 7 -  Set sUseSurr[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 7 -  Call Inc(n)
.head 5 -  Set sText = sText
.head 3 +  Function: ResetArray
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive String: sArray[*]
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMax
.head 5 -  Number: nMin
.head 4 +  Actions
.head 5 -  Call SalArrayGetUpperBound( sArray, 1, nMax )
.head 5 -  Set nMin = 0
.head 5 +  While nMin <= nMax
.head 6 -  Set sArray[nMin] = ''
.head 6 -  Call Inc(nMin)
.head 3 +  Function: CheckArray
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sType
.head 5 -  Number: nCheckID
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMin
.head 5 -  Number: nMax
.head 5 -  String: sCheckID
.head 4 +  Actions
.head 5 -  Set sCheckID = SalNumberToStrX(nCheckID, 0)
.head 5 +  If sType = 'EXPORT'
.head 6 -  Call SalArrayGetUpperBound( sUseExp, 1, nMax )
.head 6 -  Set nMin = 0
.head 6 +  While nMin <= nMax
.head 7 +  If sUseExp[nMin] = sCheckID
.head 8 -  Return TRUE
.head 7 -  Call Inc(nMin)
.head 6 -  Return FALSE
.head 5 +  Else If sType = 'SURR'
.head 6 -  Call SalArrayGetUpperBound( sUseSurr, 1, nMax )
.head 6 -  Set nMin = 0
.head 6 +  While nMin <= nMax
.head 7 +  If sUseSurr[nMin] = sCheckID
.head 8 -  Return TRUE
.head 7 -  Call Inc(nMin)
.head 6 -  Return FALSE
.head 3 +  Function: CheckRows
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Number: nColID
.head 5 -  Boolean: bSetTRUE
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Boolean: bOk
.head 5 -  Number: nRow
.head 4 +  Actions
.head 5 -  Set nRow = 0
.head 5 -  Set bOk = SalTblSetContext( tblTable, nRow)
.head 5 +  While bOk
.head 6 +  If nColID = 3
.head 7 +  If bSetTRUE
.head 8 -  Set tblTable.colUseSurr = '1'
.head 7 +  Else
.head 8 -  Set tblTable.colUseSurr = '0'
.head 6 +  Else If nColID = 2
.head 7 +  If bSetTRUE
.head 8 -  Set tblTable.colExport = '1'
.head 7 +  Else
.head 8 -  Set tblTable.colExport = '0'
.head 6 -  Call Inc(nRow)
.head 6 -  Set bOk = SalTblSetContext( tblTable, nRow)
.head 3 +  Function: GetParent_TBL_Row_Cnt
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 -  Actions
.head 3 +  Function: ResizeWindow
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sType
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If sType= 'HEADER'
.head 6 -  Call SalSetWindowSize( mlSample, 8.386, 1.25 )
.head 6 -  Call SalSetWindowLoc( mlSample, 0.071, 3.365)
.head 6 -  Call SalShowWindow( mlHeader )
.head 5 +  Else If sType = 'HEADER_NO'
.head 6 -  Call SalSetWindowSize( mlSample, 8.386, 1.927 )
.head 6 -  Call SalSetWindowLoc( mlSample, 0.071, 2.688)
.head 6 -  Call SalHideWindow( mlHeader )
.head 3 +  Function: GetExcelProgram
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sProgramUse
.head 4 +  Parameters
.head 5 -  String: sProgramSave
.head 4 +  Static Variables
.head 5 -  Number: nOffset
.head 4 +  Local variables
.head 5 -  String: sProgramUse
.head 5 -  ! String: sProgramSave
.head 5 -  String: sFileType
.head 5 -  String: sProgramType
.head 5 -  Number: hIconHandle
.head 5 -  String: sUsePath
.head 5 -  String: sSystemRoot
.head 5 -  String: sProgramFilesPath
.head 4 +  Actions
.head 5 -  ! Set sSystemRoot = ReadRegValue( HKEY_LOCAL_MACHINE, '/SOFTWARE/Microsoft/Windows NT/CurrentVersion', 'SystemRoot' )
.head 5 -  ! Set sProgramFilesPath = ReadRegValue( HKEY_LOCAL_MACHINE, '/SOFTWARE/Microsoft/Windows/CurrentVersion', 'ProgramFilesDir' )
.head 5 -  ! Set nOffset = VisStrScanReverse( sProgramSave, -1,  '.' )
.head 5 -  ! Set sFileType = SalStrMidX( sProgramSave, nOffset, 100 )
.head 5 -  ! Set sProgramUse = ReadRegistryOpenWith( sFileType)
.head 5 -  ! Set sProgramUse = sProgramUse
.head 5 -  ! Set nOffset = SalStrScan( sProgramUse, 'SystemRoot')
.head 5 +  ! If nOffset > 0
.head 6 -  Set sProgramUse =  SalStrReplaceX( sProgramUse, nOffset-1, 13, sSystemRoot||'\\')
.head 5 -  ! Set nOffset = SalStrScan( sProgramUse, 'ProgramFiles')
.head 5 +  ! If nOffset > 0
.head 6 -  Set sProgramUse =  SalStrReplaceX( sProgramUse, nOffset-1, 15, sProgramFilesPath||'\\')
.head 5 -  ! Set nOffset = SalStrScan(sProgramUse, '\\%')
.head 5 -  ! Return sProgramUse
.head 3 +  ! Function: ReadRegistryOpenWith
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 +  Parameters 
.head 5 -  String: sParam
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CLASSES_ROOT )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/'||sParam, FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( '', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Call REG.SetRootKey( HKEY_CLASSES_ROOT )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/'||s||'/Shell/open/command', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( '', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  ! Function: ReadRegValue   !Used to pull any value
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 +  Parameters 
.head 5 -  Number: sRootKey
.head 5 -  String: sKeyName
.head 5 -  String: sValueName
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  ! String: bin
.head 5 -  ! Number: BinSize
.head 5 -  ! Number: i
.head 5 -  ! Number: f
.head 5 -  ! Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  ! Number: n
.head 5 -  ! Number: min
.head 5 -  ! Number: max
.head 5 -  ! String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( sRootKey )
.head 5 +  If not REG.OpenKey( sKeyName, FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( sValueName, s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: CreateXMLLoader
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: fPath
.head 5 -  String: fXML1
.head 5 -  String: fXML2
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  File Handle: fhFile
.head 4 +  Actions
.head 5 +  If SalFileOpen( fhFile, 'C:\\Temp\\TempXML.html', OF_Create | OF_Text | OF_Write )
.head 6 -  Call SalFilePutStr( fhFile, '<html>')
.head 6 -  Call SalFilePutStr( fhFile, '<body>')
.head 6 -  Call SalFilePutStr( fhFile, '<xml')
.head 6 -  Call SalFilePutStr( fhFile, 'src="'||fPath||'"')
.head 6 -  Call SalFilePutStr( fhFile, 'id="xmlfile"')
.head 6 -  Call SalFilePutStr( fhFile, 'async="false">')
.head 6 -  Call SalFilePutStr( fhFile, '</xml>')
.head 6 +  If mlHeader != ''
.head 7 -  Call SalFilePutStr( fhFile, '<p>'|| mlHeader||'</p>')
.head 6 -  Call SalFilePutStr( fhFile, '<table')
.head 6 -  Call SalFilePutStr( fhFile, 'datasrc="#xmlfile"')
.head 6 -  Call SalFilePutStr( fhFile, 'width="100%"')
.head 6 -  Call SalFilePutStr( fhFile, 'border="1">')
.head 6 -  Call SalFilePutStr( fhFile, '<thead>')
.head 6 -  Call SalFilePutStr( fhFile, fXML1)
.head 6 -  Call SalFilePutStr( fhFile, '</thead>')
.head 6 -  Call SalFilePutStr( fhFile, '<tr align="left">')
.head 6 -  Call SalFilePutStr( fhFile, fXML2)
.head 6 -  Call SalFilePutStr( fhFile, '</tr>')
.head 6 -  Call SalFilePutStr( fhFile, '</table>')
.head 6 -  Call SalFilePutStr( fhFile, '</body>')
.head 6 -  Call SalFilePutStr( fhFile, '</html>')
.head 6 -  Call SalFileClose( fhFile)
.head 6 -  Call SalLoadApp( '"C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE"', '"C:\\temp\\TempXML.html"')
.head 2 +  Window Parameters
.head 3 -  Window Handle: hWndTBL
.head 3 -  String: sOptionalHeaderText
.head 3 -  String: sOptionalFileName
.head 3 -  String: sOperation !	null = Display Form, walk though mode
			'CSV' = 'Default to CSV and don't display Form
			'EXCEL' = Default to Excel and don't display Form
			'CSV_AUTO' = Default to CSV and don't display Form
.head 3 -  String: sFieldSep
.head 3 -  String: sFieldSurr
.head 2 +  Window Variables
.head 3 -  String: sUseSurr[*]
.head 3 -  String: sUseExp[*]
.head 3 -  String: sTableName
.head 3 -  Boolean: bSetTRUES
.head 3 -  Boolean: bSetTRUEE
.head 3 -  Number: nCOL_COUNT
.head 3 -  Number: nCOL_COUNT_SHOWN
.head 3 -  ! Used to view XML
.head 3 -  String: sViewXML1
.head 3 -  String: sViewXML2
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set cbIncludeTitle = TRUE
.head 4 -  Call SalSendMsg( tblTable, SAM_Create,0,0 )
.head 4 +  If sFieldSep != ''
.head 5 -  Set dfFieldSep = sFieldSep
.head 4 +  Else
.head 5 -  Set dfFieldSep = '}'
.head 4 +  If sFieldSurr != ''
.head 5 -  Set dfFieldSurr = sFieldSurr
.head 4 +  Else
.head 5 -  Set dfFieldSurr = '"'
.head 4 -  ! Set dfFieldSep = '}'
.head 4 -  ! Set dfFieldSurr = '"'
.head 4 -  Set cbIncludeVisible = TRUE
.head 4 -  Call SalSendMsg( cbIncludeHeader, SAM_Click, 0,0)
.head 4 -  Call FillScreen( cbIncludeVisible)
.head 4 -  Set bSetTRUES = TRUE
.head 4 -  Set bSetTRUEE = TRUE
.head 4 -  Call SalGetItemName( hWndTBL, sTableName )
.head 4 -  Call SalSetWindowText( dlgExport, 'Export data from table: '||sTableName)
.head 4 -  Set dfRowEnd = SalTblSetRow( hWndTBL, TBL_SetLastRow)
.head 4 -  Set dfRowStart = 0
.head 4 -  Set dfDS = 'ROOT_ELEMENT'
.head 4 -  Set dfRS = 'CHILD_ELEMENT'
.head 4 -  Call SalDisableWindow( dfRowStart)
.head 4 -  Call SalDisableWindow( dfRowEnd)
.head 4 -  Call SalHideWindow ( dfAppPath)
.head 4 +  If sOptionalHeaderText != ''
.head 5 -  Set cbIncludeHeader = TRUE
.head 5 -  Call SalSendMsg( cbIncludeHeader, SAM_Click, 0,0)
.head 5 -  Set mlHeader = sOptionalHeaderText
.head 4 -  Set dfAppPath = GetExcelProgram( '.xls' )
.head 4 +  If sOperation = 'EXCEL'
.head 5 -  Call SalHideWindow ( dlgExport)
.head 5 -  Set rbExcel = TRUE
.head 5 -  Call SalSendMsg( rbExcel, SAM_Click, 0,0)
.head 5 -  Set cbLoadExcel = TRUE
.head 5 -  Call SalSendMsg( pbOk, SAM_Click, 0,0)
.head 4 +  Else If sOperation = 'XML'
.head 5 -  Call SalHideWindow ( dlgExport)
.head 5 -  Set rbXML = TRUE
.head 5 -  Call SalSendMsg( rbXML, SAM_Click, 0,0)
.head 5 -  Set cbLoadExcel = TRUE
.head 5 -  Call SalSendMsg( pbOk, SAM_Click, 0,0)
.head 4 +  Else If sOperation = 'CSV_AUTO'
.head 5 -  Call SalHideWindow ( dlgExport)
.head 5 -  Set rbCSV = TRUE
.head 5 -  Call SalSendMsg( rbCSV, SAM_Click, 0,0)
.head 5 -  Set cbLoadExcel = FALSE
.head 5 -  Call SalSendMsg( pbOk, SAM_Click, 0,0)
.head 1 +  Dialog Box: dlgPrintCRDocket
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Print Job In Progress
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.325"
.head 4 -  Top: 2.375"
.head 4 -  Width:  4.486"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.781"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Bold
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 51538
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.186"
.head 5 -  Top: 0.25"
.head 5 -  Width:  4.043"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.229"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Printing Docket Please Wait....
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 +  Functions
.head 3 +  Function: PrintDocket
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: fDefendant
.head 5 -  Number: nOldCount
.head 4 +  Actions
.head 5 -  Set nPrintDocket=1
.head 5 -  Call SqlPrepare(hSql,'Select Agency from MUNI_BOOKING where
CaseYr=:fCaseYr  and CaseTy= :fCaseTy and CaseNo= :fCaseNo into :sDockAgency ')
.head 5 -  Call SqlExecute(hSql)
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 -  ! Call SqlPrepare(hSql,"Select lname||', '||fname||' '||mname||' '||title from cr_index
where CaseYr= :fCaseYr  and CaseTy= :fCaseTy and CaseNo= :fCaseNo into :fDefendant")
.head 5 -  ! Call SqlExecute(hSql)
.head 5 -  ! Call SqlFetchNext(hSql,nReturn)
.head 5 -  Call SqlPrepare(hSql, "Select LName || ', ' || FName || ' ' || MName || ' ' || Title, 	Address1 || '  ' || City || ', ' || State || ' ' || Zip
	from CR_PARTIES into :fDefendant, :sAddressRD
	where CaseYr=:fCaseYr and CaseTy=:fCaseTy and CaseNo=:fCaseNo ")
.head 5 -  Call SqlExecute(hSql)
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If fReportType = 'D'
.head 6 -  Set sReport = 'crdock.qrp'
.head 6 -  Set sReportBinds = 'sDockCase, :fDefendant,sDockAgency,dDockDate,
sDockText,dDockRecDate,sDockRec,sDockRecCode,nDockAmount, sCourt, sCourtClerk, sAddressRD'
.head 6 -  Set sReportInputs = 'CASE,DEF,AGENCY,DDATE,DTEXT,RDATE,REC,RCODE,AMOUNT, COURT, CLERK, Addr'
.head 5 +  Else If fReportType = 'R'
.head 6 -  ! Check for older Unix disb.  This report will not include those, but we can at least let the user know they exist.
.head 6 -  Call SqlPrepareAndExecute( hSql,"Select 	count(*)
			         FROM	CR_DisbMaster
			         INTO	:nOldCount
			         WHERE	(CaseYr=:fCaseYr and CaseTy=:fCaseTy and CaseNo=:fCaseNo) and
					disbdate < to_date('4-30-2002', 'MM-dd-yyyy')")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If nOldCount > 0
.head 7 -  Call SalMessageBox( 'Disbursements exist for this case that are prior to the conversion date of 4-30-2002.  These will not be printed on this report.', 'Old Disbursement records', MB_Ok| MB_IconAsterisk )
.head 6 -  Set sReport = 'crdock2.qrp'
.head 6 -  Set sReportBinds = 'sDockCase, :fDefendant,sDockAgency,dTranDateRD, nRecCheckNoRD,
		  sCashierRD, sTypeRD, sAdjRD, sLedgerRD, sStatute_DepRD,
		  nAmountRD, nTotalRD,sCourt, sCourtClerk, sReportBreakRD,
		  sDateHeader, sRec_CheckHeader, sStat_DepHeader, sAddressRD'
.head 6 -  Set sReportInputs = 'CASE,DEF,AGENCY,TRANDATE, REC_CHECK,
		  CASHIER, TYPE, ADJUSTMENT, LEDGER, STATUTE,
		  AMOUNT, TOTAL, COURT, CLERK, RPT_TYPE,
		  TRAN_DATE_HDR, REC_CHECK_HDR, STAT_DEP_HDR, Addr'
.head 5 +  If fPrintType = 'SCREEN'
.head 6 -  Call SalReportView( dlgPrintCRDocket, hWndNULL, sReport,
    sReportBinds, sReportInputs, nPrintErr )
.head 5 +  Else If fPrintType = 'PRINTER'
.head 6 -  Call SalReportPrint(hWndForm,sReport, sReportBinds, sReportInputs,1,RPT_PrintAll,0,0,nPrintDocket)
.head 2 +  Window Parameters
.head 3 -  String: fCaseYr
.head 3 -  String: fCaseTy
.head 3 -  String: fCaseNo
.head 3 -  String: fPrintType
.head 3 -  String: fReportType   ! 'D' = Docket Normal, 'R' = Receipt/Disbursements
.head 2 +  Window Variables
.head 3 -  ! !!!!!!! VARIABLES FOR PRINTING DOCKET
.head 3 -  Boolean: bDockDone
.head 3 -  Date/Time: dDockDate
.head 3 -  String: sDockText
.head 3 -  String: sDockRec
.head 3 -  String: sDockRecCode
.head 3 -  Date/Time: dDockRecDate
.head 3 -  String: nDockAmount
.head 3 -  Number: nDockAmountNo
.head 3 -  String: sDockCase
.head 3 -  Number: nPrintDocket
.head 3 -  String: sDockAgency
.head 3 -  ! !!!!!!! VARIABLES FOR PRINTING DOCKET - END
.head 3 -  Date/Time: dTranDateRD
.head 3 -  Number: nRecCheckNoRD
.head 3 -  String: sCashierRD
.head 3 -  String: sTypeRD
.head 3 -  String: sAdjRD
.head 3 -  String: sLedgerRD
.head 3 -  String: sStatute_DepRD
.head 3 -  Number: nAmountRD
.head 3 -  Number: nTotalRD
.head 3 -  String: sReportBreakRD
.head 3 -  String: sDateHeader
.head 3 -  String: sRec_CheckHeader
.head 3 -  String: sStat_DepHeader
.head 3 -  String: sAddressRD
.head 2 +  Message Actions
.head 3 +  On SAM_ReportStart
.head 4 -  Set bDockDone=FALSE
.head 4 +  If fReportType = 'D'
.head 5 -  Set dDockDate=SalStrToDate(String_Null)
.head 5 -  Set sDockText=String_Null
.head 5 -  Set sDockRec=String_Null
.head 5 -  Set sDockRecCode=String_Null
.head 5 -  Set dDockRecDate=SalStrToDate(String_Null)
.head 5 -  Set nDockAmount=String_Null
.head 5 -  Set nDockAmountNo=SalStrToNumber(String_Null)
.head 5 -  Call SqlPrepare(hSql,'SELECT		Dock_Date,Data
		        FROM		CR_DOCKET
		        WHERE		CaseYr=:fCaseYr  and
					CaseTy=:fCaseTy  and
					CaseNo= :fCaseNo
		        INTO		:dDockDate,:sDockText
order by Dock_Date,Seq,RowId ')
.head 5 -  Call SqlExecute(hSql)
.head 4 +  Else If fReportType = 'R' ! Receipt /Disbursement Report
.head 5 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 5 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 5 -  Set sCashierRD=String_Null
.head 5 -  Set sTypeRD=String_Null
.head 5 -  Set sAdjRD=String_Null
.head 5 -  Set sLedgerRD=String_Null
.head 5 -  Set sStatute_DepRD=String_Null
.head 5 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 5 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 5 -  Set sDateHeader = 'Rec Date'
.head 5 -  Set sRec_CheckHeader = 'Receipt #'
.head 5 -  Set sStat_DepHeader = 'Statute'
.head 5 -  Set sReportBreakRD = ''
.head 5 -  ! Call SqlPrepare(hSql,"SELECT		dat, receiptno, ledger, amount, pay_type,
					adj, Cashierno || ' / ' || Userid, offenseno
		        FROM		CR_RECMASTER
		        WHERE		CaseYr=:fCaseYr  and
					CaseTy=:fCaseTy  and
					CaseNo= :fCaseNo
		        INTO		:dTranDateRD,:nRecCheckNoRD, :sLedgerRD, :nAmountRD, :sTypeRD,
					:sAdjRD, :sCashierRD, :sStatute_DepRD
		        ORDER BY		DAT, RECEIPTNO")
.head 5 -  Call SqlPrepare(hSql,"Select m.Dat, m.Receiptno, m.Cashierno || ' / ' || m.Userid, m.Pay_Type, m.Void, m.Ledger, m.Offenseno, m.Amount, 'RECEIPT'
	from CR_RECMaster m
	where (m.CaseYr=:fCaseYr and m.CaseTy=:fCaseTy and m.CaseNo=:fCaseNo)
	Union all
		Select c.dat,c.receiptno, c.cashierno, c.pay_type, '', '', '', c.totamount, 'RECEIPT'
		From cr_receipt2 c
		where c.caseyr =:fCaseYr and c.casety = :fCaseTy and c.caseno = :fCaseNo and
		             transfered is null
	into :dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sTypeRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
	order by 1,2")
.head 5 -  Call SqlExecute(hSql)
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If fReportType = 'D'
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If nReturn=FETCH_EOF
.head 6 +  If bDockDone=TRUE
.head 7 -  Return FALSE
.head 6 +  Else
.head 7 -  Set bDockDone=TRUE
.head 7 -  Set dDockDate=SalStrToDate(String_Null)
.head 7 -  Set sDockText=String_Null
.head 7 -  Call SqlPrepare(hSql,'SELECT		Dat,ReceiptNo,Ledger,Amount
		        FROM		CR_RECMASTER
		        WHERE		CaseYr=:fCaseYr  and
					CaseTy= :fCaseTy  and
					CaseNo= :fCaseNo
                                        INTO		:dDockRecDate,:sDockRec,:sDockRecCode,:nDockAmountNo
		        ORDER BY		DAT')
.head 7 -  Call SqlExecute(hSql)
.head 7 -  Call SqlFetchNext(hSql,nReturn)
.head 7 +  If nDockAmountNo >0
.head 8 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 8 -  Set nDockAmount='$'||nDockAmount
.head 8 -  Set sDockRec='Receipt      '||sDockRec
.head 8 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 7 -  Return TRUE
.head 5 +  Else
.head 6 -  Set sDockCase= fCaseYr||' '||fCaseTy||' '||fCaseNo
.head 6 +  If nDockAmountNo >0
.head 7 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 7 -  Set nDockAmount='$'||nDockAmount
.head 7 -  Set sDockRec='Receipt      '||sDockRec
.head 7 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 6 -  Return TRUE
.head 4 +  Else If fReportType = 'R'
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If nReturn=FETCH_EOF
.head 6 +  If bDockDone=TRUE
.head 7 -  Return FALSE
.head 6 +  Else
.head 7 -  Set bDockDone=TRUE
.head 7 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 7 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 7 -  Set sCashierRD=String_Null
.head 7 -  Set sTypeRD=String_Null
.head 7 -  Set sAdjRD=String_Null
.head 7 -  Set sLedgerRD=String_Null
.head 7 -  Set sStatute_DepRD=String_Null
.head 7 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 7 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 7 -  Set sDateHeader = 'Dis Date'
.head 7 -  Set sRec_CheckHeader = 'Check #'
.head 7 -  Set sStat_DepHeader = 'Depositor'
.head 7 -  Set sReportBreakRD = ''
.head 7 -  Call SqlPrepare(hSql,"Select M.DisbDate, M.BankNo, M.Userid, M.CheckStatus, D.Ledger, M.Depositor, D.Amount, 'DISBURSEMENT'
   from	CR_DisbMaster M, CR_DisbDetail D
   into	:dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
   where	(M.CheckNo = D.CheckNo) and
	(M.CaseYr=:fCaseYr and M.CaseTy=:fCaseTy and M.CaseNo=:fCaseNo) and
	M.disbdate >=to_date('4-30-2002', 'MM-dd-yyyy')
	order by M.DisbDate, D.Ledger")
.head 7 -  Call SqlExecute(hSql)
.head 7 -  Call SqlFetchNext(hSql,nReturn)
.head 7 +  If nReturn = FETCH_EOF
.head 8 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 8 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 8 -  Set sCashierRD=String_Null
.head 8 -  Set sTypeRD=String_Null
.head 8 -  Set sAdjRD=String_Null
.head 8 -  Set sLedgerRD=String_Null
.head 8 -  Set sStatute_DepRD=String_Null
.head 8 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 8 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 8 -  Set sReportBreakRD = ''
.head 7 +  Else
.head 8 -  Return TRUE
.head 5 +  Else
.head 6 -  Set sDockCase= fCaseYr||' '||fCaseTy||' '||fCaseNo
.head 6 +  ! If nDockAmountNo >0
.head 7 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 7 -  Set nDockAmount='$'||nDockAmount
.head 7 -  Set sDockRec='Receipt      '||sDockRec
.head 7 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 6 -  ! Return TRUE
.head 6 +  If nAmountRD = 0
.head 7 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 7 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 7 -  Set sCashierRD=String_Null
.head 7 -  Set sTypeRD=String_Null
.head 7 -  Set sAdjRD=String_Null
.head 7 -  Set sLedgerRD=String_Null
.head 7 -  Set sStatute_DepRD=String_Null
.head 7 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 7 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 7 -  Set sReportBreakRD = ''
.head 6 +  Else
.head 7 -  Return TRUE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 3 +  On SAM_CreateComplete
.head 4 -  Call PrintDocket(  )
.head 4 -  Call SalEndDialog( hWndForm, 1 )
.head 3 +  On SAM_Create
.head 4 -  ! Call CSCDisableNonEditable( hWndForm )
.head 1 +  Dialog Box: dlgDatasource
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Input Source
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.2"
.head 4 -  Top: 1.094"
.head 4 -  Width:  2.625"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 2.563"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Data Field: dfReturn
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.038"
.head 6 -  Top: 0.021"
.head 6 -  Width:  2.125"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbExit
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 2.163"
.head 5 -  Top: 0.031"
.head 5 -  Width:  0.288"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.24"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Enter
.head 4 -  Font Name: Arial
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: None
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sDatasource = SalStrMidX( dfReturn, 0, 100)
.head 6 -  Call WriteBackupDB( sDatasource )
.head 6 -  Call SalEndDialog(dlgDatasource, 1)
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 2365
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.075"
.head 5 -  Top: 0.323"
.head 5 -  Width:  2.413"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.135"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Arial Narrow
.head 4 -  Font Size: 11
.head 4 -  Font Enhancement: Bold
.head 4 -  Text Color: Red
.head 4 -  Background Color: Gray
.head 4 -  Title: WARNING: By changing the database name, you are going to be connecting to a different database, and all actions will not be recorded on the real database.  This is to be used to change to a TEST database only, and should be done only after consulting with a supervisor!!!!
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 +  Functions
.head 3 +  Function: WriteBackupDB
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sBackupDB
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call Startup.WriteParam('', 'TestDB', sBackupDB)
.head 5 -  Call Startup.WriteParam('', 'Test', 'Y')
.head 3 +  Function: ReadBackupDB
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  ! String: bin
.head 5 -  ! Number: BinSize
.head 5 -  ! Number: i
.head 5 -  ! Number: f
.head 5 -  ! Boolean: b
.head 5 -  ! String: Arr[*]
.head 5 -  ! Number: n
.head 5 -  ! Number: min
.head 5 -  ! Number: max
.head 5 -  ! String: sKeyValue
.head 4 +  Actions
.head 5 -  Return Startup.sTestDB_S
.head 2 +  Window Parameters
.head 3 -  Receive String: sDatasource
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set dfReturn = ReadBackupDB(  )
.head 1 +  Dialog Box: dlgPrintCVDocket
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.875"
.head 4 -  Top: 2.208"
.head 4 -  Width:  3.756"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.617"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Bold
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 57424
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.275"
.head 5 -  Top: 0.183"
.head 5 -  Width:  4.075"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.183"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Printing Docket Please Wait....
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfCCOtherText
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.414"
.head 6 -  Top: 4.115"
.head 6 -  Width:  3.114"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  Function: fPrintDocket
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set nPrintErr = -1
.head 5 +  If fReport = ''
.head 6 -  Set sReport = 'CVInq.qrp'
.head 5 +  Else
.head 6 -  Set sReport = fReport
.head 5 -  Set sReportBinds = ':sCaseNum, :sLitName, :sLitType, :sLitAddr, :sLitCityStZip,
   		  :sAttorney, :sAttyName, :sAttyAddr, :sAttyCityStZip,:nDockNo,
		  :sDockDate, :sDockString, :sDockMemo, :sSpace,:sRecHeading,
		  :sRecHeading2, :dRecDate, :nReceiptNo,:sCashierDrawer, :nTotalAmnt,
   		  :sLedgerCode, :nLedgerAmnt, :nLedgerAmnt2, :sAdj, :nCheckNo,
		  :dCheckDate, :sPayee, :nDisAmnt,:sUClerk, :sUFullName,
		  :sCourtClerk, :sCourt'
.head 5 -  Set sReportInputs = 'CaseNo, Litigant, LitType, LitAddr, LitCityStZip,
   		  Attorney, AttyName, AttyAddr, AttyCityStZip,DocketNo,
		  DocketDate, DocketText, DocketMemo, Space, RecHeading,
		  RecHeading2, RecDate, ReceiptNo, CashierDrawer, TotalAmnt,
   		  LedgerCode, LedgerAmnt, LedgerAmnt2, Adj, CheckNo,
		  CheckDate, Payee, DisAmnt, ClerkInit, Clerk,
		  CourtClerk, Court'
.head 5 -  Call SalReportPrint ( hWndForm, sReport,sReportBinds, sReportInputs, 0, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 5 +  If nPrintErr > 0
.head 6 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 2 +  Window Parameters
.head 3 -  Number: fCaseYr
.head 3 -  Number: fCaseNo
.head 3 -  Sql Handle: hPrintDocket
.head 3 -  String: fReport
.head 2 +  Window Variables
.head 3 -  ! ! - - Docket Report Parameters - - !
.head 3 -  String: sLitName
.head 3 -  String: sAliasType
.head 3 -  String: sLitType
.head 3 -  String: sLitAddr2
.head 3 -  String: sLitAddr
.head 3 -  String: sLitCityStZip
.head 3 -  String: sAttorney
.head 3 -  String: sAttyName
.head 3 -  String: sAttyAddr
.head 3 -  String: sAttyPhone
.head 3 -  String: sAttyCityStZip
.head 3 -  ! ! - - Docket Data - - !
.head 3 -  Number: nDockNo
.head 3 -  String: sDockCode
.head 3 -  Date/Time: sDockDate
.head 3 -  String: sCode5
.head 3 -  Long String: sDockString
.head 3 -  Long String: sDockMemo
.head 3 -  String: sSpace
.head 3 -  String: sAttyNo
.head 3 -  Number: nAttyNo
.head 3 -  ! ! - - Receipt Data - - !
.head 3 -  Date/Time: dRecDate
.head 3 -  Number: nReceiptNo
.head 3 -  Number: nTotalAmnt
.head 3 -  String: sCashierDrawer
.head 3 -  String: sPaidByMethod
.head 3 -  String: sLedgerCode
.head 3 -  Number: nLedgerAmnt
.head 3 -  Number: nLedgerAmnt2
.head 3 -  String: sAdj
.head 3 -  Number: nCheckNo
.head 3 -  Date/Time: dCheckDate
.head 3 -  String: sPayee
.head 3 -  Number: nDisAmnt
.head 3 -  Date/Time: dPrevRecDate
.head 3 -  Number: nPrevReceiptNo
.head 3 -  Number: nPrevTotalAmnt
.head 3 -  String: sPrevCashierDrawer
.head 3 -  String: sRecoverable
.head 3 -  String: sRecHeading
.head 3 -  String: sRecHeading2
.head 3 -  String: sAddress
.head 3 -  Number: nRptFlg
.head 3 -  Boolean: bFindFlg
.head 3 -  String: sCaseNum
.head 3 -  String: sCaseTy
.head 3 -  Number: nOffSet
.head 3 -  Sql Handle: hSqlAttyD
.head 2 +  Message Actions
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlg = 0
.head 4 -  Call SqlPrepareAndExecute( hPrintDocket, 'SELECT	casetype
				        FROM	casemaster
				        WHERE	caseno = :fCaseNo and
						caseyr = :fCaseYr
				        INTO	:sCaseTy')
.head 4 -  Call SqlFetchNext(hPrintDocket, nReturn)
.head 4 -  Set sCaseNum = SalNumberToStrX( fCaseYr, 0) || '-' || 'CV'||sCaseTy || '-' || SalNumberToStrX( fCaseNo, 0 )
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit	!6/23/99  sjj  added AliasType extracted from Litigant
.head 4 -  Set nRptFlg = nRptFlg + 1
.head 4 +  If nRptFlg = 1
.head 5 -  Set sSpace = ''
.head 5 -  Set nDockNo = NUMBER_Null
.head 5 -  Set sDockDate = DATETIME_Null
.head 5 -  Set sDockString = ''
.head 5 -  Set sDockMemo = ''
.head 5 -  Set dRecDate = DATETIME_Null
.head 5 -  Set nReceiptNo = NUMBER_Null
.head 5 -  Set sCashierDrawer = ''
.head 5 -  Set sPaidByMethod = ''
.head 5 -  Set nTotalAmnt = NUMBER_Null
.head 5 -  Set sLedgerCode = ''
.head 5 -  Set nLedgerAmnt = NUMBER_Null
.head 5 -  Set nLedgerAmnt2 = NUMBER_Null
.head 5 -  Set sAdj = ''
.head 5 -  Set nCheckNo = NUMBER_Null
.head 5 -  Set dCheckDate = DATETIME_Null
.head 5 -  Set sPayee = ''
.head 5 -  Set nDisAmnt = NUMBER_Null
.head 5 -  Set sRecoverable = ''
.head 5 -  Set sRecHeading2='Litigants'
.head 5 -  Call SqlPrepareAndExecute( hPrintDocket, "SELECT NAME, '(' || LITTYPE || ')', aliastype,
 ADDR2,
STRNO || ' ' || STRNAME || ' ' || STRTYPE || ' ' || STRDIR,
 CITY || ' ' || STATE || '  ' || ZIP, ATTNO
 INTO :sLitName, :sLitType,          :sAliasType,
 :sLitAddr2, :sLitAddr, :sLitCityStZip, :nAttyNo
FROM LITIGANT WHERE CASEYR = :fCaseYr
    AND CASENO = :fCaseNo
ORDER BY LITTYPE DESC, LITNO ASC")
.head 4 +  Else If nRptFlg = 2
.head 5 -  Set sLitName = ''
.head 5 -  Set sLitType = ''
.head 5 -  Set sLitAddr = ''
.head 5 -  Set sLitCityStZip = ''
.head 5 -  Set sAttorney = ''
.head 5 -  Set sAttyName = ''
.head 5 -  Set sAttyAddr = ''
.head 5 -  Set sAttyCityStZip = ''
.head 5 -  Set sSpace = '   Space'
.head 5 -  Set sRecHeading2=''
.head 5 +  ! If sReport = 'CVCert.qrp'
.head 6 -  Set nRow = SalTblSetRow(  tblDockDisplay, TBL_SetFirstRow )
.head 5 -  Call SqlPrepareAndExecute( hPrintDocket,  'SELECT ALL D.DDATE, D.CODE5,
   C.CODE1 || D.ENTRY1 || C.CODE2 || D.ENTRY2 || C.CODE3 || D.ENTRY3 ||
   C.CODE4 || D.ENTRY4, D.MEMO
FROM DOCKET2 D, CVCODES C  INTO :sDockDate, :sCode5,
   :sDockString, :sDockMemo
 WHERE D.CODE5 = C.CODE (+)
   AND (D.RATTY != 9898989 OR D.RATTY IS NULL)
   AND D.CASEYR = :fCaseYr AND CASENO = :fCaseNo
ORDER BY D.DDATE, D.DNUM' )
.head 4 +  Else If nRptFlg = 3
.head 5 -  Call SqlPrepareAndExecute( hPrintDocket,  'SELECT ALL D.DDATE, D.CODE5,
   C.CODE1 || D.ENTRY1 || C.CODE2 || D.ENTRY2 || C.CODE3 || D.ENTRY3 ||
   C.CODE4 || D.ENTRY4, D.MEMO
FROM DOCKET D, CVCODES C  INTO :sDockDate, :sCode5,
    :sDockString, :sDockMemo
WHERE D.CODE5 = C.CODE (+)
   AND (D.RATTY != 9898989 OR D.RATTY IS NULL)
   AND D.CASEYR = :fCaseYr AND CASENO = :fCaseNo
ORDER BY D.DDATE, D.DNUM' )
.head 4 +  Else If nRptFlg = 4
.head 5 +  If sReport = 'CVCert.qrp'
.head 6 -  Set nRptFlg = nRptFlg + 1
.head 6 -  Set sRecHeading   = ''
.head 6 -  Set sRecHeading2 = ''
.head 6 -  Return TRUE
.head 5 -  Set sSpace = '   Space'
.head 5 -  Set nDockNo = NUMBER_Null
.head 5 -  Set sDockDate = DATETIME_Null
.head 5 -  Set sDockString = ''
.head 5 -  Set sDockMemo = ''
.head 5 -  Set sRecHeading   = ' Rec-Date      Rec-Numb     Cashier      Rec-Amnt       Ledger-Cat.    Adj-Amt    Check-No      Date             Payee                                             Disb-Amt'
.head 5 -  Set sRecHeading2 = ' Rec-Date      Rec-Numb     Cashier      Rec-Amnt       Ledger-Cat.    Adj-Amt    Check-No      Date             Payee                                             Disb-Amt'
.head 5 -  Call SqlPrepareAndExecute(hPrintDocket, "SELECT  rcptdate, receiptno,
   pay_name, total_amt
FROM receipt2  INTO :dRecDate, :nReceiptNo,
   :sCashierDrawer, :nLedgerAmnt
WHERE caseyr = :fCaseYr AND caseno = :fCaseNo
ORDER BY caseyr, caseno, rcptdate  ")
.head 4 +  Else If nRptFlg = 5
.head 5 +  If sReport = 'CVCert.qrp'
.head 6 -  Set nRptFlg = nRptFlg + 1
.head 6 -  Set sRecHeading   = ''
.head 6 -  Set sRecHeading2 = ''
.head 6 -  Return TRUE
.head 5 -  Set sSpace = '   Space'
.head 5 -  Set nDockNo = NUMBER_Null
.head 5 -  Set sDockDate = DATETIME_Null
.head 5 -  Set sDockString = ''
.head 5 -  Set sDockMemo = ''
.head 5 -  Set sSpace = ''
.head 5 -  Set sRecHeading = ''
.head 5 -  Set dRecDate = DATETIME_Null
.head 5 -  Set dCheckDate = DATETIME_Null
.head 5 -  Set nReceiptNo = NUMBER_Null
.head 5 -  Set sCashierDrawer = ''
.head 5 -  Set sPaidByMethod = ''
.head 5 -  Set sLedgerCode = ''
.head 5 -  Set sRecoverable = ''
.head 5 -  Set sAdj = ''
.head 5 -  Set sPayee = ''
.head 5 -  Set nLedgerAmnt = NUMBER_Null
.head 5 -  Set nDisAmnt = NUMBER_Null
.head 5 -  Set nTotalAmnt = NUMBER_Null
.head 5 -  Set nCheckNo = NUMBER_Null
.head 5 -  Set sRecHeading2 = ' Rec-Date      Rec-Numb     Cashier      Rec-Amnt       Ledger-Cat.    Adj-Amt    Check-No      Date             Payee                                             Disb-Amt'
.head 5 -  Call SqlPrepareAndExecute(hPrintDocket, "SELECT ALL m.recdate, m.receiptno,
   m.cashierid || ' ' || m.drawer,
   m.paidby || ' ' || m.method, m.totalamt,
   d.ledgercode, d.recamt, d.adj, d.disamt, d.recoverable,
   c.bankno, c.disbdate, c.payee
FROM receiptmaster m, recdisdetail d, disbmaster c
INTO :dRecDate, :nReceiptNo, :sCashierDrawer,
   :sPaidByMethod, :nTotalAmnt,
   :sLedgerCode, :nLedgerAmnt, :sAdj, :nDisAmnt, :sRecoverable,
   :nCheckNo, :dCheckDate, :sPayee
WHERE m.caseyr = :fCaseYr
  AND m.caseno = :fCaseNo AND d.receiptno = m.receiptno
   AND d.checkno = c.checkno (+)
ORDER BY m.recdate ASC, d.receiptno ASC, d.seqno ASC, d.ledgercode ASC")
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext	!5/5/99  sjj  changed attorney name - first middle last
.head 4 +  If nRptFlg = 1
.head 5 +  While SqlFetchNext( hPrintDocket, nReturn )
.head 6 +  If sAliasType != ''
.head 7 -  Set sLitName = '   ' || sAliasType || ' - ' || sLitName
.head 6 -  Set sAttyNo = SalNumberToStrX( nAttyNo, 0 )
.head 6 +  If sAttyNo = '' or sAttyNo = '0'
.head 7 -  Set sAttorney = ''
.head 7 -  Set sAttyName = ''
.head 7 -  Set sAttyAddr = ''
.head 7 -  Set sAttyCityStZip = ''
.head 6 +  Else
.head 7 -  Set sAttorney = 'Attorney: '
.head 7 -  Call SqlPrepareAndExecute( hSqlAttyD,
   "SELECT FNAME || ' ' ||MNAME || ' ' ||LNAME,
      ADDRESS, CITY || ', ' || STATE || '  '  || ZIP
INTO :sAttyName, :sAttyAddr, :sAttyCityStZip
FROM ATTORNEY WHERE ATTNO = :nAttyNo" )
.head 7 -  Call SqlFetchNext( hSqlAttyD, nReturn )
.head 6 -  Return TRUE
.head 5 -  Call SalSendMsg(hWndForm, SAM_ReportFetchInit, wParam, lParam)
.head 5 -  Return TRUE
.head 4 +  Else If nRptFlg = 2 OR nRptFlg = 3
.head 5 -  Set sSpace = ''
.head 5 +  If sReport = 'CVInq.qrp'
.head 6 +  While SqlFetchNext( hPrintDocket, nReturn )
.head 7 -  ! Set nOffSet = 0
.head 7 +  ! While nOffSet != -1
.head 8 -  Set nOffSet = SalStrScan(  sDockString, '^' )
.head 8 +  If nOffSet > 0
.head 9 -  Call SalStrReplace( sDockString, nOffSet, 1, ' ',  sDockString )
.head 7 +  ! If SalStrTrimX (sDockString) = ''
.head 8 -  Set sDockString = sDockMemo
.head 8 -  Set sDockMemo = ''
.head 7 -  Return TRUE
.head 5 +  If sReport = 'CVCert.qrp'
.head 6 +  While SqlFetchNext( hPrintDocket, nReturn )
.head 7 -  If (sCode5 = 'JEJ' OR sCode5 = 'ATJ' OR sCode5 = 'TRN') OR (sCode5 = 'HS' and nDockNo < 4 ) 
.head 7 +  Else
.head 8 -  Set nDockNo = nDockNo + 1
.head 7 -  Return TRUE
.head 5 -  Set sSpace = ''
.head 5 -  Set sDockDate = DATETIME_Null
.head 5 -  Set sDockString = ''
.head 5 -  Set sDockMemo = ''
.head 5 -  Call SalSendMsg(hWndForm, SAM_ReportFetchInit, wParam, lParam)
.head 5 -  Return TRUE
.head 4 +  Else If nRptFlg = 4
.head 5 -  Set sSpace = ''
.head 5 -  Set sRecHeading = ''
.head 5 -  ! Set nDockNo = nDockNo + 1
.head 5 +  While SqlFetchNext( hPrintDocket, nReturn )
.head 6 +  If nReceiptNo = nPrevReceiptNo AND dRecDate = dPrevRecDate AND
sCashierDrawer = sPrevCashierDrawer
.head 7 -  Set dRecDate = DATETIME_Null
.head 7 -  Set nReceiptNo = NUMBER_Null
.head 7 -  Set sCashierDrawer = ''
.head 7 -  Set nTotalAmnt = NUMBER_Null
.head 6 +  Else
.head 7 -  Set dPrevRecDate = dRecDate
.head 7 -  Set nPrevReceiptNo = nReceiptNo
.head 7 -  Set sPrevCashierDrawer = sCashierDrawer
.head 7 -  Set nPrevTotalAmnt = nTotalAmnt
.head 6 -  Set sAdj = ''
.head 6 -  Set nLedgerAmnt2 = NUMBER_Null
.head 6 -  Return TRUE
.head 5 -  Call SalSendMsg(hWndForm, SAM_ReportFetchInit, wParam, lParam)
.head 5 -  Return TRUE
.head 4 +  Else If nRptFlg = 5
.head 5 -  Set sSpace = ''
.head 5 -  Set sRecHeading = ''
.head 5 +  While SqlFetchNext( hPrintDocket, nReturn )
.head 6 +  If nReceiptNo = nPrevReceiptNo AND dRecDate = dPrevRecDate AND
sCashierDrawer = sPrevCashierDrawer
.head 7 -  Set dRecDate = DATETIME_Null
.head 7 -  Set nReceiptNo = NUMBER_Null
.head 7 -  Set sCashierDrawer = ''
.head 7 -  Set nTotalAmnt = NUMBER_Null
.head 6 +  Else
.head 7 -  Set dPrevRecDate = dRecDate
.head 7 -  Set nPrevReceiptNo = nReceiptNo
.head 7 -  Set sPrevCashierDrawer = sCashierDrawer
.head 7 -  Set nPrevTotalAmnt = nTotalAmnt
.head 6 +  If sAdj = 'N'
.head 7 -  Set sAdj = ''
.head 7 -  Set nLedgerAmnt2 = NUMBER_Null
.head 6 +  Else If sAdj = 'Y'
.head 7 -  Set nLedgerAmnt2 = NUMBER_Null
.head 6 +  Else
.head 7 -  Set nLedgerAmnt2 =nLedgerAmnt
.head 7 -  Set nLedgerAmnt = NUMBER_Null
.head 6 -  Return TRUE
.head 4 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Call SqlCommit( hSql )
.head 4 -  Call SqlCommit( hPrintDocket )
.head 4 -  Return FALSE
.head 3 +  On SAM_Create
.head 4 -  Call IsBackup( )
.head 4 -  Call SqlConnect( hSqlAttyD )
.head 4 -  Call SqlSetResultSet( hSqlAttyD, TRUE)
.head 3 +  On SAM_CreateComplete
.head 4 -  Call fPrintDocket(  )
.head 4 -  Call SqlDisconnect( hSqlAttyD )
.head 4 -  Call SalEndDialog( dlgPrintCVDocket, 1)
.head 1 +  Dialog Box: dlgSqlErrorReport
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.525"
.head 4 -  Top: 0.042"
.head 4 -  Width:  6.633"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 5.563"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: White
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 4.443"
.head 5 -  Top: 5.125"
.head 5 -  Width:  1.914"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.298"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SalDisableWindow( pbOk )
.head 6 -  Call SalDisableWindow( pbHalt )
.head 6 -  Call SalUpdateWindow( dlgSqlErrorReport )
.head 6 -  Call SaveScreen( )
.head 6 -  Call SalSendMsg( hWndForm, SAM_Close, wParam, lParam )
.head 6 -  Call SalWaitCursor( FALSE )
.head 3 +  Pushbutton: pbHalt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Halt Application
.head 4 -  Window Location and Size
.head 5 -  Left: 2.4"
.head 5 -  Top: 5.115"
.head 5 -  Width:  1.914"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.298"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SalDisableWindow( pbOk )
.head 6 -  Call SalDisableWindow( pbHalt )
.head 6 -  Call SalUpdateWindow( dlgSqlErrorReport )
.head 6 -  Call SaveScreen( )
.head 6 -  Call SalQuit(  )
.head 6 -  Call SalWaitCursor( FALSE )
.head 3 +  Pushbutton: pbPrintScreen
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print Screen
.head 4 -  Window Location and Size
.head 5 -  Left: 0.371"
.head 5 -  Top: 5.115"
.head 5 -  Width:  1.914"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.298"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call fPrintScreen(  )
.head 3 +  Multiline Field: mlError
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  String Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Border? Yes
.head 5 -  Word Wrap? Yes
.head 5 -  Vertical Scroll? Yes
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 2.792"
.head 6 -  Width:  6.317"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 2.012"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Multiline Field: mlStatus
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  String Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Border? Yes
.head 5 -  Word Wrap? Yes
.head 5 -  Vertical Scroll? Yes
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 0.688"
.head 6 -  Width:  6.317"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 2.012"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  ! Pushbutton: pbPrint
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Print
.head 4 -  Window Location and Size
.head 5 -  Left: 3.95"
.head 5 -  Top: 5.345"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.298"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.end
.head 4 +  Message Actions 
.head 5 +  On SAM_Click
.head 6 -  Call fPrintError( )
.head 3 -  Background Text: There has been a database error. To help us diagnose and fix the problem please type in a brief description of what you where doing before this error screen appeared.
.head 4 -  Resource Id: 7811
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 1.3"
.head 5 -  Top: 0.048"
.head 5 -  Width:  5.0"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.512"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: There has been a database error.  Please print this message and consult your supervisor.
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Picture: pic1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.129"
.head 5 -  Top: 0.021"
.head 5 -  Width:  1.0"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.625"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name: O:\apps\images\error_button.jpg
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Size to Fit
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 1
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  ! Function: fPrintError
.head 4 -  Description: 
.head 4 -  Returns 
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  Number: nReportError
.head 5 -  String: sInputs
.head 5 -  String: sVariables
.head 5 -  String: sAppName
.head 5 -  String: sAppVersion
.head 4 +  Actions 
.head 5 -  Call SalStrTrim( mlDescription, mlDescription )
.head 5 +  If mlDescription = STRING_Null
.head 6 -  Call SalMessageBox( 'There is nothing in the description box.
Please enter some information and try again.','Description error', MB_Ok | MB_IconStop )
.head 6 -  Return FALSE
.head 5 -  Set sAppName = sGLAppName
.head 5 -  Set sAppVersion = sGLAppVersion
.head 5 -  Set sInputs =  'sAppName, sAppVersion, sSqlStatement, sErrorMessage, sDescription, sDatabase, sUserName, nClientNo,  nErrorNumber'
.head 5 -  Set sVariables =  'sAppName, sAppVersion, sSqlStatement, sErrorText, mlDescription, SqlDatabase, SqlUser, nGLClientNo, nErrorNumber'
.head 5 +  If nReportCounter >= 1
.head 6 +  If SalMessageBox( 'This error report has already been printed.
Would you like to print the report again?', 'Printing', MB_YesNo | MB_IconQuestion | MB_DefButton2 ) = IDYES
.head 7 -  Set nReportCounter = 0
.head 7 -  Call SalReportPrint( hWndForm, 'SQLError.qrp', sVariables, sInputs, 1, RPT_PrintAll, 0, 0, nReportError )
.head 5 +  Else 
.head 6 -  Call SalReportPrint( hWndForm, 'SQLError.qrp', sVariables, sInputs, 1, RPT_PrintAll, 0, 0, nReportError )
.head 3 +  Function: SaveScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sPath
.head 5 -  String: sFile
.head 5 -  String: sFull
.head 5 -  String: sDrive
.head 5 -  String: sDir
.head 5 -  String: sAppName
.head 5 -  String: sExt
.head 4 +  Actions
.head 5 -  Set sPath = 'O:\\Images\\App_Error\\'|| SQL.sComputerName 
.head 5 +  If NOT VisDosExist( sPath )
.head 6 -  Call SalFileCreateDirectory( sPath )
.head 5 -  Call VisDosSplitPath ( strArgArray[0] , sDrive, sDir, sAppName, sExt)
.head 5 -  Set sFile = sAppName ||' - '||SalNumberToStrX( nErrorNumber, 0) || SalFmtFormatDateTime( SalDateCurrent(  ), 'M-d-yy-hh.mm.ss AMPM' )
.head 5 -  ! Set sFile = sAppName ||' - '||SalNumberToStrX( nErrorNumber, 0) ||' - ' || SalFmtFormatDateTime( Current_Date( '' ), 'MM-dd-yyyy-hh:mm:ss' )||'_1.tif'
.head 5 -  Set sFull = sPath|| '\\'|| sFile
.head 5 -  Call fSaveScreen(sFull ||'_1.tif')
.head 5 -  Call SalPause( 2000 )
.head 5 -  ! Call SalHideWindow( dlgSqlErrorReport )
.head 5 -  ! Call SalShowWindow( SQL.hWndParent )
.head 5 -  ! Call SalBringWindowToTop( SQL.hWndParent )
.head 5 -  ! Set sFull = sPath|| '\\'|| sFile
.head 5 -  ! Call fSaveScreen( sFull ||'_2.tif')
.head 5 -  ! Call SalPause( 2000 )
.head 2 +  Window Parameters
.head 3 -  String: sSqlStatement
.head 3 -  String: sErrorText
.head 3 -  Number: nErrorNumber
.head 2 +  Window Variables
.head 3 -  ! Number: nReportCounter
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalSetWindowText( hWndForm, strArgArray[0] )
.head 4 -  Set mlError = 'Error description:
'||sErrorText||'

SQL Statement:
'||sSqlStatement
.head 4 -  Set mlStatus = 'SQL User: '|| SqlUser ||'
SQL Database: '|| SqlDatabase ||'
Error Number: '||  SalNumberToStrX( nErrorNumber, 0 )||'
OS User: '|| SQL.sOSUserName||'
Application Name: '|| strArgArray[0]
.head 4 -  ! Set dfUserName = SqlUser
.head 4 -  ! Set dfDatabaseName = SqlDatabase
.head 4 -  ! Set dfErrorNumber = SalNumberToStrX( nErrorNumber, 0 )
.head 4 -  Call SalCenterWindow( hWndForm )
.head 4 -  Call SalWaitCursor( FALSE )
.head 3 +  ! On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  ! On SAM_ReportStart
.head 4 -  Return TRUE
.head 3 +  ! On SAM_ReportFetchNext
.head 4 -  Set nReportCounter = nReportCounter + 1
.head 4 +  If nReportCounter > 1
.head 5 -  Return FALSE
.head 4 +  Else 
.head 5 -  Return TRUE
.head 1 +  Dialog Box: dlgCrLabels
.head 2 -  Class: cQuickTabsDialog
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Labels Processing
.head 2 -  Accessories Enabled? Yes
.head 2 -  Visible? No
.head 2 -  Display Settings
.head 3 -  Display Style? Class Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Class Default
.head 3 -  Allow Dock to Parent? Class Default
.head 3 -  Docking Orientation: Class Default
.head 3 -  Window Location and Size
.head 4 -  Left: 2.538"
.head 4 -  Top: 0.052"
.head 4 -  Width:  7.629"
.head 4 -  Width Editable? Class Default
.head 4 -  Height: 5.958"
.head 4 -  Height Editable? Class Default
.head 3 -  Absolute Screen Location? Class Default
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Class Default
.head 3 -  Background Color: Class Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Class Default
.head 3 -  Horizontal Scroll? Class Default
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Class Default
.head 4 -  Location? Top
.head 4 -  Visible? Class Default
.head 4 -  Size: 0.458"
.head 4 -  Size Editable? Class Default
.head 4 -  Docking Toolbar? Class Default
.head 4 -  Toolbar Docking Orientation: Class Default
.head 4 -  Font Name: Class Default
.head 4 -  Font Size: Class Default
.head 4 -  Font Enhancement: Class Default
.head 4 -  Text Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  Resizable? Class Default
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 +  Contents
.head 4 +  Pushbutton: pbPrint
.data CLASSPROPSSIZE
0000: 4400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE2200
0020: 7400620041006400 6400720065007300 7300090074006200 4C00690073007400
0040: 00000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Print &Label
.head 5 -  Window Location and Size
.head 6 -  Left: 1.838"
.head 6 -  Top: 0.073"
.head 6 -  Width:  1.475"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: Enter
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 +  ! If sCourtConst ='CMC'
.head 8 -  Set sReport = 'Envelope.qrp'
.head 7 +  ! Else If sCourtConst ='AMC'
.head 8 -  Set sReport = 'Envelope.qrp'
.head 7 -  ! Else If sCourtConst ='MMC'
.head 7 -  Set sReport = 'Labelmmc.qrp'
.head 7 +  If dfName != ''
.head 8 -  Call PrintLabels(  )
.head 4 +  Pushbutton: pbPrintCert
.data CLASSPROPSSIZE
0000: 4400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE2200
0020: 7400620041006400 6400720065007300 7300090074006200 4C00690073007400
0040: 00000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Print Certified
.head 5 -  Window Location and Size
.head 6 -  Left: 3.375"
.head 6 -  Top: 0.073"
.head 6 -  Width:  1.475"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 +  If sCaseYrP != '0'
.head 8 -  Call SalModalDialog(dlgCertifiedMail, hWndForm, dfName, sCaseYrP||'-'||sCaseTyP||'-'||sCaseNoP, dfAddress, dfAddress2, dfLabelCity, dfLabelState, dfLabelZip, SalStrToNumber( sCaseYrP) , sCaseTyP, SalStrToNumber(sCaseNoP), '', 'CRIM')
.head 7 +  Else
.head 8 -  Call SalModalDialog(dlgCertifiedMail, hWndForm, dfName, '', dfAddress, dfAddress2, dfLabelCity, dfLabelState, dfLabelZip, SalStrToNumber( sCaseYrP) , sCaseTyP, SalStrToNumber(sCaseNoP), '', 'CRIM')
.head 6 +  On SAM_Create
.head 7 +  If sUDivision != 'JU'
.head 8 -  Call SalShowWindow( MyValue )
.head 7 +  Else
.head 8 -  Call SalHideWindow( MyValue )
.head 4 +  Pushbutton: pbCancel
.data CLASSPROPSSIZE
0000: 4400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE2200
0020: 7400620041006400 6400720065007300 7300090074006200 4C00690073007400
0040: 00000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: &Cancel, Exit
.head 5 -  Window Location and Size
.head 6 -  Left: 4.888"
.head 6 -  Top: 0.073"
.head 6 -  Width:  1.475"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: Esc
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Call SalEndDialog( dlgCrLabels, 0 )
.head 4 +  Data Field: dfNumber
.data CLASSPROPSSIZE
0000: 4400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE2200
0020: 7400620041006400 6400720065007300 7300090074006200 4C00690073007400
0040: 00000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Data
.head 6 -  Maximum Data Length: 2
.head 6 -  Data Type: Number
.head 6 -  Editable? Yes
.head 5 -  Display Settings
.head 6 -  Window Location and Size
.head 7 -  Left: 0.95"
.head 7 -  Top: 0.125"
.head 7 -  Width:  0.586"
.head 7 -  Width Editable? Yes
.head 7 -  Height: 0.219"
.head 7 -  Height Editable? Yes
.head 6 -  Visible? Yes
.head 6 -  Border? Yes
.head 6 -  Justify: Right
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Font Name: Default
.head 6 -  Font Size: Default
.head 6 -  Font Enhancement: Default
.head 6 -  Text Color: Default
.head 6 -  Background Color: Default
.head 6 -  Input Mask: Unformatted
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Spell Check? No
.head 5 -  Message Actions
.head 4 -  Background Text: bkgd9
.data CLASSPROPSSIZE
0000: 4400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE2200
0020: 7400620041006400 6400720065007300 7300090074006200 4C00690073007400
0040: 00000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 5 -  Resource Id: 10820
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Window Location and Size
.head 6 -  Left: 0.325"
.head 6 -  Top: 0.156"
.head 6 -  Width:  0.588"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.167"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Justify: Left
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Title: # Labels
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 2 +  Contents
.head 3 +  Picture: picTabs
.data CLASSPROPS
0000: 5400610062005400 6F0070004D006100 7200670069006E00 0000FFFE04003000
0020: 0000540061006200 46006F0072006D00 5000610067006500 73000000FFFE0400
0040: 0900000054006100 6200440072006100 7700530074007900 6C0065000000FFFE
0060: 1600570069006E00 3900350053007400 79006C0065000000 5400610062004200
0080: 6F00740074006F00 6D004D0061007200 670069006E000000 FFFE040030000000
00A0: 5400610062005000 6100670065004300 6F0075006E007400 0000FFFE04003100
00C0: 0000540061006200 4C00610062006500 6C0073000000FFFE 1A00410064006400
00E0: 7200650073007300 09004C0069007300 7400000054006100 62004E0061006D00
0100: 650073000000FFFE 2200740062004100 6400640072006500 7300730009007400
0120: 62004C0069007300 7400000054006100 6200520069006700 680074004D006100
0140: 7200670069006E00 0000FFFE04003000 0000540061006200 4300750072007200
0160: 65006E0074000000 FFFE0E0074006200 4C00690073007400 0000540061006200
0180: 4C00650066007400 4D00610072006700 69006E000000FFFE 0400300000000000
01A0: 0000000000000000 0000000000000000 0000
.enddata
.data CLASSPROPSSIZE
0000: B201
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 1
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cQuickTabsDialog
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: Class Default
.head 5 -  Top: Class Default
.head 5 -  Width:  7.329"
.head 5 -  Width Editable? Class Default
.head 5 -  Height: 5.302"
.head 5 -  Height Editable? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Editable? Class Default
.head 4 -  File Name:
.head 4 -  Storage: Class Default
.head 4 -  Picture Transparent Color: Class Default
.head 4 -  Fit: Class Default
.head 4 -  Scaling
.head 5 -  Width:  Class Default
.head 5 -  Height:  Class Default
.head 4 -  Corners: Class Default
.head 4 -  Border Style: Class Default
.head 4 -  Border Thickness: Class Default
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 3 +  Child Table: tblDisplay
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.157"
.head 6 -  Top: 1.646"
.head 6 -  Width:  7.088"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 3.708"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Yes
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 100000
.head 5 -  Discardable? No
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: col1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  4.3"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 1, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay, 1, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 5 +  Column: col2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  4.333"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 2, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay,2, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 5 +  Column: colRowID
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 3, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay, 3, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 4 -  Functions
.head 4 +  Window Variables
.head 5 -  Boolean: bIncr
.head 4 +  Message Actions
.head 5 +  On SAM_DoubleClick
.head 6 -  ! Set sReturnRowid = colRowID
.head 6 -  ! Call SalEndDialog(dlgLabelHelp, 1)
.head 6 -  ! Set sReturnRowid = colRowID
.head 6 -  Call FillScreen( colRowID )
.head 6 -  Call picTabs.BringToTop( 0, TRUE )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_RETURN
.head 7 -  Call SalSendMsg( tblDisplay, SAM_DoubleClick, 0, 0 )
.head 5 +  On SAM_Create
.head 6 -  Call SAM_CreateTBL( )
.head 6 -  Call SalTblSetTableFlags( tblDisplay, TBL_Flag_SelectableCols, TRUE )
.head 3 +  Radio Button: rbDefendant
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: For Defendant - F1
.head 4 -  Window Location and Size
.head 5 -  Left: 0.4"
.head 5 -  Top: 0.573"
.head 5 -  Width:  1.943"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SalSendMsg( rbDefendant, SAM_Click, 0, 0 )
.head 5 +  On SAM_Click
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call FillTable(  )
.head 6 +  Else If fType  = 'CRIM'
.head 7 -  Call FillScreen( '' )
.head 3 +  Radio Button: rbDAttorney
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Defendant's Atty - F2
.head 4 -  Window Location and Size
.head 5 -  Left: 0.414"
.head 5 -  Top: 0.917"
.head 5 -  Width:  2.0"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  ! Call FillScreen( '' )
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call FillTable(  )
.head 6 +  Else If fType  = 'CRIM'
.head 7 -  Call FillScreen( '' )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbBondsman
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Bondsman - F3
.head 4 -  Window Location and Size
.head 5 -  Left: 0.4"
.head 5 -  Top: 1.271"
.head 5 -  Width:  1.538"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'BOND', sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbOtherAtt
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Attorney - F5
.head 4 -  Window Location and Size
.head 5 -  Left: 2.557"
.head 5 -  Top: 0.917"
.head 5 -  Width:  1.943"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'ATT', sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 3 +  Radio Button: rbProbCS
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Probation/CS Agency - F7
.head 4 -  Window Location and Size
.head 5 -  Left: 4.6"
.head 5 -  Top: 0.583"
.head 5 -  Width:  2.413"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'PROBCS', sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 3 +  Radio Button: rbLit
.data CLASSPROPSSIZE
0000: 3000
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 7300740000000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Litigant - F8
.head 4 -  Window Location and Size
.head 5 -  Left: 4.6"
.head 5 -  Top: 0.917"
.head 5 -  Width:  1.357"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'PROBCS', sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 5 +  On SAM_Create
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call picTabs.ShowWindow( rbLit )
.head 7 -  Call picTabs.ShowWindow( rbLitAtt )
.head 6 +  Else If fType= 'CRIM'
.head 7 -  Call picTabs.HideWindow( rbLit )
.head 7 -  Call picTabs.HideWindow( rbLitAtt )
.head 7 -  Call SalHideWindow( rbLit )
.head 7 -  Call SalHideWindow( rbLitAtt)
.head 3 +  Radio Button: rbLitAtt
.data CLASSPROPSSIZE
0000: 3000
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 7300740000000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Litigant's Atty - F9
.head 4 -  Window Location and Size
.head 5 -  Left: 4.6"
.head 5 -  Top: 1.271"
.head 5 -  Width:  1.843"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'PROBCS', sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 5 +  On SAM_Create
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call picTabs.ShowWindow( rbLit )
.head 7 -  Call picTabs.ShowWindow( rbLitAtt )
.head 6 +  Else If fType= 'CRIM'
.head 7 -  Call picTabs.HideWindow( rbLit )
.head 7 -  Call picTabs.HideWindow( rbLitAtt )
.head 7 -  Call SalHideWindow( rbLit )
.head 7 -  Call SalHideWindow( rbLitAtt)
.head 3 +  Radio Button: rbOtherAgency
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Agency - F6
.head 4 -  Window Location and Size
.head 5 -  Left: 2.557"
.head 5 -  Top: 1.271"
.head 5 -  Width:  1.914"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! Call SalModalDialog(dlgLabelHelp, hWndForm, 'OTHERA', sReturnRowid)
.head 7 -  ! Call FillScreen( )
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'OTHERA', sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 3 +  Radio Button: rbPolice
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Police Agency - F4
.head 4 -  Window Location and Size
.head 5 -  Left: 2.557"
.head 5 -  Top: 0.573"
.head 5 -  Width:  1.957"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  ! If SalModalDialog(dlgLabelHelp, hWndForm, 'POLICE',sReturnRowid) = 1
.head 7 -  Call FillScreen( )
.head 6 -  Call FillTable( )
.head 3 +  Frame: frame1
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 730074000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 10814
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.171"
.head 5 -  Top: 0.479"
.head 5 -  Width:  6.975"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.115"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Border Color: Default
.head 4 -  Background Color: White
.head 4 -  Xaml:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Message Actions
.head 3 +  Data Field: dfName
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.557"
.head 6 -  Top: 1.052"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.557"
.head 6 -  Top: 1.365"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress2
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.557"
.head 6 -  Top: 1.729"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelCity
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.557"
.head 6 -  Top: 2.104"
.head 6 -  Width:  2.963"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelState
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.529"
.head 6 -  Top: 2.104"
.head 6 -  Width:  0.538"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelZip
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.071"
.head 6 -  Top: 2.104"
.head 6 -  Width:  2.013"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd6
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 10815
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.671"
.head 5 -  Top: 1.115"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd7
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 10816
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.671"
.head 5 -  Top: 1.813"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address 2:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd8
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 10817
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.671"
.head 5 -  Top: 2.219"
.head 5 -  Width:  0.913"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: City:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd11
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 10818
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.671"
.head 5 -  Top: 1.417"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Group Box: grp1
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620041006400 6400720065007300 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 10819
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.357"
.head 5 -  Top: 0.813"
.head 5 -  Width:  6.813"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.698"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address Information:
.head 4 -  Line Thickness: 1
.head 4 -  Line Color: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton: pb1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F1
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDefendant = TRUE
.head 6 -  Call SalSendMsg(rbDefendant, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F2
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDAttorney = TRUE
.head 6 -  Call SalSendMsg(rbDAttorney, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb3
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F3
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbBondsman = TRUE
.head 6 -  Call SalSendMsg(rbBondsman, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb4
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F4
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbPolice = TRUE
.head 6 -  Call SalSendMsg(rbPolice, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb5
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F5
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAtt = TRUE
.head 6 -  Call SalSendMsg(rbOtherAtt, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb6
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F6
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAgency = TRUE
.head 6 -  Call SalSendMsg(rbOtherAgency, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb7
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F7
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbProbCS = TRUE
.head 6 -  Call SalSendMsg(rbProbCS, SAM_Click, 0,0)
.head 3 +  Child Table: tblPltfCV_IL
.data CLASSPROPSSIZE
0000: 3000
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004C006900 7300740000000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.171"
.head 6 -  Top: 1.635"
.head 6 -  Width:  7.086"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 3.708"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Default
.head 5 -  Discardable? Yes
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colLitNoCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: No.
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 4
.head 6 -  Data Type: Number
.head 6 -  Justify: Left
.head 6 -  Width:  0.5"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colNameCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Name
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.267"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colAddr2CV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address Line 2
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.283"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colAddrCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Street Name
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.633"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colCityStCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: City, State, Zip
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.983"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colCityCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: City
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.286"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colStateCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: State
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.55"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colZipCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Zip
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.771"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colRowidCV_IL
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Rowid
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.771"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 +  Message Actions
.head 5 +  On SAM_DoubleClick
.head 6 -  ! Set sReturnRowid = colRowID
.head 6 -  ! Call SalEndDialog(dlgLabelHelp, 1)
.head 6 -  ! Set sReturnRowid = colRowID
.head 6 -  Call FillScreen( colRowidCV_IL )
.head 6 -  Call picTabs.BringToTop( 0, TRUE )
.head 5 +  On SAM_FetchRowDone
.head 6 +  ! If colAKACV_I = '' or colAKACV_I = ' '
.head 7 -  Set nCount = nCount + 1
.head 6 +  ! If nCount > 1
.head 7 -  Set sPltfEtal = '(ETAL)'
.head 6 +  ! Else
.head 7 -  Set sPltfEtal = ''
.head 3 +  Pushbutton: pb8
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F8
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbLit = TRUE
.head 6 -  Call SalSendMsg(rbLit, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb9
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F9
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbLitAtt = TRUE
.head 6 -  Call SalSendMsg(rbLitAtt, SAM_Click, 0,0)
.head 2 +  Functions
.head 3 +  Function: PrintLabels
.head 4 -  Description: Call this function to 15/16 x 3 1/2 Inch Labels
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sPrinterNameL = GetDestPrinter( 'CRIM_LABEL' )
.head 5 -  Set sCSZ = dfLabelCity||', '||dfLabelState||'   '||dfLabelZip
.head 5 -  Set sAddress = dfAddress
.head 5 -  Set sAddress2 = dfAddress2
.head 5 +  If sAddress2 = ''
.head 6 -  Set sAddress2 = sCSZ
.head 6 -  Set sCSZ = ''
.head 5 -  Set nPrintErr = -1
.head 5 +  If rbDefendant = TRUE
.head 6 -  Set sCaseFullLe = sCaseYrP||'-'||sCaseTyP||'-'||sCaseNoP
.head 5 +  Else
.head 6 -  Set sCaseFullLe = ''
.head 5 -  Set sReportBinds = 'dfName,sAddress, sAddress2, sCSZ, sCaseFullLe'
.head 5 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM'
.head 5 -  Call SalReportPrint ( dlgCrLabels, sReport, sReportBinds, sReportInputs,
	dlgCrLabels.dfNumber, RPT_PrintAll| RPT_PrintNoWarn, nRow, nMaxRow, nPrintErr )
.head 5 +  If nPrintErr > 0
.head 6 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),
	MB_Ok )
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sReturnRowid
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If rbDefendant = TRUE
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	NAME, ADDR2,STRNO || ' ' || STRNAME || ' ' || STRTYPE || ' ' || STRDIR || ' ' || APT,   CITY, STATE, ZIP
			      FROM		LITIGANT
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 +  If dfAddress = ''
.head 8 -  Set dfAddress = dfAddress2
.head 8 -  Set dfAddress2 = ''
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 6 +  Else If fType = 'CRIM'
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	lname||', '||fname||' '||mname, address1, city, state, zip
			     FROM		cr_parties
			     INTO		:dfName, :dfAddress, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP")
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.ShowWindow( tblDisplay )
.head 7 -  Call picTabs.BringToTop( 0, TRUE )
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	casecode
			     FROM		cr_docket
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP and
					casecode = 'SCI'")
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 +  If nReturn = FETCH_Ok
.head 8 -  Call SalMessageBox( 'If printing a Defendant label, please be advised that Defendant has a Secondary Contact Infomation in Docket', 'Secondary Information', MB_IconExclamation | MB_Ok)
.head 7 +  If sCaseNoP = ''
.head 8 -  Call ClearScreen( )
.head 5 +  Else If rbLit = TRUE
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	NAME, ADDR2,STRNO || ' ' || STRNAME || ' ' || STRTYPE || ' ' || STRDIR || ' ' || APT,   CITY, STATE, ZIP
			      FROM		LITIGANT
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 +  If dfAddress = ''
.head 8 -  Set dfAddress = dfAddress2
.head 8 -  Set dfAddress2 = ''
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 6 +  If sCaseNoP = ''
.head 7 -  Call ClearScreen( )
.head 5 +  Else If rbDAttorney = TRUE
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	a.fname||' '||a.mname||' '||a.lname, a.address, a.address2, a.city, a.state, a.zip
			      FROM	attorney a
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	a.rowid = :sReturnRowid")
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 +  If dfAddress = ''
.head 8 -  Set dfAddress = dfAddress2
.head 8 -  Set dfAddress2 = ''
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 6 +  Else
.head 7 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute(hSql, 'SELECT	attno
			     FROM		muni_booking
			     INTO		:nAttNo
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP')
.head 8 -  Call SqlFetchNext(hSql, nReturn)
.head 8 -  If nAttNo = NUMBER_Null
.head 8 +  Else
.head 9 -  Call SqlPrepareAndExecute(hSql, "SELECT	 fname||' '||mname||' '||lname, address, address2,
					 city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	attno = :nAttNo")
.head 9 -  Call SqlFetchNext(hSql, nReturn)
.head 9 -  Call picTabs.BringToTop( 0, TRUE )
.head 5 +  Else If rbLitAtt
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call SqlPrepareAndExecute(hSql, "SELECT	a.fname||' '||a.mname||' '||a.lname, a.address, a.address2, a.city, a.state, a.zip
			      FROM	attorney a
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	a.rowid = :sReturnRowid")
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 +  If dfAddress = ''
.head 8 -  Set dfAddress = dfAddress2
.head 8 -  Set dfAddress2 = ''
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 5 +  Else If rbBondsman = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	bondsman,address1, address2, city, state, zip1
			      FROM		cr_bondsman
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 5 +  Else If rbOtherAtt = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	fname||' '||mname||' '||lname,address, address2, city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 5 +  Else If rbPolice = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agency,address1, address2, city, state, zip
			      FROM		agency_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 5 +  Else If rbOtherAgency = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agencynm,address1, address2, city, state, zip
			      FROM		other_agency
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 5 +  Else If rbProbCS = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	description, supervisor ,address1, city, state, zip
			      FROM		program_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If dfAddress != ''
.head 7 -  Set dfAddress = 'ATTN. '||dfAddress
.head 6 +  Else
.head 7 -  Set dfAddress = dfAddress2
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 3 +  Function: ClearScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set dfName = ''
.head 5 -  Set dfAddress = ''
.head 5 -  Set dfAddress2 = ''
.head 5 -  Set dfLabelCity = ''
.head 5 -  Set dfLabelState = ''
.head 5 -  Set dfLabelZip = ''
.head 5 -  Set sCSZ = ''
.head 3 +  Function: FillTable
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sSelect
.head 5 -  Number: nCaseYr
.head 5 -  Number: nCaseNo
.head 5 -  String: sLitType
.head 4 +  Actions
.head 5 -  Call SalShowWindow( tblDisplay.col2)
.head 5 +  If rbBondsman
.head 6 -  Set sSelect = 'SELECT	bondsman, insurance_co, rowid
			           FROM	cr_bondsman
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	bondsman'
.head 6 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'BONDSMAN')
.head 6 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'INSURANCE CO')
.head 6 -  Call SalTblPopulate(tblDisplay, hSql, sSelect, TBL_FillNormal)
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 5 +  Else If rbOtherAtt
.head 6 -  Set sSelect ="SELECT	to_char(attno), lname||', '||fname||' '||mname, rowid
			           FROM	attorney
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	attno"
.head 6 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'ATT. #')
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 6 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'ATT NAME')
.head 6 -  Call SalTblPopulate(tblDisplay, hSql, sSelect, TBL_FillNormal)
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 5 +  Else If rbPolice
.head 6 -  Set sSelect = "SELECT	code, agency, rowid
			           FROM	agency_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	code"
.head 6 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY CODE')
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 6 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'AGENCY NAME')
.head 6 -  Call SalTblPopulate(tblDisplay, hSql, sSelect, TBL_FillNormal)
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 5 +  Else If rbOtherAgency
.head 6 -  Set sSelect = "SELECT	agencynm, rowid
			           FROM	other_agency
			           INTO	:tblDisplay.col1, :tblDisplay.colRowID
			           ORDER BY	agencynm"
.head 6 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY NAME')
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col1,7.088)
.head 6 -  Call SalHideWindow( tblDisplay.col2)
.head 6 -  Call SalTblPopulate(tblDisplay, hSql, sSelect, TBL_FillNormal)
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 5 +  Else If rbProbCS
.head 6 -  Set sSelect = 'SELECT	agency, description, rowid
			           FROM	program_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	agency'
.head 6 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY NAME')
.head 6 -  Call SalTblSetColumnTitle(  tblDisplay.col2, 'DESCRIPTION')
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 6 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 6 -  Call SalTblPopulate(tblDisplay, hSql, sSelect, TBL_FillNormal)
.head 6 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.ShowWindow( tblDisplay )
.head 5 +  Else If rbDefendant
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 7 -  Set nCaseYr = SalStrToNumber( sCaseYrP )
.head 7 -  Set nCaseNo = SalStrToNumber( sCaseNoP )
.head 7 -  Set sSelect="SELECT 	ALL  LITNO, NAME, ADDR2,
  			STRNO || ' ' || STRNAME || ' ' || STRTYPE || ' ' || STRDIR || ' ' || APT,
  			CITY || ', ' || STATE || ' ' || ZIP,   CITY, STATE, ZIP, ROWID FROM LITIGANT
 	      WHERE 	CASEYR=:nCaseYr and CASENO=:nCaseNo and LITTYPE = :sLitType"
.head 7 -  Set sLitType = 'D'
.head 7 -  Call SalTblPopulate( tblPltfCV_IL, hSql, sSelect, TBL_FillAll)
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 5 +  Else If rbLit
.head 6 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.HideWindow( tblDisplay )
.head 6 -  Set nCaseYr = SalStrToNumber( sCaseYrP )
.head 6 -  Set nCaseNo = SalStrToNumber( sCaseNoP )
.head 6 -  Set sSelect="SELECT 	ALL  LITNO, NAME, ADDR2,
  			STRNO || ' ' || STRNAME || ' ' || STRTYPE || ' ' || STRDIR || ' ' || APT,
  			CITY || ', ' || STATE || ' ' || ZIP,   CITY, STATE, ZIP, ROWID FROM LITIGANT
 	      WHERE 	CASEYR=:nCaseYr and CASENO=:nCaseNo and LITTYPE = :sLitType"
.head 6 -  Set sLitType = 'P'
.head 6 -  Call SalTblPopulate( tblPltfCV_IL, hSql, sSelect, TBL_FillAll)
.head 6 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 6 -  Call picTabs.HideWindow( tblDisplay )
.head 5 +  Else If rbDAttorney
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 7 -  Set nCaseYr = SalStrToNumber( sCaseYrP )
.head 7 -  Set nCaseNo = SalStrToNumber( sCaseNoP )
.head 7 -  Set sSelect="SELECT 	l.litno, a.fname||' '||a.mname||' '||a.lname, a.address, a.address2, a.city||', '||a.state||' '||a.zip, a.city, a.state, a.zip, a.rowid
                      FROM 	LITIGANT l, attorney a
 	      WHERE 	l.CASEYR=:nCaseYr and l.CASENO=:nCaseNo and LITTYPE = :sLitType and l.attno is not null AND l.attno IS NOT NULL AND l.attno = a.attno"
.head 7 -  Set sLitType = 'D'
.head 7 -  Call SalTblPopulate( tblPltfCV_IL, hSql, sSelect, TBL_FillAll)
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 5 +  Else If rbLitAtt
.head 6 +  If fType = 'CIVIL'
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 7 -  Set nCaseYr = SalStrToNumber( sCaseYrP )
.head 7 -  Set nCaseNo = SalStrToNumber( sCaseNoP )
.head 7 -  Set sSelect="SELECT 	l.litno, a.fname||' '||a.mname||' '||a.lname, a.address, a.address2, a.city||', '||a.state||' '||a.zip, a.city, a.state, a.zip, a.rowid
                      FROM 	LITIGANT l, attorney a
 	      WHERE 	l.CASEYR=:nCaseYr and l.CASENO=:nCaseNo and LITTYPE = :sLitType and l.attno is not null AND l.attno IS NOT NULL AND l.attno = a.attno"
.head 7 -  Set sLitType = 'P'
.head 7 -  Call SalTblPopulate( tblPltfCV_IL, hSql, sSelect, TBL_FillAll)
.head 7 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 7 -  Call picTabs.HideWindow( tblDisplay )
.head 2 +  Window Parameters
.head 3 -  String: sCaseYrP
.head 3 -  String: sCaseTyP
.head 3 -  String: sCaseNoP
.head 3 -  String: fType !!!! CIVIL, CRIM, TRUST
.head 2 +  Window Variables
.head 3 -  Number: nRow
.head 3 -  Number: nMaxRow
.head 3 -  String: sAddress2
.head 3 -  String: sAddress
.head 3 -  Number: nAttNo
.head 3 -  String: sCSZ
.head 3 -  String: sCaseFullLe
.head 3 -  ! String: sReturnRowid
.head 3 -  Number: nRptFlag
.head 3 -  String: sPrinterNameL
.head 2 +  Message Actions
.head 3 +  On SAM_CreateComplete
.head 4 -  Call IsBackup( )
.head 4 -  Call SalSetDefButton( pbPrint )
.head 4 +  If sCaseYrP = ''
.head 5 -  Call SalDisableWindow( rbDefendant )
.head 5 -  Call SalDisableWindow( rbDAttorney)
.head 5 -  Call SalDisableWindow( rbLit )
.head 5 -  Call SalDisableWindow( rbLitAtt)
.head 5 -  Set rbBondsman = TRUE
.head 5 -  Call SalSendMsg( rbBondsman, SAM_Click, 0,0)
.head 4 +  Else
.head 5 -  Call picTabs.BringToTop( 0, TRUE )
.head 5 +  If fType = 'CRIM'
.head 6 -  Call SalSetWindowText( dlgCrLabels, 'Criminal / Traffic Labels Processing - Case - ' || sCaseYrP||'-'|| sCaseTyP ||'-'|| sCaseNoP )
.head 5 +  Else If fType = 'CIVIL'
.head 6 -  Call SalSetWindowText( dlgCrLabels, 'Civil Labels Processing - Case - ' || sCaseYrP||'-'|| sCaseTyP ||'-'|| sCaseNoP )
.head 4 -  Set dfNumber= 1
.head 4 +  If sCourtConst = 'CMC'
.head 5 -  Call SalSetWindowText( pbPrint, 'Print Envelope' )
.head 4 +  If fType = 'CIVIL'
.head 5 -  Call picTabs.ShowWindow( rbLit )
.head 5 -  Call picTabs.ShowWindow( rbLitAtt )
.head 5 -  Call picTabs.ShowWindow( tblPltfCV_IL )
.head 5 -  Call picTabs.HideWindow( tblDisplay )
.head 4 +  Else If fType= 'CRIM'
.head 5 -  Call picTabs.HideWindow( rbLit )
.head 5 -  Call picTabs.HideWindow( rbLitAtt )
.head 5 -  Call picTabs.HideWindow( tblPltfCV_IL )
.head 5 -  Call picTabs.ShowWindow( tblDisplay )
.head 5 -  Call SalHideWindow( rbLit )
.head 5 -  Call SalHideWindow( rbLitAtt)
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlag=0
.head 4 -  ! Call SalReportSetPrinterSettings( SalNumberToWindowHandle(wParam), sPrinterNameL, RPT_Portrait, RPT_PaperLetter, NUMBER_Null, NUMBER_Null)
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 -  Set nRptFlag=nRptFlag+1
.head 4 +  If nRptFlag=1
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 1 +  Dialog Box: dlgCalcTime
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Date Calculator
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.738"
.head 4 -  Top: 1.938"
.head 4 -  Width:  8.029"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 3.677"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 4474
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.2"
.head 5 -  Top: 0.146"
.head 5 -  Width:  1.443"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.177"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Starting Date:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfStartDateCT
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.4"
.head 6 -  Top: 0.104"
.head 6 -  Width:  1.429"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ccBeg.SetFocusDate( MyValue )
.head 3 -  Background Text: bkgd4
.head 4 -  Resource Id: 4475
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 5.2"
.head 5 -  Top: 0.135"
.head 5 -  Width:  1.443"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.177"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Ending Date:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfEndDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 6.386"
.head 6 -  Top: 0.094"
.head 6 -  Width:  1.386"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Custom Control: ccBeg
.data CLASSPROPSSIZE
0000: 2A00
.enddata
.data CLASSPROPS
0000: 56543A43616C656E 646172001B000100 0000000100010001 0200000000010101
0020: 0001000000000000 0000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cCalendar
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  DLL Name:
.head 5 -  MS Windows Class Name:
.head 5 -  Style:  Class Default
.head 5 -  ExStyle:  Class Default
.head 5 -  Title: Pick Date
.head 5 -  Window Location and Size
.head 6 -  Left: 0.175"
.head 6 -  Top: 0.365"
.head 6 -  Width:  2.714"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 1.979"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Etched Border? Class Default
.head 5 -  Hollow? Class Default
.head 5 -  Vertical Scroll? Class Default
.head 5 -  Horizontal Scroll? Class Default
.head 5 -  Tab Stop? Class Default
.head 5 -  Tile To Parent? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  DLL Settings
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 +  Message Actions
.head 5 +  ! On SAM_CustControlCmd
.head 6 -  Call SetTitle( )
.head 6 +  Select Case (wParam)
.head 7 +  Case (CALN_DateChanged)
.head 8 -  Break 
.head 7 +  Case (CALN_DateFinished)
.head 8 -  Break 
.head 7 +  Case (CALN_Scroll)
.head 8 -  Break 
.head 5 +  On SAM_Click
.head 6 -  Call ccBeg.GetFocusDate(dtReturnDate)
.head 6 -  Set dfStartDateCT = dtReturnDate
.head 6 +  If dfNumDays > 0
.head 7 -  Call SalSendMsg( dfNumDays, SAM_Validate, 0,0)
.head 3 +  Custom Control: ccEnd
.data CLASSPROPSSIZE
0000: 2A00
.enddata
.data CLASSPROPS
0000: 56543A43616C656E 646172001B000100 0000000100010001 0200000000010101
0020: 0001000000000000 0000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cCalendar
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  DLL Name:
.head 5 -  MS Windows Class Name:
.head 5 -  Style:  Class Default
.head 5 -  ExStyle:  Class Default
.head 5 -  Title: Pick Date
.head 5 -  Window Location and Size
.head 6 -  Left: 5.114"
.head 6 -  Top: 0.365"
.head 6 -  Width:  2.714"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 1.979"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Etched Border? Class Default
.head 5 -  Hollow? Class Default
.head 5 -  Vertical Scroll? Class Default
.head 5 -  Horizontal Scroll? Class Default
.head 5 -  Tab Stop? Class Default
.head 5 -  Tile To Parent? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  DLL Settings
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 +  Message Actions
.head 5 +  ! On SAM_CustControlCmd
.head 6 -  Call SetTitle( )
.head 6 +  Select Case (wParam)
.head 7 +  Case (CALN_DateChanged)
.head 8 -  Break 
.head 7 +  Case (CALN_DateFinished)
.head 8 -  Break 
.head 7 +  Case (CALN_Scroll)
.head 8 -  Break 
.head 5 +  ! On SAM_Click
.head 6 -  Call ccEnd.GetFocusDate(dtReturnDate)
.head 6 -  Set dfEndDate = dtReturnDate
.head 3 +  Data Field: dfEndDateFormal
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.743"
.head 6 -  Top: 2.49"
.head 6 -  Width:  2.725"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: dddd, MMMM d, yyyy
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 4476
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 3.014"
.head 5 -  Top: 1.542"
.head 5 -  Width:  2.043"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.208"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Number of Days to Add:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfNumDays
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 3.557"
.head 6 -  Top: 1.833"
.head 6 -  Width:  0.8"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Center
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If MyValue != 0
.head 7 -  Call FindDate( )
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 3.4"
.head 5 -  Top: 3.208"
.head 5 -  Width:  1.463"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog(dlgCalcTime, 0)
.head 3 +  Data Field: dfHoliday
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.743"
.head 6 -  Top: 2.74"
.head 6 -  Width:  2.725"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.365"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: Uppercase
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 18
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Red
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Radio Button: rbJail
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Jail Date (off by 1 day)
.head 4 -  Window Location and Size
.head 5 -  Left: 2.971"
.head 5 -  Top: 0.823"
.head 5 -  Width:  2.014"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: 9
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: #a0fdff
.head 4 -  ToolTip: The Jail Date calculation will be one less day due to the way jail dates are calculated.
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If dfNumDays != NUMBER_Null
.head 7 -  Call FindDate(  )
.head 3 +  Radio Button: rbStandard
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Standard Date
.head 4 -  Window Location and Size
.head 5 -  Left: 2.971"
.head 5 -  Top: 1.135"
.head 5 -  Width:  1.514"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: 9
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: #a0fdff
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If dfNumDays != NUMBER_Null
.head 7 -  Call FindDate(  )
.head 3 +  Frame: frame1
.head 4 -  Resource Id: 54615
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.914"
.head 5 -  Top: 0.76"
.head 5 -  Width:  2.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.688"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 1
.head 4 -  Border Color: Default
.head 4 -  Background Color: #a0fdff
.head 4 -  Xaml:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Message Actions
.head 2 +  Functions
.head 3 +  Function: FindDate
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHoliday
.head 4 +  Actions
.head 5 -  Set dfEndDate = DATETIME_Null
.head 5 -  Set dfEndDateFormal = DATETIME_Null
.head 5 -  Set dfHoliday =''
.head 5 -  Set sHoliday = ''
.head 5 +  If rbStandard = TRUE
.head 6 -  Set dfEndDate = (dfStartDateCT + dfNumDays) 
.head 5 +  Else If rbJail= TRUE
.head 6 -  Set dfEndDate = (dfStartDateCT + dfNumDays)  - 1
.head 5 -  !
.head 5 -  Set dfEndDateFormal = dfEndDate
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	text
			      FROM		calendar
 			      INTO		:sHoliday
			      WHERE	:dfEndDate = holiday')
.head 5 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  If nReturn = FETCH_Ok
.head 6 -  Call SalMessageBeep( MB_IconAsterisk )
.head 6 -  Set dfHoliday = sHoliday
.head 5 +  Else If SalDateWeekday( dfEndDate ) = 0
.head 6 -  Call SalMessageBeep( MB_IconAsterisk )
.head 6 -  Set dfHoliday = 'WEEKEND DATE'
.head 5 +  Else If SalDateWeekday( dfEndDate ) = 1
.head 6 -  Call SalMessageBeep( MB_IconAsterisk )
.head 6 -  Set dfHoliday = 'WEEKEND DATE'
.head 5 -  Call ccEnd.SetFocusDate( dfEndDate )
.head 2 +  Window Parameters
.head 3 -  String: fType !!!  "NORMAL" - Normal calculation
		"JAIL" - Exclude 1 day used for jail calculations in court
.head 2 +  Window Variables
.head 3 -  String: sHoliday
.head 3 -  Date/Time: dtReturnDate
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalSetDefButton(pbOk)
.head 4 -  Call SalCenterWindow(dlgCalcTime)
.head 4 -  Set dfStartDateCT = SalDateCurrent( )
.head 4 -  Call SalSetFocus(dfNumDays)
.head 4 -  Call IsBackup( )
.head 4 +  If fType = 'NORMAL'
.head 5 -  Set rbStandard = TRUE
.head 4 +  Else
.head 5 -  Set rbJail = TRUE
.head 1 +  Dialog Box: dlgPrintCVLabels
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Mailing Label
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 0.625"
.head 4 -  Top: 0.625"
.head 4 -  Width:  4.738"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.938"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 30673
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.313"
.head 5 -  Top: 0.292"
.head 5 -  Width:  4.138"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.365"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Arial Rounded MT Bold
.head 4 -  Font Size: 16
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Printing Label, Please Wait.....
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 +  Functions
.head 3 +  Function: PrintLabels
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sLAddr2
.head 5 -  String: sLAddr
.head 5 -  String: sLCitySt
.head 5 -  String: sReportType
.head 4 +  Actions
.head 5 -  Set sLAddr2 = pAddr2
.head 5 -  Set sLAddr = pAddr
.head 5 -  Set sLCitySt= pCSZ
.head 5 +  If nCopies = NUMBER_Null
.head 6 -  Set nCopies = 1
.head 5 +  If sLAddr2 = ''
.head 6 -  Set sLAddr2 = sLAddr
.head 6 -  Set sLAddr = sLCitySt
.head 6 -  Set sLCitySt = ''
.head 5 -  Set nPrintErr = -1
.head 5 -  Set sReportType = 'Labelmmc.qrp'
.head 5 -  Set sReportBinds = 'pName, sLAddr2, sLAddr, sLCitySt, pCase'
.head 5 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM'
.head 5 -  Call SalReportPrint ( dlgPrintCVLabels, sReportType, sReportBinds, sReportInputs,
	nCopies, RPT_PrintAll| RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 5 +  If nPrintErr > 0
.head 6 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),
	MB_Ok )
.head 5 +  ! If bLabels = TRUE
.head 6 -  ! Set nFldLength = SalNumberToStr( dfCaseNo, 0, sCaseNum )
.head 6 +  ! While nFldLength<4
.head 7 -  Set sCaseNum = '0' || sCaseNum
.head 7 -  Set nFldLength =SalStrLength( sCaseNum )
.head 6 -  ! Set sCase=SalNumberToStrX( dfCaseYr, 0) || '-' || dfCaseType || '-' || sCaseNum
.head 6 -  ! 3/24/99  sjj  Test addr2 line for spaces to eliminate blank line
.head 5 +  ! Else
.head 6 -  Set sReportBinds = ':dfLine1, :dfLine2, :dfLine3, :dfLine4, :dfLabelCase'
.head 6 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM'
.head 2 +  Window Parameters
.head 3 -  String: pName
.head 3 -  String: pAddr
.head 3 -  String: pAddr2
.head 3 -  String: pCSZ
.head 3 -  String: pCase
.head 3 -  Number: nCopies
.head 2 +  Window Variables
.head 3 -  Number: nRptFlagL
.head 2 +  Message Actions
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlagL=0
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 -  Set nRptFlagL=nRptFlagL+1
.head 4 +  If nRptFlagL=1
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 3 +  On SAM_CreateComplete
.head 4 -  Call PrintLabels(  )
.head 4 -  Call SalEndDialog( dlgPrintCVLabels, 0)
.head 1 +  Dialog Box: dlgViewORC
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 0.088"
.head 4 -  Top: 0.063"
.head 4 -  Width:  12.625"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 6.479"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Custom Control: ccBrowser
.data CLASSPROPSSIZE
0000: 7300
.enddata
.data CLASSPROPS
0000: 5100570041005800 3A00630051007500 690063006B004800 54004D004C000000
0020: 00004F0001000000 0000000000000000 0000000000000000 0000000000000000
0040: 0000000000000000 0000000000000000 0000000000000000 0000000000000000
0060: 0000000000000000 0000000000000000 000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cQuickHTML
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  DLL Name:
.head 5 -  MS Windows Class Name:
.head 5 -  Style:  Class Default
.head 5 -  ExStyle:  Class Default
.head 5 -  Title:
.head 5 -  Window Location and Size
.head 6 -  Left: 0.0"
.head 6 -  Top: 0.0"
.head 6 -  Width:  12.55"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 6.417"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Yes
.head 5 -  Border? Class Default
.head 5 -  Etched Border? Class Default
.head 5 -  Hollow? Class Default
.head 5 -  Vertical Scroll? Class Default
.head 5 -  Horizontal Scroll? Class Default
.head 5 -  Tab Stop? Class Default
.head 5 -  Tile To Parent? Yes
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  DLL Settings
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_CustControlCmd
.head 6 -  Set nReturn = ccBrowser.GetStatus(  )
.head 6 +  If nReturn = STATUS_LoadComplete
.head 7 -  ! Call SalMessageBox( 'Done', '', 1)
.head 3 +  Pushbutton: pbExit
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgViewORC, 1)
.head 2 +  Functions
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sCheckCode
.head 5 -  Number: nChar
.head 5 -  Number: nOffset
.head 5 -  Boolean: bOk
.head 4 +  Actions
.head 5 -  Set sCheckCode = fCode
.head 5 -  Set bOk = SalStrFirstC( sCheckCode, nChar )
.head 5 +  While bOk
.head 6 +  If nChar > 64
.head 7 -  Set nOffset = SalStrScan( fCode, SalNumberToChar( nChar ) )
.head 7 -  Set bOk = FALSE
.head 6 +  Else
.head 7 -  Set bOk = SalStrFirstC( sCheckCode, nChar )
.head 5 +  If nOffset > 0
.head 6 -  Set sCheckCode = SalStrMidX( fCode, 0, nOffset )
.head 5 +  Else
.head 6 -  Set sCheckCode = fCode
.head 5 -  Set sURL = 'http://codes.ohio.gov/orc/'|| sCheckCode
.head 5 -  Call ccBrowser.PageLoad( sURL)
.head 2 +  Window Parameters
.head 3 -  String: fCode
.head 2 +  Window Variables
.head 3 -  String: sURL
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call FillScreen( )
