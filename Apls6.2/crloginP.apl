.head 0 +  Application Description: Criminal Software Integrated Library
3-20-2001 - WRP
   * Includes 	-Login Screen
		-Registry checking( for Public/Non-Public Access)
		-Classes for formating of (Caseyr, caseno, casety, dates)
		-Retains last user to login
		-Selects User information from database
		-Selects Court information from database
		-8-2-2001-WRP-Added "/" ability to cDate class, allows the backslash to be entered to display todays date.
		-9-6-2001-WRP-Added Holiday Date Class to check for holiday and weekends
.head 1 -  Outline Version - 4.0.50
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000A30000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF2C0000 002C0000002F0200 0066010000010000 0001000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0000000000
.enddata
.data DT_MAKERUNDLG
0000: 0B00001002000000 FFFEFF00FFFEFF33 43003A005C004300 65006E0074007500
0020: 720061005C004300 720069006D003300 320020002D002000 4F00520041004300
0040: 4C0045005C004300 720069006D004E00 650077002D004D00 6100730073005C00
0060: 430072004C006F00 670069006E002E00 650078006500FFFE FF3343003A005C00
0080: 430065006E007400 7500720061005C00 4300720069006D00 3300320020002D00
00A0: 20004F0052004100 43004C0045005C00 4300720069006D00 4E00650077002D00
00C0: 4D00610073007300 5C00430072004C00 6F00670069006E00 2E0064006C006C00
00E0: FFFEFF3343003A00 5C00430065006E00 7400750072006100 5C00430072006900
0100: 6D00330032002000 2D0020004F005200 410043004C004500 5C00430072006900
0120: 6D004E0065007700 2D004D0061007300 73005C0043007200 4C006F0067006900
0140: 6E002E0061007000 6300000001010100 64000000FFFEFF33 43003A005C004300
0160: 65006E0074007500 720061005C004300 720069006D003300 320020002D002000
0180: 4F00520041004300 4C0045005C004300 720069006D004E00 650077002D004D00
01A0: 6100730073005C00 430072004C006F00 670069006E002E00 720075006E00FFFE
01C0: FF3343003A005C00 430065006E007400 7500720061005C00 4300720069006D00
01E0: 3300320020002D00 20004F0052004100 43004C0045005C00 4300720069006D00
0200: 4E00650077002D00 4D00610073007300 5C00430072004C00 6F00670069006E00
0220: 2E0064006C006C00 FFFEFF3343003A00 5C00430065006E00 7400750072006100
0240: 5C00430072006900 6D00330032002000 2D0020004F005200 410043004C004500
0260: 5C00430072006900 6D004E0065007700 2D004D0061007300 73005C0043007200
0280: 4C006F0067006900 6E002E0061007000 6300000001010100 64000000FFFEFF33
02A0: 43003A005C004300 65006E0074007500 720061005C004300 720069006D003300
02C0: 320020002D002000 4F00520041004300 4C0045005C004300 720069006D004E00
02E0: 650077002D004D00 6100730073005C00 430072004C006F00 670069006E002E00
0300: 610070006400FFFE FF3343003A005C00 430065006E007400 7500720061005C00
0320: 4300720069006D00 3300320020002D00 20004F0052004100 43004C0045005C00
0340: 4300720069006D00 4E00650077002D00 4D00610073007300 5C00430072004C00
0360: 6F00670069006E00 2E0064006C006C00 FFFEFF3343003A00 5C00430065006E00
0380: 7400750072006100 5C00430072006900 6D00330032002000 2D0020004F005200
03A0: 410043004C004500 5C00430072006900 6D004E0065007700 2D004D0061007300
03C0: 73005C0043007200 4C006F0067006900 6E002E0061007000 6300000001010100
03E0: 64000000FFFEFF33 43003A005C004300 65006E0074007500 720061005C004300
0400: 720069006D003300 320020002D002000 4F00520041004300 4C0045005C004300
0420: 720069006D004E00 650077002D004D00 6100730073005C00 430072004C006F00
0440: 670069006E002E00 610070006C00FFFE FF3343003A005C00 430065006E007400
0460: 7500720061005C00 4300720069006D00 3300320020002D00 20004F0052004100
0480: 43004C0045005C00 4300720069006D00 4E00650077002D00 4D00610073007300
04A0: 5C00430072004C00 6F00670069006E00 2E0064006C006C00 FFFEFF3343003A00
04C0: 5C00430065006E00 7400750072006100 5C00430072006900 6D00330032002000
04E0: 2D0020004F005200 410043004C004500 5C00430072006900 6D004E0065007700
0500: 2D004D0061007300 73005C0043007200 4C006F0067006900 6E002E0061007000
0520: 6300000001010100 64000000FFFEFF00 FFFEFF00FFFEFF00 0000010101006400
0540: 0000FFFEFF00FFFE FF00FFFEFF000000 0101010064000000 FFFEFF00FFFEFF00
0560: FFFEFF0000000101 0100640000000000 000001FFFEFF00FF FEFF00FFFEFF00FF
0580: FEFF00FFFEFF00FF FEFF000000000000 000000FFFEFF00FF FEFF00FFFEFF0000
05A0: 0000000001000000 0100000001FFFEFF 0001000000000000 00FFFEFF00010000
05C0: 1000000000000000 0000000000020000 00
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040002001B00 0200000000000000 00003816A00F0500 1D00FFFF4D61696E
0020: 0020000100040000 0000000000F51E81 0F0000DF00FFFF56 61726961626C6573
0040: 0029000100040000 0000000000F51E81 0F00008600FFFF49 6E7465726E616C20
0060: 46756E6374696F6E 73001E0001000400 000000000000F51E 810F0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: 0.01"
.head 3 -  Top: 0.133"
.head 3 -  Width:  7.96"
.head 3 -  Height: 4.425"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C4389105B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 8.325"
.head 3 -  Top: 0.135"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 2 -  Assembly Symbol File:
.head 1 +  Libraries
.head 2 -  File Include: RegistryW.apl
.head 2 -  File Include: Datafields.Apl
.head 2 -  File Include: qcktabs.apl
.head 2 -  File Include: mtbl.apl
.head 2 -  File Include: mimg.apl
.head 2 -  File Include: vttblwin.apl
.head 2 -  File Include: vtfile.apl
.head 2 -  File Include: vtstr.apl
.head 2 -  File Include: vtdos.apl
.head 2 -  File Include: vtcomm.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Line Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 3 +  Tree Control
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Navigation Bar
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 -  Pane Separator
.head 3 +  Tab Bar
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Progress Bar
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MM-dd-yyyy
.head 2 +  External Functions
.head 2 +  External Assemblies
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 3 +  User
.head 4 -  Number: WM_NEXTDLGCTL = 0x0028
.head 4 -  Number: VKInsert = 0x2D
.head 4 -  Number: VKDelete = 0x2E
.head 4 -  Number: VK_Backslash = 220
.head 4 -  Number: WM_KEYUP = 0x0101
.head 4 -  Number: VK_RETURN=0x0D
.head 4 -  Number: VKForwardslash = 191
.head 4 -  Number: Key_Up = 0x26
.head 4 -  Number: Key_Down = 0x28
.head 4 -  String: TAB = '	'
.head 4 -  String: FROM_DUAL = ''
.head 4 -  String: CV_Program_Ver = 'CIVIL_APPLICATIONS_6.2' 
.head 4 -  String: CR_Program_Ver = 'CRIMINAL_APPLICATIONS_6.2' 
.head 4 -  String: CV_REPORT_Path = '\\\\192.168.38.5\\Courts\\Civil\\Qrps\\'
.head 4 -  String: CR_REPORT_Path = '\\\\192.168.38.5\\Courts\\Criminal\\Qrps\\'
.head 4 -  String: CivilPath = "\\\\192.168.38.5\\Courts\\Civil\\Civil6.2"
.head 4 -  String: CrimPath = "\\\\192.168.38.5\\Courts\\Criminal\\Criminal6.2"
.head 4 -  String: LocalPath = "c:\\"
.head 4 -  String: ImagePath = "\\\\192.168.38.5\\Images\\"
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.data RESOURCE 0 0 1 3757594391
0000: 5E0000004A000000 0000000000000000 02000003000000DE 0500001019000000
0020: 010210200F0F0000 00F107004A010B00 04000000A3F70500 04C5000202102B00
0040: F14B01DE0B0068FB 050019000D017F10 00000A00F161024A 00360400
.enddata
.head 3 -  Boolean: bLogin
.head 3 -  Boolean: bfrmLabels
.head 3 -  Boolean: bFormDirty
.head 3 -  Boolean: bPrintbyUser
.head 3 -  Boolean: bDriveMapped
.head 3 -  Boolean: bCertifiedProcess
.head 3 -  Boolean: bPrintCertOneAtATime
.head 3 -  Number: nResult
.head 3 -  Number: nDefaultPrinter1
.head 3 -  String: sReprintBarcode
.head 3 -  Date/Time: dTimestamped
.head 3 -  ! !!!!!!
.head 3 -  String: sHoliday
.head 3 -  String: GlobCaseTy
.head 3 -  String: GlobCaseYr
.head 3 -  String: GlobCaseNo
.head 3 -  Number: nGlobRatnum
.head 3 -  ! !!!!!! <<For Certified Mail Bar Code >>
.head 3 -  String: sCertNo
.head 3 -  String: sCertifiedQrp
.head 3 -  String: sBarcodeEncrypted
.head 3 -  ! !!!!!! <<Class instance used to acces the Registry >>
.head 3 -  FunctionalVar: REG
.head 4 -  Class: cBTRegistryW
.head 3 -  ! !!!!!  <<Handle & String  to open Last User Login File >>
.head 3 -  File Handle: hFLogin
.head 3 -  Sql Handle: hSql
.head 3 -  String: sLogIn
.head 3 -  String: sCourtConst
.head 3 -  ! !!!!!  <<Print Report Functions>>
.head 3 -  String: sDialog
.head 3 -  Boolean: bPrinting
.head 3 -  Boolean: bDebugView
.head 3 -  Window Handle: hDialog
.head 3 -  ! !!!!!  <<CourtInfo Data Fields >>
.head 3 -  String: sCourt
.head 3 -  String: sCourtCity
.head 3 -  String: sCourtClerk
.head 3 -  String: sCourtCounty
.head 3 -  String: sCourtAddr
.head 3 -  String: sCourtAddr2
.head 3 -  String: sCourtCityState
.head 3 -  String: sCourtPhone
.head 3 -  String: sCourtPhone2
.head 3 -  String: sCourtPhone3
.head 3 -  String: sCourtCode
.head 3 -  String: sCourtORI
.head 3 -  String: sCourtWarrantFlag
.head 3 -  String: sCourtPrelimFlag
.head 3 -  String: sCourtPJudge
.head 3 -  Number: nCourtPostage
.head 3 -  Date/Time: dCostsDate
.head 3 -  Number: nVBDFineInc
.head 3 -  Number: nVBDFineIncI
.head 3 -  ! !!!!!  <<User Table Data Fields >>
.head 3 -  String: sUClerk
.head 3 -  String: sUDivision
.head 3 -  String: sUDrawer
.head 3 -  String: sUDepartment
.head 3 -  String: sUFullName
.head 3 -  String: sUWPhone
.head 3 -  String: sUWPhone_Ext
.head 3 -  String: sUJudge
.head 3 -  Number: nUserId
.head 3 -  Number: nULevel
.head 3 -  Number: nUCashier
.head 3 -  Number: nURecCopies
.head 3 -  Boolean: bChange
.head 3 -  Number: nError
.head 3 -  String: sAdminLock
.head 3 -  String: sAdminComment
.head 3 -  ! !!!!!! <<User Data Fields from Civil>>
.head 3 -  String: sCourtDirections
.head 3 -  String: sCourtCBailiff
.head 3 -  Number: nCV_CF
.head 3 -  Number: nCV_Replevin
.head 3 -  Number: nCV_LegalAid
.head 3 -  Number: nCV_TechFee
.head 3 -  Number: nCV_Garn
.head 3 -  Number: nCV_GFee
.head 3 -  Number: nCVH_CF
.head 3 -  Number: nCVH_LegalAid
.head 3 -  Number: nCVH_TechFee
.head 3 -  Number: nCVI_CF
.head 3 -  Number: nCVI_LegalAid
.head 3 -  Number: nCVI_TechFee
.head 3 -  Number: nCertifiedCode1
.head 3 -  Number: nCertifiedCode2
.head 3 -  Number: nCertifiedCode3
.head 3 -  Number: nCertifiedCode4
.head 3 -  Number: nCertifiedCode5
.head 3 -  Number: nMarriage
.head 3 -  ! !!!!!! <<The 'where clause' depending on where you are>>
.head 3 -  Number: nX
.head 3 -  Number: nY
.head 3 -  String: sSelectWhere
.head 3 -  Number: nReturn
.head 3 -  ! !!!!!! <<LABEL VARIABLES> >>
.head 3 -  String: sReturnRowid
.head 3 -  ! !!!!! <<REPORT VARIABLES>>
.head 3 -  String: sReport
.head 3 -  String: sReportBinds
.head 3 -  String: sReportInputs
.head 3 -  Number: nPrintErr
.head 3 -  String: sDevice
.head 3 -  String: sDriver
.head 3 -  String: sPort
.head 3 -  ! ! !! << EXPORT VAR >>
.head 3 -  String: fGlobTitle
.head 3 -  String: fGlobFileName
.head 3 -  String: sCTABLE_MENU_TYPE
.head 3 -  Window Handle: hWndTable
.head 3 -  Boolean: bSortTblCol
.head 3 -  String: sJPhoto
.head 3 -  String: sJPhotoDate
.head 3 -  Date/Time: dJPhotoDate
.head 3 -  ! !
.head 3 -  Boolean: bReceiptDefault
.head 3 -  String: sDefaultDevice
.head 3 -  String: sDefaultDriver
.head 3 -  String: sDefaultPort
.head 3 -  String: sAppsPath
.head 3 -  Date/Time: dTimestampReprint
.head 3 -  Number: nLastRow
.head 3 -  String: sReprintArticle
.head 3 -  String: sFilePath
.head 2 +  Internal Functions
.head 3 +  Function: Mod10
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sMod10
.head 4 +  Parameters
.head 5 -  String: sDataToEncode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sMod10
.head 5 -  Number: nMod10
.head 5 -  Number: nTotalVal
.head 5 -  Number: nTotalVal1
.head 5 -  Number: n
.head 5 -  Number: s
.head 5 -  String: sDigit
.head 5 -  Number: nDigit
.head 4 +  Actions
.head 5 -  Set nTotalVal = 0
.head 5 -  Set n = 0
.head 5 +  Loop
.head 6 +  If s > 9
.head 7 -  Break
.head 6 -  Set sDigit = SalStrMidX( sDataToEncode, n , 1 )
.head 6 -  Set nDigit = SalStrToNumber( sDigit )
.head 6 -  Set nTotalVal = nTotalVal + nDigit
.head 6 -  Set n = n + 2
.head 6 -  Set s = s + 1
.head 5 -  Set nTotalVal = nTotalVal * 3
.head 5 -  Set n = 1
.head 5 -  Set s = 0
.head 5 -  Set nTotalVal1 = 0
.head 5 +  Loop
.head 6 +  If s > 8
.head 7 -  Break
.head 6 -  Set sDigit = SalStrMidX( sDataToEncode, n, 1 )
.head 6 -  Set nDigit = SalStrToNumber( sDigit )
.head 6 -  Set nTotalVal1 = nTotalVal1 + nDigit
.head 6 -  Set n = n + 2
.head 6 -  Set s = s + 1
.head 5 -  Set nTotalVal = nTotalVal + nTotalVal1
.head 5 -  Set nMod10 = SalNumberMod( nTotalVal, 10 )
.head 5 +  If nMod10 != 0
.head 6 -  Set nMod10 = 10 - nMod10
.head 5 +  Else
.head 6 -  Set nMod10 = 0
.head 5 -  Call SalNumberToStr( nMod10, 0, sMod10 )
.head 5 -  Return sMod10
.head 3 +  Function: Mod10_Length22
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sMod10
.head 4 +  Parameters
.head 5 -  String: sDataToEncode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sMod10
.head 5 -  Number: nMod10
.head 5 -  Number: nTotalVal
.head 5 -  Number: nTotalVal1
.head 5 -  Number: n
.head 5 -  Number: s
.head 5 -  String: sDigit
.head 5 -  Number: nDigit
.head 4 +  Actions
.head 5 -  Set nTotalVal = 0
.head 5 -  Set n = 0
.head 5 +  Loop
.head 6 +  If s > 10
.head 7 -  Break
.head 6 -  Set sDigit = SalStrMidX( sDataToEncode, n , 1 )
.head 6 -  Set nDigit = SalStrToNumber( sDigit )
.head 6 -  Set nTotalVal = nTotalVal + nDigit
.head 6 -  Set n = n + 2
.head 6 -  Set s = s + 1
.head 5 -  Set nTotalVal = nTotalVal * 3
.head 5 -  Set n = 1
.head 5 -  Set s = 0
.head 5 -  Set nTotalVal1 = 0
.head 5 +  Loop
.head 6 +  If s > 9
.head 7 -  Break
.head 6 -  Set sDigit = SalStrMidX( sDataToEncode, n, 1 )
.head 6 -  Set nDigit = SalStrToNumber( sDigit )
.head 6 -  Set nTotalVal1 = nTotalVal1 + nDigit
.head 6 -  Set n = n + 2
.head 6 -  Set s = s + 1
.head 5 -  Set nTotalVal = nTotalVal + nTotalVal1
.head 5 -  Set nMod10 = SalNumberMod( nTotalVal, 10 )
.head 5 +  If nMod10 != 0
.head 6 -  Set nMod10 = 10 - nMod10
.head 5 +  Else
.head 6 -  Set nMod10 = 0
.head 5 -  Call SalNumberToStr( nMod10, 0, sMod10 )
.head 5 -  Return sMod10
.head 3 +  Function: Code128c
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sDataToEncode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: DataToPrint
.head 5 -  String: sStartCode
.head 5 -  String: sStopCode
.head 5 -  Number: WeightedTotal
.head 5 -  Number: WeightValue
.head 5 -  Number: i
.head 5 -  String: sCurrentValue
.head 5 -  Number: nCurrentValue
.head 5 -  String: Data1
.head 5 -  Number: CheckDigitValue
.head 5 -  Number: n128_CheckDigit
.head 5 -  String: s128_CheckDigit
.head 5 -  String: Code128c
.head 4 +  Actions
.head 5 -  Set DataToPrint = ''
.head 5 -  Set sDataToEncode = SalStrTrimX( sDataToEncode )
.head 5 -  Set sStartCode = SalNumberToChar( 205 )
.head 5 -  Set sStopCode = SalNumberToChar( 206 )
.head 5 -  Set WeightedTotal = 105
.head 5 -  Set WeightValue = 1
.head 5 -  Set i = 0
.head 5 +  Loop
.head 6 +  If i > 18
.head 7 -  Break
.head 6 -  Set sCurrentValue = SalStrMidX( sDataToEncode,i, 2 )
.head 6 -  Set nCurrentValue = SalStrToNumber(sCurrentValue )
.head 6 +  If nCurrentValue < 95 and nCurrentValue > 0
.head 7 -  Set Data1 = SalNumberToChar(nCurrentValue + 32 )
.head 7 -  Set DataToPrint = DataToPrint || Data1
.head 6 +  Else If nCurrentValue > 94
.head 7 -  Set Data1 = SalNumberToChar(nCurrentValue + 100 )
.head 7 -  Set DataToPrint = DataToPrint || Data1
.head 6 +  Else If nCurrentValue = 0
.head 7 -  Set Data1 = SalNumberToChar( 194 )
.head 7 -  Set DataToPrint = DataToPrint || Data1
.head 6 -  Set nCurrentValue = nCurrentValue * WeightValue
.head 6 -  Set WeightedTotal = WeightedTotal + nCurrentValue
.head 6 -  Set WeightValue = WeightValue + 1
.head 6 -  Set i = i + 2
.head 5 -  Set CheckDigitValue = SalNumberMod( WeightedTotal, 103)
.head 5 +  If ( CheckDigitValue < 95 ) and ( CheckDigitValue > 0 )
.head 6 -  Set n128_CheckDigit = CheckDigitValue + 32
.head 5 +  Else If CheckDigitValue > 94
.head 6 -  Set n128_CheckDigit = CheckDigitValue + 100
.head 5 +  Else If CheckDigitValue = 0
.head 6 -  Set n128_CheckDigit = 194
.head 5 -  Set s128_CheckDigit = SalNumberToChar( n128_CheckDigit)
.head 5 -  Set Code128c = sStartCode || DataToPrint ||s128_CheckDigit || sStopCode || ' '
.head 5 -  Return Code128c
.head 3 +  ! Function: TrackCertified
.head 4 -  Description: Adds the Requested Data to the Docket File
.head 4 -  Returns 
.head 4 +  Parameters 
.head 5 -  Number: nCaseYr
.head 5 -  String: sCaseTy
.head 5 -  Number: nCaseNo
.head 5 -  Date/Time: dDocketDate
.head 5 -  String: sCode5
.head 5 -  Number: nSeq
.head 5 -  String: sArticle1_A1
.head 5 -  String: sArticle1_A2
.head 5 -  String: sArticle1_A3
.head 5 -  String: sArticle1_A4
.head 5 -  String: sArticle1_A5
.head 5 -  String: sAction
.head 5 -  String: sAddressLine1
.head 5 -  String: sAddressLine2
.head 5 -  String: sAddressLine3
.head 5 -  String: sAddressLine4
.head 5 -  String: sPrinted
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: sArticleNo
.head 5 -  String: sCertStatus
.head 5 -  Number: nFetchCertified
.head 5 -  Number: nCertifiedCount
.head 4 +  Actions 
.head 5 +  If sArticle1_A1 = 'Assign'
.head 6 -  Set sArticleNo = NextCertifiedNo(  )
.head 5 +  Else If SalStrLength( sArticle1_A1 ) = 20
.head 6 -  Set sArticleNo = sArticle1_A1
.head 5 +  Else 
.head 6 -  Set sArticleNo = sArticle1_A1 || sArticle1_A2 || sArticle1_A3 || sArticle1_A4 || sArticle1_A5
.head 5 +  If SalStrLength( sArticleNo ) < 17
.head 6 -  Return FALSE
.head 5 +  If SalStrLength( sArticleNo ) < 20
.head 6 -  Call SalMessageBox( 'Possible Article Number Error

Please verify that article number ' || sArticleNo || ' is correct', 'Article Number Error', MB_YesNo | MB_IconQuestion )
.head 6 -  Return FALSE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select code5
	from Certified into :sCertStatus
	where ArticleNo=:sArticleNo' )
.head 5 +  If Not SqlFetchNext( hSql, nFetchCertified )
.head 6 +  If sPrinted = 'Electronic'
.head 7 -  Set sPrinted = STRING_Null
.head 6 +  Else If bPrintCertOneAtATime or sPrinted = 'GreenCard'
.head 7 -  Set sPrinted = '1'
.head 6 +  Else If sPrinted = 'Y'
.head 7 -  Set sPrinted = STRING_Null
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Insert into Certified
	(caseyr, casety, caseno, ddate, dnum, code5, ArticleNo, 
		AddressLine1, AddressLine2, AddressLine3, AddressLine4, print ) VALUES
	(:nCaseYr, :sCaseTy, :nCaseNo, :dDocketDate, :nSeq, :sCode5, :sArticleNo, 
		:sAddressLine1, :sAddressLine2, :sAddressLine3, :sAddressLine4, :sPrinted)' )
.head 6 -  Call SqlCommit ( hSql )
.head 6 +  If sPrinted = '1'
.head 7 -  Call SalModalDialog( dlgCertifiedPrint, hWndNULL, sArticleNo, nCaseYr, sCaseTy, nCaseNo, sAddressLine1, sAddressLine2, sAddressLine3, sAddressLine4, 1 )
.head 6 +  Else 
.head 7 -  Call SqlPrepareAndExecute( hSql, 'Select count(*)
	from certified m into :nCertifiedCount
	where m.ddate >= sysdate - 5 and m.print is null ' )
.head 7 -  Call SqlFetchNext( hSql, nFetchCertified )
.head 7 +  If nCertifiedCount > 3
.head 8 -  Call SalCreateWindow( frmCertifiedProcess, hWndNULL, 'Y' )
.head 5 +  Else If sCertStatus = 'DEL'
.head 6 -  Call SqlPrepareAndExecute( hSql, "Update Certified
	set ddate=:dDocketDate, dnum=:nSeq, code5=:sCode5, AddressLine1=:sAddressLine1,
		AddressLine2=:sAddressLine2, AddressLine3=:sAddressLine3, AddressLine4=:sAddressLine4 
	where ArticleNo = :sArticleNo" )
.head 6 -  Call SqlCommit ( hSql )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select status
	from CertifiedStatus into :sCertStatus
	where ArticleNo=:sArticleNo and statusdate = :dDocketDate
	order by statusdate' )
.head 5 +  If SqlFetchNext( hSql, nFetchCertified )
.head 6 +  If sCertStatus = 'Deleted'
.head 7 -  Call SqlPrepareAndExecute( hSql, "Insert into CertifiedStatus
	(ArticleNo, statusdate, status ) values
	(:sArticleNo, sysdate, :sAction)" )
.head 6 +  Else 
.head 7 -  Call SqlPrepareAndExecute( hSql, "Update CertifiedStatus
	set status = :sAction, username=user
	where ArticleNo = :sArticleNo and statusdate = :dDocketDate" )
.head 5 +  Else 
.head 6 -  Call SqlPrepareAndExecute( hSql, "Insert into CertifiedStatus
	(ArticleNo, statusdate, status ) VALUES
	(:sArticleNo, :dDocketDate, :sAction)" )
.head 5 -  Call SqlCommit ( hSql )
.head 3 +  Function: TrackCertified
.head 4 -  Description: Adds the Requested Data to the Docket File
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Number: nCaseYr
.head 5 -  String: sCaseTy
.head 5 -  Number: nCaseNo
.head 5 -  Date/Time: dDocketDate
.head 5 -  String: sCode5
.head 5 -  Number: nSeq
.head 5 -  String: sArticle1_A1
.head 5 -  String: sArticle1_A2
.head 5 -  String: sArticle1_A3
.head 5 -  String: sArticle1_A4
.head 5 -  String: sArticle1_A5
.head 5 -  String: sAction
.head 5 -  String: sAddressLine1
.head 5 -  String: sAddressLine2
.head 5 -  String: sAddressLine3
.head 5 -  String: sAddressLine4
.head 5 -  String: sPrinted
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseNo
.head 5 -  String: sArticleNo
.head 5 -  String: sCertStatus
.head 5 -  Number: nFetchCertified
.head 5 -  Number: nCertifiedCount
.head 4 +  Actions
.head 5 +  If sArticle1_A1 = 'Assign'
.head 6 -  Set sArticleNo = NextCertifiedNo(  )
.head 5 +  Else If SalStrLength( sArticle1_A1 ) = 20
.head 6 -  Set sArticleNo = sArticle1_A1
.head 5 +  Else
.head 6 -  Set sArticleNo = sArticle1_A1 || sArticle1_A2 || sArticle1_A3 || sArticle1_A4 || sArticle1_A5
.head 5 +  If SalStrLength( sArticleNo ) < 17
.head 6 -  Return FALSE
.head 5 +  If SalStrLength( sArticleNo ) < 20
.head 6 -  Call SalMessageBox( 'Possible Article Number Error

Please verify that article number ' || sArticleNo || ' is correct', 'Article Number Error', MB_YesNo | MB_IconQuestion )
.head 6 -  Return FALSE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select code5
	     from crim.Certified
	     where ArticleNo=:sArticleNo
          Union
	Select code5
	     from crim.CR_Certified
	     where ArticleNo=:sArticleNo
	into :sCertStatus' )
.head 5 +  If SalStrLeftX( sCaseTy, 2 ) = 'CV'
.head 6 +  If Not SqlFetchNext( hSql, nFetchCertified )
.head 7 +  If sPrinted = 'Electronic'
.head 8 -  Set sPrinted = STRING_Null
.head 7 +  Else If bPrintCertOneAtATime or sPrinted = 'GreenCard'
.head 8 -  Set sPrinted = '1'
.head 7 +  Else If sPrinted = 'Y'
.head 8 -  Set sPrinted = STRING_Null
.head 7 -  Call SqlPrepareAndExecute( hSql, 'insert into crim.Certified
	(caseyr, casety, caseno, ddate, dnum, code5, ArticleNo,
		AddressLine1, AddressLine2, AddressLine3, AddressLine4, print ) VALUES
	(:nCaseYr, :sCaseYr, :nCaseNo, :dDocketDate, :nSeq, :sCode5, :sArticleNo,
		:sAddressLine1, :sAddressLine2, :sAddressLine3, :sAddressLine4, :sPrinted)' )
.head 7 -  Call SqlCommit ( hSql )
.head 7 +  If sPrinted = '1'
.head 8 -  Call SalModalDialog( dlgCertifiedPrint, hWndNULL, sArticleNo, nCaseYr, sCaseTy, nCaseNo, sAddressLine1, sAddressLine2, sAddressLine3, sAddressLine4, 1 )
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute( hSql, 'Select count(*)
	from crim.certified m into :nCertifiedCount
	where m.ddate >= sysdate - 5 and m.print is null ' )
.head 8 -  Call SqlFetchNext( hSql, nFetchCertified )
.head 8 +  If nCertifiedCount > 3
.head 9 -  Call SalCreateWindow( frmCertifiedProcess, hWndNULL, 'Y' )
.head 6 +  Else If sCertStatus = 'DEL'
.head 7 -  Call SqlPrepareAndExecute( hSql, "Update crim.Certified
	set ddate=:dDocketDate, dnum=:nSeq, code5=:sCode5, AddressLine1=:sAddressLine1,
		AddressLine2=:sAddressLine2, AddressLine3=:sAddressLine3, AddressLine4=:sAddressLine4
	where ArticleNo = :sArticleNo" )
.head 7 -  Call SqlCommit ( hSql )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select status
	from crim.CertifiedStatus into :sCertStatus
	where ArticleNo=:sArticleNo and statusdate = :dDocketDate
	order by statusdate' )
.head 6 +  If SqlFetchNext( hSql, nFetchCertified )
.head 7 +  If sCertStatus = 'Deleted'
.head 8 -  Call SqlPrepareAndExecute( hSql, "insert into crim.CertifiedStatus
	(ArticleNo, statusdate, status ) values
	(:sArticleNo, sysdate, :sAction)" )
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute( hSql, "Update crim.CertifiedStatus
	set status = :sAction
	where ArticleNo = :sArticleNo and statusdate = :dDocketDate" )
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute( hSql, "insert into crim.CertifiedStatus
	(ArticleNo, statusdate, status ) VALUES
	(:sArticleNo, :dDocketDate, :sAction)" )
.head 5 +  Else
.head 6 -  Set sCaseYr = SalFmtFormatNumber( nCaseYr, '0000' )
.head 6 -  Set sCaseNo = SalFmtFormatNumber( nCaseNo, '00000' )
.head 6 +  If Not SqlFetchNext( hSql, nFetchCertified )
.head 7 +  If sPrinted = 'Electronic'
.head 8 -  Set sPrinted = STRING_Null
.head 7 +  Else If sPrinted = 'Y'
.head 8 -  Set sPrinted = STRING_Null
.head 7 -  Call SqlPrepareAndExecute( hSql, 'insert into crim.cr_certified
	(caseyr, casety, caseno, ddate, dnum, code5, ArticleNo,
		AddressLine1, AddressLine2, AddressLine3, AddressLine4, print ) VALUES
	(:sCaseYr, :sCaseTy, :sCaseNo, :dDocketDate, :nSeq, :sCode5, :sArticleNo,
		:sAddressLine1, :sAddressLine2, :sAddressLine3, :sAddressLine4, :sPrinted)' )
.head 7 -  Call SqlCommit ( hSql )
.head 7 -  Call SqlPrepareAndExecute( hSql, 'Select count(*)
	from crim.cr_certified m into :nCertifiedCount
	where m.ddate >= sysdate - 5 and m.print is null ' )
.head 7 -  Call SqlFetchNext( hSql, nFetchCertified )
.head 7 +  If nCertifiedCount > 3
.head 8 -  Call SalCreateWindow( frmCRCertified, hWndNULL, 'Y' )
.head 6 +  Else If sCertStatus = 'DEL'
.head 7 -  Call SqlPrepareAndExecute( hSql, "Update crim.cr_certified
	set ddate=:dDocketDate, dnum=:nSeq, code5=:sCode5, AddressLine1=:sAddressLine1,
		AddressLine2=:sAddressLine2, AddressLine3=:sAddressLine3, AddressLine4=:sAddressLine4
	where ArticleNo = :sArticleNo" )
.head 7 -  Call SqlCommit ( hSql )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select status
	from crim.cr_certifiedStatus into :sCertStatus
	where ArticleNo=:sArticleNo and statusdate = :dDocketDate
	order by statusdate' )
.head 6 +  If SqlFetchNext( hSql, nFetchCertified )
.head 7 +  If sCertStatus = 'Deleted'
.head 8 -  Call SqlPrepareAndExecute( hSql, "insert into crim.cr_certifiedStatus
	(ArticleNo, statusdate, status ) values
	(:sArticleNo, sysdate, :sAction)" )
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute( hSql, "Update crim.cr_certifiedStatus
	set status = :sAction
	where ArticleNo = :sArticleNo and statusdate = :dDocketDate" )
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute( hSql, "insert into crim.cr_certifiedStatus
	(ArticleNo, statusdate, status ) VALUES
	(:sArticleNo, :dDocketDate, :sAction)" )
.head 5 -  Call SqlCommit ( hSql )
.head 3 +  Function: NextCertifiedNo
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCertifiedNumb
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Update crim.crim.cr_rec_control SET certifiedno = certifiedno+1' )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select crim.certifiedno from cr_rec_control into :nCertifiedNumb' )
.head 5 -  Call SqlFetchNext( hSql, nReturn )
.head 5 -  Call SqlCommit( hSql )
.head 5 +  If nReturn != FETCH_Ok
.head 6 -  Call SalMessageBox( 'Error Selecting next certified number - Contact Programmer', 'Certified Select Error',  MB_Ok )
.head 6 -  Set nCertifiedNumb = -1
.head 5 -  ! Return nCertifiedNumb
.head 5 +  If nCertifiedNumb >0
.head 6 -  Set sCertNo = '71176849750' || SalStrRightX( '000000000' || SalNumberToStrX( nCertifiedNumb, 0 ), 8 )
.head 6 -  Set sCertNo = sCertNo || Mod10( sCertNo )
.head 6 -  Set sBarcodeEncrypted = Code128c(sCertNo)
.head 6 -  Return sCertNo
.head 3 +  Function: GetUserInfo
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Receive String: SqlUser
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 -  ! Set sSelectWhere = ReadRegistrySelect(  )
.head 5 -  Set sSelectWhere = "username = user"
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	userid, seclevel, division, drawer, cashier, 
		sname, name, rec_copies, wphone, wphone_ext, department, Judge, certprintbyuser, DefaultPrinter1 
	FROM crim.users  INTO  :nUserId, :nULevel, :sUDivision, :sUDrawer, :nUCashier, 
		:sUClerk, :sUFullName, :nURecCopies, :sUWPhone, :sUWPhone_Ext, :sUDepartment, :sUJudge, :bPrintbyUser, :nDefaultPrinter1 
	WHERE '||sSelectWhere)
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlCommit( hSql )
.head 5 +  If nFetch = FETCH_Ok
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: GetCourtInfo
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT Title, Court, Clerk, County, Address, Address2, CityState, Phone, Phone2, Phone3, Code, 
		VBDIncDate, VBDIncFine, VBDIncFineI, CourtConst, ori, warrant_letter, prelim_notice, postage, 
		CV_CF, CV_Replevin, CV_LegalAid, CV_TechFee, CV_Garn, CV_GFee, CVH_CF, CVH_LegalAid, 
		CVH_TechFee, CVI_CF, CVI_LegalAid, CVI_TechFee, Marriage, Directions, CBailiff, PJudge, 
		CertifiedCode1, CertifiedCode2, CertifiedCode3, CertifiedCode4, CertifiedCode5 
	  FROM	crim.CourtInfo  INTO  :sCourt, :sCourtCity, :sCourtClerk, :sCourtCounty, :sCourtAddr, :sCourtAddr2, :sCourtCityState, :sCourtPhone,:sCourtPhone2,:sCourtPhone3, :sCourtCode, 
		:dCostsDate, :nVBDFineInc, :nVBDFineIncI, :sCourtConst, :sCourtORI, :sCourtWarrantFlag, :sCourtPrelimFlag, :nCourtPostage, 
		:nCV_CF, :nCV_Replevin, :nCV_LegalAid, :nCV_TechFee, :nCV_Garn, :nCV_GFee, :nCVH_CF, :nCVH_LegalAid, 
		:nCVH_TechFee, :nCVI_CF, :nCVI_LegalAid, :nCVI_TechFee, :nMarriage, :sCourtDirections, :sCourtCBailiff, :sCourtPJudge, 
		:nCertifiedCode1, :nCertifiedCode2, :nCertifiedCode3, :nCertifiedCode4, :nCertifiedCode5' )
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlCommit( hSql )
.head 5 +  If sCourtConst != 'CMC' and sCourtConst != 'MMC'
.head 6 -  Call SalMessageBox('Error reading Court Title from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 6 -  Call SalQuit( )
.head 5 -  Set sCourtCity = SalStrProperX( sCourtCity )
.head 3 +  Function: GetCourtInfoFlag
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sFlagName
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 5 -  String: sFlagSet
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, "Select " || sFlagName || " 
	  from crim.CourtInfo  into  :sFlagSet" )
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Return sFlagSet
.head 3 +  ! Function: GetJailPhoto
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: sJPhotoDate
.head 4 +  Parameters 
.head 5 -  String: sJSSNo
.head 5 -  Receive String: sJPhoto
.head 5 -  Receive String: sJPhotoDate
.head 5 -  Receive Date/Time: dJPhotoDate
.head 5 -  Receive Window Handle: hWndPicture
.head 4 +  Static Variables 
.head 5 -  Boolean: bPhotoDirSet
.head 5 -  String: sJPhotoDirectory
.head 4 +  Local variables 
.head 5 -  String: sJPhotoIFile
.head 5 -  String: sJPhotoFlag
.head 5 -  String: sJPhotoName
.head 5 -  Number: nSqlReturn
.head 4 +  Actions 
.head 5 +  If Not bPhotoDirSet
.head 6 -  Set bPhotoDirSet = TRUE
.head 6 +  If VisFileGetSize( '\\\\City_hall\\Ch2_data\\courts\\shared\\Jail_Images\\Front\\s\\00\\0000000000.jpg' ) > 0
.head 7 -  Set sJPhotoDirectory = '\\\\City_hall\\Ch2_data\\courts\\shared\\Jail_Images\\Front\\s\\'
.head 7 -  ! Else If VisFileGetSize( 'I:\\Images\\Jail_Images\\Front\\s\\0000000000.jpg' )  > 0
.head 6 +  Else 
.head 7 -  Set sJPhotoDirectory = '\\\\CMCFS1\\Imaging\\Jail_Images\\Front\\s\\'
.head 5 -  Set sJPhotoFlag = sJPhoto
.head 5 +  If SalStrLength( sJSSNo ) = 11
.head 6 -  Set sJSSNo = SalStrMidX( sJSSNo, 0, 3 ) || SalStrMidX( sJSSNo, 4, 2 ) || SalStrMidX( sJSSNo, 7, 4 )
.head 5 +  If SalStrLength( sJSSNo ) = 9
.head 6 -  Call SqlPrepareAndExecute( hSql, "Select p.idate, p.ifile
	from Inmate I, image p into :dJPhotoDate, :sJPhotoIFile
	where i.id=p.id and i.ssn=:sJSSNo and p.itype like 'F%'
	order by p.idate desc, ifile desc" )
.head 6 +  If SqlFetchNext( hSql, nSqlReturn )
.head 7 +  If sJPhotoFlag = 'Name Only'
.head 8 -  Set sJPhoto = sJPhotoIFile
.head 8 -  Set sJPhotoDate = 'Photo Taken: ' || SalFmtFormatDateTime( dJPhotoDate, 'MM-dd-yyyy')
.head 8 -  Return sJPhotoDate
.head 7 -  Set sJPhotoName = sJPhotoDirectory || SalStrRightX( sJPhotoIFile, 2 ) || '\\'
.head 7 -  Set sJPhotoName = sJPhotoName || sJPhotoIFile || '.jpg'
.head 7 +  If SalPicSetFile ( hWndPicture, sJPhotoName )
.head 8 +  If SalPicGetString ( hWndPicture, PIC_FormatObject, sJPhoto ) > 0
.head 9 -  Set sJPhotoDate = 'Photo Taken: ' || SalFmtFormatDateTime( dJPhotoDate, 'MM-dd-yyyy')
.head 8 +  Else 
.head 9 -  Set sJPhoto = STRING_Null
.head 9 -  Call SalPicClear( hWndPicture )
.head 9 -  Set sJPhotoDate = 'Photo ' || sJPhotoIFile || '.jpg Not Available'
.head 9 -  ! Call SalMessageBox( sJPhotoName || ' Image Not Found', 'Read Error', MB_Ok )
.head 7 +  Else 
.head 8 -  Call SalPicClear( hWndPicture )
.head 8 -  Set sJPhoto = STRING_Null
.head 8 -  Set sJPhotoDate = 'Photo '|| sJPhotoIFile || ' not available'
.head 6 +  Else 
.head 7 -  Call SalPicClear( hWndPicture )
.head 7 -  Set sJPhoto = STRING_Null
.head 7 -  Set sJPhotoDate = 'No Photo Available'
.head 5 +  Else 
.head 6 -  Call SalPicClear( hWndPicture )
.head 6 -  Set sJPhoto = STRING_Null
.head 6 -  Set sJPhotoDate = 'No Photo Available'
.head 5 -  Return sJPhotoDate
.head 3 +  Function: GetJailPhoto
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sJPhotoDate
.head 4 +  Parameters
.head 5 -  String: sJSSNo
.head 5 -  Receive String: sJPhoto
.head 5 -  Receive String: sJPhotoDate
.head 5 -  Receive Date/Time: dJPhotoDate
.head 5 -  Receive Window Handle: hWndPicture
.head 4 +  Static Variables
.head 5 -  Boolean: bPhotoDirSet
.head 5 -  String: sJPhotoDirectory
.head 4 +  Local variables
.head 5 -  String: sJPhotoIFile
.head 5 -  String: sJPhotoFlag
.head 5 -  String: sJPhotoName
.head 5 -  Number: nSqlReturn
.head 5 -  Number: nLoadReturn
.head 4 +  Actions
.head 5 +  If Not bPhotoDirSet
.head 6 -  Set bPhotoDirSet = TRUE
.head 6 +  If VisFileGetSize( '\\\\192.168.38.5\\Imaging\\Jail_Images\\Front\\s\\00\\0000000000.jpg' ) < 0
.head 7 -  Call SalLoadAppAndWait( 'NetUseAttach.bat jail photos', Window_Normal, nLoadReturn)
.head 6 -  Set sJPhotoDirectory = '\\\\192.168.38.5\\Imaging\\Jail_Images\\Front\\s\\'
.head 5 -  Set sJPhotoFlag = sJPhoto
.head 5 +  If SalStrLength( sJSSNo ) = 11
.head 6 -  Set sJSSNo = SalStrMidX( sJSSNo, 0, 3 ) || SalStrMidX( sJSSNo, 4, 2 ) || SalStrMidX( sJSSNo, 7, 4 )
.head 5 +  If SalStrLength( sJSSNo ) = 9
.head 6 -  Call SqlPrepareAndExecute( hSql, "Select p.idate, p.ifile
	from jail.Inmate I, jail.image p into :dJPhotoDate, :sJPhotoIFile
	where i.id=p.id and i.ssn=:sJSSNo and p.itype like 'F%'
	order by p.idate desc, ifile desc" )
.head 6 +  If SqlFetchNext( hSql, nSqlReturn )
.head 7 +  If sJPhotoFlag = 'Name Only'
.head 8 -  Set sJPhoto = sJPhotoIFile
.head 8 -  Set sJPhotoDate = 'Photo Taken: ' || SalFmtFormatDateTime( dJPhotoDate, 'MM-dd-yyyy')
.head 8 -  Return sJPhotoDate
.head 7 -  Set sJPhotoName = sJPhotoDirectory || SalStrRightX( sJPhotoIFile, 2 ) || '\\'
.head 7 -  Set sJPhotoName = sJPhotoName || sJPhotoIFile || '.jpg'
.head 7 +  If SalPicSetFile ( hWndPicture, sJPhotoName )
.head 8 +  If SalPicGetString ( hWndPicture, PIC_FormatObject, sJPhoto ) > 0
.head 9 -  Set sJPhotoDate = 'Photo Taken: ' || SalFmtFormatDateTime( dJPhotoDate, 'MM-dd-yyyy')
.head 8 +  Else
.head 9 -  Set sJPhoto = STRING_Null
.head 9 -  Call SalPicClear( hWndPicture )
.head 9 -  Set sJPhotoDate = 'Photo ' || sJPhotoIFile || '.jpg Not Available'
.head 9 -  ! Call SalMessageBox( sJPhotoName || ' Image Not Found', 'Read Error', MB_Ok )
.head 7 +  Else
.head 8 -  Call SalPicClear( hWndPicture )
.head 8 -  Set sJPhoto = STRING_Null
.head 8 -  Set sJPhotoDate = 'Photo '|| sJPhotoIFile || ' not available'
.head 6 +  Else
.head 7 -  Call SalPicClear( hWndPicture )
.head 7 -  Set sJPhoto = STRING_Null
.head 7 -  Set sJPhotoDate = 'No Photo Available'
.head 5 +  Else
.head 6 -  Call SalPicClear( hWndPicture )
.head 6 -  Set sJPhoto = STRING_Null
.head 6 -  Set sJPhotoDate = 'No Photo Available'
.head 5 -  Return sJPhotoDate
.head 3 +  Function: ReadRegistryTitle
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/Court', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Title', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: WriteUser
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Write )
.head 6 -  Call SalFileWrite( hFLogin, SqlUser, 9 )
.head 6 -  Call SalFileClose( hFLogin )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  Call REG.WriteStringAt('Software/CJIS/LastUser',TRUE, 'User', SqlUser)
.head 5 -  Call REG.CloseKey( )
.head 3 +  Function: ReadRegistry
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/PUBLIC', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'PUBLIC', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: ReadRegistrySelect
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/UserSelect', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Select', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: ReadRegistryLastUser
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/LastUser', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'User', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: Check_Change_Password
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtChange_Password_Date
.head 5 -  String: sChangePassword
.head 5 -  Number: nGraceLogins
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select change_password_date  INTO  :dtChange_Password_Date  from crim.CourtInfo ' )
.head 5 -  Call SqlFetchNext( hSql, nError)
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select change_password, gracelogins
	into  :sChangePassword, :nGraceLogins
	from crim.Users where '||sSelectWhere)
.head 5 -  Call SqlFetchNext( hSql, nError )
.head 5 +  If (dtChange_Password_Date <= SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nGraceLogins > 0and sChangePassword = 'Y')
.head 6 +  If sChangePassword = 'Y'
.head 7 -  Set bChange = TRUE
.head 7 -  Call SalMessageBox( 'PASSWORD HAS EXPIRED,  You have '||SalNumberToStrX(nGraceLogins, 0 )||' grace logins left.  Please change your password.  NOTE:  Password must be unique, not used before for your login!', 'Warning', MB_Ok|MB_IconExclamation )
.head 7 -  Call SalModalDialog( dlgPasswordAPL, hWndForm )
.head 5 +  Else If (dtChange_Password_Date < SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nGraceLogins = 0 and sChangePassword = 'Y')
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalMessageBox(  'Your user login has been disabled, the grace logins of 5 have been used.  Please Contact System Administrator', 'NO GRACE LOGINS', MB_Ok|MB_IconExclamation )
.head 6 -  Call SalQuit(  )
.head 3 +  Function: Inc
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive Number: n
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set n = n + 1
.head 5 -  Return n
.head 3 +  Function: Dec
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive Number: n
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set n = n - 1
.head 5 -  Return n
.head 3 +  Function: MapDrive
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sOpenType
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nLoadAppRet
.head 5 -  File Handle: hFileWrite
.head 4 +  Actions
.head 5 +  If sOpenType = 'Read'
.head 6 -  Set bDriveMapped = VisDosExist( 'I:\\Images\\Connect.txt' )
.head 6 +  If Not bDriveMapped
.head 7 -  Call SalModalDialog( dlgMapDriveWait, hWndForm, sOpenType )
.head 5 +  Else If sOpenType = 'Write'
.head 6 -  Set bDriveMapped = VisDosExist( 'I:\\Images\\Connect.txt' )
.head 6 +  If Not bDriveMapped
.head 7 -  Call SalModalDialog( dlgMapDriveWait, hWndForm, sOpenType )
.head 6 +  Else
.head 7 +  If SalFileOpen( hFileWrite, 'I:\\Images\\Connect.txt', OF_ReadWrite )	
.head 8 -  Call SalFileClose( hFileWrite )
.head 7 +  Else
.head 8 -  Call SalLoadAppAndWait( 'NetUseDelete.bat', Window_NotVisible, nLoadAppRet )  !Window_NotVisible
.head 8 -  Call SalModalDialog( dlgMapDriveWait, hWndForm, sOpenType )
.head 3 +  Function: Current_Date
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Parameters
.head 5 -  String: sTimestamp
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Actions
.head 5 +  If sTimestamp = 'Timestamp'
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select sysdate
	from dual into :dCurrentDate' )
.head 6 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  Else
.head 6 -  Set dCurrentDate = SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 )
.head 5 -  Return dCurrentDate
.head 3 +  Function: HolidayCheck	!! Returns TRUE if lands on holiday
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean: bHoliday
.head 4 +  Parameters
.head 5 -  Date/Time: dHearDate
.head 5 -  Boolean: bMessage
.head 5 -  Boolean: bWeekEnd
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sHoliday = STRING_Null
.head 5 -  Set dHearDate = SalDateConstruct( SalDateYear( dHearDate ), SalDateMonth( dHearDate ), SalDateDay( dHearDate ), 0, 0, 0 )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT  text
			      FROM  crim.calendar  INTO :sHoliday
			      WHERE :dHearDate = holiday')
.head 5 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  If nReturn = FETCH_Ok
.head 6 +  If bMessage
.head 7 -  Call SalMessageBeep( MB_IconAsterisk )
.head 7 -  Call SalMessageBox('Date would have fallen on ' || sHoliday || '.', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 -  Return TRUE
.head 5 +  Else If SalDateWeekday( dHearDate ) = 0
.head 6 +  If bMessage
.head 7 -  Call SalMessageBeep( MB_IconAsterisk )
.head 7 -  Call SalMessageBox('Date would have fallen on a Saturday', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 +  If bWeekEnd
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Return FALSE
.head 5 +  Else If SalDateWeekday( dHearDate ) = 1
.head 6 +  If bMessage
.head 7 -  Call SalMessageBeep( MB_IconAsterisk )
.head 7 -  Call SalMessageBox('Date would have fallen on a Sunday', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 +  If bWeekEnd
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Return FALSE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: CopyRows
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Number: nParam
.head 5 -  Window Handle: hWndTable
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If nParam = 1
.head 6 +  If SalTblCopyRows( hWndTable, 2, 0 )
.head 7 -  Call SalMessageBox( 'Selected Rows copied to Clipboard', 'Process Complete', MB_Ok)
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'No Rows selected to copy to Clipboard, please make selection and try again.', 'Process Incomplete', MB_Ok)
.head 5 +  Else If nParam = 2
.head 6 +  If SalTblCopyRows( hWndTable, 0, 0 )
.head 7 -  Call SalMessageBox( 'All Rows copied to Clipboard', 'Process Complete', MB_Ok)
.head 3 +  Function: UpdateSupreme
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 5 -  Boolean: bCommit
.head 5 -  String: sProgramLocation
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtToday
.head 5 -  Number: nMaxSeq
.head 4 +  Actions
.head 5 +  If dtInsDateF = DATETIME_Null
.head 6 -  Set dtToday = Current_Date( 'Date' )
.head 5 +  Else
.head 6 -  Set dtToday =dtInsDateF
.head 5 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	caseyr
			      FROM	cr_supreme
			      WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo and
					type = :sType and
					ins_date = :dtToday and
					line_no = :nLine')
.head 5 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 5 +  If nReturn = FETCH_EOF
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	max(seq)
			     FROM		cr_supreme
			     WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo
			     INTO		:nMaxSeq')
.head 6 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 6 +  If nMaxSeq = NUMBER_Null
.head 7 -  Set nMaxSeq = 0
.head 6 -  Set nMaxSeq = nMaxSeq + 1
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'INSERT INTO	cr_supreme (caseyr, casety, caseno, seq, ins_date, type, line_no, judge, program_location)
			             VALUES		   ( :sCaseYr, :sCaseTy, :sCaseNo, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation)')
			             				
.head 6 +  If bCommit
.head 7 -  Call SqlCommit(hSqlSup)
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: Encode_BarCode
.head 4 -  Description: WRP (10-26-2003)
	-This function takes a string parameter and converts it into the code
	 that is understandable to a  barcode reader using CODE 128 font.
	-It returns the encoded text as a string variable
	-This function is compatible with Elfring Font INC
	-This uses Subset C
	-Perfered font is
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: BarTextIn
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: BarTextOut
.head 5 -  String: BarTextInA
.head 5 -  String: TempString
.head 5 -  String: BarTempOut
.head 5 -  String: BarCodeOut
.head 5 -  Number: Sum
.head 5 -  Number: n
.head 5 -  Number: nStrLength
.head 5 -  Number: ThisChar
.head 5 -  Number: CharValue
.head 5 -  Number: CheckSumValue
.head 5 -  String: CheckSum
.head 5 -  Number: Subset
.head 5 -  String: StartChar
.head 5 -  Number: Weighting
.head 5 -  Number: UCC
.head 5 -  String: sCurrentCharReal
.head 4 +  Actions
.head 5 -  ! Initialize input and output strings
.head 5 -  Set Sum = 104
.head 5 -  Set StartChar = '|'
.head 5 -  Set n = 1
.head 5 -  Set nStrLength = SalStrLength(BarTextIn)
.head 5 +  While n <= nStrLength
.head 6 -  ! ThisChar = (Asc(Mid(BarTextIn, II, 1)))
.head 6 -  ! Call SalStrFirstC( 'A', ThisChar )
.head 6 -  ! Call SalStrFirstC( SalStrMidX(BarTextIn, (n-1), 1), ThisChar )
.head 6 -  Set sCurrentCharReal = SalStrMidX(BarTextIn, (n-1), 1)
.head 6 -  Call SalStrFirstC( sCurrentCharReal, ThisChar )
.head 6 +  If ThisChar < 127
.head 7 -  Set CharValue = ThisChar - 32
.head 6 +  Else
.head 7 -  Set CharValue = ThisChar - 103
.head 6 -  Set Sum = Sum + (CharValue * n)
.head 6 +  If SalStrMidX(BarTextIn, (n-1), 1) = " "
.head 7 -  Set BarTextOut = BarTextOut || SalNumberToChar(228)
.head 6 +  Else If ThisChar = 34
.head 7 -  Set BarTextOut = BarTextOut || SalNumberToChar(226)
.head 6 +  Else
.head 7 -  Set BarTextOut = BarTextOut || SalStrMidX(BarTextIn, (n-1), 1)
.head 6 -  Set n = n + 1
.head 5 -  Set CheckSumValue = SalNumberMod( Sum, 103 )
.head 5 +  If CheckSumValue > 90
.head 6 -  Set CheckSum = SalNumberToChar(CheckSumValue + 103)
.head 5 +  Else If CheckSumValue > 0
.head 6 -  Set CheckSum = SalNumberToChar(CheckSumValue + 32)
.head 5 +  Else
.head 6 -  Set CheckSum = SalNumberToChar(228)
.head 5 -  Return (StartChar || BarTextOut || CheckSum || "~ ")
.head 3 +  Function: GetAdminLock
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sAdminLock
.head 4 +  Parameters
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseTy
.head 5 -  String: fCaseNo
.head 5 -  Receive String: fAdminLockComment
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: fSystem
.head 5 -  String: sAdminLock
.head 5 -  Number: fnCaseYr
.head 5 -  Number: fnCaseNo
.head 4 +  Actions
.head 5 +  If SalStrLeftX( fCaseTy, 2 ) = 'CV'
.head 6 -  Set fSystem = 'CIVIL'
.head 5 +  Else
.head 6 -  Set fSystem = 'CRIM'
.head 5 +  If fSystem = 'CRIM'
.head 6 -  Call SqlPrepareAndExecute(hSql, 'SELECT	admin_lock, admin_lock_comment
		  	      FROM	muni_booking
			      WHERE	caseyr = :fCaseYr and casety = :fCaseTy and caseno = :fCaseNo
			      INTO		:sAdminLock, :fAdminLockComment')
.head 5 +  Else
.head 6 -  Set fnCaseYr = SalStrToNumber( fCaseYr )
.head 6 -  Set fnCaseNo = SalStrToNumber( fCaseNo )
.head 6 -  Call SqlPrepareAndExecute(hSql, 'SELECT	admin_lock, admin_lock_comment
		  	      FROM	casemaster
			      WHERE	caseyr = :fnCaseYr and caseno = :fnCaseNo
			      INTO		:sAdminLock, :fAdminLockComment')
.head 5 -  Call SqlFetchNext(hSql, nReturn)
.head 5 -  Return sAdminLock
.head 3 +  Function: ValidatePhone
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Receive String: sVPhone
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Boolean: bEditOk
.head 4 +  Actions
.head 5 -  Set bEditOk = TRUE
.head 5 -  Set sVPhone = StrReplace (sVPhone, ' ', '-' )
.head 5 -  If SalStrLength( sVPhone ) = 0
.head 5 +  Else If SalStrLength( sVPhone ) = 7
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) > 0 and SalStrToNumber( SalStrMidX( sVPhone, 3, 4 ) ) > 0
.head 7 -  Set sVPhone = SalStrMidX( sVPhone, 0, 3 ) || '-' || SalStrMidX( sVPhone, 3, 4 )
.head 6 +  Else
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 8, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else If SalStrLength( sVPhone ) = 8
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 4, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else If SalStrLength( sVPhone ) = 10
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) > 0 and SalStrToNumber( SalStrMidX( sVPhone, 3, 3 ) ) > 0 and SalStrToNumber( SalStrMidX( sVPhone, 6, 4 ) ) > 0
.head 7 -  Set sVPhone = SalStrMidX( sVPhone, 0, 3 ) || '-' || SalStrMidX( sVPhone, 3, 3 ) || '-' || SalStrMidX( sVPhone, 6, 4 )
.head 6 +  Else
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 8, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else If SalStrLength( sVPhone ) = 12
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 4, 3 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 8, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else
.head 6 -  Set bEditOk = FALSE
.head 5 +  If Not bEditOk
.head 6 -  Call SalMessageBox( 'Invalid Phone Number Entered;  Please verify', 'Phone/Area Code Error', MB_IconStop | MB_Ok )
.head 5 -  Return bEditOk
.head 3 +  Function: Insert_Address
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sIACaseYr
.head 5 -  String: sIACaseTy
.head 5 -  String: sIACaseNo
.head 5 -  String: sIASSN
.head 5 -  Date/Time: dIADOB
.head 5 -  String: sIAAddress
.head 5 -  String: sIACity
.head 5 -  String: sIAState
.head 5 -  String: sIAZip
.head 5 -  String: sIAAreaCode
.head 5 -  String: sIAPhone
.head 5 -  String: sReason
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAddrUser
.head 5 -  String: sAddrRowId
.head 5 -  Date/Time: sAddrDate
.head 5 -  Number: nIAReturn
.head 4 +  Actions
.head 5 +  If sIAAddress=STRING_Null and sIACity=STRING_Null and sIAState=STRING_Null and sIAZip=STRING_Null and sIAPhone=STRING_Null
.head 6 -  Return TRUE
.head 5 -  Call SalStrTrim( sIAPhone, sIAPhone )
.head 5 +  If SalStrLength( sIAPhone ) > 8 and sIAAreaCode = STRING_Null
.head 6 +  If SalStrLeftX( sIAPhone, 1 ) >= '0' and SalStrLeftX( sIAPhone, 1 ) <= '9'
.head 7 -  Set sIAAreaCode = SalStrLeftX( sIAPhone, 3 )
.head 6 +  Else
.head 7 -  Set sIAAreaCode = SalStrMidX( sIAPhone, 1, 3 )
.head 6 -  Set sIAPhone = SalStrRightX( sIAPhone, 8 )
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT entrydate, username, rowid
	FROM crim.cr_address into :sAddrDate, :sAddrUser, :sAddrRowId
	WHERE caseyr = :sIACaseYr and casety = :sIACaseTy and caseno = :sIACaseNo and
		court='CMC' and reason=:sReason 
	ORDER BY entrydate desc")
.head 5 +  If Not SqlFetchNext (hSql, nIAReturn )
.head 6 -  Call SqlPrepareAndExecute (hSql, "insert into crim.cr_address
	(entrydate, ssno, dob, court, caseyr, casety, caseno, city, state, zip, address1, areacode, phone, reason) VALUES
	(sysdate, :sIASSN, :dIADOB, 'CMC', :sIACaseYr, :sIACaseTy, :sIACaseNo, :sIACity, :sIAState, :sIAZip, :sIAAddress, :sIAAreaCode, :sIAPhone, :sReason)")
.head 5 +  Else
.head 6 +  If sAddrUser = SqlUser and sAddrDate > SalDateCurrent(  ) - 1
.head 7 -  Call SqlPrepareAndExecute (hSql, "Update crim.cr_address set
	entrydate=sysdate, ssno=:sIASSN, dob=:dIADOB, caseyr=:sIACaseYr, casety=:sIACaseTy,
	caseno=:sIACaseNo, city=:sIACity, state=:sIAState, zip=:sIAZip, address1=:sIAAddress, areacode=:sIAAreaCode, phone=:sIAPhone
	where rowid=:sAddrRowId")
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute (hSql, "insert into crim.cr_address
	(entrydate, ssno, dob, court, caseyr, casety, caseno, city, state, zip, address1, areacode, phone, reason) VALUES
	(sysdate, :sIASSN, :dIADOB, 'CMC', :sIACaseYr, :sIACaseTy, :sIACaseNo, :sIACity, :sIAState, :sIAZip, :sIAAddress, :sIAAreaCode, :sIAPhone, :sReason)")
.head 5 -  Call SqlCommit(hSql)
.head 3 +  Function: StrReplace
.head 4 -  Description: Replaces any occurences of sFind with sReplace
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sSource
.head 5 -  String: sFind
.head 5 -  String: sReplace
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFoundPos
.head 5 -  String: sLeft
.head 5 -  String: sRight
.head 4 +  Actions
.head 5 -  Set sRight = sSource
.head 5 +  Loop
.head 6 -  Set nFoundPos = SalStrScan( sRight, sFind )
.head 6 +  If nFoundPos = -1
.head 7 -  Set sLeft = sLeft || sRight
.head 7 -  Break
.head 6 -  Set sLeft = sLeft || SalStrLeftX( sRight, nFoundPos ) || sReplace
.head 6 -  Set sRight = SalStrRightX( sRight, SalStrLength( sRight ) - nFoundPos - SalStrLength( sFind ) )
.head 5 -  Return sLeft
.head 3 +  Function: Set_Printer
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sName
.head 5 -  String: sDriver
.head 5 -  String: sPort
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalPrtGetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 5 -  Call SalPrtSetDefault( sName, sDriver, sPort )
.head 3 +  Function: Reset_Printer
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If sDefaultDevice != STRING_Null
.head 6 -  Call SalPrtSetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 6 -  Set sDefaultDevice = STRING_Null
.head 6 -  Set sDefaultDriver = STRING_Null
.head 6 -  Set sDefaultPort = STRING_Null
.head 3 +  Function: fTotalFinesCosts
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: sTFCaseYr
.head 5 -  String: sTFCaseTy
.head 5 -  String: sTFCaseNo
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetchResult
.head 5 -  Number: nTotalFine
.head 5 -  Number: nTotalOwed
.head 4 +  Actions
.head 5 -  Set nTotalOwed = 0
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select sum(costs) - sum(nvl(paid, 0))
	from crim.cr_costs into :nTotalOwed
	where caseyr=:sTFCaseYr and casety=:sTFCaseTy and caseno=:sTFCaseNo and status is null'  )
.head 5 -  Call SqlFetchNext( hSql, nFetchResult )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select sum(nvl(net_fine,0)) - sum(nvl(fine_paid, 0))
	from crim.cr_charge into :nTotalFine
	where caseyr=:sTFCaseYr and casety=:sTFCaseTy and caseno=:sTFCaseNo and status is null'  )
.head 5 -  Call SqlFetchNext( hSql, nFetchResult )
.head 5 -  Set nTotalOwed = nTotalOwed + nTotalFine
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select sum(nvl(net_fine,0)) - sum(nvl(fine_paid, 0))
	from crim.cr_reduced into :nTotalFine
	where caseyr=:sTFCaseYr and casety=:sTFCaseTy and caseno=:sTFCaseNo and status is null'  )
.head 5 -  Call SqlFetchNext( hSql, nFetchResult )
.head 5 -  Set nTotalOwed = nTotalOwed + nTotalFine
.head 5 -  Return nTotalOwed
.head 3 +  Function: fEMailNotify
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sPassCRCaseYr
.head 5 -  String: sPassCRCaseTy
.head 5 -  String: sPassCRCaseNo
.head 5 -  Number: nPassCVCaseYr
.head 5 -  Number: nPassCVCaseNo
.head 5 -  String: sPassCaseCode
.head 5 -  Date/Time: dPassDockDate
.head 5 -  Number: nPassSeq
.head 5 -  String: sNotifyUser1
.head 5 -  String: sNotifyUser2
.head 5 -  String: sNotifyUser3
.head 5 -  String: sPassMessage
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 5 -  Number: nMessageNumber
.head 4 +  Actions
.head 5 +  If nPassCVCaseYr != NUMBER_Null
.head 6 -  Call fGetEMailAddress ( 1, sNotifyUser1, STRING_Null, STRING_Null, STRING_Null, nPassCVCaseYr, nPassCVCaseNo )
.head 6 +  If sNotifyUser2 != STRING_Null
.head 7 -  Call fGetEMailAddress ( 1, sNotifyUser2, STRING_Null, STRING_Null, STRING_Null, nPassCVCaseYr, nPassCVCaseNo )
.head 6 +  If sNotifyUser3 != STRING_Null
.head 7 -  Call fGetEMailAddress ( 1, sNotifyUser3, STRING_Null, STRING_Null, STRING_Null, nPassCVCaseYr, nPassCVCaseNo )
.head 5 +  If sNotifyUser1 = STRING_Null and sNotifyUser2 = STRING_Null and sNotifyUser3 = STRING_Null
.head 6 -  Return TRUE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select EMAILSEQUENCE.NEXTVAL
	from Dual into :nMessageNumber' )
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'insert into crim.EMailMessages
	(MessageNo, CRCaseYr, CRCaseTy, CRCaseNo,
		CVCaseYr, CVCaseNo, CaseCode, DockDate, Seq, Message)  Values
	(:nMessageNumber, :sPassCRCaseYr, :sPassCRCaseTy, :sPassCRCaseNo,
		:nPassCVCaseYr, :nPassCVCaseNo, :sPassCaseCode, :dPassDockDate, :nPassSeq, :sPassMessage) ' )
.head 5 +  If sNotifyUser1 != STRING_Null
.head 6 -  Call fInsertEMailAddress ( sNotifyUser1, nMessageNumber)
.head 5 +  If sNotifyUser2 != STRING_Null
.head 6 -  Call fInsertEMailAddress ( sNotifyUser2, nMessageNumber)
.head 5 +  If sNotifyUser3 != STRING_Null
.head 6 -  Call fInsertEMailAddress ( sNotifyUser3, nMessageNumber)
.head 5 -  Call SqlCommit( hSql )
.head 3 +  Function: fGetEMailAddress
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bCivil
.head 5 -  Receive String: sNotify
.head 5 -  String: sPassCRCaseYr
.head 5 -  String: sPassCRCaseTy
.head 5 -  String: sPassCRCaseNo
.head 5 -  Number: nPassCVCaseYr
.head 5 -  Number: nPassCVCaseNo
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 +  If bCivil
.head 6 +  If sNotify = 'JUDGE'
.head 7 -  Call SqlPrepareAndExecute( hSql, 'Select email
	from crim.casemaster m, crim.users u into :sNotify
	where m.caseyr=:nPassCVCaseYr and m.caseno=:nPassCVCaseNo and
		u.secretary is not null and substr(m.judge, 1, 1) || substr(m.judge, 3, 1) = u.judge' )
.head 7 +  If Not SqlFetchNext( hSql, nFetch )
.head 8 -  Set sNotify = STRING_Null
.head 6 +  Else If sNotify = 'SECRETARY'
.head 7 -  Call SqlPrepareAndExecute( hSql, 'Select s.email
	from crim.casemaster m, crim.users u, users s   into :sNotify
	where m.caseyr=:nPassCVCaseYr and m.caseno=:nPassCVCaseNo and
		u.secretary=s.username and substr(m.judge, 1, 1) || substr(m.judge, 3, 1)=u.judge' )
.head 7 +  If Not SqlFetchNext( hSql, nFetch )
.head 8 -  Set sNotify = STRING_Null
.head 6 +  Else If SalStrScan( sNotify, '@' ) = -1
.head 7 -  Call SqlPrepareAndExecute( hSql, 'Select u.email
	from crim.users u  into :sNotify
	where u.username = :sNotify' )
.head 7 +  If Not SqlFetchNext( hSql, nFetch )
.head 8 -  Set sNotify = STRING_Null
.head 3 +  Function: CheckOVIReg
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseTy
.head 5 -  String: fCaseNo
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sStatuteFull
.head 5 -  String: sStatuteC
.head 5 -  String: sStatuteR
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, "SELECT    statute||' ('|| degree ||')'
	FROM    crim.cr_charge   INTO    :sStatuteC 
	WHERE    caseyr = :fCaseYr and casety = :fCaseTy and caseno = :fCaseNo and 
		status is null and bmv in ('02', '87', '26', '93', 'AS', 'BA', 'AR', 'BY', 'BZ', '77', 'AI', '19', '65', '75', 'AG', 'AH', '78', '87', '93', 'AJ') and 
		finding not in ('C', 'D', 'DD', 'DP', 'PM') ")
.head 5 +  While SqlFetchNext( hSql, nReturn)
.head 6 +  If sStatuteFull = ''
.head 7 -  Set sStatuteFull = sStatuteC
.head 6 +  Else
.head 7 -  Set sStatuteFull = sStatuteFull || '
' || sStatuteC
.head 5 -  Call SqlPrepareAndExecute( hSql, "SELECT    statute||' ('|| degree ||')'
	FROM    crim.cr_reduced   INTO  :sStatuteR
	WHERE    caseyr = :fCaseYr and casety = :fCaseTy and caseno = :fCaseNo and 
		bmv in ('02', '87', '26', '93', 'AS', 'BA', 'AR', 'BY', 'BZ', '77', 'AI', '19', '65', '75', 'AG', 'AH', '78', '87', '93', 'AJ') and
		finding not in ('C', 'D', 'DD', 'DP', 'PM') ")
.head 5 +  While SqlFetchNext( hSql, nReturn)
.head 6 +  If sStatuteFull = ''
.head 7 -  Set sStatuteFull = sStatuteR
.head 6 +  Else
.head 7 -  Set sStatuteFull = sStatuteFull || '
' || sStatuteR
.head 5 +  If sStatuteFull != ''
.head 6 -  Call SalMessageBox( 'This Case must be CHECKED.  Defendant may be a habitual Offender.', 'CASE MUST BE CHECKED', MB_IconStop | MB_Ok)
.head 6 -  Return sStatuteFull
.head 5 +  Else
.head 6 -  Return STRING_Null
.head 5 -  ! '02', '87', '26', '93', 'AS', 'BA', 'AR', 'BY', 'BZ', '77', 'AI', '19', '65', '75', 'AG', 'AH', '78', '87', '93', 'AJ'"
.head 3 +  Function: fInsertEMailAddress
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sNotify
.head 5 -  Number: nMessageNumber
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sEMailAddressInsert
.head 4 +  Actions
.head 5 -  Set sEMailAddressInsert = 'insert into crim.EMailAddresses
	(MessageNo, EMailAddress)  Values
	(:nMessageNumber, :sNotify) '
.head 5 -  Call SqlPrepareAndExecute( hSql, sEMailAddressInsert)
.head 3 +  Function: PrintBarcodeLabelCV
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseNo
.head 5 -  Number: nRatnum
.head 5 -  String: sDockCode
.head 5 -  Number: nBarcodeId
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sCaseNumber
.head 5 -  String: sBarCode
.head 5 -  Date/Time: dTimestamp
.head 5 -  Number: nError
.head 5 -  String: sReportBinds
.head 5 -  String: sReportInputs
.head 5 -  String: sCaseTy
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select casetype
	from crim.casemaster into :sCaseTy 
	where caseyr = :sCaseYr and caseno = :sCaseNo")
.head 5 -  Call SqlFetchNext(hSql, nError)
.head 5 -  Call SalCreateWindow(frmLabels, hWndNULL)
.head 5 -  Set sCaseNumber = sCaseYr || 'CV' || sCaseTy || sCaseNo
.head 5 -  Set sCaseTy = 'CV' || sCaseTy
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select barcode_number  
	from dotnet.barcodes into :sBarCode 
	where caseyr = :sCaseYr and casety = :sCaseTy and caseno = :sCaseNo and ratnum = :nRatnum")
.head 5 -  Call SqlFetchNext(hSql, nError)
.head 5 +  If sBarCode = STRING_Null
.head 6 -  Call SqlPrepareAndExecute(hSql, "Update crim.control set wait = 1")
.head 6 -  Call SqlPrepareAndExecute(hSql, "Select lpad(max(barcode_number)+1, 10, '0') INTO :sBarCode from dotnet.barcodes")
.head 6 -  Call SqlFetchNext(hSql, nError)
.head 6 -  Call SqlPrepareAndExecute(hSql, "Insert into dotnet.barcodes(barcode_number, caseyr, casety, caseno, ratnum, division, barcodeid)
			VALUES(:sBarCode, :sCaseYr, :sCaseTy, :sCaseNo, :nRatnum, 'CIVIL', :nBarcodeId)")
.head 6 -  Call SqlCommit(hSql)
.head 5 -  Set sReprintBarcode = sBarCode
.head 5 -  Set dTimestamp = SalDateCurrent()
.head 5 -  Set sBarCode = '*' || sBarCode || '*'
.head 5 -  Call SalPrtGetDefault(sDefaultDevice, sDefaultDriver, sDefaultPort)
.head 5 -  Call SalPrtSetDefault('Labels', 'DYMO LabelWriter 310', 'USB001')
.head 5 -  Set sReportBinds = 'sCaseNumber, dTimestamp, sBarCode, sDockCode'
.head 5 -  Set sReportInputs = 'CASENO, TIMESTAMP, BARCODE, DOCKCODE'
.head 5 -  Call SalReportPrint ( frmLabels, CV_REPORT_Path || 'BarcodeLabel.qrp',
	sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nError )
.head 5 -  Call SalDestroyWindow (frmLabels)
.head 5 -  Call SalPrtSetDefault(sDefaultDevice, sDefaultDriver, sDefaultPort)
.head 3 +  Function: AddtoDocket
.head 4 -  Description: Adds the Requested Data to the Docket File
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Number: DCaseYr
.head 5 -  Number: DCaseNo
.head 5 -  Date/Time: DocketDate
.head 5 -  String: DCode
.head 5 -  String: DEntry1
.head 5 -  String: DEntry2
.head 5 -  String: DEntry3
.head 5 -  String: DEntry4
.head 5 -  String: DMemo
.head 5 -  Date/Time: RDate
.head 5 -  Number: RAmt
.head 5 -  Number: RAtty
.head 5 -  Boolean: bPrintBarcode
.head 5 -  Number: nBarcodeId
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetchNum
.head 5 -  Boolean: bFetchOK
.head 5 -  Number: nDockNum
.head 5 -  Number: nRatnum
.head 5 -  String: sPrintBarcode
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT Max( DNUM)
	FROM crim.DOCKET INTO :nDockNum
	WHERE CASEYR = :DCaseYr AND CASENO = :DCaseNo ')
.head 5 -  Set bFetchOK = SqlFetchRow (hSql, 0, nFetchNum)
.head 5 +  If bFetchOK = FALSE  OR  nFetchNum != FETCH_Ok
.head 6 -  Set nDockNum=0
.head 5 -  Set nDockNum=nDockNum+4
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT docket_ratnum_seq.nextval FROM dual INTO :nRatnum')
.head 5 -  Call SqlFetchNext(hSql, nFetchNum)
.head 5 -  Set nGlobRatnum = nRatnum
.head 5 -  Call SqlPrepareAndExecute( hSql, 'insert into crim.DOCKET
   (CASEYR, CASENO, DDATE, DNUM, CODE5, ENTRY1, ENTRY2, ENTRY3, ENTRY4, MEMO, RDATE, RAMT, RATTY, ratnum ) VALUES
   (:DCaseYr, :DCaseNo, :DocketDate, :nDockNum, :DCode, :DEntry1, :DEntry2, :DEntry3, :DEntry4, :DMemo, :RDate, :RAmt, :RAtty, :nRatnum)' )
.head 5 -  Call SqlCommit ( hSql )
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select printbarcode INTO :sPrintBarcode 
	from crim.cvcodes where code = :DCode")
.head 5 -  Call SqlFetchNext(hSql, nFetchNum)
.head 5 +  If bPrintBarcode and sPrintBarcode = 'Y'
.head 6 -  Call PrintBarcodeLabelCV(SalNumberToStrX(DCaseYr, 0), SalFmtFormatNumber(DCaseNo, '00000'), nRatnum, DCode, nBarcodeId)
.head 5 -  Return nDockNum
.head 3 +  Function: Get_File_Date
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Date/Time:
.head 4 +  Parameters
.head 5 -  String: spFile
.head 5 -  String: spType
.head 5 -  String: sDivision
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sPath
.head 5 -  Date/Time: dRet
.head 4 +  Actions
.head 5 +  If spType = "LIVE"
.head 6 -  Set sPath = sAppsPath || "\\Live\\Apps\\" || spFile
.head 5 +  Else If spType = "TEST"
.head 6 -  Set sPath = sAppsPath || "\\Test\\Apps\\" || spFile
.head 5 +  Else If spType = "LOCAL"
.head 6 -  Set sPath = LocalPath || sDivision || "\\" || spFile
.head 5 -  Call SalFileGetDateTime( sPath, dRet )
.head 5 -  Return dRet
.head 3 +  Function: Update_App
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: spFile
.head 5 -  String: sDivision
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sType
.head 5 -  String: sPath
.head 5 -  Date/Time: dLocal
.head 5 -  Date/Time: dLive
.head 5 -  String: sFile
.head 5 -  String: sTarget
.head 5 -  String: sReg
.head 5 -  String: sApp
.head 5 -  String: spFileNoExe
.head 4 +  Actions
.head 5 +  If sDivision = 'CIVIL'
.head 6 -  Set sAppsPath = CivilPath
.head 5 +  Else
.head 6 -  Set sAppsPath = CrimPath
.head 5 +  If FALSE
.head 6 -  Set sType = "TEST"
.head 6 -  Set sPath = sAppsPath || "\\Test\\Apps\\"
.head 5 +  Else
.head 6 -  Set sType = "LIVE"
.head 6 -  Set sPath = sAppsPath || "\\Live\\Apps\\"
.head 5 -  Set dLocal = Get_File_Date( spFile, "LOCAL", sDivision )
.head 5 -  Set dLive = Get_File_Date( spFile, sType, sDivision )
.head 5 +  ! If dLocal = DATETIME_Null Or dLive != dLocal
.head 6 -  Call SalFileCopy( sPath || spFile, sLocalPath || "\\" || spFile, TRUE )
.head 5 -  Set spFileNoExe = SalStrLeftX( spFile, SalStrLength( spFile ) - 4 )
.head 5 +  If SalAppFind( spFileNoExe, TRUE ) = hWndNULL
.head 6 +  If dLocal = DATETIME_Null Or dLive != dLocal
.head 7 -  Call SalFileCopy( sPath || spFile, LocalPath || sDivision || "\\" || spFile, TRUE )
.head 7 -  Call SalFileSetDateTime(LocalPath || sDivision || "\\" || spFile, dLive)
.head 6 -  Call SalLoadApp( LocalPath || sDivision || "\\" || spFile, SqlUser || ' ' ||  SqlPassword || ' ' || SqlDatabase )
.head 3 +  Function: GetImagePath
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Number: nRatnum
.head 5 -  String: sImageType
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sPath
.head 5 -  File Handle: hFile
.head 4 +  Actions
.head 5 +  If nRatnum = 1
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT ratnum FROM dotnet.barcodes INTO :nRatnum WHERE caseyr = :sCaseYr and casety = :sCaseTy and caseno = :sCaseNo")
.head 6 -  Call SqlFetchNext(hSql, nResult)
.head 5 -  Set sPath = "\\\\192.168.38.5\\Images\\"
.head 5 -  Set sPath = sPath || sCaseYr || "\\" || sCaseTy || "\\" || sCaseNo || "\\" || sCaseYr || sCaseTy || sCaseNo || "-" || SalFmtFormatNumber(nRatnum, '0000000') || sImageType || ".pdf"
.head 5 +  If VisFileOpen(hFile, sPath, OF_Exist) = VTERR_Ok
.head 6 -  Return sPath
.head 5 +  Else
.head 6 -  Set sPath =sPath || '.lnk'
.head 6 +  If VisFileOpen(hFile, sPath, OF_Exist) = VTERR_Ok
.head 7 -  Return sPath
.head 6 +  Else
.head 7 -  Return STRING_Null
.head 3 +  Function: PrintPDF
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Number: nRatnum
.head 5 -  String: sPassReprintBarcode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nGetCaseYr
.head 5 -  Number: nGetCaseNo
.head 5 -  String: sMatchCaseYr
.head 5 -  String: sMatchCaseTy
.head 5 -  String: sMatchCaseNo
.head 5 -  Number: nMatchRatnum
.head 5 -  Boolean: bCreateDir
.head 5 -  Number: nDirectories
.head 5 -  String: sGetDirectories[*]
.head 4 +  Actions
.head 5 +  If SalStrLeftX( sCaseTy, 2 ) = 'CR'  or 
	SalStrLeftX( sCaseTy, 2 ) = 'TR'
.head 6 +  If nRatnum = NUMBER_Null
.head 7 -  Call SalMessageBox( 'Procedure Error - Ratnum is null - Notify Programmer', 'Integrity  Error', MB_Ok )
.head 7 -  Return TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "Select caseyr, casety, caseno, ratnum
	from crim.cr_docket INTO :sMatchCaseYr, :sMatchCaseTy, :sMatchCaseNo, :nMatchRatnum  
	where ratnum = :nRatnum")
.head 6 +  If SqlFetchNext(hSql, nError)
.head 7 +  If sMatchCaseYr != sCaseYr  or  sMatchCaseTy != sCaseTy  or  sMatchCaseNo != sCaseNo
.head 8 -  Call SalMessageBox( 'Ratnum does not match the case number on file - Notify Programmer', 'Integrity  Error', MB_Ok )
.head 8 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Ratnum is not on file - Notify Programmer', 'Integrity  Error', MB_Ok )
.head 7 -  Return TRUE
.head 5 +  Else
.head 6 +  If nRatnum != NUMBER_Null
.head 7 -  Call SqlPrepareAndExecute(hSql, "Select caseyr, caseno
	FROM crim.docket INTO :nGetCaseYr, :nGetCaseNo 
	where ratnum = :nRatnum")
.head 7 +  If SqlFetchNext(hSql, nError)
.head 8 +  If sCaseYr != STRING_Null
.head 9 +  If nGetCaseYr != SalStrToNumber( sCaseYr )  or nGetCaseNo != SalStrToNumber( sCaseNo )
.head 10 -  Call SalMessageBox( 'Ratnum does not match the case number on file - Notify Programmer', 'Integrity  Error', MB_Ok )
.head 10 -  Return TRUE
.head 7 +  Else
.head 8 -  Call SalMessageBox( 'Ratnum not found on file - Notify Programmer', 'Integrity  Error', MB_Ok )
.head 8 -  Return TRUE
.head 7 +  If sCaseTy = STRING_Null
.head 8 -  Call SqlPrepareAndExecute(hSql, "Select casetype
	FROM crim.casemaster into :sCaseTy 
	WHERE caseyr = :sCaseYr and caseno = :sCaseNo")
.head 8 -  Call SqlFetchNext(hSql, nError)
.head 8 -  Set sCaseTy = 'CV' || sCaseTy
.head 6 +  Else
.head 7 -  Set nGetCaseYr = SalStrToNumber( sCaseYr )
.head 7 -  Set nGetCaseNo = SalStrToNumber( sCaseNo )
.head 7 -  Call SqlPrepareAndExecute(hSql, "Select max(ratnum) INTO :nRatnum 
	FROM crim.docket where caseyr = :nGetCaseYr and caseno = :nGetCaseNo")
.head 7 -  Call SqlFetchNext(hSql, nError)
.head 7 -  Set nGlobRatnum = nRatnum
.head 6 -  Set sCaseYr = SalFmtFormatNumber( nGetCaseYr, '0000' )
.head 6 -  Set sCaseNo = SalFmtFormatNumber( nGetCaseNo, '00000' )
.head 5 +  If sPassReprintBarcode = STRING_Null
.head 6 -  Call SqlPrepareAndExecute(hSql, "Update crim.control set wait = 1")
.head 6 -  Call SqlPrepareAndExecute(hSql, "Select lpad(max(barcode_number)+1, 10, '0')
	from dotnet.barcodes  into :sReprintBarcode ")
.head 6 -  Call SqlFetchNext(hSql, nError)
.head 6 -  Call SqlPrepareAndExecute(hSql, "Insert into dotnet.barcodes 
	(barcode_number, caseyr, casety, caseno, ratnum, division, barcodeid, scan_date, scan_user)  values 
	(:sReprintBarcode, :sCaseYr, :sCaseTy, :sCaseNo, :nRatnum, 'CIVIL', 500, sysdate, user)")
.head 6 -  Call SqlCommit(hSql)
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute(hSql, "Select ratnum into :nRatnum 
	from dotnet.barcodes where barcode_number = :sReprintBarcode")
.head 6 -  Call SqlFetchNext(hSql, nError)
.head 5 -  ! Set nDirectories = VisDosEnumDirs( "\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseTy || "\\" || sCaseNo, sGetDirectories )
.head 5 +  ! If nDirectories < 1
.head 6 -  Call SalFileCreateDirectory("\\\\192.168.38.5\\images\\" || sCaseYr )
.head 6 -  Call SalFileCreateDirectory("\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseTy )
.head 6 -  Set bCreateDir = SalFileCreateDirectory("\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseTy || "\\" || sCaseNo)
.head 6 +  If Not bCreateDir
.head 7 -  Call SalMessageBox( 'Unable to Create Directory 

' || "\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseTy || "\\" || sCaseNo || ' - Notify Programmer', 'Directory Error', MB_IconStop | MB_Ok )
.head 5 -  ! Set sFilePath = "\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseTy || "\\" || sCaseNo || "\\" || sCaseYr || sCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || ".pdf"
.head 5 -  Set sFilePath = "\\\\192.168.38.5\\images\\OversizeDocuments\\DirectAttach\\" || sCaseYr || sCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || "X.pdf"
.head 5 -  ! Call SalReportPrintToFileEx ( wForm, sReportType, sFilePath,
	sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, 2, nPrintErr, 0, 0, 0, 1 )
.head 3 +  Function: CheckReprintStatus
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: sBarcode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nReturn
.head 5 -  Number: nFetchNum
.head 5 -  Number: nRatnum
.head 5 -  String: sReprintBarcode
.head 4 +  Actions
.head 5 +  If sReprintBarcode = "NOREPRINT"
.head 6 -  Call SalMessageBox("You can't reprint the document from here unless the case number on the barcode that is scanned matches the case number that you are working on.",
"Can't Reprint", MB_IconAsterisk)
.head 6 -  Return 0
.head 5 +  Else If sReprintBarcode = "NOREPRINTCLOSE"
.head 6 -  Return 0
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select ratnum
	from dotnet.barcodes  into :nRatnum 
	where barcode_number = :sBarcode")
.head 5 -  Call SqlFetchNext(hSql, nFetchNum)
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select count(*)
	from crim.docket  into :nReturn 
	where ratnum = :nRatnum")
.head 5 -  Call SqlFetchNext(hSql, nFetchNum)
.head 5 +  If nReturn = 0
.head 6 -  Call SalMessageBox("You can't reprint this document.  The Barcode number can not be found.

If you need to reprint the same document that was originally printed go to the docket and print the image that is attached.  If that document is incorrect you have to re-docket the entry.", "Can't 
Reprint", MB_IconAsterisk)
.head 6 -  Return 0
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select count(*)
	from crim.docket  INTO :nReturn 
	where ratnum = :nRatnum and
		((((to_char(entrydate, 'mm-dd-yyyy') = to_char(sysdate, 'mm-dd-yyyy') and to_char(entrydate, 'hh24') < 14) or
		(to_char(entrydate, 'mm-dd-yyyy') = to_char(sysdate-1, 'mm-dd-yyyy') and to_char(entrydate, 'hh24') >= 14)) and to_char(sysdate, 'hh24') < 14) or
		((to_char(entrydate, 'mm-dd-yyyy') = to_char(sysdate, 'mm-dd-yyyy') and to_char(entrydate, 'hh24') >= 14) and to_char(sysdate, 'hh24') >= 14))")
.head 5 -  Call SqlFetchNext(hSql, nFetchNum)
.head 5 +  If nReturn = 0
.head 6 -  Call SalMessageBox("You can't reprint this document. 

The system is preventing you from reprinting this document due to time constraints.

Please see your supervisor", "Can't Reprint", MB_IconAsterisk)
.head 6 +  If nULevel < 7
.head 7 -  Return 0
.head 6 +  Else
.head 7 -  Return 1
.head 5 -  Return nReturn
.head 3 +  Function: fProgramSecurity
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sPassProgram
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sRights
.head 5 -  Number: nProgramSecLevel
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, "Select seclevel
	from crim.app_rights into :nProgramSecLevel
	where app_name = :sPassProgram ")
.head 5 +  If Not SqlFetchNext( hSql, nReturn)
.head 6 -  Call SalMessageBox( 'No Security Entry for Program ' || sPassProgram, 'Missing Entry in the app_rights table', MB_Ok )
.head 6 -  Call SalDisableWindowAndLabel( hWndItem )
.head 6 -  Return FALSE
.head 5 +  If nULevel >= nProgramSecLevel 
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSql, 'SELECT rights
	   		    FROM crim.APP_RIGHTS_EXEPTIONS INTO :sRights
	   		    WHERE app_name = :sPassProgram and user_name = :SqlUser')
.head 6 +  If Not SqlFetchNext( hSql, nReturn)
.head 7 -  Return FALSE
.head 6 +  If sRights = 'E'
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Return FALSE
.head 3 +  ! Function: Select_Docket
.head 4 -  Description: 
.head 4 -  Returns 
.head 4 +  Parameters 
.head 5 -  Window Handle: hDocket
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  Number: nRatnum
.head 5 -  Number: n
.head 5 -  Number: nRow
.head 4 +  Actions 
.head 5 -  Set nRow = SalTblQueryContext( hDocket )
.head 5 -  Set nRatnum = hDocket.colRatnum
.head 5 +  While SalTblSetContext( hDocket, n )
.head 6 +  If hDocket.colRatnum = nRatnum
.head 7 -  Call SalTblSetRowFlags( hDocket, n, ROW_Selected, TRUE )
.head 6 +  Else 
.head 7 -  Call SalTblSetRowFlags( hDocket, n, ROW_Selected, FALSE )
.head 6 -  Set n = n + 1
.head 5 -  Call SalTblSetContext( hDocket, nRow )
.head 3 +  ! Function: SwitchCaseType
.head 4 -  Description: 
.head 4 -  Returns 
.head 4 +  Parameters 
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseNo
.head 5 -  String: sNewCaseTy
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: sOldCaseTy
.head 5 -  String: sOldPath
.head 5 -  String: sNewPath
.head 5 -  Number: nRatnum
.head 4 +  Actions 
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT casetype FROM casemaster WHERE caseyr = :sCaseYr and caseno = :sCaseNo INTO :sOldCaseTy")
.head 5 -  Call SqlFetchNext(hSql, nResult)
.head 5 -  Set sOldPath = "\\\\cmcfs1\\images\\" || sCaseYr || "\\CV" || sOldCaseTy || "\\" || sCaseNo || "\\"
.head 5 -  Set sNewPath = "\\\\cmcfs1\\images\\" || sCaseYr || "\\CV" || sNewCaseTy || "\\" || sCaseNo || "\\"
.head 5 -  Call SalFileCreateDirectory(sNewPath)
.head 5 -  Call VisFileRename (sOldPath || '*.pdf', sNewPath)
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT ratnum FROM dotnet.barcodes WHERE caseyr = :sCaseYr and caseno = :sCaseNo and division = 'CIVIL' INTO :nRatnum")
.head 5 +  While SqlFetchNext(hSql, nResult)
.head 6 -  Call VisFileRename (sNewPath || sCaseYr || sOldCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || ".pdf", sNewPath || sCaseYr || sNewCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || ".pdf")
.head 6 -  Call VisFileRename (sNewPath || sCaseYr || sOldCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || "R.pdf", sNewPath || sCaseYr || sNewCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || "R.pdf")
.head 5 -  Call SqlPrepareAndExecute(hSql, "UPDATE dotnet.barcodes SET casety = :sNewCaseTy WHERE caseyr = :sCaseYr and caseno = :sCaseNo and division = 'CIVIL'")
.head 5 -  Call SqlCommit(hSql)
.head 3 +  Function: SwitchCaseType
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseNewTy
.head 5 -  String: sCaseNo
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sCaseTy
.head 5 -  Number: nRatnum
.head 5 -  String: sToPath
.head 5 -  String: sFromPath
.head 5 -  Number: nResult
.head 4 +  Actions
.head 5 -  Set sCaseTy = ""
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT casety
				INTO :sCaseTy
				FROM dotnet.barcodes
				WHERE Caseyr = :sCaseYr and Caseno = :sCaseNo and division = 'CIVIL'")
.head 5 -  Call SqlFetchNext(hSql, nResult)
.head 5 +  If sCaseTy = ""
.head 6 -  ! Call SalMessageBox("There was an error with transfering the images or there were none to be transferred.", "Error", 0)
.head 5 +  Else If sCaseNewTy != sCaseTy
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT casety, ratnum
				INTO :sCaseTy, :nRatnum
				FROM dotnet.barcodes
				WHERE Caseyr = :sCaseYr and Caseno = :sCaseNo and division = 'CIVIL'")
.head 6 +  While SqlFetchNext(hSql, nResult)
.head 7 -  Set sFromPath = "\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseTy || "\\" || sCaseNo || "\\" || sCaseYr || sCaseTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0)
.head 7 -  Set sToPath = "\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseNewTy || "\\" || sCaseNo || "\\" || sCaseYr || sCaseNewTy || sCaseNo || "-" || SalNumberToStrX(nRatnum, 0)
.head 7 -  Call SalFileCreateDirectory("\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseNewTy)
.head 7 -  Call SalFileCreateDirectory("\\\\192.168.38.5\\images\\" || sCaseYr || "\\" || sCaseNewTy || "\\" || sCaseNo)
.head 7 -  Call VisFileRename(sFromPath || ".pdf", sToPath || ".pdf")
.head 7 -  Call VisFileRename(sFromPath || "R.pdf", sToPath || "R.pdf")
.head 7 -  ! Call VisFileDelete(sFromPath || ".pdf")
.head 7 -  ! Call VisFileDelete(sFromPath || "R.pdf")
.head 6 -  Call SqlPrepareAndExecute( hSql, "UPDATE Dotnet.Barcodes SET
	casety = :sCaseNewTy
	WHERE Caseyr = :sCaseYr and Caseno = :sCaseNo and division = 'CIVIL'")
.head 6 -  Call SqlCommit(hSql)
.head 6 -  Call SqlPrepareAndExecute( hSql, "Update crim.Certified SET
	casety = :sCaseNewTy
	WHERE Caseyr = :sCaseYr and casety = :sCaseTy and Caseno = :sCaseNo")
.head 6 -  Call SqlCommit(hSql)
.head 3 +  Function: SwitchDocketEntry
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sNewCaseYr
.head 5 -  String: sNewCaseNo
.head 5 -  String: sRowID
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sOldCaseTy
.head 5 -  String: sNewCaseTy
.head 5 -  String: sOldCaseYr
.head 5 -  String: sOldCaseNo
.head 5 -  String: sOldPath
.head 5 -  String: sNewPath
.head 5 -  Number: nRatnum
.head 5 -  Number: nDNum
.head 5 -  String: sCode5
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT caseyr, caseno, ratnum, dnum, code5 
	from crim.docket INTO :sOldCaseYr, :sOldCaseNo, :nRatnum, :nDNum, :sCode5
	where rowid = :sRowID")
.head 5 -  Call SqlFetchNext(hSql, nResult)
.head 5 -  Set sOldCaseNo = SalFmtFormatNumber( SalStrToNumber(sOldCaseNo), '00000' )
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT casetype FROM crim.casemaster WHERE caseyr = :sOldCaseYr and caseno = :sOldCaseNo INTO :sOldCaseTy")
.head 5 -  Call SqlFetchNext(hSql, nResult)
.head 5 +  If sNewCaseYr = sOldCaseYr and sNewCaseNo = sOldCaseNo
.head 6 -  Return 0
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT casetype FROM crim.casemaster WHERE caseyr = :sNewCaseYr and caseno = :sNewCaseNo INTO :sNewCaseTy")
.head 5 -  Call SqlFetchNext(hSql, nResult)
.head 5 -  Set sOldPath = "\\\\192.168.38.5\\images\\" || sOldCaseYr || "\\CV" || sOldCaseTy || "\\" || sOldCaseNo || "\\"
.head 5 -  Set sNewPath = "\\\\192.168.38.5\\images\\" || sNewCaseYr || "\\CV" || sNewCaseTy || "\\" || sNewCaseNo || "\\"
.head 5 -  Set sOldCaseTy = 'CV' || sOldCaseTy
.head 5 -  Set sNewCaseTy = 'CV' || sNewCaseTy
.head 5 -  Call SalFileCreateDirectory(sNewPath)
.head 5 -  Call VisFileRename (sOldPath || sOldCaseYr || sOldCaseTy || sOldCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || ".pdf", sNewPath || sNewCaseYr || sNewCaseTy || sNewCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || ".pdf")
.head 5 -  Call VisFileRename (sOldPath || sOldCaseYr || sOldCaseTy || sOldCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || "R.pdf", sNewPath || sNewCaseYr || sNewCaseTy || sNewCaseNo || "-" || SalNumberToStrX(nRatnum, 0) || "R.pdf")
.head 5 -  Call SqlPrepareAndExecute(hSql, "UPDATE dotnet.barcodes SET caseyr = :sNewCaseYr, casety = :sNewCaseTy, caseno = :sNewCaseNo WHERE ratnum = :nRatnum and division = 'CIVIL'")
.head 5 -  Call SqlCommit(hSql)
.head 5 -  Call SqlPrepareAndExecute( hSql, "Update crim.Certified SET
	caseyr = :sNewCaseYr, casety = :sNewCaseTy, caseno = :sNewCaseNo
	WHERE Caseyr = :sOldCaseYr and casety = :sOldCaseTy and Caseno = :sOldCaseNo and dnum = :nDNum and code5 = :sCode5")
.head 5 -  Call SqlCommit(hSql)
.head 3 +  Function: GetJailImagePath
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: fType
.head 5 -  Sql Handle: hSqlImage
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nReturnImage
.head 5 -  String: sLocation
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT	path
				   FROM		cr_image_location
				    WHERE	type = :fType
				    INTO		:sLocation')
.head 5 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 5 -  Return sLocation
.head 3 +  Function: fOkToExecute
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sPAppName
.head 5 -  ! Number: nPSecurityLevel
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nSecurityLevel
.head 5 -  String: sUserExeceptionRights
.head 4 +  Actions
.head 5 -  Set nSecurityLevel = fGetSecurityLevel( sPAppName )
.head 5 -  Set sUserExeceptionRights = fGetExecption( sPAppName )
.head 5 +  If nULevel < nSecurityLevel
.head 6 +  If sUserExeceptionRights != 'E'
.head 7 -  Return FALSE
.head 5 +  Else If sUserExeceptionRights = 'D'
.head 6 -  Return FALSE
.head 5 -  Return TRUE
.head 3 +  Function: fGetSecurityLevel
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: fAppName
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: spdTitle
.head 5 -  Number: nReturnSecLevel
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT seclevel
	FROM crim.app_rights into :nReturnSecLevel
	WHERE app_name = :fAppName ')
.head 5 +  If Not SqlFetchNext( hSql, nReturn)
.head 6 -  Call SalGetWindowText( hWndItem, spdTitle, 30 )
.head 6 -  Call SalMessageBox( 'No Security Entry for Push Button ' || spdTitle || ' - ' || fAppName, 'Missing Entry in the app_rights table', MB_Ok )
.head 6 -  Call SalDisableWindow( hWndItem )
.head 6 -  Return TRUE
.head 5 +  If nReturnSecLevel = NUMBER_Null
.head 6 -  Set nReturnSecLevel = 0
.head 5 -  Return nReturnSecLevel
.head 3 +  Function: fGetExecption
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: fAppName
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sRights
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT rights
	   		    FROM crim.APP_RIGHTS_EXEPTIONS INTO :sRights
	   		    WHERE app_name = :fAppName and user_name = :SqlUser')
.head 5 -  Call SqlFetchNext( hSql, nReturn)
.head 5 -  Return sRights
.head 2 -  Named Exceptions
.head 2 -  Named Toolbars
.head 2 +  Named Menus
.head 3 +  Menu: CTABLE_MENU
.head 4 -  Resource Id: 5490
.head 4 -  Picture File Name:
.head 4 -  Title:
.head 4 -  Description:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Export Table Data
.head 5 -  Resource Id: 5491
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Copy Selected Rows to Clipboard
.head 5 -  Resource Id: 5492
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Copy All Rows to Clipboard
.head 5 -  Resource Id: 5493
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Find
.head 5 -  Resource Id: 5494
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'FIND'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 2639999565
0000: 1C010000DA000000 0000000000000000 0200000200FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F19003C00 00FFFE00FF084300
0040: 72004400006F0063 006B006500007400 2200000001003C00 0019000500010210
0060: C71008000000F107 7B00C02500040000 0000018029000000 01003900FF630088
0080: 52006570006F0A72 7473009A00C00000 050000005B190001 A202FB10000002F1
00A0: 050033001C976800 0000190016040002 0210AF00F1F80600 3382001D00000004
00C0: F500022B00F10700 BE33006021000000 0400FD028A00F108 0033AF0025580000
00E0: 040002BF00E2F109 00330B00
.enddata
.head 3 +  Data Field Class: cCaseYr
.head 4 -  Data
.head 5 -  Maximum Data Length: 4
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalStrLength( MyValue ) = 1
.head 7 -  Set MyValue = '200' || MyValue
.head 6 +  If SalStrLength( MyValue ) = 2
.head 7 +  If MyValue > '49'
.head 8 -  Set MyValue = '19' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  Else
.head 8 -  Set MyValue = '20' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If SalStrLength( MyValue ) = 2 and (MyValue != '19' and MyValue !='20')
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'CRA'
.head 6 +  Else If MyValue = 'B'
.head 7 -  Set MyValue = 'CRB'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'TRC'
.head 6 +  Else If MyValue = 'D'
.head 7 -  Set MyValue = 'TRD'
.head 6 -  If MyValue = 'CRA' OR MyValue = 'CRB'
.head 6 -  Else If MyValue = 'TRC' OR MyValue = 'TRD'
.head 6 -  Else If MyValue = 'N'
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseVTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'CRA'
.head 6 +  Else If MyValue = 'B'
.head 7 -  Set MyValue = 'CRB'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'TRC'
.head 6 +  Else If MyValue = 'D'
.head 7 -  Set MyValue = 'TRD'
.head 6 -  If MyValue = 'CRA' OR MyValue = 'CRB' or MyValue = 'TRC' or MyValue = 'TRD' or
	MyValue = 'VBD' OR MyValue = 'VBB' or MyValue = 'N'
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseNo
.head 4 -  Data
.head 5 -  Maximum Data Length: 5
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.8"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If Not SalIsNull( MyValue )
.head 7 +  While SalStrLength( MyValue ) < 5
.head 8 -  Set MyValue = '0' || MyValue
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cDate
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_Backslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ( 'Date' )
.head 7 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If wParam = Key_Up
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue + 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 6 +  Else If wParam = Key_Down
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue - 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 3 +  Data Field Class: cDateDOB
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalDateYear( MyValue ) > SalDateYear( SalDateCurrent(  ) )
.head 7 -  Set MyValue = SalDateConstruct( SalDateYear( MyValue ) - 100, SalDateMonth( MyValue ), SalDateDay( MyValue ), SalDateHour( MyValue ), SalDateMinute( MyValue ), 0 )
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  ! If MyValue > SalDateCurrent( ) - 6480
.head 8 +  If IDOK =  SalMessageBox( 'Birth Date is Less than 18 Years, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else 
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  If MyValue > SalDateConstruct( SalDateYear( SalDateCurrent( ) ) - 18, SalDateMonth( SalDateCurrent( ) ), SalDateDay( SalDateCurrent( ) ), SalDateHour( SalDateCurrent( ) ), SalDateMinute( SalDateCurrent( ) ), SalDateSecond( SalDateCurrent( ) ) )
.head 8 -  If IDOK =  SalMessageBox( 'Birth Date is Less than 18 Years, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 3 +  Data Field Class: cLoginUser
.head 4 -  Data
.head 5 -  Maximum Data Length: 16
.head 5 -  Data Type: String
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Unformatted
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Window Handle: hWndNext
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Read )
.head 7 -  Call SalFileRead( hFLogin, sLogIn, 9 )
.head 7 -  Set SqlUser = SalStrTrimX( SalStrLeftX( sLogIn, 9 ) )
.head 7 -  Call SalFileClose( hFLogin )
.head 6 -  ! Set MyValue = SqlUser
.head 6 +  ! If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 6 -  Set SqlUser =ReadRegistryLastUser(  )
.head 6 -  Set MyValue = SqlUser
.head 6 +  If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 3 +  Data Field Class: cDfAutoTab
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  Class Default
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Class Default
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description: This data field class will automatically set focus
to the next object in the tab order when the
maximum length has been reached.
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nMaxLength
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set nMaxLength = SalGetMaxDataLength( hWndItem )
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = nMaxLength
.head 7 -  Call SalSendMsg( hWndForm, WM_NEXTDLGCTL, 0, 0 )
.head 3 +  ! Functional Class: Costs
.winattr
.end
.head 4 -  Description: 
.head 4 -  Derived From 
.head 4 -  Class Variables 
.head 4 +  Instance Variables 
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 5 -  Sql Handle: hSqlCost
.head 5 -  Boolean: bCostClass
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseNo
.head 5 -  String: fCaseTy
.head 4 +  Functions 
.head 5 +  Function: C_Insert_Costs
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: fCode
.head 7 -  Number: fCount
.head 7 -  String: fCity_State_Code	!!! This allows only 1 part of the group to be inserted
.head 7 -  Boolean: bCommit
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Date/Time: dCurrentDate
.head 7 -  String: sCodeAmt
.head 7 -  String: sCodeDesc
.head 7 -  String: sInsertCost
.head 7 -  String: sWhereC2S2Code   !!! Used to only select 1 code out of the group when inserting
.head 6 +  Actions 
.head 7 -  Set fCode = SalStrTrimX( fCode )
.head 7 -  Set dCurrentDate = Current_Date(  )
.head 7 -  Set sCodeAmt = C_Get_Primary_Charge_Type( )
.head 7 +  If sCodeAmt = 'C'
.head 8 -  Set sCodeAmt = 'STATE'
.head 8 -  Set sCodeDesc = 'DESCRIPTION2'
.head 8 +  If fCount = NUMBER_Null 
.head 9 -  Set fCount = C_Count_State ( )
.head 7 +  Else If sCodeAmt = 'D'
.head 8 -  Set sCodeAmt = 'CITY'
.head 8 -  Set sCodeDesc = 'DESCRIPTION'
.head 8 +  If fCount = NUMBER_Null 
.head 9 -  Set fCount = C_Count_City ( )
.head 7 +  Else 
.head 8 -  Call C_Error( 'Primary charge not found.' )
.head 8 -  Return FALSE
.head 7 +  If fCity_State_Code != ''
.head 8 -  Set sWhereC2S2Code = ' and '||sCodeAmt||" = '"||fCity_State_Code||"' "
.head 7 +  Else 
.head 8 -  Set sWhereC2S2Code = ' '
.head 7 -  Set sInsertCost = "INSERT INTO		cr_costs
				(caseyr, casety, caseno, description,
				 timestamped, title, costs, userid, ratnum)
			SELECT 		:fCaseYr, :fCaseTy, :fCaseNo, "|| sCodeDesc || ",
					:dCurrentDate ," || sCodeAmt || ", ((1 *cost) + ((:fCount - 1) * costx)), :nUserId, cr_costs_ratnum_seq.nextval
			 FROM		dock_code_costs
			 WHERE		code =:fCode and 
					((effective_end_date is null and effective_beg_date <= sysdate) or
                                			(effective_end_date >= sysdate and effective_beg_date < sysdate))"||' '
					||sWhereC2S2Code
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, sInsertCost)
.head 7 +  If bCommit
.head 8 -  Call C_Commit( )
.head 5 +  Function: C_Get_Primary_Charge_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'C'    -      STATE
		 'D'   -        CITY
		 '~'   -         NO PRIMARY CHARGE EXISTS
.head 6 +  Returns 
.head 7 -  String: sChargeType
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  String: sChargeType
.head 6 +  Actions 
.head 7 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	cr_charge
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  Set sChargeType = '~'
.head 7 -  Return sChargeType
.head 5 +  Function: C_Error
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: fErrorType
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 -  Call SalMessageBox( fErrorType, 'Error in Inserting Cost', MB_Ok )
.head 5 +  Function: C_Count_City
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: nCountCity
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountCity
.head 6 +  Actions 
.head 7 -  Set sSelect = "SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 -  Return nCountCity
.head 5 +  Function: C_Count_State
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: nCountState
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountState
.head 6 +  Actions 
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountState
.head 5 +  Function: C_Count_All
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: nCountAll
.head 6 +  Parameters 
.head 7 -  String: fCode
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountAll
.head 7 -  String: fCondition
.head 6 +  Actions 
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	condition
			               FROM	dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCondition')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If fCondition = ''
.head 8 -  Set fCondition = '1=1'
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 +  If nCountAll < 1
.head 8 -  ! Call C_Error( 'No Charges found for Case.' )
.head 8 -  Return FALSE
.head 7 +  Else 
.head 8 -  Return nCountAll
.head 5 +  Function: C_Count_Charge
.head 6 -  Description: -Count # of specific type of charge
	-Parameter is the type of charge ('MM', 'M1',...)
	-Returns count
.head 6 +  Returns 
.head 7 -  Number: nCountAll
.head 6 +  Parameters 
.head 7 -  String: fChargeType
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountCharge
.head 6 +  Actions 
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountCharge
.head 5 +  Function: Costs_Constructor
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: CaseYr
.head 7 -  String: CaseTy
.head 7 -  String: CaseNo
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 +  If bLogin = TRUE
.head 8 +  If bCostClass = FALSE
.head 9 -  Set bCostClass = SqlConnect( hSqlCost )
.head 9 -  Call SqlSetResultSet( hSqlCost, TRUE )
.head 8 -  Set nFetchResult = 0
.head 8 -  Set sSelect = ''
.head 8 -  Set fCaseYr = CaseYr
.head 8 -  Set fCaseNo = CaseNo
.head 8 -  Set fCaseTy = CaseTy
.head 5 +  Function: Costs_Destructor
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 +  If bCostClass = TRUE
.head 8 -  Call SqlDisconnect( hSqlCost )
.head 5 +  Function: C_Is_Moving
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  String: sMoving
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  String: sMoving
.head 6 +  Actions 
.head 7 -  Set sSelect = 'SELECT 	s2.moving
	       FROM		cr_charge s1,
			crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 5 +  Function: C_Cost_Exists
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: fCode	!!! Cost Code to Check for
		Pass in the code with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('N%', '', NUMBER_Null)
				The above will find all costs for the case where the
				title is like 'N%'
.head 7 -  String: fDesc	!!! Cost Description to check for
		  Pass in the description with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('NG', ':%TRIAL', NUMBER_Null)
				The above will find all costs for the case where the
				title is 'NG' and the description is like '%TRIAL'
.head 7 -  Number: fCost	!!! Cost Amount to check for
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  String: fWhere
.head 6 +  Actions 
.head 7 -  Set fWhere = 'WHERE	caseno = :fCaseNo and
			casety = :fCaseTy and
			caseyr = :fCaseYr  '
.head 7 +  If fCode != ''
.head 8 +  If SalStrLeftX(fCode, 1) = ':'
.head 9 -  Set fCode = SalStrReplaceX(fCode, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and title like :fCode '
.head 8 +  Else 
.head 9 -  Set fWhere = fWhere ||' and title = :fCode '
.head 7 +  If fDesc != ''
.head 8 +  If SalStrLeftX(fDesc, 1) = ':'
.head 9 -  Set fDesc = SalStrReplaceX(fDesc, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and description like :fDesc '
.head 8 +  Else 
.head 9 -  Set fWhere = fWhere ||' and description = :fDesc '
.head 7 +  If fCost != NUMBER_Null
.head 8 -  Set fWhere = fWhere ||' and costs = :fCost '
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, 'SELECT	caseno
			      FROM		cr_costs '||fWhere)
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else 
.head 8 -  Return FALSE
.head 5 +  Function: C_Commit
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Boolean: bReturn
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Boolean: bReturn
.head 6 +  Actions 
.head 7 -  Set bReturn  = SqlCommit( hSqlCost )
.head 7 -  Return bReturn
.head 3 +  Functional Class: Costs
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 5 -  Sql Handle: hSqlCost
.head 5 -  Boolean: bCostClass
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseNo
.head 5 -  String: fCaseTy
.head 4 +  Functions
.head 5 +  Function: C_Insert_Costs
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fCode
.head 7 -  Number: fCount
.head 7 -  Date/Time: dCompareDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dCurrentDate
.head 7 -  String: sCodeAmt
.head 7 -  String: sCodeDesc
.head 7 -  String: sInsertCost
.head 6 +  Actions
.head 7 -  Set fCode = SalStrTrimX( fCode )
.head 7 -  Set dCurrentDate = Current_Date( 'Date' )
.head 7 -  Set sCodeAmt = C_Get_Primary_Charge_Type( )
.head 7 +  If dCompareDate = DATETIME_Null
.head 8 -  Call SqlPrepareAndExecute(hSqlCost, 'Select sysdate
	from Dual into :dCompareDate')
.head 8 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 +  If sCodeAmt = 'C'
.head 8 -  Set sCodeAmt = 'STATE'
.head 8 -  Set sCodeDesc = 'DESCRIPTION2'
.head 8 +  If fCount = NUMBER_Null
.head 9 -  Set fCount = C_Count_State ( )
.head 7 +  Else If sCodeAmt = 'D'
.head 8 -  Set sCodeAmt = 'CITY'
.head 8 -  Set sCodeDesc = 'DESCRIPTION'
.head 8 +  If fCount = NUMBER_Null
.head 9 -  Set fCount = C_Count_City ( )
.head 7 +  Else
.head 8 -  Call C_Error( 'Primary charge not found.' )
.head 8 -  Return FALSE
.head 7 -  Set sInsertCost = "INSERT INTO		crim.cr_costs
				(caseyr, casety, caseno, description,
				 timestamped, title, costs, dock_codes, ratnum)
			SELECT 		:fCaseYr, :fCaseTy, :fCaseNo, "|| sCodeDesc || ",
					:dCurrentDate ," || sCodeAmt || ", ((1 *cost) + ((:fCount - 1) * costx)), code, cr_costs_ratnum_seq.nextval
			 FROM		crim.dock_code_costs
			 WHERE		code =:fCode and
					((effective_end_date is null and effective_beg_date <= :dCompareDate) or
                                			 (effective_end_date >= :dCompareDate and effective_beg_date <= :dCompareDate))"
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, sInsertCost)
.head 7 -  Call SqlCommit(hSqlCost)
.head 5 +  Function: C_Get_Primary_Charge_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'C'    -       STATE
		 'D'   -        CITY
		 '~'   -         NO PRIMARY CHARGE EXISTS
.head 6 +  Returns
.head 7 -  String: sChargeType
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sChargeType
.head 6 +  Actions
.head 7 +  If SalStrLeftX( fCaseTy, 1) = 'V'
.head 8 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	vb_ticket
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 +  Else
.head 8 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	cr_charge
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  Set sChargeType = '~'
.head 7 -  Return sChargeType
.head 5 +  Function: C_Error
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fErrorType
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SalMessageBox( fErrorType, 'Error in Inserting Cost', MB_Ok )
.head 5 +  Function: C_Count_City
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountCity
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCity
.head 6 +  Actions
.head 7 +  If SalStrLeftX( fCaseTy, 1) = 'V'
.head 8 -  Set sSelect = "SELECT	count(*)
	       FROM	crim.vb_ticket
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 +  Else
.head 8 -  Set sSelect = "SELECT	count(*)
	       FROM	crim.cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 -  Return nCountCity
.head 5 +  Function: C_Count_State
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountState
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountState
.head 6 +  Actions
.head 7 +  If SalStrLeftX( fCaseTy, 1) = 'V'
.head 8 -  Set sSelect = 'SELECT	count(*)
	       FROM	crim.vb_ticket
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 +  Else
.head 8 -  Set sSelect = 'SELECT	count(*)
	       FROM	crim.cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountState
.head 5 +  Function: C_Count_All
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountAll
.head 7 -  String: fCondition
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	condition
			               FROM	crim.dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCondition')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If fCondition = ''
.head 8 -  Set fCondition = '1=1'
.head 7 +  If SalStrLeftX( fCaseTy, 1) = 'V'
.head 8 -  Set sSelect = 'SELECT	count(*)
	       FROM	crim.vb_ticket
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 +  Else
.head 8 -  Set sSelect = 'SELECT	count(*)
	       FROM	crim.cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 +  If nCountAll < 1
.head 8 -  Call C_Error( 'No Charges found for Case.' )
.head 8 -  Return FALSE
.head 7 +  Else
.head 8 -  Return nCountAll
.head 5 +  Function: C_Count_Charge
.head 6 -  Description: -Count # of specific type of charge
	-Parameter is the type of charge ('MM', 'M1',...)
	-Returns count
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fChargeType
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCharge
.head 6 +  Actions
.head 7 +  If SalStrLeftX( fCaseTy, 1) = 'V'
.head 8 -  Set sSelect = 'SELECT	count(*)
	       FROM	crim.vb_ticket
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 +  Else
.head 8 -  Set sSelect = 'SELECT	count(*)
	       FROM	crim.cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountCharge
.head 5 +  Function: Costs_Constructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: CaseYr
.head 7 -  String: CaseNo
.head 7 -  String: CaseTy
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bLogin = TRUE
.head 8 +  If bCostClass = FALSE
.head 9 -  Set bCostClass = SqlConnect( hSqlCost )
.head 9 -  Call SqlSetResultSet( hSqlCost, TRUE )
.head 8 -  Set nFetchResult = 0
.head 8 -  Set sSelect = ''
.head 8 -  Set fCaseYr = CaseYr
.head 8 -  Set fCaseNo = CaseNo
.head 8 -  Set fCaseTy = CaseTy
.head 5 +  Function: Costs_Destructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bCostClass = TRUE
.head 8 -  Call SqlDisconnect( hSqlCost )
.head 5 +  Function: C_Is_Moving
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: sMoving
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sMoving
.head 6 +  Actions
.head 7 +  If SalStrLeftX( fCaseTy, 1) = 'V'
.head 8 -  Set sSelect = "SELECT 	s2.moving
	       FROM	crim.vb_ticket s1, crim.crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute || s1.statutecode = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc"
.head 7 +  Else
.head 8 -  Set sSelect = 'SELECT 	s2.moving
	       FROM	crim.cr_charge s1, crim.crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 3 +  Functional Class: CrDocket
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 4 +  Functions
.head 5 +  Function: D_Insert_Docket
.head 6 -  Description: Adds the Requested Data to the Cr_Docket
	- String (4):		Case Yr
	- String (3):		Case Ty
	- String (5):		Case No
	- Date/Time:	Docket Date
	- String (6):		Docket Code
	- String (250):	Docket Data
	- Number:		Seq (If NULL, will find max seq from Docket)
	- Number:		Seq2 (Always NULL, except on charge entry)
	- String:		Variable Mask
	- String:		Commit confirmation
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  Date/Time: DDate
.head 7 -  String: DCode
.head 7 -  String: DData
.head 7 -  Number: nSeq
.head 7 -  Number: nSeq2
.head 7 -  String: DMask
.head 7 -  String: sCommit
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnSeq
.head 6 +  Actions
.head 7 -  Set nReturnSeq =  D_Decode_Mask( DCaseYr, DCaseTy, DCaseNo, DCode, DMask)
.head 7 +  If nReturnSeq != 0
.head 8 -  Set nSeq = nReturnSeq
.head 7 +  If nSeq = 0 or nSeq = NUMBER_Null
.head 8 -  Call SalMessageBox( 'Warning: ' || DCode || ' Docket Code is being inserted without a sequence number', 'Docket Warning', MB_IconInformation | MB_Ok )
.head 7 -  Call SqlPrepareAndExecute( hSql, 'INSERT INTO	cr_docket
						(caseyr, casety, caseno, dock_date, seq,
						 seq2 ,casecode, data, userid, ratnum,Accesskey)
				VALUES		(:DCaseYr, :DCaseTy, :DCaseNo, :DDate, :nSeq,
						 :nSeq2, :DCode, :DData, :nUserId, cr_docket_ratnum_seq.nextval,sysdate )')
.head 7 +  If sCommit != 'N'
.head 8 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Docket_Exists
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute (hSql, 'SELECT	caseno
			      FROM		cr_docket '||fWhere)
.head 7 -  Call SqlFetchNext( hSql, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: D_Delete_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSql,'DELETE		cr_docket '||fWhere)
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Update_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 7 -  String: fDData
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSql,'UPDATE	cr_docket
			    SET		data =:fDData '||fWhere)
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Get_Max_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 6 +  Static Variables
.head 7 -  Number: fReturnSeq
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	max(seq)
			             FROM	cr_docket
			             WHERE	caseno =:DCaseNo and
					casety =:DCaseTy and
					caseyr =:DCaseYr
			              INTO	:fReturnSeq' )
.head 7 +  If Not SqlFetchNext( hSql, nReturn )
.head 8 -  Set fReturnSeq = 1
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	seq
			     FROM		new_codes
			     INTO		:fReturnSeq
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Decode_Mask
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: DCode
.head 7 -  String: DMask
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 +  ! If DMask = ''
.head 8 -  Set fReturnSeq = D_Get_Max_Seq( DCaseNo, DCaseTy, DCaseYr)
.head 8 -  Set fReturnSeq = fReturnSeq + 4
.head 7 +  If DMask = '#'
.head 8 -  Set fReturnSeq = D_Get_Seq(DCode)
.head 7 +  Else If DMask = ''
.head 8 -  Set fReturnSeq = fReturnSeq
.head 7 +  Else
.head 8 -  Set DMask = SalStrReplaceX( DMask, 0, 1, '')
.head 8 +  If SalStrIsValidNumber( DMask )
.head 9 +  If SalStrToNumber(DMask) > 0
.head 10 -  Set fReturnSeq = D_Get_Max_Seq( DCaseYr, DCaseTy, DCaseNo)
.head 10 -  Set fReturnSeq = fReturnSeq + SalStrToNumber(DMask)
.head 8 +  Else
.head 9 -  Call SalMessageBox('Error reading Mask string', 'Invalid Mask', MB_Ok|MB_IconExclamation)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Data
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	description
			     FROM		new_codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnData
.head 5 +  Function: D_Get_Costs
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	costs
			     FROM		new_codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnData
.head 3 +  Data Field Class: cDateHoliday
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Class Default
.head 4 -  Spell Check? Class Default
.head 4 -  Description: Checks to see if a date is a holiday
Parameter 1 - Date to Check
Parameter 2 - If True a Message is Displayed if the Date is a Holiday
Parameter 3 - If True Saturday & Sunday are flagged as Holidays
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  If HolidayCheck(MyValue, TRUE, TRUE)
.head 8 -  Call SalSetFocus(MyValue)
.head 7 +  Else
.head 8 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_Backslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ( 'Date' )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If wParam = Key_Up
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue + 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 6 +  Else If wParam = Key_Down
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue - 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 3 +  Child Table Class: CTable
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.2"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.833"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 8
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? Class Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 100000
.head 5 -  Discardable? No
.head 4 -  Next Class Child Key: 0
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? Class Default
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Description:
.head 4 +  Derived From
.head 5 -  Class: CWindow
.head 4 -  Contents
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Boolean: bDisableC
.head 4 +  Functions
.head 5 +  Function: SAM_CreateTBL
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call MTblSubClass( hWndForm )
.head 7 -  Call MTblEnableMWheelScroll( hWndForm, TRUE )
.head 7 +  If NOT bDisableC
.head 8 -  Call SalTblDefineRowHeader( hWndForm, '?', 20, 41, hWndNULL )
.head 7 -  Set hWndTable = hWndForm
.head 7 -  Call SalTblSetTableFlags( hWndForm, TBL_Flag_SelectableCols, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_TEXT, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_COMPLETETEXT, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_IMAGE, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_BTN, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_COLHDR, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_COLHDRGRP, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_ROWHDR, TRUE )
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CORNER, TRUE )
.head 7 -  Call MTblSetTipOpacity( hWndForm, TBL_Error, MTBL_TIP_DEFAULT, 220 )
.head 7 -  Call MTblSetTipFadeInTime( hWndForm, TBL_Error, MTBL_TIP_DEFAULT, 150 )
.head 5 +  Function: PrintTable
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Window Handle: hWndTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nError
.head 7 -  Number: nParamArray[*]
.head 6 +  Actions
.head 7 -  Call SalReportTableCreate( 'PrnTbl.qrp', hWndTable, nError )
.head 7 +  If fType = 'PRINTER'
.head 8 -  Call SalReportTablePrint( hWndTable, 'PrnTbl.qrp', nParamArray , nError )
.head 7 +  Else If fType = 'SCREEN'
.head 8 -  Call SalReportTableView( hWndTable, hWndNULL, 'PrnTbl.qrp' , nError )
.head 7 -  Call VisFileDelete(  'PrnTbl.qrp' )
.head 5 +  Function: AutoSize
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: hTable
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bFound
.head 7 -  Number: nColID
.head 7 -  Window Handle: hWndColumn
.head 6 +  Actions
.head 7 -  Set bFound = FALSE
.head 7 -  Set nColID = 1
.head 7 -  Set hWndColumn = SalTblGetColumnWindow( hTable, nColID, COL_GetID )
.head 7 +  While hWndColumn
.head 8 +  If SalTblQueryColumnFlags( hWndColumn, COL_Selected )
.head 9 -  Call VisTblAutoSizeColumn( hTable, hWndColumn )
.head 9 -  Set bFound = TRUE
.head 9 -  Break
.head 8 -  Set nColID = nColID + 1
.head 8 -  Set hWndColumn = SalTblGetColumnWindow( hTable, nColID, COL_GetID )
.head 7 +  If Not bFound
.head 8 -  Call VisTblAutoSizeColumn( hTable, hWndNULL )
.head 4 +  Message Actions
.head 5 +  On SAM_CacheFull
.head 6 -  Call AddMsgToLog( "SAM_CacheFull", wParam, lParam )
.head 5 +  On SAM_ColumnSelectClick
.head 6 -  Call AddMsgToLog( "SAM_ColumnSelectClick", wParam, lParam )
.head 5 +  On SAM_CornerClick
.head 6 -  Call AddMsgToLog( "SAM_CornerClick", wParam, lParam )
.head 6 +  If NOT bDisableC
.head 7 -  Call SalTrackPopupMenu(hWndForm, 'CTABLE_MENU', TPM_CursorX | TPM_CursorY, 0, 0 )
.head 5 +  On SAM_CornerDoubleClick
.head 6 -  Call AddMsgToLog( "SAM_CornerDoubleClick", wParam, lParam )
.head 5 +  On SAM_CountRows
.head 6 -  Call AddMsgToLog( "SAM_CountRows", wParam, lParam )
.head 5 +  On SAM_EndCellTab
.head 6 -  Call AddMsgToLog( "SAM_EndCellTab", wParam, lParam )
.head 5 +  On SAM_FetchDone
.head 6 -  Call AddMsgToLog( "SAM_FetchDone", wParam, lParam )
.head 5 +  On SAM_FetchRow
.head 6 -  Call AddMsgToLog( "SAM_FetchRow", wParam, lParam )
.head 5 +  On SAM_FetchRowDone
.head 6 -  Call AddMsgToLog( "SAM_FetchRowDone", wParam, lParam )
.head 5 +  On SAM_RowSetContext
.head 6 -  Call AddMsgToLog( "SAM_RowSetContext", wParam, lParam )
.head 5 +  On SAM_RowValidate
.head 6 -  Call AddMsgToLog( "SAM_RowValidate", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_First
.head 6 -  Call AddMsgToLog( "MTM_First", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_AreaLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_AreaLBtnDown", wParam, lParam )
.head 5 +  On MTM_AreaLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_AreaLBtnUp", wParam, lParam )
.head 5 +  On MTM_AreaLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_AreaLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_AreaRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_AreaRBtnDown", wParam, lParam )
.head 5 +  On MTM_AreaRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_AreaRBtnUp", wParam, lParam )
.head 5 +  On MTM_AreaRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_AreaRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_CornerLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_CornerLBtnDown", wParam, lParam )
.head 5 +  On MTM_CornerLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_CornerLBtnUp", wParam, lParam )
.head 5 +  On MTM_CornerLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_CornerLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_CornerRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_CornerRBtnDown", wParam, lParam )
.head 5 +  On MTM_CornerRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_CornerRBtnUp", wParam, lParam )
.head 5 +  On MTM_CornerRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_CornerRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_ColHdrLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrLBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrLBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_ColHdrRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrRBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrRBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_ColHdrSepLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepLBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrSepLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepLBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrSepLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepLBtnDblClk", wParam, lParam )
.head 6 -  Call MTblAutoSizeColumn( hWndForm, SalNumberToWindowHandle( wParam ), MTASC_SPLITROWS )
.head 5 +  On MTM_ColHdrSepRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepRBtnDown", wParam, lParam )
.head 5 +  On MTM_ColHdrSepRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepRBtnUp", wParam, lParam )
.head 5 +  On MTM_ColHdrSepRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_ColHdrSepRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_RowHdrLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_RowHdrLBtnDown", wParam, lParam )
.head 5 +  On MTM_RowHdrLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_RowHdrLBtnUp", wParam, lParam )
.head 5 +  On MTM_RowHdrLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_RowHdrLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_RowHdrRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_RowHdrRBtnDown", wParam, lParam )
.head 5 +  On MTM_RowHdrRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_RowHdrRBtnUp", wParam, lParam )
.head 5 +  On MTM_RowHdrRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_RowHdrRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_SplitBarLBtnDown
.head 6 -  Call AddMsgToLog( "MTM_SplitBarLBtnDown", wParam, lParam )
.head 5 +  On MTM_SplitBarLBtnUp
.head 6 -  Call AddMsgToLog( "MTM_SplitBarLBtnUp", wParam, lParam )
.head 5 +  On MTM_SplitBarLBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_SplitBarLBtnDblClk", wParam, lParam )
.head 5 +  On MTM_SplitBarRBtnDown
.head 6 -  Call AddMsgToLog( "MTM_SplitBarRBtnDown", wParam, lParam )
.head 5 +  On MTM_SplitBarRBtnUp
.head 6 -  Call AddMsgToLog( "MTM_SplitBarRBtnUp", wParam, lParam )
.head 5 +  On MTM_SplitBarRBtnDblClk
.head 6 -  Call AddMsgToLog( "MTM_SplitBarRBtnDblClk", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_ColMoved
.head 6 -  Call AddMsgToLog( "MTM_ColMoved", wParam, lParam )
.head 5 +  On MTM_ColSized
.head 6 -  Call AddMsgToLog( "MTM_ColSized", wParam, lParam )
.head 5 +  On MTM_Reset
.head 6 -  Call AddMsgToLog( "MTM_Reset", wParam, lParam )
.head 5 +  On MTM_RowDeleted
.head 6 -  Call AddMsgToLog( "MTM_RowDeleted", wParam, lParam )
.head 5 +  On MTM_RowInserted
.head 6 -  Call AddMsgToLog( "MTM_RowInserted", wParam, lParam )
.head 5 +  On MTM_RowsSwapped
.head 6 -  Call AddMsgToLog( "MTM_RowsSwapped", wParam, lParam )
.head 5 +  On MTM_SplitBarMoved
.head 6 -  Call AddMsgToLog( "MTM_SplitBarMoved", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_CollapseRow
.head 6 -  Call AddMsgToLog( "MTM_CollapseRow", wParam, lParam )
.head 5 +  On MTM_CollapseRowDone
.head 6 -  Call AddMsgToLog( "MTM_CollapseRowDone", wParam, lParam )
.head 5 +  On MTM_ExpandRow
.head 6 -  Call AddMsgToLog( "MTM_ExpandRow", wParam, lParam )
.head 5 +  On MTM_ExpandRowDone
.head 6 -  Call AddMsgToLog( "MTM_ExpandRowDone", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_QuerySortValue
.head 6 -  Return SalSendMsg( SalNumberToWindowHandle( wParam ), MTM_QuerySortValue, 0, lParam )
.head 5 -  !
.head 5 +  On MTM_BtnClick
.head 6 -  Call AddMsgToLog( "MTM_BtnClick", wParam, lParam )
.head 5 +  On MTM_HyperlinkClick
.head 6 -  Call AddMsgToLog( "MTM_HyperlinkClick", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_Paint
.head 6 -  Call AddMsgToLog( "MTM_Paint", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_LoadChildRows
.head 6 -  Call AddMsgToLog( "MTM_LoadChildRows", wParam, lParam )
.head 5 +  On SAM_User
.head 6 +  If sCTABLE_MENU_TYPE = 'EXPORT'
.head 7 -  Call SalModalDialog( dlgExport, hWndForm, hWndItem, fGlobTitle, fGlobFileName, '', '', '')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'EXPORT_SAVE_LOAD'
.head 7 -  Call SalModalDialog( dlgExport, hWndForm, hWndItem, fGlobTitle, fGlobFileName, 'EXCEL', '', '')
.head 6 +  Else If sCTABLE_MENU_TYPE = 'PRINT_TABLE'
.head 7 -  Call PrintTable( 'PRINTER' , hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'VIEW_TABLE'
.head 7 -  Call PrintTable( 'SCREEN' , hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 7 -  Call CopyRows( 1, hWndItem )
.head 6 +  Else If sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 7 -  Call CopyRows( 2, hWndItem )
.head 6 +  Else If sCTABLE_MENU_TYPE = 'FIND'
.head 7 -  Call SalCreateWindow( frmCTable_Find, hWndForm, hWndItem)
.head 6 +  Else If sCTABLE_MENU_TYPE = 'AUTO_SIZE'
.head 7 -  Call AutoSize( hWndItem  )
.head 3 +  General Window Class: CWindow
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 +  Functions
.head 5 +  Function: AddMsgToLog
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: spMsg
.head 7 -  Number: npwParam
.head 7 -  Number: nplParam
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sItemName
.head 6 +  Actions
.head 7 +  ! If frmMain.whWndMsgLog
.head 8 -  Call SalGetItemName( hWndItem, sItemName )
.head 8 -  Return dlgMsgLog.AddToLog( spMsg || "[" || sItemName || "], wParam: " || SalNumberToStrX( npwParam, 0 ) || ", lParam: " || SalNumberToStrX( nplParam, 0 ) )
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call AddMsgToLog( "SAM_Click", wParam, lParam )
.head 5 +  On SAM_ContextMenu
.head 6 -  Call AddMsgToLog( "SAM_ContextMenu", wParam, lParam )
.head 5 +  On SAM_DoubleClick
.head 6 -  Call AddMsgToLog( "SAM_DoubleClick", wParam, lParam )
.head 5 +  On SAM_DropDown
.head 6 -  Call AddMsgToLog( "SAM_DropDown", wParam, lParam )
.head 5 +  On SAM_KillFocus
.head 6 -  Call AddMsgToLog( "SAM_KillFocus", wParam, lParam )
.head 5 +  On SAM_SetFocus
.head 6 -  Call AddMsgToLog( "SAM_SetFocus", wParam, lParam )
.head 5 -  !
.head 5 +  On MTM_KeyDown
.head 6 -  Call AddMsgToLog( "MTM_KeyDown", wParam, lParam )
.head 5 -  !
.head 5 +  ! On WM_KEYDOWN
.head 6 -  Call AddMsgToLog( "WM_KEYDOWN", wParam, lParam )
.head 5 +  ! On WM_LBUTTONDOWN
.head 6 -  Call AddMsgToLog( "WM_LBUTTONDOWN", wParam, lParam )
.head 5 +  ! On WM_LBUTTONUP
.head 6 -  Call AddMsgToLog( "WM_LBUTTONUP", wParam, lParam )
.head 3 +  Column Class: CColumn
.head 4 -  Title:
.head 4 -  Visible? Class Default
.head 4 -  Editable? Class Default
.head 4 -  Maximum Data Length: Class Default
.head 4 -  Data Type: Class Default
.head 4 -  Justify: Class Default
.head 4 -  Width:  Class Default
.head 4 -  Width Editable? Class Default
.head 4 -  Format: Class Default
.head 4 -  Country: Class Default
.head 4 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Cell Options
.head 5 -  Cell Type? Class Default
.head 5 -  Multiline Cell? Class Default
.head 5 -  Cell DropDownList
.head 6 -  Sorted? Class Default
.head 6 -  Vertical Scroll? Class Default
.head 6 -  Auto Drop Down? Class Default
.head 6 -  Allow Text Editing? Class Default
.head 5 -  Cell CheckBox
.head 6 -  Check Value:
.head 6 -  Uncheck Value:
.head 6 -  Ignore Case? Class Default
.head 4 -  ToolTip:
.head 4 -  Column Aggregate Type: Class Default
.head 4 -  Flow Direction: Class Default
.head 4 -  Description:
.head 4 +  Derived From
.head 5 -  Class: CEditWindow
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 +  Functions
.head 5 +  Function: OnQuerySortValue
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dt
.head 7 -  Number: nColType
.head 7 -  Number: nDataType
.head 7 -  String: sText
.head 6 +  Actions
.head 7 +  If SalTblQueryColumnCellType( hWndItem, nColType )
.head 8 +  If nColType = COL_CellType_DropDownList
.head 9 -  Return SalListQuerySelection( hWndItem )
.head 7 -  !
.head 7 -  Set nDataType = SalGetDataType( hWndItem )
.head 7 -  Call SalTblGetColumnText( hWndForm, SalTblQueryColumnID( hWndItem ), sText )
.head 7 +  Select Case nDataType
.head 8 +  Case DT_String
.head 9 -  Return SalStrLength( sText )
.head 8 +  Case DT_DateTime
.head 9 -  Set dt = SalStrToDate( sText )
.head 9 -  Return dt - 0000-01-01
.head 8 +  Case DT_Number
.head 9 -  Return SalStrToNumber( sText )
.head 8 +  Default
.head 9 -  Return 0
.head 4 -  List Values
.head 4 +  Message Actions
.head 5 +  On MTM_QuerySortValue
.head 6 -  Return OnQuerySortValue( lParam )
.head 5 +  On SAM_ColumnSelectClick
.head 6 +  If bSortTblCol = FALSE
.head 7 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( hWndItem ), TBL_SortIncreasing )
.head 7 -  Set bSortTblCol = TRUE
.head 6 +  Else
.head 7 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( hWndItem ), TBL_SortDecreasing )
.head 7 -  Set bSortTblCol =FALSE
.head 3 +  General Window Class: CEditWindow
.head 4 -  Description:
.head 4 +  Derived From
.head 5 -  Class: CWindow
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 -  Call AddMsgToLog( "SAM_AnyEdit", wParam, lParam )
.head 5 +  On SAM_Validate
.head 6 -  Call AddMsgToLog( "SAM_Validate", wParam, lParam )
.head 3 +  Functional Class: cReports
.head 4 -  Description:
.head 4 -  Derived From
.head 4 +  Class Variables
.head 5 -  Boolean: bPrinting
.head 5 -  String: sCurReport
.head 5 -  String: sDefaultDevice
.head 5 -  String: sDefaultDriver
.head 5 -  String: sDefaultPort
.head 4 +  Instance Variables
.head 5 -  String: sDialog
.head 5 -  Window Handle: hDialog
.head 5 -  !
.head 5 -  String: sPOCaseNum
.head 5 -  String: sPOName
.head 5 -  String: sPOAddress
.head 5 -  String: sPOCity
.head 5 -  String: sPOJudge
.head 5 -  String: sPOOfficer
.head 5 -  String: sPOReason2
.head 5 -  String: sPOSex
.head 5 -  String: sPORace
.head 5 -  String: sPOHeight
.head 5 -  String: sPOWeight
.head 5 -  String: sPOHair
.head 5 -  String: sPOEyes
.head 5 -  String: sPOScars
.head 5 -  String: sPOSSNo
.head 5 -  String: sPOPhone1
.head 5 -  String: sPOPhone2
.head 5 -  String: sPOMarried
.head 5 -  String: sPOEmployed
.head 5 -  String: sPOEmployer
.head 5 -  String: sPOEmpPhone
.head 5 -  String: sPOEducation
.head 5 -  String: sPOPAddress 
.head 5 -  String: sPOPhone
.head 5 -  Long String: sPOComments
.head 5 -  String: sPOCo_Def
.head 5 -  String: sPOViolent
.head 5 -  String: sPOFelonies
.head 5 -  String: sPOCharges
.head 5 -  String: sPOAgency
.head 5 -  String: sPOClassification
.head 5 -  Date/Time: dPOAdm
.head 5 -  Date/Time: dPORelease
.head 5 -  Date/Time: dPODOB
.head 5 -  Number: nPOCosts
.head 5 -  Number: nPORestitution
.head 5 -  Number: nPOJailDays
.head 5 -  String: sPOParents
.head 5 -  String: sPOSpouse
.head 5 -  String: sPORelation
.head 5 -  String: sPOLivesWith
.head 5 -  String: sPODependants
.head 5 -  String: sPOEmp_Supervisor
.head 5 -  String: sPOEmp_Position
.head 5 -  String: sPOEmp_Schedule
.head 5 -  String: sPOEmp_Former
.head 5 -  String: sPOEmp_Phone
.head 5 -  String: sPOEmp_Address
.head 5 -  String: sPOAlerts
.head 5 -  String: sPOConditions
.head 4 +  Functions
.head 5 +  Function: Set_Printer
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sName
.head 7 -  String: sDriver
.head 7 -  String: sPort
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SalPrtGetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 7 -  Call SalPrtSetDefault( sName, sDriver, sPort )
.head 5 +  Function: Set_Dialog
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: spDialog
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sDialog = spDialog
.head 5 +  Function: Reset_Printer
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If sDefaultDevice != STRING_Null
.head 8 -  Call SalPrtSetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 8 -  Set sDefaultDevice = STRING_Null
.head 8 -  Set sDefaultDriver = STRING_Null
.head 8 -  Set sDefaultPort = STRING_Null
.head 5 +  Function: Print_Report
.head 6 -  Description: Prints the Report
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sReport
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 7 -  Number: nCopies
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nResult
.head 6 +  Actions
.head 7 +  If sDialog = STRING_Null
.head 8 -  Set sDialog = "dlgPrintReport"
.head 7 -  ! Wait until done printing before starting the next report
.head 7 +  If bPrinting
.head 8 -  Call SalYieldStartMessages( hDialog )
.head 8 -  While bPrinting
.head 8 -  Call SalYieldStopMessages( )
.head 7 -  Set sCurReport = sReport
.head 7 -  Set hDialog = SalCreateWindow( sDialog, hWndNULL )
.head 7 -  Call SalReportPrint( hDialog, sReport, sVars, sInputs, nCopies, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nResult )
.head 7 -  Return ( nResult = 0 )
.head 5 +  Function: View_Report
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sReport
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nFlags
.head 7 -  Window Handle: hDialog
.head 6 +  Actions
.head 7 +  If sDialog = STRING_Null
.head 8 -  Set sDialog = "dlgPrintReport"
.head 7 -  ! Wait until done printing before starting the next report
.head 7 +  If bPrinting
.head 8 -  Call SalYieldStartMessages( hDialog )
.head 8 -  While bPrinting
.head 8 -  Call SalYieldStopMessages( )
.head 7 -  Set sCurReport = sReport
.head 7 -  Set hDialog = SalCreateWindow( sDialog, hWndNULL )
.head 7 -  Call SalReportView( hDialog, hWndNULL, sReport, sVars, sInputs, nFlags )
.head 5 +  Function: Print_DFA
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bViewOnly
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 +  Actions
.head 7 -  Set sVars = "Reports.DFA.sCaseNo, Reports.DFA.sJuvenile, Reports.DFA.nOccurance, Reports.DFA.dArnDate"
.head 7 -  Set sInputs = "CASENO, JUVENILE, OCCURANCE, ARN_DATE"
.head 7 +  If bViewOnly
.head 8 -  Call View_Report( "JU_TRA_DFA.QRP", sVars, sInputs )
.head 7 +  Else
.head 8 -  Call Print_Report( "JU_TRA_DFA.QRP", sVars, sInputs, 1 )
.head 5 +  Function: Print_Probation_Offender
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sName
.head 7 -  Number: nCosts
.head 7 -  Number: nRestitution
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sCharges
.head 7 -  Number: nDefId
.head 7 -  Number: nCaseYr
.head 7 -  Number: nCaseNo
.head 7 -  String: sNoDashSSNo
.head 6 +  Actions
.head 7 -  Set sPOName = sName
.head 7 -  Call SalCreateWindow( dlgPrintPicture, hWndNULL )
.head 7 -  Set sPOCaseNum = sCaseYr || '-' || sCaseTy || '-' || sCaseNo
.head 7 -  Set nCaseYr = SalStrToNumber( sCaseYr )
.head 7 -  Set nCaseNo = SalStrToNumber( sCaseNo )
.head 7 -  Call SqlPrepareAndExecute( hSql, "Select sex, color, dob, height, weight, hair, eyes, 
		scars, ssno, employer_phone, education, substr(ssno, 1,3) || substr(ssno, 5,2) || substr(ssno,8,4) 
	 from crim.cr_parties INTO :sPOSex, :sPORace, :dPODOB, :sPOHeight, :sPOWeight, :sPOHair, :sPOEyes, 
		:sPOScars, :sPOSSNo, :sPOEmpPhone, :sPOEducation, :sNoDashSSNo
	where caseyr=:sCaseYr and casety=:sCaseTy and caseno=:sCaseNo" )
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  !
.head 7 -  Call SqlPrepareAndExecute( hSql, "Select co_def, violent, felonies, first_name || ' ' || mid_name || ' ' || last_name, c.def_id, c.Conditions
	from crim.tr_case c, judge j into :sPOCo_Def, :sPOViolent, :sPOFelonies, :sPOJudge, :nDefId, :sPOConditions
	where caseyr=:nCaseYr and casety=:sCaseTy and caseno=:nCaseNo and c.judge=j.judge_number(+)" )
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  !
.head 7 -  Call SqlPrepareAndExecute( hSql, "Select c.name, p.pr_direct, p.pr_adm, p.pr_classification, p.pr_release, pr_jail 
	from crim.tr_probation p, tr_contact c into :sPOOfficer, :sPOAgency, :dPOAdm, :sPOClassification, :dPORelease, :nPOJailDays 
	where p.pr_contact=c.num and caseyr=:nCaseYr and casety=:sCaseTy and caseno=:nCaseNo" )
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  !
.head 7 -  Set sPOCharges = STRING_Null
.head 7 -  Call SqlPrepareAndExecute( hSql, "Select Statute || ' (' || Degree || ') ' || Description 
	from crim.cr_charge c into :sCharges 
	where caseyr=:sCaseYr and casety=:sCaseTy and caseno=:sCaseNo
	order by chargeno" )
.head 7 +  While SqlFetchNext( hSql, nResult )
.head 8 +  If sPOCharges != STRING_Null
.head 9 -  Set sPOCharges = sPOCharges || '
'
.head 8 -  Set sPOCharges = sPOCharges || sCharges
.head 7 -  !
.head 7 -  Call SqlPrepareAndExecute( hSql, "SELECT marital_stat, Phone, education, employed, 
		employer, parent_addr || ' ' || parent_city || ' ' || parent_state || ' ' || parent_zip, parent_phone, comments, LivesWith, 
		CellPhone, Parents, Spouse, Relation, Dependants, Emp_Supervisor, Emp_Position, 
		Emp_Schedule, Emp_Former, Emp_Phone, Emp_Address || ' ' || Emp_City, Alerts  
  	from crim.tr_defendant  INTO :sPOMarried, :sPOPhone1, :sPOEducation, :sPOEmployed,
          		:sPOEmployer, :sPOPAddress, :sPOPhone, :sPOComments, :sPOLivesWith, 
		:sPOPhone2, :sPOParents, :sPOSpouse, :sPORelation, :sPODependants, :sPOEmp_Supervisor, :sPOEmp_Position, 
		:sPOEmp_Schedule, :sPOEmp_Former, :sPOEmp_Phone, :sPOEmp_Address, :sPOAlerts 
  	WHERE id = :nDefId")
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 +  If nResult = FETCH_Ok
.head 8 +  If sPOMarried = 'S'
.head 9 -  Set sPOMarried = 'Single'
.head 8 +  Else If sPOMarried = 'M'
.head 9 -  Set sPOMarried = 'Married'
.head 8 +  Else If sPOMarried = 'D'
.head 9 -  Set sPOMarried = 'Divorced'
.head 8 +  Else If sPOMarried = 'P'
.head 9 -  Set sPOMarried = 'Separated'
.head 7 -  !
.head 7 +  If SalStrLength(sPOSSNo) > 8
.head 8 -  Call SqlPrepareAndExecute (hSql, "Select Address1, City || ' ' || State || ' ' ||  Zip, DECODE(areacode, null, Phone, AreaCode || '-' || Phone)
	from crim.cr_address into :sPOAddress, :sPOCity, :sPOPhone1 
	where ssno = :sPOSSNo
	order by entrydate desc")
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute (hSql, "Select Address1, City || ' ' || State || ' ' ||  Zip, DECODE(areacode, null, Phone, AreaCode || '-' || Phone)
	from crim.cr_address into :sPOAddress, :sPOCity, :sPOPhone1 
	where caseyr=:sCaseYr and casety=:sCaseTy and caseno=:sCaseNo
	order by entrydate desc")
.head 7 -  Call SqlFetchNext(hSql, nResult )
.head 7 -  !
.head 7 +  If nCosts = NUMBER_Null 
.head 8 -  Set nPOCosts = fTotalFinesCosts(sCaseYr, sCaseTy, sCaseNo)
.head 7 +  Else
.head 8 -  Set nPOCosts = nCosts
.head 7 +  If nRestitution = NUMBER_Null
.head 8 -  Call SqlPrepareAndExecute( hSql, 'Select sum(nvl(v_owed, 0)) - sum(nvl(v_paid, 0))
	from crim.tr_victim into :nPORestitution
	where caseyr=:nCaseYr and casety=:sCaseTy and caseno=:nCaseNo'  )
.head 8 -  Call SqlFetchNext( hSql, nResult )
.head 7 +  Else
.head 8 -  Set nPORestitution = nRestitution
.head 7 -  !
.head 7 +  If SalStrLength( sNoDashSSNo ) = 9
.head 8 -  Call GetJailPhoto( sNoDashSSNo, sJPhoto, sJPhotoDate, dJPhotoDate, dlgPrintPicture.pJPhoto )
.head 7 +  Else
.head 8 -  Set sJPhoto = STRING_Null
.head 8 -  Set sJPhotoDate = STRING_Null
.head 8 -  Set dJPhotoDate = DATETIME_Null
.head 7 -  ! !
.head 7 -  Set sReport = CV_REPORT_Path || 'ProbOffender.qrp'
.head 7 -  Set sReportBinds = 'cReports.sPOCaseNum, sPOName, sPOAddress, sPOCity, sPOJudge, 
	sPOSex, sPORace, dPODOB, sPOHeight, sPOWeight, sPOHair, sPOEyes, nPOCosts, nPORestitution, nPOJailDays, 
	sPOScars, sPOSSNo, sPOPhone1, sPOPhone2, sPOMarried, sPOEmployed, sPOEmployer, sPOEmpPhone, sPOEducation,
	sPOPAddress, sPOPhone, sPOComments, sPOCo_Def, sPOViolent, sPOFelonies, sPOCharges, sPOAlerts, 
	dPOAdm, sPOClassification, dPORelease,  sPOMarried, sPOEducation, sPOPAddress, sPOPhone, 
	sPOReason2, sPOConditions, dPOAdm, sPOOfficer, sJPhoto, sJPhotoDate, sPOParents, sPOSpouse, sPORelation, 
	sPOLivesWith, sPODependants, sPOEmp_Supervisor, sPOEmp_Position, sPOEmp_Schedule, sPOEmp_Former, sPOEmp_Phone, sPOEmp_Address, sCourt'
.head 7 -  Set sReportInputs= 'CASENO, DEFENDANT, ADDRESS, CITY, JUDGE,
	Sex, Race, DOB, Height, Weight, Hair, Eyes, ProbCosts, ProbRestitution, ProbJailDays, 
	Scars, SSNo, Phone1, Phone2, Married, Employed, Employer, EmpPhone, Education,
	PAddress, PPhone, Comments, Co_Def, Violent, Felonies, Charges, Alerts, 
	ProbAdm, ProbClassification, ProbRelease, Married, Education, PAddress, PPhone, 
	PROGRAM, Conditions, ProbationDate, Officer, Photo, PhotoDate, Parents, Spouse, Relation, 
	LivesWith, Dependants, Emp_Supervisor, Emp_Position, Emp_Schedule, Emp_Former, Emp_Phone, Emp_Address, Court'
.head 7 -  Call Print_Report( sReport, sReportBinds, sReportInputs, 1)
.head 7 -  Call SalDestroyWindow( dlgPrintPicture )
.head 3 +  Functional Class: cImage
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  ! String: sImageFileName
.head 5 -  String: sPrtDeviceSave
.head 5 -  String: sPrtDriverSave
.head 5 -  String: sPrtPortSave
.head 5 -  String: sScriptFileName
.head 4 +  Functions
.head 5 +  Function: Construct
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fFileName
.head 7 -  String: fDirName
.head 7 -  Boolean: bSetToDefault   !If the qrp needs to print to default, this will set TIFF to the default
.head 7 -  Boolean: bOverwrite  !Usually true, if False, will append
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bSetToDefault
.head 8 -  Call SalPrtGetDefault( sPrtDeviceSave, sPrtDriverSave, sPrtPortSave )
.head 8 -  Call SalPrtSetDefault( 'TIFF', 'winspool', 'PNTIF7' )
.head 7 -  Set sScriptFileName = 'PNETIF7S.INI'
.head 7 -  !
.head 7 +  If NOT CreateINI( fFileName, fDirName, bOverwrite)
.head 8 -  Call SalMessageBox( 'Errors occurred while creating INI File.', 'Error', MB_Ok)
.head 5 +  Function: Destruct
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bSetToDefault   !If the qrp needs to print to default, this will set TIFF to the default
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bSetToDefault
.head 8 -  Call SalPrtSetDefault( sPrtDeviceSave, sPrtDriverSave, sPrtPortSave )
.head 5 +  Function: CreateINI
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: fFileName
.head 7 -  String: fDirectory
.head 7 -  Boolean: bOverwrite  !Usually true, if False, will append
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sTempDir
.head 7 -  String: sOverwrite
.head 7 -  String: sAppend
.head 6 +  Actions
.head 7 -  ! Get Temp Directory
.head 7 -  Set sTempDir = VisDosGetEnvString ('TEMP')
.head 7 -  ! Write Script Strings
.head 7 +  If sTempDir = ''
.head 8 -  Call SalMessageBox( 'Error in retrieving user temp directory - eTiff Creation failed to write ini file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Output directory', fDirectory, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Output directory

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Output filename', fFileName, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Output filename

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Prompt', '0', sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Prompt

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If bOverwrite
.head 8 -  Set sOverwrite = '1'
.head 8 -  Set sAppend = '0'
.head 7 +  Else
.head 8 -  Set sOverwrite = '0'
.head 8 -  Set sAppend = '1'
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Overwrite',sOverwrite, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Overwrite

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 +  If NOT SalSetProfileString ( 'Save', 'Append', sAppend, sTempDir||'\\' || sScriptFileName )
.head 8 -  Call SalMessageBox( 'Error in writing to INI file - Append

eTiff Creation failed to write INI file.
Please contact your programmer.', 'Error - INI File Creation', MB_IconStop| MB_Ok)
.head 8 -  Return FALSE
.head 7 -  Return TRUE
.head 5 +  Function: MoveFiles
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: fPath1
.head 7 -  String: fPath2
.head 7 -  Boolean: bShowErrors ! Report errors interactively during processing.  If FALSE, function will only return a TRUE or FALSE when complete.
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sPath1
.head 7 -  String: sPath2
.head 7 -  Number: nReturn
.head 7 -  Number: nFilesCopied
.head 6 +  Actions
.head 7 +  If SalStrRightX( fPath1, 1 ) != '\\'
.head 8 -  Set fPath1 = fPath1 ||'\\'
.head 7 +  If SalStrRightX( fPath2, 1 ) != '\\'
.head 8 -  Set fPath2 = fPath2 ||'\\'
.head 7 +  If NOT VisDosExist( fPath1 )
.head 8 +  If bShowErrors
.head 9 -  Call SalMessageBox( 'Original Path - '|| fPath1 ||' does not exist.  Please verify and try again.', 'Path not found', MB_IconStop|MB_Ok)
.head 8 -  Return -1
.head 7 +  If VisDosExist( fPath2 )
.head 8 +  If bShowErrors
.head 9 -  If SalMessageBox( 'Copy to Path - '|| fPath2 ||' already exists.
If you continue, you will overwrite any files within this directory that have the same name as the Original directory  Would you like to continue coping?', 'Copy to Path already exists',
MB_IconQuestion|MB_YesNo) = IDYES
.head 9 +  Else
.head 10 -  Return -2
.head 7 +  Else
.head 8 +  If NOT SalFileCreateDirectory( fPath2 )
.head 9 +  If bShowErrors
.head 10 -  Call SalMessageBox( 'An error occurred while creating Copy to path. Please contact suprevisor.', 'Copy to directory not created', MB_IconStop|MB_Ok)
.head 9 -  Return -3
.head 7 -  ! Set sCommand = 'c:\\windows\\system32\\xcopy.exe '|| fPath1 ||' '|| fPath2 ||' /Y/D/E/I/C//K'
.head 7 -  ! 
xcopy E:\MyDocu~1\email G:\Backup\MyDocuments\email /Y/D/E
.head 7 -  ! If SalLoadAppAndWait( sCommand, Window_NotVisible, nReturn )
.head 7 -  Set sPath1 = fPath1||'*.*'
.head 7 -  Set sPath2 = fPath2||'*.*'
.head 7 -  Set nFilesCopied = VisFileCopy(sPath1, fPath2 )
.head 7 +  If nFilesCopied < 1
.head 8 +  If bShowErrors
.head 9 -  Call SalMessageBox( 'An error occurred while coping files, or no files exist in Original directory.  Please contact suprevisor.', 'Files not copied', MB_IconStop|MB_Ok)
.head 8 -  Return -4
.head 7 +  Else
.head 8 -  Set nReturn = VisFileDelete( sPath1 )
.head 8 +  If nReturn < 1
.head 9 +  If bShowErrors
.head 10 -  Call SalMessageBox( 'An error occurred while deleting files from Original directory.  Please contact suprevisor.', 'Files not deleted', MB_IconStop|MB_Ok)
.head 9 -  Return -5
.head 8 +  If SalFileRemoveDirectory(  fPath1 ) = FALSE
.head 9 +  If bShowErrors
.head 10 -  Call SalMessageBox( 'An error occurred while removing Original directory.  Please contact suprevisor.', 'Original Direcotry not removed', MB_IconStop|MB_Ok)
.head 9 -  Return -6
.head 8 +  Else
.head 9 -  Return nFilesCopied
.head 5 +  Function: InsertLog
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Sql Handle: hSqlImage
.head 7 -  String: fCaseYr
.head 7 -  String: fCaseTy
.head 7 -  String: fCaseNo
.head 7 -  String: fOriginalPath
.head 7 -  String: fNewPath
.head 7 -  Number: fErrorCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nNumCopied
.head 6 +  Actions
.head 7 +  If fErrorCode > 0
.head 8 -  Set nNumCopied = fErrorCode
.head 8 -  Set fErrorCode = NUMBER_Null
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'INSERT INTO	cr_image_log
						(caseyr,casety, caseno, original_path, new_path, error_code, num_file_copied)
				     values	(:fCaseYr, :fCaseTy, :fCaseNo, :fOriginalPath, :fNewPath, :fErrorCode, :nNumCopied)')
.head 7 -  Call SqlCommit( hSqlImage )
.head 3 +  Pushbutton Class: cLoadApp
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left:
.head 5 -  Top:
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Class Default
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Keyboard Accelerator: Class Default
.head 4 -  Font Name: Class Default
.head 4 -  Font Size: Class Default
.head 4 -  Font Enhancement: Class Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: Class Default
.head 4 -  Image Style: Class Default
.head 4 -  Text Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Button Appearance: Class Default
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Class Default
.head 4 -  Text Alignment: Class Default
.head 4 -  Text Image Relation: Class Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Description: This class can be used to load an application.  It will check the server to determin
				that the local copy of the application is the most current
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sAppName
.head 5 -  Number: nSecurityLevel
.head 5 -  String: sUserExeceptionRights
.head 5 -  String: sApplication_Type
.head 5 -  String: sAppNameLocal
.head 5 -  String: sAppDriveL
.head 5 -  String: sAppDirL
.head 5 -  String: sAppFileL
.head 5 -  String: sAppExtL
.head 5 -  String: spFileNoExe
.head 5 -  Boolean: bSQLConnect
.head 4 +  Functions
.head 5 +  Function: GetImagePath
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fType
.head 7 -  Sql Handle: hSqlImage
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnImage
.head 7 -  String: sLocation
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlImage, 'SELECT path
 	from crim.cr_image_location  into  :sLocation
	WHERE type = :fType')
.head 7 -  Call SqlFetchNext( hSqlImage, nReturnImage)
.head 7 -  Return sLocation
.head 5 +  Function: CheckVersion
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bCopy  !tells function to copy if not the same
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sServerApp
.head 7 -  Date/Time: dtLocalDate
.head 7 -  Date/Time: dtServerDate
.head 7 -  String: sServerDepend
.head 7 -  String: sLocalDepend
.head 7 -  String: sDependName
.head 7 -  String: sAppFileCheck
.head 6 +  Actions
.head 7 -  Set sServerApp = GetImagePath( sApplication_Type , hSql)
.head 7 +  If SalStrRightX( sServerApp, 1 ) = '\\'
.head 8 -  Set sServerApp = sServerApp || sAppFileL
.head 7 +  Else
.head 8 -  Set sServerApp = sServerApp || '\\' || sAppFileL
.head 7 -  Set sServerDepend = GetImagePath( sApplication_Type , hSql)
.head 7 -  Set sLocalDepend = GetImagePath( sApplication_Type || '_LOCAL' , hSql)
.head 7 -  Call SalFileGetDateTime( sAppName, dtLocalDate )
.head 7 -  If SalFileGetDateTime( sServerApp, dtServerDate ) = FALSE !Could not connect to server, don't try to copy
.head 7 +  Else
.head 8 +  If bCopy
.head 9 +  If dtLocalDate != dtServerDate
.head 10 +  If NOT VisDosExist( GetImagePath( sApplication_Type || '_LOCAL' , hSql)  )
.head 11 -  Call SalFileCreateDirectory( GetImagePath( sApplication_Type || '_LOCAL' , hSql)  )
.head 10 +  If NOT VisDosExist( sAppName )
.head 11 -  Call VisFileCopy( sServerApp, sAppName )
.head 11 -  Set sAppFileCheck = VisStrSubstitute( sAppFileL, '.exe', '.app')
.head 11 -  ! Set sAppFileCheck = SalStrUpperX( sAppFileCheck )
.head 11 -  Call SqlPrepareAndExecute( hSql, 'SELECT dep_name
		   		    from crim.app_depend
		   		    INTO :sDependName
		   		    WHERE app_name = :sAppFileCheck')
.head 11 +  While SqlFetchNext( hSql, nReturn)
.head 12 -  Call VisFileCopy( sServerDepend || sDependName, sLocalDepend || sDependName )
.head 10 +  Else
.head 11 -  Call VisFileCopy( sServerApp, sAppName )
.head 5 +  Function: GetSecurityLevel
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: fAppName
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: spdTitle
.head 7 -  Number: nReturnSecLevel
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT seclevel
	from crim.app_rights into :nReturnSecLevel
	WHERE app_name = :fAppName ')
.head 7 +  If Not SqlFetchNext( hSql, nReturn)
.head 8 -  Call SalGetWindowText( hWndItem, spdTitle, 30 )
.head 8 -  Call SalMessageBox( 'No Security Entry for Push Button ' || spdTitle || ' - ' || fAppName, 'Missing Entry in the app_rights table', MB_Ok )
.head 8 -  Call SalDisableWindowAndLabel( hWndItem )
.head 8 -  Return TRUE
.head 7 +  If nReturnSecLevel = NUMBER_Null
.head 8 -  Set nReturnSecLevel = 0
.head 7 -  Return nReturnSecLevel
.head 5 +  Function: Constructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sPAppName
.head 7 -  Number: nPSecurityLevel
.head 7 -  String: sPApplication_Type   !CRIMINAL_APPLICATIONS_5.1, CRIMINAL_J_APPLICATIONS_5.1, CIVIL_APPLICATIONS_5.1, CIVIL_J_APPLICATIONS_5.1,
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sAppName = GetImagePath(sPApplication_Type || '_LOCAL', hSql ) || '\\' || sPAppName
.head 7 -  Set nSecurityLevel = nPSecurityLevel
.head 7 -  ! Set sAppName = SalStrUpperX( sAppName )
.head 7 +  If SalStrRightX( sAppName, 4 ) = '.exe'
.head 8 +  If SalStrScan( sAppName, '\\') > 0
.head 9 -  Call VisDosSplitPath( sAppName, sAppDriveL, sAppDirL, sAppFileL, sAppExtL)
.head 9 -  Set sAppFileL = sAppFileL || sAppExtL
.head 8 +  Else
.head 9 -  Set sAppFileL = sAppName
.head 8 -  Set sApplication_Type = sPApplication_Type
.head 7 +  Else
.head 8 -  ! Set sAppFileL = SalStrUpperX( sPAppName )
.head 8 -  Set sAppFileL = sPAppName
.head 7 +  If nSecurityLevel = NUMBER_Null
.head 8 -  Set nSecurityLevel = GetSecurityLevel( sAppFileL )
.head 8 -  Set sUserExeceptionRights = GetExecption( sAppFileL )
.head 7 -  !
.head 7 +  If nULevel < nSecurityLevel
.head 8 +  If sUserExeceptionRights != 'E'
.head 9 -  Call SalDisableWindowAndLabel( hWndItem )
.head 7 +  Else If sUserExeceptionRights = 'D'
.head 8 -  Call SalDisableWindow( hWndItem )
.head 5 +  Function: GetExecption
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: fAppName
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sRights
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, 'SELECT rights
	   		    from crim.APP_RIGHTS_EXEPTIONS INTO :sRights
	   		    WHERE app_name = :fAppName and user_name = :SqlUser')
.head 7 -  Call SqlFetchNext( hSql, nReturn)
.head 7 -  Return sRights
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  ! If IsBackup(  ) = FALSE  !Don't copy down live version if running is "TEST" mode
.head 6 +  If hSql = hWndNULL
.head 7 -  Set bSQLConnect = SqlConnect( hSql )
.head 6 +  If SalStrRightX( sAppName, 4 ) = '.exe'
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, 1, SalStrLength( sAppName ) - 4 )
.head 7 +  If nULevel < 11
.head 8 +  If SalAppFind( spFileNoExe, TRUE ) != hWndNULL
.head 9 -  Return TRUE
.head 6 +  Else If SalStrScan( sAppName, '.dlg') > 0
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, SalStrScan( sAppName, '.dlg') + 1, 30 )
.head 7 -  Call SalModalDialogFromStr( spFileNoExe, hWndForm )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrScan( sAppName, '.frm') > 0 or SalStrScan( sAppName, '.mdi') > 0
.head 7 -  Set spFileNoExe = SalStrMidX( sAppName, SalStrScan( sAppName, '.frm') + 1, 30 )
.head 7 -  Call SalCreateWindow( spFileNoExe, hWndForm )
.head 7 -  Return TRUE
.head 6 -  Call CheckVersion( TRUE )
.head 6 -  Call SalDisableWindow( MyValue )
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  If NOT VisDosExist( sAppName )
.head 7 -  Call SalMessageBox( 'Error - Could not find the following executable file:
		'|| sAppName||'
		Contact Supervisor', 'Executable Not Found', MB_IconStop| MB_Ok)
.head 6 +  Else
.head 7 -  Call SalLoadApp(sAppName, SqlUser || ' ' || SqlPassword|| ' ' || SqlDatabase )
.head 7 -  Call SalEnableWindow( MyValue )
.head 6 +  If bSQLConnect
.head 7 -  Call SqlDisconnect( hSql )
.head 6 -  Call SalWaitCursor( FALSE )
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Separator:
.head 3 -  Tree Control:
.head 3 -  Navigation Bar:
.head 3 -  Pane Separator:
.head 3 -  Progress Bar:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 +  Application Actions
.head 3 +  On SAM_AppStartup
.head 4 +  If SalModalDialog( dlgLogin, hWndNULL, 'Login Test' )
.head 5 -  Call SalWaitCursor( FALSE )
.head 5 -  Call SalModalDialog( dlgLabels, hWndNULL, STRING_Null, STRING_Null, STRING_Null, 'Certified')
.head 1 +  Dialog Box: dlgLogin
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Criminal Program Login Screen
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.7"
.head 4 -  Top: 0.219"
.head 4 -  Width:  5.925"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.833"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Blue
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description: This window is the initial
login screen
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Pushbutton: pbConnectTestDB
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Test DB
.head 4 -  Window Location and Size
.head 5 -  Left: 4.6"
.head 5 -  Top: 3.313"
.head 5 -  Width:  0.913"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F9
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set dfPass = SalStrTrimX( dfPass)
.head 6 -  Set dfUser = SalStrTrimX( dfUser)
.head 6 -  Set dfPass = SalStrUpperX( dfPass )
.head 6 +  If SqlDatabase = ''
.head 7 -  Set SqlDatabase = 'PostgreSQL35W'
.head 6 -  Set SqlUser = dfUser
.head 6 -  Set SqlPassword = dfPass
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  When SqlError
.head 7 -  Return FALSE
.head 6 -  Set bLogin = SqlConnect (hSql)
.head 6 +  If Not bLogin
.head 7 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Call SalSetFocus( dfUser )
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SqlSetResultSet( hSql, TRUE )
.head 7 +  If SalMessageBox( 'Confirm Connecting to Test Data Base', 'Test DB', MB_OkCancel | MB_IconHand) = IDCANCEL
.head 8 -  Call SalEndDialog( dlgLogin, 0 )
.head 7 +  Else
.head 8 -  Call Check_Change_Password(  )
.head 8 -  Call SalEndDialog( dlgLogin, 1 )
.head 3 +  Picture: picLogin
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.313"
.head 5 -  Top: 0.938"
.head 5 -  Width:  5.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.667"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name: c:\Images\lo178.bmp
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Size to Fit
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 5
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Black
.head 4 -  Background Color: Black
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 28676
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.25"
.head 5 -  Top: 3.74"
.head 5 -  Width:  1.113"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.219"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: UserName:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 28677
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.275"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.038"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfUser
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cLoginUser
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 16
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.388"
.head 6 -  Top: 3.667"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Unformatted
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = 'cyates'
.head 3 +  Data Field: dfPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.388"
.head 6 -  Top: 3.969"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = 'Tiap4c!'
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 2.413"
.head 5 -  Top: 4.271"
.head 5 -  Width:  1.45"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set dfPass = SalStrTrimX( dfPass)
.head 6 -  Set dfUser = SalStrTrimX( dfUser)
.head 6 -  ! Set dfPass = SalStrUpperX( dfPass )
.head 6 +  If SalStrRightX( dfPass, 4 ) = 'TEST'
.head 7 -  Set dfPass = SalStrLeftX( dfPass, SalStrLength( dfPass ) - 4 )
.head 7 -  Call SalSendMsg( pbConnectTestDB, SAM_Click, 0, 0 )
.head 7 -  Return TRUE
.head 6 +  If SqlDatabase = ''
.head 7 -  Set SqlDatabase = 'crimb'
.head 6 -  Set SqlUser = dfUser
.head 6 -  Set SqlPassword = dfPass
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  When SqlError
.head 7 -  Return FALSE
.head 6 -  Set bLogin = SqlConnect (hSql)
.head 6 +  If Not bLogin
.head 7 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Call SalSetFocus( dfUser )
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SqlSetResultSet( hSql, TRUE )
.head 7 -  Set sSelectWhere = "username = user"
.head 7 -  Call Check_Change_Password(  )
.head 6 -  Call SalEndDialog( dlgLogin, 1 )
.head 6 -  Call WriteUser(  )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton( pbOk )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 4.038"
.head 5 -  Top: 4.271"
.head 5 -  Width:  1.45"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgLogin, 0 )
.head 6 -  Call SalQuit(  )
.head 3 +  Data Field: dfCourt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.4"
.head 6 -  Top: 0.198"
.head 6 -  Width:  5.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.49"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Copperplate Gothic Light
.head 5 -  Font Size: 18
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: Blue
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChangeDATABASE
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F9
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SqlUser = 'RYANP'
.head 7 -  Call SalModalDialog(dlgDatasource,  hWndForm, SqlDatabase)
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sWindowTitle
.head 2 +  Window Variables
.head 3 -  String: sLoginTitle
.head 3 -  Number: nFetch
.head 3 -  String: sPublic
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set sPublic = ReadRegistry(  )
.head 4 -  ! Set sSelectWhere = ReadRegistrySelect(  )
.head 4 +  ! If sSelectWhere = ''
.head 5 -  Call SalMessageBox('Error reading Where Clause from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 5 -  Call SalQuit( )
.head 4 +  If sPublic = 'Y'
.head 5 -  Call SalMessageBox( 'Access has been denied', 'Error', MB_Ok|MB_IconExclamation )
.head 5 -  Call SalQuit(  )
.head 4 -  Set sLoginTitle = ReadRegistryTitle(  )
.head 4 -  Set dfCourt = sLoginTitle
.head 4 +  If sWindowTitle != STRING_Null
.head 5 -  Call SalSetWindowText( hWndForm, sWindowTitle )
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgLabels
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Labels Processing
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.225"
.head 4 -  Top: 0.219"
.head 4 -  Width:  8.075"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.146"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description: !Added docket entry made for Bank Attach - Notice Debtor
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfName
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 1.521"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 1.833"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 2.198"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelCity
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 2.573"
.head 6 -  Width:  2.963"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelState
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.825"
.head 6 -  Top: 2.573"
.head 6 -  Width:  0.538"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelZip
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.375"
.head 6 -  Top: 2.573"
.head 6 -  Width:  2.013"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfNumber
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.413"
.head 6 -  Top: 3.083"
.head 6 -  Width:  0.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbPrint
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print &Label
.head 4 -  Window Location and Size
.head 5 -  Left: 0.3"
.head 5 -  Top: 3.604"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalStrTrim( dfAddress2, dfAddress2 )
.head 6 +  If sCourtConst = 'CMC'
.head 7 +  If sUDivision != 'CI'
.head 8 +  If sFormP = 'Certified'
.head 9 -  Set sReport = 'CRCertM.qrp'
.head 8 +  Else
.head 9 -  Set sReport = 'Envelope.qrp'
.head 7 +  Else
.head 8 +  If sFormP = 'Certified'
.head 9 -  Set sReport = CV_REPORT_Path || 'CRCertM.qrp'
.head 8 +  Else
.head 9 -  Set sReport = CV_REPORT_Path || 'Envelope.qrp'
.head 6 +  Else If sCourtConst = 'MMC'
.head 7 -  Set sReport = 'Labelmmc.qrp'
.head 6 +  Else If sCourtConst = 'AMC'
.head 7 -  Set sReport = 'Labels.qrp'
.head 6 +  If dfName != ''
.head 7 +  If sFormP = 'DYMO'
.head 8 +  If dfAddress2 = STRING_Null
.head 9 -  Set sReport = CV_REPORT_Path || 'LabelDymo3.qrp'
.head 8 +  Else
.head 9 -  Set sReport = CV_REPORT_Path || 'LabelDymo4.qrp'
.head 8 -  Call Set_Printer( 'DYMO LabelWriter', 'winspool', 'LPT1' )
.head 8 -  Call PrintLabels(  )
.head 8 -  Call Reset_Printer(  )
.head 7 +  Else
.head 8 -  Call SalPrtGetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 8 +  If sDefaultDevice = 'Xerox Phaser 4400N PCL6'
.head 9 -  Call Set_Printer( 'Envelopes', 'winspool', 'LPT1' )
.head 9 -  Call PrintLabels(  )
.head 9 -  Call Reset_Printer(  )
.head 8 +  Else
.head 9 -  Call PrintLabels(  )
.head 5 +  On SAM_Create
.head 6 +  If sCourtConst = 'CMC'
.head 7 +  If sFormP = 'Certified'
.head 8 -  Call SalSetWindowText( pbPrint, 'Print Certified Mail' )
.head 7 +  Else
.head 8 -  Call SalSetWindowText( pbPrint, 'Print Envelope' )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel, Exit
.head 4 -  Window Location and Size
.head 5 -  Left: 5.625"
.head 5 -  Top: 3.615"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 36308
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 1.583"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 36309
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 2.281"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address 2:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 36310
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 2.688"
.head 5 -  Width:  0.913"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: City:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 36305
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.75"
.head 5 -  Top: 3.104"
.head 5 -  Width:  1.5"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Number of Labels
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd10
.head 4 -  Resource Id: 36306
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.925"
.head 5 -  Top: 1.313"
.head 5 -  Width:  0.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd11
.head 4 -  Resource Id: 36307
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 1.885"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Radio Button: rbDefendant
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: For Defendant - F1
.head 4 -  Window Location and Size
.head 5 -  Left: 0.775"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.763"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SalSendMsg( rbDefendant, SAM_Click, 0, 0 )
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( )
.head 3 +  Radio Button: rbDAttorney
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Defendant's Atty - F2
.head 4 -  Window Location and Size
.head 5 -  Left: 0.788"
.head 5 -  Top: 0.521"
.head 5 -  Width:  2.0"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbBondsman
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Bondsman - F3
.head 4 -  Window Location and Size
.head 5 -  Left: 0.775"
.head 5 -  Top: 0.875"
.head 5 -  Width:  1.538"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'BOND') = 1
.head 7 -  Call FillScreen( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbOtherAtt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Attorney - F5
.head 4 -  Window Location and Size
.head 5 -  Left: 2.938"
.head 5 -  Top: 0.521"
.head 5 -  Width:  1.85"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'ATT') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbProbCS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Probation/CS Agency - F7
.head 4 -  Window Location and Size
.head 5 -  Left: 4.85"
.head 5 -  Top: 0.521"
.head 5 -  Width:  2.413"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'PROBCS') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbOtherAgency
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Agency - F6
.head 4 -  Window Location and Size
.head 5 -  Left: 4.838"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.75"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'OTHERA') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbPolice
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Police Agency - F4
.head 4 -  Window Location and Size
.head 5 -  Left: 2.925"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.75"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'POLICE') = 1
.head 7 -  Call FillScreen( )
.head 3 +  ! Pushbutton: pbEnvelope		!7/7/99  MMC - not used
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print &Envelope
.head 4 -  Window Location and Size
.head 5 -  Left: 0.125"
.head 5 -  Top: 5.292"
.head 5 -  Width:  2.175"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.end
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sReportType = 'Envelope.qrp'
.head 6 -  ! Call PrintLabels(  )
.head 6 -  Set bCertified = FALSE
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 +  ! Pushbutton: pbMailing		!7/7/99  MMC - not used
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Certificate of Mailing
.head 4 -  Window Location and Size
.head 5 -  Left: 2.375"
.head 5 -  Top: 5.292"
.head 5 -  Width:  2.175"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.end
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sReportType = 'Mailing.qrp'
.head 6 -  ! Call PrintLabels(  )
.head 6 -  Set bCertified = FALSE
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 +  Frame: frame1
.head 4 -  Resource Id: 36311
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.538"
.head 5 -  Top: 0.083"
.head 5 -  Width:  6.975"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.115"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Border Color: Default
.head 4 -  Background Color: White
.head 4 -  Xaml:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Message Actions
.head 3 +  Radio Button: rbScreen
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Screen
.head 4 -  Window Location and Size
.head 5 -  Left: 3.588"
.head 5 -  Top: 3.323"
.head 5 -  Width:  0.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: 3D Face Color
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 3 +  Radio Button: rbPrinter
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Printer
.head 4 -  Window Location and Size
.head 5 -  Left: 3.588"
.head 5 -  Top: 3.073"
.head 5 -  Width:  0.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: 3D Face Color
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = TRUE
.head 3 -  Group Box: grp1
.head 4 -  Resource Id: 36312
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.65"
.head 5 -  Top: 1.281"
.head 5 -  Width:  6.813"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.698"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address Information:
.head 4 -  Line Thickness: 1
.head 4 -  Line Color: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton: pb1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F1
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDefendant = TRUE
.head 6 -  Call SalSendMsg(rbDefendant, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F2
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDAttorney = TRUE
.head 6 -  Call SalSendMsg(rbDAttorney, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb3
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F3
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbBondsman = TRUE
.head 6 -  Call SalSendMsg(rbBondsman, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb4
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F4
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbPolice = TRUE
.head 6 -  Call SalSendMsg(rbPolice, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb5
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F5
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAtt = TRUE
.head 6 -  Call SalSendMsg(rbOtherAtt, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb6
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F6
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAgency = TRUE
.head 6 -  Call SalSendMsg(rbOtherAgency, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb7
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F7
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbProbCS = TRUE
.head 6 -  Call SalSendMsg(rbProbCS, SAM_Click, 0,0)
.head 2 +  Functions
.head 3 +  Function: PrintLabels
.head 4 -  Description: Call this function to 15/16 x 3 1/2 Inch Labels
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sCSZ = dfLabelCity||', '||dfLabelState||'   '||dfLabelZip
.head 5 -  Set sAddress = dfAddress
.head 5 -  Set sAddress2 = dfAddress2
.head 5 +  If sAddress2 = ''
.head 6 -  Set sAddress2 = sCSZ
.head 6 -  Set sCSZ = ''
.head 5 -  Set nPrintErr = -1
.head 5 +  If rbDefendant = TRUE
.head 6 -  Set sCaseFullLe = sCaseYrP||'-'||sCaseTyP||'-'||sCaseNoP
.head 5 +  Else
.head 6 -  Set sCaseFullLe = ''
.head 5 +  If sFormP = 'Certified'
.head 6 +  If sAddress2 = ''
.head 7 -  Set sReportBinds = 'dfName, sAddress, sCSZ, sCaseFullLe, sCourt, sUClerk'
.head 7 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, CASENUM, COURT, CLERK'
.head 6 +  Else
.head 7 -  Set sReportBinds = 'dfName, sAddress, sAddress2, sCSZ, sCaseFullLe, sCourt, sUClerk'
.head 7 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM, COURT, CLERK'
.head 5 +  Else
.head 6 -  Set sReportBinds = 'dfName, sAddress, sAddress2, sCSZ, sCaseFullLe'
.head 6 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM'
.head 5 +  If rbScreen = TRUE
.head 6 -  Call SalReportView( hWndForm, hWndNULL, sReport,
    sReportBinds, sReportInputs, nPrintErr )
.head 5 +  Else If rbPrinter = TRUE
.head 6 -  Call SalReportPrint ( dlgLabels, sReport, sReportBinds, sReportInputs,
	dlgLabels.dfNumber, RPT_PrintAll | RPT_PrintNoWarn, nRow, nMaxRow, nPrintErr )
.head 5 +  If nPrintErr > 0
.head 6 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),
	MB_Ok )
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If rbDefendant = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	lname||', '||fname||' '||mname, address1, address2, city, state, zip
			     FROM		cr_parties
			     INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If sCaseNoP = ''
.head 7 -  Call ClearScreen( )
.head 5 +  Else If rbDAttorney = TRUE
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	attno
			     FROM		muni_booking
			     INTO		:nAttNo
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP')
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 -  If nAttNo = NUMBER_Null
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute(hSql, "SELECT	 fname||' '||mname||' '||lname, address, address2,
					 city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	attno = :nAttNo")
.head 8 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbBondsman = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	bondsman,address1, address2, city, state, zip1
			      FROM		cr_bondsman
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbOtherAtt = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	fname||' '||mname||' '||lname,address, address2, city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbPolice = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agency,address1, address2, city, state, zip
			      FROM		agency_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbOtherAgency = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agencynm,address1, address2, city, state, zip
			      FROM		other_agency
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbProbCS = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	description, address1, address2, citystate
			      FROM		program_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity 
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If dfAddress != ''
.head 7 -  Set dfAddress = 'ATTN. '||dfAddress
.head 6 +  Else
.head 7 -  Set dfAddress = dfAddress2
.head 3 +  Function: ClearScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set dfName = ''
.head 5 -  Set dfAddress = ''
.head 5 -  Set dfAddress2 = ''
.head 5 -  Set dfLabelCity = ''
.head 5 -  Set dfLabelState = ''
.head 5 -  Set dfLabelZip = ''
.head 5 -  Set sCSZ = ''
.head 2 +  Window Parameters
.head 3 -  String: sCaseYrP
.head 3 -  String: sCaseTyP
.head 3 -  String: sCaseNoP
.head 3 -  String: sFormP
.head 2 +  Window Variables
.head 3 -  ! String: sFind
.head 3 -  ! String: sSearch
.head 3 -  Number: nRow
.head 3 -  Number: nMaxRow
.head 3 -  String: sAddress2
.head 3 -  String: sAddress
.head 3 -  Number: nAttNo
.head 3 -  String: sCSZ
.head 3 -  String: sCaseFullLe
.head 3 -  ! String: sCode1
.head 3 -  ! String: sCode2
.head 3 -  ! String: sCode3
.head 3 -  ! String: sLECode
.head 3 -  ! String: sLCaseNo
.head 3 -  ! String: sLDefName
.head 3 -  ! String: sLDefAddr2
.head 3 -  ! String: sLDefAddr
.head 3 -  ! String: sLDefCSZ
.head 3 -  ! String: sLPlaName
.head 3 -  ! String: sLPlaAddr2
.head 3 -  ! String: sLPlaAddr
.head 3 -  ! String: sLPlaCSZ
.head 3 -  ! String: sNDefName
.head 3 -  ! String: sNDefName2
.head 3 -  ! String: sNPlaName
.head 3 -  ! String: sNPlaName2
.head 3 -  ! Date/Time: dtHearDt
.head 3 -  ! Number: nOffSet
.head 3 -  ! String: sBDebtNtcService
.head 3 -  Number: nRptFlag
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalSetDefButton( pbPrint )
.head 4 -  Set dfNumber= 1
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlag=0
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 -  Set nRptFlag=nRptFlag+1
.head 4 +  If nRptFlag=1
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgExport
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.15"
.head 4 -  Top: 0.167"
.head 4 -  Width:  8.629"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.74"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Child Table: tblTable
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 0.344"
.head 6 -  Width:  4.057"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 2.292"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Class Default
.head 5 -  Discardable? Class Default
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colColName
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Column
.head 6 -  Visible? Class Default
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: String
.head 6 -  Justify: Class Default
.head 6 -  Width:  2.7"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Class Default
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colExport
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Export?
.head 6 -  Visible? Class Default
.head 6 -  Editable? Class Default
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: String
.head 6 -  Justify: Class Default
.head 6 -  Width:  0.686"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Check Box
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value: 1
.head 8 -  Uncheck Value: 0
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_AnyEdit
.head 8 -  Call SalTblSetRowFlags( tblTable, SalTblQueryContext( tblTable ), ROW_Edited, FALSE )
.head 8 -  Call FillArray(  )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bSetTRUEE = TRUE
.head 9 -  Set bSetTRUEE = FALSE
.head 8 +  Else If bSetTRUEE = FALSE
.head 9 -  Set bSetTRUEE = TRUE
.head 8 -  Call CheckRows(2, bSetTRUEE)
.head 8 -  Call FillArray(  )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 5 +  Column: colUseSurr
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Use
Surrounder
.head 6 -  Visible? Class Default
.head 6 -  Editable? Class Default
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: String
.head 6 -  Justify: Class Default
.head 6 -  Width:  1.057"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Check Box
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value: 1
.head 8 -  Uncheck Value: 0
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_AnyEdit
.head 8 -  Call FillArray(  )
.head 8 -  Call SalTblSetRowFlags( tblTable, SalTblQueryContext( tblTable ), ROW_Edited, FALSE )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bSetTRUES = TRUE
.head 9 -  Set bSetTRUES = FALSE
.head 8 +  Else If bSetTRUES = FALSE
.head 9 -  Set bSetTRUES = TRUE
.head 8 -  Call CheckRows(3, bSetTRUES)
.head 8 -  Call FillArray(  )
.head 8 -  Call ShowSample( cbIncludeVisible )
.head 5 +  Column: colColID
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Class Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Class Default
.head 6 -  Width:  Default
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Class Default
.head 6 -  Country: Class Default
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Class Default
.head 7 -  Multiline Cell? Class Default
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call MTblSubClass( hWndForm )
.head 6 -  Call MTblDefineRowLines( tblTable,0, 0 )
.head 6 -  Call MTblDefineColLines( tblTable,1, COLOR_Black )
.head 6 -  Call SalTblDefineRowHeader( tblTable, '', 20, 33, hWndNULL )
.head 6 -  Call SalTblSetTableFlags( tblTable, TBL_Flag_SelectableCols, TRUE )
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 7.171"
.head 5 -  Top: 0.365"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.229"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call ExportTable(cbIncludeVisible )
.head 6 -  Call SalEndDialog(dlgExport, 1 )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 7.171"
.head 5 -  Top: 0.615"
.head 5 -  Width:  1.114"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.208"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgExport, 0 )
.head 3 +  Check Box: cbIncludeVisible
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Include Only Visible Columns
.head 4 -  Window Location and Size
.head 5 -  Left: 4.314"
.head 5 -  Top: 0.927"
.head 5 -  Width:  2.629"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( cbIncludeVisible)
.head 3 +  Check Box: cbIncludeTitle
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Include Title
.head 4 -  Window Location and Size
.head 5 -  Left: 4.314"
.head 5 -  Top: 1.177"
.head 5 -  Width:  1.286"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( cbIncludeVisible)
.head 3 +  Check Box: cbIncludeHeader
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Create a Header
.head 4 -  Window Location and Size
.head 5 -  Left: 4.229"
.head 5 -  Top: 2.208"
.head 5 -  Width:  1.514"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If MyValue = TRUE
.head 7 -  Call ResizeWindow('HEADER')
.head 7 -  Call SalSetFocus(mlHeader)
.head 6 +  Else
.head 7 -  Call ResizeWindow('HEADER_NO')
.head 3 +  Check Box: cbLoadExcel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Load In Excel After Creation
.head 4 -  Window Location and Size
.head 5 -  Left: 5.814"
.head 5 -  Top: 2.208"
.head 5 -  Width:  2.586"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Message Actions
.head 3 -  Group Box: grp2
.head 4 -  Resource Id: 11964
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.214"
.head 5 -  Top: 0.271"
.head 5 -  Width:  2.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.188"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Export Format:
.head 4 -  Line Thickness: 1
.head 4 -  Line Color: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Group Box: grp3
.head 4 -  Resource Id: 11965
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.229"
.head 5 -  Top: 1.458"
.head 5 -  Width:  2.843"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.75"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Rows from Parent TBL to Export:
.head 4 -  Line Thickness: 1
.head 4 -  Line Color: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 11966
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 6.0"
.head 5 -  Top: 1.917"
.head 5 -  Width:  0.243"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.188"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: to
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 11967
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 5.729"
.head 5 -  Top: 0.5"
.head 5 -  Width:  0.7"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.333"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Seperate Fields:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfFieldSep
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 6.5"
.head 6 -  Top: 0.542"
.head 6 -  Width:  0.529"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample(cbIncludeVisible  )
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 11968
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.271"
.head 5 -  Top: 0.5"
.head 5 -  Width:  0.814"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.365"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Surround Values:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfFieldSurr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.114"
.head 6 -  Top: 0.542"
.head 6 -  Width:  0.529"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 11969
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.329"
.head 5 -  Top: 0.49"
.head 5 -  Width:  0.814"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.177"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Dataset:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfDS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.243"
.head 6 -  Top: 0.448"
.head 6 -  Width:  1.657"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.229"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 11970
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.314"
.head 5 -  Top: 0.74"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.177"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Row Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfRS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.243"
.head 6 -  Top: 0.698"
.head 6 -  Width:  1.686"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Data Field: dfAppPath
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.214"
.head 6 -  Top: 2.417"
.head 6 -  Width:  4.257"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Data Field: dfRowStart
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.3"
.head 6 -  Top: 1.875"
.head 6 -  Width:  0.671"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Data Field: dfRowEnd
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 6.229"
.head 6 -  Top: 1.875"
.head 6 -  Width:  0.671"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Multiline Field: mlSample
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 20000
.head 5 -  String Type: Long String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Border? Yes
.head 5 -  Word Wrap? No
.head 5 -  Vertical Scroll? Yes
.head 5 -  Window Location and Size
.head 6 -  Left: 0.071"
.head 6 -  Top: 2.688"
.head 6 -  Width:  8.386"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 1.927"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Radio Button: rbAll
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &All Rows
.head 4 -  Window Location and Size
.head 5 -  Left: 4.414"
.head 5 -  Top: 1.625"
.head 5 -  Width:  1.057"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDisableWindow( dfRowStart)
.head 6 -  Call SalDisableWindow( dfRowEnd)
.head 3 +  Radio Button: rbRange
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Range
.head 4 -  Window Location and Size
.head 5 -  Left: 4.414"
.head 5 -  Top: 1.906"
.head 5 -  Width:  0.843"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEnableWindow( dfRowStart)
.head 6 -  Call SalEnableWindow( dfRowEnd)
.head 6 -  Call SalSetFocus(dfRowStart)
.head 3 +  Multiline Field: mlHeader
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2000
.head 5 -  String Type: Long String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Border? Yes
.head 5 -  Word Wrap? Yes
.head 5 -  Vertical Scroll? Yes
.head 5 -  Window Location and Size
.head 6 -  Left: 0.071"
.head 6 -  Top: 2.688"
.head 6 -  Width:  8.386"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.688"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Radio Button: rbCSV
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: CSV
.head 4 -  Window Location and Size
.head 5 -  Left: 2.557"
.head 5 -  Top: 0.031"
.head 5 -  Width:  0.814"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  ! Set dfFieldSep = '}'
.head 6 +  If sFieldSep != ''
.head 7 -  Set dfFieldSep = sFieldSep
.head 6 +  Else
.head 7 -  Set dfFieldSep = '}'
.head 6 +  If sFieldSurr != ''
.head 7 -  Set dfFieldSurr = sFieldSurr
.head 6 +  Else
.head 7 -  Set dfFieldSurr = '"'
.head 6 -  Call SalShowWindowAndLabel( dfFieldSep )
.head 6 -  Call SalShowWindowAndLabel( dfFieldSurr )
.head 6 -  Call SalHideWindowAndLabel( dfDS )
.head 6 -  Call SalHideWindowAndLabel( dfRS )
.head 6 -  Call SalHideWindow( cbLoadExcel )
.head 6 -  Call SalHideWindow ( dfAppPath)
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Radio Button: rbExcel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Excel Format
.head 4 -  Window Location and Size
.head 5 -  Left: 3.243"
.head 5 -  Top: 0.031"
.head 5 -  Width:  1.357"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set dfFieldSep = TAB
.head 6 -  Call SalHideWindowAndLabel( dfFieldSep )
.head 6 -  Call SalShowWindowAndLabel( dfFieldSurr )
.head 6 -  Call SalShowWindow( cbLoadExcel )
.head 6 -  Call SalShowWindow ( dfAppPath)
.head 6 -  Call SalHideWindowAndLabel( dfDS )
.head 6 -  Call SalHideWindowAndLabel( dfRS )
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 3 +  Radio Button: rbXML
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: XML
.head 4 -  Window Location and Size
.head 5 -  Left: 4.643"
.head 5 -  Top: 0.031"
.head 5 -  Width:  1.357"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalHideWindowAndLabel( dfFieldSep )
.head 6 -  Call SalHideWindowAndLabel( dfFieldSurr )
.head 6 -  Call SalShowWindow( cbLoadExcel )
.head 6 -  Call SalSetWindowText( cbLoadExcel, 'Load In IE After Creation' )
.head 6 -  Call SalHideWindow ( dfAppPath)
.head 6 -  Call SalShowWindowAndLabel( dfDS )
.head 6 -  Call SalShowWindowAndLabel( dfRS )
.head 6 -  Call ShowSample( cbIncludeVisible)
.head 2 +  Functions
.head 3 +  Function: ExportTable
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bIncludeVisible
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 5 -  String: sLineText
.head 5 -  Boolean: bOk
.head 5 -  Number: nRow
.head 5 -  ! Save Variables
.head 5 -  String: saFilter[*]
.head 5 -  Number: nIndex
.head 5 -  Number: nFilters
.head 5 -  File Handle: fhFile
.head 5 -  String: sPath
.head 5 -  String: sFile
.head 5 -  String: sColTitle
.head 5 -  String: sRS
.head 5 -  String: sDS
.head 4 +  Actions
.head 5 -  ! ! SAVE AS...
.head 5 +  If rbCSV
.head 6 -  Set saFilter[0] = "CSV File"
.head 6 -  Set saFilter[1] = "*.csv"
.head 6 -  Set nFilters = 2
.head 6 -  Set nIndex = 1
.head 5 +  Else If rbExcel
.head 6 -  Set saFilter[0] = "Excel File"
.head 6 -  Set saFilter[1] = "*.xls"
.head 6 -  Set nFilters = 2
.head 6 -  Set nIndex = 1
.head 5 +  Else If rbXML
.head 6 -  Set saFilter[0] = "XML File"
.head 6 -  Set saFilter[1] = "*.xml"
.head 6 -  Set nFilters = 2
.head 6 -  Set nIndex = 1
.head 5 -  ! Set sFile = sTableName
.head 5 -  ! !
.head 5 -  ! !
.head 5 +  If sOptionalFileName != ''
.head 6 -  Set sFile = sOptionalFileName
.head 6 -  Set sPath = sFile
.head 5 +  Else
.head 6 -  Call SalDlgSaveFile ( hWndForm, "Save Export File", saFilter, nFilters, nIndex, sFile, sPath )
.head 5 +  If sFile != ''
.head 6 +  If SalFileOpen( fhFile, sPath, OF_Create | OF_Text | OF_Write )
.head 7 -  Call SalWaitCursor( TRUE )
.head 7 +  If rbXML
.head 8 -  Set sLineText = ''
.head 8 -  Set sViewXML1 = ''
.head 8 -  Set sViewXML2 = ''
.head 8 -  Set sDS = dfDS
.head 8 -  Set sRS = dfRS
.head 8 -  Call SalFilePutStr( fhFile, '<?xml version="1.0" encoding="ISO-8859-1" ?> ')
.head 8 -  Call SalFilePutStr( fhFile, '<'|| sDS ||'>'  )
.head 8 -  Set nRow = 0
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 +  While bOk
.head 9 -  Set nCol = 1
.head 9 -  Set sText = ''
.head 9 -  Set sColTitle = ''
.head 9 +  ! If bIncludeVisible
.head 10 -  Set nCOL_COUNT = nCOL_COUNT_SHOWN
.head 9 +  While nCol <= nCOL_COUNT
.head 10 +  If bIncludeVisible
.head 11 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 12 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 12 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 12 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 12 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 12 +  If sText != '' and sColTitle != ''
.head 13 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 12 +  If sColTitle != ''
.head 13 +  If nRow = 1
.head 14 -  Set sViewXML1 = sViewXML1||'
<th>'||sColTitle||'</th>'
.head 14 -  Set sViewXML2 = sViewXML2||'
<td><span datafld="'||sColTitle||'"></span></td>'
.head 10 +  Else
.head 11 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 11 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 11 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 11 +  If sText != '' and sColTitle != ''
.head 12 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 11 +  If sColTitle != ''
.head 12 +  If nRow = 1
.head 13 -  Set sViewXML1 = sViewXML1||'
<th>'||sColTitle||'</th>'
.head 13 -  Set sViewXML2 = sViewXML2||'
<td><span datafld="'||sColTitle||'"></span></td>'
.head 10 -  Set nCol = nCol + 1
.head 9 +  If sLineText != ''
.head 10 -  Set sLineText = '<'||sRS ||'>'  ||sLineText || '</'|| sRS ||'>'
.head 9 -  Call SalFilePutStr( fhFile, sLineText )
.head 9 -  Set sLineText = ''
.head 9 -  Set sText = ''
.head 9 -  Set nRow = nRow +1
.head 9 +  If rbRange = TRUE
.head 10 +  If nRow > dfRowEnd
.head 11 -  Set bOk = FALSE
.head 9 +  Else
.head 10 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 -  Call SalFilePutStr( fhFile, '</'|| sDS ||'>'  )
.head 8 -  Call SalFileClose( fhFile )
.head 8 +  If cbLoadExcel = TRUE
.head 9 -  Call CreateXMLLoader( sPath, sViewXML1, sViewXML2)
.head 8 -  Call SalWaitCursor( FALSE )
.head 7 +  Else
.head 8 +  If cbIncludeHeader = TRUE
.head 9 +  If mlHeader != ''
.head 10 -  Call SalFilePutStr( fhFile, mlHeader )
.head 8 -  Set nIndex = 0
.head 8 -  ! ! GET COLUMN TITLES
.head 8 -  Set nCol = 1
.head 8 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 8 +  If CheckArray('EXPORT', nCol)
.head 9 +  If bIncludeVisible
.head 10 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText||dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText||dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 8 +  While sText != ''
.head 9 -  Set nCol = nCol + 1
.head 9 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 9 +  If CheckArray('EXPORT', nCol)
.head 10 +  If bIncludeVisible
.head 11 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 12 +  If sText != ''
.head 13 +  If sLineText != ''
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sLineText||sText||dfFieldSep
.head 13 +  Else
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText||dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 8 -  Call SalFilePutStr( fhFile, sLineText )
.head 8 -  Set sLineText = ''
.head 8 +  If rbRange = TRUE
.head 9 -  Set nRow = dfRowStart
.head 8 +  Else
.head 9 -  Set nRow = 0
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 +  While bOk
.head 9 -  Set nCol = 1
.head 9 +  While nCol <= nCOL_COUNT
.head 10 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 10 +  If CheckArray('EXPORT', nCol)
.head 11 +  If bIncludeVisible
.head 12 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 13 +  If sLineText != ''
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sLineText||sText||dfFieldSep
.head 13 +  Else
.head 14 +  If CheckArray('SURR', nCol)
.head 15 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 14 +  Else
.head 15 -  Set sLineText = sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText||dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 10 -  Set nCol = nCol + 1
.head 9 -  Call SalFilePutStr( fhFile, sLineText )
.head 9 -  Set sLineText = ''
.head 9 -  Set sText = ''
.head 9 -  Set nRow = nRow +1
.head 9 +  If rbRange = TRUE
.head 10 +  If nRow > dfRowEnd
.head 11 -  Set bOk = FALSE
.head 9 +  Else
.head 10 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 8 -  Call SalFileClose( fhFile )
.head 8 -  Call SalWaitCursor( FALSE )
.head 8 +  If cbLoadExcel = TRUE
.head 9 -  Call SalLoadApp( dfAppPath, '"'||sPath||'"' )
.head 8 +  Else
.head 9 -  Call SalMessageBox( "Export File  <" || sPath || "> saved.", "Save File", 0 )
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bIncludeVisible
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 5 -  Number: nTBLRow
.head 4 +  Actions
.head 5 -  Call SalTblReset( tblTable)
.head 5 -  Set nCol = 1
.head 5 -  Set nCOL_COUNT_SHOWN = 1
.head 5 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 5 +  If bIncludeVisible
.head 6 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 7 +  If sText != ''
.head 8 -  Call FillRow( sText , nTBLRow, nCol)
.head 7 -  Set nCOL_COUNT_SHOWN = nCOL_COUNT_SHOWN + 1
.head 5 +  Else
.head 6 +  If sText != ''
.head 7 -  Call FillRow( sText , nTBLRow, nCol)
.head 5 +  While sText != ''
.head 6 -  Set nCol = nCol + 1
.head 6 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 6 +  If bIncludeVisible
.head 7 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 8 +  If sText != ''
.head 9 -  Call FillRow( sText , nTBLRow, nCol)
.head 8 -  Set nCOL_COUNT_SHOWN = nCOL_COUNT_SHOWN + 1
.head 6 +  Else
.head 7 +  If sText != ''
.head 8 -  Call FillRow( sText , nTBLRow, nCol)
.head 5 -  Set nCOL_COUNT = nCol
.head 5 -  Call FillArray( )
.head 5 -  Call ShowSample(  cbIncludeVisible)
.head 3 +  Function: FillRow
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sText
.head 5 -  Receive Number: nTBLRow
.head 5 -  Number: nColID
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalTblInsertRow( tblTable, nTBLRow  )
.head 5 -  Call SalTblSetContext( tblTable, nTBLRow  )
.head 5 -  Call SalTblSetRowFlags( tblTable, nTBLRow, ROW_New, FALSE )
.head 5 -  Set tblTable.colColName = sText
.head 5 -  Set tblTable.colExport= '1'
.head 5 -  Set tblTable.colUseSurr = '1'
.head 5 -  Set tblTable.colColID = nColID
.head 5 +  If SalNumberMod(nTBLRow, 2) = 0
.head 6 -  Call MTblSetRowBackColor( tblTable, nTBLRow, COLOR_LightGray, 0)
.head 5 -  Call Inc( nTBLRow )
.head 3 +  Function: ShowSample
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Boolean: bIncludeVisible
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 5 -  String: sLineText
.head 5 -  Boolean: bOk
.head 5 -  Number: nRow
.head 5 -  String: sColTitle
.head 5 -  String: sRS
.head 5 -  String: sDS
.head 4 +  Actions
.head 5 -  Set mlSample = ''
.head 5 +  If rbXML
.head 6 -  Set sLineText = ''
.head 6 -  Set sDS = dfDS
.head 6 -  Set sRS = dfRS
.head 6 -  ! ! GET 10 LINES OF SAMPLE DATA
.head 6 -  Set nRow = 0
.head 6 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 6 +  While bOk
.head 7 -  Set nCol = 1
.head 7 +  ! If bIncludeVisible
.head 8 -  Set nCOL_COUNT = nCOL_COUNT_SHOWN
.head 7 +  While nCol <= nCOL_COUNT
.head 8 +  If bIncludeVisible
.head 9 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 10 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 10 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 10 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 10 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 10 +  If sText != '' and sColTitle != ''
.head 11 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 8 +  Else
.head 9 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 9 -  Set sColTitle = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 9 -  Set sColTitle = VisStrSubstitute( sColTitle, ' ', '' )
.head 9 -  Set sColTitle = VisStrSubstitute( sColTitle, '#', '' )
.head 9 +  If sText != '' and sColTitle != ''
.head 10 -  Set sLineText = sLineText||'<'|| sColTitle ||'>' ||sText|| '</'|| sColTitle||'>'
.head 8 -  Set nCol = nCol + 1
.head 7 +  If sLineText != ''
.head 8 -  Set sLineText = '<'||sRS ||'>'  ||sLineText || '</'|| sRS ||'>'
.head 7 -  Set mlSample = mlSample ||'
'||sLineText
.head 7 -  Set sLineText = ''
.head 7 -  Set sText = ''
.head 7 -  Set nRow = nRow +1
.head 7 +  If nRow = 10
.head 8 -  Set bOk = FALSE
.head 7 +  Else
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 6 +  If mlSample != ''
.head 7 -  Set mlSample = '<'||sDS ||'>'  || mlSample || '</'|| sDS ||'>'
.head 7 -  Set mlSample = '<?xml version="1.0" encoding="ISO-8859-1" ?> '||
mlSample
.head 5 +  Else
.head 6 -  ! ! GET COLUMN TITLES
.head 6 +  If cbIncludeTitle
.head 7 -  Set nCol = 1
.head 7 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 7 +  If CheckArray('EXPORT', nCol)
.head 8 +  If bIncludeVisible
.head 9 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sLineText != ''
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 8 +  Else
.head 9 +  If sText != ''
.head 10 +  If sLineText != ''
.head 11 +  If CheckArray('SURR', nCol)
.head 12 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If CheckArray('SURR', nCol)
.head 12 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sLineText != ''
.head 11 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 10 +  Else
.head 11 -  Set sLineText = sText|| dfFieldSep
.head 7 +  While sText != ''
.head 8 -  Set nCol = nCol + 1
.head 8 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 8 +  If CheckArray('EXPORT', nCol)
.head 9 +  If bIncludeVisible
.head 10 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If sLineText != ''
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sLineText != ''
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 7 -  Set mlSample = sLineText
.head 6 -  Set sLineText = ''
.head 6 -  ! ! GET 10 LINES OF SAMPLE DATA
.head 6 -  Set nRow = 0
.head 6 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 6 +  While bOk
.head 7 -  Set nCol = 1
.head 7 +  While nCol <= nCOL_COUNT
.head 8 -  Set sText=VisTblGetCell( hWndTBL, nRow, SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos )  )
.head 8 +  If CheckArray('EXPORT', nCol)
.head 9 +  If bIncludeVisible
.head 10 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTBL, nCol, COL_GetPos ) )
.head 11 +  If sText != ''
.head 12 +  If sLineText != ''
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 +  If CheckArray('SURR', nCol)
.head 14 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 13 +  Else
.head 14 -  Set sLineText = sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If sLineText != ''
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 9 +  Else
.head 10 +  If sText != ''
.head 11 +  If sLineText != ''
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = sLineText||dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 +  If CheckArray('SURR', nCol)
.head 13 -  Set sLineText = dfFieldSurr||sText||dfFieldSurr|| dfFieldSep
.head 12 +  Else
.head 13 -  Set sLineText = sText|| dfFieldSep
.head 10 +  Else
.head 11 +  If sLineText != ''
.head 12 -  Set sLineText = sLineText||sText|| dfFieldSep
.head 11 +  Else
.head 12 -  Set sLineText = sText|| dfFieldSep
.head 8 -  Set nCol = nCol + 1
.head 7 -  Set mlSample = mlSample ||'
'||sLineText
.head 7 -  Set sLineText = ''
.head 7 -  Set sText = ''
.head 7 -  Set nRow = nRow +1
.head 7 +  If nRow = 10
.head 8 -  Set bOk = FALSE
.head 7 +  Else
.head 8 -  Set bOk = SalTblSetContext( hWndTBL, nRow)
.head 3 +  Function: FillArray
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: n
.head 5 -  Number: nRow
.head 5 -  String: sText
.head 4 +  Actions
.head 5 -  Call ResetArray( sUseExp)
.head 5 -  Call ResetArray( sUseSurr)
.head 5 -  Set n = 0
.head 5 -  Set nRow = 0
.head 5 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 2, COL_GetPos )  )
.head 5 +  If sText = '1'
.head 6 -  Set sUseExp[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 6 -  Call Inc(n)
.head 5 +  While sText != ''
.head 6 -  Call Inc(nRow)
.head 6 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 2, COL_GetPos )  )
.head 6 +  If sText = '1'
.head 7 -  Set sUseExp[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 7 -  Call Inc(n)
.head 5 -  Set n = 0
.head 5 -  Set nRow = 0
.head 5 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 3, COL_GetPos )  )
.head 5 +  If sText = '1'
.head 6 -  Set sUseSurr[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 6 -  Call Inc(n)
.head 5 +  While sText != ''
.head 6 -  Call Inc(nRow)
.head 6 -  Set sText =  VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 3, COL_GetPos )  )
.head 6 +  If sText = '1'
.head 7 -  Set sUseSurr[n] = VisTblGetCell( tblTable, nRow, SalTblGetColumnWindow ( hWndTBL, 4, COL_GetPos )  )
.head 7 -  Call Inc(n)
.head 5 -  Set sText = sText
.head 3 +  Function: ResetArray
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive String: sArray[*]
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMax
.head 5 -  Number: nMin
.head 4 +  Actions
.head 5 -  Call SalArrayGetUpperBound( sArray, 1, nMax )
.head 5 -  Set nMin = 0
.head 5 +  While nMin <= nMax
.head 6 -  Set sArray[nMin] = ''
.head 6 -  Call Inc(nMin)
.head 3 +  Function: CheckArray
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sType
.head 5 -  Number: nCheckID
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMin
.head 5 -  Number: nMax
.head 5 -  String: sCheckID
.head 4 +  Actions
.head 5 -  Set sCheckID = SalNumberToStrX(nCheckID, 0)
.head 5 +  If sType = 'EXPORT'
.head 6 -  Call SalArrayGetUpperBound( sUseExp, 1, nMax )
.head 6 -  Set nMin = 0
.head 6 +  While nMin <= nMax
.head 7 +  If sUseExp[nMin] = sCheckID
.head 8 -  Return TRUE
.head 7 -  Call Inc(nMin)
.head 6 -  Return FALSE
.head 5 +  Else If sType = 'SURR'
.head 6 -  Call SalArrayGetUpperBound( sUseSurr, 1, nMax )
.head 6 -  Set nMin = 0
.head 6 +  While nMin <= nMax
.head 7 +  If sUseSurr[nMin] = sCheckID
.head 8 -  Return TRUE
.head 7 -  Call Inc(nMin)
.head 6 -  Return FALSE
.head 3 +  Function: CheckRows
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Number: nColID
.head 5 -  Boolean: bSetTRUE
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Boolean: bOk
.head 5 -  Number: nRow
.head 4 +  Actions
.head 5 -  Set nRow = 0
.head 5 -  Set bOk = SalTblSetContext( tblTable, nRow)
.head 5 +  While bOk
.head 6 +  If nColID = 3
.head 7 +  If bSetTRUE
.head 8 -  Set tblTable.colUseSurr = '1'
.head 7 +  Else
.head 8 -  Set tblTable.colUseSurr = '0'
.head 6 +  Else If nColID = 2
.head 7 +  If bSetTRUE
.head 8 -  Set tblTable.colExport = '1'
.head 7 +  Else
.head 8 -  Set tblTable.colExport = '0'
.head 6 -  Call Inc(nRow)
.head 6 -  Set bOk = SalTblSetContext( tblTable, nRow)
.head 3 +  Function: GetParent_TBL_Row_Cnt
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 -  Actions
.head 3 +  Function: ResizeWindow
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sType
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If sType= 'HEADER'
.head 6 -  Call SalSetWindowSize( mlSample, 8.386, 1.25 )
.head 6 -  Call SalSetWindowLoc( mlSample, 0.071, 3.365)
.head 6 -  Call SalShowWindow( mlHeader )
.head 5 +  Else If sType = 'HEADER_NO'
.head 6 -  Call SalSetWindowSize( mlSample, 8.386, 1.927 )
.head 6 -  Call SalSetWindowLoc( mlSample, 0.071, 2.688)
.head 6 -  Call SalHideWindow( mlHeader )
.head 3 +  Function: GetExcelProgram
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sProgramUse
.head 4 +  Parameters
.head 5 -  String: sProgramSave
.head 4 +  Static Variables
.head 5 -  Number: nOffset
.head 4 +  Local variables
.head 5 -  String: sProgramUse
.head 5 -  ! String: sProgramSave
.head 5 -  String: sFileType
.head 5 -  String: sProgramType
.head 5 -  Number: hIconHandle
.head 5 -  String: sUsePath
.head 5 -  String: sSystemRoot
.head 5 -  String: sProgramFilesPath
.head 4 +  Actions
.head 5 -  ! Set sProgramSave =
.head 5 -  ! Set sSystemRoot = ReadRegValue( HKEY_LOCAL_MACHINE, '/SOFTWARE/Microsoft/Windows NT/CurrentVersion', 'SystemRoot' )
.head 5 -  ! Set sProgramFilesPath = ReadRegValue( HKEY_LOCAL_MACHINE, '/SOFTWARE/Microsoft/Windows/CurrentVersion', 'ProgramFilesDir' )
.head 5 -  Set nOffset = VisStrScanReverse( sProgramSave, -1,  '.' )
.head 5 -  Set sFileType = SalStrMidX( sProgramSave, nOffset, 100 )
.head 5 -  ! Set sProgramUse = ReadRegistryOpenWith( sFileType)
.head 5 -  Set sProgramUse = sProgramUse
.head 5 -  Set nOffset = SalStrScan( sProgramUse, 'SystemRoot')
.head 5 +  If nOffset > 0
.head 6 -  Set sProgramUse =  SalStrReplaceX( sProgramUse, nOffset-1, 13, sSystemRoot||'\\')
.head 5 -  Set nOffset = SalStrScan( sProgramUse, 'ProgramFiles')
.head 5 +  If nOffset > 0
.head 6 -  Set sProgramUse =  SalStrReplaceX( sProgramUse, nOffset-1, 15, sProgramFilesPath||'\\')
.head 5 -  Set nOffset = SalStrScan(sProgramUse, '\\%')
.head 5 -  Return sProgramUse
.head 3 +  Function: ReadRegistryOpenWith
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 +  Parameters
.head 5 -  String: sParam
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CLASSES_ROOT )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/'||sParam, FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( '', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Call REG.SetRootKey( HKEY_CLASSES_ROOT )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/'||s||'/Shell/open/command', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( '', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: ReadRegValue   !Used to pull any value
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 +  Parameters
.head 5 -  Number: sRootKey
.head 5 -  String: sKeyName
.head 5 -  String: sValueName
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  ! String: bin
.head 5 -  ! Number: BinSize
.head 5 -  ! Number: i
.head 5 -  ! Number: f
.head 5 -  ! Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  ! Number: n
.head 5 -  ! Number: min
.head 5 -  ! Number: max
.head 5 -  ! String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( sRootKey )
.head 5 +  If not REG.OpenKey( sKeyName, FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( sValueName, s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: CreateXMLLoader
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: fPath
.head 5 -  String: fXML1
.head 5 -  String: fXML2
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  File Handle: fhFile
.head 4 +  Actions
.head 5 +  If SalFileOpen( fhFile, 'C:\\Temp\\TempXML.html', OF_Create | OF_Text | OF_Write )
.head 6 -  Call SalFilePutStr( fhFile, '<html>')
.head 6 -  Call SalFilePutStr( fhFile, '<body>')
.head 6 -  Call SalFilePutStr( fhFile, '<xml')
.head 6 -  Call SalFilePutStr( fhFile, 'src="'||fPath||'"')
.head 6 -  Call SalFilePutStr( fhFile, 'id="xmlfile"')
.head 6 -  Call SalFilePutStr( fhFile, 'async="false">')
.head 6 -  Call SalFilePutStr( fhFile, '</xml>')
.head 6 +  If mlHeader != ''
.head 7 -  Call SalFilePutStr( fhFile, '<p>'|| mlHeader||'</p>')
.head 6 -  Call SalFilePutStr( fhFile, '<table')
.head 6 -  Call SalFilePutStr( fhFile, 'datasrc="#xmlfile"')
.head 6 -  Call SalFilePutStr( fhFile, 'width="100%"')
.head 6 -  Call SalFilePutStr( fhFile, 'border="1">')
.head 6 -  Call SalFilePutStr( fhFile, '<thead>')
.head 6 -  Call SalFilePutStr( fhFile, fXML1)
.head 6 -  Call SalFilePutStr( fhFile, '</thead>')
.head 6 -  Call SalFilePutStr( fhFile, '<tr align="left">')
.head 6 -  Call SalFilePutStr( fhFile, fXML2)
.head 6 -  Call SalFilePutStr( fhFile, '</tr>')
.head 6 -  Call SalFilePutStr( fhFile, '</table>')
.head 6 -  Call SalFilePutStr( fhFile, '</body>')
.head 6 -  Call SalFilePutStr( fhFile, '</html>')
.head 6 -  Call SalFileClose( fhFile)
.head 6 -  Call SalLoadApp( '"C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE"', '"C:\\temp\\TempXML.html"')
.head 2 +  Window Parameters
.head 3 -  Window Handle: hWndTBL
.head 3 -  String: sOptionalHeaderText
.head 3 -  String: sOptionalFileName
.head 3 -  String: sOperation !	null = Display Form, walk though mode
			'CSV' = 'Default to CSV and don't display Form
			'EXCEL' = Default to Excel and don't display Form
			'CSV_AUTO' = Default to CSV and don't display Form
.head 3 -  String: sFieldSep
.head 3 -  String: sFieldSurr
.head 2 +  Window Variables
.head 3 -  String: sUseSurr[*]
.head 3 -  String: sUseExp[*]
.head 3 -  String: sTableName
.head 3 -  Boolean: bSetTRUES
.head 3 -  Boolean: bSetTRUEE
.head 3 -  Number: nCOL_COUNT
.head 3 -  Number: nCOL_COUNT_SHOWN
.head 3 -  ! Used to view XML
.head 3 -  String: sViewXML1
.head 3 -  String: sViewXML2
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set cbIncludeTitle = TRUE
.head 4 -  Call SalSendMsg( tblTable, SAM_Create,0,0 )
.head 4 +  If sFieldSep != ''
.head 5 -  Set dfFieldSep = sFieldSep
.head 4 +  Else
.head 5 -  Set dfFieldSep = '}'
.head 4 +  If sFieldSurr != ''
.head 5 -  Set dfFieldSurr = sFieldSurr
.head 4 +  Else
.head 5 -  Set dfFieldSurr = '"'
.head 4 -  ! Set dfFieldSep = '}'
.head 4 -  ! Set dfFieldSurr = '"'
.head 4 -  Set cbIncludeVisible = TRUE
.head 4 -  Call SalSendMsg( cbIncludeHeader, SAM_Click, 0,0)
.head 4 -  Call FillScreen( cbIncludeVisible)
.head 4 -  Set bSetTRUES = TRUE
.head 4 -  Set bSetTRUEE = TRUE
.head 4 -  Call SalGetItemName( hWndTBL, sTableName )
.head 4 -  Call SalSetWindowText( dlgExport, 'Export data from table: '||sTableName)
.head 4 -  Set dfRowEnd = SalTblSetRow( hWndTBL, TBL_SetLastRow)
.head 4 -  Set dfRowStart = 0
.head 4 -  Set dfDS = 'ROOT_ELEMENT'
.head 4 -  Set dfRS = 'CHILD_ELEMENT'
.head 4 -  Call SalDisableWindow( dfRowStart)
.head 4 -  Call SalDisableWindow( dfRowEnd)
.head 4 -  Call SalHideWindow ( dfAppPath)
.head 4 +  If sOptionalHeaderText != ''
.head 5 -  Set cbIncludeHeader = TRUE
.head 5 -  Call SalSendMsg( cbIncludeHeader, SAM_Click, 0,0)
.head 5 -  Set mlHeader = sOptionalHeaderText
.head 4 -  Set dfAppPath = GetExcelProgram( '.xls' )
.head 4 +  If sOperation = 'EXCEL'
.head 5 -  Call SalHideWindow ( dlgExport)
.head 5 -  Set rbExcel = TRUE
.head 5 -  Call SalSendMsg( rbExcel, SAM_Click, 0,0)
.head 5 -  Set cbLoadExcel = TRUE
.head 5 -  Call SalSendMsg( pbOk, SAM_Click, 0,0)
.head 4 +  Else If sOperation = 'XML'
.head 5 -  Call SalHideWindow ( dlgExport)
.head 5 -  Set rbXML = TRUE
.head 5 -  Call SalSendMsg( rbXML, SAM_Click, 0,0)
.head 5 -  Set cbLoadExcel = TRUE
.head 5 -  Call SalSendMsg( pbOk, SAM_Click, 0,0)
.head 4 +  Else If sOperation = 'CSV_AUTO'
.head 5 -  Call SalHideWindow ( dlgExport)
.head 5 -  Set rbCSV = TRUE
.head 5 -  Call SalSendMsg( rbCSV, SAM_Click, 0,0)
.head 5 -  Set cbLoadExcel = FALSE
.head 5 -  Call SalSendMsg( pbOk, SAM_Click, 0,0)
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 1 +  Dialog Box: dlgLabelHelp
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.513"
.head 4 -  Top: 0.104"
.head 4 -  Width:  7.338"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.813"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 41602
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 1.138"
.head 5 -  Top: 0.104"
.head 5 -  Width:  1.038"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Search Text
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfSearch
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 25
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.313"
.head 6 -  Top: 0.063"
.head 6 -  Width:  1.8"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Uppercase
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 -  Set nRowLabel = 0
.head 6 -  Set nSearchLength = SalStrLength( MyValue )
.head 6 -  Set sSearchText = SalStrLeftX( MyValue, nSearchLength )
.head 6 -  Call SalTblSetRowFlags( tblDisplay, nRowSelected, ROW_Selected, FALSE )
.head 6 +  While TRUE
.head 7 -  Set nRowLabel = VisTblFindString( tblDisplay, nRowLabel, tblDisplay.col2, MyValue )
.head 7 +  If sSearchText = SalStrUpperX( SalStrLeftX( tblDisplay.col2, nSearchLength ) )  or  nRowLabel = -1
.head 8 -  Break
.head 7 -  Set nRowLabel = nRowLabel + 1
.head 6 +  If nRowLabel > -1
.head 7 -  Call SalTblSetFocusRow( tblDisplay, nRowLabel + 10)
.head 7 -  Call SalTblSetRowFlags( tblDisplay, nRowLabel, ROW_Selected, TRUE )
.head 7 -  Call SalTblSetContext( tblDisplay, nRowLabel )
.head 7 -  Call SalTblSetFocusRow( tblDisplay, nRowLabel )
.head 7 -  Call SalSetFocus( dfSearch )
.head 6 -  Set nRowSelected = nRowLabel
.head 3 +  Pushbutton: pbProcess
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Process Selection
.head 4 -  Window Location and Size
.head 5 -  Left: 4.413"
.head 5 -  Top: 0.063"
.head 5 -  Width:  1.775"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: F12
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalSendMsg( tblDisplay, SAM_DoubleClick, 0, 0 )
.head 3 +  Child Table: tblDisplay
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 0.354"
.head 6 -  Width:  7.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 4.323"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 40000
.head 5 -  Discardable? No
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: col1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.871"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 1, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay, 1, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 5 +  Column: col2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 2, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay,2, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 5 +  Column: colRowID
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 3, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay, 3, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 4 -  Functions
.head 4 +  Window Variables
.head 5 -  Boolean: bIncr
.head 5 -  String: sSearch
.head 5 -  String: sSearch2
.head 5 -  Number: nFindRow
.head 4 +  Message Actions
.head 5 +  On SAM_DoubleClick
.head 6 -  Set sReturnRowid = colRowID
.head 6 -  Call SalEndDialog(dlgLabelHelp, 1)
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_RETURN
.head 7 -  Call SalSendMsg( tblDisplay, SAM_DoubleClick, 0, 0 )
.head 6 +  Else If wParam > 64 and wParam < 91
.head 7 -  Set sSearch = sSearch || SalNumberToChar( wParam )
.head 7 +  If SalStrScan( sSearch, ',' ) = -1
.head 8 -  Set sSearch2 = sSearch || '%,%'
.head 7 -  Set nFindRow = VisTblFindString( hWndForm, nFindRow, col2, sSearch2 )
.head 7 +  If nFindRow = -1
.head 8 -  Set nFindRow = 0
.head 8 -  Set sSearch = STRING_Null
.head 7 +  Else
.head 8 -  Call SalTblSetFocusRow( hWndForm, nFindRow )
.head 5 +  On SAM_Create
.head 6 -  Call SalTblSetTableFlags( tblDisplay, TBL_Flag_SelectableCols, TRUE )
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sType
.head 2 +  Window Variables
.head 3 -  String: sSearchText
.head 3 -  Number: nRowLabel
.head 3 -  Number: nRowSelected
.head 3 -  Number: nSearchLength
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 +  If sType = 'BOND'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, 'SELECT	bondsman, insurance_co, rowid
			           FROM	cr_bondsman
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	bondsman', TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'BONDSMAN')
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'INSURANCE CO')
.head 4 +  Else If sType = 'ATT'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	to_char(attno), lname||', '||fname||' '||mname, rowid
			           FROM	attorney
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	lname, fname, mname", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'ATT. #')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'ATT NAME')
.head 4 +  Else If sType = 'POLICE'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	code, agency, rowid
			           FROM	agency_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	code", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY CODE')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'AGENCY NAME')
.head 4 +  Else If sType = 'OTHERA'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	agencynm, rowid
			           FROM	other_agency
			           INTO	:tblDisplay.col1, :tblDisplay.colRowID
			           ORDER BY	agencynm", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY NAME')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1,7.088)
.head 5 -  Call SalHideWindow( tblDisplay.col2)
.head 4 +  Else If sType = 'PROBCS'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, 'SELECT	agency, description, rowid
			           FROM	program_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	agency', TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY NAME')
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col2, 'DESCRIPTION')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 1 +  Dialog Box: dlgCertifiedPrint
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Certified Status
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.413"
.head 4 -  Top: 0.729"
.head 4 -  Width:  5.925"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 2.552"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfCase
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 14
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 0.052"
.head 6 -  Width:  1.763"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfCertNo
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 24
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 0.365"
.head 6 -  Width:  2.875"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddrLine1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 120
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 0.802"
.head 6 -  Width:  5.063"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddrLine2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 60
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 1.083"
.head 6 -  Width:  5.063"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddrLine3
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 60
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 1.365"
.head 6 -  Width:  5.063"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddrLine4
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 60
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 1.646"
.head 6 -  Width:  5.063"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sCertNo
.head 3 -  Number: nPassCaseYr
.head 3 -  String: sPassCaseTy
.head 3 -  Number: nPassCaseNo
.head 3 -  String: sAddrLine1
.head 3 -  String: sAddrLine2
.head 3 -  String: sAddrLine3
.head 3 -  String: sAddrLine4
.head 3 -  Number: nPrintCnt
.head 2 +  Window Variables
.head 3 -  String: sStringCaseNo
.head 3 -  String: sSelectCertfied
.head 3 -  Number: nFldLength
.head 3 -  Number: nScanPosition
.head 3 -  Boolean: bPrintFlag
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set nFldLength = SalNumberToStr( nPassCaseNo, 0, sStringCaseNo )
.head 4 +  While nFldLength<4
.head 5 -  Set sStringCaseNo = '0' || sStringCaseNo
.head 5 -  Set nFldLength =SalStrLength( sStringCaseNo )
.head 4 -  Set dfCase=SalNumberToStrX( nPassCaseYr, 0) || '-' || sPassCaseTy || '-' || sStringCaseNo
.head 4 -  Set dfCertNo = SalStrMidX( sCertNo, 0, 4 ) || ' ' || SalStrMidX( sCertNo, 4, 4 ) || ' ' ||
	SalStrMidX( sCertNo, 8, 4 ) || ' ' ||  SalStrMidX( sCertNo, 12, 4 ) || ' ' ||  SalStrMidX( sCertNo, 16, 4 )
.head 4 -  Set dfCertNo = sCertNo
.head 4 -  Set sBarcodeEncrypted = Code128c(sCertNo)
.head 4 -  Set dlgCertifiedPrint.dfAddrLine1 = sAddrLine1
.head 4 -  Set dlgCertifiedPrint.dfAddrLine2 = sAddrLine2
.head 4 -  Set dlgCertifiedPrint.dfAddrLine3 = sAddrLine3
.head 4 -  Set dlgCertifiedPrint.dfAddrLine4 = sAddrLine4
.head 3 +  On SAM_CreateComplete
.head 4 -  Set nPrintErr = -1
.head 4 -  Set sReport = sCertifiedQrp
.head 4 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM, BARNUMBER, BARCODE, Username, CourtClerk'
.head 4 -  Set sReportBinds = 'dlgCertifiedPrint.dfAddrLine1, dlgCertifiedPrint.dfAddrLine2, dlgCertifiedPrint.dfAddrLine3, 
	dlgCertifiedPrint.dfAddrLine4, dfCase, sCertNo, sBarcodeEncrypted, sUClerk, sCourtClerk'
.head 4 -  Call SalReportPrint (dlgCertifiedPrint, sReport, sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 4 -  Call SalEndDialog(dlgCertifiedPrint, TRUE )
.head 4 +  If nPrintErr > 0
.head 5 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 3 +  On SAM_ReportStart
.head 4 -  Set bPrintFlag = TRUE
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If bPrintFlag
.head 5 -  Set bPrintFlag = FALSE
.head 5 -  Return TRUE
.head 4 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return TRUE
.head 1 +  Dialog Box: dlgDatasource
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Input Source
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.913"
.head 4 -  Top: 3.26"
.head 4 -  Width:  2.275"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.114"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfReturn
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.038"
.head 6 -  Top: 0.021"
.head 6 -  Width:  2.125"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbExit
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 0.542"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sDatasource = SalStrMidX( dfReturn, 1, 100)
.head 6 -  Call SalEndDialog(dlgDatasource, 1)
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  Receive String: sDatasource
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgPrintPicture
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Picture...
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modeless
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 7.425"
.head 4 -  Top: 4.188"
.head 4 -  Width:  2.55"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.385"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: 3D Face Color
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Picture: pJPhoto
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 0.063"
.head 5 -  Width:  1.744"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.094"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name:
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Size to Fit
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 1 +  Dialog Box: dlgPrintReport
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Printing...
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modeless
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top: Default
.head 4 -  Width:  Default
.head 4 -  Width Editable? Yes
.head 4 -  Height: Default
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: 3D Face Color
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 2900
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.038"
.head 5 -  Top: 0.302"
.head 5 -  Width:  4.85"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.271"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Arial Black
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: REPORT IS PRINTING
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 2901
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: -0.013"
.head 5 -  Top: 0.677"
.head 5 -  Width:  4.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: PLEASE WAIT . . .
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Boolean: bPrintOnce
.head 3 -  Boolean: bPrintGeneric
.head 3 -  Boolean: bPrintCustom
.head 3 -  Number: n
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set cReports.bPrinting = TRUE
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_Destroy
.head 4 -  Call Reset_Printer(  )
.head 4 -  Set cReports.bPrinting = FALSE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  ! Do init steps here per report, and set mode
.head 4 -  Set bPrintOnce = TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If bPrintOnce = TRUE
.head 5 -  Set bPrintOnce = FALSE
.head 5 -  Return TRUE
.head 4 +  Else If bPrintGeneric = TRUE
.head 5 -  Return SqlFetchNext( hSql, nResult )
.head 4 +  Else If bPrintCustom = TRUE
.head 5 -  ! Handle each report individually here
.head 4 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Call SalDestroyWindow( hWndForm )
.head 1 +  Dialog Box: dlgPrintDocket
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Print Job In Progress
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.325"
.head 4 -  Top: 2.375"
.head 4 -  Width:  4.486"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.781"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Bold
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 12405
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.186"
.head 5 -  Top: 0.25"
.head 5 -  Width:  4.043"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.229"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Printing Docket Please Wait....
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 +  Functions
.head 3 +  Function: PrintDocket
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: fDefendant
.head 5 -  Number: nOldCount
.head 4 +  Actions
.head 5 -  Set nPrintDocket=1
.head 5 +  If SalStrLeftX( fCaseTy, 2 ) = 'CV'
.head 6 -  Set nCVCaseYr = SalStrToNumber( fCaseYr )
.head 6 -  Set nCVCaseNo = SalStrToNumber( fCaseNo )
.head 6 -  Call SqlPrepareAndExecute(hSql, "Select Plaintiff || ' vs ' || Defendant
	from crim.casemaster  into  :fDefendant 
	where CaseYr=:nCVCaseYr and CaseNo=:nCVCaseNo ")
.head 6 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute (hSql, 'Select Agency 
	from crim.MUNI_BOOKING  into  :sDockAgency 
	where CaseYr=:fCaseYr  and CaseTy= :fCaseTy and CaseNo= :fCaseNo')
.head 6 -  Call SqlFetchNext(hSql,nReturn)
.head 6 -  Call SqlPrepareAndExecute(hSql, "Select LName || ', ' || FName || ' ' || MName || ' ' || Title, 	Address1 || '  ' || City || ', ' || State || ' ' || Zip
	from crim.CR_PARTIES into :fDefendant, :sAddressRD
	where CaseYr=:fCaseYr and CaseTy=:fCaseTy and CaseNo=:fCaseNo ")
.head 6 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If fReportType = 'D'
.head 6 -  Set sReport = CV_REPORT_Path || 'crdock.qrp'
.head 6 -  Set sReportBinds = 'sDockCase, fDefendant, sDockAgency, dDockDate, 
	sDockText, dDockRecDate, sDockRec, sDockRecCode, nDockAmount, sCourt, sCourtClerk, sAddressRD'
.head 6 -  Set sReportInputs = 'CASE, DEF, AGENCY, DDATE, 
	DTEXT, RDATE, REC, RCODE, AMOUNT, COURT, CLERK, Addr'
.head 5 +  Else If fReportType = 'R'
.head 6 -  ! Check for older Unix disb.  This report will not include those, but we can at least let the user know they exist.
.head 6 -  Call SqlPrepareAndExecute( hSql,"Select 	count(*)
			         FROM	CR_DisbMaster
			         INTO	:nOldCount
			         WHERE	(CaseYr=:fCaseYr and CaseTy=:fCaseTy and CaseNo=:fCaseNo) and
					disbdate < to_date('4-30-2002', 'MM-dd-yyyy')")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If nOldCount > 0
.head 7 -  Call SalMessageBox( 'Disbursements exist for this case that are prior to the conversion date of 4-30-2002.  These will not be printed on this report.', 'Old Disbursement records', MB_Ok| MB_IconAsterisk )
.head 6 -  Set sReport = CV_REPORT_Path || 'crdock2.qrp'
.head 6 -  Set sReportBinds = 'sDockCase, :fDefendant,sDockAgency,dTranDateRD, nRecCheckNoRD,
		  sCashierRD, sTypeRD, sAdjRD, sLedgerRD, sStatute_DepRD,
		  nAmountRD, nTotalRD,sCourt, sCourtClerk, sReportBreakRD,
		  sDateHeader, sRec_CheckHeader, sStat_DepHeader, sAddressRD'
.head 6 -  Set sReportInputs = 'CASE,DEF,AGENCY,TRANDATE, REC_CHECK,
		  CASHIER, TYPE, ADJUSTMENT, LEDGER, STATUTE,
		  AMOUNT, TOTAL, COURT, CLERK, RPT_TYPE,
		  TRAN_DATE_HDR, REC_CHECK_HDR, STAT_DEP_HDR, Addr'
.head 5 +  If fPrintType = 'SCREEN'
.head 6 -  Call SalReportView( dlgPrintDocket, hWndNULL, sReport,
    sReportBinds, sReportInputs, nPrintErr )
.head 5 +  Else If fPrintType = 'PRINTER'
.head 6 -  Call SalReportPrint(hWndForm,sReport, sReportBinds, sReportInputs,1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintDocket)
.head 2 +  Window Parameters
.head 3 -  String: fCaseYr
.head 3 -  String: fCaseTy
.head 3 -  String: fCaseNo
.head 3 -  String: fPrintType
.head 3 -  String: fReportType   ! 'D' = Docket Normal, 'R' = Receipt/Disbursements
.head 2 +  Window Variables
.head 3 -  ! !!!!!!! VARIABLES FOR PRINTING DOCKET
.head 3 -  Boolean: bDockDone
.head 3 -  Date/Time: dDockDate
.head 3 -  Long String: sDockText
.head 3 -  String: sDockRec
.head 3 -  String: sDockRecCode
.head 3 -  Date/Time: dDockRecDate
.head 3 -  String: nDockAmount
.head 3 -  Number: nDockAmountNo
.head 3 -  String: sDockCase
.head 3 -  Number: nPrintDocket
.head 3 -  ! !!!!!!! VARIABLES FOR PRINTING DOCKET - END
.head 3 -  Date/Time: dTranDateRD
.head 3 -  Number: nRecCheckNoRD
.head 3 -  String: sCashierRD
.head 3 -  String: sTypeRD
.head 3 -  String: sAdjRD
.head 3 -  String: sLedgerRD
.head 3 -  String: sStatute_DepRD
.head 3 -  Number: nAmountRD
.head 3 -  Number: nTotalRD
.head 3 -  Number: nCVCaseYr
.head 3 -  Number: nCVCaseNo
.head 3 -  String: sReportBreakRD
.head 3 -  String: sDateHeader
.head 3 -  String: sRec_CheckHeader
.head 3 -  String: sStat_DepHeader
.head 3 -  String: sAddressRD
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call PrintDocket(  )
.head 4 -  Call SalEndDialog( hWndForm, 1 )
.head 3 +  On SAM_ReportStart
.head 4 -  Set bDockDone=FALSE
.head 4 +  If fReportType = 'D'
.head 5 -  Set dDockDate=SalStrToDate(STRING_Null)
.head 5 -  Set sDockText=STRING_Null
.head 5 -  Set sDockRec=STRING_Null
.head 5 -  Set sDockRecCode=STRING_Null
.head 5 -  Set dDockRecDate=SalStrToDate(STRING_Null)
.head 5 -  Set nDockAmount=STRING_Null
.head 5 -  Set nDockAmountNo=SalStrToNumber(STRING_Null)
.head 5 +  If SalStrLeftX( fCaseTy, 2 ) = 'CV'
.head 6 -  Call SqlPrepare(hSql, "Select d.DDate, c.code1 || d.entry1 || c.code2 || d.entry2 || c.code3 || d.entry3 || c.code4 || d.entry4 || ' ' || d.memo 
	from crim.DOCKET d, cvcodes c  into  :dDockDate, :sDockText
	where d.code5=c.code and d.CaseYr=:nCVCaseYr and d.CaseNo=:nCVCaseNo
	order by d.DDate, d.dnum, d.RowId ")
.head 5 +  Else
.head 6 -  Call SqlPrepare(hSql, "SELECT		Dock_Date, Data || '     '
		        FROM		CR_DOCKET
		        WHERE		CaseYr=:fCaseYr  and
					CaseTy=:fCaseTy  and
					CaseNo= :fCaseNo
		        INTO		:dDockDate, :sDockText
	order by Dock_Date, Seq, RowId ")
.head 5 -  Call SqlExecute(hSql)
.head 4 +  Else If fReportType = 'R' ! Receipt /Disbursement Report
.head 5 -  Set dTranDateRD=SalStrToDate(STRING_Null)
.head 5 -  Set nRecCheckNoRD=SalStrToNumber(STRING_Null)
.head 5 -  Set sCashierRD=STRING_Null
.head 5 -  Set sTypeRD=STRING_Null
.head 5 -  Set sAdjRD=STRING_Null
.head 5 -  Set sLedgerRD=STRING_Null
.head 5 -  Set sStatute_DepRD=STRING_Null
.head 5 -  Set nAmountRD=SalStrToNumber(STRING_Null)
.head 5 -  Set nTotalRD=SalStrToNumber(STRING_Null)
.head 5 -  Set sDateHeader = 'Rec Date'
.head 5 -  Set sRec_CheckHeader = 'Receipt #'
.head 5 -  Set sStat_DepHeader = 'Statute'
.head 5 -  Set sReportBreakRD = ''
.head 5 +  If SalStrLeftX( fCaseTy, 2 ) = 'CV'
.head 6 -  Call SqlPrepare(hSql, "Select m.RecDate, m.Receiptno, m.Cashierid || ' / ' || m.drawer, m.method, m.adj, d.Ledgercode, null, d.recamt, 'RECEIPT'
	from crim.receiptmaster m, crim.recdisdetail d 
	where m.CaseYr=:nCVCaseYr and m.CaseNo=:nCVCaseNo and m.receiptno = d.receiptno 
	into :dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sTypeRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
	order by 1,2")
.head 5 +  Else
.head 6 -  Call SqlPrepare(hSql,"Select m.Dat, m.Receiptno, m.Cashierno || ' / ' || m.Userid, m.Pay_Type, m.Void, m.Ledger, m.Offenseno, m.Amount, 'RECEIPT'
	from crim.CR_RECMaster m
	where (m.CaseYr=:fCaseYr and m.CaseTy=:fCaseTy and m.CaseNo=:fCaseNo)
	Union all
		Select c.dat,c.receiptno, c.cashierno, c.pay_type, '', '', '', c.totamount, 'RECEIPT'
		from crim.cr_receipt2 c
		where c.caseyr =:fCaseYr and c.casety = :fCaseTy and c.caseno = :fCaseNo and
		             transfered is null
	into :dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sTypeRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
	order by 1,2")
.head 5 -  Call SqlExecute(hSql)
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If fReportType = 'D'
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If SalStrRightX( sDockText, 5 ) = '     '
.head 6 -  Set sDockText = VisStrSubstitute( sDockText, '     ', STRING_Null )
.head 5 +  If nReturn=FETCH_EOF
.head 6 +  If bDockDone=TRUE
.head 7 -  Return FALSE
.head 6 +  Else
.head 7 -  Set bDockDone=TRUE
.head 7 -  Set dDockDate=SalStrToDate(STRING_Null)
.head 7 -  Set sDockText=STRING_Null
.head 7 +  If SalStrLeftX( fCaseTy, 2 ) = 'CV'
.head 8 -  Call SqlPrepareAndExecute(hSql, "Select m.RecDate, m.Receiptno, d.Ledgercode, d.recamt 
	from crim.receiptmaster m, crim.recdisdetail d 
	where m.CaseYr=:nCVCaseYr and m.CaseNo=:nCVCaseNo and m.receiptno = d.receiptno 
	into :dDockRecDate, :sDockRec, :sDockRecCode, :nDockAmountNo
	order by m.receiptno")
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute (hSql,'SELECT	Dat, ReceiptNo, Ledger, Amount
		        FROM		CR_RECMASTER
		        WHERE		CaseYr=:fCaseYr and CaseTy= :fCaseTy and CaseNo= :fCaseNo
                                        INTO		:dDockRecDate, :sDockRec, :sDockRecCode, :nDockAmountNo
		        ORDER BY		DAT')
.head 7 -  Call SqlFetchNext(hSql,nReturn)
.head 7 +  If nDockAmountNo >0
.head 8 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 8 -  Set nDockAmount='$'||nDockAmount
.head 8 -  Set sDockRec='Receipt      '||sDockRec
.head 8 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 7 -  Return TRUE
.head 5 +  Else
.head 6 -  Set sDockCase= fCaseYr||' '||fCaseTy||' '||fCaseNo
.head 6 +  If nDockAmountNo >0
.head 7 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 7 -  Set nDockAmount='$'||nDockAmount
.head 7 -  Set sDockRec='Receipt      '||sDockRec
.head 7 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 6 -  Return TRUE
.head 4 +  Else If fReportType = 'R'
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If nReturn=FETCH_EOF
.head 6 +  If bDockDone=TRUE
.head 7 -  Return FALSE
.head 6 +  Else
.head 7 -  Set bDockDone=TRUE
.head 7 -  Set dTranDateRD=SalStrToDate(STRING_Null)
.head 7 -  Set nRecCheckNoRD=SalStrToNumber(STRING_Null)
.head 7 -  Set sCashierRD=STRING_Null
.head 7 -  Set sTypeRD=STRING_Null
.head 7 -  Set sAdjRD=STRING_Null
.head 7 -  Set sLedgerRD=STRING_Null
.head 7 -  Set sStatute_DepRD=STRING_Null
.head 7 -  Set nAmountRD=SalStrToNumber(STRING_Null)
.head 7 -  Set nTotalRD=SalStrToNumber(STRING_Null)
.head 7 -  Set sDateHeader = 'Dis Date'
.head 7 -  Set sRec_CheckHeader = 'Check #'
.head 7 -  Set sStat_DepHeader = 'Depositor'
.head 7 -  Set sReportBreakRD = ''
.head 7 -  Call SqlPrepare(hSql,"Select M.DisbDate, M.BankNo, M.Userid, M.CheckStatus, D.Ledger, M.Depositor, D.Amount, 'DISBURSEMENT'
   from	CR_DisbMaster M, CR_DisbDetail D
   into	:dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
   where	(M.CheckNo = D.CheckNo) and
	(M.CaseYr=:fCaseYr and M.CaseTy=:fCaseTy and M.CaseNo=:fCaseNo) and
	M.disbdate >=to_date('4-30-2002', 'MM-dd-yyyy')
	order by M.DisbDate, D.Ledger")
.head 7 -  Call SqlExecute(hSql)
.head 7 -  Call SqlFetchNext(hSql,nReturn)
.head 7 +  If nReturn = FETCH_EOF
.head 8 -  Set dTranDateRD=SalStrToDate(STRING_Null)
.head 8 -  Set nRecCheckNoRD=SalStrToNumber(STRING_Null)
.head 8 -  Set sCashierRD=STRING_Null
.head 8 -  Set sTypeRD=STRING_Null
.head 8 -  Set sAdjRD=STRING_Null
.head 8 -  Set sLedgerRD=STRING_Null
.head 8 -  Set sStatute_DepRD=STRING_Null
.head 8 -  Set nAmountRD=SalStrToNumber(STRING_Null)
.head 8 -  Set nTotalRD=SalStrToNumber(STRING_Null)
.head 8 -  Set sReportBreakRD = ''
.head 7 +  Else
.head 8 -  Return TRUE
.head 5 +  Else
.head 6 -  Set sDockCase= fCaseYr||' '||fCaseTy||' '||fCaseNo
.head 6 +  ! If nDockAmountNo >0
.head 7 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 7 -  Set nDockAmount='$'||nDockAmount
.head 7 -  Set sDockRec='Receipt      '||sDockRec
.head 7 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 6 -  ! Return TRUE
.head 6 +  If nAmountRD = 0
.head 7 -  Set dTranDateRD=SalStrToDate(STRING_Null)
.head 7 -  Set nRecCheckNoRD=SalStrToNumber(STRING_Null)
.head 7 -  Set sCashierRD=STRING_Null
.head 7 -  Set sTypeRD=STRING_Null
.head 7 -  Set sAdjRD=STRING_Null
.head 7 -  Set sLedgerRD=STRING_Null
.head 7 -  Set sStatute_DepRD=STRING_Null
.head 7 -  Set nAmountRD=SalStrToNumber(STRING_Null)
.head 7 -  Set nTotalRD=SalStrToNumber(STRING_Null)
.head 7 -  Set sReportBreakRD = ''
.head 6 +  Else
.head 7 -  Return TRUE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 1 +  Dialog Box: dlgPasswordAPL
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Change Password
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.163"
.head 4 -  Top: 1.813"
.head 4 -  Width:  4.06"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.723"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfCurrentPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.242"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfNewPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.575"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfVerifyPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.908"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChange
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Change Password
.head 4 -  Window Location and Size
.head 5 -  Left: 0.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  ! Set dfCurrentPass = SalStrUpperX( dfCurrentPass )
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '.')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '>')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '<')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ',')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '/')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '?')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '~')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '`')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '!')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '@')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '#')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '%')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '^')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '$')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '&')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '*')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '(')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ')')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '-')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '_')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '+')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '=')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ':')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ';')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '"')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, "'")
.head 6 +  If SalStrLength( dfNewPass ) < 6
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('A Password Must be at Least 6 Charaters Long', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX(dfNewPass) != SalStrUpperX(dfVerifyPass)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Passwords do not Match', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX(dfCurrentPass) != SalStrUpperX(SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Current Password must be Re-entered before Password can be Changed','Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX( dfVerifyPass) =SalStrUpperX(  SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox( 'Password Must be UNIQUE, please re-enter a valid password', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  ! Else If nCheckIllegal > -1
.head 7 -  Call SalMessageBox( "Password can not contain illegal characters such as
 	(. ; : ! @ # $ % ^ & * ( ) - _ + = ~ ` ' \" ? / \ | < > , ).
Please re-enter a valid password", 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SqlConnect (hSql)
.head 6 +  If SqlPLSQLCommand(hSql, 'change_pwd(SqlUser,dfNewPass)' )
.head 7 -  Call SalMessageBox( 'Password Successfully Changed', 'Change Password',MB_Ok|MB_IconExclamation)
.head 7 -  Call SqlPrepareAndExecute( hSql,"Update crim.users set change_password = 'N' ,gracelogins = 5 where "||sSelectWhere)
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Set SqlPassword = dfNewPass
.head 7 -  Set bChange = FALSE
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Password Change failed - - Contact Programmer', 'Change Password',MB_Ok|MB_IconStop)
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton(pbChange)
.head 6 -  Call SalSetFocus(dfCurrentPass)
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 2.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 52744
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.258"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Current Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd4
.head 4 -  Resource Id: 52745
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.592"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: New Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 52746
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.925"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Retype Password:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nErrorCnt
.head 3 -  Boolean: bOk
.head 3 -  Number: nCheckIllegal
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set nErrorCnt = 0
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 4 -  Call SalWaitCursor( FALSE )
.head 3 +  On SAM_Destroy
.head 4 +  If bChange = TRUE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Update crim.users set gracelogins = gracelogins - 1 where username = :SqlUser' )
.head 5 -  Call SqlCommit( hSql )
.head 5 -  Set bChange = FALSE
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgMapDriveWait
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Waiting on Network
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.313"
.head 4 -  Top: 0.885"
.head 4 -  Width:  6.013"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.885"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 21213
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.088"
.head 5 -  Top: 0.052"
.head 5 -  Width:  5.775"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.365"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Times New Roman
.head 4 -  Font Size: 20
.head 4 -  Font Enhancement: Bold
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Canton Municipal Court
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd4
.head 4 -  Resource Id: 21214
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.088"
.head 5 -  Top: 0.375"
.head 5 -  Width:  5.775"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Scanned Images
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 21215
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.088"
.head 5 -  Top: 0.948"
.head 5 -  Width:  5.775"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Blue
.head 4 -  Background Color: Default
.head 4 -  Title: Please be patient while the system connects to the Courts Images....
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfCLWCount
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.075"
.head 6 -  Top: 1.5"
.head 6 -  Width:  0.825"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: #0
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Abort Wait Process
.head 4 -  Window Location and Size
.head 5 -  Left: 2.063"
.head 5 -  Top: 1.479"
.head 5 -  Width:  1.838"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalTimerKill( hWndForm, 1 )
.head 6 -  Call SalEndDialog( hWndForm, 1 )
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sOpenType
.head 2 +  Window Variables
.head 3 -  String: sCLWError
.head 3 -  Boolean: bFirstLoop
.head 3 -  Number: nLoadAppRet
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow( hWndForm )
.head 4 -  Call CSCDisableNonEditable ( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call SalTimerSet( hWndForm, 1, 500 )
.head 3 +  On SAM_Timer
.head 4 +  If Not bFirstLoop
.head 5 -  Set bFirstLoop = TRUE
.head 5 -  ! Call SalLoadApp( 'NetUseAttach.bat', '/user:jail@courts2000.gov photos' )  !Window_NotVisible
.head 5 +  If sOpenType = 'Write'
.head 6 -  Call SalLoadAppAndWait( 'NetUseAttach.bat /user:ScanIn@courts2000.gov courtscan I:', Window_NotVisible, nLoadAppRet )  ! Window_NotVisible
.head 5 +  Else
.head 6 -  Call SalLoadAppAndWait( 'NetUseAttach.bat /user:jail@courts2000.gov photos I:', Window_NotVisible, nLoadAppRet )  ! Window_NotVisible
.head 4 -  Set dfCLWCount = dfCLWCount + 1
.head 4 +  If bDriveMapped
.head 5 -  Call SalTimerKill( hWndForm, 1 )
.head 5 -  Call SalEndDialog( hWndForm, 1 )
.head 4 -  Set bDriveMapped = VisDosExist( '\\\\192.168.38.5\\Imaging\\Images\\Connect.txt' )
.head 3 +  On SAM_Close
.head 4 -  Call SalSendMsg( pbCancel, SAM_Click, 0, 0 )
.head 1 +  Dialog Box: dlgAbout
.head 2 -  Class: cQuickTabsDialog
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Class Default
.head 2 -  Display Settings
.head 3 -  Display Style? Class Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Class Default
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.8"
.head 4 -  Top: 1.604"
.head 4 -  Width:  5.383"
.head 4 -  Width Editable? Class Default
.head 4 -  Height: Class Default
.head 4 -  Height Editable? Class Default
.head 3 -  Absolute Screen Location? Class Default
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 8
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Class Default
.head 3 -  Background Color: Class Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 3 -  Allow Child Docking? No
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Class Default
.head 4 -  Location? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Size: Class Default
.head 4 -  Size Editable? Class Default
.head 4 -  Docking Toolbar? Class Default
.head 4 -  Toolbar Docking Orientation: Class Default
.head 4 -  Font Name: Class Default
.head 4 -  Font Size: Class Default
.head 4 -  Font Enhancement: Class Default
.head 4 -  Text Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  Resizable? Class Default
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? Class Default
.head 2 +  Contents
.head 3 +  Picture: picTabs
.data CLASSPROPS
0000: 5400610062004C00 6500660074004D00 6100720067006900 6E000000FFFE0400
0020: 3000000054006100 6200430075007200 720065006E007400 0000FFFE0E007400
0040: 62004D0061006900 6E00000054006100 6200520069006700 680074004D006100
0060: 7200670069006E00 0000FFFE04003000 0000540061006200 4E0061006D006500
0080: 73000000FFFE3000 740062004D006100 69006E0009007400 6200520065007000
00A0: 6F00720074007300 0900740062005000 7200650076000000 5400610062004C00
00C0: 6100620065006C00 73000000FFFE4E00 4D00610069006E00 090049006E006300
00E0: 6C00750064006500 2000520065007000 6F00720074007300 0900500072006500
0100: 760069006F007500 7300200052006500 7600690073006900 6F006E0000005400
0120: 6100620050006100 6700650043006F00 75006E0074000000 FFFE040031000000
0140: 5400610062004200 6F00740074006F00 6D004D0061007200 670069006E000000
0160: FFFE040030000000 5400610062004400 7200610077005300 740079006C006500
0180: 0000FFFE16005700 69006E0039003500 5300740079006C00 6500000054006100
01A0: 620046006F007200 6D00500061006700 650073000000FFFE 0600090009000000
01C0: 5400610062005400 6F0070004D006100 7200670069006E00 0000FFFE04003000
01E0: 0000
.enddata
.data CLASSPROPSSIZE
0000: E201
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 1
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cQuickTabsDialog
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: Class Default
.head 5 -  Top: Class Default
.head 5 -  Width:  Class Default
.head 5 -  Width Editable? Class Default
.head 5 -  Height: Class Default
.head 5 -  Height Editable? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Editable? Class Default
.head 4 -  File Name:
.head 4 -  Storage: Class Default
.head 4 -  Picture Transparent Color: Class Default
.head 4 -  Fit: Class Default
.head 4 -  Scaling
.head 5 -  Width:  Class Default
.head 5 -  Height:  Class Default
.head 4 -  Corners: Class Default
.head 4 -  Border Style: Class Default
.head 4 -  Border Thickness: Class Default
.head 4 -  Tile To Parent? Class Default
.head 4 -  Border Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 3.95"
.head 5 -  Top: 1.762"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgAbout, 1)
.head 3 -  Background Text: bkgd3
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50572
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.619"
.head 5 -  Width:  1.3"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Program Name:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd4
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50573
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.976"
.head 5 -  Width:  1.233"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Revision Date:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd5
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50574
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 1.333"
.head 5 -  Width:  1.467"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Revision Number:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfAboutName
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 0.619"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAboutRDate
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 0.976"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: DateTime
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfAboutRNumber
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 1.333"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  List Box: lbReportFile
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620052006500 70006F0072007400 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.488"
.head 5 -  Width:  3.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.571"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Multiple selection? No
.head 4 -  Sorted? Yes
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Horizontal Scroll? No
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  List Initialization
.head 4 -  Message Actions
.head 3 +  Child Table: tblPrev
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 7400620050007200 650076000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.233"
.head 6 -  Top: 0.476"
.head 6 -  Width:  4.833"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 1.25"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? No
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: 8
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Class Default
.head 5 -  Discardable? Class Default
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colPrevDate
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Date
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Left
.head 6 -  Width:  0.9"
.head 6 -  Width Editable? Yes
.head 6 -  Format: MM-dd-yy
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colPrev
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colProblem
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 500
.head 6 -  Data Type: Long String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colNote
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 500
.head 6 -  Data Type: Long String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalMessageBox(  'REVISION DATE: '|| SalFmtFormatDateTime( tblPrev.colPrevDate, 'MM-dd-yyyy' ) ||'

PROBLEM: '||  tblPrev.colProblem ||'

NOTE/FIX: '|| tblPrev.colNote, tblPrev.colPrev, MB_Ok|MB_IconInformation)
.head 2 +  Functions
.head 3 +  Function: FillReportFile
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMax
.head 5 -  Number: n
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 -  Call SalListPopulate( dlgAbout.lbReportFile, hSql, 'SELECT	dep_name
			      FROM	admin.app_depend@admin_link
			      WHERE	UPPER(app_name) like UPPER(:sAppName)')
.head 3 +  Function: FillPrev
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 -  Call SalTblPopulate( tblPrev, hSql, 'SELECT	mod_label, note, problem, timestamp
			       FROM	admin.app_mod@admin_link
			       INTO		:tblPrev.colPrev, :tblPrev.colNote, :tblPrev.colProblem, :tblPrev.colPrevDate
			       WHERE	UPPER(app_name) like UPPER( :sAppName)', TBL_FillAll)
.head 3 +  Function: FillMain
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 +  If sAboutRNumber != ''
.head 6 -  Set dfAboutRDate = dtAboutRDate
.head 6 -  Set dfAboutRNumber = sAboutRNumber
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSql, 'SELECT	timestamp, seq_no
			       FROM	admin.app_mod@admin_link
			       INTO		:dlgAbout.dfAboutRDate, :dlgAbout.dfAboutRNumber
			       WHERE	UPPER(app_name) like UPPER( :sAppName)
			       ORDER BY	seq_no desc')
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 2 +  Window Parameters
.head 3 -  String: sAboutName
.head 3 -  Date/Time: dtAboutRDate
.head 3 -  String: sAboutRNumber
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow( hWndForm )
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 4 -  Set dfAboutName = sAboutName
.head 4 -  Call FillMain( )
.head 4 -  Call SalSetWindowText( hWndForm, 'About '||sAboutName )
.head 4 -  Call FillReportFile( )
.head 4 -  Call FillPrev( )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call picTabs.BringToTop( 0, TRUE)
.head 1 +  Form Window: frmLabels
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Normal
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top: Default
.head 4 -  Width:  12.0"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 7.271"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 2 -  Contents
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nRptFlag
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set bfrmLabels = TRUE
.head 4 -  Call SalHideWindow(frmLabels)
.head 3 +  On SAM_Destroy
.head 4 -  Set bfrmLabels = FALSE
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlag=0
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 -  Set nRptFlag=nRptFlag+1
.head 4 +  If nRptFlag=1
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 1 +  Form Window: frmCRCertified
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? Yes
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Maximized
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 0.275"
.head 4 -  Top: 0.229"
.head 4 -  Width:  12.0"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 7.396"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Black
.head 3 -  Background Color: Default
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 +  Menu
.head 3 +  Popup Menu: &File
.head 4 -  Resource Id: 18020
.head 4 -  Picture File Name:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: E&xit
.head 5 -  Resource Id: 18021
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text: Exit this form
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalPostMsg( hWndForm, SAM_Close, 0, 0 )
.head 5 -  Menu Item Name:
.head 3 -  !
.head 3 +  Popup Menu: &Edit
.head 4 -  Resource Id: 18022
.head 4 -  Picture File Name:
.head 4 -  Enabled when:
.head 4 -  Status Text: Undo, Cut, Copy, Paste, Clear
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Undo
.head 5 -  Resource Id: 18023
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: Alt+BkSp
.head 5 -  Status Text: Reverses the last action
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanUndo()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditUndo()
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: Cu&t
.head 5 -  Resource Id: 18024
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: Shift+Del
.head 5 -  Status Text: Cuts the selection and puts it on the clipboard
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanCut()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditCut()
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Copy
.head 5 -  Resource Id: 18025
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: Ctrl+Ins
.head 5 -  Status Text: Copies the selection and puts it on the clipboard
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanCut()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditCopy()
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Paste
.head 5 -  Resource Id: 18026
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: Shift+Ins
.head 5 -  Status Text: Inserts the Clipboard contents at the insertion point
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanPaste()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditPaste()
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: C&lear
.head 5 -  Resource Id: 18027
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: Del
.head 5 -  Status Text: Clears the selection
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanCut()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditClear()
.head 5 -  Menu Item Name:
.head 3 +  Popup Menu: &Retrieve
.head 4 -  Resource Id: 18028
.head 4 -  Picture File Name:
.head 4 -  Enabled when:
.head 4 -  Status Text: Enter criteria and execute a query to build a specific result set
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Enter:
.head 5 -  Resource Id: 18029
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: F1
.head 5 -  Status Text: Enter criteria for result set by Case No, Case No/Date, or Date
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set bRetrieve = TRUE
.head 6 -  Set hWndDatafield = SalGetFirstChild ( hWndForm, TYPE_DataField  | TYPE_MultilineText )
.head 6 -  Set hWndSave = hWndDatafield
.head 6 +  While hWndDatafield != hWndNULL
.head 7 -  Call SalClearField( hWndDatafield )
.head 7 -  Set hWndDatafield = SalGetNextChild( hWndDatafield, TYPE_DataField | TYPE_MultilineText )
.head 6 -  Call SalSetFocus( hWndSave )
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: E&xecute
.head 5 -  Resource Id: 18030
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: F12
.head 5 -  Status Text: Retrieve requested record(s)
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call Retrieve(STRING_Null)
.head 5 -  Menu Item Name:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: 0.646"
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Arial
.head 4 -  Font Size: 9
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 +  Contents
.head 4 -  Background Text: bkgd6
.head 5 -  Resource Id: 18032
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Window Location and Size
.head 6 -  Left: 0.125"
.head 6 -  Top: 0.083"
.head 6 -  Width:  1.117"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.167"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Justify: Left
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Black
.head 5 -  Background Color: 3D Face Color
.head 5 -  Title: Article No:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 4 +  Data Field: dfArticleNo
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class: cDfAutoTab
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Data
.head 6 -  Maximum Data Length: 20
.head 6 -  Data Type: String
.head 6 -  Editable? Yes
.head 5 -  Display Settings
.head 6 -  Window Location and Size
.head 7 -  Left: 0.075"
.head 7 -  Top: 0.281"
.head 7 -  Width:  2.314"
.head 7 -  Width Editable? Class Default
.head 7 -  Height: 0.25"
.head 7 -  Height Editable? Class Default
.head 6 -  Visible? Class Default
.head 6 -  Border? Class Default
.head 6 -  Justify: Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Font Name: Class Default
.head 6 -  Font Size: Class Default
.head 6 -  Font Enhancement: Class Default
.head 6 -  Text Color: Class Default
.head 6 -  Background Color: Class Default
.head 6 -  Input Mask: Unformatted
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Spell Check? No
.head 5 +  Message Actions
.head 6 +  On SAM_AnyEdit
.head 7 +  If bRetrieve = FALSE
.head 8 +  If bFormDirty = FALSE
.head 9 -  Set bFormDirty = TRUE
.head 9 -  Call SalStatusSetText(hWndForm, 'This record has been modified' )
.head 8 -  ! Call SalSendClassMessage(SAM_AnyEdit, 0, 0 )
.head 7 +  If SalStrLength( MyValue ) = 20
.head 8 -  Call SalSetFieldEdit( MyValue, FALSE )
.head 8 -  Call SalSendMsg( MyValue, SAM_Validate, 0, 0 )
.head 6 +  On SAM_Validate
.head 7 +  If SalStrLength( MyValue ) = 20
.head 8 -  Set nRow = VisTblFindString( tblCertCRProcess, 0, colCrimArticle, dfArticleNo )
.head 8 +  If nRow < 0
.head 9 -  !
.head 9 -  Set sSelectString = 'Select m.articleno, m.caseyr, c.casetype, m.caseno, m.ddate, m.code5, m.dnum,
		confirmed, addressline1, addressline2, addressline3, addressline4, print, batch, d.username, d.data
	from crim.cr_certified m, crim.muni_booking c, crim.cr_docket d c into :colCrimArticle, :colCrimCaseYr, :colCrimCaseTy, :colCrimCaseNo, :colCrimDate, :colCrimCode5, :colCrimDNum,
		:colCrimConfirmed, :colCrimAddress1, :colCrimAddress2, :colCrimAddress3, :colCrimAddress4, :colCrimPrint, :colCrimBatch, :colCrimClerk, :colCrimData
	where m.caseyr=c.caseyr and m.caseno=c.caseno and
		m.caseyr=d.caseyr and m.caseno=d.caseno and m.ddate=d.ddate and m.dnum=d.dnum and m.code5=d.code5(+) '
.head 9 -  Set sSelectString = sSelectString || 'and m.articleno = :dfArticleNo '
.head 9 -  Set nRow = SalTblInsertRow( tblCertCRProcess, TBL_MaxRow )
.head 9 -  Call SqlPrepareAndExecute( hSql, sSelectString )
.head 9 +  If SqlFetchNext( hSql, nResult )
.head 10 -  Call SalSendMsg( tblCertCRProcess, SAM_FetchRowDone, 0, 0 )
.head 10 +  If colCrimBatch != cmbBatch and Not SalIsNull( colCrimBatch )
.head 11 -  Set colCrimPrintCnt = 0
.head 11 -  Call SalMessageBox( 'Certified Mail has already been uploaded to the Post Office

on ' || SalFmtFormatDateTime( colCrimConfirmed, 'ddd, MMM d yyyy' ) || '.  The mail piece should be mailed as soon as possible.

The Mail piece can not be uploaded again.', 'Mail upload warning', MB_IconInformation | MB_Ok )
.head 11 -  ! Return TRUE
.head 10 +  Else If colCrimBatch = cmbBatch or SalIsNull( colCrimBatch )
.head 11 +  If colCrimPrint = STRING_Null
.head 12 -  Set colCrimPrint = 'Y'
.head 11 -  Set colCrimPrintCnt = 1
.head 11 -  Set colCrimConfirmed = SalDateCurrent(  )
.head 11 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_New, FALSE )
.head 11 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_Edited, TRUE )
.head 10 -  Call SalClearField( dfArticleNo )
.head 9 +  Else
.head 10 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_New, FALSE )
.head 10 -  Call SalMessageBox( 'Certified Number Not Found in Table', 'Not Found', MB_Ok )
.head 8 +  Else
.head 9 -  Call SalTblSetContext( tblCertCRProcess, nRow )
.head 9 -  Call SalClearField( dfArticleNo )
.head 9 +  If SalIsNull (colCrimConfirmed)
.head 10 -  Set colCrimPrint = 'Y'
.head 10 -  Set colCrimPrintCnt = 1
.head 10 -  Set colCrimConfirmed = SalDateCurrent(  )
.head 10 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_Edited, TRUE )
.head 9 +  Else If SalIsNull (colCrimPrint)
.head 10 -  Set colCrimPrint = 'Y'
.head 10 -  Set colCrimPrintCnt = 1
.head 10 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_Edited, TRUE )
.head 9 +  Else If colCrimBatch != cmbBatch and Not SalIsNull( colCrimBatch )
.head 10 -  Set colCrimPrintCnt = 0
.head 10 -  Call SalMessageBox( 'Certified Mail has already been uploaded to the Post Office

on ' || SalFmtFormatDateTime( colCrimConfirmed, 'ddd, MMM d yyyy' ) || '.  The mail piece should be mailed as soon as possible.

The Mail piece can not be uploaded again.', 'Mail upload warning', MB_IconInformation | MB_Ok )
.head 9 +  Else
.head 10 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_New, TRUE )
.head 8 -  Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 4 +  Pushbutton: pbShowMail
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Display Mail
.head 5 -  Window Location and Size
.head 6 -  Left: 3.05"
.head 6 -  Top: 0.052"
.head 6 -  Width:  2.338"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Call Retrieve(STRING_Null)
.head 4 +  Radio Button: rbUnPrinted
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: UnPrinted
.head 5 -  Window Location and Size
.head 6 -  Left: 5.533"
.head 6 -  Top: 0.083"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 8
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbConfirmed
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Confirmations
.head 5 -  Window Location and Size
.head 6 -  Left: 2.567"
.head 6 -  Top: 0.354"
.head 6 -  Width:  1.617"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 9
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbPending
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Pending
.head 5 -  Window Location and Size
.head 6 -  Left: 4.267"
.head 6 -  Top: 0.354"
.head 6 -  Width:  1.117"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 9
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbMine
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: My Certs.
.head 5 -  Window Location and Size
.head 6 -  Left: 5.4"
.head 6 -  Top: 0.354"
.head 6 -  Width:  1.15"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 9
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbAll
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: All
.head 5 -  Window Location and Size
.head 6 -  Left: 6.567"
.head 6 -  Top: 0.354"
.head 6 -  Width:  0.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Arial
.head 5 -  Font Size: 9
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Pushbutton: pbUpdateTable
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Update Confirmations
.head 5 -  Window Location and Size
.head 6 -  Left: 7.35"
.head 6 -  Top: 0.167"
.head 6 -  Width:  2.167"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Call SqlPrepare( hSql, "Update crim.cr_certified set addressline1=:colCrimAddress1,
		addressline2=:colCrimAddress2, addressline3=:colCrimAddress3, addressline4=:colCrimAddress4,
		confirmed=:colCrimConfirmed, print=:colCrimPrint, batch=:colCrimBatch
  	where articleno = :tblCertCRProcess.colCrimArticle" )
.head 7 -  Call SalTblDoUpdates( tblCertCRProcess, hSql, TRUE )
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Call Retrieve(cmbBatch)
.head 4 +  ! Pushbutton: pbPrintMail
.winattr
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Mail Report
.head 5 -  Window Location and Size
.head 6 -  Left: 10.163"
.head 6 -  Top: 0.167"
.head 6 -  Width:  1.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.end
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Set nPrintErr = -1
.head 7 -  Set sReport = 'CertMailMissing.qrp'
.head 7 -  Set sReportInputs = 'aDEF1, aDEF1NAM2, aDEF1ADR, aDEF1CITY, aCASENUM, aBARNUMBER, aBARCODE, aUsername,
	CourtClerk, Court'
.head 7 -  Set sReportBinds = 'colCrimAddress1, colCrimAddress2, colCrimAddress3, colCrimAddress4, colCrimCase, colCrimArticle, scolCrimBarcodeEncrypted[1], colCrimClerk,
	sCourtClerk, sCourt'
.head 7 -  Call SalTrackPopupMenu(hWndForm, 'VIEW_REPORT', TPM_CursorX | TPM_CursorY, 0, 0 )
.head 4 +  Pushbutton: pbPrint
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Print Certified Mail
.head 5 -  Window Location and Size
.head 6 -  Left: 9.6"
.head 6 -  Top: 0.167"
.head 6 -  Width:  1.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Set nPrintErr = -1
.head 7 -  ! If nDefaultPrinter1 = 1
.head 7 -  Set sReport = 'CertMail4Up.qrp'
.head 7 +  ! Else
.head 8 -  Set sReport = 'CertMail4UpBack.qrp'
.head 7 -  Set sReportInputs = 'aDEF1, aDEF1NAM2, aDEF1ADR, aDEF1CITY, aCASENUM, aBARNUMBER, aBARCODE, aUsername,
	bDEF1, bDEF1NAM2, bDEF1ADR, bDEF1CITY, bCASENUM, bBARNUMBER, bBARCODE, bUsername,
	cDEF1, cDEF1NAM2, cDEF1ADR, cDEF1CITY, cCASENUM, cBARNUMBER, cBARCODE, cUsername,
	dDEF1, dDEF1NAM2, dDEF1ADR, dDEF1CITY, dCASENUM, dBARNUMBER, dBARCODE, dUsername, CourtClerk'
.head 7 -  Set sReportBinds = 'scolCrimAddress1[0], scolCrimAddress2[0], scolCrimAddress3[0], scolCrimAddress4[0], scolCrimCase[0], scolCrimCertNo[0], scolCrimBarcodeEncrypted[0], scolCrimClerk[0],
	scolCrimAddress1[1], scolCrimAddress2[1], scolCrimAddress3[1], scolCrimAddress4[1], scolCrimCase[1], scolCrimCertNo[1], scolCrimBarcodeEncrypted[1], scolCrimClerk[1],
	scolCrimAddress1[2], scolCrimAddress2[2], scolCrimAddress3[2], scolCrimAddress4[2], scolCrimCase[2], scolCrimCertNo[2], scolCrimBarcodeEncrypted[2], scolCrimClerk[2],
	scolCrimAddress1[3], scolCrimAddress2[3], scolCrimAddress3[3], scolCrimAddress4[3], scolCrimCase[3], scolCrimCertNo[3], scolCrimBarcodeEncrypted[3], scolCrimClerk[3], sCourtClerk'
.head 7 -  Call SalReportPrint ( hWndForm, sReport, sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 7 +  If nPrintErr > 0
.head 8 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 7 +  Else
.head 8 -  Set strUpdate = "Update crim.cr_certified set print=:colCrimPrint
  	where articleno = :colCrimArticle"
.head 8 -  Call SqlPrepare( hSql, strUpdate )
.head 8 -  Call SalTblDoUpdates( tblCertCRProcess, hSql, TRUE )
.head 8 -  Call SqlCommit( hSql )
.head 7 +  ! If bDefaultPrinter1
.head 8 -  ! Call Reset_Printer(  )
.head 4 -  Background Text: bkgd7
.head 5 -  Resource Id: 18033
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Window Location and Size
.head 6 -  Left: 12.267"
.head 6 -  Top: 0.094"
.head 6 -  Width:  0.514"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.167"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Justify: Left
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Black
.head 5 -  Background Color: Default
.head 5 -  Title: Batch
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 4 +  Combo Box: cmbBatch
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Window Location and Size
.head 6 -  Left: 11.667"
.head 6 -  Top: 0.323"
.head 6 -  Width:  1.729"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.885"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Editable? No
.head 5 -  String Type: String
.head 5 -  Maximum Data Length: Default
.head 5 -  Sorted? Yes
.head 5 -  Always Show List? No
.head 5 -  Vertical Scroll? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 5 -  ToolTip:
.head 5 -  AutoFill? Yes
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  List Initialization
.head 5 +  Message Actions
.head 6 +  On SAM_Create
.head 7 +  If dfBegDate < SalDateCurrent(  ) and SalIsValidDateTime( dfBegDate )
.head 8 -  ! Call SalListPopulate( MyValue, hSql, 'Select distinct m.batch
	from cr_certified m
	where m.ddate >= :dfBegDate
	order by m.batch' )
.head 8 -  Call SalListPopulate( MyValue, hSql, 'Select distinct m.batch
	from crim.cr_certified m
	where m.ddate > sysdate - 40
	order by m.batch' )
.head 7 +  Else
.head 8 -  Call SalListPopulate( MyValue, hSql, 'Select distinct m.batch
	from crim.cr_certified m
	where m.ddate > sysdate - 24
	order by m.batch' )
.head 7 -  ! Call SalListAdd( MyValue, STRING_Null )
.head 6 +  On SAM_Click
.head 7 -  Call Retrieve(cmbBatch)
.head 4 -  Background Text: bkgd8
.head 5 -  Resource Id: 18034
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Window Location and Size
.head 6 -  Left: 14.033"
.head 6 -  Top: 0.094"
.head 6 -  Width:  2.183"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.167"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Justify: Left
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Black
.head 5 -  Background Color: Default
.head 5 -  Title: USPS Process Dates
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 4 +  Data Field: dfBegDate
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class: cDfAutoTab
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Data
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Editable? Yes
.head 5 -  Display Settings
.head 6 -  Window Location and Size
.head 7 -  Left: 13.55"
.head 7 -  Top: 0.313"
.head 7 -  Width:  1.257"
.head 7 -  Width Editable? Class Default
.head 7 -  Height: 0.25"
.head 7 -  Height Editable? Class Default
.head 6 -  Visible? Yes
.head 6 -  Border? Yes
.head 6 -  Justify: Left
.head 6 -  Format: MM-dd-yyyy
.head 6 -  Country: USA
.head 6 -  Font Name: Class Default
.head 6 -  Font Size: Class Default
.head 6 -  Font Enhancement: Class Default
.head 6 -  Text Color: Class Default
.head 6 -  Background Color: Class Default
.head 6 -  Input Mask: Unformatted
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Spell Check? No
.head 5 +  Message Actions
.head 6 +  On SAM_Create
.head 7 -  Call SqlPrepareAndExecute( hSql, "Select min(ddate)
	from crim.cr_certified into :dfBegDate
	where (print is null or batch is null) and code5 not in 'DEL'
	order by ddate" )
.head 7 -  Call SqlFetchNext( hSql, nFetchResult )
.head 7 +  If SalIsNull( dfBegDate )
.head 8 -  Set dfBegDate = SalDateCurrent(  )
.head 4 -  Background Text: bkgd9
.head 5 -  Resource Id: 18035
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Window Location and Size
.head 6 -  Left: 13.05"
.head 6 -  Top: 0.635"
.head 6 -  Width:  4.614"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.167"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Justify: Left
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Black
.head 5 -  Background Color: Default
.head 5 -  Title: Date and Time Mail is to be delivered to the Post Office
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 4 +  Data Field: dfDeliveryDate
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class: cDfAutoTab
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Data
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Editable? Yes
.head 5 -  Display Settings
.head 6 -  Window Location and Size
.head 7 -  Left: 14.85"
.head 7 -  Top: 0.313"
.head 7 -  Width:  2.029"
.head 7 -  Width Editable? Class Default
.head 7 -  Height: 0.25"
.head 7 -  Height Editable? Class Default
.head 6 -  Visible? Yes
.head 6 -  Border? Yes
.head 6 -  Justify: Left
.head 6 -  Format: MM-dd-yyyy hhhh:mm
.head 6 -  Country: USA
.head 6 -  Font Name: Class Default
.head 6 -  Font Size: Class Default
.head 6 -  Font Enhancement: Class Default
.head 6 -  Text Color: Class Default
.head 6 -  Background Color: Class Default
.head 6 -  Input Mask: Unformatted
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Spell Check? No
.head 5 +  Message Actions
.head 6 +  On SAM_Create
.head 7 -  Set MyValue = SalDateCurrent(  )
.head 7 -  Set MyValue = SalDateConstruct( SalDateYear(MyValue), SalDateMonth(MyValue), SalDateDay(MyValue), 17, 0, 0 )
.head 2 +  Contents
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 18031
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.5"
.head 5 -  Top: 0.344"
.head 5 -  Width:  1.057"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Title: Mail Pieces
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfCMCount
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cDfAutoTab
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.686"
.head 6 -  Top: 0.302"
.head 6 -  Width:  1.257"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: USA
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Data Field: dfRowid
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.0"
.head 6 -  Top: Default
.head 6 -  Width:  0.83"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Child Table: tblCertCRProcess
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.043"
.head 6 -  Top: 0.125"
.head 6 -  Width:  11.357"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 4.344"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 5000
.head 5 -  Discardable? No
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colCrimArticle
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Article Number
.head 6 -  Visible? Class Default
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 24
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.217"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Set dfArticleNo = MyValue
.head 8 -  Call SalSendMsg( dfArticleNo, SAM_AnyEdit, 0, 0 )
.head 5 +  Column: colCrimCase
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Case Number
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 14
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  1.533"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bSortTblCol = FALSE
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCrimCase2 ), TBL_SortIncreasing )
.head 9 -  Set bSortTblCol = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCrimCase2 ), TBL_SortDecreasing )
.head 9 -  Set bSortTblCol =FALSE
.head 7 +  ! On SAM_CaptionDoubleClick
.head 8 +  If bSortTblCol = FALSE
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCrimCase2 ), TBL_SortIncreasing )
.head 9 -  Set bSortTblCol = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCrimCase2 ), TBL_SortDecreasing )
.head 9 -  Set bSortTblCol =FALSE
.head 5 +  Column: colCrimCase2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Case Number
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 14
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  1.533"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimCaseYr
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: CaseYr
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 4
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #0
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimCaseTy
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: CaseTy
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 3
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimCaseNo
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: CaseNo
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimDate
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Date
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Center
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: MM-dd-yyyy
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimCode5
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Code
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimDNum
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Num
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 3
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.65"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimData
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Particulars
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 250
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.917"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimZip
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Zip Code
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.85"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimZip2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Zip+4
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.817"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimPrint
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Print
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 1
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.433"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 +  If MyValue = 'Y'
.head 9 +  If colCrimBatch = STRING_Null
.head 10 -  Set MyValue = 'D'
.head 10 -  Set colCrimBatch = 'DEL'
.head 9 -  ! Set colCrimPrintCnt = NUMBER_Null
.head 9 -  ! Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 9 -  Call SalTblSetRowFlags( tblCertCRProcess, SalTblQueryContext( tblCertCRProcess ), ROW_Edited, TRUE )
.head 8 +  Else If MyValue = 'D'
.head 9 -  Set MyValue = STRING_Null
.head 9 -  Set colCrimBatch = STRING_Null
.head 9 -  ! Set colCrimPrintCnt = NUMBER_Null
.head 9 -  ! Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 9 -  Call SalTblSetRowFlags( tblCertCRProcess, SalTblQueryContext( tblCertCRProcess ), ROW_Edited, TRUE )
.head 8 +  Else If MyValue = STRING_Null
.head 9 -  Set MyValue = 'Y'
.head 9 -  ! Set colCrimPrintCnt = 1
.head 9 -  ! Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 9 -  Call SalTblSetRowFlags( tblCertCRProcess, SalTblQueryContext( tblCertCRProcess ), ROW_Edited, TRUE )
.head 5 +  Column: colCrimConfirmed
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Confirmed
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Center
.head 6 -  Width:  0.983"
.head 6 -  Width Editable? Yes
.head 6 -  Format: MM-dd-yy
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 +  If SalIsNull( MyValue )
.head 9 -  Set colCrimPrintCnt = 1
.head 9 -  Set MyValue = SalDateCurrent(  )
.head 8 +  Else
.head 9 -  Set colCrimPrintCnt = NUMBER_Null
.head 9 -  Set MyValue = DATETIME_Null
.head 9 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_Edited, TRUE )
.head 8 -  Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 8 -  Call SalTblSetRowFlags( tblCertCRProcess, SalTblQueryContext( tblCertCRProcess ), ROW_Edited, TRUE )
.head 5 +  Column: colCrimSaveBatch
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Batch
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 8
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.983"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Set MyValue = STRING_Null
.head 8 -  Call SalTblSetRowFlags( tblCertCRProcess, SalTblQueryContext( tblCertCRProcess ), ROW_Edited, TRUE )
.head 5 +  Column: colCrimClerk
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Clerk
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 30
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.583"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimAddress1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 1
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: 60
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.917"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimAddress2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 2
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: 60
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimAddress3
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 2
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: 60
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimAddress4
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 4
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: 60
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCrimBatch
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Batch
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 8
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.917"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Set MyValue = STRING_Null
.head 8 -  Call SalTblSetRowFlags( tblCertCRProcess, SalTblQueryContext( tblCertCRProcess ), ROW_Edited, TRUE )
.head 5 +  Column: colCrimPrintCnt
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Print Flag
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 1
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.533"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 +  Window Variables
.head 5 -  Number: nLength
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SAM_CreateTBL(  )
.head 6 +  If nULevel > 9
.head 7 -  Call SalEnableWindow( colCrimBatch )
.head 5 +  On SAM_FetchRowDone
.head 6 +  If Not SalIsNull (colCrimConfirmed) and SalIsNull (colCrimBatch) or Not SalIsNull (cmbBatch)  ! colCrimPrint = 'Y'
.head 7 -  Set colCrimPrintCnt = 1
.head 7 -  Set nRows = nRows + 1
.head 6 -  Set colCrimCase = SalFmtFormatNumber( colCrimCaseYr, '0000' ) || '-' ||
	colCrimCaseTy || '-' || SalFmtFormatNumber( colCrimCaseNo, '#0000' )
.head 6 -  Set colCrimCase2 = SalFmtFormatNumber( colCrimCaseYr, '0000' ) || SalFmtFormatNumber( colCrimCaseNo, '0000' )
.head 6 -  Call SalStrRight( colCrimAddress4, 5, colCrimZip)
.head 6 +  If SalStrLength( colCrimAddress4 ) > 5 and (SalIsValidInteger( colCrimZip ) or SalStrLeftX( colCrimZip, 1 ) = '-')
.head 7 +  If SalStrLeftX( colCrimZip, 1 ) = '-'
.head 8 -  Set colCrimZip2 = colCrimZip
.head 8 -  Set nLength = SalStrLength( colCrimAddress4 )
.head 8 -  Set colCrimZip = SalStrMidX( colCrimAddress4, nLength-10, 5)
.head 6 +  Else
.head 7 -  Set colCrimZip = SalStrRightX( colCrimAddress3, 5)
.head 7 +  If SalStrLeftX( colCrimZip, 1 ) = '-'
.head 8 -  Set colCrimZip2 = colCrimZip
.head 8 -  Set nLength = SalStrLength( colCrimAddress3 )
.head 8 -  Set colCrimZip = SalStrMidX( colCrimAddress3, nLength-10, 5)
.head 6 +  If colCrimBatch != STRING_Null
.head 7 -  Call SalDisableWindow( colCrimAddress1 )
.head 7 -  Call SalDisableWindow( colCrimAddress2 )
.head 7 -  Call SalDisableWindow( colCrimAddress3)
.head 7 -  Call SalDisableWindow( colCrimAddress4 )
.head 5 +  ! On SAM_RowHeaderDoubleClick
.head 6 +  If bCertifiedForm
.head 7 -  Set frmCertified.sArticleNo = colCrimArticle
.head 7 -  Call SalBringWindowToTop( frmCertified )
.head 7 -  Call SalSendMsg( frmCertified, SAM_CreateComplete, 0, 0 )
.head 6 +  Else
.head 7 -  Call SalCreateWindow( frmCertified, hWndForm, colCrimArticle )
.head 6 -  ! Call DisableButtons ( )
.head 2 +  Functions
.head 3 +  Function: RecordNumber
.head 4 -  Description: Displays current record number of result set in status bar
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sRowNumber = SalNumberToStrX (nRowNumber + 1 ,0)
.head 5 -  Set sRows = SalNumberToStrX (nRows,0)
.head 5 -  Call SalStatusSetText( hWndForm, 'Record ' || sRowNumber || ' of '  || sRows )
.head 5 -  Set bRetrieve = FALSE
.head 3 +  Function: Retrieve
.head 4 -  Description: Retrieves requested result set
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: sPassBatch
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set nRows = 0
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 -  Set dfCMCount = NUMBER_Null
.head 5 -  Set sSelectString = "Select m.articleno, m.caseyr, m.casety, m.caseno, m.ddate, m.code5, m.dnum,
		m.confirmed, m.addressline1, m.addressline2, m.addressline3, m.addressline4, m.print, m.batch, s.username, nvl(d.data, m.addressline1)
	from crim.cr_certified m, crim.cr_certifiedstatus s, crim.cr_docket d into :colCrimArticle, :colCrimCaseYr, :colCrimCaseTy, :colCrimCaseNo, :colCrimDate, :colCrimCode5, :colCrimDNum,
		:colCrimConfirmed, :colCrimAddress1, :colCrimAddress2, :colCrimAddress3, :colCrimAddress4, :colCrimPrint, :colCrimBatch, :colCrimClerk, :colCrimData
	where m.articleno=s.articleno and s.status in 'Generated' and
		m.caseyr=d.caseyr(+) and m.casety=d.casety(+) and m.caseno=d.caseno(+) and m.ddate=d.dock_date(+) and m.dnum=d.seq(+)  "
.head 5 +  If sAutoPrint = 'Y' or sAutoPrint = 'F'
.head 6 +  If bPrintbyUser
.head 7 -  Set sSelectString = sSelectString || 'and s.username = user '
.head 6 +  Else
.head 7 -  Set sSelectString = sSelectString || 'and s.username not in (Select username from crim.users where certprintbyuser in 1) '
.head 5 +  If sPassBatch = STRING_Null
.head 6 -  Set sSelectString = sSelectString || 'and m.ddate between :dfBegDate and :dfDeliveryDate '
.head 6 +  If rbUnPrinted
.head 7 -  Set sSelectString = sSelectString || "and batch is null and m.print is null "
.head 6 +  Else If rbConfirmed
.head 7 -  Set sSelectString = sSelectString || "and (m.batch is null and m.print in 'Y' and m.confirmed is not null) "
.head 6 +  Else If rbPending
.head 7 -  Set sSelectString = sSelectString || "and (m.batch is null and m.print in 'Y' and m.confirmed is null) "
.head 6 +  Else If rbMine
.head 7 -  Set sSelectString = sSelectString || "and (m.batch is null and s.username in user) "
.head 6 +  Else
.head 7 -  Set sSelectString = sSelectString || "and (m.batch is null and (m.print in 'Y' or m.print is null) )  "
.head 5 +  Else
.head 6 -  Set sSelectString = sSelectString || 'and m.batch = :sPassBatch '
.head 5 -  Set sSelectString = sSelectString || "
	order by m.caseyr, m.caseno, m.articleno"
.head 5 -  Call SalTblPopulate( tblCertCRProcess, hSql, sSelectString, TBL_FillAll )
.head 5 +  ! If nRows = 0
.head 6 -  Call SalStatusSetText(hWndForm, 'No records retrieved' )
.head 5 -  Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 5 -  Call SalWaitCursor( FALSE )
.head 5 -  Return nRows
.head 3 +  Function: fWriteHeader
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHdrHour
.head 5 -  String: sUSPSHeader
.head 5 -  Number: nHdrSequence
.head 5 -  Number: nDigit
.head 5 -  Number: nLoop
.head 5 -  Number: nDigit1
.head 5 -  Number: nDigit2
.head 5 -  Number: nDigitOdd
.head 5 -  Number: nDigitEven
.head 5 -  Number: nMod10CheckDigit
.head 4 +  Actions
.head 5 -  Call SalFileOpen( hUSPSCrimUpload, 'I:\\USPSManifest\\mmcp7a.manifest', OF_Create | OF_Write )
.head 5 -  !
.head 5 -  Set sUSPSHeader = 'H1'
.head 5 -  Set sUSPSHeader = sUSPSHeader || '7'
.head 5 -  !
.head 5 -  Set sHdrSequence = '91'
.head 5 -  Set sHdrSequence = sHdrSequence || '50'
.head 5 -  Set sHdrSequence = sHdrSequence || '176849750'
.head 5 +  If SalIsNull (cmbBatch)
.head 6 -  ! PassUpdate
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Update crim.control
	set usps_seq = nvl(usps_seq, 0) + 1' )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select max(usps_seq)
	from crim.control into :nHdrSequence' )
.head 6 -  Call SqlFetchNext( hSql, nResult )
.head 6 -  Call SqlCommit( hSql )
.head 5 +  Else
.head 6 -  Set nHdrSequence = SalStrToNumber( cmbBatch )
.head 5 -  Set sHdrSequence = sHdrSequence || SalFmtFormatNumber( nHdrSequence, '00000000' )
.head 5 -  Set sUSPSHeader = sUSPSHeader || sHdrSequence || Mod10_Length22( sHdrSequence ) ! || '  '
.head 5 -  Set sHdrSequence = SalFmtFormatNumber( nHdrSequence, '00000000' )
.head 5 -  ! Set nLoop = 20
.head 5 -  ! Set nDigitOdd = 0
.head 5 -  ! Set nDigitEven = 0
.head 5 +  ! While nLoop >= 0
.head 6 -  Set nDigit = SalStrToNumber( SalStrMidX( sHdrSequence, nLoop, 1 ) )
.head 6 +  If SalNumberMod( nLoop, 2 ) = 1
.head 7 -  Set nDigitOdd = nDigitOdd + nDigit
.head 6 +  Else
.head 7 -  Set nDigitEven = nDigitEven + nDigit
.head 6 -  Set nLoop = nLoop - 1
.head 5 -  ! Set nDigit = (nDigitEven * 3) + nDigitOdd
.head 5 -  ! Set nMod10CheckDigit = SalNumberMod( nDigit, 10 )
.head 5 -  ! Set sHdrSequence = sHdrSequence || SalFmtFormatNumber( nMod10CheckDigit, '0' )
.head 5 -  ! Set sUSPSHeader = sUSPSHeader || sHdrSequence
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalFmtFormatDateTime( dfDeliveryDate, 'yyyyMMdd' )
.head 5 -  Set sHdrHour = SalFmtFormatDateTime( dfDeliveryDate, 'hhhh' )
.head 5 +  If SalStrLength( sHdrHour ) = 1
.head 6 -  Set sHdrHour = '0' || sHdrHour
.head 5 +  Else If SalStrLength( sHdrHour ) = 0
.head 6 -  Set sHdrHour = '00' || sHdrHour
.head 5 -  Set sUSPSHeader = sUSPSHeader || sHdrHour || SalFmtFormatDateTime( dfDeliveryDate, 'mmss' )
.head 5 -  Set sUSPSHeader = sUSPSHeader || '44702'
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('0000000000', 10)	! USPS Account No
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('00', 2) 		! Method of Payment
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('00000', 5)		! Post Office of Account
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('           ' || '  ', 12)	! Confirmation Number
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX(' ', 1)		! USPS Will Pick up Mail
.head 5 -  ! Set sUSPSHeader = sUSPSHeader || SalStrLeftX('           ' || '           ' || '           ', 30)
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || '013'	! USPS File Version
.head 5 -  Set sUSPSHeader = sUSPSHeader || '850'	! Developer ID Code
.head 5 -  Set sUSPSHeader = sUSPSHeader || '1004    '	! Shippers Version Number (8)
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalFmtFormatNumber( dfCMCount+1, '000000000' )
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('           ' || '           ' || '           ', 33)
.head 5 -  Set nDigit = SalStrLength( sUSPSHeader )
.head 5 +  If nDigit != 130
.head 6 -  Call SalMessageBox( 'The length of the Header record is incorrect

The length is ' || SalNumberToStrX( nDigit, 0 ) || '; the lenght should be 130 characters', 'Invalid Header Length', MB_Ok | MB_IconStop )
.head 5 -  ! Set sUSPSHeader = sUSPSHeader || '
'
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalNumberToChar( 13 )
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalNumberToChar( 10 )
.head 5 -  Set nDigit = nDigit + 2
.head 5 -  Call SalFileWrite( hUSPSCrimUpload, sUSPSHeader, nDigit )
.head 3 +  Function: fWriteDetail
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHdrHour
.head 5 -  String: sUSPSDetail
.head 5 -  Number: nHdrSequence
.head 5 -  Number: nDigit
.head 5 -  Number: nLoop
.head 5 -  Number: nDigit1
.head 5 -  Number: nDigit2
.head 5 -  Number: nDigitOdd
.head 5 -  Number: nDigitEven
.head 5 -  Number: nWriteCount
.head 5 -  Number: nMod10CheckDigit
.head 4 +  Actions
.head 5 -  Set nWriteCount = 0
.head 5 -  Set nRows = SalTblSetRow( tblCertCRProcess, TBL_SetFirstRow )
.head 5 +  While nRows < TBL_MaxRow
.head 6 +  If Not SalIsNull( colCrimConfirmed )
.head 7 -  Set sUSPSDetail = 'D1'
.head 7 -  Set sUSPSDetail = sUSPSDetail || 'FC'
.head 7 -  !
.head 7 -  Set sUSPSDetail = sUSPSDetail || colCrimArticle || '  '
.head 7 -  Set sUSPSDetail = sUSPSDetail || colCrimZip
.head 7 +  If SalStrLength( colCrimZip2 ) = 5
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalStrRightX( colCrimZip2, 4)
.head 7 +  Else
.head 8 -  Set sUSPSDetail = sUSPSDetail || '    '
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '
.head 7 -  Set sUSPSDetail = sUSPSDetail || '0000434'
.head 7 -  !
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX('           ', 11)
.head 7 -  Set sUSPSDetail = sUSPSDetail || 'N'
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX('            ' || '            ', 23)
.head 7 -  !
.head 7 -  Set sUSPSDetail = sUSPSDetail || '06'     ! 1st Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00115'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '     ! 2nd Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 3rd Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 4th Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 5th Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 6th Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  !
.head 7 -  Set sUSPSDetail = sUSPSDetail || '176849750'
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX(SalFmtFormatNumber( colCrimCaseYr, '0000') || 'CV' ||
	colCrimCaseTy || SalFmtFormatNumber( colCrimCaseNo, '00000') || '           ' || '           ' || '           ', 30)
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX('           ' || '           ' || '           ' || '           ', 40)
.head 7 -  !
.head 7 -  Set nDigit = SalStrLength( sUSPSDetail )
.head 7 +  If nDigit != 200
.head 8 -  Call SalMessageBox( 'The length of the Detail record is incorrect

The length is ' || SalNumberToStrX( nDigit, 0 ) || '; the length should be 200 characters', 'Invalid Detail Length', MB_Ok | MB_IconStop )
.head 7 -  Set nWriteCount = nWriteCount+1
.head 7 +  If nWriteCount < dfCMCount
.head 8 -  ! Set sUSPSDetail = sUSPSDetail || '
'
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalNumberToChar( 13 )
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalNumberToChar( 10 )
.head 8 -  Set nDigit = nDigit + 2
.head 7 -  Call SalFileWrite( hUSPSCrimUpload, sUSPSDetail, nDigit )
.head 7 +  If SalIsNull( colCrimBatch )
.head 8 -  ! PassUpdate
.head 8 -  Call SqlPrepareAndExecute ( hSql, "Update crim.cr_certified
	set batch=:sHdrSequence, confirmed=:colCrimConfirmed
  	where articleno = :tblCertCRProcess.colCrimArticle" )
.head 6 -  Set nRows = SalTblSetRow( tblCertCRProcess, TBL_SetNextRow )
.head 5 -  Call SqlCommit( hSql )
.head 3 -  !
.head 2 +  Window Parameters
.head 3 -  String: sAutoPrint
.head 2 +  Window Variables
.head 3 -  String: strSelect
.head 3 -  String: sPaperTray
.head 3 -  String: sPrinter
.head 3 -  Number: nOrientation
.head 3 -  Number: nPaperType
.head 3 -  Number: nWidth
.head 3 -  Number: nHeight
.head 3 -  String: strUpdate
.head 3 -  String: strDelete
.head 3 -  String: strDeleteMove
.head 3 -  String: strInsert
.head 3 -  String: strMaster
.head 3 -  String: strDocket
.head 3 -  String: strSelectDate
.head 3 -  String: sRowNumber
.head 3 -  String: sRows
.head 3 -  String: sCertNo
.head 3 -  String: sHdrSequence
.head 3 -  String: sSelectString
.head 3 -  String: sReadHeader
.head 3 -  String: sSaveFileYear
.head 3 -  String: sSaveFileName
.head 3 -  String: scolCrimBatch
.head 3 -  Date/Time: dcolCrimConfirmed
.head 3 -  Number: nPrintCnt
.head 3 -  Number: nPrintLabel
.head 3 -  Number: nLabelType
.head 3 -  Number: nFetchResult
.head 3 -  Number: nRow
.head 3 -  Number: nRows
.head 3 -  Number: nRowNumber
.head 3 -  Boolean: bFormDirty
.head 3 -  Number: nCaseYr
.head 3 -  Number: nCaseNo
.head 3 -  Number: nYear
.head 3 -  Number: nFldLength
.head 3 -  Date/Time: dtDate
.head 3 -  Date/Time: dtNow
.head 3 -  Boolean: bRetrieve
.head 3 -  Window Handle: hWndDatafield
.head 3 -  Window Handle: hWndSave
.head 3 -  Boolean: bPrintFlag
.head 3 -  Boolean: bLoginMyValue
.head 3 -  Boolean: bFirstIsEnabled
.head 3 -  Boolean: bPrevIsEnabled
.head 3 -  Boolean: bNextIsEnabled
.head 3 -  Boolean: bLastIsEnabled
.head 3 -  Boolean: bFirstpbLastClick
.head 3 -  ! !
.head 3 -  File Handle: hUSPSCrimUpload
.head 3 -  String: scolCrimCase[*]
.head 3 -  String: scolCrimClerk[*]
.head 3 -  String: scolCrimCertNo[*]
.head 3 -  String: scolCrimAddress1[*]
.head 3 -  String: scolCrimAddress2[*]
.head 3 -  String: scolCrimAddress3[*]
.head 3 -  String: scolCrimAddress4[*]
.head 3 -  String: scolCrimBarcodeEncrypted[*]
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 +  If Not bLogin
.head 5 -  Call SqlConnect(hSql)
.head 5 -  Set bLoginMyValue = TRUE
.head 5 -  Call SqlSetResultSet( hSql, TRUE )
.head 4 -  Set nLabelType = 4
.head 4 -  Set bCertifiedProcess = TRUE
.head 4 -  ! Set strSelect = 'SELECT caseyr, caseno, ddate, dnum, articleno, code5,
		addressline1, addressline2, addressline3, addressline4, print, batch, rowid
	FROM cr_certified INTO :dfCaseYr, :dfCaseNo, :dfDdate, :dfDnum, :dfArticleNo, :dfCode5,
		:mlEntry1, :mlEntry2, :mlEntry3, :mlEntry4, :dfRowid
	WHERE '
.head 4 -  ! Set strSelectCaseNo = strSelect || 'caseyr = :nCaseYr and caseno = :nCaseNo
	ORDER BY caseyr, caseno, ddate, dnum'
.head 4 -  ! Set strInsert = 'INSERT INTO cr_certified
  	(caseyr, caseno, ddate, dnum, code5, entry1, entry2, entry3, entry4, ArticleNo) VALUES
 	 (:dfCaseYr, :dfCaseNo, :dfDdate, :dfDnum, :dfCode5, :mlEntry1, :mlEntry2, :mlEntry3, :mlEntry4, :dfArticleNo)'
.head 4 -  Set nRows = 0
.head 4 -  Set bFormDirty = FALSE
.head 4 -  Set bFirstpbLastClick = TRUE
.head 4 -  Set dtNow = SalDateCurrent(  )
.head 4 -  Set nYear =  SalDateYear( dtNow )
.head 4 -  Set bRetrieve = TRUE
.head 4 -  Call Retrieve(STRING_Null)
.head 4 -  Call SalStatusSetText( hWndForm, 'Enter criteria and execute a query to build a specific result set' )
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 +  If sAutoPrint = 'Y' or sAutoPrint = 'F'
.head 5 +  If bPrintbyUser
.head 6 -  Call SqlPrepareAndExecute( hSql, "Select count(*)
	from crim.cr_certified m, crim.cr_certifiedstatus s into :nPrintCnt
	where m.articleno=s.articleno and s.status in 'Generated' and
		s.username = :SqlUser and m.ddate >= :dfBegDate and m.print is null" )
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select count(*)
	from crim.cr_certified m into :nPrintCnt
	where m.ddate >= :dfBegDate and m.print is null' )
.head 5 -  Call SqlFetchNext( hSql, nFetchResult )
.head 5 -  ! Call SalDisableWindow( pbPrintMail )
.head 5 +  If nPrintCnt >= nLabelType or ( nPrintCnt > 0 and sAutoPrint = 'F')
.head 6 -  Call Retrieve(STRING_Null)
.head 6 -  Call SalSendMsg( pbPrint, SAM_Click, 0, 0 )
.head 5 -  Call SalDestroyWindow( hWndForm )
.head 5 +  If nULevel < 9
.head 6 -  Call SalHideWindow( cmbBatch )
.head 6 -  Call SalHideWindowAndLabel( dfBegDate )
.head 6 -  Call SalHideWindowAndLabel( dfDeliveryDate )
.head 3 +  On SAM_Close
.head 4 +  If bFormDirty or SalTblAnyRows( tblCertCRProcess, ROW_Edited, 0 )
.head 5 +  If SalMessageBox( 'Discard Changes?', 'Confirmation',
MB_YesNo | MB_IconQuestion | MB_DefButton2) = IDNO
.head 6 -  Return FALSE
.head 4 +  If bLogin and bLoginMyValue
.head 5 -  Call SqlDisconnect( hSql )
.head 3 +  On SAM_Destroy
.head 4 -  Set bCertifiedProcess = FALSE
.head 3 -  ! ! A nPrintCnt of 1 is the Missing Case Report
.head 3 -  ! ! A nPrintCnt greater the 1 is the 4 Up Certified Labels
.head 3 +  On SAM_ReportStart
.head 4 -  Set nPrintLabel = 0
.head 4 -  Set bPrintFlag = TRUE
.head 4 -  Set nRow = SalTblSetRow( tblCertCRProcess, TBL_SetFirstRow )
.head 4 +  If nLabelType > 2
.head 5 +  While nRow < TBL_MaxRow and (colCrimPrint = 'Y' or colCrimPrint = '1')
.head 6 -  Set nRow = SalTblSetRow( tblCertCRProcess, TBL_SetNextRow )
.head 4 +  Else If nLabelType > 1
.head 5 +  While nRow < TBL_MaxRow and colCrimConfirmed != DATETIME_Null
.head 6 -  Set nRow = SalTblSetRow( tblCertCRProcess, TBL_SetNextRow )
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If nRow > TBL_MaxRow
.head 5 -  Return FALSE
.head 4 +  If nLabelType > 2
.head 5 -  Set nPrintCnt = 0
.head 5 -  Set nPrintLabel = 0
.head 5 -  Call SalSetArrayBounds( scolCrimCase, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimClerk, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimCertNo, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimAddress1, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimAddress2, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimAddress3, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimAddress4, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCrimBarcodeEncrypted, 1, -1 )
.head 5 +  While nPrintCnt < nLabelType
.head 6 +  If Not bPrintFlag
.head 7 +  While nRow < TBL_MaxRow and (colCrimPrint = 'Y' or colCrimPrint = '1')
.head 8 -  Set nRow = SalTblSetRow( tblCertCRProcess, TBL_SetNextRow )
.head 7 +  If nRow > TBL_MaxRow or nPrintLabel >= 4
.head 8 +  If nPrintCnt = 0
.head 9 -  Return FALSE
.head 8 +  Else
.head 9 -  Set nRow = TBL_MaxRow + 1
.head 9 -  Return TRUE
.head 6 -  Set nPrintLabel = nPrintLabel + 1
.head 6 -  Set scolCrimCase[nPrintCnt] = colCrimCase
.head 6 -  Set scolCrimClerk[nPrintCnt] = colCrimClerk
.head 6 -  Set scolCrimCertNo[nPrintCnt] = colCrimArticle
.head 6 -  Set scolCrimAddress1[nPrintCnt] = colCrimAddress1
.head 6 -  Set scolCrimAddress2[nPrintCnt] = colCrimAddress2
.head 6 -  Set scolCrimAddress3[nPrintCnt] = colCrimAddress3
.head 6 -  Set scolCrimAddress4[nPrintCnt] = colCrimAddress4
.head 6 -  Set scolCrimBarcodeEncrypted[nPrintCnt] = Code128c(colCrimArticle)
.head 6 -  Set colCrimPrint = 'Y'
.head 6 -  Set colCrimPrintCnt = 1
.head 6 -  Set nPrintCnt = nPrintCnt + 1
.head 6 -  Call SalTblSetRowFlags( tblCertCRProcess, nRow, ROW_Edited, TRUE )
.head 6 -  Set bPrintFlag = FALSE
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 +  If Not bPrintFlag
.head 6 -  Set nRow = SalTblSetRow( tblCertCRProcess, TBL_SetNextRow )
.head 6 +  If nLabelType > 1
.head 7 +  While nRow < TBL_MaxRow and colCrimConfirmed != DATETIME_Null
.head 8 -  Set nRow = SalTblSetRow( tblCertCRProcess, TBL_SetNextRow )
.head 6 +  If nRow > TBL_MaxRow
.head 7 -  Return FALSE
.head 5 -  Set scolCrimBarcodeEncrypted[1] = Code128c(colCrimArticle)
.head 5 -  Set bPrintFlag = FALSE
.head 5 -  Return TRUE
.head 3 +  On SAM_ReportFinish
.head 4 -  Set dfCMCount = SalTblColumnSum( tblCertCRProcess, SalTblQueryColumnPos(colCrimPrintCnt), 0, 0 )
.head 4 -  Return TRUE
.head 1 +  Form Window: frmCTable_Find
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Find Entry
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Normal
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 2.225"
.head 4 -  Top: 2.24"
.head 4 -  Width:  6.243"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.291"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description:
.head 2 -  Ribbon
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 52524
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.386"
.head 5 -  Top: 0.188"
.head 5 -  Width:  1.038"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Find:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfCTable_FindText
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.886"
.head 6 -  Top: 0.125"
.head 6 -  Width:  3.471"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 -  Set bCTableFound = FALSE
.head 3 +  Pushbutton: pbCTable_Find
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Find Next
.head 4 -  Window Location and Size
.head 5 -  Left: 4.371"
.head 5 -  Top: 0.135"
.head 5 -  Width:  0.957"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.24"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalTblSetRowFlags( hWndTbl, nLastFind - 1, ROW_Selected, FALSE )
.head 6 +  If rbCTableAll
.head 7 -  Set nRowFind =  VisTblFindString( hWndTbl,nLastFind, hWndNULL, dfCTable_FindText )
.head 7 -  Call SalTblSetRowFlags( hWndTbl, nRowFind, ROW_Selected, TRUE )
.head 6 +  Else
.head 7 -  Set nRowFind =  VisTblFindString( hWndTbl,nLastFind, CTable_GetSelected(  ), dfCTable_FindText )
.head 7 -  Call SalTblSetRowFlags( hWndTbl, nRowFind, ROW_Selected, TRUE )
.head 6 +  If nRowFind < 0
.head 7 +  If bCTableFound =  FALSE
.head 8 -  Call SalMessageBox('Finished searching Table, no matches found', 'Search Results', MB_Ok|MB_IconInformation)
.head 8 -  Set nRowFind = 0
.head 8 -  Set nLastFind = 0
.head 7 +  Else
.head 8 -  Call SalTblSetFocusRow( hWndTbl, nRowFind )
.head 8 -  Set nLastFind = nRowFind + 1
.head 8 -  Set bCTableFound = TRUE
.head 6 +  Else
.head 7 -  Call SalTblSetFocusRow( hWndTbl, nRowFind )
.head 7 -  Set nLastFind = nRowFind + 1
.head 7 -  Set bCTableFound = TRUE
.head 6 -  Call SalSetFocus( pbCTable_Find )
.head 3 +  Radio Button: rbCTableAll
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: All Columns
.head 4 -  Window Location and Size
.head 5 -  Left: 0.271"
.head 5 -  Top: 0.563"
.head 5 -  Width:  1.257"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDisableWindow( cmbCTable_Col)
.head 3 +  Radio Button: rbCTableSelect
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Select Column
.head 4 -  Window Location and Size
.head 5 -  Left: 1.543"
.head 5 -  Top: 0.563"
.head 5 -  Width:  1.386"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEnableWindow( cmbCTable_Col)
.head 6 -  Call SalSetFocus( cmbCTable_Col)
.head 3 +  Combo Box: cmbCTable_Col
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 2.957"
.head 5 -  Top: 0.573"
.head 5 -  Width:  2.729"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.875"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? Yes
.head 4 -  String Type: String
.head 4 -  Maximum Data Length: Default
.head 4 -  Sorted? No
.head 4 -  Always Show List? No
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  AutoFill? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  List Initialization
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call CTable_GetSelected( )
.head 6 -  Set bCTableFound = FALSE
.head 3 +  Pushbutton: pbExitHide
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Exit Hide
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDestroyWindow( frmCTable_Find)
.head 2 +  Functions
.head 3 +  Function: CTable_FillCmb
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nCol
.head 5 -  String: sText
.head 4 +  Actions
.head 5 -  Set nCol = 1
.head 5 -  Set sText = VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 5 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 6 +  If sText != ''
.head 7 -  Call SalListAdd( cmbCTable_Col, '('|| SalNumberToStrX(nCol, 0) ||') '||sText )
.head 5 +  While sText != ''
.head 6 -  Set nCol = nCol + 1
.head 6 -  Set sText=VisTblGetColumnTitle( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 6 +  If SalIsWindowVisible( SalTblGetColumnWindow ( hWndTbl, nCol, COL_GetPos ) )
.head 7 +  If sText != ''
.head 8 -  Call SalListAdd( cmbCTable_Col, '('|| SalNumberToStrX(nCol, 0) ||') '||sText )
.head 3 +  Function: CTable_GetSelected
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Window Handle: hWndCol
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nOffSet
.head 5 -  Number: nColSelected
.head 5 -  Window Handle: hWndCol
.head 4 +  Actions
.head 5 -  Set nOffSet = SalStrScan( cmbCTable_Col, ')')
.head 5 -  Set nOffSet = nOffSet -  1
.head 5 -  Set nColSelected = SalStrToNumber(SalStrMidX( cmbCTable_Col, 1, nOffSet))
.head 5 -  Set hWndCol = SalTblGetColumnWindow( hWndTbl, nColSelected, COL_GetPos )
.head 5 -  Return hWndCol
.head 2 +  Window Parameters
.head 3 -  Window Handle: hWndTbl
.head 2 +  Window Variables
.head 3 -  Number: nRowFind
.head 3 -  Number: nLastFind
.head 3 -  Boolean: bCTableFound
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call CTable_FillCmb(  )
.head 4 -  Call SalDisableWindow( cmbCTable_Col)
.head 4 -  Set rbCTableAll = TRUE
.head 4 -  Call SalSetDefButton( pbCTable_Find)
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 1 +  Form Window: frmCertifiedProcess
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Certified Mail Processing
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? Yes
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Maximized
.head 3 -  Maximizable? Yes
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? Yes
.head 3 -  Window Location and Size
.head 4 -  Left: 1.3"
.head 4 -  Top: 0.15"
.head 4 -  Width:  11.143"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 5.458"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Black
.head 3 -  Background Color: Gray
.head 2 -  XAML Style:
.head 2 -  Background Brush:
.head 2 -  Ribbon Bar Enabled? No
.head 2 -  Description: Process Certified Mail Form
.head 2 -  Ribbon
.head 2 +  Named Menus
.head 3 +  Menu: VIEW_REPORT
.head 4 -  Resource Id: 9071
.head 4 -  Picture File Name:
.head 4 -  Title:
.head 4 -  Description:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: Print All Certified Mailers
.head 5 -  Resource Id: 9072
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set nLabelType = 1
.head 6 -  Call SalReportPrint ( hWndForm, sReport, sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 6 +  If nPrintErr > 0
.head 7 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 6 +  Else
.head 7 -  Set strUpdate = "Update crim.certified 
	set print=:colCPPrint, confirmed = trunc(sysdate)
  	where articleno = :colCPArticle"
.head 7 -  Call SqlPrepare( hSql, strUpdate )
.head 7 -  Call SalTblDoUpdates( tblCertProcess, hSql, TRUE )
.head 7 -  Call SqlCommit( hSql )
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: Print Missing Certified Mailers
.head 5 -  Resource Id: 9073
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set nLabelType = 2
.head 6 -  Call SalReportPrint ( hWndForm, sReport, sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 6 +  If nPrintErr > 0
.head 7 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 6 +  Else
.head 7 -  Set strUpdate = "Update crim.certified set print=:colCPPrint
  	where articleno = :colCPArticle"
.head 7 -  Call SqlPrepare( hSql, strUpdate )
.head 7 -  Call SalTblDoUpdates( tblCertProcess, hSql, TRUE )
.head 7 -  Call SqlCommit( hSql )
.head 5 -  Menu Item Name:
.head 2 +  Menu
.head 3 +  Popup Menu: &File
.head 4 -  Resource Id: 9074
.head 4 -  Picture File Name:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: E&xit
.head 5 -  Resource Id: 9075
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text: Exit this form
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalPostMsg( hWndForm, SAM_Close, 0, 0 )
.head 5 -  Menu Item Name:
.head 3 +  Popup Menu: &Edit
.head 4 -  Resource Id: 9076
.head 4 -  Picture File Name:
.head 4 -  Enabled when:
.head 4 -  Status Text: Undo, Cut, Copy, Paste, Clear
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Undo
.head 5 -  Resource Id: 9077
.head 5 -  Picture File Name:
.head 5 -  Menu Item Name:
.head 5 -  Status Text: Reverses the last action
.head 5 -  Keyboard Accelerator: Alt+BkSp
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanUndo()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditUndo()
.head 4 -  Menu Separator
.head 4 +  Menu Item: Cu&t
.head 5 -  Resource Id: 9078
.head 5 -  Picture File Name:
.head 5 -  Menu Item Name:
.head 5 -  Status Text: Cuts the selection and puts it on the clipboard
.head 5 -  Keyboard Accelerator: Shift+Del
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanCut()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditCut()
.head 4 +  Menu Item: &Copy
.head 5 -  Resource Id: 9079
.head 5 -  Picture File Name:
.head 5 -  Menu Item Name:
.head 5 -  Status Text: Copies the selection and puts it on the clipboard
.head 5 -  Keyboard Accelerator: Ctrl+Ins
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanCut()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditCopy()
.head 4 +  Menu Item: &Paste
.head 5 -  Resource Id: 9080
.head 5 -  Picture File Name:
.head 5 -  Menu Item Name:
.head 5 -  Status Text: Inserts the Clipboard contents at the insertion point
.head 5 -  Keyboard Accelerator: Shift+Ins
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanPaste()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditPaste()
.head 4 +  Menu Item: C&lear
.head 5 -  Resource Id: 9081
.head 5 -  Picture File Name:
.head 5 -  Menu Item Name:
.head 5 -  Status Text: Clears the selection
.head 5 -  Keyboard Accelerator: Del
.head 5 +  Menu Settings
.head 6 -  Enabled when: SalEditCanCut()
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call SalEditClear()
.head 3 +  Popup Menu: &Retrieve
.head 4 -  Resource Id: 9082
.head 4 -  Picture File Name:
.head 4 -  Enabled when:
.head 4 -  Status Text: Enter criteria and execute a query to build a specific result set
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Enter
.head 5 -  Resource Id: 9083
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: F1
.head 5 -  Status Text: Enter criteria for result set by Case No, Case No/Date, or Date
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set bRetrieve = TRUE
.head 6 -  Set hWndDatafield = SalGetFirstChild ( hWndForm, TYPE_DataField  | TYPE_MultilineText )
.head 6 -  Set hWndSave = hWndDatafield
.head 6 +  While hWndDatafield != hWndNULL
.head 7 -  Call SalClearField( hWndDatafield )
.head 7 -  Set hWndDatafield = SalGetNextChild( hWndDatafield, TYPE_DataField | TYPE_MultilineText )
.head 6 -  Call SalSetFocus( hWndSave )
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: E&xecute
.head 5 -  Resource Id: 9084
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: F12
.head 5 -  Status Text: Retrieve requested record(s)
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Call Retrieve(STRING_Null)
.head 5 -  Menu Item Name:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: 0.604"
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Gray
.head 4 -  Resizable? No
.head 3 -  XAML Style:
.head 3 -  Background Brush:
.head 3 +  Contents
.head 4 -  Background Text: bkgd11
.head 5 -  Resource Id: 9090
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Window Location and Size
.head 6 -  Left: 0.125"
.head 6 -  Top: 0.083"
.head 6 -  Width:  0.878"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.167"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Justify: Left
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: None
.head 5 -  Text Color: Black
.head 5 -  Background Color: Gray
.head 5 -  Title: Article No:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 4 +  Data Field: dfArticleNo
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class: cDfAutoTab
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Data
.head 6 -  Maximum Data Length: 20
.head 6 -  Data Type: String
.head 6 -  Editable? Yes
.head 5 -  Display Settings
.head 6 -  Window Location and Size
.head 7 -  Left: 0.075"
.head 7 -  Top: 0.281"
.head 7 -  Width:  2.314"
.head 7 -  Width Editable? Class Default
.head 7 -  Height: 0.25"
.head 7 -  Height Editable? Class Default
.head 6 -  Visible? Class Default
.head 6 -  Border? Class Default
.head 6 -  Justify: Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Font Name: Class Default
.head 6 -  Font Size: Class Default
.head 6 -  Font Enhancement: Class Default
.head 6 -  Text Color: Class Default
.head 6 -  Background Color: Class Default
.head 6 -  Input Mask: Unformatted
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Spell Check? No
.head 5 +  Message Actions
.head 6 +  On SAM_AnyEdit
.head 7 +  If bRetrieve = FALSE
.head 8 +  If bFormDirty = FALSE
.head 9 -  Set bFormDirty = TRUE
.head 9 -  Call SalStatusSetText(hWndForm, 'This record has been modified' )
.head 8 -  ! Call SalSendClassMessage(SAM_AnyEdit, 0, 0 )
.head 7 +  If SalStrLength( MyValue ) = 20
.head 8 -  Call SalSetFieldEdit( MyValue, FALSE )
.head 8 -  Call SalSendMsg( MyValue, SAM_Validate, 0, 0 )
.head 6 +  On SAM_Validate
.head 7 +  If SalStrLength( MyValue ) = 20
.head 8 -  Set nRow = VisTblFindString( tblCertProcess, 0, colCPArticle, dfArticleNo )
.head 8 +  If nRow < 0
.head 9 -  !
.head 9 -  Set sSelectString = 'Select m.articleno, m.caseyr, c.casetype, m.caseno, m.ddate, m.code5, m.dnum,
		confirmed, addressline1, addressline2, addressline3, addressline4, print, batch, d.username,
		substr(nvl(c.code1 || d.entry1 || c.code2 || d.entry2 || c.code3 || d.entry3 || c.code4 || d.entry4, d.memo), 1, 200)
	from crim.certified m, crim.casemaster c, crim.docket d, crim.cvcodes c into :colCPArticle, :colCPCaseYr, :colCPCaseTy, :colCPCaseNo, :colCPDate, :colCPCode5, :colCPDNum,
		:colCPConfirmed, :colCPAddress1, :colCPAddress2, :colCPAddress3, :colCPAddress4, :colCPPrint, :colCPBatch, :colCPClerk, :colCPData
	where m.caseyr=c.caseyr and m.caseno=c.caseno and
		m.caseyr=d.caseyr and m.caseno=d.caseno and m.ddate=d.ddate and m.dnum=d.dnum and
		m.code5=d.code5(+) and m.code5=c.code(+) '
.head 9 -  Set sSelectString = sSelectString || 'and m.articleno = :dfArticleNo '
.head 9 -  Set nRow = SalTblInsertRow( tblCertProcess, TBL_MaxRow )
.head 9 -  Call SqlPrepareAndExecute( hSql, sSelectString )
.head 9 +  If SqlFetchNext( hSql, nResult )
.head 10 -  Call SalSendMsg( tblCertProcess, SAM_FetchRowDone, 0, 0 )
.head 10 +  If colCPBatch != cmbBatch and Not SalIsNull( colCPBatch )
.head 11 -  Set colCPPrintCnt = 0
.head 11 -  Call SalMessageBox( 'Certified Mail has already been uploaded to the Post Office 

on ' || SalFmtFormatDateTime( colCPConfirmed, 'ddd, MMM d yyyy' ) || '.  The mail piece should be mailed as soon as possible. 

The Mail piece can not be uploaded again.', 'Mail upload warning', MB_IconInformation | MB_Ok )
.head 11 -  ! Return TRUE
.head 10 +  Else If colCPBatch = cmbBatch or SalIsNull( colCPBatch )
.head 11 +  If colCPPrint = STRING_Null
.head 12 -  Set colCPPrint = 'Y'
.head 11 -  Set colCPPrintCnt = 1
.head 11 -  Set colCPConfirmed = SalDateCurrent(  )
.head 11 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_New, FALSE )
.head 11 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_Edited, TRUE )
.head 10 -  Call SalClearField( dfArticleNo )
.head 9 +  Else
.head 10 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_New, FALSE )
.head 10 -  Call SalMessageBox( 'Certified Number Not Found in Table', 'Not Found', MB_Ok )
.head 8 +  Else
.head 9 -  Call SalTblSetContext( tblCertProcess, nRow )
.head 9 -  Call SalClearField( dfArticleNo )
.head 9 +  If SalIsNull (colCPConfirmed)
.head 10 -  Set colCPPrint = 'Y'
.head 10 -  Set colCPPrintCnt = 1
.head 10 -  Set colCPConfirmed = SalDateCurrent(  )
.head 10 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_Edited, TRUE )
.head 9 +  Else If SalIsNull (colCPPrint)
.head 10 -  Set colCPPrint = 'Y'
.head 10 -  Set colCPPrintCnt = 1
.head 10 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_Edited, TRUE )
.head 9 +  Else If colCPBatch != cmbBatch and Not SalIsNull( colCPBatch )
.head 10 -  Set colCPPrintCnt = 0
.head 10 -  Call SalMessageBox( 'Certified Mail has already been uploaded to the Post Office 

on ' || SalFmtFormatDateTime( colCPConfirmed, 'ddd, MMM d yyyy' ) || '.  The mail piece should be mailed as soon as possible. 

The Mail piece can not be uploaded again.', 'Mail upload warning', MB_IconInformation | MB_Ok )
.head 9 +  Else
.head 10 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_New, TRUE )
.head 8 -  Set dfCMCount = SalTblColumnSum( tblCertProcess, SalTblQueryColumnPos(colCPPrintCnt), 0, 0 )
.head 4 +  Pushbutton: pbShowMail
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Display Mail
.head 5 -  Window Location and Size
.head 6 -  Left: 3.413"
.head 6 -  Top: 0.052"
.head 6 -  Width:  2.338"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Call Retrieve(STRING_Null)
.head 4 +  Radio Button: rbUnPrinted
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: UnPrinted
.head 5 -  Window Location and Size
.head 6 -  Left: 2.488"
.head 6 -  Top: 0.333"
.head 6 -  Width:  0.825"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbConfirmed
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Confirmations
.head 5 -  Window Location and Size
.head 6 -  Left: 3.388"
.head 6 -  Top: 0.333"
.head 6 -  Width:  1.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbPending
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Pending
.head 5 -  Window Location and Size
.head 6 -  Left: 4.488"
.head 6 -  Top: 0.333"
.head 6 -  Width:  0.775"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbMine
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: My Certs.
.head 5 -  Window Location and Size
.head 6 -  Left: 5.288"
.head 6 -  Top: 0.333"
.head 6 -  Width:  0.8"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Radio Button: rbAll
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: All
.head 5 -  Window Location and Size
.head 6 -  Left: 6.213"
.head 6 -  Top: 0.333"
.head 6 -  Width:  0.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.2"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  ToolTip:
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 -  Flow Direction: Default
.head 5 -  Message Actions
.head 4 +  Pushbutton: pbUpdateTable
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Update Confirmations
.head 5 -  Window Location and Size
.head 6 -  Left: 7.05"
.head 6 -  Top: 0.167"
.head 6 -  Width:  1.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Call SqlPrepare( hSql, "Update crim.certified set batch=:colCPBatch,
		confirmed=:colCPConfirmed, print=:colCPPrint
  	where articleno = :tblCertProcess.colCPArticle" )
.head 7 -  Call SalTblDoUpdates( tblCertProcess, hSql, TRUE )
.head 7 -  Call SqlPrepare( hSql, "Update crim.cr_certified set batch=:colCPBatch,
		confirmed=:colCPConfirmed, print=:colCPPrint
  	where articleno = :tblCertProcess.colCPArticle" )
.head 7 -  Call SalTblDoDeletes( tblCertProcess, hSql, ROW_MarkDeleted )
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Call Retrieve(cmbBatch)
.head 4 +  Pushbutton: pbPrintMail
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Mail Report
.head 5 -  Window Location and Size
.head 6 -  Left: 10.163"
.head 6 -  Top: 0.167"
.head 6 -  Width:  1.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Set nPrintErr = -1
.head 7 -  Set sReport = CV_REPORT_Path || 'CertMailMissing.qrp'
.head 7 -  Set sReportInputs = 'aDEF1, aDEF1NAM2, aDEF1ADR, aDEF1CITY, aCASENUM, aBARNUMBER, aBARCODE, aUsername,
	CourtClerk, Court'
.head 7 -  Set sReportBinds = 'colCPAddress1, colCPAddress2, colCPAddress3, colCPAddress4, colCPCase, colCPArticle, scolCPBarcodeEncrypted[1], colCPClerk,
	sCourtClerk, sCourt'
.head 7 -  Call SalTrackPopupMenu(hWndForm, 'VIEW_REPORT', TPM_CursorX | TPM_CursorY, 0, 0 )
.head 4 +  Pushbutton: pbPrint
.head 5 -  Class Child Ref Key: 0
.head 5 -  Class ChildKey: 0
.head 5 -  Class:
.head 5 -  Property Template:
.head 5 -  Class DLL Name:
.head 5 -  Title: Print Certified Mail
.head 5 -  Window Location and Size
.head 6 -  Left: 8.613"
.head 6 -  Top: 0.167"
.head 6 -  Width:  1.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.292"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Picture File Name:
.head 5 -  Picture Transparent Color: None
.head 5 -  Image Style: Single
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Button Appearance: Standard
.head 5 -  ToolTip:
.head 5 -  Image Alignment: Default
.head 5 -  Text Alignment: Default
.head 5 -  Text Image Relation: Default
.head 5 -  XAML Style:
.head 5 -  Background Brush:
.head 5 -  Text Brush:
.head 5 +  Message Actions
.head 6 +  On SAM_Click
.head 7 -  Set nPrintErr = -1
.head 7 +  If nLabelType = 3
.head 8 -  Set sReport = CV_REPORT_Path || 'CertMail3Up.qrp'
.head 7 +  Else If nLabelType = 4
.head 8 +  If nDefaultPrinter1 = 1 or nDefaultPrinter1 = 11 or nDefaultPrinter1 = 21
.head 9 -  Set sReport = CV_REPORT_Path || 'CertMail4UpBack.qrp'
.head 8 +  Else If nDefaultPrinter1 = 2 or nDefaultPrinter1 = 12 or nDefaultPrinter1 = 22
.head 9 -  Set sReport = CV_REPORT_Path || 'CertMail4UpSupervisorOffice.qrp'
.head 8 +  Else If nDefaultPrinter1 = 3 or nDefaultPrinter1 = 13 or nDefaultPrinter1 = 23
.head 9 -  Set sReport = CV_REPORT_Path || 'CertMail4UpCashier1.qrp'
.head 8 +  Else If nDefaultPrinter1 = 4 or nDefaultPrinter1 = 14 or nDefaultPrinter1 = 24
.head 9 -  Set sReport = CV_REPORT_Path || 'CertMail4UpCashier2.qrp'
.head 8 +  Else
.head 9 -  Set sReport = CV_REPORT_Path || 'CertMail4Up.qrp'
.head 7 -  Set sReportInputs = 'aDEF1, aDEF1NAM2, aDEF1ADR, aDEF1CITY, aCASENUM, aBARNUMBER, aBARCODE, aUsername,
	bDEF1, bDEF1NAM2, bDEF1ADR, bDEF1CITY, bCASENUM, bBARNUMBER, bBARCODE, bUsername,
	cDEF1, cDEF1NAM2, cDEF1ADR, cDEF1CITY, cCASENUM, cBARNUMBER, cBARCODE, cUsername,
	dDEF1, dDEF1NAM2, dDEF1ADR, dDEF1CITY, dCASENUM, dBARNUMBER, dBARCODE, dUsername, CourtClerk'
.head 7 -  Set sReportBinds = 'scolCPAddress1[0], scolCPAddress2[0], scolCPAddress3[0], scolCPAddress4[0], scolCPCase[0], scolCPCertNo[0], scolCPBarcodeEncrypted[0], scolCPClerk[0],
	scolCPAddress1[1], scolCPAddress2[1], scolCPAddress3[1], scolCPAddress4[1], scolCPCase[1], scolCPCertNo[1], scolCPBarcodeEncrypted[1], scolCPClerk[1],
	scolCPAddress1[2], scolCPAddress2[2], scolCPAddress3[2], scolCPAddress4[2], scolCPCase[2], scolCPCertNo[2], scolCPBarcodeEncrypted[2], scolCPClerk[2],
	scolCPAddress1[3], scolCPAddress2[3], scolCPAddress3[3], scolCPAddress4[3], scolCPCase[3], scolCPCertNo[3], scolCPBarcodeEncrypted[3], scolCPClerk[3], sCourtClerk'
.head 7 -  Call SalReportPrint ( hWndForm, sReport, sReportBinds, sReportInputs, 1, RPT_PrintAll | RPT_PrintNoWarn, 0, 0, nPrintErr )
.head 7 +  If nPrintErr > 0
.head 8 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),MB_Ok )
.head 7 +  Else
.head 8 -  Set strUpdate = "Update crim.certified set print=:colCPPrint
  	where articleno = :colCPArticle"
.head 8 -  Call SqlPrepare( hSql, strUpdate )
.head 8 -  Call SalTblDoUpdates( tblCertProcess, hSql, TRUE )
.head 8 -  Call SqlCommit( hSql )
.head 7 +  ! If bDefaultPrinter1
.head 8 -  ! Call Reset_Printer(  )
.head 2 +  Contents
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 9085
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.2"
.head 5 -  Top: 0.365"
.head 5 -  Width:  1.186"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Title: Date Entered:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 9086
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.2"
.head 5 -  Top: 0.083"
.head 5 -  Width:  4.471"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Italic
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Title: Process Certified Mail to be sent to the Post Office
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfBegDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cDfAutoTab
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.457"
.head 6 -  Top: 0.323"
.head 6 -  Width:  1.257"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: USA
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  ! Set MyValue = SalDateCurrent(  ) - 1
.head 6 +  ! While HolidayCheck( MyValue, FALSE, TRUE )
.head 7 -  Set MyValue = MyValue - 1
.head 6 -  Call SqlPrepareAndExecute( hSql, "Select min(ddate) 
		from crim.certified 
		where (print is null or batch is null) and 
			code5 not in 'DEL' and ddate > '1-Dec-2012' 
          Union
	Select min(ddate) 
		from crim.cr_certified 
		where (print is null or batch is null) and code5 not in 'DEL' 
	into :dfBegDate 
	order by 1 asc" )
.head 6 -  Call SqlFetchNext( hSql, nFetchResult )
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 9087
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.229"
.head 5 -  Top: 0.698"
.head 5 -  Width:  4.614"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Title: Date and Time Mail is to be delivered to the Post Office
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfDeliveryDate
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cDfAutoTab
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.914"
.head 6 -  Top: 0.656"
.head 6 -  Width:  2.029"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: MM-dd-yyyy hhhh:mm
.head 5 -  Country: USA
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = SalDateCurrent(  )
.head 6 -  Set MyValue = SalDateConstruct( SalDateYear(MyValue), SalDateMonth(MyValue), SalDateDay(MyValue), 17, 0, 0 )
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 9088
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 4.5"
.head 5 -  Top: 0.344"
.head 5 -  Width:  1.057"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Title: Mail Pieces
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Data Field: dfCMCount
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cDfAutoTab
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.686"
.head 6 -  Top: 0.302"
.head 6 -  Width:  1.257"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: #0
.head 5 -  Country: USA
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd10
.head 4 -  Resource Id: 9089
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 8.329"
.head 5 -  Top: 0.302"
.head 5 -  Width:  0.514"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Black
.head 4 -  Background Color: Default
.head 4 -  Title: Batch
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 3 +  Combo Box: cmbBatch
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 9.057"
.head 5 -  Top: 0.26"
.head 5 -  Width:  1.729"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.104"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  String Type: String
.head 4 -  Maximum Data Length: Default
.head 4 -  Sorted? Yes
.head 4 -  Always Show List? No
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  AutoFill? Yes
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  List Initialization
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 +  If dfBegDate < SalDateCurrent(  ) and SalIsValidDateTime( dfBegDate )
.head 7 -  Call SalListPopulate( MyValue, hSql, 'Select distinct m.batch
	from crim.certified m
	where m.ddate > sysdate - 40
	order by m.batch' )
.head 6 +  Else
.head 7 -  Call SalListPopulate( MyValue, hSql, 'Select distinct m.batch
	from crim.certified m
	where m.ddate > sysdate - 24
	order by m.batch' )
.head 6 -  ! Call SalListAdd( MyValue, STRING_Null )
.head 5 +  On SAM_Click
.head 6 -  Call Retrieve(cmbBatch)
.head 3 +  Data Field: dfRowid
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.0"
.head 6 -  Top: Default
.head 6 -  Width:  0.83"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 -  Flow Direction: Default
.head 4 -  Spell Check? No
.head 4 -  Message Actions
.head 3 +  Child Table: tblCertProcess
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: CTable
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.043"
.head 6 -  Top: 1.01"
.head 6 -  Width:  14.443"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 5.229"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 5 -  Hide Column Headers? No
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 5000
.head 5 -  Discardable? No
.head 4 -  XAML Style:
.head 4 -  Summary Bar Enabled? No
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Contents
.head 5 +  Column: colCPArticle
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Article Number
.head 6 -  Visible? Class Default
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 24
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.217"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Set dfArticleNo = MyValue
.head 8 -  Call SalSendMsg( dfArticleNo, SAM_AnyEdit, 0, 0 )
.head 5 +  Column: colCPCase
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Case Number
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 14
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  1.533"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bSortTblCol = FALSE
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCPCase2 ), TBL_SortIncreasing )
.head 9 -  Set bSortTblCol = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCPCase2 ), TBL_SortDecreasing )
.head 9 -  Set bSortTblCol =FALSE
.head 7 +  ! On SAM_CaptionDoubleClick
.head 8 +  If bSortTblCol = FALSE
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCPCase2 ), TBL_SortIncreasing )
.head 9 -  Set bSortTblCol = TRUE
.head 8 +  Else 
.head 9 -  Call SalTblSortRows( hWndTable, SalTblQueryColumnID( colCPCase2 ), TBL_SortDecreasing )
.head 9 -  Set bSortTblCol =FALSE
.head 5 +  Column: colCPCase2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Case Number
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 14
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  1.533"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Class Default
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPCaseYr
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: CaseYr
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 4
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #0
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPCaseTy
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: CaseTy
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 3
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPCaseNo
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: CaseNo
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #000
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPDate
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Date
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Center
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: MM-dd-yyyy
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPCode5
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Code
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.867"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPDNum
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Num
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 3
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.65"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPData
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Particulars
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 250
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.65"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPZip
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Zip Code
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.85"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPZip2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Zip+4
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 5
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  0.817"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPPrint
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Print
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 1
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.433"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 +  If MyValue = 'Y'
.head 9 -  Set MyValue = STRING_Null
.head 9 -  Set colCPConfirmed = DATETIME_Null
.head 9 -  Set colCPBatch = STRING_Null
.head 9 -  ! Set colCPPrintCnt = NUMBER_Null
.head 9 -  ! Set dfCMCount = SalTblColumnSum( tblCertProcess, SalTblQueryColumnPos(colCPPrintCnt), 0, 0 )
.head 9 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_Edited, TRUE )
.head 8 +  Else If MyValue = STRING_Null
.head 9 -  Set MyValue = 'Y'
.head 9 -  ! Set colCPPrintCnt = 1
.head 9 -  ! Set dfCMCount = SalTblColumnSum( tblCertProcess, SalTblQueryColumnPos(colCPPrintCnt), 0, 0 )
.head 9 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_Edited, TRUE )
.head 8 +  If SalTblQueryRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_New )
.head 9 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_New, FALSE )
.head 5 +  Column: colCPConfirmed
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Confirmed
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Center
.head 6 -  Width:  0.983"
.head 6 -  Width Editable? Yes
.head 6 -  Format: MM-dd-yy
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 +  If SalIsNull( MyValue )
.head 9 -  Set colCPPrintCnt = 1
.head 9 -  Set MyValue = SalDateCurrent(  )
.head 8 +  Else
.head 9 -  Set colCPPrintCnt = NUMBER_Null
.head 9 -  Set MyValue = DATETIME_Null
.head 9 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_Edited, TRUE )
.head 8 -  Set dfCMCount = SalTblColumnSum( tblCertProcess, SalTblQueryColumnPos(colCPPrintCnt), 0, 0 )
.head 8 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_Edited, TRUE )
.head 8 +  If SalTblQueryRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_New )
.head 9 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_New, FALSE )
.head 5 +  Column: colCPBatch
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Batch
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 8
.head 6 -  Data Type: String
.head 6 -  Justify: Center
.head 6 -  Width:  0.983"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Set MyValue = STRING_Null
.head 8 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_Edited, TRUE )
.head 8 +  If SalTblQueryRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_New )
.head 9 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_New, FALSE )
.head 5 +  Column: colCPClerk
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Clerk
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 30
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.583"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPAddress1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class: CColumn
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 1
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 80
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.917"
.head 6 -  Width Editable? Class Default
.head 6 -  Format: Unformatted
.head 6 -  Country: USA
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Class Default
.head 8 -  Vertical Scroll? Class Default
.head 8 -  Auto Drop Down? Class Default
.head 8 -  Allow Text Editing? Class Default
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Class Default
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPAddress2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 2
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 66
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPAddress3
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 2
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 60
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPAddress4
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Address 4
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 60
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  2.95"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_DoubleClick
.head 8 -  Call SalSendMsg( hWndForm, SAM_RowHeaderDoubleClick, 0, 0 )
.head 5 +  Column: colCPPrintCnt
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Print Flag
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 1
.head 6 -  Data Type: Number
.head 6 -  Justify: Center
.head 6 -  Width:  0.533"
.head 6 -  Width Editable? Yes
.head 6 -  Format: #
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  Column Aggregate Type: None
.head 6 -  Flow Direction: Default
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 +  Window Variables
.head 5 -  Number: nLength
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SAM_CreateTBL(  )
.head 6 +  If nULevel > 9
.head 7 -  Call SalEnableWindow( colCPBatch )
.head 5 +  On SAM_FetchRowDone
.head 6 +  If Not SalIsNull (colCPConfirmed) and SalIsNull (colCPBatch) or Not SalIsNull (cmbBatch)  ! colCPPrint = 'Y'
.head 7 -  Set colCPPrintCnt = 1
.head 7 -  Set nRows = nRows + 1
.head 6 +  If SalStrLength( colCPCaseTy ) = 1
.head 7 -  Set colCPCaseTy = 'CV' || colCPCaseTy
.head 6 +  If SalStrLeftX( colCPCaseTy, 2 ) = 'CR' or SalStrLeftX( colCPCaseTy, 2 ) = 'TR'  
.head 7 -  Call SalTblSetRowFlags( tblCertProcess, SalTblQueryContext( tblCertProcess ), ROW_MarkDeleted, TRUE )
.head 6 -  Set colCPCase = SalFmtFormatNumber( colCPCaseYr, '0000' ) || '-' ||
	colCPCaseTy || '-' || SalFmtFormatNumber( colCPCaseNo, '#0000' )
.head 6 -  Set colCPCase2 = SalFmtFormatNumber( colCPCaseYr, '0000' ) || SalFmtFormatNumber( colCPCaseNo, '0000' )
.head 6 -  Call SalStrRight( colCPAddress4, 5, colCPZip)
.head 6 +  If SalStrLength( colCPAddress4 ) > 5 and (SalIsValidInteger( colCPZip ) or SalStrLeftX( colCPZip, 1 ) = '-')
.head 7 +  If SalStrLeftX( colCPZip, 1 ) = '-'
.head 8 -  Set colCPZip2 = colCPZip
.head 8 -  Set nLength = SalStrLength( colCPAddress4 )
.head 8 -  Set colCPZip = SalStrMidX( colCPAddress4, nLength-10, 5)
.head 6 +  Else
.head 7 -  Set colCPZip = SalStrRightX( colCPAddress3, 5)
.head 7 +  If SalStrLeftX( colCPZip, 1 ) = '-'
.head 8 -  Set colCPZip2 = colCPZip
.head 8 -  Set nLength = SalStrLength( colCPAddress3 )
.head 8 -  Set colCPZip = SalStrMidX( colCPAddress3, nLength-10, 5)
.head 5 +  ! On SAM_RowHeaderDoubleClick
.head 6 +  If bCertifiedForm
.head 7 -  Set frmCertified.sArticleNo = colCPArticle
.head 7 -  Call SalBringWindowToTop( frmCertified )
.head 7 -  Call SalSendMsg( frmCertified, SAM_CreateComplete, 0, 0 )
.head 6 +  Else 
.head 7 -  Call SalCreateWindow( frmCertified, hWndForm, colCPArticle )
.head 6 -  ! Call DisableButtons ( )
.head 3 +  Pushbutton: pbProcessSend
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Process Mail
.head 4 -  Window Location and Size
.head 5 -  Left: 11.186"
.head 5 -  Top: 0.167"
.head 5 -  Width:  2.086"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalIsNull( dfDeliveryDate ) or dfDeliveryDate < SalDateCurrent(  ) or dfDeliveryDate > SalDateCurrent(  ) + 5
.head 7 -  Call SalMessageBox( 'Invalid Delivery Date; Please verify', 'Delivery Date Error', MB_Ok | MB_IconStop)
.head 7 -  Return TRUE
.head 6 +  If SalIsNull( dfCMCount ) or dfCMCount < 1
.head 7 -  Call SalMessageBox( 'Invalid Number of records to be processed', 'Mailer Count Error', MB_Ok | MB_IconStop)
.head 7 -  Return TRUE
.head 6 -  !
.head 6 -  Call fWriteHeader ()
.head 6 -  Call fWriteDetail ( )
.head 6 -  !
.head 6 -  Call SalFileClose( hUSPSUpload )
.head 6 -  Call Retrieve (sHdrSequence)
.head 6 -  Call SalEnableWindow( cmbBatch )
.head 6 -  Call SalEnableWindow( pbSendUSPS )
.head 6 -  Call SalDisableWindow( pbProcessSend )
.head 3 +  Pushbutton: pbSendUSPS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Send Manifest to USPS
.head 4 -  Window Location and Size
.head 5 -  Left: 11.186"
.head 5 -  Top: 0.583"
.head 5 -  Width:  2.086"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 -  XAML Style:
.head 4 -  Background Brush:
.head 4 -  Text Brush:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  ! PassUpdate
.head 6 -  Call SalLoadAppAndWait ('I:\\USPSManifest\\FTPUSPS.bat', Window_Normal, nResult)
.head 6 -  Call SalBringWindowToTop( frmCertifiedProcess )
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 +  If SalMessageBox( 'Did the file Transfer Correctly?

(Was the number of bytes transferred greater than zero?)', 'Successful Transfer', MB_YesNo | MB_IconQuestion ) = IDYES
.head 7 +  If SalIsNull (cmbBatch)
.head 8 -  Set sSaveFileName = sHdrSequence || '.manifest'
.head 7 +  Else
.head 8 -  Set sSaveFileName = cmbBatch || '.manifest.reprocess'
.head 7 -  Set sSaveFileYear = SalFmtFormatNumber( SalDateYear( SalDateCurrent(  ) ) , '0000' )
.head 7 -  ! If VisFileRename( 'I:\\USPSManifest\\mmcp7a.manifest', 'I:\\USPSManifest\\' || sSaveFileYear || '\\' || sSaveFileName) < 1
.head 7 +  If VisFileCopy( '\\\\192.168.38.5\\Imaging\\USPSManifest\\mmcp7a.manifest', '\\\\192.168.38.5\\Imaging\\USPSManifest\\' || sSaveFileYear || '\\' || sSaveFileName) > 0
.head 8 -  Call VisFileDelete( '\\\\192.168.38.5\\Imaging\\USPSManifest\\mmcp7a.manifest' )
.head 8 -  Call SalMessageBox( 'Congratulations!

You have completed the USPS Manifest Upload Procedure', 'UPLoad Successful', MB_Ok ) 
.head 7 +  Else
.head 8 -  Call SalMessageBox( 'The USPS Manifest file did not get moved to the Archive Directory.

Please notify the programmmer', 'Un-Successful File Rename', MB_Ok ) 
.head 7 -  Call SalDisableWindow( pbSendUSPS )
.head 2 +  Functions
.head 3 +  Function: RecordNumber
.head 4 -  Description: Displays current record number of result set in status bar
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sRowNumber = SalNumberToStrX (nRowNumber + 1 ,0)
.head 5 -  Set sRows = SalNumberToStrX (nRows,0)
.head 5 -  Call SalStatusSetText( hWndForm, 'Record ' || sRowNumber || ' of '  || sRows )
.head 5 -  Set bRetrieve = FALSE
.head 3 +  Function: Retrieve
.head 4 -  Description: Retrieves requested result set
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: sPassBatch
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set nRows = 0
.head 5 -  Call SalWaitCursor( TRUE )
.head 5 -  Set dfCMCount = NUMBER_Null
.head 5 -  Set sSelectString = "Select m.articleno, m.caseyr, c.casetype, m.caseno, m.ddate, m.code5, m.dnum,
		m.confirmed, m.addressline1, m.addressline2, m.addressline3, m.addressline4, m.print, m.batch, s.username,
		substr(nvl(c.code1 || d.entry1 || c.code2 || d.entry2 || c.code3 || d.entry3 || c.code4 || d.entry4, d.memo), 1, 200)
	from crim.certified m, crim.certifiedstatus s, crim.casemaster c, crim.docket d, crim.cvcodes c 
	where m.articleno=s.articleno and s.status in 'Generated' and 
		m.caseyr=c.caseyr and m.caseno=c.caseno and
		m.caseyr=d.caseyr and m.caseno=d.caseno and m.ddate=d.ddate and m.dnum=d.dnum and
		m.code5=d.code5 and m.code5=c.code "
.head 5 +  If sAutoPrint = 'Y' or sAutoPrint = 'F'
.head 6 +  If bPrintbyUser
.head 7 -  Set sSelectString = sSelectString || 'and s.username = user '
.head 6 +  Else
.head 7 -  Set sSelectString = sSelectString || 'and s.username not in (Select username from crim.users where certprintbyuser in 1) '
.head 6 -  Set sSelectString = sSelectString || 'and m.ddate between :dfBegDate and :dfDeliveryDate '
.head 6 -  Set sSelectString = sSelectString || "and batch is null and m.print is null "
.head 5 +  Else If sPassBatch = STRING_Null
.head 6 -  Set sSelectString = sSelectString || 'and m.ddate between :dfBegDate and :dfDeliveryDate '
.head 6 +  If rbUnPrinted
.head 7 -  Set sSelectString = sSelectString || "and batch is null and m.print is null "
.head 6 +  Else
.head 7 +  If rbConfirmed
.head 8 -  Set sSelectString = sSelectString || "and (m.batch is null and m.print in 'Y' and m.confirmed is not null) "
.head 7 +  Else If rbPending
.head 8 -  Set sSelectString = sSelectString || "and (m.batch is null and m.print in 'Y' and m.confirmed is null) "
.head 7 +  Else If rbMine
.head 8 -  Set sSelectString = sSelectString || "and (m.batch is null and s.username in user) "
.head 7 +  Else
.head 8 -  Set sSelectString = sSelectString || "and (m.batch is null and (m.print in 'Y' or m.print is null) )  "
.head 7 -  Call fCreateCRUnionString ( )
.head 5 +  Else
.head 6 -  Set sSelectString = sSelectString || 'and m.batch = :sPassBatch '
.head 6 -  Call fCreateCRUnionString ( )
.head 5 -  Set sSelectString = sSelectString || "
	into :colCPArticle, :colCPCaseYr, :colCPCaseTy, :colCPCaseNo, :colCPDate, :colCPCode5, :colCPDNum,
		:colCPConfirmed, :colCPAddress1, :colCPAddress2, :colCPAddress3, :colCPAddress4, :colCPPrint, :colCPBatch, :colCPClerk, :colCPData"
.head 5 -  Set sSelectString = sSelectString || "
	order by 2, 4, 1"
.head 5 -  Call SalTblPopulate( tblCertProcess, hSql, sSelectString, TBL_FillAll )
.head 5 +  ! If nRows = 0
.head 6 -  Call SalStatusSetText(hWndForm, 'No records retrieved' )
.head 5 -  Set dfCMCount = SalTblColumnSum( tblCertProcess, SalTblQueryColumnPos(colCPPrintCnt), 0, 0 )
.head 5 -  Call SalWaitCursor( FALSE )
.head 5 -  Return nRows
.head 3 +  Function: fWriteHeader
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHdrHour
.head 5 -  String: sUSPSHeader
.head 5 -  Number: nHdrSequence
.head 5 -  Number: nDigit
.head 5 -  Number: nLoop
.head 5 -  Number: nDigit1
.head 5 -  Number: nDigit2
.head 5 -  Number: nDigitOdd
.head 5 -  Number: nDigitEven
.head 5 -  Number: nMod10CheckDigit
.head 4 +  Actions
.head 5 -  Call SalFileOpen( hUSPSUpload, 'I:\\USPSManifest\\mmcp7a.manifest', OF_Create | OF_Write )
.head 5 -  !
.head 5 -  Set sUSPSHeader = 'H1'
.head 5 -  Set sUSPSHeader = sUSPSHeader || '7'
.head 5 -  !
.head 5 -  Set sHdrSequence = '91'
.head 5 -  Set sHdrSequence = sHdrSequence || '50'
.head 5 -  Set sHdrSequence = sHdrSequence || '176849750'
.head 5 +  If SalIsNull (cmbBatch)
.head 6 -  ! PassUpdate
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Update crim.control
	set usps_seq = nvl(usps_seq, 0) + 1' )
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select max(usps_seq)
	from crim.control into :nHdrSequence' )
.head 6 -  Call SqlFetchNext( hSql, nResult )
.head 6 -  Call SqlCommit( hSql )
.head 5 +  Else
.head 6 -  Set nHdrSequence = SalStrToNumber( cmbBatch )
.head 5 -  Set sHdrSequence = sHdrSequence || SalFmtFormatNumber( nHdrSequence, '00000000' )
.head 5 -  Set sUSPSHeader = sUSPSHeader || sHdrSequence || Mod10_Length22( sHdrSequence ) ! || '  '
.head 5 -  Set sHdrSequence = SalFmtFormatNumber( nHdrSequence, '00000000' )
.head 5 -  ! Set nLoop = 20
.head 5 -  ! Set nDigitOdd = 0
.head 5 -  ! Set nDigitEven = 0
.head 5 +  ! While nLoop >= 0
.head 6 -  Set nDigit = SalStrToNumber( SalStrMidX( sHdrSequence, nLoop, 1 ) )
.head 6 +  If SalNumberMod( nLoop, 2 ) = 1
.head 7 -  Set nDigitOdd = nDigitOdd + nDigit
.head 6 +  Else
.head 7 -  Set nDigitEven = nDigitEven + nDigit
.head 6 -  Set nLoop = nLoop - 1
.head 5 -  ! Set nDigit = (nDigitEven * 3) + nDigitOdd
.head 5 -  ! Set nMod10CheckDigit = SalNumberMod( nDigit, 10 )
.head 5 -  ! Set sHdrSequence = sHdrSequence || SalFmtFormatNumber( nMod10CheckDigit, '0' )
.head 5 -  ! Set sUSPSHeader = sUSPSHeader || sHdrSequence
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalFmtFormatDateTime( dfDeliveryDate, 'yyyyMMdd' )
.head 5 -  Set sHdrHour = SalFmtFormatDateTime( dfDeliveryDate, 'hhhh' )
.head 5 +  If SalStrLength( sHdrHour ) = 1
.head 6 -  Set sHdrHour = '0' || sHdrHour
.head 5 +  Else If SalStrLength( sHdrHour ) = 0
.head 6 -  Set sHdrHour = '00' || sHdrHour
.head 5 -  Set sUSPSHeader = sUSPSHeader || sHdrHour || SalFmtFormatDateTime( dfDeliveryDate, 'mmss' )
.head 5 -  Set sUSPSHeader = sUSPSHeader || '44702'
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('0000000000', 10)	! USPS Account No
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('00', 2) 		! Method of Payment
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('00000', 5)		! Post Office of Account
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('           ' || '  ', 12)	! Confirmation Number
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX(' ', 1)		! USPS Will Pick up Mail
.head 5 -  ! Set sUSPSHeader = sUSPSHeader || SalStrLeftX('           ' || '           ' || '           ', 30)
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || '013'	! USPS File Version
.head 5 -  Set sUSPSHeader = sUSPSHeader || '850'	! Developer ID Code
.head 5 -  Set sUSPSHeader = sUSPSHeader || '1004    '	! Shippers Version Number (8)
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalFmtFormatNumber( dfCMCount+1, '000000000' )
.head 5 -  !
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalStrLeftX('           ' || '           ' || '           ', 33)
.head 5 -  Set nDigit = SalStrLength( sUSPSHeader )
.head 5 +  If nDigit != 130
.head 6 -  Call SalMessageBox( 'The length of the Header record is incorrect

The length is ' || SalNumberToStrX( nDigit, 0 ) || '; the lenght should be 130 characters', 'Invalid Header Length', MB_Ok | MB_IconStop )
.head 5 -  ! Set sUSPSHeader = sUSPSHeader || '
'
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalNumberToChar( 13 )
.head 5 -  Set sUSPSHeader = sUSPSHeader || SalNumberToChar( 10 )
.head 5 -  Set nDigit = nDigit + 2
.head 5 -  Call SalFileWrite( hUSPSUpload, sUSPSHeader, nDigit )
.head 3 +  Function: fWriteDetail
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHdrHour
.head 5 -  String: sUSPSDetail
.head 5 -  Number: nHdrSequence
.head 5 -  Number: nDigit
.head 5 -  Number: nLoop
.head 5 -  Number: nDigit1
.head 5 -  Number: nDigit2
.head 5 -  Number: nDigitOdd
.head 5 -  Number: nDigitEven
.head 5 -  Number: nWriteCount
.head 5 -  Number: nMod10CheckDigit
.head 4 +  Actions
.head 5 -  Set nWriteCount = 0
.head 5 -  Set nRows = SalTblSetRow( tblCertProcess, TBL_SetFirstRow )
.head 5 +  While nRows < TBL_MaxRow
.head 6 +  If Not SalIsNull( colCPConfirmed )
.head 7 -  Set sUSPSDetail = 'D1'
.head 7 -  Set sUSPSDetail = sUSPSDetail || 'FC'
.head 7 -  !
.head 7 -  Set sUSPSDetail = sUSPSDetail || colCPArticle || '  '
.head 7 -  Set sUSPSDetail = sUSPSDetail || colCPZip
.head 7 -  ! Zip +4
.head 7 +  If SalStrLength( colCPZip2 ) = 5
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalStrRightX( colCPZip2, 4)
.head 7 +  Else
.head 8 -  Set sUSPSDetail = sUSPSDetail || '    '
.head 7 -  ! Not Used
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '
.head 7 -  ! Postage
.head 7 -  Set sUSPSDetail = sUSPSDetail || '0000' || sPostage  ! was  '0000045'
.head 7 -  ! Not Used
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX('           ', 11)
.head 7 -  ! Destination Rate Indicator (N=nonapplicable services)
.head 7 -  Set sUSPSDetail = sUSPSDetail || 'N'
.head 7 -  ! Not Used
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX('            ' || '            ', 23)
.head 7 -  !
.head 7 -  Set sUSPSDetail = sUSPSDetail || '06'     ! 1st Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || sElectronicReceipt  ! was  '00115'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 2nd Special Service  -- was '  ' & '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 3rd Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 4th Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 5th Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  Set sUSPSDetail = sUSPSDetail || '  '       ! 6th Special Service
.head 7 -  Set sUSPSDetail = sUSPSDetail || '00000'
.head 7 -  ! Client DUNS Number
.head 7 -  Set sUSPSDetail = sUSPSDetail || '176849750'
.head 7 -  ! Customer Reference Number
.head 7 +  If SalStrLeftX( colCPCaseTy, 2 ) = 'CV'
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX(SalFmtFormatNumber( colCPCaseYr, '0000') || 
	colCPCaseTy || SalFmtFormatNumber( colCPCaseNo, '00000') || '           ' || '           ' || '           ', 30)
.head 7 +  Else
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX(SalFmtFormatNumber( colCPCaseYr, '0000') || 
	colCPCaseTy || SalFmtFormatNumber( colCPCaseNo, '00000') || '          ' || SalStrLeftX(colCPClerk, 2) || '           ' || '           ', 30)
.head 7 -  Set sUSPSDetail = sUSPSDetail || SalStrLeftX('           ' || '           ' || '           ' || '           ', 40)
.head 7 -  !
.head 7 -  Set nDigit = SalStrLength( sUSPSDetail )
.head 7 +  If nDigit != 200
.head 8 -  Call SalMessageBox( 'The length of the Detail record is incorrect

The length is ' || SalNumberToStrX( nDigit, 0 ) || '; the length should be 200 characters', 'Invalid Detail Length', MB_Ok | MB_IconStop )
.head 7 -  Set nWriteCount = nWriteCount+1
.head 7 +  If nWriteCount < dfCMCount
.head 8 -  ! Set sUSPSDetail = sUSPSDetail || '
'
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalNumberToChar( 13 )
.head 8 -  Set sUSPSDetail = sUSPSDetail || SalNumberToChar( 10 )
.head 8 -  Set nDigit = nDigit + 2
.head 7 -  Call SalFileWrite( hUSPSUpload, sUSPSDetail, nDigit )
.head 7 +  If SalIsNull( colCPBatch )
.head 8 -  ! PassUpdate
.head 8 +  If SalStrLeftX( colCPCaseTy, 2 ) = 'CV'
.head 9 -  Call SqlPrepareAndExecute ( hSql, "Update crim.certified 
	set batch=:sHdrSequence, confirmed=:colCPConfirmed 
  	where articleno = :tblCertProcess.colCPArticle" )
.head 8 +  Else
.head 9 -  Call SqlPrepareAndExecute ( hSql, "Update crim.cr_certified 
	set batch=:sHdrSequence, confirmed=:colCPConfirmed 
  	where articleno = :tblCertProcess.colCPArticle" )
.head 6 -  Set nRows = SalTblSetRow( tblCertProcess, TBL_SetNextRow )
.head 5 -  Call SqlCommit( hSql )
.head 3 +  Function: fCreateCRUnionString
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sSelectString = sSelectString || "
     Union
	Select m.articleno, to_number(m.caseyr), m.casety, to_number(m.caseno), m.ddate, m.code5, m.dnum,
		m.confirmed, m.addressline1, m.addressline2, m.addressline3, m.addressline4, m.print, m.batch, s.username, nvl(d.data, m.addressline1) 
	from crim.cr_certified m, crim.cr_certifiedstatus s, crim.cr_docket d 
	where m.articleno=s.articleno and s.status in 'Generated' and 
		m.caseyr=d.caseyr(+) and m.casety=d.casety(+) and m.caseno=d.caseno(+) and m.ddate=d.dock_date(+) and m.dnum=d.seq(+)  "
.head 5 +  If cmbBatch = STRING_Null
.head 6 -  Set sSelectString = sSelectString || 'and m.ddate between :dfBegDate and :dfDeliveryDate '
.head 5 +  Else
.head 6 -  Set sSelectString = sSelectString || 'and m.batch = :sPassBatch '
.head 5 -  !
.head 5 +  If rbConfirmed
.head 6 -  Set sSelectString = sSelectString || "and (m.batch is null and m.print in 'Y' and m.confirmed is not null) "
.head 5 +  Else If rbPending
.head 6 -  Set sSelectString = sSelectString || "and (m.batch is null and m.print in 'Y' and m.confirmed is null) "
.head 5 +  Else If rbMine
.head 6 -  Set sSelectString = sSelectString || "and (m.batch is null and s.username in user) "
.head 5 +  Else
.head 6 -  Set sSelectString = sSelectString || "and (m.batch is null and (m.print in 'Y' or m.print is null) )  "
.head 2 +  Window Parameters
.head 3 -  String: sAutoPrint
.head 2 +  Window Variables
.head 3 -  String: strSelect
.head 3 -  String: sPaperTray
.head 3 -  String: sPrinter
.head 3 -  Number: nOrientation
.head 3 -  Number: nPaperType
.head 3 -  Number: nWidth
.head 3 -  Number: nHeight
.head 3 -  String: strUpdate
.head 3 -  String: strDelete
.head 3 -  String: strDeleteMove
.head 3 -  String: strInsert
.head 3 -  String: strMaster
.head 3 -  String: strDocket
.head 3 -  String: strSelectDate
.head 3 -  String: sRowNumber
.head 3 -  String: sRows
.head 3 -  String: sCertNo
.head 3 -  String: sHdrSequence
.head 3 -  String: sSelectString
.head 3 -  String: sReadHeader
.head 3 -  String: sSaveFileYear
.head 3 -  String: sSaveFileName
.head 3 -  String: scolCPBatch
.head 3 -  String: sPostage
.head 3 -  Number: nPostage
.head 3 -  String: sElectronicReceipt
.head 3 -  Number: nElectronicReceipt
.head 3 -  Date/Time: dcolCPConfirmed
.head 3 -  Number: nPrintCnt
.head 3 -  Number: nPrintLabel
.head 3 -  Number: nLabelType
.head 3 -  Number: nFetchResult
.head 3 -  Number: nRow
.head 3 -  Number: nRows
.head 3 -  Number: nRowNumber
.head 3 -  Boolean: bFormDirty
.head 3 -  Number: nCaseYr
.head 3 -  Number: nCaseNo
.head 3 -  Number: nYear
.head 3 -  Number: nFldLength
.head 3 -  Date/Time: dtDate
.head 3 -  Date/Time: dtNow
.head 3 -  Boolean: bRetrieve
.head 3 -  Window Handle: hWndDatafield
.head 3 -  Window Handle: hWndSave
.head 3 -  Boolean: bPrintFlag
.head 3 -  Boolean: bLoginMyValue
.head 3 -  Boolean: bFirstIsEnabled
.head 3 -  Boolean: bPrevIsEnabled
.head 3 -  Boolean: bNextIsEnabled
.head 3 -  Boolean: bLastIsEnabled
.head 3 -  Boolean: bFirstpbLastClick
.head 3 -  !
.head 3 -  File Handle: hUSPSUpload
.head 3 -  String: scolCPCase[*]
.head 3 -  String: scolCPClerk[*]
.head 3 -  String: scolCPCertNo[*]
.head 3 -  String: scolCPAddress1[*]
.head 3 -  String: scolCPAddress2[*]
.head 3 -  String: scolCPAddress3[*]
.head 3 -  String: scolCPAddress4[*]
.head 3 -  String: scolCPBarcodeEncrypted[*]
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 +  If Not bLogin
.head 5 -  Call SqlConnect(hSql)
.head 5 -  Set bLoginMyValue = TRUE
.head 5 -  Call SqlSetResultSet( hSql, TRUE )
.head 4 -  Set nLabelType = 4
.head 4 -  Set bCertifiedProcess = TRUE
.head 4 -  Set nRows = 0
.head 4 -  Set bFormDirty = FALSE
.head 4 -  Set bFirstpbLastClick = TRUE
.head 4 -  Set dtNow = SalDateCurrent(  )
.head 4 -  Set nYear =  SalDateYear( dtNow )
.head 4 -  Set bRetrieve = TRUE
.head 4 -  Call Retrieve(STRING_Null)
.head 4 -  Call SalStatusSetText( hWndForm, 'Enter criteria and execute a query to build a specific result set' )
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_CreateComplete
.head 4 +  If sAutoPrint = 'Y' or sAutoPrint = 'F'
.head 5 +  If bPrintbyUser
.head 6 -  Call SqlPrepareAndExecute( hSql, "Select count(*)
	from crim.certified m, crim.certifiedstatus s into :nPrintCnt
	where m.articleno=s.articleno and s.status in 'Generated' and 
		s.username = :SqlUser and m.ddate >= :dfBegDate and m.print is null" )
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select count(*)
	from crim.certified m into :nPrintCnt
	where m.ddate >= :dfBegDate and m.print is null' )
.head 5 -  Call SqlFetchNext( hSql, nFetchResult )
.head 5 -  Call SalDisableWindow( pbPrintMail )
.head 5 +  If nPrintCnt >= nLabelType or ( nPrintCnt > 0 and sAutoPrint = 'F')
.head 6 -  Call Retrieve(STRING_Null)
.head 6 -  Call SalSendMsg( pbPrint, SAM_Click, 0, 0 )
.head 5 -  Call SalDestroyWindow( hWndForm )
.head 4 +  Else
.head 5 -  Call SqlPrepareAndExecute( hSql, "Select postage, ElectronicReceipt
	from crim.courtinfo into :nPostage, :nElectronicReceipt" )
.head 5 -  Call SqlFetchNext( hSql, nFetchResult )
.head 5 -  Set sPostage = SalFmtFormatNumber( nPostage * 100, '000' )
.head 5 -  Set sElectronicReceipt = SalFmtFormatNumber( nElectronicReceipt * 100, '00000' )
.head 5 +  If VisDosExist( 'I:\\USPSManifest\\mmcp7a.manifest' )
.head 6 -  Call SalDisableWindow( pbProcessSend )
.head 6 -  Call SalFileOpen( hUSPSUpload, 'I:\\USPSManifest\\mmcp7a.manifest', OF_Text | OF_Read )
.head 6 -  Call SalFileRead ( hUSPSUpload, sReadHeader, 24 )
.head 6 -  Call SalStrRight( sReadHeader, 8, sReadHeader )
.head 6 -  Call SalListSelectString( cmbBatch, 0, sReadHeader )
.head 6 -  Call SalDisableWindow( cmbBatch )
.head 6 -  Call SalFileClose( hUSPSUpload )
.head 6 -  Call Retrieve(sReadHeader)
.head 5 +  Else
.head 6 -  Call SalDisableWindow( pbSendUSPS )
.head 3 +  On SAM_Close
.head 4 +  If bFormDirty or SalTblAnyRows( tblCertProcess, ROW_Edited, 0 )
.head 5 +  If SalMessageBox( 'Discard Changes?', 'Confirmation',
MB_YesNo | MB_IconQuestion | MB_DefButton2) = IDNO
.head 6 -  Return FALSE
.head 4 +  If bLogin and bLoginMyValue
.head 5 -  Call SqlDisconnect( hSql )
.head 3 +  On SAM_Destroy
.head 4 -  Set bCertifiedProcess = FALSE
.head 3 -  ! A nPrintCnt of 1 is the Missing Case Report
.head 3 -  ! A nPrintCnt greater the 1 is the 4 Up Certified Labels
.head 3 +  On SAM_ReportStart
.head 4 -  Set nPrintLabel = 0
.head 4 -  Call SalReportGetPrinterTray( SalNumberToWindowHandle(wParam), RPT_PageAll, sPaperTray )
.head 4 +  ! If bDefaultPrinter1 and sPaperTray != 'Tray 2'
.head 5 -  Call SalMessageBox( 'Printer tray currenty set to ' || sPaperTray, 'Set Printer Tray', MB_Ok )
.head 5 -  Call SalReportSetPrinterTray( SalNumberToWindowHandle(wParam), RPT_PageAll, 'Tray 2' )
.head 5 -  Call SalReportGetPrinterTray( SalNumberToWindowHandle(wParam), RPT_PageAll, sPaperTray )
.head 5 -  Call SalReportGetPrinterSettings( SalNumberToWindowHandle(wParam), sPrinter, nOrientation, nPaperType, nWidth, nHeight )
.head 5 -  Call SalMessageBox( 'Printer tray set to ' || sPaperTray, 'Set Printer Tray', MB_Ok )
.head 4 -  Set bPrintFlag = TRUE
.head 4 -  Set nRow = SalTblSetRow( tblCertProcess, TBL_SetFirstRow )
.head 4 +  If nLabelType > 2
.head 5 +  While nRow < TBL_MaxRow and (colCPPrint = 'Y' or colCPPrint = '1')
.head 6 -  Set nRow = SalTblSetRow( tblCertProcess, TBL_SetNextRow )
.head 4 +  Else If nLabelType > 1
.head 5 +  While nRow < TBL_MaxRow and colCPConfirmed != DATETIME_Null
.head 6 -  Set nRow = SalTblSetRow( tblCertProcess, TBL_SetNextRow )
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If nRow > TBL_MaxRow
.head 5 -  Return FALSE
.head 4 +  If nLabelType > 2
.head 5 -  Set nPrintCnt = 0
.head 5 -  Set nPrintLabel = 0
.head 5 -  Call SalSetArrayBounds( scolCPCase, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPClerk, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPCertNo, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPAddress1, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPAddress2, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPAddress3, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPAddress4, 1, -1 )
.head 5 -  Call SalSetArrayBounds( scolCPBarcodeEncrypted, 1, -1 )
.head 5 +  While nPrintCnt < nLabelType
.head 6 +  If Not bPrintFlag
.head 7 +  While nRow < TBL_MaxRow and (colCPPrint = 'Y' or colCPPrint = '1')
.head 8 -  Set nRow = SalTblSetRow( tblCertProcess, TBL_SetNextRow )
.head 7 +  If nRow > TBL_MaxRow or nPrintLabel >= 4
.head 8 +  If nPrintCnt = 0
.head 9 -  Return FALSE
.head 8 +  Else
.head 9 -  Set nRow = TBL_MaxRow + 1
.head 9 -  Return TRUE
.head 6 -  Set nPrintLabel = nPrintLabel + 1
.head 6 -  Set scolCPCase[nPrintCnt] = colCPCase
.head 6 -  Set scolCPClerk[nPrintCnt] = colCPClerk
.head 6 -  Set scolCPCertNo[nPrintCnt] = colCPArticle
.head 6 -  Set scolCPAddress1[nPrintCnt] = colCPAddress1
.head 6 -  Set scolCPAddress2[nPrintCnt] = colCPAddress2
.head 6 -  Set scolCPAddress3[nPrintCnt] = colCPAddress3
.head 6 -  Set scolCPAddress4[nPrintCnt] = colCPAddress4
.head 6 -  Set scolCPBarcodeEncrypted[nPrintCnt] = Code128c(colCPArticle)
.head 6 -  Set colCPPrint = 'Y'
.head 6 -  Set colCPPrintCnt = 1
.head 6 -  Set nPrintCnt = nPrintCnt + 1
.head 6 -  Call SalTblSetRowFlags( tblCertProcess, nRow, ROW_Edited, TRUE )
.head 6 -  Set bPrintFlag = FALSE
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 +  If Not bPrintFlag
.head 6 -  Set nRow = SalTblSetRow( tblCertProcess, TBL_SetNextRow )
.head 6 +  If nLabelType > 1
.head 7 +  While nRow < TBL_MaxRow and colCPConfirmed != DATETIME_Null
.head 8 -  Set nRow = SalTblSetRow( tblCertProcess, TBL_SetNextRow )
.head 6 +  If nRow > TBL_MaxRow
.head 7 -  Return FALSE
.head 5 -  Set scolCPBarcodeEncrypted[1] = Code128c(colCPArticle)
.head 5 -  Set bPrintFlag = FALSE
.head 5 -  Return TRUE
.head 3 +  On SAM_ReportFinish
.head 4 -  Set dfCMCount = SalTblColumnSum( tblCertProcess, SalTblQueryColumnPos(colCPPrintCnt), 0, 0 )
.head 4 -  Return TRUE
