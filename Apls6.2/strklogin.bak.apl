.head 0 +  Application Description: Criminal Software Integrated Library
3-20-2001 - WRP
   * Includes 	-Login Screen
		-Registry checking( for Public/Non-Public Access)
		-Classes for formating of (Caseyr, caseno, casety, dates)
		-Retains last user to login
		-Selects User information from database
		-Selects Court information from database
		-8-2-2001-WRP-Added "/" ability to cDate class, allows the backslash to be entered to display todays date.
.head 1 -  Outline Version - 4.0.39
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000E80000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFF2C0000 002C0000002F0200 0066010000010000 0000000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0002000000FF FEFF07570069006E 0064006F00770073 00FFFEFF0864006C
00A0: 0067004C006F0067 0069006E00
.enddata
.data DT_MAKERUNDLG
0000: 00000000000B6372 6C6F67696E2E6578 650B63726C6F6769 6E2E646C6C0B6372
0020: 6C6F67696E2E6170 6300000101011943 3A5C4372696D696E 616C33325C63726C
0040: 6F67696E2E72756E 19433A5C4372696D 696E616C33325C63 726C6F67696E2E64
0060: 6C6C19433A5C4372 696D696E616C3332 5C63726C6F67696E 2E61706300000101
0080: 0119433A5C437269 6D696E616C33325C 63726C6F67696E2E 61706419433A5C43
00A0: 72696D696E616C33 325C63726C6F6769 6E2E646C6C19433A 5C4372696D696E61
00C0: 6C33325C63726C6F 67696E2E61706300 0001010119433A5C 4372696D696E616C
00E0: 33325C63726C6F67 696E2E61706C1943 3A5C4372696D696E 616C33325C63726C
0100: 6F67696E2E646C6C 19433A5C4372696D 696E616C33325C63 726C6F67696E2E61
0120: 70630000010101
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040002001B00 0200000000000000 00003816A00F0500 1D00FFFF4D61696E
0020: 0020000100040000 0000000000F51E81 0F0000DF00FFFF56 61726961626C6573
0040: 0029000100040000 0000000000F51E81 0F00008600FFFF49 6E7465726E616C20
0060: 46756E6374696F6E 73001E0001000400 000000000000F51E 810F0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: 0.01"
.head 3 -  Top: 0.133"
.head 3 -  Width:  7.96"
.head 3 -  Height: 4.425"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C4389105B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 8.325"
.head 3 -  Top: 0.135"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 1 +  Libraries
.head 2 -  File Include: Registry.apl
.head 2 -  File Include: vttblwin.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 3 +  User
.head 4 -  Number: VKInsert = 0x2D
.head 4 -  Number: VKDelete = 0x2E
.head 4 -  Number: VKRETURN=0x0D
.head 4 -  Number: VKForwardslash = 191
.head 4 -  Number: WM_KEYUP = 0x0101
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.head 3 -  Boolean: bLogin
.head 3 -  ! !!!!!! <<Class instance used to acces the Registry >>
.head 3 -  ! cBTRegistry: REG
.head 3 -  ! !!!!!  <<Handle & String  to open Last User Login File >>
.head 3 -  File Handle: hFLogin
.head 3 -  Sql Handle: hSql
.head 3 -  String: sLogIn
.head 3 -  String: sCourtConst
.head 3 -  ! ! !!!!!! <<LABEL VARIABLES> >>
.head 3 -  String: sReturnRowid
.head 3 -  ! ! !!!!! <<REPORT VARIABLES>>
.head 3 -  String: sReport
.head 3 -  String: sReportInputs
.head 3 -  String: sReportBinds
.head 3 -  Number: nPrintErr
.head 3 -  ! !!!!!  <<CourtInfo Data Fields >>
.head 3 -  String: sCourt
.head 3 -  String: sCourtCity
.head 3 -  String: sCourtClerk
.head 3 -  String: sCourtCounty
.head 3 -  String: sCourtAddr
.head 3 -  String: sCourtAddr2
.head 3 -  String: sCourtCityState
.head 3 -  String: sCourtPhone
.head 3 -  String: sCourtPhone2
.head 3 -  String: sCourtPhone3
.head 3 -  String: sCourtCode
.head 3 -  String: sCourtORI
.head 3 -  Date/Time: dCostsDate
.head 3 -  Number: nVBDFineInc
.head 3 -  Number: nVBDFineIncI
.head 3 -  ! !!!!!  <<User Table Data Fields >>
.head 3 -  String: sUClerk
.head 3 -  String: sUDivision
.head 3 -  String: sUDrawer
.head 3 -  String: sUFullName
.head 3 -  Number: nUserId
.head 3 -  Number: nULevel
.head 3 -  Number: nUCashier
.head 3 -  Number: nURecCopies
.head 3 -  Boolean: bChange
.head 3 -  Number: nError
.head 3 -  ! !!!!!! <<The 'where clause' depending on where you are>>
.head 3 -  Number: nX
.head 3 -  Number: nY
.head 3 -  String: sSelectWhere
.head 3 -  Number: nReturn
.head 2 +  Internal Functions
.head 3 +  Function: GetUserInfo
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive String: SqlUser
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 -  ! Set sSelectWhere = ReadRegistrySelect(  )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	userid, seclevel, division, drawer, cashier, sname, name, rec_copies 
			      FROM		users
			      INTO		:nUserId, :nULevel, :sUDivision, :sUDrawer, :nUCashier, :sUClerk, :sUFullName, :nURecCopies 
			      WHERE username = SqlUser ')
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlCommit( hSql )
.head 3 +  Function: GetCourtInfo
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	Title, Court, Clerk, County, Address, Address2, CityState, Phone,Phone2,Phone3, Code, VBDIncDate, VBDIncFine, VBDIncFineI, CourtConst, ORI 
			               INTO	:sCourt, :sCourtCity, :sCourtClerk, :sCourtCounty, :sCourtAddr, :sCourtAddr2, :sCourtCityState, :sCourtPhone,:sCourtPhone2,:sCourtPhone3, :sCourtCode, :dCostsDate, :nVBDFineInc, :nVBDFineIncI, :sCourtConst, :sCourtORI 
			               FROM	CourtInfo' )
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlCommit( hSql )
.head 5 +  If sCourtConst != 'CMC' and sCourtConst != 'MMC'
.head 6 -  Call SalMessageBox('Error reading Court Title from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 6 -  Call SalQuit( )
.head 5 -  Set sCourtCity = SalStrProperX( sCourtCity )
.head 3 +  ! Function: ReadRegistryTitle
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/Court', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Title', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: WriteUser
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Write )
.head 6 -  Call SalFileWrite( hFLogin, SqlUser, 9 )
.head 6 -  Call SalFileClose( hFLogin )
.head 5 -  ! Call REG.CloseKey( )
.head 5 -  ! Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! Call REG.WriteStringAt('Software/CJIS/LastUser',TRUE, 'User', SqlUser)
.head 5 -  ! Call REG.CloseKey( )
.head 3 +  ! Function: ReadRegistry
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/PUBLIC', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'PUBLIC', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  ! Function: ReadRegistrySelect
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: s
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/UserSelect', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Select', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  ! Function: ReadRegistryLastUser
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: 
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/LastUser', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'User', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  ! Function: ReadRegistryUserCode
.head 4 -  Description: 
.head 4 +  Returns 
.head 5 -  String: 
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  String: s1
.head 5 -  String: s2
.head 5 -  String: sUser
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions 
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If REG.OpenKey( '/Software/CJIS/User', FALSE )
.head 6 +  If REG.ReadString( 'Name', sUser )
.head 7 -  ! ! retrieve a list of value names...
.head 7 +  If SalStrProperX(dlgLogin.dfUser) = sUser
.head 8 -  Call REG.OpenKey( '/Software/CJIS/UserID', FALSE )
.head 8 -  Call REG.ReadString( 'Code', s1 )
.head 8 -  Call REG.OpenKey( '/Software/CJIS/UserCode', FALSE )
.head 8 -  Call REG.ReadString( 'Code', s2 )
.head 8 +  If REG.EnumValues( Arr )
.head 9 -  ! Call ERR( 'Error reading values.' )
.head 9 -  Call REG.CloseKey( )
.head 9 -  Set s1 = SalStrMidX( s2, 0, 2 ) || s1 || SalStrMidX( s2, 4, 8 )
.head 9 -  Return (s1)
.head 5 -  Call REG.CloseKey( )
.head 3 +  Function: Check_Change_Password
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtChange_Password_Date
.head 5 -  String: sChangePassword
.head 5 -  Number: nGraceLogins
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select change_password_date  INTO  :dtChange_Password_Date  from CourtInfo ' )
.head 5 -  Call SqlFetchNext( hSql, nError)
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select change_password, gracelogins
	into  :sChangePassword, :nGraceLogins
	from Users where '||sSelectWhere)
.head 5 -  Call SqlFetchNext( hSql, nError )
.head 5 +  If (dtChange_Password_Date <= SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nGraceLogins > 0and sChangePassword = 'Y')
.head 6 +  If sChangePassword = 'Y'
.head 7 -  Set bChange = TRUE
.head 7 -  Call SalMessageBox( 'PASSWORD HAS EXPIRED,  You have '||SalNumberToStrX(nGraceLogins, 0 )||' grace logins left.  Please change your password.  NOTE:  Password must be unique, not used before for your login!', 'Warning', MB_Ok|MB_IconExclamation )
.head 7 -  Call SalModalDialog( dlgPasswordAPL, hWndForm )
.head 5 +  Else If (dtChange_Password_Date =SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nGraceLogins = 0 and sChangePassword = 'Y')
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalMessageBox(  'Your user login has been disabled, the grace logins of 5 have been used.  Please Contact System Administrator', 'NO GRACE LOGINS', MB_Ok|MB_IconExclamation )
.head 6 -  Call SalQuit(  )
.head 3 +  Function: Inc
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive Number: n
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set n = n + 1
.head 5 -  Return n
.head 3 +  Function: Dec
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive Number: n
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set n = n - 1
.head 5 -  Return n
.head 3 +  Function: Current_Date
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Date/Time: dCurrentDate
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Actions
.head 5 -  Set dCurrentDate = SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 )
.head 5 -  Return dCurrentDate
.head 3 +  Function: Compress
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Receive String: sComp
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sComp = SalStrMidX( sComp, 4, 1 ) || SalStrMidX( sComp, 1, 1 ) || SalStrMidX( sComp, 3, 1 ) || 
	SalStrMidX( sComp, 8, 1 ) || SalStrMidX( sComp, 6, 1 ) || SalStrMidX( sComp, 7, 1 ) || SalStrMidX( sComp, 5, 1 ) || SalStrMidX( sComp, 0, 1 )
.head 5 -  Return TRUE
.head 2 -  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 3830300670
0000: D6040000EE020000 0000000000000000 0200000600FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F19003C00 00FFFE00FF084300
0040: 72004400006F0063 006B006500007400 2200000001003C00 001900150001104F
0060: C2080000001E007A 00821E489D000000 0001801301000001 001900FF0F63008A
0080: 446573006B0A746F 70004C0028690073 744200826F78004E 0100F00B00000056
00A0: 190001102893C200 004500E807004548 9A00190019F50001 2B00450800BA4548
00C0: 0060320000001900 FD018A0045090045 2E48004B00580000 190001BF00A2450A
00E0: 0045480B00640000 00D6190001AF0045 EA0B454882007D00 000019F500012B00
0100: 450C00BA45480060 960000001900FD01 8A00450D00452E48 00AF005800001900
0120: 01BF00A2450E0045 480B00C8000000D6 190001AF0045E80F 0045488200E10000
0140: 0019F500012B0045 1000BA45480060FA 0000001900ED0193 8A00451100452E48
0160: 0001800464000002 000000C6FF0B6300 EA69724CFE73037C 0000000400BC0000
0180: 19E50001008B0048 0007B248489D0066 19001900FD01A200 48000848AC489D00
01A0: 325900190001BF00 2848000948489D6B 004B00D61900012F 004800CA0A48489D
01C0: 420001804B000060 03000000FF0FBF72 A85400726565EE4C 733F5E00D8000003
01E0: 005B190001BE0000 AA4B074B486B0019 00D6190001AF004B EA084B489A003200
0200: 19F50001AB004B09 BA4B4800900180AF 0000041900FF0C63 00884600696C0065
0220: EF733FD600C80000 0700005B190001BE 0000A85100065148 6B001900D6190001
0240: AF0051EA0751489A 00320019F50001AB 005108BA51480062 4B00001900FD01AA
0260: 00510951AE480064 5900190001BF00AA 510A51486B007D00 D6190001AF0051EA
0280: 0B51489A00960019 F50001AB00510CBA 5148001001807D00 000500180000FF0D
02A0: 6300A05200610064 69BA6F4C73BF7860 9A00000005006F19 00F90100AA007306
02C0: 73AE480019590019 0001BF00AA730773 486B003200D61900 01AF0073EA087348
02E0: 9A004B0019F50001 AB007309BA734800 626400001900FD01 AA00730A732E4800
.enddata
.head 3 +  Data Field Class: cCaseYr
.head 4 -  Data
.head 5 -  Maximum Data Length: 4
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalStrLength( MyValue ) = 1
.head 7 -  Set MyValue = '200' || MyValue
.head 6 +  If SalStrLength( MyValue ) = 2
.head 7 +  If MyValue > '49'
.head 8 -  Set MyValue = '19' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  Else
.head 8 -  Set MyValue = '20' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If SalStrLength( MyValue ) = 2 and (MyValue != '19' and MyValue !='20')
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'CRA'
.head 6 +  Else If MyValue = 'B'
.head 7 -  Set MyValue = 'CRB'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'TRC'
.head 6 +  Else If MyValue = 'D'
.head 7 -  Set MyValue = 'TRD'
.head 6 -  If MyValue = 'CRA' OR MyValue = 'CRB'
.head 6 -  Else If MyValue = 'TRC' OR MyValue = 'TRD'
.head 6 -  Else If MyValue = 'N'
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseVTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'CRA'
.head 6 +  Else If MyValue = 'B'
.head 7 -  Set MyValue = 'CRB'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'TRC'
.head 6 +  Else If MyValue = 'D'
.head 7 -  Set MyValue = 'TRD'
.head 6 -  If MyValue = 'CRA' OR MyValue = 'CRB' or MyValue = 'TRC' or MyValue = 'TRD' or
	MyValue = 'VBD' OR MyValue = 'VBB' or MyValue = 'N'
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseNo
.head 4 -  Data
.head 5 -  Maximum Data Length: 5
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.8"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If Not SalIsNull( MyValue )
.head 7 +  While SalStrLength( MyValue ) < 5
.head 8 -  Set MyValue = '0' || MyValue
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cDate
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sTemp1
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 7 -  Set sTemp1 = STRING_Null
.head 7 -  Call SqlPrepareAndExecute( hSql, 'Select text
	from calendar into :sTemp1
	where holiday=:MyValue' )
.head 7 +  If SqlFetchNext( hSql, nReturn )
.head 8 -  Set sTemp1 = SalStrProperX( sTemp1 )
.head 7 +  Else If SalDateWeekday(MyValue ) = 0
.head 8 -  Set sTemp1 = 'Saturday'
.head 7 +  Else If SalDateWeekday(MyValue ) = 1
.head 8 -  Set sTemp1 = 'Sunday'
.head 7 +  If sTemp1 != STRING_Null
.head 8 +  If SalMessageBox( 'Date falls on ' || sTemp1 || '; Do you wish to continue?' , 'Date Warning', MB_YesNo | MB_IconStop | MB_DefButton2 ) = IDNO
.head 9 -  Return VALIDATE_Cancel
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VKForwardslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ( )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cDateDOB
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalDateYear( MyValue ) > SalDateYear( SalDateCurrent(  ) )
.head 7 -  Set MyValue = SalDateConstruct( SalDateYear( MyValue ) - 100, SalDateMonth( MyValue ), SalDateDay( MyValue ), SalDateHour( MyValue ), SalDateMinute( MyValue ), 0 )
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue > SalDateCurrent( ) - 6574
.head 8 +  If IDOK =  SalMessageBox( 'Birth Date is Less than 18 Years, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 3 +  Data Field Class: cLoginUser
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: String
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Unformatted
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Window Handle: hWndNext
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Read )
.head 7 -  Call SalFileRead( hFLogin, sLogIn, 9 )
.head 7 -  Set SqlUser = SalStrTrimX( SalStrLeftX( sLogIn, 9 ) )
.head 7 -  Call SalFileClose( hFLogin )
.head 6 -  ! Set MyValue = SqlUser
.head 6 +  ! If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 6 -  ! Set SqlUser =ReadRegistryLastUser(  )
.head 6 -  Set MyValue = SqlUser
.head 6 +  If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 3 +  Functional Class: Costs
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 5 -  Sql Handle: hSqlCost
.head 5 -  Boolean: bCostClass
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseNo
.head 5 -  String: fCaseTy
.head 4 +  Functions
.head 5 +  Function: C_Insert_Costs
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fCode
.head 7 -  Number: fCount
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dCurrentDate
.head 7 -  String: sCodeAmt
.head 7 -  String: sCodeDesc
.head 7 -  String: sInsertCost
.head 6 +  Actions
.head 7 -  Set fCode = SalStrTrimX( fCode )
.head 7 -  Set dCurrentDate = Current_Date(  )
.head 7 -  Set sCodeAmt = C_Get_Primary_Charge_Type( )
.head 7 +  If sCodeAmt = 'C'
.head 8 -  Set sCodeAmt = 'STATE'
.head 8 -  Set sCodeDesc = 'DESCRIPTION2'
.head 8 +  If fCount = NUMBER_Null 
.head 9 -  Set fCount = C_Count_State ( )
.head 7 +  Else If sCodeAmt = 'D'
.head 8 -  Set sCodeAmt = 'CITY'
.head 8 -  Set sCodeDesc = 'DESCRIPTION'
.head 8 +  If fCount = NUMBER_Null 
.head 9 -  Set fCount = C_Count_City ( )
.head 7 +  Else
.head 8 -  Call C_Error( 'Primary charge not found.' )
.head 8 -  Return FALSE
.head 7 -  Set sInsertCost = "INSERT INTO		cr_costs
				(caseyr, casety, caseno, description,
				 timestamped, title, costs, dock_codes, ratnum)
			SELECT 		:fCaseYr, :fCaseTy, :fCaseNo, "|| sCodeDesc || ",
					:dCurrentDate ," || sCodeAmt || ", ((1 *cost) + ((:fCount - 1) * costx)), code, cr_costs_ratnum_seq.nextval
			 FROM		dock_code_costs
			 WHERE		code =:fCode"
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, sInsertCost)
.head 7 -  Call SqlCommit(hSqlCost)
.head 5 +  Function: C_Get_Primary_Charge_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'C'    -       STATE
		 'D'   -        CITY
		 '~'   -         NO PRIMARY CHARGE EXISTS
.head 6 +  Returns
.head 7 -  String: sChargeType
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sChargeType
.head 6 +  Actions
.head 7 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	cr_charge
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  Set sChargeType = '~'
.head 7 -  Return sChargeType
.head 5 +  Function: C_Error
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fErrorType
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SalMessageBox( fErrorType, 'Error in Inserting Cost', MB_Ok )
.head 5 +  Function: C_Count_City
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountCity
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCity
.head 6 +  Actions
.head 7 -  Set sSelect = "SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 -  Return nCountCity
.head 5 +  Function: C_Count_State
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountState
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountState
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountState
.head 5 +  Function: C_Count_All
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountAll
.head 7 -  String: fCondition
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	condition
			               FROM	dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCondition')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If fCondition = ''
.head 8 -  Set fCondition = '1=1'
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 +  If nCountAll < 1
.head 8 -  Call C_Error( 'No Charges found for Case.' )
.head 8 -  Return FALSE
.head 7 +  Else
.head 8 -  Return nCountAll
.head 5 +  Function: C_Count_Charge
.head 6 -  Description: -Count # of specific type of charge
	-Parameter is the type of charge ('MM', 'M1',...)
	-Returns count
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fChargeType
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCharge
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountCharge
.head 5 +  Function: Costs_Constructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: CaseYr
.head 7 -  String: CaseNo
.head 7 -  String: CaseTy
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bLogin = TRUE
.head 8 +  If bCostClass = FALSE
.head 9 -  Set bCostClass = SqlConnect( hSqlCost )
.head 9 -  Call SqlSetResultSet( hSqlCost, TRUE )
.head 8 -  Set nFetchResult = 0
.head 8 -  Set sSelect = ''
.head 8 -  Set fCaseYr = CaseYr
.head 8 -  Set fCaseNo = CaseNo
.head 8 -  Set fCaseTy = CaseTy
.head 5 +  Function: Costs_Destructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bCostClass = TRUE
.head 8 -  Call SqlDisconnect( hSqlCost )
.head 5 +  Function: C_Is_Moving
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: sMoving
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sMoving
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT 	s2.moving
	       FROM		cr_charge s1,
			crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 3 +  Functional Class: CrDocket
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 4 +  Functions
.head 5 +  Function: D_Insert_Docket
.head 6 -  Description: Adds the Requested Data to the Cr_Docket
	- String (4):		Case No
	- String (3):		Case Ty
	- String (5):		Case Yr
	- Date/Time:	Docket Date
	- String (6):		Docket Code
	- String (250):	Docket Data
	- Number:		Seq (If NULL, will find max seq from Docket)
	- Number:		Seq2 (Always NULL, except on charge entry)
	- String:		Variable Mask
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: DCaseNo
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseYr
.head 7 -  Date/Time: DDate
.head 7 -  String: DCode
.head 7 -  String: DData
.head 7 -  Number: nSeq
.head 7 -  Number: nSeq2
.head 7 -  String: DMask
.head 7 -  String: sCommit
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnSeq
.head 6 +  Actions
.head 7 -  Set nReturnSeq =  D_Decode_Mask(DCaseNo, DCaseTy, DCaseYr, DCode, DMask)
.head 7 +  If nReturnSeq != 0
.head 8 -  Set nSeq = nReturnSeq
.head 7 +  If nSeq = 0 or nSeq = NUMBER_Null
.head 8 -  Call SalMessageBox( 'Warning: ' || DCode || ' Docket Code is being inserted without a sequence number', 'Docket Warning', MB_IconInformation | MB_Ok )
.head 7 -  Call SqlPrepareAndExecute( hSql, 'INSERT INTO	cr_docket
						(caseyr, casety, caseno, dock_date, seq,
						 seq2 ,casecode, data, ratnum)
				VALUES		(:DCaseYr, :DCaseTy, :DCaseNo, :DDate, :nSeq,
						 :nSeq2, :DCode, :DData, cr_docket_ratnum_seq.nextval)')
.head 7 +  If sCommit != 'N'
.head 8 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Docket_Exists
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseNo
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseYr
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute (hSql, 'SELECT	caseno
			      FROM		cr_docket '||fWhere)
.head 7 +  If SqlFetchNext( hSql, nFetchResult )
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: D_Delete_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseNo
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseYr
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSql,'DELETE		cr_docket '||fWhere)
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Update_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseNo
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseYr
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 7 -  String: fDData
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSql,'UPDATE	cr_docket
			    SET		data =:fDData '||fWhere)
.head 7 -  Call SqlCommit( hSql )
.head 5 +  Function: D_Get_Max_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseNo
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseYr
.head 6 +  Static Variables
.head 7 -  Number: fReturnSeq
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	max(seq)
			             FROM	cr_docket
			             WHERE	caseno =:DCaseNo and
					casety =:DCaseTy and
					caseyr =:DCaseYr
			              INTO	:fReturnSeq' )
.head 7 +  If Not SqlFetchNext( hSql, nReturn )
.head 8 -  Set fReturnSeq = 1
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	seq
			     FROM		new_codes
			     INTO		:fReturnSeq
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Decode_Mask
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseNo
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseYr
.head 7 -  String: DCode
.head 7 -  String: DMask
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 +  ! If DMask = ''
.head 8 -  Set fReturnSeq = D_Get_Max_Seq( DCaseNo, DCaseTy, DCaseYr)
.head 8 -  Set fReturnSeq = fReturnSeq + 4
.head 7 +  If DMask = '#'
.head 8 -  Set fReturnSeq = D_Get_Seq(DCode)
.head 7 +  Else If DMask = ''
.head 8 -  Set fReturnSeq = fReturnSeq
.head 7 +  Else
.head 8 -  Set DMask = SalStrReplaceX( DMask, 0, 1, '')
.head 8 +  If SalStrIsValidNumber( DMask )
.head 9 +  If SalStrToNumber(DMask) > 0
.head 10 -  Set fReturnSeq = D_Get_Max_Seq( DCaseNo, DCaseTy, DCaseYr)
.head 10 -  Set fReturnSeq = fReturnSeq + SalStrToNumber(DMask)
.head 8 +  Else
.head 9 -  Call SalMessageBox('Error reading Mask string', 'Invalid Mask', MB_Ok|MB_IconExclamation)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Data
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	description
			     FROM		new_codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSql, nFetchResult)
.head 7 -  Return fReturnData
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 +  Application Actions
.head 3 +  On SAM_AppStartup
.head 4 -  Call SalModalDialog( dlgLogin, hWndNULL, 'Login Test' )
.head 1 +  Dialog Box: dlgLogin
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Criminal Program Login Screen
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.975"
.head 4 -  Top: 0.656"
.head 4 -  Width:  5.925"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.677"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Blue
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description: This window is the initial
login screen
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Picture: picLogin
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.313"
.head 5 -  Top: 0.938"
.head 5 -  Width:  5.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.667"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name: LO178.BMP
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Size to Fit
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 5
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Black
.head 4 -  Background Color: Black
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 28676
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.25"
.head 5 -  Top: 3.74"
.head 5 -  Width:  1.113"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.219"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: UserName:
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 28677
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.275"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.038"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Password:
.head 3 +  Data Field: dfUser
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cLoginUser
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.388"
.head 6 -  Top: 3.667"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( dfUser ) = 10
.head 7 -  Call SalSetFocus( dfPass )
.head 6 +  If Not bChange
.head 7 -  Set bChange = TRUE
.head 7 -  Set dfPass = STRING_Null
.head 3 +  Data Field: dfPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 10
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.388"
.head 6 -  Top: 3.969"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  ! Set MyValue =ReadRegistryUserCode(  )
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 2.413"
.head 5 -  Top: 4.271"
.head 5 -  Width:  1.45"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SqlDatabase = STRING_Null
.head 7 -  Set SqlDatabase = 'CRIM'
.head 6 -  Set dfPass = SalStrUpperX( dfPass )
.head 6 -  Set SqlUser = dfUser
.head 6 -  Set SqlPassword = dfPass
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  When SqlError Default
.head 7 -  Return FALSE
.head 6 -  Set bLogin = SqlConnect (hSql)
.head 6 +  If Not bLogin
.head 7 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Call SalSetFocus( dfUser )
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SqlSetResultSet( hSql, TRUE )
.head 7 -  ! Call Check_Change_Password(  )
.head 6 -  Call SalEndDialog( dlgLogin, 0 )
.head 6 -  Call WriteUser(  )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton( pbOk )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 4.038"
.head 5 -  Top: 4.271"
.head 5 -  Width:  1.45"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalQuit(  )
.head 3 +  Data Field: dfCourt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.4"
.head 6 -  Top: 0.198"
.head 6 -  Width:  5.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.49"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Copperplate Gothic Light
.head 5 -  Font Size: 16
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: Blue
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sWindowTitle
.head 2 +  Window Variables
.head 3 -  String: sLoginTitle
.head 3 -  Number: nFetch
.head 3 -  String: sPublic
.head 3 -  Boolean: bChange
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  ! Set sPublic = ReadRegistry(  )
.head 4 -  ! Set sSelectWhere = ReadRegistrySelect(  )
.head 4 +  ! If sSelectWhere = ''
.head 5 -  Call SalMessageBox('Error reading Where Clause from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 5 -  Call SalQuit( )
.head 4 +  ! If sPublic = 'Y'
.head 5 -  Call SalMessageBox( 'Access has been denied', 'Error', MB_Ok|MB_IconExclamation )
.head 5 -  Call SalQuit(  )
.head 4 -  ! Set sLoginTitle = ReadRegistryTitle(  )
.head 4 -  Set dfCourt = 'Canton Municipal Court'
.head 4 +  If sWindowTitle != STRING_Null
.head 5 -  Call SalSetWindowText( hWndForm, sWindowTitle )
.head 1 +  Dialog Box: dlgPasswordAPL
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Change Password
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.163"
.head 4 -  Top: 1.813"
.head 4 -  Width:  4.06"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.963"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Data Field: dfCurrentPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 8
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.242"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfNewPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 8
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.575"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfVerifyPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 8
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.908"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChange
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Change Password
.head 4 -  Window Location and Size
.head 5 -  Left: 0.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set dfCurrentPass = SalStrUpperX( dfCurrentPass )
.head 6 +  If SalStrLength( dfNewPass ) < 3
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('A Password Must be at Least 6 Charaters Long', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If dfNewPass != dfVerifyPass
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Passwords do not Match', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If dfCurrentPass != SqlPassword
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Current Password must be Re-entered before Password can be Changed','Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX( dfVerifyPass) =SalStrUpperX(  SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox( 'Password Must be UNIQUE, please re-enter a valid password', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SqlConnect (hSql)
.head 6 +  If SqlPLSQLCommand(hSql, 'change_pwd(SqlUser,dfNewPass)' )
.head 7 -  Call SalMessageBox( 'Password Successfully Changed', 'Change Password',MB_Ok|MB_IconExclamation)
.head 7 -  Call SqlPrepareAndExecute( hSql,"Update users set change_password = 'N' ,gracelogins = 5 where "||sSelectWhere)
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Set SqlPassword = dfNewPass
.head 7 -  Set bChange = FALSE
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Password Change failed - - Contact Programmer', 'Change Password',MB_Ok|MB_IconStop)
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 2.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 52744
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.258"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Current Password:
.head 3 -  Background Text: bkgd4
.head 4 -  Resource Id: 52745
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.592"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: New Password:
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 52746
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.925"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Retype Password:
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nErrorCnt
.head 3 -  Boolean: bOk
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set nErrorCnt = 0
.head 4 -  Call SalWaitCursor( TRUE )
.head 3 +  On SAM_Destroy
.head 4 +  If bChange = TRUE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'update users set gracelogins = gracelogins - 1 where userid = uid' )
.head 5 -  Call SqlCommit( hSql )
.head 5 -  Set bChange = FALSE
.head 1 +  Dialog Box: dlgLabels
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Labels Processing
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.038"
.head 4 -  Top: 0.552"
.head 4 -  Width:  8.075"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 3.865"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description: !Added docket entry made for Bank Attach - Notice Debtor
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Data Field: dfName
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.838"
.head 6 -  Top: 1.281"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.838"
.head 6 -  Top: 1.594"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.838"
.head 6 -  Top: 1.958"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelCity
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.838"
.head 6 -  Top: 2.333"
.head 6 -  Width:  2.963"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelState
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.8"
.head 6 -  Top: 2.333"
.head 6 -  Width:  0.55"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelZip
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.35"
.head 6 -  Top: 2.333"
.head 6 -  Width:  1.988"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfNumber
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.325"
.head 6 -  Top: 2.906"
.head 6 -  Width:  0.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  ! Pushbutton: pbPrintLabel	!5/7/2002  CMC - not used
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print &Label
.head 4 -  Window Location and Size
.head 5 -  Left: 0.275"
.head 5 -  Top: 3.375"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.end
.head 4 +  Message Actions 
.head 5 +  On SAM_Click
.head 6 -  Set sReport = 'Labels.qrp'
.head 6 +  If dfName != ''
.head 7 -  Call PrintLabels(  )
.head 3 +  Pushbutton: pbEnvelope
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print &Envelope
.head 4 -  Window Location and Size
.head 5 -  Left: 0.275"
.head 5 -  Top: 3.375"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sReport = 'Envelope.qrp'
.head 6 +  If dfName != ''
.head 7 -  Call PrintLabels(  )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel, Exit
.head 4 -  Window Location and Size
.head 5 -  Left: 5.6"
.head 5 -  Top: 3.375"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 33767
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.913"
.head 5 -  Top: 1.323"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 33768
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.913"
.head 5 -  Top: 2.0"
.head 5 -  Width:  0.913"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address 2:
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 33769
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.913"
.head 5 -  Top: 2.375"
.head 5 -  Width:  0.875"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: City:
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 33770
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.663"
.head 5 -  Top: 2.927"
.head 5 -  Width:  1.5"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Number of Labels
.head 3 -  Background Text: bkgd10
.head 4 -  Resource Id: 33771
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.9"
.head 5 -  Top: 1.073"
.head 5 -  Width:  0.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 3 -  Background Text: bkgd11
.head 4 -  Resource Id: 33772
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.913"
.head 5 -  Top: 1.635"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address:
.head 3 +  Radio Button: rbDefendant
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: For Defendant - F1
.head 4 -  Window Location and Size
.head 5 -  Left: 1.138"
.head 5 -  Top: 0.167"
.head 5 -  Width:  1.763"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SalSendMsg( rbDefendant, SAM_Click, 0, 0 )
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( )
.head 3 +  Radio Button: rbDAttorney
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Defendant's Atty - F2
.head 4 -  Window Location and Size
.head 5 -  Left: 1.138"
.head 5 -  Top: 0.552"
.head 5 -  Width:  2.0"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbBondsman
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Bondsman - F3
.head 4 -  Window Location and Size
.head 5 -  Left: 3.363"
.head 5 -  Top: 0.167"
.head 5 -  Width:  1.538"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'BOND') = 1
.head 7 -  Call FillScreen( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbOtherAtt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Attorney - F5
.head 4 -  Window Location and Size
.head 5 -  Left: 5.225"
.head 5 -  Top: 0.167"
.head 5 -  Width:  1.85"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'ATT') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbOtherAgency
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Agency - F6
.head 4 -  Window Location and Size
.head 5 -  Left: 5.213"
.head 5 -  Top: 0.563"
.head 5 -  Width:  1.75"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'OTHERA') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbPolice
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Police Agency - F4
.head 4 -  Window Location and Size
.head 5 -  Left: 3.363"
.head 5 -  Top: 0.552"
.head 5 -  Width:  1.75"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'POLICE') = 1
.head 7 -  Call FillScreen( )
.head 3 +  ! Pushbutton: pbMailing		!7/7/99  MMC - not used
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Certificate of Mailing
.head 4 -  Window Location and Size
.head 5 -  Left: 2.375"
.head 5 -  Top: 5.292"
.head 5 -  Width:  2.175"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.end
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sReportType = 'Mailing.qrp'
.head 6 -  ! Call PrintLabels(  )
.head 6 -  Set bCertified = FALSE
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 -  Frame: frame1
.head 4 -  Resource Id: 33773
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.9"
.head 5 -  Top: 0.063"
.head 5 -  Width:  6.463"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.885"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Border Color: Default
.head 4 -  Background Color: White
.head 3 +  Radio Button: rbScreen
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Screen
.head 4 -  Window Location and Size
.head 5 -  Left: 2.975"
.head 5 -  Top: 3.052"
.head 5 -  Width:  0.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Gray
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Radio Button: rbPrinter
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Printer
.head 4 -  Window Location and Size
.head 5 -  Left: 2.975"
.head 5 -  Top: 2.802"
.head 5 -  Width:  0.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Gray
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = TRUE
.head 3 -  Group Box: grp1
.head 4 -  Resource Id: 33774
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.625"
.head 5 -  Top: 1.042"
.head 5 -  Width:  6.813"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.698"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address Information:
.head 3 +  Pushbutton: pb1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F1
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDefendant = TRUE
.head 6 -  Call SalSendMsg(rbDefendant, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F2
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDAttorney = TRUE
.head 6 -  Call SalSendMsg(rbDAttorney, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb3
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F3
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbBondsman = TRUE
.head 6 -  Call SalSendMsg(rbBondsman, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb4
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F4
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbPolice = TRUE
.head 6 -  Call SalSendMsg(rbPolice, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb5
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F5
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAtt = TRUE
.head 6 -  Call SalSendMsg(rbOtherAtt, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb6
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F6
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAgency = TRUE
.head 6 -  Call SalSendMsg(rbOtherAgency, SAM_Click, 0,0)
.head 2 +  Functions
.head 3 +  Function: PrintLabels
.head 4 -  Description: Call this function to 15/16 x 3 1/2 Inch Labels
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sCSZ = dfLabelCity||', '||dfLabelState||'   '||dfLabelZip
.head 5 -  Set sAddress = dfAddress
.head 5 -  Set sAddress2 = dfAddress2
.head 5 +  If sAddress2 = ''
.head 6 -  Set sAddress2 = sCSZ
.head 6 -  Set sCSZ = ''
.head 5 -  Set nPrintErr = -1
.head 5 -  Set sReportBinds = 'dfName,sAddress, sAddress2, sCSZ'
.head 5 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY'
.head 5 +  If rbScreen = TRUE
.head 6 -  Call SalReportView( hWndForm, hWndNULL, sReport,
    sReportBinds, sReportInputs, nPrintErr )
.head 5 +  Else If rbPrinter = TRUE
.head 6 -  Call SalReportPrint ( dlgLabels, sReport, sReportBinds, sReportInputs,
	dlgLabels.dfNumber, RPT_PrintAll, nRow, nMaxRow, nPrintErr )
.head 5 +  If nPrintErr > 0
.head 6 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),
	MB_Ok )
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If rbDefendant = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	lname||', '||fname||' '||mname, address1, address2, city, state, zip
			     FROM		cr_parties
			     INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If sCaseNoP = ''
.head 7 -  Call ClearScreen( )
.head 5 +  Else If rbDAttorney = TRUE
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	attno
			     FROM		muni_booking
			     INTO		:nAttNo
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP')
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 -  If nAttNo = NUMBER_Null
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute(hSql, "SELECT	 fname||' '||mname||' '||lname, address, address2,
					 city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	attno = :nAttNo")
.head 8 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbBondsman = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	bondsman,address1, address2, city, state, zip1
			      FROM		cr_bondsman
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbOtherAtt = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	fname||' '||mname||' '||lname,address, address2, city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbPolice = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agency,address1, address2, city, state, zip
			      FROM		agency_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbOtherAgency = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agencynm,address1, address2, city, state, zip
			      FROM		other_agency
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 3 +  Function: ClearScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set dfName = ''
.head 5 -  Set dfAddress = ''
.head 5 -  Set dfAddress2 = ''
.head 5 -  Set dfLabelCity = ''
.head 5 -  Set dfLabelState = ''
.head 5 -  Set dfLabelZip = ''
.head 5 -  Set sCSZ = ''
.head 2 +  Window Parameters
.head 3 -  String: sCaseYrP
.head 3 -  String: sCaseTyP
.head 3 -  String: sCaseNoP
.head 2 +  Window Variables
.head 3 -  ! String: sFind
.head 3 -  ! String: sSearch
.head 3 -  Number: nRow
.head 3 -  Number: nMaxRow
.head 3 -  String: sAddress2
.head 3 -  String: sAddress
.head 3 -  Number: nAttNo
.head 3 -  String: sCSZ
.head 3 -  ! String: sCode1
.head 3 -  ! String: sCode2
.head 3 -  ! String: sCode3
.head 3 -  ! String: sLECode
.head 3 -  ! String: sLCaseNo
.head 3 -  ! String: sLDefName
.head 3 -  ! String: sLDefAddr2
.head 3 -  ! String: sLDefAddr
.head 3 -  ! String: sLDefCSZ
.head 3 -  ! String: sLPlaName
.head 3 -  ! String: sLPlaAddr2
.head 3 -  ! String: sLPlaAddr
.head 3 -  ! String: sLPlaCSZ
.head 3 -  ! String: sNDefName
.head 3 -  ! String: sNDefName2
.head 3 -  ! String: sNPlaName
.head 3 -  ! String: sNPlaName2
.head 3 -  ! Date/Time: dtHearDt
.head 3 -  ! Number: nOffSet
.head 3 -  ! String: sBDebtNtcService
.head 3 -  Number: nRptFlag
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalSetDefButton( SalGetFirstChild( hWndForm, TYPE_PushButton ) )
.head 4 -  Set dfNumber= 1
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlag=0
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 -  Set nRptFlag=nRptFlag+1
.head 4 +  If nRptFlag=1
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 1 +  Dialog Box: dlgLabelHelp
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.55"
.head 4 -  Top: 0.135"
.head 4 -  Width:  7.338"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.802"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Child Table: tblDisplay
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 0.052"
.head 6 -  Width:  7.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 4.625"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 4000
.head 5 -  Discardable? No
.head 4 +  Contents
.head 5 +  Column: col1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.871"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: col2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colRowID
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colSearch
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Index
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 +  Window Variables
.head 5 -  String: sLetter
.head 4 +  Message Actions
.head 5 +  On SAM_DoubleClick
.head 6 -  Set sReturnRowid = colRowID
.head 6 -  Call SalEndDialog(dlgLabelHelp, 1)
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VKRETURN
.head 7 -  Call SalSendMsg( tblDisplay, SAM_DoubleClick, 0, 0 )
.head 6 +  Else
.head 7 -  Call SalTblSetFocusRow( tblDisplay, VisTblFindString (tblDisplay, 0, colSearch, SalNumberToChar(wParam)))
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sType
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 +  If sType = 'BOND'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, 'SELECT	bondsman, insurance_co, substr(bondsman, 1, 1), rowid
			           FROM	cr_bondsman
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colSearch, :tblDisplay.colRowID
			           ORDER BY	bondsman', TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'BONDSMAN')
.head 5 -  Call SalTblSetColumnTitle(tblDisplay.col2,  'INSURANCE CO')
.head 4 +  Else If sType = 'ATT'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	to_char(attno), fname||' '||mname||' '||lname, substr(lname, 1, 1), rowid
			           FROM	attorney
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colSearch, :tblDisplay.colRowID
			           ORDER BY	lname, fname, mname", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'ATT. #')
.head 5 -  Call SalTblSetColumnWidth(tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth(tblDisplay.col2, 6)
.head 5 -  Call SalTblSetColumnTitle(tblDisplay.col2,  'ATT NAME')
.head 4 +  Else If sType = 'POLICE'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	code, agency, substr(agency, 1, 1), rowid
			           FROM	agency_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colSearch, :tblDisplay.colRowID
			           ORDER BY	code", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'AGENCY CODE')
.head 5 -  Call SalTblSetColumnWidth(tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth(tblDisplay.col2, 6)
.head 5 -  Call SalTblSetColumnTitle(tblDisplay.col2,  'AGENCY NAME')
.head 4 +  Else If sType = 'OTHERA'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	agencynm, substr(agencynm, 1, 1), rowid
			           FROM	other_agency
			           INTO	:tblDisplay.col1, :tblDisplay.colSearch, :tblDisplay.colRowID
			           ORDER BY	agencynm", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'AGENCY NAME')
.head 5 -  Call SalTblSetColumnWidth(tblDisplay.col1,7.088)
.head 5 -  Call SalHideWindow(tblDisplay.col2)
