.head 0 +  Application Description: Centura Team Developper 3.0.0
© Christian Astor 
    - Adresse E-mail : 	castorix@club-internet.fr
® 3 Décembre 2000
® 21 Novembre 2002 : Addition of printing
® 17 Mars 2004 : Remove DLL (SaveBitmap() + PrintBitmap() in CTD)
.head 1 -  Outline Version - 4.0.39
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000FB0000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0001000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0000000000
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left: -0.013"
.head 3 -  Top: 0.0"
.head 3 -  Width:  8.013"
.head 3 -  Height: 4.969"
.head 2 +  Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 6.388"
.head 3 -  Top: 0.729"
.head 2 -  Fully Qualified External References? Yes
.head 2 -  Reject Multiple Window Instances? No
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 1 +  Libraries
.head 2 -  File Include: cstructl.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: 3D Face Color
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 3 +  Library name: USER32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: CopyImage
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: UINT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: UINT
.head 4 +  Function: DestroyIcon
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: DrawIconEx
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: UINT
.head 6 -  Number: LONG
.head 6 -  Number: UINT
.head 4 +  Function: GetCapture
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Window Handle: HWND
.head 5 -  Parameters
.head 4 +  Function: GetCursorPos
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 4 +  Function: GetDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: GetDesktopWindow
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Window Handle: HWND
.head 5 -  Parameters
.head 4 +  Function: GetIconInfo
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 +  structPointer
.head 7 -  Receive Boolean: BOOL
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: HANDLE
.head 4 +  Function: GetMessageExtraInfo
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 -  Parameters
.head 4 +  Function: GetWindowDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: GetWindowRect
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 4 +  Function: mouse_event
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 4 +  Function: ReleaseCapture
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 -  Parameters
.head 4 +  Function: ReleaseDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 -  Number: LONG
.head 4 +  Function: SetCapture
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Window Handle: HWND
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: SetWindowTextA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 -  String: LPCSTR
.head 4 +  Function: ShowWindow
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 -  Number: INT
.head 4 +  Function: UpdateWindow
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: WindowFromPoint
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Window Handle: HWND
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 -  !
.head 4 +  Function: CloseClipboard
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 -  Parameters
.head 4 +  Function: EmptyClipboard
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 -  Parameters
.head 4 +  Function: OpenClipboard
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: SetClipboardData
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: UINT
.head 6 -  Number: HANDLE
.head 3 +  Library name: GDI32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: BitBlt
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: DWORD
.head 4 +  Function: CreateDCA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  String: LPVOID
.head 4 +  Function: CreateDIBSection
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  String: LPVOID
.head 6 -  Number: UINT
.head 6 -  Receive String: LPVOID
.head 6 -  Number: HANDLE
.head 6 -  Number: DWORD
.head 4 +  Function: CreatePen
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: LONG
.head 4 +  Function: DeleteObject
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: DeleteDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: DeleteEnhMetaFile
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: CreateSolidBrush
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 4 +  Function: CreateCompatibleBitmap
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 4 +  Function: CreateCompatibleDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: GetDeviceCaps
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 4 +  Function: GetDIBits
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: HANDLE
.head 6 -  Number: UINT
.head 6 -  Number: UINT
.head 6 -  Receive String: LPVOID
.head 6 -  String: LPVOID
.head 6 -  Number: UINT
.head 4 +  Function: GetObjectA
.head 5 -  Description: int GetObject(  HGDIOBJ hgdiobj,	// handle to graphics object of interest
 		int cbBuffer,		// size of buffer for object information
 		LPVOID lpvObject 	// pointer to buffer for object information );
 Cas particulier : LPVOID pointe sur un objet Bitmap.

.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Receive String: LPVOID
.head 4 +  Function: PatBlt
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: LONG
.head 4 +  Function: PlayEnhMetaFile
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: HANDLE
.head 6 +  structPointer
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 4 +  Function: Rectangle
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 4 +  Function: ResetDCA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  String: LPVOID
.head 4 +  Function: SelectObject
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: SetROP2
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: INT
.head 4 +  Function: SetStretchBltMode
.head 5 -  Description: SETSTRETCHBLTMODE
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: WORD
.head 6 -  Number: INT
.head 4 +  Function: SetTextColor
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: LONG
.head 6 -  Number: LONG
.head 4 +  Function: StretchBlt
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: DWORD
.head 4 +  Function: StretchDIBits
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: LPVOID
.head 6 -  String: LPVOID
.head 6 -  Number: UINT
.head 6 -  Number: DWORD
.head 4 +  Function: AbortDoc
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: EndDoc
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: EndPage
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: Escape
.head 5 -  Description: ESCAPE
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  String: LPCSTR
.head 6 -  Receive String: LPVOID
.head 4 +  Function: ExtEscape
.head 5 -  Description: ESCAPE
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  String: LPCSTR
.head 6 -  Number: INT
.head 6 -  Receive String: LPVOID
.head 4 +  Function: SetAbortProc
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  String: LPVOID
.head 4 +  Function: StartDocA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 +  structPointer
.head 7 -  Number: INT
.head 7 -  String: LPSTR
.head 7 -  String: LPSTR
.head 7 -  String: LPSTR
.head 7 -  Number: DWORD
.head 4 +  Function: StartPage
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 3 +  Library name: COMDLG32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: PrintDlgA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Receive String: LPVOID
.head 3 +  ! Library name: COMDLG32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: PrintDlgA
.head 5 -  Description: 
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 +  structPointer 
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Window Handle: HWND
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: WORD
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: DWORD
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive String: LPSTR
.head 7 -  Receive String: LPSTR
.head 7 -  Receive Number: HANDLE
.head 7 -  Receive Number: HANDLE
.head 4 +  ! Function: PrintDlgExA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LONG
.head 5 +  Parameters
.head 6 -  Receive String: LPVOID
.head 4 +  Function: PageSetupDlgA
.head 5 -  Description: 
.head 5 -  Export Ordinal: 0
.head 5 +  Returns 
.head 6 -  Boolean: BOOL
.head 5 +  Parameters 
.head 6 -  Receive String: LPVOID
.head 3 +  Library name: KERNEL32.DLL
.head 4 -  ThreadSafe: No
.head 4 +  Function: RtlZeroMemory
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 -  Returns
.head 5 +  Parameters
.head 6 -  String: LPSTR
.head 6 -  Number: DWORD
.head 4 +  Function: CloseHandle
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: CreateFileA
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Number: LPVOID
.head 6 -  Number: DWORD
.head 6 -  Number: DWORD
.head 6 -  Number: HANDLE
.head 4 +  Function: WriteFile
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  String: LPVOID
.head 6 -  Number: DWORD
.head 6 -  Receive Number: LPDWORD
.head 6 -  String: LPVOID
.head 4 +  Function: GetLastError
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 -  Parameters
.head 4 +  Function: GlobalLock
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: LPVOID
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: GlobalUnlock
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 +  System
.head 4 -  Number: WM_MOUSEMOVE	= 0x0200
.head 4 -  Number: WM_LBUTTONDOWN	= 0x0201
.head 4 -  Number: WM_LBUTTONUP	= 0x0202
.head 4 -  Number: WM_LBUTTONDBLCLK	= 0x0203
.head 4 -  Number: MOUSEEVENTF_LEFTDOWN	= 0x0002
.head 4 -  Number: MOUSEEVENTF_LEFTUP 	 	= 0x0004
.head 4 -  Number: SW_SHOWMINIMIZED	= 2
.head 4 -  Number: SW_RESTORE		= 9
.head 4 -  Number: PS_SOLID		= 0
.head 4 -  Number: R2_NOTXORPEN		= 10
.head 4 -  Number: SRCCOPY		= 0x00CC0020
.head 4 -  Number: IMAGE_BITMAP		= 0
.head 4 -  Number: IMAGE_ICON		= 1
.head 4 -  Number: WHITENESS    		= 0x00FF0062
.head 4 -  Number: COLORONCOLOR		= 3
.head 4 -  Number: DI_NORMAL 		= 0x0003
.head 4 -  Number: IMAGE_ENHMETAFILE	= 3
.head 4 -  Number: STM_SETIMAGE 		= 0x0172
.head 4 -  Number: CF_BITMAP =		0x2
.head 4 -  Number: LR_COPYDELETEORG	= 0x0008
.head 4 -  Number: DIB_RGB_COLORS = 0
.head 4 -  Number: DIB_PAL_COLORS = 1
.head 4 -  Number: HORZRES     =  8
.head 4 -  Number: VERTRES    =   10
.head 4 -  Number: BI_RGB = 0
.head 4 -  Number: BI_RLE8 = 1
.head 4 -  Number: BI_RLE4 = 2
.head 4 -  Number: BI_BITFIELDS = 3
.head 4 -  Number: BI_JPEG = 4
.head 4 -  Number: BI_PNG = 5
.head 4 -  Number: INVALID_HANDLE_VALUE			= 0xFFFFFFFF
.head 4 -  Number: GENERIC_ALL		 = 0x10000000
.head 4 -  Number: GENERIC_EXECUTE	 = 0x20000000
.head 4 -  Number: GENERIC_WRITE		 = 0x40000000
.head 4 -  Number: GENERIC_READ		 = 0x80000000
.head 4 -  Number: FILE_SHARE_READ	 = 0x00000001
.head 4 -  Number: FILE_SHARE_WRITE	 = 0x00000002
.head 4 -  Number: FILE_SHARE_DELETE	 = 0x00000004
.head 4 -  Number: FILE_ATTRIBUTE_READONLY	= 0x00000001
.head 4 -  Number: FILE_ATTRIBUTE_HIDDEN  	= 0x00000002
.head 4 -  Number: FILE_ATTRIBUTE_SYSTEM	= 0x00000004
.head 4 -  Number: FILE_ATTRIBUTE_DIRECTORY	= 0x00000010
.head 4 -  Number: FILE_ATTRIBUTE_ARCHIVE	= 0x00000020
.head 4 -  Number: FILE_ATTRIBUTE_ENCRYPTED	= 0x00000040
.head 4 -  Number: FILE_ATTRIBUTE_NORMAL	= 0x00000080
.head 4 -  Number: FILE_ATTRIBUTE_TEMPORARY	= 0x00000100
.head 4 -  Number: FILE_ATTRIBUTE_SPARSE_FILE	= 0x00000200
.head 4 -  Number: FILE_ATTRIBUTE_REPARSE_POINT = 0x00000400
.head 4 -  Number: FILE_ATTRIBUTE_COMPRESSED	= 0x00000800
.head 4 -  Number: FILE_ATTRIBUTE_OFFLINE	= 0x00001000
.head 4 -  Number: FILE_ATTRIBUTE_NOT_CONTENT_IND= 0x00002000
.head 4 -  Number: FILE_FLAG_OVERLAPPED	 = 0x40000000
.head 4 -  Number: CREATE_NEW 		= 1
.head 4 -  Number: CREATE_ALWAYS 	= 2
.head 4 -  Number: OPEN_EXISTING 		= 3
.head 4 -  Number: OPEN_ALWAYS 		= 4
.head 4 -  Number: TRUNCATE_EXISTING 	= 5
.head 4 -  Number: DMORIENT_PORTRAIT  = 1
.head 4 -  Number: DMORIENT_LANDSCAPE  = 2
.head 4 -  !
.head 4 -  Number: DM_ORIENTATION      =0x00000001
.head 4 -  Number: DM_PAPERSIZE        =0x00000002
.head 4 -  Number: DM_PAPERLENGTH      =0x00000004
.head 4 -  Number: DM_PAPERWIDTH       =0x00000008
.head 4 -  Number: DM_SCALE            =0x00000010
.head 4 -  Number: DM_POSITION         =0x00000020
.head 4 -  Number: DM_NUP              =0x00000040
.head 4 -  Number: DM_COPIES           =0x00000100
.head 4 -  Number: DM_DEFAULTSOURCE    =0x00000200
.head 4 -  Number: DM_PRINTQUALITY     =0x00000400
.head 4 -  Number: DM_COLOR            =0x00000800
.head 4 -  Number: DM_DUPLEX           =0x00001000
.head 4 -  Number: DM_YRESOLUTION      =0x00002000
.head 4 -  Number: DM_TTOPTION         =0x00004000
.head 4 -  Number: DM_COLLATE          =0x00008000
.head 4 -  Number: DM_FORMNAME         =0x00010000
.head 4 -  Number: DM_LOGPIXELS        =0x00020000
.head 4 -  Number: DM_BITSPERPEL       =0x00040000
.head 4 -  Number: DM_PELSWIDTH        =0x00080000
.head 4 -  Number: DM_PELSHEIGHT       =0x00100000
.head 4 -  Number: DM_DISPLAYFLAGS     =0x00200000
.head 4 -  Number: DM_DISPLAYFREQUENCY =0x00400000
.head 4 -  Number: DM_ICMMETHOD        =0x00800000
.head 4 -  Number: DM_ICMINTENT        =0x01000000
.head 4 -  Number: DM_MEDIATYPE        =0x02000000
.head 4 -  Number: DM_DITHERTYPE       =0x04000000
.head 4 -  Number: DM_PANNINGWIDTH     =0x08000000
.head 4 -  Number: DM_PANNINGHEIGHT    =0x10000000
.head 4 -  ! ! !-
.head 4 -  ! ! !-  Flags for PrintDlg and PrintDlgEx.
.head 4 -  ! ! !-
.head 4 -  Number: PD_ALLPAGES                    = 0x00000000
.head 4 -  Number: PD_SELECTION                   = 0x00000001
.head 4 -  Number: PD_PAGENUMS                    = 0x00000002
.head 4 -  Number: PD_NOSELECTION                 = 0x00000004
.head 4 -  Number: PD_NOPAGENUMS                  = 0x00000008
.head 4 -  Number: PD_COLLATE                     = 0x00000010
.head 4 -  Number: PD_PRINTTOFILE                 = 0x00000020
.head 4 -  Number: PD_PRINTSETUP                  = 0x00000040
.head 4 -  Number: PD_NOWARNING                   = 0x00000080
.head 4 -  Number: PD_RETURNDC                    = 0x00000100
.head 4 -  Number: PD_RETURNIC                    = 0x00000200
.head 4 -  Number: PD_RETURNDEFAULT               = 0x00000400
.head 4 -  Number: PD_SHOWHELP                    = 0x00000800
.head 4 -  Number: PD_ENABLEPRINTHOOK             = 0x00001000
.head 4 -  Number: PD_ENABLESETUPHOOK             = 0x00002000
.head 4 -  Number: PD_ENABLEPRINTTEMPLATE         = 0x00004000
.head 4 -  Number: PD_ENABLESETUPTEMPLATE         = 0x00008000
.head 4 -  Number: PD_ENABLEPRINTTEMPLATEHANDLE   = 0x00010000
.head 4 -  Number: PD_ENABLESETUPTEMPLATEHANDLE   = 0x00020000
.head 4 -  Number: PD_USEDEVMODECOPIES            = 0x00040000
.head 4 -  Number: PD_USEDEVMODECOPIESANDCOLLATE  = 0x00040000
.head 4 -  Number: PD_DISABLEPRINTTOFILE          = 0x00080000
.head 4 -  Number: PD_HIDEPRINTTOFILE             = 0x00100000
.head 4 -  Number: PD_NONETWORKBUTTON             = 0x00200000
.head 4 -  ! !
.head 4 -  Number: PD_CURRENTPAGE                 = 0x00400000
.head 4 -  Number: PD_NOCURRENTPAGE               = 0x00800000
.head 4 -  Number: PD_EXCLUSIONFLAGS              = 0x01000000
.head 4 -  Number: PD_USELARGETEMPLATE            = 0x10000000
.head 4 -  ! !
.head 4 -  Number: PD_EXCL_COPIESANDCOLLATE       = (DM_COPIES | DM_COLLATE)
.head 3 +  User
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 -  Variables
.head 2 +  Internal Functions
.head 3 +  Function: fPrintScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalCreateWindow( frmCapture, hWndForm)
.head 5 -  Call SalHideWindow( frmCapture )
.head 5 -  Set frmCapture.rbScreen = TRUE
.head 5 -  Set frmCapture.cbPrinter = TRUE
.head 5 -  Set frmCapture.rbPortrait = TRUE
.head 5 -  Call SalSendMsg( frmCapture.pbStart, SAM_Click, 0,0)
.head 5 -  ! Call SalSendMsg( frmMain.pbFile, SAM_Click, 0,0)
.head 3 +  Function: fSaveScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sFilePath
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalCreateWindow( frmCapture, hWndForm)
.head 5 -  Call SalHideWindow( frmCapture )
.head 5 -  Set frmCapture.rbScreen = TRUE
.head 5 -  Set frmCapture.cbFile = TRUE
.head 5 -  Set frmCapture.dfFile = sFilePath
.head 5 -  Set frmCapture.rbPortrait = TRUE
.head 5 -  Call SalSendMsg( frmCapture.pbStart, SAM_Click, 0,0)
.head 5 -  ! Call SalSendMsg( frmMain.pbFile, SAM_Click, 0,0)
.head 2 -  Named Menus
.head 2 -  Class Definitions
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 2 -  Application Actions
.head 1 +  Form Window: frmCapture
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Test Capture
.head 2 -  Icon File:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Automatically Created at Runtime? No
.head 3 -  Initial State: Normal
.head 3 -  Maximizable? No
.head 3 -  Minimizable? Yes
.head 3 -  Allow Child Docking? No
.head 3 -  Docking Orientation: All
.head 3 -  System Menu? Yes
.head 3 -  Resizable? No
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top: Default
.head 4 -  Width:  9.057"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 6.006"
.head 4 -  Height Editable? Yes
.head 3 -  Form Size
.head 4 -  Width:  Default
.head 4 -  Height: Default
.head 4 -  Number of Pages: Dynamic
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 2 -  Description:
.head 2 -  Named Menus
.head 2 -  Menu
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Pushbutton: pbStart
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Start Capture
.head 4 -  Window Location and Size
.head 5 -  Left: 0.557"
.head 5 -  Top: 0.26"
.head 5 -  Width:  2.071"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If cbFile and SalIsNull(dfFile)
.head 7 -  Call SalMessageBox( "File name is empty !", "Error", MB_IconStop )
.head 6 +  Else
.head 7 +  If rbWindow
.head 8 -  Set hWndCaptureOld = hWndNULL
.head 8 -  Set hWndCapture = hWndNULL
.head 8 -  Call mouse_event( MOUSEEVENTF_LEFTDOWN, 0, 0 ,0,-1 )
.head 8 -  Call SetCapture( hWndForm )
.head 8 -  Call ShowWindow(hWndForm, SW_SHOWMINIMIZED)
.head 8 -  Call SalTimerSet( hWndForm, 1, 500 )
.head 7 +  Else If rbScreen
.head 8 -  Call ShowWindow(hWndForm, SW_SHOWMINIMIZED)
.head 8 -  Call SalTimerSet( hWndForm, 1, 500 )
.head 3 -  !
.head 3 +  Radio Button: rbWindow
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Window
.head 4 -  Window Location and Size
.head 5 -  Left: 3.343"
.head 5 -  Top: 0.292"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Radio Button: rbScreen
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Screen
.head 4 -  Window Location and Size
.head 5 -  Left: 5.171"
.head 5 -  Top: 0.292"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 -  !
.head 3 +  Check Box: cbClipboard
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Clipboard
.head 4 -  Window Location and Size
.head 5 -  Left: 0.633"
.head 5 -  Top: 0.81"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Check Box: cbFile
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: File
.head 4 -  Window Location and Size
.head 5 -  Left: 2.8"
.head 5 -  Top: 0.81"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If MyValue
.head 7 -  Call SalShowWindow( dfFile )
.head 7 -  Call SalShowWindow( pbFile )
.head 6 +  Else
.head 7 -  Call SalHideWindow( dfFile )
.head 7 -  Call SalHideWindow( pbFile )
.head 3 +  Data Field: dfFile
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.2"
.head 6 -  Top: 0.798"
.head 6 -  Width:  3.686"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SalHideWindow( hWndItem )
.head 3 +  Pushbutton: pbFile
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: ...
.head 4 -  Window Location and Size
.head 5 -  Left: 7.9"
.head 5 -  Top: 0.798"
.head 5 -  Width:  0.329"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SalHideWindow( hWndItem )
.head 5 +  On SAM_Click
.head 6 +  If DlgFile( hWndForm, 2, sFileName, sPathFileName, '6', sFileName, sPathFileName, nIndice )
.head 7 -  Set dfFile = sPathFileName
.head 3 +  Check Box: cbPrinter
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Printer
.head 4 -  Window Location and Size
.head 5 -  Left: 0.633"
.head 5 -  Top: 1.214"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If MyValue
.head 7 -  Call SalShowWindow( rbLandscape )
.head 7 -  Call SalShowWindow( rbPortrait )
.head 6 +  Else
.head 7 -  Call SalHideWindow( rbLandscape )
.head 7 -  Call SalHideWindow( rbPortrait )
.head 3 -  !
.head 3 +  Custom Control: ccBitmap
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  DLL Name: USER32.DLL
.head 5 -  MS Windows Class Name: Static
.head 5 -  Style:  0x0000120E
.head 5 -  ExStyle:  0x00000000
.head 5 -  Title:
.head 5 -  Window Location and Size
.head 6 -  Left: 0.614"
.head 6 -  Top: 1.625"
.head 6 -  Width:  7.586"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 4.104"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Etched Border? No
.head 5 -  Hollow? No
.head 5 -  Vertical Scroll? No
.head 5 -  Horizontal Scroll? No
.head 5 -  Tab Stop? None
.head 5 -  Tile To Parent? No
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  DLL Settings
.head 4 -  Message Actions
.head 3 +  Radio Button: rbLandscape
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Landscape
.head 4 -  Window Location and Size
.head 5 -  Left: 1.933"
.head 5 -  Top: 1.202"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SalHideWindow( hWndItem )
.head 3 +  Radio Button: rbPortrait
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Portrait
.head 4 -  Window Location and Size
.head 5 -  Left: 3.55"
.head 5 -  Top: 1.202"
.head 5 -  Width:  1.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call SalHideWindow( hWndItem )
.head 2 +  Functions
.head 3 +  Function: OutLine
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Window Handle: hWndOutline
.head 5 -  Number: nColorOutline
.head 5 -  Number: nStyleOutline
.head 5 -  Number: nSizeOutline
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: hDCOutline
.head 5 -  Number: hPenOutline
.head 5 -  Number: hPenOldOutline
.head 5 -  Number: nLeftOutline
.head 5 -  Number: nTopOutline
.head 5 -  Number: nRightOutline
.head 5 -  Number: nBottomOutline
.head 4 +  Actions
.head 5 -  Set hDCOutline = GetWindowDC(hWndOutline)
.head 5 -  Call SetROP2( hDCOutline, R2_NOTXORPEN )
.head 5 -  Set hPenOutline =CreatePen( nStyleOutline, nSizeOutline, nColorOutline )
.head 5 -  Set hPenOldOutline = SelectObject( hDCOutline, hPenOutline )
.head 5 -  Call GetWindowRect( hWndOutline, nLeftOutline, nTopOutline, nRightOutline, nBottomOutline )
.head 5 -  Call Rectangle( hDCOutline, 0, 0, nRightOutline - nLeftOutline, nBottomOutline - nTopOutline )
.head 5 -  Call SelectObject( hDCOutline, hPenOldOutline )
.head 5 -  Call ReleaseDC( hWndOutline, hDCOutline )
.head 5 -  Call DeleteObject( hPenOutline )
.head 3 +  Function: MouseMove
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If GetCapture() = hWndForm and bCapture  
.head 6 -  Call GetCursorPos( nX, nY )
.head 6 -  Set hWndCapture =  WindowFromPoint(nX,nY  )
.head 6 +  If hWndCapture != hWndCaptureOld
.head 7 -  Call OutLine( hWndCaptureOld,  SalColorFromRGB( 255, 0, 0 ), PS_SOLID, 4 )
.head 7 -  Call OutLine( hWndCapture,  SalColorFromRGB( 255, 0, 0 ), PS_SOLID, 4 )
.head 7 -  Set hWndCaptureOld =  hWndCapture
.head 3 +  Function: StopCapture
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If GetCapture() = hWndForm  
.head 6 -  Call mouse_event( MOUSEEVENTF_LEFTUP, 0, 0 ,0,0 )
.head 6 -  Call SalTimerKill( hWndForm, 1 )
.head 6 +  If bCapture and hWndCapture
.head 7 -  Call OutLine( hWndCapture,  SalColorFromRGB( 255, 0, 0 ), PS_SOLID, 4 )
.head 6 -  Call SalInvalidateWindow( hWndCapture )
.head 6 -  Call UpdateWindow( hWndCapture )
.head 6 -  Call ReleaseCapture(  )
.head 6 -  Set bCapture = FALSE
.head 6 -  Call DoAction()
.head 5 -  Set bCapture = FALSE
.head 3 +  Function: InitCapture
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SetWindowTextA( pbStart, "Left Button to stop" )
.head 5 +  If GetCapture() = hWndForm
.head 6 -  Set bCapture = TRUE
.head 3 +  Function: CopyToStatic
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Window Handle: hWndStatic
.head 5 -  Number: hBitmapToCopy
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nLeft
.head 5 -  Number: nTop
.head 5 -  Number: nRight
.head 5 -  Number: nBottom
.head 5 -  Number: nImageWidth
.head 5 -  Number: nImageHeight
.head 5 -  Number: nXFactor
.head 5 -  Number: nYFactor
.head 4 +  Actions
.head 5 -  Call GetWindowRect( hWndStatic, nLeft, nTop, nRight, nBottom )
.head 5 -  Call GetBitmapSize( hBitmapToCopy, nImageWidth, nImageHeight )
.head 5 -  Set nXFactor = nImageWidth /  (nRight - nLeft)
.head 5 -  Set nYFactor = nImageHeight /   (nBottom - nTop)
.head 5 +  If nXFactor > 1 or  nYFactor > 1
.head 6 -  Set nImageWidth = nImageWidth / SalNumberMax( nXFactor, nYFactor )
.head 6 -  Set nImageHeight = nImageHeight / SalNumberMax( nXFactor, nYFactor )
.head 6 -  ! Set hBitmapCopy = CopyBitmap( hBitmapToCopy, IMAGE_BITMAP, nImageWidth, nImageHeight )
.head 6 -  Set hBitmapStatic = CopyImage( hBitmapToCopy, IMAGE_BITMAP, nImageWidth, nImageHeight, 0 )
.head 5 +  Else
.head 6 -  Set hBitmapStatic = hBitmapToCopy
.head 5 -  Call SalSendMsg( hWndStatic, STM_SETIMAGE, IMAGE_BITMAP, hBitmapStatic )
.head 5 -  Return hBitmapStatic
.head 3 +  Function: DoAction
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  ! If cbClipboard
.head 6 -  Call OpenClipboard( hWndForm )
.head 6 -  Call EmptyClipboard(  )
.head 6 -  Call CloseClipboard(  )
.head 5 +  If hBitmap
.head 6 -  Call DeleteObject( hBitmap )
.head 5 +  If hBitmapStatic
.head 6 -  Call DeleteObject(hBitmapStatic)
.head 5 -  Set hBitmap =GetBitmap( hWndCapture )
.head 5 +  If cbClipboard
.head 6 -  Call OpenClipboard( hWndNULL )
.head 6 -  Call EmptyClipboard(  )
.head 6 -  Call SetClipboardData( CF_BITMAP, hBitmap )
.head 6 -  Call CloseClipboard(  )
.head 5 +  If cbFile
.head 6 -  Call SaveBitmap(hBitmap, dfFile)
.head 5 +  If cbPrinter
.head 6 -  Call Print()
.head 5 -  Set hBitmapStatic = CopyToStatic(ccBitmap, hBitmap)
.head 5 -  Call SetWindowTextA( pbStart, "Start capture" )
.head 5 -  Call ShowWindow(hWndForm, SW_RESTORE)
.head 3 +  Function: Print
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sBuffer
.head 5 -  Number: nAddr
.head 5 -  Number: nSize
.head 5 -  Number: hDC
.head 5 -  Boolean: bReturn
.head 5 -  Number: nWidth
.head 5 -  Number: nHeight
.head 5 -  String: sPrintDevice
.head 5 -  String: sPrintDriver
.head 5 -  String: sPrintPort
.head 5 -  Number: nImageWidth
.head 5 -  Number: nImageHeight
.head 5 -  Number: nFactor
.head 5 -  Number: nHorz
.head 5 -  Number: nVert
.head 5 -  Number: nOrientation
.head 4 +  Actions
.head 5 -  Call SalPrtGetDefault( sPrintDevice, sPrintDriver, sPrintPort )
.head 5 -  Set hDC = CreateDCA( sPrintDriver, sPrintDevice, STRING_Null, STRING_Null )
.head 5 -  Set nWidth = GetDeviceCaps(hDC, HORZRES)
.head 5 -  Set nHeight = GetDeviceCaps(hDC, VERTRES)
.head 5 -  Call GetBitmapSize( hBitmap, nImageWidth, nImageHeight )
.head 5 +  If rbLandscape
.head 6 -  Set nHorz = nHeight
.head 6 -  Set nVert = nWidth
.head 6 -  Set nOrientation = DMORIENT_LANDSCAPE
.head 5 +  Else
.head 6 -  Set nHorz = nWidth
.head 6 -  Set nVert = nHeight
.head 6 -  Set nOrientation = DMORIENT_PORTRAIT
.head 5 -  Set nFactor = nHorz/nImageWidth
.head 5 +  If nImageHeight *nFactor > nVert
.head 6 -  Set nHeight = nVert
.head 6 -  Set nWidth = nImageWidth * (nVert/nImageHeight)
.head 5 +  Else
.head 6 -  Set nWidth = nHorz
.head 6 -  Set nHeight = nImageHeight *nFactor
.head 5 -  Call PrintBitmap( hBitmap, nWidth, nHeight, nOrientation )
.head 5 -  Call DeleteDC( hDC )
.head 3 +  Function: DlgFile
.head 4 -  Description: Dlg d'ouverture d'un fichier
Paramètres :	- hWndOwner
		- Type de traitement : 1 = Ouvrir, 2 = Sauvegarder
		- Nom du fichier à ouvrir
		- Répertoire
		- Chaine contenant les filtres (0= All, 1 = TAB, 2= ';', 3= ' ', 4=',', 5=Largeur fixe, 6=BMP, 7=GIF, 8=JPEG, 9=PCX, 10=TIF, 11=WMF, 12=EMF, 13=ICO)
Return		- Nom
		- Nom complet
		- nFilter reçoit l'indice du filtre choisi
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Window Handle: hWndOwner
.head 5 -  Number: nTypeTraitement
.head 5 -  String: sFile
.head 5 -  String: sPath
.head 5 -  String: sStrFilters
.head 5 -  Receive String: sChosenFile
.head 5 -  Receive String: sChosenPathAndFile
.head 5 -  Receive Number: nFilter
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sFileFilters[*]
.head 5 -  String: sTabFilters[*]
.head 5 -  Number: nNumFilters
.head 5 -  Number: nWhichFilter
.head 5 -  ! String: sChosenFile
.head 5 -  String: sChosenPath
.head 5 -  String: sDirSauve
.head 5 -  String: sTypeTraitement
.head 5 -  Number: nCpt
.head 5 -  Number: nPos
.head 4 +  Actions
.head 5 -  Call SalFileGetCurrentDirectory (sDirSauve)
.head 5 -  Set sChosenFile =  sFile
.head 5 -  Set sChosenPath = sPath
.head 5 -  Call SalFileSetDrive (SalStrLeftX (sPath, 1))
.head 5 -  Call SalFileSetCurrentDirectory (sPath)
.head 5 -  Call SalStrTokenize( sStrFilters, '', ',', sTabFilters )
.head 5 -  Call SalArrayGetUpperBound( sTabFilters, 1, nNumFilters )
.head 5 -  Set nNumFilters = nNumFilters + 1
.head 5 -  Set nCpt = 0
.head 5 +  While nCpt < nNumFilters
.head 6 +  If sTabFilters[nCpt] = '0'
.head 7 -  Set sFileFilters[nCpt * 2] = 'All Files'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.*'
.head 6 +  If sTabFilters[nCpt] = '1'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Text Files (Sep. [TAB])'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.txt'
.head 6 +  Else If sTabFilters[nCpt] = '2'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Text Files (Sep. [;])'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.xls'
.head 6 +  Else If sTabFilters[nCpt] = '3'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Text Files (Sep. [SPACE])'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.txt'
.head 6 +  Else If sTabFilters[nCpt] = '4'
.head 7 -  Set sFileFilters[nCpt * 2] = 'CSV Files (Sep. [,])'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.csv'
.head 6 +  Else If sTabFilters[nCpt] = '5'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Text Files (Fixed width)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.*'
.head 6 +  Else If sTabFilters[nCpt] = '6'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Bitmap (*.bmp)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.bmp'
.head 6 +  Else If sTabFilters[nCpt] = '7'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Graphics Interchange Format (*.gif)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.gif'
.head 6 +  Else If sTabFilters[nCpt] = '8'
.head 7 -  Set sFileFilters[nCpt * 2] =  'JPEG (*.jpg)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] =  '*.jpg'
.head 6 +  Else If sTabFilters[nCpt] = '9'
.head 7 -  Set sFileFilters[nCpt * 2] = 'Paintbrush (*.pcx)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.pcx'
.head 6 +  Else If sTabFilters[nCpt] = '10'
.head 7 -  Set sFileFilters[nCpt * 2] =  'Tagged Image File Format (*.tif)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.tif'
.head 6 +  Else If sTabFilters[nCpt] = '11'
.head 7 -  Set sFileFilters[nCpt * 2] =  'Windows Meta File (*.wmf)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] = '*.wmf'
.head 6 +  Else If sTabFilters[nCpt] = '12'
.head 7 -  Set sFileFilters[nCpt * 2] =  'Windows Enhanced Meta File (*.emf)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] =  '*.emf'
.head 6 +  Else If sTabFilters[nCpt] = '13'
.head 7 -  Set sFileFilters[nCpt * 2] =  'Icon File (*.ico)'
.head 7 -  Set sFileFilters[nCpt * 2 + 1] =  '*.ico'
.head 6 -  Set nCpt = nCpt + 1
.head 5 -  Call SalArrayGetUpperBound( sFileFilters, 1, nNumFilters )
.head 5 -  Set nNumFilters = nNumFilters + 1
.head 5 +  If nTypeTraitement = 1
.head 6 +  If sChosenPath != ''
.head 7 -  Set nPos = VisStrScanReverse( sChosenPath, -1, "\\" )
.head 7 +  If nPos > -1
.head 8 -  Set sChosenPath = SalStrLeftX( sChosenPath, nPos )
.head 6 +  If SalDlgOpenFile( hWndOwner, 'Open', sFileFilters, nNumFilters, nWhichFilter, sChosenFile, sChosenPath )
.head 7 -  Set nFilter = nWhichFilter
.head 7 -  Set sChosenFile = sChosenFile
.head 7 -  Set sChosenPathAndFile = sChosenPath
.head 7 -  Call SalFileSetDrive (SalStrLeftX (sDirSauve, 1))
.head 7 -  Call SalFileSetCurrentDirectory (sDirSauve)
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SalFileSetDrive (SalStrLeftX (sDirSauve, 1))
.head 7 -  Call SalFileSetCurrentDirectory (sDirSauve)
.head 7 -  Return FALSE
.head 5 +  Else If nTypeTraitement = 2
.head 6 +  If SalDlgSaveFile( hWndForm, 'Save as', sFileFilters, nNumFilters, nWhichFilter, sChosenFile, sChosenPath )
.head 7 -  Set nFilter = nWhichFilter
.head 7 -  Set sChosenPathAndFile = sChosenPath
.head 7 -  Call SalFileSetDrive (SalStrLeftX (sDirSauve, 1))
.head 7 -  Call SalFileSetCurrentDirectory (sDirSauve)
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SalFileSetDrive (SalStrLeftX (sDirSauve, 1))
.head 7 -  Call SalFileSetCurrentDirectory (sDirSauve)
.head 7 -  Return FALSE
.head 5 -  !
.head 3 +  Function: GetBitmap
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Window Handle: hWndBitmap
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: hDC
.head 5 -  Number: hDCMem
.head 5 -  Number: hBitmapReturn
.head 5 -  Number: hBitmapOld
.head 5 -  Number: nLeft
.head 5 -  Number: nTop
.head 5 -  Number: nRight
.head 5 -  Number: nBottom
.head 4 +  Actions
.head 5 -  Call GetWindowRect( hWndBitmap, nLeft, nTop, nRight, nBottom )
.head 5 -  Set hDC = GetWindowDC(hWndBitmap)
.head 5 -  Set hDCMem=CreateCompatibleDC(hDC)
.head 5 -  Set hBitmapReturn = CreateCompatibleBitmap(hDC, nRight - nLeft, nBottom - nTop)
.head 5 -  Set hBitmapOld = SelectObject(hDCMem, hBitmapReturn)
.head 5 -  Call BitBlt( hDCMem, 0, 0 , nRight - nLeft,  nBottom - nTop , hDC, 0, 0,  SRCCOPY )
.head 5 -  Call SelectObject(hDCMem, hBitmapOld)
.head 5 -  Call DeleteDC( hDCMem )
.head 5 -  Call ReleaseDC( hWndBitmap, hDC )
.head 5 -  Return hBitmapReturn
.head 3 +  Function: GetPrinterDC
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number: hDC
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sBuffer
.head 5 -  Number: nSize
.head 4 +  Actions
.head 5 -  Set nSize = 66
.head 5 -  ! Call RtlZeroMemory(sBuffer, nSize)
.head 5 -  Call SalStrSetBufferLength( sBuffer, nSize )
.head 5 -  Call CStructPutLong( sBuffer, 0 , nSize )
.head 5 -  Call CStructPutLong( sBuffer, 20 , PD_RETURNDEFAULT | PD_RETURNDC )
.head 5 -  Call PrintDlgA( sBuffer )
.head 5 -  Return CStructGetLong( sBuffer, 16 ) 
.head 3 +  ! Function: CopyBitmap
.head 4 -  Description: 
.head 4 -  Returns 
.head 4 +  Parameters 
.head 5 -  Number: hBitmapToCopy
.head 5 -  Number: nType
.head 5 -  Number: nBitmapWidth
.head 5 -  Number: nBitmapHeight
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  Number: nHeightImage
.head 5 -  Number: nWidthImage
.head 5 -  Number: hBitmapReturn
.head 5 -  Number: hBitmapOld
.head 5 -  Number: hBitmapOld2
.head 5 -  Number: hDC
.head 5 -  Number: hDCMem
.head 5 -  Number: hDCMem2
.head 4 +  Actions 
.head 5 -  Set hDC = GetDC(hWndNULL)
.head 5 -  Set hDCMem=CreateCompatibleDC(hDC)
.head 5 -  Set hDCMem2=CreateCompatibleDC(hDC)
.head 5 +  If nType = IMAGE_BITMAP
.head 6 -  Call GetBitmapSize( hBitmapToCopy, nWidthImage, nHeightImage )
.head 5 +  Else If nType = IMAGE_ICON
.head 6 -  Call GetIconSize( hBitmapToCopy, nWidthImage, nHeightImage )
.head 5 +  If nBitmapWidth != 0 and nBitmapHeight != 0
.head 6 -  Set hBitmapReturn = CreateCompatibleBitmap(hDC,nBitmapWidth,nBitmapHeight)
.head 5 +  Else 
.head 6 -  Set hBitmapReturn = CreateCompatibleBitmap(hDC,nWidthImage,nHeightImage)
.head 5 -  Set hBitmapOld = SelectObject(hDCMem, hBitmapReturn)
.head 5 -  ! If nType = IMAGE_BITMAP
.head 5 -  Set hBitmapOld2 = SelectObject(hDCMem2, hBitmapToCopy)
.head 5 -  Call SetStretchBltMode( hDCMem, COLORONCOLOR )
.head 5 +  If nBitmapWidth != 0 and nBitmapHeight != 0
.head 6 -  Call PatBlt( hDCMem, 0, 0, nBitmapWidth, nBitmapHeight, WHITENESS )
.head 6 +  If nType = IMAGE_BITMAP
.head 7 -  Call StretchBlt( hDCMem, 0, 0 , nBitmapWidth, nBitmapHeight , hDCMem2, 0, 0, nWidthImage, nHeightImage, SRCCOPY )
.head 6 +  Else If nType = IMAGE_ICON
.head 7 -  Call DrawIconEx( hDCMem, 0, 0, hBitmapToCopy, nBitmapWidth, nBitmapHeight,  0, 0, DI_NORMAL )
.head 6 +  Else If nType =IMAGE_ENHMETAFILE
.head 7 -  Call PlayEnhMetaFile(hDCMem, hBitmapToCopy, 0, 0,nBitmapWidth, nBitmapHeight)
.head 5 +  Else 
.head 6 -  Call PatBlt( hDCMem, 0, 0, nWidthImage, nHeightImage, WHITENESS )
.head 6 +  If nType = IMAGE_BITMAP
.head 7 -  Call StretchBlt( hDCMem, 0, 0 , nWidthImage, nHeightImage, hDCMem2, 0, 0, nWidthImage, nHeightImage, SRCCOPY )
.head 6 +  Else If nType = IMAGE_ICON
.head 7 -  Call DrawIconEx( hDCMem, 0, 0, hBitmapToCopy, nWidthImage, nHeightImage,  0, 0, DI_NORMAL )
.head 6 +  Else If nType =IMAGE_ENHMETAFILE
.head 7 -  Call PlayEnhMetaFile(hDCMem, hBitmapToCopy, 0, 0,nBitmapWidth, nBitmapHeight)
.head 5 -  Call SelectObject(hDCMem,hBitmapOld)
.head 5 -  Call DeleteDC( hDCMem )
.head 5 -  Call SelectObject(hDCMem2,hBitmapOld2)
.head 5 -  Call DeleteDC( hDCMem2 )
.head 5 +  If nType = IMAGE_BITMAP
.head 6 -  Call DeleteObject( hBitmapToCopy )
.head 5 +  Else If nType = IMAGE_ICON
.head 6 -  Call DestroyIcon(SalNumberToWindowHandle( hBitmapToCopy )  )
.head 5 +  Else If nType = IMAGE_ENHMETAFILE
.head 6 -  Call DeleteEnhMetaFile (hBitmapToCopy)
.head 5 -  Call ReleaseDC( hWndNULL, hDC )
.head 5 -  Return hBitmapReturn
.head 3 +  ! Function: GetIconSize
.head 4 -  Description: 
.head 4 -  Returns 
.head 4 +  Parameters 
.head 5 -  Number: hICO
.head 5 -  Receive Number: nWidth
.head 5 -  Receive Number: nHeight
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  Boolean: bIcon
.head 5 -  Number: nXHotspot
.head 5 -  Number: nYHotspot
.head 5 -  Number: hBitmapMask
.head 5 -  Number: hBitmapColor
.head 4 +  Actions 
.head 5 -  Call GetIconInfo( hICO, bIcon, nXHotspot, nYHotspot, hBitmapMask, hBitmapColor )
.head 5 -  Call GetBitmapSize( hBitmapColor, nWidth, nHeight )
.head 5 -  Return TRUE
.head 3 +  Function: Create24BPPDIBSection
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Number: hDC
.head 5 -  Number: nWidth
.head 5 -  Number: nHeight
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sBuffer
.head 5 -  String: pBitsBuffer
.head 5 -  Number: hDibSection
.head 4 +  Actions
.head 5 -  Call SalStrSetBufferLength( sBuffer, 44 )
.head 5 -  Call CStructPutLong( sBuffer, 0, 40 )
.head 5 -  Call CStructPutLong( sBuffer, 4, nWidth )
.head 5 -  Call CStructPutLong( sBuffer, 8, nHeight )
.head 5 -  Call CStructPutWord( sBuffer, 12, 1 )
.head 5 -  Call CStructPutWord( sBuffer, 14, 24 )
.head 5 -  Call CStructPutLong( sBuffer, 16, BI_RGB )
.head 5 -  Call CStructPutLong( sBuffer, 20, 0)
.head 5 -  Call CStructPutLong( sBuffer, 24, 0 )
.head 5 -  Call CStructPutLong( sBuffer, 28, 0 )
.head 5 -  Call CStructPutLong( sBuffer, 32, 0 )
.head 5 -  Call CStructPutLong( sBuffer, 36, 0 )
.head 5 -  Call SalStrSetBufferLength( pBitsBuffer, 255 )
.head 5 -  Set hDibSection = CreateDIBSection(hDC, sBuffer, DIB_RGB_COLORS, pBitsBuffer, 0,  0 )
.head 5 -  Return hDibSection
.head 3 +  Function: GetBitmapSize
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Number: hBMP
.head 5 -  Receive Number: nWidth
.head 5 -  Receive Number: nHeight
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nImageInfo
.head 5 -  Number: nFlags
.head 5 -  String: sBitmapBuffer
.head 4 +  Actions
.head 5 -  Call SalStrSetBufferLength( sBitmapBuffer, 24 )
.head 5 -  Call GetObjectA( hBMP, 24, sBitmapBuffer )
.head 5 -  Set nWidth =  CStructGetInt( sBitmapBuffer, 4 )
.head 5 -  Set nHeight =  CStructGetInt( sBitmapBuffer, 8 )
.head 5 -  Return TRUE
.head 3 +  Function: PrintBitmap
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Number: hBitmap
.head 5 -  Number: nWidth
.head 5 -  Number: nHeight
.head 5 -  Number: nOrientation
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nBitmapWidth
.head 5 -  Number: nBitmapHeight
.head 5 -  Number: hPrinterDC
.head 5 -  Number: hScreenDC
.head 5 -  String: sBuffer
.head 5 -  Number: hDevMode
.head 5 -  Number: nBufferLength
.head 5 -  Number: nOrientationOffset
.head 5 -  Number: pDevMode
.head 5 -  Number: nAddr
.head 5 -  Number: hDib
.head 5 -  Number: hMemDC
.head 5 -  Number: hMemDC2
.head 5 -  Number: nDibWidth
.head 5 -  Number: nDibHeight
.head 5 -  Number: hBitmapOld
.head 5 -  String: sBitmap
.head 5 -  String: sBitmapInfo
.head 4 +  Actions
.head 5 -  Call GetBitmapSize( hBitmap, nBitmapWidth, nBitmapHeight )
.head 5 -  Set nBufferLength = 66
.head 5 -  Call SalStrSetBufferLength( sBuffer, nBufferLength )
.head 5 -  Call CStructPutLong( sBuffer, 0 , nBufferLength )
.head 5 -  Call CStructPutLong( sBuffer, 20 , PD_RETURNDEFAULT | PD_RETURNDC )
.head 5 -  Call PrintDlgA( sBuffer )
.head 5 -  Set hPrinterDC = CStructGetLong( sBuffer, 16 ) 
.head 5 -  Set hDevMode = CStructGetLong( sBuffer, 8 ) 
.head 5 -  Set pDevMode = GlobalLock(hDevMode)
.head 5 -  Set nBufferLength=1024
.head 5 -  Call SalStrSetBufferLength(sBuffer, nBufferLength)
.head 5 -  Set nAddr = CStructAllocFarMem( 156 )
.head 5 -  Call CStructCopyFromFarMem( pDevMode, sBuffer, 156 )
.head 5 -  Set nOrientationOffset = 32 + 2*4 + 4
.head 5 -  Call CStructPutWord( sBuffer, nOrientationOffset, nOrientation )
.head 5 +  If not ResetDCA( hPrinterDC, sBuffer )
.head 6 -  Call SalMessageBox("ResetDC Error", " Error", MB_Ok| MB_IconStop)
.head 6 -  Return FALSE
.head 5 -  Call CStructFreeFarMem( nAddr )
.head 5 -  Call GlobalUnlock( hDevMode )
.head 5 -  Set hScreenDC  = GetDC(hWndNULL)
.head 5 -  Set hMemDC = CreateCompatibleDC(hScreenDC)
.head 5 -  Set hBitmapOld = SelectObject(hMemDC, hBitmap)
.head 5 -  Set hDib = Create24BPPDIBSection(hMemDC, nBitmapWidth, nBitmapHeight)
.head 5 +  If (not hDib)
.head 6 -  Call DeleteDC(hPrinterDC)
.head 6 -  Call ReleaseDC(hWndNULL, hScreenDC)
.head 6 -  Call SelectObject(hMemDC, hBitmapOld)
.head 6 -  Call DeleteDC(hMemDC)
.head 6 -  Call SalMessageBox( "Error Creating DIB Section", "Error", MB_Ok| MB_IconStop)
.head 5 -  Set hMemDC2 = CreateCompatibleDC(hScreenDC)
.head 5 -  Call SelectObject(hMemDC2, hDib)
.head 5 -  Call BitBlt(hMemDC2, 0, 0, nBitmapWidth, nBitmapHeight, hMemDC, 0, 0, SRCCOPY)
.head 5 +  If StartDocA(hPrinterDC, 20, "Print", STRING_Null, STRING_Null, 0)
.head 6 +  If StartPage(hPrinterDC)
.head 7 -  Call SalStrSetBufferLength( sBuffer, 84 )
.head 7 -  Call GetObjectA( hDib, 84, sBuffer )
.head 7 -  Set nDibWidth =  CStructGetInt( sBuffer, 4 )
.head 7 -  Set nDibHeight =  CStructGetInt( sBuffer, 8 )
.head 7 -  Call SalStrSetBufferLength( sBitmapInfo, 44 )
.head 7 -  Call CStructPutLong( sBitmapInfo, 0, CStructGetLong( sBuffer, 24) )
.head 7 -  Call CStructPutLong( sBitmapInfo, 4, CStructGetLong( sBuffer, 28) )
.head 7 -  Call CStructPutLong( sBitmapInfo, 8, CStructGetLong( sBuffer, 32)  )
.head 7 -  Call CStructPutWord( sBitmapInfo, 12, CStructGetLong( sBuffer, 36) )
.head 7 -  Call CStructPutWord( sBitmapInfo, 14,  CStructGetLong( sBuffer, 38)  )
.head 7 -  Call CStructPutLong( sBitmapInfo, 16, CStructGetLong( sBuffer, 40) )
.head 7 -  Call CStructPutLong( sBitmapInfo, 20, 0)
.head 7 -  Call CStructPutLong( sBitmapInfo, 24, 0 )
.head 7 -  Call CStructPutLong( sBitmapInfo, 28, 0 )
.head 7 -  Call CStructPutLong( sBitmapInfo, 32, 0 )
.head 7 -  Call CStructPutLong( sBitmapInfo, 36, 0 )
.head 7 -  Call StretchDIBits( hPrinterDC, 0, 0, nWidth, nHeight, 0, 0, nDibWidth, nDibHeight, CStructGetLong( sBuffer, 20), sBitmapInfo, DIB_RGB_COLORS, SRCCOPY )
.head 7 -  Call EndPage(hPrinterDC)
.head 6 -  Call EndDoc(hPrinterDC)
.head 5 -  Call DeleteDC(hPrinterDC)
.head 5 -  Call DeleteDC(hMemDC2)
.head 5 -  Call ReleaseDC(hWndNULL, hScreenDC)
.head 5 -  Call SelectObject(hMemDC, hBitmapOld)
.head 5 -  Call DeleteDC(hMemDC)
.head 5 -  Call DeleteObject(hDib)
.head 3 +  Function: CreateBitmapInfoStruct
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  Number: hBitmap
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sBuffer
.head 5 -  Number: nColorBits
.head 5 -  Number: nAddr
.head 5 -  Number: nSize
.head 5 -  String: sBitmapInfo
.head 4 +  Actions
.head 5 -  Call SalStrSetBufferLength( sBuffer, 24 )
.head 5 -  Call GetObjectA( hBitmap, 24, sBuffer )
.head 5 -  Set nColorBits = CStructGetWord( sBuffer, 16) * CStructGetWord( sBuffer, 18)
.head 5 +  If nColorBits = 1
.head 6 -  Set nColorBits = 1
.head 5 +  Else If nColorBits <= 4
.head 6 -  Set nColorBits = 4
.head 5 +  Else If nColorBits <= 8
.head 6 -  Set nColorBits = 8
.head 5 +  Else If nColorBits <= 16
.head 6 -  Set nColorBits = 16
.head 5 +  Else If nColorBits <= 24
.head 6 -  Set nColorBits = 24
.head 5 +  Else
.head 6 -  Set nColorBits = 32
.head 5 +  If nColorBits != 24
.head 6 -  Set nSize = 40 + 4 * SalNumberPower(2, nColorBits)
.head 5 +  Else
.head 6 -  Set nSize = 40
.head 5 -  ! Set nAddr = CStructAllocFarMem( nSize)
.head 5 -  Call SalStrSetBufferLength( sBitmapInfo, nSize )
.head 5 -  ! Call CStructCopyFromFarMem( nAddr, sBuffer, 0 )
.head 5 -  Call CStructPutLong( sBitmapInfo, 0, 40 )
.head 5 -  Call CStructPutLong( sBitmapInfo, 4, CStructGetLong( sBuffer, 4 ) )
.head 5 -  Call CStructPutLong( sBitmapInfo, 8, CStructGetLong( sBuffer, 8 ) )
.head 5 -  Call CStructPutWord( sBitmapInfo, 12, CStructGetWord( sBuffer, 16) )
.head 5 -  Call CStructPutWord( sBitmapInfo, 14,  CStructGetWord( sBuffer, 18) )
.head 5 +  If nColorBits < 24
.head 6 -  Call CStructPutLong( sBitmapInfo, 32,  SalNumberPower(2, nColorBits) )
.head 5 -  Call CStructPutLong( sBitmapInfo, 16, BI_RGB )
.head 5 -  Call CStructPutLong( sBitmapInfo, 20, ((CStructGetLong( sBuffer, 4 ) * nColorBits + 31) & (-32)) / 8 * CStructGetLong( sBuffer, 8 ) )
.head 5 -  Call CStructPutLong( sBitmapInfo, 36,  0 )
.head 5 -  ! Call CStructCopyToFarMem( nAddr, sBuffer, 40 )
.head 5 -  Return sBitmapInfo
.head 3 +  Function: SaveBitmap
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Number: hBitmap
.head 5 -  String: sFile
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Return CreateBMPFile(sFile, CreateBitmapInfoStruct(hBitmap), hBitmap)
.head 3 +  Function: CreateBMPFile
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sFile
.head 5 -  String: pBitmapInfo
.head 5 -  Number: hBitmap
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: lpBits
.head 5 -  Number: hScreenDC
.head 5 -  Number: hFile
.head 5 -  String: sBitmapFileHeader
.head 5 -  Number: nNbBytesWritten
.head 5 -  Number: nPaletteSize
.head 5 -  Number: nDataSize
.head 4 +  Actions
.head 5 -  Call SalStrSetBufferLength( lpBits,  CStructGetLong( pBitmapInfo, 20 )  )
.head 5 -  Set hScreenDC = GetDC(hWndNULL)
.head 5 +  If not GetDIBits( hScreenDC, hBitmap, 0, CStructGetLong( pBitmapInfo, 8 ), lpBits, pBitmapInfo, DIB_RGB_COLORS )
.head 6 -  Call ReleaseDC(hWndNULL, hScreenDC)
.head 6 -  Call SalMessageBox( "Error in getting bitmap bits", "Error", MB_Ok|MB_IconStop)
.head 6 -  Return FALSE
.head 5 -  Call ReleaseDC(hWndNULL, hScreenDC)
.head 5 -  Set hFile = CreateFileA(sFile,  GENERIC_READ | GENERIC_WRITE, 0, 0,  CREATE_ALWAYS,  FILE_ATTRIBUTE_NORMAL, 0)
.head 5 +  If hFile =  INVALID_HANDLE_VALUE
.head 6 -  Call SalMessageBox( "Error in creating file", "Error", MB_Ok|MB_IconStop)
.head 6 -  Return FALSE
.head 5 -  Set nPaletteSize = GetPaletteSize(pBitmapInfo)
.head 5 -  Set nDataSize = GetDataSize(pBitmapInfo)
.head 5 -  Call SalStrSetBufferLength( sBitmapFileHeader,  14)
.head 5 -  Call CStructPutWord( sBitmapFileHeader, 0, 0x4d42 )
.head 5 -  Call CStructPutLong( sBitmapFileHeader, 2,  14 + CStructGetLong( pBitmapInfo, 0 ) +  nPaletteSize + nDataSize)
.head 5 -  Call CStructPutWord( sBitmapFileHeader, 6, 0 )
.head 5 -  Call CStructPutWord( sBitmapFileHeader, 8, 0 )
.head 5 -  Call CStructPutLong( sBitmapFileHeader, 10, 14 + CStructGetLong( pBitmapInfo, 0 ) + nPaletteSize )
.head 5 +  If not WriteFile( hFile, sBitmapFileHeader, 14, nNbBytesWritten, STRING_Null )
.head 6 -  Call SalMessageBox("Error in writing file (BITMAPFILEHEADER)", "Error", MB_Ok|MB_IconStop)
.head 6 -  Return FALSE
.head 5 +  If not WriteFile( hFile, pBitmapInfo,  CStructGetLong( pBitmapInfo, 0 ) + nPaletteSize, nNbBytesWritten, STRING_Null )
.head 6 -  Call SalMessageBox("Error in writing file (BITMAPINFOHEADER)", "Error", MB_Ok|MB_IconStop)
.head 6 -  Return FALSE
.head 5 +  If not WriteFile( hFile, lpBits, CStructGetLong( pBitmapInfo, 20 ), nNbBytesWritten, STRING_Null )
.head 6 -  Call SalMessageBox("Error in writing file", "Error", MB_Ok|MB_IconStop)
.head 6 -  Return FALSE
.head 5 -  Call CloseHandle( hFile )
.head 3 +  Function: GetPaletteSize
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: pBitmapInfoHeader
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nEntries
.head 5 -  Number: nSize
.head 4 +  Actions
.head 5 -  ! - 32 = biClrUsed
.head 5 +  If CStructGetLong( pBitmapInfoHeader, 32)
.head 6 -  Set nEntries = CStructGetLong( pBitmapInfoHeader, 32)
.head 5 -  ! - 14 = biBitCount
.head 5 +  Else If CStructGetWord( pBitmapInfoHeader, 14)  > 0 and  CStructGetWord( pBitmapInfoHeader, 14 ) <= 8
.head 6 -  Set nEntries =  SalNumberPower(2, CStructGetWord( pBitmapInfoHeader, 14 ) )
.head 5 +  Else
.head 6 -  Set nEntries = 0
.head 5 -  Set nSize = nEntries * 4
.head 5 -  ! 16 = biCompression
.head 5 +  If CStructGetWord( pBitmapInfoHeader, 16)  = BI_BITFIELDS
.head 6 -  Set nSize = nSize + 3 * 4
.head 5 -  Return nSize
.head 3 +  Function: GetDataSize
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: pBitmapInfoHeader
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nWidthBytes
.head 4 +  Actions
.head 5 -  ! 20 = biSizeImage
.head 5 +  If CStructGetLong( pBitmapInfoHeader, 20 )
.head 6 -  Return CStructGetLong( pBitmapInfoHeader, 20 )
.head 5 -  Set nWidthBytes = ((CStructGetLong( pBitmapInfoHeader, 4)  * CStructGetLong( pBitmapInfoHeader, 14)  + 7) / 8 + 3) & (-4)
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nX
.head 3 -  Number: nY
.head 3 -  Window Handle: hWndCapture
.head 3 -  Window Handle: hWndCaptureOld
.head 3 -  Boolean: bCapture
.head 3 -  Number: hBitmap
.head 3 -  Number: hBitmapStatic
.head 3 -  !
.head 3 -  String: sPathFileName
.head 3 -  String: sFileName
.head 3 -  Number: nIndice
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow(hWndForm)
.head 3 +  On SAM_Destroy
.head 4 +  If hBitmap
.head 5 -  Call DeleteObject( hBitmap )
.head 3 +  On SAM_Timer
.head 4 -  Call SalTimerKill( hWndForm, 1 )
.head 4 +  If rbWindow
.head 5 -  Call InitCapture(  )
.head 4 +  Else If rbScreen
.head 5 -  Set hWndCapture =  GetDesktopWindow(  )
.head 5 -  Call DoAction()
.head 4 -  Call SalPause( 2000 )
.head 4 -  Call SalDestroyWindow( frmCapture )
.head 3 +  On WM_MOUSEMOVE
.head 4 -  Call MouseMove()
.head 3 +  On WM_LBUTTONDOWN
.head 4 +  If GetMessageExtraInfo()  != -1
.head 5 -  Call StopCapture()
.head 3 +  On WM_LBUTTONDBLCLK
.head 4 -  Call StopCapture()
