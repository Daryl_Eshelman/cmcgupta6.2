.head 0 +  Application Description: Criminal Software Integrated Library
3-20-2001 - WRP
   * Includes 	-Login Screen
		-Registry checking( for Public/Non-Public Access)
		-Classes for formating of (Caseyr, caseno, casety, dates)
		-Retains last user to login
		-Selects User information from database
		-Selects Court information from database
		-8-2-2001-WRP-Added "/" ability to cDate class, allows the backslash to be entered to display todays date.
		-9-6-2001-WRP-Added Holiday Date Class to check for holiday and weekends
.head 1 -  Outline Version - 4.0.39
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 00000000000F0100 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE2FFFFFF2C0000 002C000000380400 0038030000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D0400000007436C 617373657305436F
0080: 7374730946756E63 74696F6E730E435F 496E736572745F43 6F737473
.enddata
.data DT_MAKERUNDLG
0000: 0900001003000000 0033433A5C43656E 747572615C437269 6D3332202D204F52
0020: 41434C455C437269 6D4E65772D4D6173 735C43724C6F6769 6E2E65786533433A
0040: 5C43656E74757261 5C4372696D333220 2D204F5241434C45 5C4372696D4E6577
0060: 2D4D6173735C4372 4C6F67696E2E646C 6C33433A5C43656E 747572615C437269
0080: 6D3332202D204F52 41434C455C437269 6D4E65772D4D6173 735C43724C6F6769
00A0: 6E2E617063000001 0101006400000033 433A5C43656E7475 72615C4372696D33
00C0: 32202D204F524143 4C455C4372696D4E 65772D4D6173735C 43724C6F67696E2E
00E0: 72756E33433A5C43 656E747572615C43 72696D3332202D20 4F5241434C455C43
0100: 72696D4E65772D4D 6173735C43724C6F 67696E2E646C6C33 433A5C43656E7475
0120: 72615C4372696D33 32202D204F524143 4C455C4372696D4E 65772D4D6173735C
0140: 43724C6F67696E2E 6170630000010101 006400000006696E 2E6170640B43724C
0160: 6F67696E2E646C6C 0B43724C6F67696E 2E61706300000101 0100640000000C4A
0180: 7576656E696C652E 61706C0B43724C6F 67696E2E646C6C0B 43724C6F67696E2E
01A0: 6170630000010101 0064000000000000 0000010101006400 0000000000000001
01C0: 0101006400000000 0000000001010100 6400000000000000 0000000000000000
01E0: 0000000000000000 0000000000000100 0000010000000100 0100000000000000
0200: 0001000010000000 0000000000000000 00
.enddata
.head 2 -  Outline Window State: Maximized
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040002001B00 0200000000000000 00003816A00F0500 1D00FFFF4D61696E
0020: 0020000100040000 0000000000F51E81 0F0000DF00FFFF56 61726961626C6573
0040: 0029000100040000 0000000000F51E81 0F00008600FFFF49 6E7465726E616C20
0060: 46756E6374696F6E 73001E0001000400 000000000000F51E 810F0000F400FFFF
0080: 436C617373657300
.enddata
.data VIEWSIZE
0000: 8800
.enddata
.head 3 -  Left: 0.01"
.head 3 -  Top: 0.133"
.head 3 -  Width:  7.96"
.head 3 -  Height: 4.425"
.head 2 +  Options Box Location
.data VIEWINFO
0000: C4389105B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 8.325"
.head 3 -  Top: 0.135"
.head 2 -  Fully Qualified External References? No
.head 2 -  Reject Multiple Window Instances? Yes
.head 2 -  Enable Runtime Checks Of External References? No
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 1 +  Libraries
.head 2 -  File Include: Registry.apl
.head 2 -  File Include: Datafields.Apl
.head 2 -  File Include: qcktabs.apl
.head 2 -  File Include: mtbl.apl
.head 2 -  File Include: mimg.apl
.head 2 -  File Include: vttblwin.apl
.head 2 -  File Include: vtfile.apl
.head 2 -  File Include: vtstr.apl
.head 2 -  ! File Include: Crystal Reports ActiveX Designer Run Time Library 11.5.apl
.head 2 -  File Include: Microsoft Windows Common Controls-2 6.0 (SP4).apl
.head 2 -  ! File Include: Crystal ActiveX Report Viewer Library 11.5.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MM-dd-yyyy
.head 2 -  External Functions
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 -  System
.head 3 +  User
.head 4 -  Number: WM_NEXTDLGCTL = 0x0028
.head 4 -  Number: VKInsert = 0x2D
.head 4 -  Number: VKDelete = 0x2E
.head 4 -  Number: VK_Backslash = 220
.head 4 -  Number: VKForwardslash = 191
.head 4 -  Number: Key_Up           = 0x26
.head 4 -  Number: Key_Down         = 0x28
.head 4 -  String: String_Null         = ''
.head 4 -  Number: CRYSTAL_TYPE_SINGLE = 0
.head 4 -  Number: VK_NEXT = 0x22
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 +  Variables
.head 3 -  Boolean: bLogin
.head 3 -  Date/Time: dTimestamped
.head 3 -  ! !!!!!  <<Handle & String  to open Last User Login File >>
.head 3 -  File Handle: hFLogin
.head 3 -  String: sLogIn
.head 3 -  String: sCourtConst
.head 3 -  ! !!!!!  <<CourtInfo Data Fields >>
.head 3 -  String: sCourt
.head 3 -  String: sCourtCity
.head 3 -  String: sCourtClerk
.head 3 -  String: sCourtCounty
.head 3 -  String: sCourtAddr
.head 3 -  String: sCourtAddr2
.head 3 -  String: sCourtCityState
.head 3 -  String: sCourtPhone
.head 3 -  String: sCourtPhone2
.head 3 -  String: sCourtPhone3
.head 3 -  String: sCourtCode
.head 3 -  String: sCourtORI
.head 3 -  String: sCourtWarrantFlag
.head 3 -  String: sCourtPrelimFlag
.head 3 -  Date/Time: dCostsDate
.head 3 -  Number: nVBDFineInc
.head 3 -  Number: nVBDFineIncI
.head 3 -  ! !!!!!  <<User Table Data Fields >>
.head 3 -  String: sUClerk
.head 3 -  String: sUDivision
.head 3 -  String: sUDrawer
.head 3 -  String: sUDepartment
.head 3 -  String: sUFullName
.head 3 -  String: sUWPhone
.head 3 -  String: sUWPhone_Ext
.head 3 -  String: sUJudge
.head 3 -  Number: nUserId
.head 3 -  Number: nULevel
.head 3 -  Number: nUCashier
.head 3 -  Number: nURecCopies
.head 3 -  Boolean: bChange
.head 3 -  Number: nError
.head 3 -  String: sAdminLock
.head 3 -  String: sAdminComment
.head 3 -  ! !!!!!! <<The 'where clause' depending on where you are>>
.head 3 -  Number: nX
.head 3 -  Number: nY
.head 3 -  String: sSelectWhere
.head 3 -  Number: nReturn
.head 3 -  ! !!!!!! <<LABEL VARIABLES> >>
.head 3 -  String: sReturnRowid
.head 3 -  ! !!!!! <<REPORT VARIABLES>>
.head 3 -  String: sReport
.head 3 -  String: sReportBinds
.head 3 -  String: sReportInputs
.head 3 -  Number: nPrintErr
.head 3 -  FunctionalVar: Reports
.head 4 -  Class: cReports
.head 3 -  FunctionalVar: Service
.head 4 -  Class: cService
.head 3 -  FunctionalVar: DockClass
.head 4 -  Class: CrDocket
.head 3 -  ! ! !! << Error Log >>
.head 3 -  Long String: mlErrorLog
.head 2 +  Internal Functions
.head 3 +  Function: GetUserInfo
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Receive String: SqlUser
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 -  ! Set sSelectWhere = ReadRegistrySelect(  )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	userid, seclevel, division, drawer, cashier, 
		sname, name, rec_copies, wphone, wphone_ext, department, Judgeno
	FROM JCR_USERS  INTO  :nUserId, :nULevel, :sUDivision, :sUDrawer, :nUCashier, 
		:sUClerk, :sUFullName, :nURecCopies, :sUWPhone, :sUWPhone_Ext, :sUDepartment, :sUJudge
	WHERE username = Upper(:SqlUser)')
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlCommit( hSql )
.head 5 +  If nFetch = FETCH_Ok
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: GetCourtInfo
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFetch
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	Title, Court, Clerk, County, Address, Address2, CityState, Phone, Phone2, Phone3, Code, 
		VBDIncDate, VBDIncFine, VBDIncFineI, CourtConst, ori, warrant_letter, prelim_notice 
	  FROM	JCR_COURTINFO  INTO  :sCourt, :sCourtCity, :sCourtClerk, :sCourtCounty, :sCourtAddr, :sCourtAddr2, :sCourtCityState, :sCourtPhone,:sCourtPhone2,:sCourtPhone3, :sCourtCode, 
		:dCostsDate, :nVBDFineInc, :nVBDFineIncI, :sCourtConst, :sCourtORI, :sCourtWarrantFlag, :sCourtPrelimFlag' )
.head 5 -  Call SqlFetchNext( hSql, nFetch )
.head 5 -  Call SqlCommit( hSql )
.head 5 +  If sCourtConst = STRING_Null
.head 6 -  Call SalMessageBox('Error reading Court Title from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 6 -  Call SalQuit( )
.head 5 -  Set sCourtCity = SalStrProperX( sCourtCity )
.head 3 +  Function: ReadRegistryTitle
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/Court', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Title', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: WriteUser
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Write )
.head 6 -  Call SalFileWrite( hFLogin, SqlUser, 9 )
.head 6 -  Call SalFileClose( hFLogin )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  Call REG.WriteStringAt('Software/CJIS/LastUser',TRUE, 'User', SqlUser)
.head 5 -  Call REG.CloseKey( )
.head 3 +  Function: ReadRegistry
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/PUBLIC', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'PUBLIC', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: ReadRegistrySelect
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/UserSelect', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'Select', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: ReadRegistryLastUser
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: s
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: s
.head 5 -  String: bin
.head 5 -  Number: BinSize
.head 5 -  Number: i
.head 5 -  Number: f
.head 5 -  Boolean: b
.head 5 -  String: Arr[*]
.head 5 -  Number: n
.head 5 -  Number: min
.head 5 -  Number: max
.head 5 -  String: sKeyValue
.head 4 +  Actions
.head 5 -  ! ! Set Path to point to HKEY_CURRENT_USER
.head 5 -  Call REG.SetRootKey( HKEY_CURRENT_USER )
.head 5 -  ! ! CHECK FOR VALUE OF PUBLIC STRING
.head 5 -  ! !
.head 5 +  If not REG.OpenKey( '/Software/CJIS/LastUser', FALSE )
.head 6 -  ! Call ERR( 'Error opening key.' )
.head 5 +  If not REG.ReadString( 'User', s )
.head 6 -  ! Call ERR( 'Error reading string.' )
.head 5 -  ! ! retrieve a list of value names...
.head 5 +  If not REG.EnumValues( Arr )
.head 6 -  ! Call ERR( 'Error reading values.' )
.head 5 -  Call REG.CloseKey( )
.head 5 -  Return (s)
.head 3 +  Function: Check_Change_Password
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtChange_Password_Date
.head 5 -  String: sChangePassword
.head 5 -  Number: nGraceLogins
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select change_password_date  INTO  :dtChange_Password_Date  from JCR_COURTINFO ' )
.head 5 -  Call SqlFetchNext( hSql, nError)
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Select change_password, gracelogins
	into  :sChangePassword, :nGraceLogins
	from JCR_USERS where '||sSelectWhere)
.head 5 -  Call SqlFetchNext( hSql, nError )
.head 5 +  If (dtChange_Password_Date <= SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nGraceLogins > 0and sChangePassword = 'Y')
.head 6 +  If sChangePassword = 'Y'
.head 7 -  Set bChange = TRUE
.head 7 -  Call SalMessageBox( 'PASSWORD HAS EXPIRED,  You have '||SalNumberToStrX(nGraceLogins, 0 )||' grace logins left.  Please change your password.  NOTE:  Password must be unique, not used before for your login!', 'Warning', MB_Ok|MB_IconExclamation )
.head 7 -  Call SalModalDialog( dlgPasswordAPL, hWndForm )
.head 5 +  Else If (dtChange_Password_Date < SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 ) and nGraceLogins = 0 and sChangePassword = 'Y')
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalMessageBox(  'Your user login has been disabled, the grace logins of 5 have been used.  Please Contact System Administrator', 'NO GRACE LOGINS', MB_Ok|MB_IconExclamation )
.head 6 -  Call SalQuit(  )
.head 3 +  Function: Current_Date
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Parameters
.head 5 -  String: sTimestamp
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dCurrentDate
.head 4 +  Actions
.head 5 +  If sTimestamp = 'Timestamp'
.head 6 -  Call SqlPrepareAndExecute( hSql, 'Select sysdate
	from dual into :dCurrentDate' )
.head 6 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  Else
.head 6 -  Set dCurrentDate = SalDateConstruct( SalDateYear( SalDateCurrent(  ) ), SalDateMonth( SalDateCurrent(  ) ), SalDateDay( SalDateCurrent(  ) ), 0, 0, 0 )
.head 5 -  Return dCurrentDate
.head 3 +  Function: HolidayCheck	!! Returns TRUE if lands on holiday
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean: bHoliday
.head 4 +  Parameters
.head 5 -  Date/Time: dHearDate
.head 5 -  Boolean: bMessage
.head 5 -  Boolean: bWeekEnd
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sHoliday
.head 4 +  Actions
.head 5 -  Set sHoliday = STRING_Null
.head 5 -  Set dHearDate = SalDateConstruct( SalDateYear( dHearDate ), SalDateMonth( dHearDate ), SalDateDay( dHearDate ), 0, 0, 0 )
.head 5 -  Call SqlPrepareAndExecute( hSql, 'SELECT	text
			      FROM	jcr_calendar 
 			      INTO		:sHoliday
			      WHERE	:dHearDate = holiday')
.head 5 -  Call SqlFetchNext( hSql, nReturn )
.head 5 +  If nReturn = FETCH_Ok
.head 6 +  If bMessage
.head 7 -  Call SalMessageBeep( MB_IconAsterisk )
.head 7 -  Call SalMessageBox('Date would have fallen on ' || sHoliday || '.', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 -  Return TRUE
.head 5 +  Else If SalDateWeekday( dHearDate ) = 0
.head 6 +  If bMessage
.head 7 -  Call SalMessageBeep( MB_IconAsterisk )
.head 7 -  Call SalMessageBox('Date would have fallen on a Saturday', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 +  If bWeekEnd
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Return FALSE
.head 5 +  Else If SalDateWeekday( dHearDate ) = 1
.head 6 +  If bMessage
.head 7 -  Call SalMessageBeep( MB_IconAsterisk )
.head 7 -  Call SalMessageBox('Date would have fallen on a Sunday', 'Invalid Date', MB_Ok|MB_IconStop)
.head 6 +  If bWeekEnd
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Return FALSE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: Encode_BarCode
.head 4 -  Description: WRP (10-26-2003)
	-This function takes a string parameter and converts it into the code
	 that is understandable to a  barcode reader using CODE 128 font.
	-It returns the encoded text as a string variable
	-This function is compatible with Elfring Font INC
	-This uses Subset C
	-Perfered font is
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: BarTextIn
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: BarTextOut
.head 5 -  String: BarTextInA
.head 5 -  String: TempString
.head 5 -  String: BarTempOut
.head 5 -  String: BarCodeOut
.head 5 -  Number: Sum
.head 5 -  Number: n
.head 5 -  Number: nStrLength
.head 5 -  Number: ThisChar
.head 5 -  Number: CharValue
.head 5 -  Number: CheckSumValue
.head 5 -  String: CheckSum
.head 5 -  Number: Subset
.head 5 -  String: StartChar
.head 5 -  Number: Weighting
.head 5 -  Number: UCC
.head 5 -  String: sCurrentCharReal
.head 4 +  Actions
.head 5 -  ! Initialize input and output strings
.head 5 -  Set Sum = 104
.head 5 -  Set StartChar = '|'
.head 5 -  Set n = 1
.head 5 -  Set nStrLength = SalStrLength(BarTextIn)
.head 5 +  While n <= nStrLength
.head 6 -  ! ThisChar = (Asc(Mid(BarTextIn, II, 1)))
.head 6 -  ! Call SalStrFirstC( 'A', ThisChar )
.head 6 -  ! Call SalStrFirstC( SalStrMidX(BarTextIn, (n-1), 1), ThisChar )
.head 6 -  Set sCurrentCharReal = SalStrMidX(BarTextIn, (n-1), 1)
.head 6 -  Call SalStrFirstC( sCurrentCharReal, ThisChar )
.head 6 +  If ThisChar < 127
.head 7 -  Set CharValue = ThisChar - 32
.head 6 +  Else
.head 7 -  Set CharValue = ThisChar - 103
.head 6 -  Set Sum = Sum + (CharValue * n)
.head 6 +  If SalStrMidX(BarTextIn, (n-1), 1) = " "
.head 7 -  Set BarTextOut = BarTextOut || SalNumberToChar(228)
.head 6 +  Else If ThisChar = 34
.head 7 -  Set BarTextOut = BarTextOut || SalNumberToChar(226)
.head 6 +  Else
.head 7 -  Set BarTextOut = BarTextOut || SalStrMidX(BarTextIn, (n-1), 1)
.head 6 -  Set n = n + 1
.head 5 -  Set CheckSumValue = SalNumberMod( Sum, 103 )
.head 5 +  If CheckSumValue > 90
.head 6 -  Set CheckSum = SalNumberToChar(CheckSumValue + 103)
.head 5 +  Else If CheckSumValue > 0
.head 6 -  Set CheckSum = SalNumberToChar(CheckSumValue + 32)
.head 5 +  Else
.head 6 -  Set CheckSum = SalNumberToChar(228)
.head 5 -  Return (StartChar || BarTextOut || CheckSum || "~ ")
.head 3 +  Function: GetAdminLock
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sAdminLock
.head 4 +  Parameters
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseTy
.head 5 -  String: fCaseNo
.head 5 -  Receive String: fAdminLockComment
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAdminLock
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute(hSql, 'SELECT	admin_lock, admin_lock_comment
		  	      FROM	jcr_master
			      WHERE	caseyr = :fCaseYr and
					casety = :fCaseTy and
					caseno = :fCaseNo
			      INTO		:sAdminLock, :fAdminLockComment')
.head 5 -  Call SqlFetchNext(hSql, nReturn)
.head 5 -  Return sAdminLock
.head 3 +  Function: ValidatePhone
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  Receive String: sVPhone
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Boolean: bEditOk
.head 4 +  Actions
.head 5 -  Set bEditOk = TRUE
.head 5 -  Set sVPhone = StrReplace (sVPhone, ' ', '-' )
.head 5 -  If SalStrLength( sVPhone ) = 0
.head 5 +  Else If SalStrLength( sVPhone ) = 7
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) > 0 and SalStrToNumber( SalStrMidX( sVPhone, 3, 4 ) ) > 0
.head 7 -  Set sVPhone = SalStrMidX( sVPhone, 0, 3 ) || '-' || SalStrMidX( sVPhone, 3, 4 )
.head 6 +  Else
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 8, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else If SalStrLength( sVPhone ) = 8
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 4, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else If SalStrLength( sVPhone ) = 10
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) > 0 and SalStrToNumber( SalStrMidX( sVPhone, 3, 3 ) ) > 0 and SalStrToNumber( SalStrMidX( sVPhone, 6, 4 ) ) > 0
.head 7 -  Set sVPhone = SalStrMidX( sVPhone, 0, 3 ) || '-' || SalStrMidX( sVPhone, 3, 3 ) || '-' || SalStrMidX( sVPhone, 6, 4 )
.head 6 +  Else
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 8, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else If SalStrLength( sVPhone ) = 12
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 0, 3 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 4, 3 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 6 +  If SalStrToNumber( SalStrMidX( sVPhone, 8, 4 ) ) < 1
.head 7 -  Set bEditOk = FALSE
.head 5 +  Else
.head 6 -  Set bEditOk = FALSE
.head 5 +  If Not bEditOk
.head 6 -  Call SalMessageBox( 'Invalid Phone Number Entered;  Please verify', 'Phone/Area Code Error', MB_IconStop | MB_Ok )
.head 5 -  Return bEditOk
.head 3 +  Function: Insert_Address
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sIACaseYr
.head 5 -  String: sIACaseTy
.head 5 -  String: sIACaseNo
.head 5 -  String: sIASSN
.head 5 -  Date/Time: dIADOB
.head 5 -  String: sIAAddress
.head 5 -  String: sIACity
.head 5 -  String: sIAState
.head 5 -  String: sIAZip
.head 5 -  String: sIAAreaCode
.head 5 -  String: sIAPhone
.head 5 -  String: sReason
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAddrUser
.head 5 -  String: sAddrRowId
.head 5 -  Date/Time: sAddrDate
.head 5 -  Number: nIAReturn
.head 4 +  Actions
.head 5 +  If sIAAddress=STRING_Null and sIACity=STRING_Null and sIAState=STRING_Null and sIAZip=STRING_Null and sIAPhone=STRING_Null
.head 6 -  Return TRUE
.head 5 -  Call SalStrTrim( sIAPhone, sIAPhone )
.head 5 +  If SalStrLength( sIAPhone ) > 8 and sIAAreaCode = STRING_Null
.head 6 +  If SalStrLeftX( sIAPhone, 1 ) >= '0' and SalStrLeftX( sIAPhone, 1 ) <= '9'
.head 7 -  Set sIAAreaCode = SalStrLeftX( sIAPhone, 3 )
.head 6 +  Else
.head 7 -  Set sIAAreaCode = SalStrMidX( sIAPhone, 1, 3 )
.head 6 -  Set sIAPhone = SalStrRightX( sIAPhone, 8 )
.head 5 -  Call SqlPrepareAndExecute(hSql, "SELECT entrydate, username, rowid
	FROM jcr_address into :sAddrDate, :sAddrUser, :sAddrRowId
	WHERE caseyr = :sIACaseYr and casety = :sIACaseTy and caseno = :sIACaseNo and
		court='CMC' and reason=:sReason 
	ORDER BY entrydate desc")
.head 5 +  If Not SqlFetchNext (hSql, nIAReturn )
.head 6 -  Call SqlPrepareAndExecute (hSql, "INSERT INTO jcr_address
	(entrydate, ssno, dob, court, caseyr, casety, caseno, city, state, zip, address1, areacode, phone, reason) VALUES
	(sysdate, :sIASSN, :dIADOB, 'JU', :sIACaseYr, :sIACaseTy, :sIACaseNo, :sIACity, :sIAState, :sIAZip, :sIAAddress, :sIAAreaCode, :sIAPhone, :sReason)")
.head 5 +  Else
.head 6 +  If sAddrUser = SqlUser and sAddrDate > SalDateCurrent(  ) - 1
.head 7 -  Call SqlPrepareAndExecute (hSql, "Update jcr_address set
	entrydate=sysdate, ssno=:sIASSN, dob=:dIADOB, caseyr=:sIACaseYr, casety=:sIACaseTy,
	caseno=:sIACaseNo, city=:sIACity, state=:sIAState, zip=:sIAZip, address1=:sIAAddress, areacode=:sIAAreaCode, phone=:sIAPhone
	where rowid=:sAddrRowId")
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute (hSql, "INSERT INTO jcr_address
	(entrydate, ssno, dob, court, caseyr, casety, caseno, city, state, zip, address1, areacode, phone, reason) VALUES
	(sysdate, :sIASSN, :dIADOB, 'CMC', :sIACaseYr, :sIACaseTy, :sIACaseNo, :sIACity, :sIAState, :sIAZip, :sIAAddress, :sIAAreaCode, :sIAPhone, :sReason)")
.head 5 -  Call SqlCommit(hSql)
.head 3 +  Function: StrReplace
.head 4 -  Description: Replaces any occurences of sFind with sReplace
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sSource
.head 5 -  String: sFind
.head 5 -  String: sReplace
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nFoundPos
.head 5 -  String: sLeft
.head 5 -  String: sRight
.head 4 +  Actions
.head 5 -  Set sRight = sSource
.head 5 +  Loop
.head 6 -  Set nFoundPos = SalStrScan( sRight, sFind )
.head 6 +  If nFoundPos = -1
.head 7 -  Set sLeft = sLeft || sRight
.head 7 -  Break
.head 6 -  Set sLeft = sLeft || SalStrLeftX( sRight, nFoundPos ) || sReplace
.head 6 -  Set sRight = SalStrRightX( sRight, SalStrLength( sRight ) - nFoundPos - SalStrLength( sFind ) )
.head 5 -  Return sLeft
.head 3 +  Function: TBL_DisplaySum
.head 4 -  Description: WRP - Function will set a windows status bar with the number of rows
          selected, the sum of the a certain column ( hTBL_Col), and the average
          of the certain column.
          Function returns TRUE if it was succesful in setting the Status Text and FALSE if not.
.head 4 +  Returns
.head 5 -  Boolean: bReturn
.head 4 +  Parameters
.head 5 -  Window Handle: hTBL   !Table to use
.head 5 -  Window Handle: hTBL_Col    !Table Column to sum and average * must be a number
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nRowAmount
.head 5 -  Number: nCountRow
.head 5 -  Number: nRow
.head 5 -  String: sText
.head 5 -  Boolean: bReturn
.head 4 +  Actions
.head 5 -  Set nRowAmount = 0
.head 5 -  Set nCountRow = 0
.head 5 -  Set nRow = -1
.head 5 +  While SalTblFindNextRow( hTBL, nRow, ROW_Selected, NUMBER_Null )
.head 6 -  Call SalTblSetFocusRow( hTBL, nRow )
.head 6 -  Set sText =  VisTblGetCell( hTBL, nRow, hTBL_Col )
.head 6 -  Set nRowAmount = nRowAmount + SalStrToNumber( sText)
.head 6 -  Set nCountRow = nCountRow + 1
.head 5 +  If nCountRow > 0
.head 6 -  Set bReturn = SalStatusSetText( hWndForm, SalNumberToStrX(nCountRow, 0)||' Rows Selected - $'||SalNumberToStrX( nRowAmount, 2)||' Rows Average - $'||SalNumberToStrX( nRowAmount/nCountRow, 2))
.head 5 +  Else
.head 6 -  Set bReturn = SalStatusSetText( hWndForm, 'Ready')
.head 5 -  Return bReturn
.head 3 +  Function: TBL_ReturnSum
.head 4 -  Description: WRP - Function will return the number of rows (nRetRows), sum of rows (nRetSumRows) and
          average of rows (nRetAvgRows) of the selected rows in a table (hTBL)
          Function returns TRUE if it was succesful in setting the Status Text and FALSE if not.
.head 4 +  Returns
.head 5 -  Boolean: bReturn
.head 4 +  Parameters
.head 5 -  Window Handle: hTBL   !Table to use
.head 5 -  Window Handle: hTBL_Col    !Table Column to sum and average * must be a number
.head 5 -  Receive Number: nRetRows
.head 5 -  Receive Number: nRetSumRows
.head 5 -  Receive Number: nRetAvgRows
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nRowAmount
.head 5 -  Number: nCountRow
.head 5 -  Number: nRow
.head 5 -  String: sText
.head 5 -  Boolean: bReturn
.head 4 +  Actions
.head 5 -  Set nRowAmount = 0
.head 5 -  Set nCountRow = 0
.head 5 -  Set nRow = -1
.head 5 +  While SalTblFindNextRow( hTBL, nRow, ROW_Selected, NUMBER_Null )
.head 6 -  Set bReturn = SalTblSetFocusRow( hTBL, nRow )
.head 6 -  Set sText =  VisTblGetCell( hTBL, nRow, hTBL_Col )
.head 6 -  Set nRowAmount = nRowAmount + SalStrToNumber( sText)
.head 6 -  Set nCountRow = nCountRow + 1
.head 5 -  Set nRetRows = nCountRow
.head 5 -  Set nRetSumRows = nRowAmount
.head 5 +  If nCountRow > 0
.head 6 -  Set nRetAvgRows = nRowAmount/nCountRow
.head 5 -  Return bReturn
.head 3 +  Function: JU_On_Startup
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sWindow
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call On_Startup( STRING_Null )
.head 5 -  Call GetCourtInfo(  )
.head 5 -  Call GetUserInfo( SqlUser )
.head 5 -  Call Run_Startup_Form( sWindow )
.head 3 +  Function: UpdateSupreme
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  String: sJU_Code
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 5 -  Boolean: bCommit
.head 5 -  String: sProgramLocation
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Date/Time: dtToday
.head 5 -  Number: nMaxSeq
.head 5 -  String: sJU_Code2
.head 5 -  String: sID
.head 5 -  String: sJudgeMaster
.head 5 -  String: sLastType
.head 4 +  Actions
.head 5 +  If dtInsDateF = DATETIME_Null
.head 6 -  Set dtToday = Current_Date( '' )
.head 5 +  Else
.head 6 -  Set dtToday =dtInsDateF
.head 5 -  ! Get JU_CODE, JUDGE
.head 5 +  If sCaseTy != "JCV"
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	ju_code, id, judge
			      FROM	jcr_master into :sJU_Code2, :sID, :sJudgeMaster
			      WHERE	caseyr = :sCaseYr and casety = :sCaseTy and caseno = :sCaseNo')
.head 6 -  Call SqlFetchNext( hSqlSup, nReturn)
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSqlSup, "SELECT	ju_code, judge_number
			       FROM	jcv_master INTO :sJU_Code2, :sJudgeMaster
			       WHERE	caseyr = :sCaseYr and casety = :sCaseTy and caseno = :sCaseNo" )
.head 6 -  Call SqlFetchNext( hSqlSup, nReturn)
.head 5 +  If sJudge = ''
.head 6 -  Set sJudge = sJudgeMaster
.head 6 -  Call SqlPrepareAndExecute( hSqlSup, "SELECT last_name FROM judge INTO :sJudge WHERE judge_number = :sJudgeMaster" )
.head 6 -  Call SqlFetchNext( hSqlSup, nReturn )
.head 5 +  If sJU_Code = STRING_Null
.head 6 -  ! Set JU_Code equal to what you previously received from database
.head 6 -  Set sJU_Code = sJU_Code2
.head 6 +  If sCaseTy = 'TRA' or sCaseTy = 'JTR'
.head 7 -  Set sJU_Code = 'B'
.head 6 +  Else
.head 7 +  If sJU_Code = 'DEL'
.head 8 -  Set sJU_Code = 'A'
.head 7 +  Else If sJU_Code = 'TRA'
.head 8 -  Set sJU_Code = 'B'
.head 7 +  Else If sJU_Code = 'UNR'
.head 8 -  Set sJU_Code = 'D'
.head 7 +  Else If sJU_Code = 'ADU'
.head 8 -  Set sJU_Code = 'E'
.head 7 +  Else
.head 8 -  Call SalMessageBox ('Warning: Supreme Court code could not be set on

Case ' || sCaseYr || sCaseTy || sCaseNo || ' - Please make of note of this.', 'Supreme Court Count Error', MB_Ok | MB_IconInformation)
.head 5 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	caseyr
			      FROM	jcr_supreme
			      WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo and
					type = :sType and
					ins_date = :dtToday and
					line_no = :nLine')
.head 5 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 5 +  If nReturn = FETCH_EOF
.head 6 -  Call SqlPrepareAndExecute(hSqlSup, 'SELECT	max(seq)
			     FROM		jcr_supreme
			     WHERE	caseyr = :sCaseYr and
					casety = :sCaseTy and
					caseno = :sCaseNo
			     INTO		:nMaxSeq')
.head 6 -  Call SqlFetchNext(hSqlSup, nReturn)
.head 6 +  If nMaxSeq = NUMBER_Null
.head 7 -  Set nMaxSeq = 0
.head 6 -  Set nMaxSeq = nMaxSeq + 1
.head 6 -  Call SqlPrepareAndExecute( hSqlSup, "SELECT type INTO :sLastType
				FROM jcr_supreme
				WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND 
					seq = (SELECT MAX(seq) FROM jcr_supreme where caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo)
				")
.head 6 -  Call SqlFetchNext( hSqlSup, nResult )
.head 6 +  If sLastType = "AJO" And sType = "AJO"
.head 7 -  Call SqlPrepareAndExecute(hSqlSup, "INSERT INTO jcr_supreme_error 
	(caseyr, casety, caseno, ju_code, seq, ins_date, type, line_no, judge, program_location, id, error_msg)  VALUES 
	(:sCaseYr, :sCaseTy, :sCaseNo, :sJU_Code, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation, :sID, 'Tried to insert an AJO record when the last record inserted was an AJO.')" )
.head 6 +  Else If sLastType = "AJI" And sType = "AJI"
.head 7 -  Call SqlPrepareAndExecute(hSqlSup, "INSERT INTO jcr_supreme_error 
	(caseyr, casety, caseno, ju_code, seq, ins_date, type, line_no, judge, program_location, id, error_msg)  VALUES 
	(:sCaseYr, :sCaseTy, :sCaseNo, :sJU_Code, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation, :sID, 'Tried to insert an AJI record when the last record inserted was an AJI.')" )
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute(hSqlSup, 'INSERT INTO jcr_supreme 
	(caseyr, casety, caseno, ju_code, seq, ins_date, type, line_no, judge, program_location, id)  VALUES 
	(:sCaseYr, :sCaseTy, :sCaseNo, :sJU_Code, :nMaxSeq, :dtToday, :sType, :nLine, :sJudge, :sProgramLocation, :sID)')
.head 6 +  If bCommit
.head 7 -  Call SqlCommit(hSqlSup)
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: ExistSupreme
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Number: nLine
.head 5 -  String: sType
.head 5 -  Date/Time: dtInsDateF
.head 5 -  String: sJudge
.head 5 -  Sql Handle: hSqlSup
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nReturnSup
.head 5 -  String: sWhere
.head 4 +  Actions
.head 5 +  If nLine != NUMBER_Null
.head 6 -  Set sWhere = ' and line_no = :nLine '
.head 5 +  If sType != ''
.head 6 -  Set sWhere = sWhere||' and type = :sType '
.head 5 +  If dtInsDateF != DATETIME_Null
.head 6 -  Set sWhere = sWhere||' and ins_date = :dtInsDateF '
.head 5 +  If sJudge != ''
.head 6 -  Set sWhere = sWhere||' and judge = :sJudge '
.head 5 -  Call SqlPrepareAndExecute( hSqlSup, 'SELECT		*
			               FROM		jcr_supreme
				 WHERE		caseyr = :sCaseYr and
						casety = :sCaseTy and
						caseno = :sCaseNo '||
				    sWhere)
.head 5 -  Call SqlFetchNext( hSqlSup, nReturnSup )
.head 5 +  If nReturnSup = FETCH_Ok
.head 6 -  Return TRUE
.head 5 +  Else
.head 6 -  Return FALSE
.head 3 +  Function: Get_Service_Number  !!! BARCODE 3 OF 9
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: spCaseYr
.head 5 -  String: spCaseTy
.head 5 -  String: spCaseNo
.head 5 -  String: spZip
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sDefNo
.head 4 +  Actions
.head 5 -  Call SqlPrepare(hSqlOne,"Select nvl(max(Service_seq)+1,0)  from JCR_SERVICE where
caseyr = :spCaseYr AND casety = :spCaseTy AND caseno = :spCaseNo into :nServiceSeq ")
.head 5 -  Call SqlExecute(hSqlOne)
.head 5 -  Call SqlFetchNext(hSqlOne,nResult)
.head 5 -  Call SalNumberToStr(nServiceSeq,0,sServiceSeq)
.head 5 -  Set sServiceSeq='000'||sServiceSeq
.head 5 -  Call SalStrRight(sServiceSeq,3,sServiceSeq)
.head 5 -  Set strServiceNumber= spZip || spCaseYr || spCaseTy || spCaseNo || sServiceSeq
.head 5 -  ! changing bar code seq
.head 5 -  ! Set strServiceNumber=strServiceNumber || '0000000000000000000'
.head 5 -  Set strServiceNumber=strServiceNumber || 'XXXXXXXXXXXXXXXXXXX'
.head 5 -  ! changing left to right
.head 5 -  Call SalStrLeft(strServiceNumber,20,strServiceNumber)
.head 5 -  Set strServiceRef=strServiceNumber
.head 5 -  Set strServiceNumber='*'||strServiceNumber||'*'
.head 3 +  Function: Error
.head 4 -  Description: Shows an error message box and returns FALSE for convenience
.head 4 +  Returns
.head 5 -  Boolean:
.head 4 +  Parameters
.head 5 -  String: sMsg
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalMessageBox( sMsg, "ERROR", MB_Ok | MB_IconStop )
.head 5 -  Return FALSE
.head 3 +  Function: Get_Age
.head 4 -  Description: Returns age of passed dob
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Date/Time: dDOB
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nAge
.head 5 -  Number: nCurYear
.head 5 -  Number: nBirthYear
.head 5 -  Date/Time: dCur
.head 5 -  Date/Time: dCurBirthday
.head 4 +  Actions
.head 5 -  Set dCur = SalDateCurrent()
.head 5 -  Set nCurYear = SalDateYear( dCur )
.head 5 -  Set nBirthYear = SalDateYear( dDOB )
.head 5 -  Set nAge = nCurYear - nBirthYear
.head 5 -  Set dCurBirthday = SalDateConstruct( nCurYear, SalDateMonth( dDOB ), SalDateDay( dDOB ), 0, 0, 0 )
.head 5 +  If dCur < dCurBirthday
.head 6 -  Set nAge = nAge - 1
.head 5 -  Return nAge
.head 3 +  Function: Get_Code
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sType
.head 5 -  String: sCode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sDescrip
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT description INTO :sDescrip FROM jcr_valid_codes WHERE type = :sType AND code = :sCode" )
.head 5 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 5 -  Return sDescrip
.head 3 +  Function: Msg
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sMsg
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalMessageBox( sMsg, "NOTICE", MB_Ok | MB_IconAsterisk )
.head 3 +  Function: YesNo
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  String: sQuestion
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Return SalMessageBox( sQuestion, "QUESTION", MB_YesNo | MB_IconQuestion )
.head 3 +  Function: Insert_Docket
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  String: sCode
.head 5 -  String: sText
.head 5 -  Date/Time: dDate
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sID
.head 4 +  Actions
.head 5 +  If sText = STRING_Null
.head 6 -  Call SqlPrepareAndExecute( hSqlOne, "SELECT description INTO :sText FROM jcr_docket_codes WHERE code = :sCode" )
.head 6 -  Call SqlFetchNext( hSqlOne, nResult )
.head 5 -  Call SqlPrepareAndExecute( hSqlOne, "SELECT id INTO :sID FROM jcr_master WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo" )
.head 5 -  Call SqlFetchNext( hSqlOne, nResult )
.head 5 +  If nResult != FETCH_EOF
.head 6 -  Call DockClass.D_Insert_Docket( sID, sCaseYr, sCaseTy, sCaseNo, dDate, sCode, sText, NUMBER_Null, NUMBER_Null, "#1", STRING_Null, 'N')
.head 3 +  Function: Get_Date_Suffix
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  Number: nDate
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If nDate = 1 Or nDate = 21 Or nDate = 31
.head 6 -  Return SalNumberToStrX( nDate, 0 ) || "st"
.head 5 +  If nDate = 2 Or nDate = 22
.head 6 -  Return SalNumberToStrX( nDate, 0 ) || "nd"
.head 5 +  If nDate = 3 Or nDate = 23
.head 6 -  Return SalNumberToStrX( nDate, 0 ) || "rd"
.head 5 -  Return SalNumberToStrX( nDate, 0 ) || "th"
.head 3 +  Function: TblRowCount
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Window Handle: h
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: n
.head 4 +  Actions
.head 5 +  While SalTblSetContext( h, n )
.head 6 -  Set n = n + 1
.head 5 -  Return n
.head 3 +  Function: Get_User_Fullname
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String: sRet
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sReturn
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, "SELECT user_fullname INTO :sReturn FROM system_users WHERE UPPER(user_name) = UPPER(:SqlUser)" )
.head 5 -  Call SqlFetchNext( hSql, nResult )
.head 5 -  Return sReturn
.head 3 +  Function: Get_Case_Type
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sCode
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sType
.head 4 +  Actions
.head 5 -  Set sType = "Juvenile " || Get_Code( "JU_CODE", sCode ) || " Offender"
.head 5 +  If sType = "Juvenile Adult Offender"
.head 6 -  Set sType = "Adult Offender"
.head 5 -  Return sType
.head 3 +  Function: Get_Charges
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 5 -  Receive String: sDegree
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sCharges
.head 5 -  String: sStatute
.head 5 -  Number: nCount
.head 5 -  String: sCharge
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT statute, degree, description INTO :sStatute, :sDegree :sCharge FROM jcr_charge WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND chargeno = 1 ORDER BY chargeno" )
.head 5 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 5 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT
	count( description )
INTO
	:nCount
FROM
	jcr_charge
WHERE
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND description = :sCharge
ORDER BY
	chargeno
" )
.head 5 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 5 +  If nCount > 0
.head 6 -  Set sCharges = sCharges || "
" || SalNumberToStrX( nCount, 0 ) || " count"
.head 6 +  If nCount > 1
.head 7 -  Set sCharges = sCharges || "s"
.head 6 -  Set sCharges = sCharges || " of " || sStatute || " - " || sCharge || "."
.head 6 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT
	count( description ), statute, description
INTO
	:nCount, :sStatute, :sCharge
FROM
	jcr_charge
WHERE
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND description != :sCharge
GROUP BY
	statute, description
ORDER BY
	description
" )
.head 6 +  While SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set sCharges = sCharges || "
" || SalNumberToStrX( nCount, 0 ) || " count"
.head 7 +  If nCount > 1
.head 8 -  Set sCharges = sCharges || "s"
.head 7 -  Set sCharges = sCharges || " of " || sStatute || " - " || sCharge || "."
.head 6 -  Set sCharges = SalStrRightX( sCharges, SalStrLength( sCharges ) - 2 )
.head 5 -  Return sCharges
.head 3 +  Function: Warn
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sMsg
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalMessageBox( sMsg, "WARNING", MB_Ok | MB_IconExclamation )
.head 3 +  Function: Get_Docket_String
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sCaseYr
.head 5 -  String: sCaseTy
.head 5 -  String: sCaseNo
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sDocket
.head 5 -  Date/Time: dDate
.head 5 -  Long String: sText
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSql, "
SELECT 
	dock_date, data 
INTO 
	:dDate, :sText 
FROM 
	jcr_docket 
WHERE 
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo
ORDER BY 
	TRUNC( dock_date ), seq, seq2
" )
.head 5 +  While SqlFetchNext( hSql, nResult )
.head 6 -  Set sDocket = sDocket || SalFmtFormatDateTime( dDate, "M/d/yy" ) || " - " || sText || "
"
.head 5 -  Return sDocket
.head 3 +  Function: Split_CaseNo
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sCaseNumber
.head 5 -  Receive String: sCaseYr
.head 5 -  Receive String: sCaseTy
.head 5 -  Receive String: sCaseNo
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalStrMid( sCaseNumber, 0, 4, sCaseYr )
.head 5 -  Call SalStrMid( sCaseNumber, 4, 3, sCaseTy )
.head 5 -  Call SalStrMid( sCaseNumber, 7, 99, sCaseNo )
.head 3 +  Function: Get_Code_Rev
.head 4 -  Description: Returns the proper code for the description in the given type
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  String: sType
.head 5 -  String: sDescrip
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sCode
.head 4 +  Actions
.head 5 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT code INTO :sCode FROM jcr_valid_codes WHERE UPPER( description ) = UPPER( :sDescrip ) AND type = :sType" )
.head 5 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 5 -  Return sCode
.head 3 +  Function: Word_Wrap
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  String:
.head 4 +  Parameters
.head 5 -  Number: nWidth
.head 5 -  Receive Long String: sText
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Long String: sTemp
.head 5 -  String: sChar
.head 4 +  Actions
.head 5 +  If SalStrLength( sText ) > nWidth
.head 6 -  Set sTemp = SalStrLeftX( sText, nWidth )
.head 6 -  Set sText = SalStrRightX( sText, SalStrLength( sText ) - nWidth )
.head 6 -  Set sChar = SalStrRightX( sTemp, 1 )
.head 6 +  While sChar != " " And SalStrLength( sTemp ) > 0
.head 7 -  Set sTemp = SalStrLeftX( sTemp, SalStrLength( sTemp ) - 1 )
.head 7 -  Set sText = sChar || sText
.head 7 -  Set sChar = SalStrRightX( sTemp, 1 )
.head 5 +  Else
.head 6 -  Set sTemp = sText
.head 6 -  Set sText = ""
.head 5 +  If sTemp = ""
.head 6 -  Set sTemp = SalStrLeftX( sText, nWidth )
.head 6 -  Set sText = SalStrRightX( sText, SalStrLength( sText ) - nWidth )
.head 5 -  Return sTemp
.head 3 +  Function: AddToError
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sEProgram
.head 5 -  String: sECaseYr
.head 5 -  String: sECaseTy
.head 5 -  String: sECaseNo
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nPos
.head 5 -  Number: nError
.head 5 -  Boolean: bFirstError
.head 5 -  Long String: sErrorText
.head 5 -  Long String: sStatement
.head 5 -  Window Handle: hSqlError
.head 4 +  Actions
.head 5 +  If mlErrorLog = STRING_Null
.head 6 -  Set bFirstError = TRUE
.head 5 -  Call SqlExtractArgs( wParam, lParam, hSqlError, nError, nPos )
.head 5 -  Set sErrorText = SqlGetErrorTextX( nError )
.head 5 -  Set sStatement = SqlGetLastStatement(  )
.head 5 +  If sStatement = STRING_Null
.head 6 -  Return TRUE
.head 5 -  Set mlErrorLog = mlErrorLog || '
--- In Following statement: 
' || sStatement || '

--- Error Text: ' || sErrorText || '
'
.head 5 +  If bFirstError = TRUE
.head 6 -  Call SalMessageBox( 'Error Message Logged;  Contact Programmer', 'SQL Error', MB_Ok | MB_IconStop )
.head 5 +  Else If SalMessageBox( 'Error Message Logged;  Contact Programmer', 'SQL Error', MB_OkCancel | MB_IconStop ) = IDCANCEL
.head 6 -  Call SalQuit(  )
.head 6 -  Return TRUE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'Insert into JCR_Error_Log 
	(POSTDATE, PROGRAM, CASEYR, CASETY, CASENO, USERNAME, MESSAGE ) Values 
	(Sysdate, substr(:sEProgram,1,16), :sECaseYr, :sECaseTy, :sECaseNo, user, substr(:mlErrorLog, 1, 1500))')
.head 5 -  Call SqlCommit( hSql )
.head 3 +  Function: SaveSQLErrorsToText
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: sApplication
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  File Handle: fFile
.head 5 -  String: sFileName
.head 4 +  Actions
.head 5 +  If mlErrorLog = STRING_Null
.head 6 -  Return TRUE
.head 5 -  Set sFileName = sApplication || '_Results_'|| SalFmtFormatDateTime( SalDateCurrent(  ), 'MM-dd-yyyy-hh.mm.ss AMPM' )||'.txt'
.head 5 -  Call SalFileOpen( fFile, sFileName,  OF_Write| OF_Create )
.head 5 -  Call SalFilePutStr( fFile, sApplication)
.head 5 -  Call SalFilePutStr( fFile, 'END DATE: '||  SalFmtFormatDateTime( SalDateCurrent(  ), 'MM-dd-yyyy-hh.mm.ss AMPM' ))
.head 5 -  Call SalFilePutStr( fFile, '')
.head 5 -  Call SalFilePutStr( fFile, 'ERRORS Encountered below:')
.head 5 -  Call SalFilePutStr( fFile, mlErrorLog)
.head 5 -  Call SalFileClose( fFile )
.head 5 -  ! Set dfStatus = dfStatus||', '|| 'Summary File Saved ('|| sFileName
.head 3 -  ! End Juvenile.apl functions
.head 3 -  !
.head 2 +  Named Menus
.head 3 +  Menu: CTABLE_MENU
.head 4 -  Resource Id: 58425
.head 4 -  Picture File Name:
.head 4 -  Title:
.head 4 -  Description:
.head 4 -  Enabled when:
.head 4 -  Status Text:
.head 4 -  Menu Item Name:
.head 4 +  Menu Item: &Export Table Data
.head 5 -  Resource Id: 58426
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Save to Excel and Load
.head 5 -  Resource Id: 58427
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'EXPORT_SAVE_LOAD'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Print Table Report
.head 5 -  Resource Id: 58428
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'PRINT_TABLE'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &View Table Report
.head 5 -  Resource Id: 58429
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'VIEW_TABLE'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Copy Selected Rows to Clipboard
.head 5 -  Resource Id: 58430
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_SELECTED'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 +  Menu Item: &Copy All Rows to Clipboard
.head 5 -  Resource Id: 58431
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'COPY_ALL'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Find
.head 5 -  Resource Id: 58432
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'FIND'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 4 -  Menu Separator
.head 4 +  Menu Item: &Auto Size Columns
.head 5 -  Resource Id: 58433
.head 5 -  Picture File Name:
.head 5 -  Keyboard Accelerator: (none)
.head 5 -  Status Text:
.head 5 +  Menu Settings
.head 6 -  Enabled when:
.head 6 -  Checked when:
.head 5 +  Menu Actions
.head 6 -  Set sCTABLE_MENU_TYPE = 'AUTO_SIZE'
.head 6 -  Call SalSendMsg( hWndForm, SAM_User, 0,0)
.head 5 -  Menu Item Name:
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 3219551595
0000: 7E01000016010000 0000000000000000 0200000200FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F19003C00 0008430072446F63
0040: 6B65742280000000 01000000A7190000 8001040161000000 0773000000011A00
0060: 0001804E20000000 0100000800635265 706F727473081201 00090000000F0400
0080: 0000010204010500 0000439A01730000 B601000459000400 02C3060000009E9A
00A0: 00010D0008000000 D6040002B0070000 009A67000100830C 0000001925000104
00C0: 0800ED009AD10001 0000602500000004 004D020900007B9A 0036010029005800
00E0: 00040002C30A0000 009E9A00010D002D 000000D6040002B0 0B0000009A670001
0100: 0083310000000435 00020C00ED009AD9 0001006035000000 19000901040D0000
0120: 007B9A0034010000
.enddata
.head 3 +  Data Field Class: cCaseYr
.head 4 -  Data
.head 5 -  Maximum Data Length: 4
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalStrLength( MyValue ) = 1
.head 7 -  Set MyValue = '200' || MyValue
.head 6 +  If SalStrLength( MyValue ) = 2
.head 7 +  If MyValue > '49'
.head 8 -  Set MyValue = '19' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  Else
.head 8 -  Set MyValue = '20' || MyValue
.head 8 +  If SalStrToNumber( MyValue ) = 0
.head 9 -  Set MyValue = ''
.head 9 -  Call SalMessageBox( 'Not a valid year', 'Error', MB_Ok|MB_IconStop )
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If SalStrLength( MyValue ) = 2 and (MyValue != '19' and MyValue !='20')
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'TRA'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'JCR'
.head 6 +  Else If MyValue = 'T'
.head 7 -  Set MyValue = 'JTR'
.head 6 -  If MyValue = 'TRA' or MyValue = 'JCR' or MyValue = 'JTR' 
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseVTy
.head 4 -  Data
.head 5 -  Maximum Data Length: 3
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.59"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If MyValue = 'A'
.head 7 -  Set MyValue = 'TRA'
.head 6 +  Else If MyValue = 'C'
.head 7 -  Set MyValue = 'JCR'
.head 6 +  Else If MyValue = 'T'
.head 7 -  Set MyValue = 'JTR'
.head 6 -  If MyValue = 'TRA' or MyValue = 'JCR' or MyValue = 'JTR' 
.head 6 +  Else If Not SalIsNull( MyValue )
.head 7 -  Call SalMessageBox( 'An Invalid Case Type has been Entered', 'Case Type Error', MB_Ok )
.head 7 -  Return VALIDATE_Cancel
.head 6 -  Return VALIDATE_Ok
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cCaseNo
.head 4 -  Data
.head 5 -  Maximum Data Length: 6
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  0.8"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 -  Call SalStrTrim( MyValue, MyValue )
.head 6 +  If Not SalIsNull( MyValue )
.head 7 +  While SalStrLength( MyValue ) < 5
.head 8 -  Set MyValue = '0' || MyValue
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = SalGetMaxDataLength( MyValue )
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 3 +  Data Field Class: cDate
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_Backslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ( 'Date' )
.head 7 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If wParam = Key_Up
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue + 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 6 +  Else If wParam = Key_Down
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue - 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 3 +  Data Field Class: cDateDOB
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalDateYear( MyValue ) > SalDateYear( SalDateCurrent(  ) )
.head 7 -  Set MyValue = SalDateConstruct( SalDateYear( MyValue ) - 100, SalDateMonth( MyValue ), SalDateDay( MyValue ), SalDateHour( MyValue ), SalDateMinute( MyValue ), 0 )
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue > SalDateConstruct( SalDateYear( SalDateCurrent( ) ) - 18, SalDateMonth( SalDateCurrent( ) ), SalDateDay( SalDateCurrent( ) ), SalDateHour( SalDateCurrent( ) ), SalDateMinute( SalDateCurrent( ) ), SalDateSecond( SalDateCurrent( ) ) )
.head 8 -  Call SalSetFocus( MyValue )
.head 7 +  Else
.head 8 +  If SalMessageBox( 'Birth Date is Greater than 18 Years, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation ) = IDCANCEL
.head 9 -  Return VALIDATE_Cancel
.head 3 +  Data Field Class: cLoginUser
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: String
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.0"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Unformatted
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Window Handle: hWndNext
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 +  ! If SalFileOpen( hFLogin, 'login.TXT', OF_Read )
.head 7 -  Call SalFileRead( hFLogin, sLogIn, 9 )
.head 7 -  Set SqlUser = SalStrTrimX( SalStrLeftX( sLogIn, 9 ) )
.head 7 -  Call SalFileClose( hFLogin )
.head 6 -  ! Set MyValue = SqlUser
.head 6 +  ! If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 6 -  Set SqlUser =ReadRegistryLastUser(  )
.head 6 -  Set MyValue = SqlUser
.head 6 +  If MyValue != ''
.head 7 -  Set hWndNext = SalGetNextChild( hWndItem, TYPE_DataField )
.head 7 -  Call SalSetFocus( hWndNext  )
.head 3 +  Data Field Class: cDfAutoTab
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  Class Default
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Class Default
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description: This data field class will automatically set focus
to the next object in the tab order when the
maximum length has been reached.
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nMaxLength
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set nMaxLength = SalGetMaxDataLength( hWndItem )
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( MyValue ) = nMaxLength
.head 7 -  Call SalSendMsg( hWndForm, WM_NEXTDLGCTL, 0, 0 )
.head 3 +  ! Functional Class: Costs
.winattr
.end
.head 4 -  Description: 
.head 4 -  Derived From 
.head 4 -  Class Variables 
.head 4 +  Instance Variables 
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 5 -  Sql Handle: hSqlCost
.head 5 -  Boolean: bCostClass
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseNo
.head 5 -  String: fCaseTy
.head 4 +  Functions 
.head 5 +  Function: C_Insert_Costs
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: fCode
.head 7 -  Number: fCount
.head 7 -  String: fCity_State_Code	!!! This allows only 1 part of the group to be inserted
.head 7 -  Boolean: bCommit
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Date/Time: dCurrentDate
.head 7 -  String: sCodeAmt
.head 7 -  String: sCodeDesc
.head 7 -  String: sInsertCost
.head 7 -  String: sWhereC2S2Code   !!! Used to only select 1 code out of the group when inserting
.head 6 +  Actions 
.head 7 -  Set fCode = SalStrTrimX( fCode )
.head 7 -  Set dCurrentDate = Current_Date(  )
.head 7 -  Set sCodeAmt = C_Get_Primary_Charge_Type( )
.head 7 +  If sCodeAmt = 'C'
.head 8 -  Set sCodeAmt = 'STATE'
.head 8 -  Set sCodeDesc = 'DESCRIPTION2'
.head 8 +  If fCount = NUMBER_Null 
.head 9 -  Set fCount = C_Count_State ( )
.head 7 +  Else If sCodeAmt = 'D'
.head 8 -  Set sCodeAmt = 'CITY'
.head 8 -  Set sCodeDesc = 'DESCRIPTION'
.head 8 +  If fCount = NUMBER_Null 
.head 9 -  Set fCount = C_Count_City ( )
.head 7 +  Else 
.head 8 -  Call C_Error( 'Primary charge not found.' )
.head 8 -  Return FALSE
.head 7 +  If fCity_State_Code != ''
.head 8 -  Set sWhereC2S2Code = ' and '||sCodeAmt||" = '"||fCity_State_Code||"' "
.head 7 +  Else 
.head 8 -  Set sWhereC2S2Code = ' '
.head 7 -  Set sInsertCost = "INSERT INTO		cr_costs
				(caseyr, casety, caseno, description,
				 timestamped, title, costs, userid, ratnum)
			SELECT 		:fCaseYr, :fCaseTy, :fCaseNo, "|| sCodeDesc || ",
					:dCurrentDate ," || sCodeAmt || ", ((1 *cost) + ((:fCount - 1) * costx)), :nUserId, cr_costs_ratnum_seq.nextval
			 FROM		dock_code_costs
			 WHERE		code =:fCode and 
					((effective_end_date is null and effective_beg_date <= sysdate) or
                                			(effective_end_date >= sysdate and effective_beg_date < sysdate))"||' '
					||sWhereC2S2Code
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, sInsertCost)
.head 7 +  If bCommit
.head 8 -  Call C_Commit( )
.head 5 +  Function: C_Get_Primary_Charge_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'C'    -      STATE
		 'D'   -        CITY
		 '~'   -         NO PRIMARY CHARGE EXISTS
.head 6 +  Returns 
.head 7 -  String: sChargeType
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  String: sChargeType
.head 6 +  Actions 
.head 7 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	cr_charge
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  Set sChargeType = '~'
.head 7 -  Return sChargeType
.head 5 +  Function: C_Error
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: fErrorType
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 -  Call SalMessageBox( fErrorType, 'Error in Inserting Cost', MB_Ok )
.head 5 +  Function: C_Count_City
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: nCountCity
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountCity
.head 6 +  Actions 
.head 7 -  Set sSelect = "SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 -  Return nCountCity
.head 5 +  Function: C_Count_State
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: nCountState
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountState
.head 6 +  Actions 
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountState
.head 5 +  Function: C_Count_All
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Number: nCountAll
.head 6 +  Parameters 
.head 7 -  String: fCode
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountAll
.head 7 -  String: fCondition
.head 6 +  Actions 
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	condition
			               FROM	dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCondition')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If fCondition = ''
.head 8 -  Set fCondition = '1=1'
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 +  If nCountAll < 1
.head 8 -  ! Call C_Error( 'No Charges found for Case.' )
.head 8 -  Return FALSE
.head 7 +  Else 
.head 8 -  Return nCountAll
.head 5 +  Function: C_Count_Charge
.head 6 -  Description: -Count # of specific type of charge
	-Parameter is the type of charge ('MM', 'M1',...)
	-Returns count
.head 6 +  Returns 
.head 7 -  Number: nCountAll
.head 6 +  Parameters 
.head 7 -  String: fChargeType
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Number: nCountCharge
.head 6 +  Actions 
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		cr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountCharge
.head 5 +  Function: Costs_Constructor
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: CaseYr
.head 7 -  String: CaseTy
.head 7 -  String: CaseNo
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 +  If bLogin = TRUE
.head 8 +  If bCostClass = FALSE
.head 9 -  Set bCostClass = SqlConnect( hSqlCost )
.head 9 -  Call SqlSetResultSet( hSqlCost, TRUE )
.head 8 -  Set nFetchResult = 0
.head 8 -  Set sSelect = ''
.head 8 -  Set fCaseYr = CaseYr
.head 8 -  Set fCaseNo = CaseNo
.head 8 -  Set fCaseTy = CaseTy
.head 5 +  Function: Costs_Destructor
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 +  If bCostClass = TRUE
.head 8 -  Call SqlDisconnect( hSqlCost )
.head 5 +  Function: C_Is_Moving
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  String: sMoving
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  String: sMoving
.head 6 +  Actions 
.head 7 -  Set sSelect = 'SELECT 	s2.moving
	       FROM		cr_charge s1,
			crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 5 +  Function: C_Cost_Exists
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: fCode	!!! Cost Code to Check for
		Pass in the code with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('N%', '', NUMBER_Null)
				The above will find all costs for the case where the
				title is like 'N%'
.head 7 -  String: fDesc	!!! Cost Description to check for
		  Pass in the description with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('NG', ':%TRIAL', NUMBER_Null)
				The above will find all costs for the case where the
				title is 'NG' and the description is like '%TRIAL'
.head 7 -  Number: fCost	!!! Cost Amount to check for
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  String: fWhere
.head 6 +  Actions 
.head 7 -  Set fWhere = 'WHERE	caseno = :fCaseNo and
			casety = :fCaseTy and
			caseyr = :fCaseYr  '
.head 7 +  If fCode != ''
.head 8 +  If SalStrLeftX(fCode, 1) = ':'
.head 9 -  Set fCode = SalStrReplaceX(fCode, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and title like :fCode '
.head 8 +  Else 
.head 9 -  Set fWhere = fWhere ||' and title = :fCode '
.head 7 +  If fDesc != ''
.head 8 +  If SalStrLeftX(fDesc, 1) = ':'
.head 9 -  Set fDesc = SalStrReplaceX(fDesc, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and description like :fDesc '
.head 8 +  Else 
.head 9 -  Set fWhere = fWhere ||' and description = :fDesc '
.head 7 +  If fCost != NUMBER_Null
.head 8 -  Set fWhere = fWhere ||' and costs = :fCost '
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, 'SELECT	caseno
			      FROM		cr_costs '||fWhere)
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else 
.head 8 -  Return FALSE
.head 5 +  Function: C_Commit
.head 6 -  Description: 
.head 6 +  Returns 
.head 7 -  Boolean: bReturn
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Boolean: bReturn
.head 6 +  Actions 
.head 7 -  Set bReturn  = SqlCommit( hSqlCost )
.head 7 -  Return bReturn
.head 3 +  Functional Class: CrDocket
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 4 +  Functions
.head 5 +  Function: D_Docket_Exists
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute (hSqlOne, 'SELECT	caseno
			      FROM		jcr_docket '||fWhere)
.head 7 -  Call SqlFetchNext( hSqlOne, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: D_Delete_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSqlOne,'DELETE		jcr_docket '||fWhere)
.head 7 -  Call SqlCommit( hSqlOne )
.head 5 +  Function: D_Update_Docket
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: fCode
.head 7 -  String: fSeq
.head 7 -  Date/Time: fDDate
.head 7 -  String: fDData
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fnSeq
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :DCaseNo and
			casety = :DCaseTy and
			caseyr = :DCaseYr  and
			casecode = :fCode '
.head 7 +  If fSeq = '#'
.head 8 -  Set fnSeq = D_Get_Seq(fCode)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  Else If (fSeq != '#' and fSeq != '')
.head 8 -  Set fnSeq = SalStrToNumber(fSeq)
.head 8 -  Set fWhere = fWhere ||' and seq = :fnSeq '
.head 7 +  If fDDate != DATETIME_Null
.head 8 -  Set fWhere = fWhere ||' and dock_date = :fDDate '
.head 7 -  Call SqlPrepareAndExecute(hSqlOne,'UPDATE	jcr_docket
			    SET		data =:fDData '||fWhere)
.head 7 -  Call SqlCommit( hSqlOne )
.head 5 +  Function: D_Get_Max_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 6 +  Static Variables
.head 7 -  Number: fReturnSeq
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSqlOne, 'SELECT	max(seq)
			             FROM	jcr_docket
			             WHERE	caseno =:DCaseNo and
					casety =:DCaseTy and
					caseyr =:DCaseYr
			              INTO	:fReturnSeq' )
.head 7 +  If Not SqlFetchNext( hSqlOne, nReturn )
.head 8 -  Set fReturnSeq = 1
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Seq
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSqlOne, 'SELECT	seq
			     FROM		jcr_docket_Codes
			     INTO		:fReturnSeq
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSqlOne, nFetchResult)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Decode_Mask
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnSeq
.head 6 +  Parameters
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  String: DCode
.head 7 -  String: DMask
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnSeq
.head 6 +  Actions
.head 7 +  ! If DMask = ''
.head 8 -  Set fReturnSeq = D_Get_Max_Seq( DCaseNo, DCaseTy, DCaseYr)
.head 8 -  Set fReturnSeq = fReturnSeq + 4
.head 7 +  If DMask = '#'
.head 8 -  Set fReturnSeq = D_Get_Seq(DCode)
.head 7 +  Else If DMask = ''
.head 8 -  Set fReturnSeq = fReturnSeq
.head 7 +  Else
.head 8 -  Set DMask = SalStrReplaceX( DMask, 0, 1, '')
.head 8 +  If SalStrIsValidNumber( DMask )
.head 9 +  If SalStrToNumber(DMask) > 0
.head 10 -  Set fReturnSeq = D_Get_Max_Seq( DCaseYr, DCaseTy, DCaseNo)
.head 10 -  Set fReturnSeq = fReturnSeq + SalStrToNumber(DMask)
.head 8 +  Else
.head 9 -  Call SalMessageBox('Error reading Mask string', 'Invalid Mask', MB_Ok|MB_IconExclamation)
.head 7 -  Return fReturnSeq
.head 5 +  Function: D_Get_Data
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSqlOne, 'SELECT	description
			     FROM		jcr_docket_Codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSqlOne, nFetchResult)
.head 7 -  Return fReturnData
.head 5 +  Function: D_Get_Costs
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: fReturnData
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: fReturnData
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute(hSqlOne, 'SELECT	costs
			     FROM		jcr_docket_Codes
			     INTO		:fReturnData
			     WHERE	code =:fCode')
.head 7 -  Call SqlFetchNext(hSqlOne, nFetchResult)
.head 7 -  Return fReturnData
.head 5 +  Function: D_Insert_Docket
.head 6 -  Description: Adds the Requested Data to the jcr_docket
	- String (4):		Case Yr
	- String (3):		Case Ty
	- String (5):		Case No
	- Date/Time:	Docket Date
	- String (6):		Docket Code
	- String (250):	Docket Data
	- Number:		Seq (If NULL, will find max seq from Docket)
	- Number:		Seq2 (Always NULL, except on charge entry)
	- String:		Variable Mask
	- String:		Commit confirmation
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: DId
.head 7 -  String: DCaseYr
.head 7 -  String: DCaseTy
.head 7 -  String: DCaseNo
.head 7 -  Date/Time: DDate
.head 7 -  String: DCode
.head 7 -  Long String: DData
.head 7 -  Number: nSeq
.head 7 -  Number: nSeq2
.head 7 -  String: DMask
.head 7 -  String: sFormalCode
.head 7 -  String: sCommit
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nReturnSeq
.head 7 -  ! Supreme Court Variables
.head 7 -  String: sAJIJType
.head 7 -  Number: nAJC
.head 6 +  Actions
.head 7 -  Set nReturnSeq =  D_Decode_Mask( DCaseYr, DCaseTy, DCaseNo, DCode, DMask)
.head 7 +  If nReturnSeq != 0
.head 8 -  Set nSeq = nReturnSeq
.head 7 +  If nSeq = 0 or nSeq = NUMBER_Null
.head 8 -  Call SalMessageBox( 'Warning: ' || DCode || ' Docket Code is being inserted without a sequence number', 'Docket Warning', MB_IconInformation | MB_Ok )
.head 7 -  ! ! ! !!! INSERT SUPREME COURT INFORMATION
.head 7 -  Call SqlPrepareAndExecute (hSqlOne, 'Select AJC, AJIJType
	from jcr_docket_codes into :nAJC, :sAJIJType  where Code=:DCode'  )
.head 7 +  If SqlFetchNext(hSqlOne,nResult)
.head 8 +  If sAJIJType != ''
.head 9 -  Call UpdateSupreme(DCaseYr, DCaseTy, DCaseNo, '', nAJC, sAJIJType, DATETIME_Null, '', hSqlOne , TRUE, 'DOCK_CLASS_INSERT')
.head 7 -  Call SqlPrepareAndExecute (hSqlOne, 'Select AJC2, AJIJType2
	from jcr_docket_codes into :nAJC, :sAJIJType  where Code=:DCode'  )
.head 7 +  If SqlFetchNext(hSqlOne,nResult)
.head 8 +  If sAJIJType != ''
.head 9 -  Call UpdateSupreme(DCaseYr, DCaseTy, DCaseNo, '', nAJC, sAJIJType, DATETIME_Null, '', hSqlOne , TRUE, 'DOCK_CLASS_INSERT')
.head 7 -  ! INSERT DOCKET RECORD
.head 7 -  Call SqlPrepareAndExecute( hSqlOne, 'INSERT INTO	jcr_docket
						(id, caseyr, casety, caseno, dock_date, seq,
						 seq2 ,casecode, data, Accesskey, formal_code )
				VALUES		(:DId, :DCaseYr, :DCaseTy, :DCaseNo, :DDate, :nSeq,
						 :nSeq2, :DCode, :DData, sysdate, :sFormalCode )')
.head 7 +  If sCommit != 'N'
.head 8 -  Call SqlCommit( hSqlOne )
.head 3 +  Data Field Class: cDateHoliday
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:
.head 6 -  Width:  1.12"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: Class Default
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: MM-dd-yyyy
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  ToolTip:
.head 4 -  Description: Checks to see if a date is a holiday
Parameter 1 - Date to Check
Parameter 2 - If True a Message is Displayed if the Date is a Holiday
Parameter 3 - If True Saturday & Sunday are flagged as Holidays
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 -  Functions
.head 4 +  Message Actions
.head 5 +  On SAM_Validate
.head 6 +  If SalIsValidDateTime( MyValue )
.head 7 +  If MyValue < SalDateCurrent( ) - 1080 or MyValue > SalDateCurrent( ) + 1080
.head 8 +  If IDOK =  SalMessageBox( 'Date is more than three years before or after the current date, continue?', 'Date Error!', MB_OkCancel|MB_IconExclamation )
.head 9 -  Return VALIDATE_Ok
.head 8 +  Else
.head 9 -  Call SalSetFocus( MyValue )
.head 7 +  If HolidayCheck(MyValue, TRUE, TRUE)
.head 8 -  Call SalSetFocus(MyValue)
.head 7 +  Else
.head 8 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_Backslash
.head 7 +  If MyValue = DATETIME_Null
.head 8 -  Set MyValue = Current_Date ( 'Date' )
.head 7 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 7 -  Call SalSetFocus( SalGetNextChild( MyValue, TYPE_Any ) )
.head 6 +  Else If wParam = Key_Up
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue + 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 6 +  Else If wParam = Key_Down
.head 7 +  If MyValue != DATETIME_Null
.head 8 -  Set MyValue = MyValue - 1
.head 8 -  Call SalSetFieldEdit( MyValue, TRUE )
.head 8 -  Call SalSendMsg(hWndItem, SAM_Validate, 0, 0)
.head 3 +  Functional Class: Costs
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nFetchResult
.head 5 -  String: sSelect
.head 5 -  Sql Handle: hSqlCost
.head 5 -  Boolean: bCostClass
.head 5 -  String: fId
.head 5 -  String: fCaseYr
.head 5 -  String: fCaseNo
.head 5 -  String: fCaseTy
.head 4 +  Functions
.head 5 +  Function: C_Insert_Costs
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fCode
.head 7 -  Number: fCount
.head 7 -  String: fCity_State_Code	!!! This allows only 1 part of the group to be inserted
.head 7 -  Boolean: bCommit
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Date/Time: dCurrentDate
.head 7 -  String: sCodeAmt
.head 7 -  String: sCodeDesc
.head 7 -  String: sInsertCost
.head 7 -  String: sWhereC2S2Code   !!! Used to only select 1 code out of the group when inserting
.head 6 +  Actions
.head 7 -  Set fCode = SalStrTrimX( fCode )
.head 7 -  Set dCurrentDate = Current_Date( 'Date' )
.head 7 -  Set sCodeAmt = C_Get_Primary_Charge_Type( )
.head 7 +  If sCodeAmt = 'C'
.head 8 -  Set sCodeAmt = 'STATE'
.head 8 -  Set sCodeDesc = 'DESCRIPTION2'
.head 8 +  If fCount = NUMBER_Null
.head 9 -  Set fCount = C_Count_State ( )
.head 7 +  Else If sCodeAmt = 'D'
.head 8 -  Set sCodeAmt = 'CITY'
.head 8 -  Set sCodeDesc = 'DESCRIPTION'
.head 8 +  If fCount = NUMBER_Null
.head 9 -  Set fCount = C_Count_City ( )
.head 7 +  Else
.head 8 -  Call C_Error( 'Primary charge not found.' )
.head 8 -  Return FALSE
.head 7 +  If fCity_State_Code != ''
.head 8 -  Set sWhereC2S2Code = ' and '||sCodeAmt||" = '"||fCity_State_Code||"' "
.head 7 +  Else
.head 8 -  Set sWhereC2S2Code = ' '
.head 7 -  Set sInsertCost = "INSERT INTO		jcr_costs
				(id, caseyr, casety, caseno, describe,
				 timestamped, dock_date, title, costs, ratnum)
			SELECT 		:fId, :fCaseYr, :fCaseTy, :fCaseNo, "|| sCodeDesc || ",
					:dCurrentDate , to_date(:dCurrentDate), " || sCodeAmt || ", ((1 *cost) + ((:fCount - 1) * costx)), jcr_costs_ratnum_seq.nextval
			 FROM		jcr_dock_code_costs
			 WHERE		code =:fCode and
					((effective_end_date is null and effective_beg_date <= sysdate) or
                                			(effective_end_date >= sysdate and effective_beg_date < sysdate))"||' '
					||sWhereC2S2Code
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, sInsertCost)
.head 7 +  If bCommit
.head 8 -  Call C_Commit( )
.head 5 +  Function: C_Get_Primary_Charge_Type
.head 6 -  Description: Function  to return the type of the primary charge:
	Returns:      'C'    -      STATE
		 'D'   -        CITY
		 '~'   -         NO PRIMARY CHARGE EXISTS
.head 6 +  Returns
.head 7 -  String: sChargeType
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sChargeType
.head 6 +  Actions
.head 7 -  Call SqlPrepare(hSqlCost,'SELECT	type
		          FROM	jcr_charge
		          INTO		:sChargeType
		          WHERE	caseyr  = :fCaseYr and
				casety   = :fCaseTy and
				caseno = :fCaseNo and
				chargeno = 1')
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  If NOT SqlFetchNext(hSqlCost, nFetchResult)
.head 8 -  Set sChargeType = '~'
.head 7 -  Return sChargeType
.head 5 +  Function: C_Error
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fErrorType
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SalMessageBox( fErrorType, 'Error in Inserting Cost', MB_Ok )
.head 5 +  Function: C_Count_City
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountCity
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCity
.head 6 +  Actions
.head 7 -  Set sSelect = "SELECT	count(*)
	       FROM		jcr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'D\'
	      INTO		:nCountCity"
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext( hSqlCost,nFetchResult)
.head 7 -  Return nCountCity
.head 5 +  Function: C_Count_State
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountState
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountState
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		jcr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			type  = \'C\'
	      INTO		:nCountState'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountState
.head 5 +  Function: C_Count_All
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fCode
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountAll
.head 7 -  String: fCondition
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlCost, 'SELECT 	condition
			               FROM	jcr_dock_code_costs
			               WHERE	code =:fCode
   			               INTO	:fCondition')
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If fCondition = ''
.head 8 -  Set fCondition = '1=1'
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		jcr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and '||fCondition||'
	      INTO		:nCountAll'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 +  If nCountAll < 1
.head 8 -  ! Call C_Error( 'No Charges found for Case.' )
.head 8 -  Return FALSE
.head 7 +  Else
.head 8 -  Return nCountAll
.head 5 +  Function: C_Count_Charge
.head 6 -  Description: -Count # of specific type of charge
	-Parameter is the type of charge ('MM', 'M1',...)
	-Returns count
.head 6 +  Returns
.head 7 -  Number: nCountAll
.head 6 +  Parameters
.head 7 -  String: fChargeType
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCountCharge
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT	count(*)
	       FROM		jcr_charge
	       WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno =:fCaseNo and
			degree =:fChargeType
	      INTO		:nCountCharge'
.head 7 -  Call SqlPrepareAndExecute(hSqlCost, sSelect)
.head 7 -  Call SqlFetchNext(hSqlCost,nFetchResult)
.head 7 -  Return nCountCharge
.head 5 +  Function: Costs_Constructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: Id
.head 7 -  String: CaseYr
.head 7 -  String: CaseTy
.head 7 -  String: CaseNo
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bCostClass = FALSE
.head 8 -  Set bCostClass = SqlConnect( hSqlCost )
.head 8 -  Call SqlSetResultSet( hSqlCost, TRUE )
.head 7 -  Set nFetchResult = 0
.head 7 -  Set sSelect = ''
.head 7 -  Set fId = Id
.head 7 -  Set fCaseYr = CaseYr
.head 7 -  Set fCaseNo = CaseNo
.head 7 -  Set fCaseTy = CaseTy
.head 5 +  Function: Costs_Destructor
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If bCostClass = TRUE
.head 8 -  Call SqlDisconnect( hSqlCost )
.head 5 +  Function: C_Is_Moving
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String: sMoving
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sMoving
.head 6 +  Actions
.head 7 -  Set sSelect = 'SELECT 	s2.moving
	       FROM		jcr_charge s1,
			jcr_crord s2
	      WHERE	caseyr = :fCaseYr and
			casety = :fCaseTy and
			caseno = :fCaseNo and
			s1.statute = s2.statute
	       INTO		:sMoving
	       ORDER BY	s1.statute  desc'
.head 7 -  Call SqlPrepare(hSqlCost, sSelect)
.head 7 -  Call SqlExecute(hSqlCost)
.head 7 +  While SqlFetchNext(hSqlCost, nFetchResult)
.head 8 +  If sMoving = 'Y'
.head 9 -  Return sMoving
.head 5 +  Function: C_Cost_Exists
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: fCode	!!! Cost Code to Check for
		Pass in the code with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('N%', '', NUMBER_Null)
				The above will find all costs for the case where the
				title is like 'N%'
.head 7 -  String: fDesc	!!! Cost Description to check for
		  Pass in the description with a colon appended to the front
		  to do a "like" operation
			Example: C_Cost_Exists('NG', ':%TRIAL', NUMBER_Null)
				The above will find all costs for the case where the
				title is 'NG' and the description is like '%TRIAL'
.head 7 -  Number: fCost	!!! Cost Amount to check for
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: fWhere
.head 6 +  Actions
.head 7 -  Set fWhere = 'WHERE	caseno = :fCaseNo and
			casety = :fCaseTy and
			caseyr = :fCaseYr  '
.head 7 +  If fCode != ''
.head 8 +  If SalStrLeftX(fCode, 1) = ':'
.head 9 -  Set fCode = SalStrReplaceX(fCode, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and title like :fCode '
.head 8 +  Else
.head 9 -  Set fWhere = fWhere ||' and title = :fCode '
.head 7 +  If fDesc != ''
.head 8 +  If SalStrLeftX(fDesc, 1) = ':'
.head 9 -  Set fDesc = SalStrReplaceX(fDesc, 0,1, '')
.head 9 -  Set fWhere = fWhere ||' and description like :fDesc '
.head 8 +  Else
.head 9 -  Set fWhere = fWhere ||' and description = :fDesc '
.head 7 +  If fCost != NUMBER_Null
.head 8 -  Set fWhere = fWhere ||' and costs = :fCost '
.head 7 -  Call SqlPrepareAndExecute (hSqlCost, 'SELECT	caseno
			      FROM		jcr_costs '||fWhere)
.head 7 -  Call SqlFetchNext( hSqlCost, nFetchResult )
.head 7 +  If nFetchResult = FETCH_Ok
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: C_Commit
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean: bReturn
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bReturn
.head 6 +  Actions
.head 7 -  Set bReturn  = SqlCommit( hSqlCost )
.head 7 -  Return bReturn
.head 3 +  Functional Class: cService
.head 4 -  Description: Class to manage service done on a case
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 -  Instance Variables
.head 4 +  Functions
.head 5 +  Function: Print_CertLabel
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Number: nSeq
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT
	first_name || ' ' || DECODE( mid_name, NULL, '', mid_name || ' ' ) || last_name || DECODE( title, NULL, '', ' ' || title ),
	address, address2, city || ', ' || state || zip, dated, service_number
INTO
	:Reports.CertLabel.sName, :Reports.CertLabel.sAddress1, :Reports.CertLabel.sAddress2, :Reports.CertLabel.sAddress3, :Reports.CertLabel.dDated, :Reports.CertLabel.sServiceNo
FROM
	jcr_service
WHERE
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND service_seq = :nSeq" )
.head 7 +  If Not SqlFetchNext( hSqlTwo, nResult )
.head 8 -  Return Error( "Can't print certified label.  Service record doesn't exist." )
.head 7 +  If Reports.CertLabel.sAddress2 = STRING_Null
.head 8 -  Set Reports.CertLabel.sAddress2 = Reports.CertLabel.sAddress3
.head 8 -  Set Reports.CertLabel.sAddress3 = STRING_Null
.head 7 -  Set Reports.CertLabel.sServiceNoBarcode = "*" || Reports.CertLabel.sServiceNo || "*"
.head 7 -  Call Reports.Set_Printer( 'ADD_LABEL', 'winspool', 'USB001' )
.head 7 -  Call Reports.Print_Report( "CERTLABEL2.QRP", 
"Reports.CertLabel.sName, Reports.CertLabel.sAddress1, Reports.CertLabel.sAddress2, 
Reports.CertLabel.sAddress3, Reports.CertLabel.sServiceNoBarcode, Reports.CertLabel.sServiceNo, 
Reports.CertLabel.dDated, Reports.CertLabel.sCaseNo",
"NAME, ADD1, ADD2, ADD3, BAR, REF, DATE, CASE", 1 )
.head 7 -  Call Reports.Reset_Printer(  )
.head 5 +  Function: Print_Label
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Number: nSeq
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT
	first_name || ' ' || DECODE( mid_name, NULL, '', mid_name || ' ' ) || last_name || DECODE( title, NULL, '', ' ' || title ),
	address, address2, city || ', ' || state || zip, dated, service_number
INTO
	:Reports.CertLabel.sName, :Reports.CertLabel.sAddress1, :Reports.CertLabel.sAddress2, :Reports.CertLabel.sAddress3, :Reports.CertLabel.dDated, :Reports.CertLabel.sServiceNo
FROM
	jcr_service
WHERE
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND service_seq = :nSeq" )
.head 7 +  If Not SqlFetchNext( hSqlTwo, nResult )
.head 8 -  Return Error( "Can't print label.  Service record doesn't exist." )
.head 7 +  If Reports.CertLabel.sAddress2 = STRING_Null
.head 8 -  Set Reports.CertLabel.sAddress2 = Reports.CertLabel.sAddress3
.head 8 -  Set Reports.CertLabel.sAddress3 = STRING_Null
.head 7 -  Set Reports.CertLabel.sServiceNo = STRING_Null
.head 7 -  Set Reports.CertLabel.sServiceNoBarcode = STRING_Null
.head 7 -  Call Reports.Set_Printer( 'ADD_LABEL', 'winspool', 'USB001' )
.head 7 -  Call Reports.Print_Report( "CERTLABEL2.QRP", 
"Reports.CertLabel.sName, Reports.CertLabel.sAddress1, Reports.CertLabel.sAddress2, 
Reports.CertLabel.sAddress3, Reports.CertLabel.sServiceNoBarcode, Reports.CertLabel.sServiceNo, 
Reports.CertLabel.dDated, Reports.CertLabel.sCaseNo",
"NAME, ADD1, ADD2, ADD3, BAR, REF, DATE, CASE", 1 )
.head 7 -  Call Reports.Reset_Printer(  )
.head 5 +  Function: Print_Certificate
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Number: nSeq
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT
	first_name || ' ' || DECODE( mid_name, NULL, '', mid_name || ' ' ) || last_name || DECODE( title, NULL, '', ' ' || title ),
	address, address2, city || ', ' || state || zip, dated, service_number
INTO
	:Reports.CertLabel.sName, :Reports.CertLabel.sAddress1, :Reports.CertLabel.sAddress2, :Reports.CertLabel.sAddress3, :Reports.CertLabel.dDated, :Reports.CertLabel.sServiceNo
FROM
	jcr_service
WHERE
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND service_seq = :nSeq" )
.head 7 +  If Not SqlFetchNext( hSqlTwo, nResult )
.head 8 -  Return Error( "Can't print certificate of mailing.  Service record doesn't exist." )
.head 7 +  If Reports.CertLabel.sAddress2 = STRING_Null
.head 8 -  Set Reports.CertLabel.sAddress2 = Reports.CertLabel.sAddress3
.head 8 -  Set Reports.CertLabel.sAddress3 = STRING_Null
.head 7 -  Set Reports.CertLabel.sServiceNoBarcode = "*" || Reports.CertLabel.sServiceNo || "*"
.head 7 -  Call Reports.Set_Printer( "CLERK_DRCERTM", "WINSPOOL", "LPT3" )
.head 7 -  Call Reports.Print_Report( "MAIL_CERT.QRP", "Reports.CertLabel.sName, Reports.CertLabel.sAddress1, Reports.CertLabel.sAddress2, Reports.CertLabel.sAddress3, Reports.CertLabel.sCaseNo","FULLNAME, ADDRESS1, ADDRESS2, 
SUBADDRESS, CASENUMBER", 1 )
.head 7 -  Call Reports.Reset_Printer(  )
.head 5 +  Function: Insert_Service
.head 6 -  Description: Examples:
Insert_Service( '2006', 'JCR', '00001', frmMain.dfDate, "SUMMONS", "FOREIGN SHERIFF", "Chris", "A", "Yates", "AKA Red Dog", "1608 19th St NW", "", "Canton", "OH", "44709", NUMBER_Null, "CUYAHOGA" )
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sID
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Date/Time: dDated
.head 7 -  String: sDocument
.head 7 -  String: sType
.head 7 -  String: sFirstName
.head 7 -  String: sMidName
.head 7 -  String: sLastName
.head 7 -  String: sTitle
.head 7 -  String: sAddress1
.head 7 -  String: sAddress2
.head 7 -  String: sCity
.head 7 -  String: sState
.head 7 -  String: sZip
.head 7 -  String: sZip2
.head 7 -  Number: nCost
.head 7 -  String: sForeignSheriff
.head 7 -  Receive String: sServiceNo
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nSeq
.head 7 -  String: sSeq
.head 6 +  Actions
.head 7 +  If sCaseYr = STRING_Null Or sCaseTy = STRING_Null or sCaseNo = STRING_Null
.head 8 -  Return Error( "Attempted to insert service with an invalid case number." )
.head 7 +  If sID = STRING_Null
.head 8 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT id INTO :sID FROM jcr_master WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo" )
.head 8 +  If Not SqlFetchNext( hSqlTwo, nResult )
.head 9 -  Return Error( "Attempted to insert service with a case number that was not found." )
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT NVL( MAX( service_seq ) + 1, 0 ) INTO :nSeq FROM jcr_service WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Call SalNumberToStr( nSeq, 0, sSeq )
.head 7 -  Set sSeq = '0000' || sSeq
.head 7 +  If SalStrLength( sCaseNo ) = 6
.head 8 -  Call SalStrRight( sSeq, 2, sSeq )
.head 7 +  Else
.head 8 -  Call SalStrRight( sSeq, 3, sSeq )
.head 7 -  Set sServiceNo = sZip || sCaseYr || SalStrLeftX( sCaseTy, 2 ) || sCaseNo || sSeq
.head 7 -  ! Set sServiceNo = sServiceNo || '0000000000000000000'
.head 7 -  Set Reports.CertLabel.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SalStrLeft( sServiceNo, 20, sServiceNo )
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "INSERT INTO jcr_service (
	id, caseyr, casety, caseno, dated, document, type, first_name, mid_name, 
	last_name, title, address, address2, city, state, zip, zip2, cost, service_number, service_seq, fsheriff
) VALUES (
	:sID, :sCaseYr, :sCaseTy, :sCaseNo, :dDated, :sDocument, :sType, :sFirstName, :sMidName, 
	:sLastName, :sTitle, :sAddress1, :sAddress2, :sCity, :sState, :sZip, :sZip2, :nCost, :sServiceNo, :nSeq, :sForeignSheriff )" )
.head 7 +  If sType = "CERTIFIED"
.head 8 -  Call Print_CertLabel( sCaseYr, sCaseTy, sCaseNo, nSeq )
.head 7 +  If sType = "ORDINARY" Or sType = "REGULAR"
.head 8 -  Call Insert_Docket( sCaseYr, sCaseTy, sCaseNo, "CERTM", STRING_Null, dDated )
.head 8 -  Call Print_Certificate( sCaseYr, sCaseTy, sCaseNo, nSeq )
.head 8 -  Call Print_Label( sCaseYr, sCaseTy, sCaseNo, nSeq )
.head 7 -  Return TRUE
.head 3 +  Functional Class: cReports
.head 4 -  Description:
.head 4 -  Derived From
.head 4 +  Class Variables
.head 5 -  String: sCourt
.head 5 -  String: sCounty
.head 5 -  String: sClerk
.head 5 -  Boolean: bPrinting
.head 5 -  String: sCurReport
.head 5 -  String: sDefaultDevice
.head 5 -  String: sDefaultDriver
.head 5 -  String: sDefaultPort
.head 5 -  Boolean: bDebugView
.head 4 +  Instance Variables
.head 5 -  FunctionalVar: Warrant
.head 6 -  Class: cWarrantData
.head 5 -  FunctionalVar: Summons
.head 6 -  Class: cSummonsData
.head 5 -  String: sDialog
.head 5 -  Window Handle: hDialog
.head 5 -  FunctionalVar: CertLabel
.head 6 -  Class: cCertLabelData
.head 5 -  FunctionalVar: DFA
.head 6 -  Class: cDFAData
.head 5 -  FunctionalVar: DRPApp
.head 6 -  Class: cDRPAppData
.head 5 -  FunctionalVar: DRPJE
.head 6 -  Class: cDRPJEData
.head 5 -  FunctionalVar: DRP
.head 6 -  Class: cDRPData
.head 5 -  FunctionalVar: PP
.head 6 -  Class: cPPData
.head 5 -  FunctionalVar: DFP
.head 6 -  Class: cDFPData
.head 5 -  FunctionalVar: AL
.head 6 -  Class: cALData
.head 5 -  FunctionalVar: PhotoID
.head 6 -  Class: cPhotoIDData
.head 5 -  FunctionalVar: LicRec
.head 6 -  Class: cLicRecData
.head 5 -  FunctionalVar: DDCList
.head 6 -  Class: cDDCListData
.head 5 -  FunctionalVar: TransferJE
.head 6 -  Class: cTransferJEData
.head 5 -  FunctionalVar: JudgeAdopt
.head 6 -  Class: cJudgeAdoptData
.head 5 -  ! CRAXDRT_Application: repapp
.head 5 -  ! CRAXDRT_Report: rep
.head 5 -  FunctionalVar: Referral
.head 6 -  Class: cReferralData
.head 5 -  FunctionalVar: CommitRelease
.head 6 -  Class: cCommitReleaseData
.head 5 -  FunctionalVar: Waiver
.head 6 -  Class: cWaiverData
.head 5 -  FunctionalVar: ServiceNotice
.head 6 -  Class: cServiceNoticeData
.head 4 +  Functions
.head 5 +  Function: Print_Warrant
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sID
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Date/Time: dIssued
.head 7 -  String: sType
.head 7 -  String: sReason
.head 7 -  String: sAgency
.head 7 -  Number: nCopies
.head 7 -  Number: nServiceCopies
.head 7 -  Boolean: bAfterArrest
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sCharges
.head 7 -  String: sStatute
.head 7 -  String: sDegree
.head 7 -  String: sCharge
.head 7 -  Number: nCount
.head 7 -  String: sParty
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 7 -  String: sSSN
.head 6 +  Actions
.head 7 -  Set Warrant.sCaseNumber = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, id.fname || ' ' || DECODE( id.mname, NULL, '', id.mname || ' ' ) || id.lname || DECODE( id.title, NULL, '', ' ' || id.title ), 
	id.sex, id.race, id.height, id.weight, id.dob, id.hair, id.eyes, id.ssno, id.scars,
	a.address1 || ' ' || a.address2 || ' ' || a.city || ', ' || a.state || ' ' || a.zip, j.judgename
INTO
	:sJUType, :Warrant.sDef,
	:Warrant.sSex, :Warrant.sColor, :Warrant.sHeight, :Warrant.nWeight, :Warrant.dDOB, :Warrant.sHair, :Warrant.sEyes, :sSSN, :Warrant.sScars,
	:Warrant.sAddress1, :Warrant.sJudge
FROM
	jcr_master m, id_master id, jcr_address a, judge j
WHERE
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	a.id = m.id AND 
	id.id = m.id AND 
	m.judge = j.judge_number(+)
ORDER BY 
	a.entrydate DESC" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Warrant.sAKA = ""
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' ')
INTO
	:sParty
FROM
	jcr_index i
WHERE
	i.id = :sID AND party_no > 0" )
.head 7 +  While SqlFetchNext( hSqlTwo, nResult )
.head 8 -  Set Warrant.sAKA = Warrant.sAKA || sParty || ", "
.head 7 -  Set Warrant.sAKA = SalStrLeftX( Warrant.sAKA, SalStrLength( Warrant.sAKA ) - 2 )
.head 7 -  Set Warrant.sAlleged = Get_Case_Type( sJUType )
.head 7 -  Set Warrant.nAge = Get_Age( Warrant.dDOB )
.head 7 -  Set Warrant.dIssued = dIssued
.head 7 -  Set Warrant.sAgency = sAgency
.head 7 -  Set sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 +  If SalStrLeftX( sDegree, 1 ) = "F"
.head 8 -  Set Warrant.sWarrType = "FELONY"
.head 7 +  Else
.head 8 -  Set Warrant.sWarrType = "MISDEMEANOR"
.head 7 +  If bAfterArrest
.head 8 -  Set Warrant.sWarrType = "AFTER ARREST"
.head 7 +  If sType = "COMPLAINT"
.head 8 -  Set Warrant.sDescription = 'There has been filed in ' || sCourt || ' a complaint stating that ' || Warrant.sDef || ' has committed the following offenses:

' || sCharges || '

A copy of the complaint is attached.'
.head 7 +  If sType = "BENCH"
.head 8 -  ! Set sReason = SalStrRightX( frmMain.mlDescription, SalStrLength( frmMain.mlDescription ) - 23 )
.head 8 -  Set Warrant.sDescription = 'There has been filed in ' || sCourt || ' a complaint stating that ' || Warrant.sDef || ' has committed the following offenses:

' || sCharges || '

This person has failed to comply with a court order, namely: ' || sReason || '.  Due to this fact the court has ordered a bench warrant to be issued.'
.head 7 -  Set sVars = "Reports.sCourt, Reports.sCounty, Reports.Warrant.sWarrType, Reports.Warrant.sCaseNumber, Reports.Warrant.sDef, Reports.Warrant.sAlleged,
Reports.Warrant.sAgency, Reports.Warrant.sDescription, Reports.Warrant.dIssued, Reports.Warrant.sBond, Reports.sClerk,
Reports.Warrant.sAKA, Reports.Warrant.sAddress1, Reports.Warrant.sAddress2, Reports.Warrant.sSex, Reports.Warrant.sColor,
Reports.Warrant.sHeight, Reports.Warrant.nWeight, Reports.Warrant.nAge, Reports.Warrant.dDOB, Reports.Warrant.sHair,
Reports.Warrant.sEyes, Reports.Warrant.sSSNO, Reports.Warrant.sScars, Reports.Warrant.sCopy, Reports.Warrant.sJudge"
.head 7 -  Set sInputs = "COURT, COUNTY, WARRTYPE, CASENUMBER, DEFENDANT, ALLEGED,
AGENCY, DESCRIPTION, ISSUE, BOND, CLERK,
AKA, ADDRESS1, ADDRESS2, SEX, COLOR,
HEIGHT, WEIGHT, AGE, DOB, HAIR,
EYES, SSNO, SCARS, COPY, JUDGE"
.head 7 +  If bAfterArrest > 0
.head 8 -  Set Warrant.sSSNO = sSSN
.head 8 -  Set Warrant.sCopy = "DEFENDANTS COPY"
.head 8 -  Call Print_Report( "JU_WARRANTPC.QRP", sVars, sInputs, nCopies )
.head 8 -  Set Warrant.sSSNO = ""
.head 8 -  Set Warrant.sCopy = "FILE COPY"
.head 8 -  Call Print_Report( "JU_WARRANTPC.QRP", sVars, sInputs, nCopies )
.head 7 +  Else If nCopies > 0
.head 8 -  Set Warrant.sCopy = "RETURN COPY"
.head 8 -  Set Warrant.sSSNO = ""
.head 8 -  Call Print_Report( "JU_WARRANT.QRP", sVars, sInputs, nCopies )
.head 8 -  Set Warrant.sCopy = "FILE COPY"
.head 8 -  Call Print_Report( "JU_WARRANT.QRP", sVars, sInputs, nCopies )
.head 8 -  Set Warrant.sCopy = "SERVICE COPY"
.head 7 +  If nServiceCopies > 0
.head 8 -  Set Warrant.sSSNO = sSSN
.head 8 -  Call Print_Report( "JU_WARRANT.QRP", sVars, sInputs, nServiceCopies )
.head 5 +  Function: Set_Printer
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sName
.head 7 -  String: sDriver
.head 7 -  String: sPort
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call SalPrtGetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 7 -  Call SalPrtSetDefault( sName, sDriver, sPort )
.head 5 +  Function: Set_Dialog
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: spDialog
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set sDialog = spDialog
.head 5 +  Function: Reset_Printer
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If sDefaultDevice != STRING_Null
.head 8 -  Call SalPrtSetDefault( sDefaultDevice, sDefaultDriver, sDefaultPort )
.head 8 -  Set sDefaultDevice = STRING_Null
.head 8 -  Set sDefaultDriver = STRING_Null
.head 8 -  Set sDefaultPort = STRING_Null
.head 5 +  Function: Print_Summons
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Date/Time: dIssueDate
.head 7 -  Date/Time: dHearingDate
.head 7 -  Number: nCopies
.head 7 -  Number: nPartyRatnum
.head 7 -  Boolean: bReturnCopy
.head 7 -  Boolean: bFileCopy
.head 7 -  Boolean: bServiceCopy
.head 7 -  Boolean: bIntakeCopy
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sCharges
.head 7 -  String: sStatute
.head 7 -  String: sDegree
.head 7 -  String: sCharge
.head 7 -  Number: nCount
.head 7 -  String: sParty
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 7 -  String: sSSN
.head 7 -  Date/Time: dHearing
.head 7 -  Date/Time: tHearing
.head 7 -  Number: nRatnum
.head 6 +  Actions
.head 7 -  Set Summons.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '),
	a.address1 || ' ' || a.address2 || ' ' || a.city || ', ' || a.state || ' ' || a.zip, j.judgename
INTO
	:sJUType, :Summons.sCaption,
	:Summons.sAddress, :Summons.sJudge
FROM
	jcr_master m, jcr_index i, jcr_address a, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	a.ratnum = ( SELECT MAX(ratnum) FROM jcr_address a2 WHERE a2.id = m.id ) AND 
	j.judge_number = m.judge" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Summons.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Summons.sIssueDate = SalFmtFormatDateTime( dIssueDate, "MMMM" ) || " " || Get_Date_Suffix( SalDateDay( dIssueDate ) ) || ", " || SalFmtFormatDateTime( dIssueDate, "yyyy" )
.head 7 +  If dHearingDate = DATETIME_Null
.head 8 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT MIN( dated ) INTO :dHearing FROM jcr_hearing WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND completed IS NULL" )
.head 8 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 8 -  Call SqlPrepareAndExecute( hSqlTwo, "SELECT hearing_time INTO :tHearing FROM jcr_hearing WHERE caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND dated = :dHearing" )
.head 8 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 8 -  Set Summons.sHearingDate = SalFmtFormatDateTime( dHearing, "MMMM" ) || " " || Get_Date_Suffix( SalDateDay( dHearing ) ) || ", " || SalFmtFormatDateTime( dHearing, "yyyy" ) || " AT " || SalFmtFormatDateTime( tHearing, "hh:mm AMPM" )
.head 7 +  Else
.head 8 -  Set Summons.sHearingDate = SalFmtFormatDateTime( dHearingDate, "MMMM" ) || " " || Get_Date_Suffix( SalDateDay( dHearingDate ) ) || ", " || SalFmtFormatDateTime( dHearingDate, "yyyy" ) || " AT " || 
SalFmtFormatDateTime( dHearingDate, "hh:mm AMPM" )
.head 7 -  Set Summons.sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 -  Set sVars = "Reports.Summons.sCaseNo, Reports.Summons.sCaption, Reports.Summons.sJudge, Reports.Summons.sCaseType,
	Reports.Summons.sAddress, Reports.Summons.sCharges, Reports.Summons.sHearingDate, Reports.Summons.sIssueDate, 
	Reports.sClerk, Reports.Summons.sCopy, Reports.Summons.sName"
.head 7 -  Set sInputs = "CASENO, CAPTION, JUDGE, CASE_TYPE,
	ADDRESS, CHARGES, HEARING_DATE, ISSUE_DATE,
	CLERK, COPY, NAME"
.head 7 -  Set Summons.sName = Summons.sCaption
.head 7 +  If nPartyRatnum = NUMBER_Null Or nPartyRatnum = 0
.head 8 +  If bReturnCopy
.head 9 -  Set Summons.sCopy = "RETURN COPY"
.head 9 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 8 +  If bFileCopy
.head 9 -  Set Summons.sCopy = "FILE COPY"
.head 9 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 8 +  If bServiceCopy
.head 9 -  Set Summons.sCopy = "SERVICE COPY"
.head 9 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 8 +  If bIntakeCopy
.head 9 -  Set Summons.sCopy = "INTAKE COPY"
.head 9 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	p.fname || ' ' || DECODE( p.mname, NULL, '', p.mname || ' ' ) || p.lname || DECODE( p.title, NULL, '', p.title || ' '),
	p.address1 || ' ' || p.address2 || ' ' || p.city || ', ' || p.state || ' ' || p.zip, p.ratnum
INTO
	:Summons.sName, 
	:Summons.sAddress, :nRatnum
FROM
	jcr_parties p
WHERE
	p.caseyr = :sCaseYr AND p.casety = :sCaseTy AND p.caseno = :sCaseNo
" )
.head 7 +  While SqlFetchNext( hSqlTwo, nResult )
.head 8 +  If nPartyRatnum = NUMBER_Null Or nPartyRatnum = nRatnum
.head 9 +  If bReturnCopy
.head 10 -  Set Summons.sCopy = "RETURN COPY"
.head 10 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 9 +  If bFileCopy
.head 10 -  Set Summons.sCopy = "FILE COPY"
.head 10 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 9 +  If bServiceCopy
.head 10 -  Set Summons.sCopy = "SERVICE COPY"
.head 10 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 9 +  If bIntakeCopy
.head 10 -  Set Summons.sCopy = "INTAKE COPY"
.head 10 -  Call Print_Report( "JU_SUMMONS.QRP", sVars, sInputs, nCopies )
.head 5 +  Function: Print_Report
.head 6 -  Description: Prints the Report
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: sReport
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 7 -  Number: nCopies
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  ! Set bDebugView = TRUE
.head 7 +  If bDebugView
.head 8 -  Call View_Report( sReport, sVars, sInputs )
.head 8 -  Return TRUE
.head 7 +  If sDialog = STRING_Null
.head 8 -  Set sDialog = "dlgPrintReport"
.head 7 -  ! Wait until done printing before starting the next report
.head 7 +  If bPrinting
.head 8 -  Call SalYieldStartMessages( hDialog )
.head 8 -  While bPrinting
.head 8 -  Call SalYieldStopMessages( )
.head 7 -  Set sCurReport = sReport
.head 7 -  Set hDialog = SalCreateWindow( sDialog, hWndNULL )
.head 7 -  Call SalReportPrint( hDialog, sReportsPath||sReport, sVars, sInputs, nCopies, RPT_PrintAll, 0, 0, nResult )
.head 7 -  Return ( nResult = 0 )
.head 5 +  Function: View_Report
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sReport
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nFlags
.head 7 -  Window Handle: hDialog
.head 6 +  Actions
.head 7 +  If sDialog = STRING_Null
.head 8 -  Set sDialog = "dlgPrintReport"
.head 7 -  ! Wait until done printing before starting the next report
.head 7 +  If bPrinting
.head 8 -  Call SalYieldStartMessages( hDialog )
.head 8 -  While bPrinting
.head 8 -  Call SalYieldStopMessages( )
.head 7 -  Set sCurReport = sReport
.head 7 -  Set hDialog = SalCreateWindow( sDialog, hWndNULL )
.head 7 -  Call SalReportView( hDialog, hWndNULL, sReportsPath || sReport, sVars, sInputs, nFlags )
.head 5 +  Function: Print_DFA
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bViewOnly
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 +  Actions
.head 7 -  Set sVars = "Reports.DFA.sCaseNo, Reports.DFA.sJuvenile, Reports.DFA.nOccurance, Reports.DFA.dArnDate"
.head 7 -  Set sInputs = "CASENO, JUVENILE, OCCURANCE, ARN_DATE"
.head 7 +  If bViewOnly
.head 8 -  Call View_Report( "JU_TRA_DFA.QRP", sVars, sInputs )
.head 7 +  Else
.head 8 -  Call Print_Report( "JU_TRA_DFA.QRP", sVars, sInputs, 1 )
.head 5 +  Function: Print_DFA_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call Print_Report( "JU_TRA_DFA_NOTICE.QRP",
	"Reports.DFA.sCaseNo, Reports.DFA.sJuvenile, Reports.DFA.sHO, Reports.DFA.dArnDate, Reports.DFA.dNextDate",
	"CASENO, CAPTION, HO, ARN_DATE, NEXT_DATE", 2 )
.head 5 +  Function: Print_DRP_App
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call Print_Report( "JU_TRA_PRIV_APP.QRP", 
	"Reports.DRPApp.sCaption, Reports.DRPApp.sCaseType, Reports.DRPApp.sHOType, Reports.DRPApp.sHO, 
	Reports.DRPApp.sCaseNo, Reports.DRPApp.dSuspFrom, Reports.DRPApp.dSuspTo, Reports.DRPApp.sJudge", 
	"CAPTION, CASE_TYPE, HO_TYPE, HO, 
	CASE_NO, SUSP_FROM, SUSP_TO, JUDGE", 1 )
.head 5 +  Function: Print_DRP
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call Print_Report( "JU_TRA_PRIV.QRP", 
	"Reports.DRP.sCaption, Reports.DRP.sCaseType, Reports.DRP.sHOType, Reports.DRP.sHO, 
	Reports.DRP.sCaseNo, Reports.DRP.dBegin, Reports.DRP.dEnd, Reports.DRP.sAddress, 
	Reports.DRP.dDOB, Reports.DRP.sOLNum, Reports.DRP.sSSN, Reports.DRP.sPassengers, 
	Reports.DRP.sLocType, Reports.DRP.sLocName, Reports.DRP.sLocAddress, Reports.DRP.sLocHours, 
	Reports.sClerk, Reports.DRP.nLocNum, Reports.DRP.dTimestamp, Reports.DRP.sJudge, 
	Reports.DRP.sDateSuffix, Reports.DRP.sOtherRestrictions", 
	"CAPTION, CASE_TYPE, HO_TYPE, HO, 
	CASE_NO, BEG_DATE, END_DATE, ADDRESS, 
	DOB, OL_NUM, SSN, PASSENGERS, 
	LOC_TYPE, LOC_NAME, LOC_ADDRESS, LOC_HOURS, 
	CLERK, LOC_NUM, TIMESTAMP, JUDGE, 
	DATE_SUFFIX, OTHER_RESTRICTIONS", 2 )
.head 5 +  Function: Print_PP
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call Print_Report( "JU_PP.QRP", 
	"Reports.PP.sCaption, Reports.PP.sCaseType, Reports.PP.sCaseNo, Reports.PP.sJudge, 
	Reports.PP.nAmount, Reports.PP.sDueDate, Reports.PP.nCosts, Reports.PP.nFines, 
	Reports.PP.nRest, Reports.PP.nOwed, Reports.PP.nReceipts, Reports.PP.nBalance, 
	Reports.sClerk", 
	"CAPTION, CASE_TYPE, CASE_NO, JUDGE, 
	AMOUNT, DUE_DATE, COSTS, FINES, 
	REST, OWED, RECEIPTS, BALANCE, 
	CLERK", 2 )
.head 5 +  Function: Print_DFP
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bViewOnly
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 +  Actions
.head 7 -  Set sVars = "Reports.DFP.sCaseNo, Reports.DFP.sJuvenile, Reports.DFP.dLastPayment, Reports.DFP.nDaysSince, Reports.DFP.nPayAmount, Reports.DFP.nDueDate, Reports.DFP.nBalance"
.head 7 -  Set sInputs = "CASENO, JUVENILE, LAST_PAYMENT, DAYS_SINCE, PAY_AMOUNT, DUE_DATE, BALANCE"
.head 7 +  If bViewOnly
.head 8 -  Call View_Report( "JU_TRA_DFP.QRP", sVars, sInputs )
.head 7 +  Else
.head 8 -  Call Print_Report( "JU_TRA_DFP.QRP", sVars, sInputs, 1 )
.head 5 +  Function: Print_ArraignmentList
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bViewOnly
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 +  Actions
.head 7 -  Set sVars = "Reports.AL.dArnDate, Reports.AL.sCaseNo, Reports.AL.sJuvenile"
.head 7 -  Set sInputs = "ARN_DATE, CASENO, JUVENILE"
.head 7 +  If bViewOnly
.head 8 -  Call View_Report( "JU_ARRAIGNMENT_LIST.QRP", sVars, sInputs )
.head 7 +  Else
.head 8 -  Call Print_Report( "JU_ARRAIGNMENT_LIST.QRP", sVars, sInputs, 1 )
.head 5 +  Function: Print_DDCList
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Boolean: bViewOnly
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sVars
.head 7 -  String: sInputs
.head 6 +  Actions
.head 7 -  Set sVars = "Reports.DDCList.dSchoolDate, Reports.DDCList.sCaseNo, Reports.DDCList.sJuvenile, Reports.DDCList.sAddress"
.head 7 -  Set sInputs = "SCHOOL_DATE, CASENO, JUVENILE, ADDRESS"
.head 7 +  If bViewOnly
.head 8 -  Call View_Report( "JU_DDC_LIST.QRP", sVars, sInputs )
.head 7 +  Else
.head 8 -  Call Print_Report( "JU_DDC_LIST.QRP", sVars, sInputs, 1 )
.head 5 +  ! Function: Print_Crystal
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: sReport
.head 7 -  FunctionalVar: RepParams
.winattr class
.head 8 -  Class: cCrystalParams
.end
.head 7 -  Boolean: bShowPrompt
.head 7 -  Number: nCopies
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  FunctionalVar: vPrompt
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  FunctionalVar: vOption
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  FunctionalVar: vCopies
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  String: sName
.head 6 +  Actions 
.head 7 -  ! Call Prepare_Crystal( sReport, RepParams )
.head 7 -  ! Call vPrompt.SetBoolean( bShowPrompt )
.head 7 -  ! Call vCopies.SetNumber( nCopies, VT_I4 )
.head 7 -  ! Call rep.PropGetReportTitle( sName )
.head 7 -  ! Call Msg( sName )
.head 7 +  ! If Not rep.PrintOut( vPrompt, vCopies, vOption, vOption, vOption )
.head 8 -  Call Error( "Error printing." )
.head 7 -  ! Call Cleanup_Crystal()
.head 5 +  ! Function: Prepare_Crystal
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: sReport
.head 7 -  FunctionalVar: RepParams
.winattr class
.head 8 -  Class: cCrystalParams
.end
.head 6 -  Static Variables 
.head 6 +  Local variables 
.head 7 -  Boolean: bRet
.head 7 -  FunctionalVar: vRep
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  FunctionalVar: vOpenMethod
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  FunctionalVar: vOption
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  ComProxyVar: db
.winattr class
.head 8 -  Class: CRAXDRT_Database
.end
.head 7 -  ComProxyVar: tbls
.winattr class
.head 8 -  Class: CRAXDRT_DatabaseTables
.end
.head 7 -  ComProxyVar: tbl
.winattr class
.head 8 -  Class: CRAXDRT_DatabaseTable
.end
.head 7 -  ComProxyVar: props
.winattr class
.head 8 -  Class: CRAXDRT_ConnectionProperties
.end
.head 7 -  ComProxyVar: prop
.winattr class
.head 8 -  Class: CRAXDRT_ConnectionProperty
.end
.head 7 -  ComProxyVar: params
.winattr class
.head 8 -  Class: CRAXDRT_ParameterFieldDefinitions
.end
.head 7 -  ComProxyVar: param
.winattr class
.head 8 -  Class: CRAXDRT_ParameterFieldDefinition
.end
.head 7 -  String: sName
.head 7 -  FunctionalVar: vProp
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  FunctionalVar: vPropVal
.winattr class
.head 8 -  Class: Variant
.end
.head 7 -  Number: n
.head 7 -  Number: p
.head 6 +  Actions 
.head 7 -  ! Call vOpenMethod.SetNumber( 0, VT_I4 )
.head 7 -  ! Call repapp.Create( )
.head 7 -  ! Call rep.Create(  )
.head 7 -  ! Set bRet = repapp.OpenReport( sReportsPath || sReport, vOpenMethod, rep )
.head 7 -  ! Call vOption.MakeOptional(  )
.head 7 -  ! Call rep.PropGetDatabase( db )
.head 7 -  ! Call db.PropGetTables( tbls )
.head 7 -  ! Call tbls.PropGetItem( 1, tbl )
.head 7 -  ! Call tbl.PropGetConnectionProperties( props )
.head 7 -  ! Call props.PropGetItem( "User ID", vProp )
.head 7 -  ! Call vProp.GetObject( prop )
.head 7 -  ! Call vPropVal.SetString( SqlUser )
.head 7 -  ! Call prop.PropSetValue( vPropVal )
.head 7 -  ! Call props.PropGetItem( "Password", vProp )
.head 7 -  ! Call vProp.GetObject( prop )
.head 7 -  ! Call vPropVal.SetString( SqlPassword )
.head 7 -  ! Call prop.PropSetValue( vPropVal )
.head 7 -  ! Call rep.PropGetParameterFields( params )
.head 7 +  ! If RepParams.nParams > 0
.head 8 +  While n < RepParams.nParams
.head 9 -  Call params.GetItemByName( RepParams.params[ n ].sName, vOption, param )
.head 9 -  Call param.AddCurrentValue( RepParams.params[ n ].vValue )
.head 9 -  Set n = n + 1
.head 5 +  ! Function: Cleanup_Crystal
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 -  Parameters 
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 -  ! Call rep.Release(  )
.head 7 -  ! Call repapp.Release(  )
.head 5 +  ! Function: View_Crystal
.head 6 -  Description: 
.head 6 -  Returns 
.head 6 +  Parameters 
.head 7 -  String: sReport
.head 7 -  FunctionalVar: RepParams
.winattr class
.head 8 -  Class: cCrystalParams
.end
.head 6 -  Static Variables 
.head 6 -  Local variables 
.head 6 +  Actions 
.head 7 -  Call SalModalDialog( dlgViewCrystal, hWndForm, sReport, RepParams )
.head 5 +  Function: Print_Judge_Adoption
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Boolean: bJudge
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  FunctionalVar: params
.head 8 -  Class: cCrystalParams
.head 7 -  String: sJUCode
.head 7 -  String: sJudgeUser
.head 7 -  Number: nRatnum
.head 7 -  String: sBailiffUser
.head 6 +  Actions
.head 7 -  ! Call params.Add_Single_String( "Case Year", sCaseYr )
.head 7 -  ! Call params.Add_Single_String( "Case Type", sCaseTy )
.head 7 -  ! Call params.Add_Single_String( "Case Number", sCaseNo )
.head 7 -  ! Call Print_Crystal( "ju_judge_adopt.rpt", params, FALSE, 1 )
.head 7 -  ! Call View_Crystal( "ju_judge_adopt.rpt", params )
.head 7 -  Call SqlPrepareAndExecute( hSql, "
SELECT 
	MAX(ratnum) 
INTO 
	:nRatnum 
FROM 
	jcr_docket 
WHERE 
	caseyr = :sCaseYr AND casety = :sCaseTy AND caseno = :sCaseNo AND 
	casecode IN ( 'ORD', 'DEC' )
")
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  Call SqlPrepareAndExecute( hSql, "
SELECT
	m.caseyr || m.casety || m.caseno, 
	i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname, 
	m.ju_code, j.judgename, m.bookdate, 
	m.disp_date, d.dock_date, j.username
INTO
	:Reports.JudgeAdopt.sCaseNo,
	:Reports.JudgeAdopt.sCaption, 
	:sJUCode, :Reports.JudgeAdopt.sJudge, :Reports.JudgeAdopt.dBookDate, 
	:Reports.JudgeAdopt.dDispDate, :Reports.JudgeAdopt.dDockDate, :sJudgeUser
FROM
	jcr_master m, jcr_docket d, id_master i, judge j
WHERE
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	m.id = i.id AND 
	d.ratnum = :nRatnum AND  
	j.judge_number = m.judge
" )
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  Call SqlPrepareAndExecute( hSql, "SELECT bailiff_username INTO :sBailiffUser FROM judge WHERE username = :sJudgeUser" )
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  Set Reports.JudgeAdopt.sCaseType = Get_Case_Type( sJUCode )
.head 7 +  If bJudge And SalStrUpperX( SqlUser ) != SalStrUpperX( sJudgeUser ) And SalStrUpperX( SqlUser ) != SalStrUpperX( sBailiffUser )
.head 8 -  Set Reports.JudgeAdopt.sOnBehalf = "On Behalf Of " || Reports.JudgeAdopt.sJudge
.head 8 -  Call SqlPrepareAndExecute( hSql, "SELECT judgename INTO :Reports.JudgeAdopt.sJudgeSign FROM judge WHERE username = :SqlUser" )
.head 8 -  Call SqlFetchNext( hSql, nResult )
.head 7 +  Else
.head 8 -  Set Reports.JudgeAdopt.sJudgeSign = Reports.JudgeAdopt.sJudge
.head 8 -  Set Reports.JudgeAdopt.sOnBehalf = ""
.head 7 -  Call Print_Report( "JU_JUDGE_ADOPT.QRP", 
"Reports.JudgeAdopt.sCaption, Reports.JudgeAdopt.sCaseType, Reports.JudgeAdopt.sJudge, 
Reports.JudgeAdopt.dBookDate, Reports.JudgeAdopt.dDispDate, Reports.JudgeAdopt.dDockDate, 
Reports.JudgeAdopt.sCaseNo, Reports.JudgeAdopt.sOnBehalf, Reports.JudgeAdopt.sJudgeSign", 
"CAPTION, CASE_TYPE, JUDGE, 
BOOKDATE, DISPDATE, DOCKDATE, 
CASE_NO, ON_BEHALF, JUDGE_SIGN", 1 )

.head 5 +  Function: Print_Referral_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sReportName
.head 7 -  Number: nCopies
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '),
	m.bookdate
INTO
	:sJUType, :Referral.sCaption,
	:Referral.dBookDate
FROM
	jcr_master m, jcr_index i
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Referral.sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 -  Set sReport = 'JU_' || sReportName || '_NOTICE.QRP'
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo,  "Select substr(Data, 1, 254)
	from jcr_docket  into  :Reports.sCounty
	where caseyr=:sCaseYr and casety=:sCaseTy and caseno=:sCaseNo and
		casecode in ('CXFERC', 'CXFERD')" )
.head 7 +  If SqlFetchNext( hSqlTwo, nResult )
.head 8 +  If SalStrScan( Reports.sCounty, 'CASE TRANSFERED FROM' ) = 0
.head 9 -  Call SalStrMid( Reports.sCounty, 21, 254, Reports.sCounty )
.head 7 -  Call Print_Report( sReport,
	"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge,
		Reports.Referral.dBookDate, Reports.Referral.sCharges, Reports.sCounty",
	"CAPTION, CASENO, CASE_TYPE, JUDGE,
		BOOK_DATE, CHARGES, COUNTY", nCopies )
.head 5 +  Function: Print_Commitment
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Date/Time: dDated
.head 7 -  Number: nACDays
.head 7 -  String: sSentence
.head 7 -  String: sHO
.head 7 -  String: sIntake
.head 7 -  String: sProbation
.head 7 -  Number: nCopies
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  Date/Time: dDOB
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Set CommitRelease.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	j.judgename, id.DOB
INTO
	:sJUType, :CommitRelease.sCaption,
	:CommitRelease.sJudge, :dDOB
FROM
	jcr_master m, jcr_index i, judge j, id_master id
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	j.judge_number = m.judge AND 
	id.id = m.id
" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set CommitRelease.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set CommitRelease.sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 +  If sSentence != STRING_Null
.head 8 -  Set CommitRelease.sSentence = sSentence
.head 7 +  Else
.head 8 -  Set CommitRelease.sSentence = SalNumberToStrX( nACDays, 0 ) || " Days"
.head 7 -  Set CommitRelease.dDated = dDated
.head 7 -  Set CommitRelease.sIssueDate = SalFmtFormatDateTime( dDated, "MMMM" ) || " " || Get_Date_Suffix( SalDateDay( dDated ) ) || ", " || SalFmtFormatDateTime( dDated, "yyyy" )
.head 7 -  Set CommitRelease.sHO = sHO
.head 7 -  Set CommitRelease.sIntake = sIntake
.head 7 -  Set CommitRelease.sProbation = sProbation
.head 7 +  If sJUType = "ADU" Or Get_Age( dDOB ) >= 18
.head 8 -  Set CommitRelease.sAgency = "Stark County Jail"
.head 7 +  Else
.head 8 -  Set CommitRelease.sAgency = "Multi-County Attention Center"
.head 7 -  Call Print_Report( "JU_COMMITMENT.QRP", 
"Reports.CommitRelease.sCaption, Reports.CommitRelease.sCaseNo, Reports.CommitRelease.sCaseType, Reports.CommitRelease.sJudge, 
Reports.CommitRelease.dDated, Reports.CommitRelease.sCharges, Reports.CommitRelease.sSentence, Reports.sClerk, 
Reports.CommitRelease.sIssueDate, Reports.CommitRelease.sAgency, Reports.CommitRelease.sHO, Reports.CommitRelease.sIntake, 
Reports.CommitRelease.sProbation", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
DATED, CHARGES, SENTENCE, CLERK, 
ISSUE_DATE, AGENCY, HO, INTAKE, 
PROBATION", nCopies )
.head 5 +  Function: Print_Release
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Date/Time: dDated
.head 7 -  String: sCondition
.head 7 -  String: sHO
.head 7 -  String: sIntake
.head 7 -  String: sProbation
.head 7 -  Number: nCopies
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  Date/Time: dDOB
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Set CommitRelease.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	j.judgename, id.DOB
INTO
	:sJUType, :CommitRelease.sCaption,
	:CommitRelease.sJudge, :dDOB
FROM
	jcr_master m, jcr_index i, judge j, id_master id
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	j.judge_number = m.judge AND 
	id.id = m.id
" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set CommitRelease.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set CommitRelease.sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 -  Set CommitRelease.sCondition = sCondition
.head 7 -  Set CommitRelease.dDated = dDated
.head 7 -  Set CommitRelease.sIssueDate = SalFmtFormatDateTime( dDated, "MMMM" ) || " " || Get_Date_Suffix( SalDateDay( dDated ) ) || ", " || SalFmtFormatDateTime( dDated, "yyyy" )
.head 7 -  Set CommitRelease.sHO = sHO
.head 7 -  Set CommitRelease.sIntake = sIntake
.head 7 -  Set CommitRelease.sProbation = sProbation
.head 7 +  If sJUType = "ADU" Or Get_Age( dDOB ) >= 18
.head 8 -  Set CommitRelease.sAgency = "Stark County Jail"
.head 7 +  Else
.head 8 -  Set CommitRelease.sAgency = "Multi-County Attention Center"
.head 7 -  Call Print_Report( "JU_RELEASE.QRP", 
"Reports.CommitRelease.sCaption, Reports.CommitRelease.sCaseNo, Reports.CommitRelease.sCaseType, Reports.CommitRelease.sJudge, 
Reports.CommitRelease.dDated, Reports.CommitRelease.sCharges, Reports.CommitRelease.sCondition, Reports.sClerk, 
Reports.CommitRelease.sIssueDate, Reports.CommitRelease.sAgency, Reports.CommitRelease.sHO, Reports.CommitRelease.sIntake, 
Reports.CommitRelease.sProbation", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
DATED, CHARGES, CONDITION, CLERK,
ISSUE_DATE, AGENCY, HO, INTAKE, 
PROBATION", nCopies )
.head 5 +  Function: Print_Waiver
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sCaption
.head 7 -  String: sHO
.head 7 -  String: sJudgeType
.head 7 -  String: sAtty
.head 7 -  String: sProsecutor
.head 7 -  String: sIntake
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, j.judgename
INTO
	:sJUType, :Waiver.sJudge
FROM
	jcr_master m, judge j
WHERE
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	j.judge_number = m.judge
" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Waiver.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Set Waiver.sCaption = sCaption
.head 7 -  Set Waiver.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Waiver.sHO = sHO
.head 7 +  If sJudgeType = "REF"
.head 8 -  Set Waiver.sHOType = "Magistrate"
.head 7 +  Else
.head 8 -  Set Waiver.sHOType = "Judge"
.head 7 -  Set Waiver.sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 -  Set Waiver.sAttorney = sAtty
.head 7 -  Set Waiver.sProsecutor = sProsecutor
.head 7 -  Set Waiver.sIntakeWorker = sIntake
.head 7 -  Call Print_Report( "JU_WAIVER_RIGHTS.QRP", 
"Reports.Waiver.sAttorney, Reports.Waiver.sCaption, Reports.Waiver.sCaseNo, Reports.Waiver.sCaseType, 
Reports.Waiver.sCharges, Reports.Waiver.sHO, Reports.Waiver.sHOType, Reports.Waiver.sIntakeWorker, 
Reports.Waiver.sJudge, Reports.Waiver.sProsecutor", 
"ATTORNEY, CAPTION, CASENO, CASE_TYPE, 
CHARGES, HO, HO_TYPE, INTAKE_WORKER, 
JUDGE, PROSECUTOR", 2 )
.head 5 +  Function: Print_Prosecutor_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  Date/Time: dArraign
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Set Referral.dArraignDate = dArraign
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '),
	j.judgename, m.bookdate
INTO
	:sJUType, :Referral.sCaption,
	:Referral.sJudge, :Referral.dBookDate
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	j.judge_number = m.judge" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Referral.sCharges = Get_Charges( sCaseYr, sCaseTy, sCaseNo, sDegree )
.head 7 -  Call Print_Report( "JU_PROSECUTOR_NOTICE.QRP",
	"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType,
		Reports.Referral.sJudge, Reports.Referral.dBookDate, Reports.Referral.sCharges, Reports.Referral.dArraignDate",
	"CAPTION, CASENO, CASE_TYPE,
		JUDGE, BOOK_DATE, CHARGES, ARRAIGN_DATE", 1 )
.head 5 +  Function: Print_Address_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	j.judgename
INTO
	:sJUType, :Referral.sCaption, 
	:Referral.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	j.judge_number = m.judge" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Call Print_Report( "JU_ADDRESS_NOTICE.QRP", 
"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge", 
"CAPTION, CASENO, CASE_TYPE, JUDGE", 1 )
.head 5 +  Function: Print_Warrant_Recall
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sAgency
.head 7 -  Date/Time: dDated
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 6 +  Actions
.head 7 -  Set Warrant.dIssued = dDated
.head 7 -  Set Warrant.sCaseNumber = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	j.judgename
INTO
	:sJUType, :Warrant.sDef,
	:Warrant.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND
	j.judge_number = m.judge
" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Warrant.sAlleged = Get_Case_Type( sJUType )
.head 7 -  Set Warrant.dIssued = dDated
.head 7 -  Set Warrant.sAgency = sAgency
.head 7 -  Call Print_Report( "JU_WARRANT_RECALL.QRP", 
"Reports.Warrant.sDef, Reports.Warrant.sCaseNumber, Reports.Warrant.sAlleged, Reports.Warrant.sJudge, 
Reports.Warrant.dIssued, Reports.Warrant.sAgency", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
DATED, AGENCY", 2 )
.head 5 +  Function: Print_Case_Track_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	m.bookdate
INTO
	:sJUType, :Referral.sCaption,
	:Referral.dBookDate
FROM
	jcr_master m, jcr_index i
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Referral.sDocket = Get_Docket_String( sCaseYr, sCaseTy, sCaseNo )
.head 7 -  Call Print_Report( "JU_TRACK_NOTICE.QRP", 
"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge, 
Reports.Referral.sDocket", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
DOCKET", 2 )
.head 5 +  Function: Print_Generic
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sTitle
.head 7 -  String: sAction
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	m.bookdate, j.judgename
INTO
	:sJUType, :Referral.sCaption,
	:Referral.dBookDate, :Referral.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	m.judge = j.judge_number(+)" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Referral.sAction = sAction
.head 7 -  Set Referral.sTitle = sTitle
.head 7 -  Call Print_Report( "JU_GENERIC_NOTICE.QRP", 
"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge, 
Reports.Referral.sAction, Reports.Referral.sTitle", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
ACTION, TITLE", 2 )
.head 5 +  Function: Print_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sTitle
.head 7 -  String: sNotice
.head 7 -  Number: nCopies
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	m.bookdate, j.judgename
INTO
	:sJUType, :Referral.sCaption,
	:Referral.dBookDate, :Referral.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	m.judge = j.judge_number(+)" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Referral.sAction = sNotice
.head 7 -  Set Referral.sTitle = sTitle
.head 7 -  Call Print_Report( "JU_GENERIC_NOTICE.QRP", 
"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge, 
Reports.Referral.sAction, Reports.Referral.sTitle", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
NOTICE, TITLE", nCopies )
.head 5 +  Function: Print_Service_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: nRatnum
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sJUType
.head 6 +  Actions
.head 7 -  Call SqlPrepareAndExecute( hSql, "
SELECT
	caseyr, casety, caseno, 
	SUBSTR( first_name || ' ' || DECODE( mid_name, NULL, '', mid_name || ' ' ) || last_name, 0, 150), 
	SUBSTR( address || ' ' || city || DECODE( state, NULL, '', ', ' || state ) || ' ' || zip, 0, 150), 
	return_date, document, fail_reason, 
	type
INTO
	:sCaseYr, :sCaseTy, :sCaseNo, 
	:ServiceNotice.sServiceTo, 
	:ServiceNotice.sAddress, 
	:ServiceNotice.dReturned, :ServiceNotice.sServiceOf, :ServiceNotice.sReason, 
	:ServiceNotice.sServiceBy
FROM
	jcr_service
WHERE
	ratnum = :nRatnum
" )
.head 7 -  Call SqlFetchNext( hSql, nResult )
.head 7 -  Set ServiceNotice.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, SUBSTR( i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 0, 150), 
	j.judgename
INTO
	:sJUType, :ServiceNotice.sCaption, 
	:ServiceNotice.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	m.judge = j.judge_number
" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set ServiceNotice.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Call Print_Report( "JU_SERVICE_NOTICE.QRP", 
"Reports.ServiceNotice.sCaption, Reports.ServiceNotice.sCaseNo, Reports.ServiceNotice.sCaseType, Reports.ServiceNotice.sJudge, 
Reports.ServiceNotice.sServiceTo, Reports.ServiceNotice.sServiceOf, Reports.ServiceNotice.sReason, Reports.ServiceNotice.dReturned, 
Reports.ServiceNotice.sAddress, Reports.ServiceNotice.sServiceBy", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
SERVICE_TO, SERVICE_OF, REASON, DATE_RETURNED, 
ADDRESS, SERVICE_BY", 2 )
.head 5 +  Function: Print_Seal_Form1
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sAddCases
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 7 -  String: sDegree
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 +  If sAddCases != STRING_Null
.head 8 -  Set Referral.sCaseNo = Referral.sCaseNo || sAddCases
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '),
	m.bookdate, j.judgename
INTO
	:sJUType, :Referral.sCaption,
	:Referral.dBookDate, :Referral.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	m.judge = j.judge_number(+)" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Call Print_Report( "ju_seal_form1_notice.qrp",
	"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge",
	"CAPTION, CASE_NO, CASE_TYPE, JUDGE", 1 )
.head 5 +  Function: Print_Intake_Notice
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sCaseYr
.head 7 -  String: sCaseTy
.head 7 -  String: sCaseNo
.head 7 -  String: sTitle
.head 7 -  String: sAction
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: sJUType
.head 6 +  Actions
.head 7 -  Set Referral.sCaseNo = sCaseYr || sCaseTy || sCaseNo
.head 7 -  Call SqlPrepareAndExecute( hSqlTwo, "
SELECT
	m.ju_code, i.fname || ' ' || DECODE( i.mname, NULL, '', i.mname || ' ' ) || i.lname || DECODE( i.title, NULL, '', i.title || ' '), 
	m.bookdate, j.judgename
INTO
	:sJUType, :Referral.sCaption,
	:Referral.dBookDate, :Referral.sJudge
FROM
	jcr_master m, jcr_index i, judge j
WHERE
	i.id = m.id AND i.party_no = 0 AND
	m.caseyr = :sCaseYr AND m.casety = :sCaseTy AND m.caseno = :sCaseNo AND 
	m.judge = j.judge_number(+)" )
.head 7 -  Call SqlFetchNext( hSqlTwo, nResult )
.head 7 -  Set Referral.sCaseType = Get_Case_Type( sJUType )
.head 7 -  Set Referral.sAction = sAction
.head 7 -  Set Referral.sTitle = sTitle
.head 7 -  Call Print_Report( "JU_INTAKE_NOTICE.QRP", 
"Reports.Referral.sCaption, Reports.Referral.sCaseNo, Reports.Referral.sCaseType, Reports.Referral.sJudge, 
Reports.Referral.sAction, Reports.Referral.sTitle", 
"CAPTION, CASENO, CASE_TYPE, JUDGE, 
ACTION, TITLE", 1 )
.head 3 +  Functional Class: cWarrantData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sWarrType
.head 5 -  String: sCaseNumber
.head 5 -  String: sDef
.head 5 -  String: sAlleged
.head 5 -  String: sAgency
.head 5 -  String: sDescription
.head 5 -  Date/Time: dIssued
.head 5 -  String: sBond
.head 5 -  String: sAKA
.head 5 -  String: sAddress1
.head 5 -  String: sAddress2
.head 5 -  String: sSex
.head 5 -  String: sColor
.head 5 -  String: sHeight
.head 5 -  Number: nWeight
.head 5 -  Number: nAge
.head 5 -  Date/Time: dDOB
.head 5 -  String: sHair
.head 5 -  String: sEyes
.head 5 -  String: sSSNO
.head 5 -  String: sScars
.head 5 -  String: sCopy
.head 5 -  String: sJudge
.head 4 -  Functions
.head 3 +  Functional Class: cCertLabelData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Long String: sName
.head 5 -  String: sAddress1
.head 5 -  String: sAddress2
.head 5 -  String: sAddress3
.head 5 -  String: sServiceNoBarcode
.head 5 -  String: sServiceNo
.head 5 -  Date/Time: dDated
.head 5 -  String: sCaseNo
.head 4 -  Functions
.head 3 +  Functional Class: cDFAData
.head 4 -  Description: Report data for the DFA report and DFA notices
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sJuvenile
.head 5 -  Number: nOccurance
.head 5 -  String: sHO
.head 5 -  Date/Time: dArnDate
.head 5 -  Date/Time: dNextDate
.head 4 -  Functions
.head 3 +  Functional Class: cDRPJEData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sHOType
.head 5 -  String: sHO
.head 5 -  String: sCaseNo
.head 5 -  Long String: sPassengers
.head 5 -  String: sJudge
.head 5 -  Date/Time: dBegin
.head 5 -  Date/Time: dEnd
.head 5 -  Number: nLocNum
.head 5 -  String: sLocType
.head 5 -  String: sLocName
.head 5 -  String: sLocAddress
.head 5 -  String: sLocHours
.head 4 -  Functions
.head 3 +  Functional Class: cDRPAppData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sCaseNo
.head 5 -  String: sJudge
.head 5 -  Date/Time: dSuspFrom
.head 5 -  Date/Time: dSuspTo
.head 5 -  String: sHO
.head 5 -  String: sHOType
.head 4 -  Functions
.head 3 +  Functional Class: cPPData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sCaseNo
.head 5 -  String: sJudge
.head 5 -  Number: nAmount
.head 5 -  String: sDueDate
.head 5 -  Number: nCosts
.head 5 -  Number: nFines
.head 5 -  Number: nRest
.head 5 -  Number: nOwed
.head 5 -  Number: nReceipts
.head 5 -  Number: nBalance
.head 4 -  Functions
.head 3 +  Functional Class: cDFPData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sJuvenile
.head 5 -  Date/Time: dLastPayment
.head 5 -  Number: nDaysSince
.head 5 -  Number: nPayAmount
.head 5 -  Number: nDueDate
.head 5 -  Number: nBalance
.head 4 -  Functions
.head 3 +  Functional Class: cALData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Date/Time: dArnDate
.head 5 -  String: sCaseNo
.head 5 -  String: sJuvenile
.head 4 -  Functions
.head 3 +  Functional Class: cPhotoIDData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sJudge
.head 5 -  String: sHeldBy
.head 5 -  String: sNotPermit
.head 4 -  Functions
.head 3 +  Functional Class: cLicRecData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sJudge
.head 4 -  Functions
.head 3 +  Functional Class: cDRPData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sCaseNo
.head 5 -  String: sJudge
.head 5 -  String: sHO
.head 5 -  String: sHOType
.head 5 -  Date/Time: dBegin
.head 5 -  Date/Time: dEnd
.head 5 -  String: sPassengers
.head 5 -  String: sAddress
.head 5 -  Date/Time: dDOB
.head 5 -  String: sOLNum
.head 5 -  String: sSSN
.head 5 -  Number: nLocNum
.head 5 -  String: sLocType
.head 5 -  String: sLocName
.head 5 -  String: sLocAddress
.head 5 -  String: sLocHours
.head 5 -  Date/Time: dTimestamp
.head 5 -  String: sDateSuffix
.head 5 -  String: sOtherRestrictions
.head 4 -  Functions
.head 3 +  Functional Class: cDDCListData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Date/Time: dSchoolDate
.head 5 -  String: sCaseNo
.head 5 -  String: sJuvenile
.head 5 -  String: sAddress
.head 4 -  Functions
.head 3 +  Functional Class: cTransferJEData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sCaseNo
.head 5 -  String: sJudge
.head 5 -  Date/Time: dBookDate
.head 5 -  String: sCityState
.head 5 -  String: sCounty
.head 4 -  Functions
.head 3 +  Functional Class: cJudgeAdoptData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sJuvenile
.head 5 -  Date/Time: dBegin
.head 5 -  Date/Time: dEnd
.head 5 -  Date/Time: dDisp
.head 5 -  Long String: sDisposition
.head 5 -  Long String: sCharges
.head 5 -  String: sJudge
.head 5 -  String: sJudgeSign
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  Date/Time: dBookDate
.head 5 -  Date/Time: dDispDate
.head 5 -  Date/Time: dDockDate
.head 5 -  String: sOnBehalf
.head 4 -  Functions
.head 3 +  Functional Class: cCrystalParams
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  FunctionalVar: params[*]
.head 6 -  Class: cCrystalParam
.head 5 -  Number: nParams
.head 4 +  Functions
.head 5 +  Function: Add_Single_String
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: sName
.head 7 -  String: sValue
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set params[ nParams ].nType = CRYSTAL_TYPE_SINGLE
.head 7 -  Set params[ nParams ].sName = sName
.head 7 -  Call params[ nParams ].vValue.SetString( sValue )
.head 7 -  Set nParams = nParams + 1
.head 5 +  Function: Clear
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set nParams = 0
.head 3 +  Functional Class: cCrystalParam
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: nType
.head 5 -  String: sName
.head 5 -  FunctionalVar: vValue
.head 6 -  Class: Variant
.head 4 -  Functions
.head 3 +  Functional Class: cSummonsData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sName
.head 5 -  String: sCaseNo
.head 5 -  String: sJudge
.head 5 -  String: sCaseType
.head 5 -  String: sAddress
.head 5 -  String: sHearingDate
.head 5 -  String: sCharges
.head 5 -  String: sIssueDate
.head 5 -  String: sCopy
.head 4 -  Functions
.head 3 +  Functional Class: cReferralData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseNo
.head 5 -  String: sCaseType
.head 5 -  String: sJudge
.head 5 -  Date/Time: dBookDate
.head 5 -  String: sCharges
.head 5 -  Date/Time: dArraignDate
.head 5 -  String: sDocket
.head 5 -  String: sAction
.head 5 -  String: sTitle
.head 4 -  Functions
.head 3 +  Functional Class: cCommitReleaseData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaption
.head 5 -  String: sCaseNo
.head 5 -  String: sCaseType
.head 5 -  String: sJudge
.head 5 -  Date/Time: dDated
.head 5 -  String: sAgency
.head 5 -  String: sCharges
.head 5 -  String: sCondition
.head 5 -  String: sSentence
.head 5 -  String: sIssueDate
.head 5 -  String: sHO
.head 5 -  String: sIntake
.head 5 -  String: sProbation
.head 4 -  Functions
.head 3 +  Functional Class: cWaiverData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sCaption
.head 5 -  String: sJudge
.head 5 -  String: sCaseType
.head 5 -  String: sAttorney
.head 5 -  String: sHOType
.head 5 -  String: sHO
.head 5 -  String: sCharges
.head 5 -  String: sIntakeWorker
.head 5 -  String: sProsecutor
.head 4 -  Functions
.head 3 +  Functional Class: cServiceNoticeData
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  String: sCaseNo
.head 5 -  String: sCaption
.head 5 -  String: sCaseType
.head 5 -  String: sJudge
.head 5 -  String: sServiceTo
.head 5 -  String: sServiceOf
.head 5 -  String: sServiceBy
.head 5 -  String: sReason
.head 5 -  Date/Time: dReturned
.head 5 -  String: sAddress
.head 4 -  Functions
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX: AX_CrystalActiveXReportViewerLib11_CrystalActiveXReportViewer
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control: cQuickGraph
.head 2 +  Application Actions
.head 3 +  On SAM_AppStartup
.head 4 -  Call SalModalDialog( dlgLogin, hWndNULL, 'Login Test' )
.head 3 +  On SAM_SqlError
.head 4 -  Call AddToError( STRING_Null, STRING_Null, STRING_Null )
.head 4 -  Return FALSE
.head 1 +  Dialog Box: dlgLogin
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Criminal Program Login Screen
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.975"
.head 4 -  Top: 0.656"
.head 4 -  Width:  5.925"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.833"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Blue
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description: This window is the initial
login screen
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Picture: picLogin
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.313"
.head 5 -  Top: 0.938"
.head 5 -  Width:  5.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.667"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name: lo178.bmp
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Size to Fit
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 5
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Black
.head 4 -  Background Color: Black
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 28676
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.25"
.head 5 -  Top: 3.74"
.head 5 -  Width:  1.113"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.219"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: UserName:
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 28677
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.275"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.038"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.198"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Password:
.head 3 +  Data Field: dfUser
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cLoginUser
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Class Default
.head 5 -  Data Type: Class Default
.head 5 -  Editable? Class Default
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.388"
.head 6 -  Top: 3.667"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Justify: Class Default
.head 5 -  Format: Uppercase
.head 5 -  Country: Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  Input Mask: Class Default
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 +  If SalStrLength( dfUser ) = 10
.head 7 -  Call SalSetFocus( dfPass )
.head 3 +  Data Field: dfPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.388"
.head 6 -  Top: 3.969"
.head 6 -  Width:  4.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.24"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Black
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 2.413"
.head 5 -  Top: 4.271"
.head 5 -  Width:  1.45"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set dfPass = SalStrTrimX( dfPass)
.head 6 -  Set dfUser = SalStrTrimX( dfUser)
.head 6 -  Set dfPass = SalStrUpperX( dfPass )
.head 6 +  If SqlDatabase = ''
.head 7 -  Set SqlDatabase = 'JU'
.head 6 -  Set SqlUser = dfUser
.head 6 -  Set SqlPassword = dfPass
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 +  When SqlError
.head 7 -  Return FALSE
.head 6 -  Set bLogin = SqlConnect (hSql)
.head 6 +  If Not bLogin
.head 7 -  Call SalMessageBox( 'Error Connecting to Database',
'Connect', MB_IconExclamation|MB_IconHand )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Call SalSetFocus( dfUser )
.head 7 -  Return TRUE
.head 6 +  Else
.head 7 -  Call SqlSetResultSet( hSql, TRUE )
.head 7 -  Call Check_Change_Password(  )
.head 6 -  Call SalEndDialog( dlgLogin, 0 )
.head 6 -  Call WriteUser(  )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton( pbOk )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 4.038"
.head 5 -  Top: 4.271"
.head 5 -  Width:  1.45"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.281"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalQuit(  )
.head 3 +  Data Field: dfCourt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.4"
.head 6 -  Top: 0.198"
.head 6 -  Width:  5.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.49"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Center
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Copperplate Gothic Light
.head 5 -  Font Size: 18
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: Blue
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChangeDATABASE
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: Default
.head 5 -  Top: Default
.head 5 -  Width:  Default
.head 5 -  Width Editable? Yes
.head 5 -  Height: Default
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F9
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SqlUser = 'RYANP'
.head 7 -  Call SalModalDialog(dlgDatasource,  hWndForm, SqlDatabase)
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sWindowTitle
.head 2 +  Window Variables
.head 3 -  String: sLoginTitle
.head 3 -  Number: nFetch
.head 3 -  String: sPublic
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set sPublic = ReadRegistry(  )
.head 4 -  Set sSelectWhere = ReadRegistrySelect(  )
.head 4 +  If sSelectWhere = ''
.head 5 -  Call SalMessageBox('Error reading Where Clause from Registry,  Contact programmer.'  , 'ERROR - CAN NOT CONTINUE', MB_Ok|MB_IconStop)
.head 5 -  Call SalQuit( )
.head 4 +  If sPublic = 'Y'
.head 5 -  Call SalMessageBox( 'Access has been denied', 'Error', MB_Ok|MB_IconExclamation )
.head 5 -  Call SalQuit(  )
.head 4 -  Set sLoginTitle = ReadRegistryTitle(  )
.head 4 -  Set dfCourt = sLoginTitle
.head 4 +  If sWindowTitle != STRING_Null
.head 5 -  Call SalSetWindowText( hWndForm, sWindowTitle )
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgAbout
.head 2 -  Class: cQuickTabsDialog
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Class Default
.head 2 -  Display Settings
.head 3 -  Display Style? Class Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Class Default
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 2.8"
.head 4 -  Top: 1.604"
.head 4 -  Width:  5.383"
.head 4 -  Width Editable? Class Default
.head 4 -  Height: Class Default
.head 4 -  Height Editable? Class Default
.head 3 -  Absolute Screen Location? Class Default
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 8
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Class Default
.head 3 -  Background Color: Class Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Class Default
.head 4 -  Location? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Size: Class Default
.head 4 -  Size Editable? Class Default
.head 4 -  Docking Toolbar? Class Default
.head 4 -  Toolbar Docking Orientation: Class Default
.head 4 -  Font Name: Class Default
.head 4 -  Font Size: Class Default
.head 4 -  Font Enhancement: Class Default
.head 4 -  Text Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  Resizable? Class Default
.head 3 -  Contents
.head 3 -  ! Resizable? Class Default
.head 2 +  Contents
.head 3 +  Picture: picTabs
.data CLASSPROPS
0000: 5400610062004C00 6500660074004D00 6100720067006900 6E000000FFFE0400
0020: 3000000054006100 6200430075007200 720065006E007400 0000FFFE0E007400
0040: 6200500072006500 7600000054006100 6200520069006700 680074004D006100
0060: 7200670069006E00 0000FFFE04003000 0000540061006200 4E0061006D006500
0080: 73000000FFFE3000 740062004D006100 69006E0009007400 6200520065007000
00A0: 6F00720074007300 0900740062005000 7200650076000000 5400610062004C00
00C0: 6100620065006C00 73000000FFFE4E00 4D00610069006E00 090049006E006300
00E0: 6C00750064006500 2000520065007000 6F00720074007300 0900500072006500
0100: 760069006F007500 7300200052006500 7600690073006900 6F006E0000005400
0120: 6100620050006100 6700650043006F00 75006E0074000000 FFFE040031000000
0140: 5400610062004200 6F00740074006F00 6D004D0061007200 670069006E000000
0160: FFFE040030000000 5400610062004400 7200610077005300 740079006C006500
0180: 0000FFFE16005700 69006E0039003500 5300740079006C00 6500000054006100
01A0: 620046006F007200 6D00500061006700 650073000000FFFE 0600090009000000
01C0: 5400610062005400 6F0070004D006100 7200670069006E00 0000FFFE04003000
01E0: 0000
.enddata
.data CLASSPROPSSIZE
0000: E201
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 1
.head 4 -  Class ChildKey: 0
.head 4 -  Class: cQuickTabsDialog
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: Class Default
.head 5 -  Top: Class Default
.head 5 -  Width:  Class Default
.head 5 -  Width Editable? Class Default
.head 5 -  Height: Class Default
.head 5 -  Height Editable? Class Default
.head 4 -  Visible? Class Default
.head 4 -  Editable? Class Default
.head 4 -  File Name:
.head 4 -  Storage: Class Default
.head 4 -  Picture Transparent Color: Class Default
.head 4 -  Fit: Class Default
.head 4 -  Scaling
.head 5 -  Width:  Class Default
.head 5 -  Height:  Class Default
.head 4 -  Corners: Class Default
.head 4 -  Border Style: Class Default
.head 4 -  Border Thickness: Class Default
.head 4 -  Tile To Parent? Class Default
.head 4 -  Border Color: Class Default
.head 4 -  Background Color: Class Default
.head 4 -  ToolTip:
.head 4 -  Enable Scroll? Yes
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 3.95"
.head 5 -  Top: 1.762"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgAbout, 1)
.head 3 -  Background Text: bkgd3
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50572
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.619"
.head 5 -  Width:  1.3"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Program Name:
.head 3 -  Background Text: bkgd4
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50573
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.976"
.head 5 -  Width:  1.233"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Revision Date:
.head 3 -  Background Text: bkgd5
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Resource Id: 50574
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 1.333"
.head 5 -  Width:  1.467"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Revision Number:
.head 3 +  Data Field: dfAboutName
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 0.619"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfAboutRDate
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 0.976"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: DateTime
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfAboutRNumber
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 740062004D006100 69006E000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.95"
.head 6 -  Top: 1.333"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: 3D Face Color
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  List Box: lbReportFile
.data CLASSPROPSSIZE
0000: 3400
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE1400
0020: 7400620052006500 70006F0072007400 73000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.383"
.head 5 -  Top: 0.488"
.head 5 -  Width:  3.4"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.571"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Multiple selection? No
.head 4 -  Sorted? Yes
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Horizontal Scroll? No
.head 4 -  ToolTip:
.head 4 -  List Initialization
.head 4 -  Message Actions
.head 3 +  Child Table: tblPrev
.data CLASSPROPSSIZE
0000: 2E00
.enddata
.data CLASSPROPS
0000: 5400610062004300 680069006C006400 4E0061006D006500 73000000FFFE0E00
0020: 7400620050007200 650076000000
.enddata
.data INHERITPROPS
0000: 0100
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.233"
.head 6 -  Top: 0.476"
.head 6 -  Width:  4.833"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 1.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Default
.head 5 -  Font Size: 8
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: Default
.head 5 -  Discardable? Yes
.head 4 +  Contents
.head 5 +  Column: colPrevDate
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Date
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Date/Time
.head 6 -  Justify: Left
.head 6 -  Width:  0.9"
.head 6 -  Width Editable? Yes
.head 6 -  Format: MM-dd-yy
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colPrev
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colProblem
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 500
.head 6 -  Data Type: Long String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colNote
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Previous Change
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: 500
.head 6 -  Data Type: Long String
.head 6 -  Justify: Left
.head 6 -  Width:  3.2"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Functions
.head 4 -  Window Variables
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalMessageBox(  'REVISION DATE: '|| SalFmtFormatDateTime( tblPrev.colPrevDate, 'MM-dd-yyyy' ) ||'

PROBLEM: '||  tblPrev.colProblem ||'

NOTE/FIX: '|| tblPrev.colNote, tblPrev.colPrev, MB_Ok|MB_IconInformation)
.head 2 +  Functions
.head 3 +  Function: FillReportFile
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nMax
.head 5 -  Number: n
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  ! Call SalArrayGetUpperBound( sAboutReportFile, 1, nMax )
.head 5 -  ! Set n = 0
.head 5 +  ! While n <= nMax
.head 6 -  Call SalListAdd( dlgAbout.lbReportFile, sAboutReportFile[n])
.head 6 -  Call Inc(n)
.head 5 -  ! Call SqlPrepareAndExecute(hSql, 'SELECT	dep_name
			      FROM	admin.app_depend@admin_link
			      INTO
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 -  Call SalListPopulate( dlgAbout.lbReportFile, hSql, 'SELECT	dep_name
			      FROM	admin.app_depend@admin_link
			      WHERE	UPPER(app_name) like UPPER(:sAppName)')
.head 3 +  Function: FillPrev
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 -  Call SalTblPopulate( tblPrev, hSql, 'SELECT	mod_label, note, problem, timestamp
			       FROM	admin.app_mod@admin_link
			       INTO		:tblPrev.colPrev, :tblPrev.colNote, :tblPrev.colProblem, :tblPrev.colPrevDate
			       WHERE	UPPER(app_name) like UPPER( :sAppName)', TBL_FillAll)
.head 3 +  Function: FillMain
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sAppName
.head 4 +  Actions
.head 5 -  Set sAppName = SalStrMidX( sAboutName, 0, SalStrLength( sAboutName) - 4)||'%'
.head 5 +  If sAboutRNumber != ''
.head 6 -  Set dfAboutRDate = dtAboutRDate
.head 6 -  Set dfAboutRNumber = sAboutRNumber
.head 5 +  Else
.head 6 -  Call SqlPrepareAndExecute( hSql, 'SELECT	timestamp, seq_no
			       FROM	admin.app_mod@admin_link
			       INTO		:dlgAbout.dfAboutRDate, :dlgAbout.dfAboutRNumber
			       WHERE	UPPER(app_name) like UPPER( :sAppName)
			       ORDER BY	seq_no desc')
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 2 +  Window Parameters
.head 3 -  String: sAboutName
.head 3 -  Date/Time: dtAboutRDate
.head 3 -  String: sAboutRNumber
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalCenterWindow( hWndForm )
.head 4 -  Set dfAboutName = sAboutName
.head 4 -  Call FillMain( )
.head 4 -  Call SalSetWindowText( hWndForm, 'About '||sAboutName )
.head 4 -  Call FillReportFile( )
.head 4 -  Call FillPrev( )
.head 3 +  On SAM_CreateComplete
.head 4 -  Call picTabs.BringToTop( 0, TRUE)
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgPasswordAPL
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Change Password
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.163"
.head 4 -  Top: 1.813"
.head 4 -  Width:  4.06"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.723"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfCurrentPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.242"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfNewPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.575"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfVerifyPass
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.29"
.head 6 -  Top: 0.908"
.head 6 -  Width:  1.1"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Invisible
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbChange
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Change Password
.head 4 -  Window Location and Size
.head 5 -  Left: 0.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  ! Set dfCurrentPass = SalStrUpperX( dfCurrentPass )
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '.')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '>')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '<')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ',')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '/')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '?')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '~')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '`')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '!')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '@')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '#')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '%')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '^')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '$')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '&')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '*')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '(')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ')')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '-')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '_')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '+')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '=')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ':')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, ';')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, '"')
.head 6 -  ! Set nCheckIllegal = SalStrScan( dfNewPass, "'")
.head 6 +  If SalStrLength( dfNewPass ) < 6
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('A Password Must be at Least 6 Charaters Long', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX(dfNewPass) != SalStrUpperX(dfVerifyPass)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Passwords do not Match', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfNewPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX(dfCurrentPass) != SalStrUpperX(SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox('Current Password must be Re-entered before Password can be Changed','Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  Else If SalStrUpperX( dfVerifyPass) =SalStrUpperX(  SqlPassword)
.head 7 -  Set nErrorCnt = nErrorCnt + 1
.head 7 -  Call SalMessageBox( 'Password Must be UNIQUE, please re-enter a valid password', 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 +  ! Else If nCheckIllegal > -1
.head 7 -  Call SalMessageBox( "Password can not contain illegal characters such as
 	(. ; : ! @ # $ % ^ & * ( ) - _ + = ~ ` ' \" ? / \ | < > , ).
Please re-enter a valid password", 'Password Error', MB_Ok )
.head 7 -  Call SalSetFocus( dfCurrentPass )
.head 7 +  If nErrorCnt > 3
.head 8 -  Call SalQuit(  )
.head 7 -  Call SalWaitCursor( FALSE )
.head 7 -  Return TRUE
.head 6 -  Call SalWaitCursor( TRUE )
.head 6 -  Call SqlConnect (hSql)
.head 6 +  If SqlPLSQLCommand(hSql, 'change_pwd(SqlUser,dfNewPass)' )
.head 7 -  Call SalMessageBox( 'Password Successfully Changed', 'Change Password',MB_Ok|MB_IconExclamation)
.head 7 -  Call SqlPrepareAndExecute( hSql,"Update JCR_USERS set change_password = 'N' ,gracelogins = 5 where "||sSelectWhere)
.head 7 -  Call SqlCommit( hSql )
.head 7 -  Set SqlPassword = dfNewPass
.head 7 -  Set bChange = FALSE
.head 6 +  Else
.head 7 -  Call SalMessageBox( 'Password Change failed - - Contact Programmer', 'Change Password',MB_Ok|MB_IconStop)
.head 6 -  Call SalWaitCursor( FALSE )
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 5 +  On SAM_Create
.head 6 -  Call SalSetDefButton(pbChange)
.head 6 -  Call SalSetFocus(dfCurrentPass)
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 2.19"
.head 5 -  Top: 1.408"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( hWndForm, 0 )
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 52744
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.258"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Current Password:
.head 3 -  Background Text: bkgd4
.head 4 -  Resource Id: 52745
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.592"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: New Password:
.head 3 -  Background Text: bkgd5
.head 4 -  Resource Id: 52746
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.49"
.head 5 -  Top: 0.925"
.head 5 -  Width:  1.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Retype Password:
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Number: nErrorCnt
.head 3 -  Boolean: bOk
.head 3 -  Number: nCheckIllegal
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Set nErrorCnt = 0
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 4 -  Call SalWaitCursor( FALSE )
.head 3 +  On SAM_Destroy
.head 4 +  If bChange = TRUE
.head 5 -  Call SqlPrepareAndExecute( hSql, 'update JCR_USERS set gracelogins = gracelogins - 1 where username = :SqlUser' )
.head 5 -  Call SqlCommit( hSql )
.head 5 -  Set bChange = FALSE
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgLabelHelp
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.55"
.head 4 -  Top: 0.135"
.head 4 -  Width:  7.338"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.562"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Child Table: tblDisplay
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.1"
.head 6 -  Top: 0.052"
.head 6 -  Width:  7.088"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 4.625"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Font Name: Times New Roman
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  View: Table
.head 5 -  Allow Row Sizing? No
.head 5 -  Lines Per Row: Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 40000
.head 5 -  Discardable? No
.head 4 +  Contents
.head 5 +  Column: col1
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.871"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 1, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay, 1, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 5 +  Column: col2
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 2, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay,2, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 5 +  Column: colRowID
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 0
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title:
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  3.614"
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  ToolTip:
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_ColumnSelectClick
.head 8 +  If bIncr = FALSE
.head 9 -  Call SalTblSortRows( tblDisplay, 3, TBL_SortIncreasing )
.head 9 -  Set bIncr = TRUE
.head 8 +  Else
.head 9 -  Call SalTblSortRows( tblDisplay, 3, TBL_SortDecreasing )
.head 9 -  Set bIncr =FALSE
.head 4 -  Functions
.head 4 +  Window Variables
.head 5 -  Boolean: bIncr
.head 4 +  Message Actions
.head 5 +  On SAM_DoubleClick
.head 6 -  Set sReturnRowid = colRowID
.head 6 -  Call SalEndDialog(dlgLabelHelp, 1)
.head 5 +  On WM_KEYUP
.head 6 +  If wParam = VK_RETURN
.head 7 -  Call SalSendMsg( tblDisplay, SAM_DoubleClick, 0, 0 )
.head 5 +  On SAM_Create
.head 6 -  Call SalTblSetTableFlags( tblDisplay, TBL_Flag_SelectableCols, TRUE )
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  String: sType
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 +  If sType = 'BOND'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, 'SELECT	bondsman, insurance_co, rowid
			           FROM	jcr_bondsman
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	bondsman', TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col1, 'BONDSMAN')
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'INSURANCE CO')
.head 4 +  Else If sType = 'ATT'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	to_char(attno), lname||', '||fname||' '||mname, rowid
			           FROM	attorney
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	attno", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'ATT. #')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'ATT NAME')
.head 4 +  Else If sType = 'POLICE'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	code, agency, rowid
			           FROM	jcr_agency_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	code", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY CODE')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 5 -  Call SalTblSetColumnTitle( tblDisplay.col2,  'AGENCY NAME')
.head 4 +  Else If sType = 'OTHERA'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, "SELECT	agencynm, rowid
			           FROM	other_agency
			           INTO	:tblDisplay.col1, :tblDisplay.colRowID
			           ORDER BY	agencynm", TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY NAME')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1,7.088)
.head 5 -  Call SalHideWindow( tblDisplay.col2)
.head 4 +  Else If sType = 'PROBCS'
.head 5 -  Call SalTblPopulate(tblDisplay, hSql, 'SELECT	agency, description, rowid
			           FROM	program_codes
			           INTO	:tblDisplay.col1, :tblDisplay.col2, :tblDisplay.colRowID
			           ORDER BY	agency', TBL_FillAll)
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col1, 'AGENCY NAME')
.head 5 -  Call SalTblSetColumnTitle(  tblDisplay.col2, 'DESCRIPTION')
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col1, 1.5)
.head 5 -  Call SalTblSetColumnWidth( tblDisplay.col2, 6)
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgLabels
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Labels Processing
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? No
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 1.225"
.head 4 -  Top: 0.219"
.head 4 -  Width:  8.075"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.146"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description: !Added docket entry made for Bank Attach - Notice Debtor
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfName
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 1.521"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 1.833"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfAddress2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 2.198"
.head 6 -  Width:  5.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelCity
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.863"
.head 6 -  Top: 2.573"
.head 6 -  Width:  2.963"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelState
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 4.825"
.head 6 -  Top: 2.573"
.head 6 -  Width:  0.538"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfLabelZip
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 5.375"
.head 6 -  Top: 2.573"
.head 6 -  Width:  2.013"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Data Field: dfNumber
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: 2
.head 5 -  Data Type: Number
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 2.413"
.head 6 -  Top: 3.083"
.head 6 -  Width:  0.5"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Right
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbPrint
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print &Label
.head 4 -  Window Location and Size
.head 5 -  Left: 0.3"
.head 5 -  Top: 3.604"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If sCourtConst = 'CMC'
.head 7 -  Set sReport = 'Envelope.qrp'
.head 6 +  Else If sCourtConst = 'MMC'
.head 7 -  Set sReport = 'Labelmmc.qrp'
.head 6 +  Else If sCourtConst = 'AMC'
.head 7 -  Set sReport = 'Labels.qrp'
.head 6 +  If dfName != ''
.head 7 -  Call PrintLabels(  )
.head 5 +  On SAM_Create
.head 6 +  If sCourtConst = 'CMC'
.head 7 -  Call SalSetWindowText( pbPrint, 'Print Envelope' )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel, Exit
.head 4 -  Window Location and Size
.head 5 -  Left: 5.625"
.head 5 -  Top: 3.615"
.head 5 -  Width:  2.125"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 -  Background Text: bkgd6
.head 4 -  Resource Id: 36308
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 1.583"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 3 -  Background Text: bkgd7
.head 4 -  Resource Id: 36309
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 2.281"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address 2:
.head 3 -  Background Text: bkgd8
.head 4 -  Resource Id: 36310
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 2.688"
.head 5 -  Width:  0.913"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: City:
.head 3 -  Background Text: bkgd9
.head 4 -  Resource Id: 36305
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.75"
.head 5 -  Top: 3.104"
.head 5 -  Width:  1.5"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Number of Labels
.head 3 -  Background Text: bkgd10
.head 4 -  Resource Id: 36306
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.925"
.head 5 -  Top: 1.313"
.head 5 -  Width:  0.6"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Name:
.head 3 -  Background Text: bkgd11
.head 4 -  Resource Id: 36307
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 1.885"
.head 5 -  Width:  0.788"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address:
.head 3 +  Radio Button: rbDefendant
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: For Defendant - F1
.head 4 -  Window Location and Size
.head 5 -  Left: 0.775"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.763"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SalSendMsg( rbDefendant, SAM_Click, 0, 0 )
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( )
.head 3 +  Radio Button: rbDAttorney
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Defendant's Atty - F2
.head 4 -  Window Location and Size
.head 5 -  Left: 0.788"
.head 5 -  Top: 0.521"
.head 5 -  Width:  2.0"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call FillScreen( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbBondsman
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Bondsman - F3
.head 4 -  Window Location and Size
.head 5 -  Left: 0.775"
.head 5 -  Top: 0.875"
.head 5 -  Width:  1.538"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'BOND') = 1
.head 7 -  Call FillScreen( )
.head 5 +  ! On WM_KEYUP
.head 6 +  If wParam = VK_Insert
.head 7 -  Call SalTblReset( tblLabels )
.head 7 -  Call SalHideWindow( dfSearch )
.head 7 -  Set nRow = SalTblInsertRow( tblLabels, TBL_MaxRow )
.head 7 -  Set colLCaseNo = SalStrRightX( SalNumberToStrX( dfCaseYr, 0), 2) ||
	 frmMain.dfCaseType || SalNumberToStrX( dfCaseNo, 0)
.head 3 +  Radio Button: rbOtherAtt
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Attorney - F5
.head 4 -  Window Location and Size
.head 5 -  Left: 2.938"
.head 5 -  Top: 0.521"
.head 5 -  Width:  1.85"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'ATT') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbProbCS
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Probation/CS Agency - F7
.head 4 -  Window Location and Size
.head 5 -  Left: 4.85"
.head 5 -  Top: 0.521"
.head 5 -  Width:  2.413"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'PROBCS') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbOtherAgency
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Other Agency - F6
.head 4 -  Window Location and Size
.head 5 -  Left: 4.838"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.75"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'OTHERA') = 1
.head 7 -  Call FillScreen( )
.head 3 +  Radio Button: rbPolice
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Police Agency - F4
.head 4 -  Window Location and Size
.head 5 -  Left: 2.925"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.75"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If SalModalDialog(dlgLabelHelp, hWndForm, 'POLICE') = 1
.head 7 -  Call FillScreen( )
.head 3 +  ! Pushbutton: pbEnvelope		!7/7/99  MMC - not used
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Print &Envelope
.head 4 -  Window Location and Size
.head 5 -  Left: 0.125"
.head 5 -  Top: 5.292"
.head 5 -  Width:  2.175"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.end
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sReportType = 'Envelope.qrp'
.head 6 -  ! Call PrintLabels(  )
.head 6 -  Set bCertified = FALSE
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 +  ! Pushbutton: pbMailing		!7/7/99  MMC - not used
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Certificate of Mailing
.head 4 -  Window Location and Size
.head 5 -  Left: 2.375"
.head 5 -  Top: 5.292"
.head 5 -  Width:  2.175"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.end
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sReportType = 'Mailing.qrp'
.head 6 -  ! Call PrintLabels(  )
.head 6 -  Set bCertified = FALSE
.head 6 -  Call SalEndDialog( dlgLabels, 0 )
.head 3 -  Frame: frame1
.head 4 -  Resource Id: 36311
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.538"
.head 5 -  Top: 0.083"
.head 5 -  Width:  6.975"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.115"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Corners: Square
.head 4 -  Border Style: Solid
.head 4 -  Border Thickness: 1
.head 4 -  Border Color: Default
.head 4 -  Background Color: White
.head 3 +  Radio Button: rbScreen
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Screen
.head 4 -  Window Location and Size
.head 5 -  Left: 3.588"
.head 5 -  Top: 3.323"
.head 5 -  Width:  0.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: 3D Face Color
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Radio Button: rbPrinter
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Printer
.head 4 -  Window Location and Size
.head 5 -  Left: 3.588"
.head 5 -  Top: 3.073"
.head 5 -  Width:  0.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: 3D Face Color
.head 4 -  ToolTip:
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Set MyValue = TRUE
.head 3 -  Group Box: grp1
.head 4 -  Resource Id: 36312
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.65"
.head 5 -  Top: 1.281"
.head 5 -  Width:  6.813"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 1.698"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Address Information:
.head 3 +  Pushbutton: pb1
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F1
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDefendant = TRUE
.head 6 -  Call SalSendMsg(rbDefendant, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb2
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F2
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbDAttorney = TRUE
.head 6 -  Call SalSendMsg(rbDAttorney, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb3
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F3
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbBondsman = TRUE
.head 6 -  Call SalSendMsg(rbBondsman, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb4
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F4
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbPolice = TRUE
.head 6 -  Call SalSendMsg(rbPolice, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb5
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F5
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAtt = TRUE
.head 6 -  Call SalSendMsg(rbOtherAtt, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb6
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F6
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbOtherAgency = TRUE
.head 6 -  Call SalSendMsg(rbOtherAgency, SAM_Click, 0,0)
.head 3 +  Pushbutton: pb7
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.063"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: F7
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set rbProbCS = TRUE
.head 6 -  Call SalSendMsg(rbProbCS, SAM_Click, 0,0)
.head 2 +  Functions
.head 3 +  Function: PrintLabels
.head 4 -  Description: Call this function to 15/16 x 3 1/2 Inch Labels
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set sCSZ = dfLabelCity||', '||dfLabelState||'   '||dfLabelZip
.head 5 -  Set sAddress = dfAddress
.head 5 -  Set sAddress2 = dfAddress2
.head 5 +  If sAddress2 = ''
.head 6 -  Set sAddress2 = sCSZ
.head 6 -  Set sCSZ = ''
.head 5 -  Set nPrintErr = -1
.head 5 +  If rbDefendant = TRUE
.head 6 -  Set sCaseFullLe = sCaseYrP||'-'||sCaseTyP||'-'||sCaseNoP
.head 5 +  Else
.head 6 -  Set sCaseFullLe = ''
.head 5 -  Set sReportBinds = 'dfName,sAddress, sAddress2, sCSZ, sCaseFullLe'
.head 5 -  Set sReportInputs = 'DEF1, DEF1NAM2, DEF1ADR, DEF1CITY, CASENUM'
.head 5 +  If rbScreen = TRUE
.head 6 -  Call SalReportView( hWndForm, hWndNULL, sReport,
    sReportBinds, sReportInputs, nPrintErr )
.head 5 +  Else If rbPrinter = TRUE
.head 6 -  Call SalReportPrint ( dlgLabels, sReport, sReportBinds, sReportInputs,
	dlgLabels.dfNumber, RPT_PrintAll, nRow, nMaxRow, nPrintErr )
.head 5 +  If nPrintErr > 0
.head 6 -  Call SalMessageBox( 'PRINT ERROR', SalNumberToStrX( nPrintErr,0),
	MB_Ok )
.head 3 +  Function: FillScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sID
.head 4 +  Actions
.head 5 +  If rbDefendant = TRUE
.head 6 -  ! Call SqlPrepareAndExecute(hSql, "SELECT	lname||', '||fname||' '||mname, address1, address2, city, state, zip
			     FROM		jcr_master
			     INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP")
.head 6 -  Call SqlPrepareAndExecute(hSql,"SELECT		trim(i.fname || ' '|| i.mname||' '|| i.lname||' '|| i.title), i.id
		         		FROM		id_master i, jcr_master m
		          		WHERE		m.caseyr = :sCaseYrP and
						m.casety = :sCaseTyP and
						m.caseno = :sCaseNoP and
						m.id = i.id
		          		INTO		:dfName, :sID")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 -  Call SqlPrepareAndExecute (hSql, "SELECT	 address1, address2, city, state, zip
			          FROM	JCR_Address
			          INTO	:dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			          WHERE	ID = :sID
			          ORDER BY	entrydate desc")
.head 6 -  Call SqlFetchNext(hSql,nReturn)
.head 6 +  If sCaseNoP = ''
.head 7 -  Call ClearScreen( )
.head 5 +  Else If rbDAttorney = TRUE
.head 6 -  If sCaseNoP = '' and sCaseYrP = '' and sCaseTyP = ''
.head 6 +  Else
.head 7 -  Call SqlPrepareAndExecute(hSql, 'SELECT	attno
			     FROM		jcr_master
			     INTO		:nAttNo
			      WHERE	caseyr = :sCaseYrP and
					casety = :sCaseTyP and
					caseno = :sCaseNoP')
.head 7 -  Call SqlFetchNext(hSql, nReturn)
.head 7 -  If nAttNo = NUMBER_Null
.head 7 +  Else
.head 8 -  Call SqlPrepareAndExecute(hSql, "SELECT	 fname||' '||mname||' '||lname, address, address2,
					 city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	attno = :nAttNo")
.head 8 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbBondsman = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	bondsman,address1, address2, city, state, zip1
			      FROM		jcr_bondsman
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbOtherAtt = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	fname||' '||mname||' '||lname,address, address2, city, state, zip
			      FROM		attorney
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbPolice = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agency,address1, address2, city, state, zip
			      FROM		jcr_agency_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbOtherAgency = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	agencynm,address1, address2, city, state, zip
			      FROM		other_agency
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 5 +  Else If rbProbCS = TRUE
.head 6 -  Call SqlPrepareAndExecute(hSql, "SELECT	description, supervisor ,address1, city, state, zip
			      FROM		program_codes
			      INTO		:dfName, :dfAddress, :dfAddress2, :dfLabelCity, :dfLabelState, :dfLabelZip
			      WHERE	rowid = :sReturnRowid")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If dfAddress != ''
.head 7 -  Set dfAddress = 'ATTN. '||dfAddress
.head 6 +  Else
.head 7 -  Set dfAddress = dfAddress2
.head 3 +  Function: ClearScreen
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set dfName = ''
.head 5 -  Set dfAddress = ''
.head 5 -  Set dfAddress2 = ''
.head 5 -  Set dfLabelCity = ''
.head 5 -  Set dfLabelState = ''
.head 5 -  Set dfLabelZip = ''
.head 5 -  Set sCSZ = ''
.head 2 +  Window Parameters
.head 3 -  String: sCaseYrP
.head 3 -  String: sCaseTyP
.head 3 -  String: sCaseNoP
.head 2 +  Window Variables
.head 3 -  ! String: sFind
.head 3 -  ! String: sSearch
.head 3 -  Number: nRow
.head 3 -  Number: nMaxRow
.head 3 -  String: sAddress2
.head 3 -  String: sAddress
.head 3 -  Number: nAttNo
.head 3 -  String: sCSZ
.head 3 -  String: sCaseFullLe
.head 3 -  ! String: sCode1
.head 3 -  ! String: sCode2
.head 3 -  ! String: sCode3
.head 3 -  ! String: sLECode
.head 3 -  ! String: sLCaseNo
.head 3 -  ! String: sLDefName
.head 3 -  ! String: sLDefAddr2
.head 3 -  ! String: sLDefAddr
.head 3 -  ! String: sLDefCSZ
.head 3 -  ! String: sLPlaName
.head 3 -  ! String: sLPlaAddr2
.head 3 -  ! String: sLPlaAddr
.head 3 -  ! String: sLPlaCSZ
.head 3 -  ! String: sNDefName
.head 3 -  ! String: sNDefName2
.head 3 -  ! String: sNPlaName
.head 3 -  ! String: sNPlaName2
.head 3 -  ! Date/Time: dtHearDt
.head 3 -  ! Number: nOffSet
.head 3 -  ! String: sBDebtNtcService
.head 3 -  Number: nRptFlag
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call SalSetDefButton( pbPrint )
.head 4 -  Set dfNumber= 1
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 3 +  On SAM_ReportStart
.head 4 -  Set nRptFlag=0
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 -  Set nRptFlag=nRptFlag+1
.head 4 +  If nRptFlag=1
.head 5 -  Return TRUE
.head 4 +  Else
.head 5 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  Dialog Box: dlgDatasource
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Input Source
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.913"
.head 4 -  Top: 3.26"
.head 4 -  Width:  2.275"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.114"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 3 -  ! Resizable? No
.head 2 +  Contents
.head 3 +  Data Field: dfReturn
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.038"
.head 6 -  Top: 0.021"
.head 6 -  Width:  2.125"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbExit
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.975"
.head 5 -  Top: 0.542"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? No
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Set sDatasource = SalStrMidX( dfReturn, 1, 100)
.head 6 -  Call SalEndDialog(dlgDatasource, 1)
.head 2 -  Functions
.head 2 +  Window Parameters
.head 3 -  Receive String: sDatasource
.head 2 -  Window Variables
.head 2 -  Message Actions
.head 2 -  ! Resizable? No
.head 2 -  ! Vertical Scroll? Yes
.head 2 -  ! Horizontal Scroll? Yes
.head 1 +  ! Dialog Box: dlgAbout
.winattr
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title:
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.225"
.head 4 -  Top: 1.896"
.head 4 -  Width:  4.883"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.714"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 8
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: Sky
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.end
.head 2 -  Description: 
.head 2 +  Tool Bar 
.winattr
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.end
.head 3 -  Contents 
.head 2 +  Contents 
.head 3 +  Pushbutton: pbOk
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 3.688"
.head 5 -  Top: 1.24"
.head 5 -  Width:  0.9"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.end
.head 4 +  Message Actions 
.head 5 +  On SAM_Click
.head 6 -  Call SalEndDialog( dlgAbout, 1)
.head 3 -  Background Text: Program Name:
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.113"
.head 5 -  Top: 0.177"
.head 5 -  Width:  1.3"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title:
.end
.head 3 -  Background Text: Revision Date:
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.113"
.head 5 -  Top: 0.531"
.head 5 -  Width:  1.233"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title:
.end
.head 3 -  Background Text: Revision Number:
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.113"
.head 5 -  Top: 0.896"
.head 5 -  Width:  1.467"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title:
.end
.head 3 +  Data Field: dfAboutName
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.688"
.head 6 -  Top: 0.179"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: 10
.head 5 -  Font Enhancement: Bold
.head 5 -  Text Color: Default
.head 5 -  Background Color: Sky
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.end
.head 4 -  Message Actions 
.head 3 +  Data Field: dfAboutRDate
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.688"
.head 6 -  Top: 0.536"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: DateTime
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: Sky
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.end
.head 4 -  Message Actions 
.head 3 +  Data Field: dfAboutRNumber
.winattr
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 1.688"
.head 6 -  Top: 0.893"
.head 6 -  Width:  2.9"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Italic
.head 5 -  Text Color: Default
.head 5 -  Background Color: Sky
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.end
.head 4 -  Message Actions 
.head 2 -  Functions 
.head 2 +  Window Parameters 
.head 3 -  String: sAboutName
.head 3 -  Date/Time: dtAboutRDate
.head 3 -  String: sAboutRNumber
.head 2 -  Window Variables 
.head 2 +  Message Actions 
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 4 -  Call SalCenterWindow( hWndForm )
.head 4 -  Set dfAboutName = sAboutName
.head 4 -  Set dfAboutRDate = dtAboutRDate
.head 4 -  Set dfAboutRNumber = sAboutRNumber
.head 4 -  Call SalSetWindowText( hWndForm, 'About '||sAboutName )
.head 1 +  Dialog Box: dlgPrintDocket
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Print Job In Progress
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: 3.325"
.head 4 -  Top: 2.375"
.head 4 -  Width:  4.486"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 0.781"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Times New Roman
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: Bold
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd1
.head 4 -  Resource Id: 22438
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.186"
.head 5 -  Top: 0.25"
.head 5 -  Width:  4.043"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.229"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: 14
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Printing Docket Please Wait....
.head 2 +  Functions
.head 3 +  Function: PrintDocket
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: fDefendant
.head 5 -  Number: nOldCount
.head 5 -  String: sID
.head 4 +  Actions
.head 5 -  Set nPrintDocket=1
.head 5 -  Call SqlPrepareAndExecute(hSql,"SELECT		trim(i.fname || ' '|| i.mname||' '|| i.lname||' '|| i.title), m.agency, i.id
		         		FROM		id_master i, jcr_master m
		          		WHERE		m.caseyr = :fCaseYr and
						m.casety = :fCaseTy and
						m.caseno = :fCaseNo and
						m.id = i.id
		          		INTO		:fDefendant, :sDockAgency, :sID")
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 -  Call SqlPrepareAndExecute(hSql, "Select address1 || ' ' || City  || ' ' || State  || ' ' || Zip
	from jcr_address into :sAddressRD
	where id = :sID
	 order by entrydate desc")
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If fReportType = 'D'
.head 6 -  Set sReport = 'crdock.qrp'
.head 6 -  Set sReportBinds = 'sDockCase, :fDefendant,sDockAgency,dDockDate,
sDockText,dDockRecDate,sDockRec,sDockRecCode,nDockAmount, sCourt, sCourtClerk, sAddressRD'
.head 6 -  Set sReportInputs = 'CASE,DEF,AGENCY,DDATE,DTEXT,RDATE,REC,RCODE,AMOUNT, COURT, CLERK, Addr'
.head 5 +  Else If fReportType = 'R'
.head 6 -  ! Check for older Unix disb.  This report will not include those, but we can at least let the user know they exist.
.head 6 -  Call SqlPrepareAndExecute( hSql,"Select 	count(*)
			         FROM	JCR_DisbMaster
			         INTO	:nOldCount
			         WHERE	(CaseYr=:fCaseYr and CaseTy=:fCaseTy and CaseNo=:fCaseNo) and
					disbdate < to_date('4-30-2002', 'MM-dd-yyyy')")
.head 6 -  Call SqlFetchNext(hSql, nReturn)
.head 6 +  If nOldCount > 0
.head 7 -  Call SalMessageBox( 'Disbursements exist for this case that are prior to the conversion date of 4-30-2002.  These will not be printed on this report.', 'Old Disbursement records', MB_Ok| MB_IconAsterisk )
.head 6 -  Set sReport = 'crdock2.qrp'
.head 6 -  Set sReportBinds = 'sDockCase, :fDefendant,sDockAgency,dTranDateRD, nRecCheckNoRD,
		  sCashierRD, sTypeRD, sAdjRD, sLedgerRD, sStatute_DepRD,
		  nAmountRD, nTotalRD,sCourt, sCourtClerk, sReportBreakRD,
		  sDateHeader, sRec_CheckHeader, sStat_DepHeader, sAddressRD'
.head 6 -  Set sReportInputs = 'CASE,DEF,AGENCY,TRANDATE, REC_CHECK,
		  CASHIER, TYPE, ADJUSTMENT, LEDGER, STATUTE,
		  AMOUNT, TOTAL, COURT, CLERK, RPT_TYPE,
		  TRAN_DATE_HDR, REC_CHECK_HDR, STAT_DEP_HDR, Addr'
.head 5 +  If fPrintType = 'SCREEN'
.head 6 -  Call SalReportView( dlgPrintDocket, hWndNULL, sReport,
    sReportBinds, sReportInputs, nPrintErr )
.head 5 +  Else If fPrintType = 'PRINTER'
.head 6 -  Call SalReportPrint(hWndForm,sReport, sReportBinds, sReportInputs,1,RPT_PrintAll,0,0,nPrintDocket)
.head 2 +  Window Parameters
.head 3 -  String: fCaseYr
.head 3 -  String: fCaseTy
.head 3 -  String: fCaseNo
.head 3 -  String: fPrintType
.head 3 -  String: fReportType   ! 'D' = Docket Normal, 'R' = Receipt/Disbursements
.head 2 +  Window Variables
.head 3 -  ! !!!!!!! VARIABLES FOR PRINTING DOCKET
.head 3 -  Boolean: bDockDone
.head 3 -  Date/Time: dDockDate
.head 3 -  Long String: sDockText
.head 3 -  String: sDockRec
.head 3 -  String: sDockRecCode
.head 3 -  Date/Time: dDockRecDate
.head 3 -  String: nDockAmount
.head 3 -  Number: nDockAmountNo
.head 3 -  String: sDockCase
.head 3 -  Number: nPrintDocket
.head 3 -  ! !!!!!!! VARIABLES FOR PRINTING DOCKET - END
.head 3 -  Date/Time: dTranDateRD
.head 3 -  Number: nRecCheckNoRD
.head 3 -  String: sCashierRD
.head 3 -  String: sTypeRD
.head 3 -  String: sAdjRD
.head 3 -  String: sLedgerRD
.head 3 -  String: sStatute_DepRD
.head 3 -  Number: nAmountRD
.head 3 -  Number: nTotalRD
.head 3 -  String: sReportBreakRD
.head 3 -  String: sDateHeader
.head 3 -  String: sRec_CheckHeader
.head 3 -  String: sStat_DepHeader
.head 3 -  String: sAddressRD
.head 2 +  Message Actions
.head 3 +  On SAM_ReportStart
.head 4 -  Set bDockDone=FALSE
.head 4 +  If fReportType = 'D'
.head 5 -  Set dDockDate=SalStrToDate(String_Null)
.head 5 -  Set sDockText=String_Null
.head 5 -  Set sDockRec=String_Null
.head 5 -  Set sDockRecCode=String_Null
.head 5 -  Set dDockRecDate=SalStrToDate(String_Null)
.head 5 -  Set nDockAmount=String_Null
.head 5 -  Set nDockAmountNo=SalStrToNumber(String_Null)
.head 5 -  Call SqlPrepare(hSql,'SELECT		Dock_Date,Data
		        FROM		JCR_DOCKET
		        WHERE		CaseYr=:fCaseYr  and
					CaseTy=:fCaseTy  and
					CaseNo= :fCaseNo
		        INTO		:dDockDate,:sDockText
order by Dock_Date,Seq,RowId ')
.head 5 -  Call SqlExecute(hSql)
.head 4 +  Else If fReportType = 'R' ! Receipt /Disbursement Report
.head 5 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 5 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 5 -  Set sCashierRD=String_Null
.head 5 -  Set sTypeRD=String_Null
.head 5 -  Set sAdjRD=String_Null
.head 5 -  Set sLedgerRD=String_Null
.head 5 -  Set sStatute_DepRD=String_Null
.head 5 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 5 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 5 -  Set sDateHeader = 'Rec Date'
.head 5 -  Set sRec_CheckHeader = 'Receipt #'
.head 5 -  Set sStat_DepHeader = 'Statute'
.head 5 -  Set sReportBreakRD = ''
.head 5 -  Call SqlPrepare(hSql,"Select m.Dat, m.Receiptno, m.Cashierno || ' / ' || m.Userid, m.Pay_Type, m.Void, m.Ledger, m.Offenseno, m.Amount, 'RECEIPT'
	from JCR_RECMaster m
	where (m.CaseYr=:fCaseYr and m.CaseTy=:fCaseTy and m.CaseNo=:fCaseNo)
	into :dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sTypeRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
	order by 1,2")
.head 5 -  Call SqlExecute(hSql)
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchInit
.head 4 -  Return TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If fReportType = 'D'
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If nReturn=FETCH_EOF
.head 6 +  If bDockDone=TRUE
.head 7 -  Return FALSE
.head 6 +  Else
.head 7 -  Set bDockDone=TRUE
.head 7 -  Set dDockDate=SalStrToDate(String_Null)
.head 7 -  Set sDockText=String_Null
.head 7 -  Call SqlPrepare(hSql,'SELECT		Dat,ReceiptNo,Ledger,Amount
		        FROM		JCR_RECMASTER
		        WHERE		CaseYr=:fCaseYr  and
					CaseTy= :fCaseTy  and
					CaseNo= :fCaseNo
                                        INTO		:dDockRecDate,:sDockRec,:sDockRecCode,:nDockAmountNo
		        ORDER BY		DAT')
.head 7 -  Call SqlExecute(hSql)
.head 7 -  Call SqlFetchNext(hSql,nReturn)
.head 7 +  If nDockAmountNo >0
.head 8 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 8 -  Set nDockAmount='$'||nDockAmount
.head 8 -  Set sDockRec='Receipt      '||sDockRec
.head 8 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 7 -  Return TRUE
.head 5 +  Else
.head 6 -  Set sDockCase= fCaseYr||' '||fCaseTy||' '||fCaseNo
.head 6 +  If nDockAmountNo >0
.head 7 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 7 -  Set nDockAmount='$'||nDockAmount
.head 7 -  Set sDockRec='Receipt      '||sDockRec
.head 7 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 6 -  Return TRUE
.head 4 +  Else If fReportType = 'R'
.head 5 -  Call SqlFetchNext(hSql,nReturn)
.head 5 +  If nReturn=FETCH_EOF
.head 6 +  If bDockDone=TRUE
.head 7 -  Return FALSE
.head 6 +  Else
.head 7 -  Set bDockDone=TRUE
.head 7 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 7 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 7 -  Set sCashierRD=String_Null
.head 7 -  Set sTypeRD=String_Null
.head 7 -  Set sAdjRD=String_Null
.head 7 -  Set sLedgerRD=String_Null
.head 7 -  Set sStatute_DepRD=String_Null
.head 7 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 7 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 7 -  Set sDateHeader = 'Dis Date'
.head 7 -  Set sRec_CheckHeader = 'Check #'
.head 7 -  Set sStat_DepHeader = 'Depositor'
.head 7 -  Set sReportBreakRD = ''
.head 7 -  Call SqlPrepare(hSql,"Select M.DisbDate, M.BankNo, M.Userid, M.CheckStatus, D.Ledger, M.Depositor, D.Amount, 'DISBURSEMENT'
   from	JCR_DisbMaster M, JCR_DisbDetail D
   into	:dTranDateRD, :nRecCheckNoRD, :sCashierRD, :sAdjRD, :sLedgerRD, :sStatute_DepRD, :nAmountRD, :sReportBreakRD
   where	(M.CheckNo = D.CheckNo) and
	(M.CaseYr=:fCaseYr and M.CaseTy=:fCaseTy and M.CaseNo=:fCaseNo) and
	M.disbdate >=to_date('4-30-2002', 'MM-dd-yyyy')
	order by M.DisbDate, D.Ledger")
.head 7 -  Call SqlExecute(hSql)
.head 7 -  Call SqlFetchNext(hSql,nReturn)
.head 7 +  If nReturn = FETCH_EOF
.head 8 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 8 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 8 -  Set sCashierRD=String_Null
.head 8 -  Set sTypeRD=String_Null
.head 8 -  Set sAdjRD=String_Null
.head 8 -  Set sLedgerRD=String_Null
.head 8 -  Set sStatute_DepRD=String_Null
.head 8 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 8 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 8 -  Set sReportBreakRD = ''
.head 7 +  Else
.head 8 -  Return TRUE
.head 5 +  Else
.head 6 -  Set sDockCase= fCaseYr||' '||fCaseTy||' '||fCaseNo
.head 6 +  ! If nDockAmountNo >0
.head 7 -  Call SalNumberToStr(nDockAmountNo,2,nDockAmount)
.head 7 -  Set nDockAmount='$'||nDockAmount
.head 7 -  Set sDockRec='Receipt      '||sDockRec
.head 7 -  Set sDockRecCode='Code   -  '||sDockRecCode
.head 6 -  ! Return TRUE
.head 6 +  If nAmountRD = 0
.head 7 -  Set dTranDateRD=SalStrToDate(String_Null)
.head 7 -  Set nRecCheckNoRD=SalStrToNumber(String_Null)
.head 7 -  Set sCashierRD=String_Null
.head 7 -  Set sTypeRD=String_Null
.head 7 -  Set sAdjRD=String_Null
.head 7 -  Set sLedgerRD=String_Null
.head 7 -  Set sStatute_DepRD=String_Null
.head 7 -  Set nAmountRD=SalStrToNumber(String_Null)
.head 7 -  Set nTotalRD=SalStrToNumber(String_Null)
.head 7 -  Set sReportBreakRD = ''
.head 6 +  Else
.head 7 -  Return TRUE
.head 3 +  On SAM_ReportFinish
.head 4 -  Return FALSE
.head 3 +  On SAM_CreateComplete
.head 4 -  Call PrintDocket(  )
.head 4 -  Call SalEndDialog( hWndForm, 1 )
.head 3 +  On SAM_Create
.head 4 -  Call CSCDisableNonEditable( hWndForm )
.head 1 +  Dialog Box: dlgPrintReport
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Printing...
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modeless
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top: Default
.head 4 -  Width:  Default
.head 4 -  Width Editable? Yes
.head 4 -  Height: Default
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: 3D Face Color
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 10611
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.038"
.head 5 -  Top: 0.302"
.head 5 -  Width:  4.85"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.271"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Arial Black
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: None
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: REPORT IS PRINTING
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 10612
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: -0.013"
.head 5 -  Top: 0.677"
.head 5 -  Width:  4.95"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Center
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: PLEASE WAIT . . .
.head 2 -  Functions
.head 2 -  Window Parameters
.head 2 +  Window Variables
.head 3 -  Boolean: bPrintOnce
.head 3 -  Boolean: bPrintGeneric
.head 3 -  Boolean: bPrintCustom
.head 3 -  Number: n
.head 2 +  Message Actions
.head 3 +  On SAM_ReportFetchInit
.head 4 -  ! Do init steps here per report, and set mode
.head 4 -  If Reports.sCurReport = "report.qrp"
.head 4 +  Else
.head 5 -  Set bPrintOnce = TRUE
.head 3 +  On SAM_ReportFetchNext
.head 4 +  If bPrintOnce = TRUE
.head 5 -  Set bPrintOnce = FALSE
.head 5 -  Return TRUE
.head 4 +  Else If bPrintGeneric = TRUE
.head 5 -  Return SqlFetchNext( hSql, nResult )
.head 4 +  Else If bPrintCustom = TRUE
.head 5 -  ! Handle each report individually here
.head 4 -  Return FALSE
.head 3 +  On SAM_ReportFinish
.head 4 -  Call SalDestroyWindow( hWndForm )
.head 3 +  On SAM_Create
.head 4 -  Set Reports.bPrinting = TRUE
.head 3 +  On SAM_Destroy
.head 4 -  Set Reports.bPrinting = FALSE
.head 4 -  Call Reports.Reset_Printer(  )
.head 1 +  ! Dialog Box: dlgViewCrystal
.winattr
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Report Preview
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top: Default
.head 4 -  Width:  13.113"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 8.125"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.end
.head 2 -  Description: 
.head 2 +  Tool Bar 
.winattr
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.end
.head 3 -  Contents 
.head 2 -  Contents 
.head 2 +  Functions 
.head 3 +  Function: wfOnCreate
.head 4 -  Description: 
.head 4 -  Returns 
.head 4 -  Parameters 
.head 4 -  Static Variables 
.head 4 +  Local variables 
.head 5 -  FunctionalVar: vRep
.winattr class
.head 6 -  Class: Variant
.end
.head 4 +  Actions 
.head 5 -  Call Reports.Prepare_Crystal( sReport, RepParams )
.head 5 -  Call vRep.SetObject( Reports.rep )
.head 5 -  Call axReport.PropSetReportSource( vRep )
.head 5 -  Call axReport.ViewReport(  )
.head 2 +  Window Parameters 
.head 3 -  String: sReport
.head 3 -  FunctionalVar: RepParams
.winattr class
.head 4 -  Class: cCrystalParams
.end
.head 2 -  Window Variables 
.head 2 +  Message Actions 
.head 3 +  On SAM_Create
.head 4 -  Call wfOnCreate()
.head 1 +  Dialog Box: dlgDateTime
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Select Date
.head 2 -  Accessories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Allow Dock to Parent? No
.head 3 -  Docking Orientation: All
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top: Default
.head 4 -  Width:  6.671"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 4.74"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Arial
.head 3 -  Font Size: 10
.head 3 -  Font Enhancement: None
.head 3 -  Text Color: Default
.head 3 -  Background Color: 3D Face Color
.head 3 -  Resizable? No
.head 3 -  Vertical Scroll? Yes
.head 3 -  Horizontal Scroll? Yes
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Docking Toolbar? No
.head 4 -  Toolbar Docking Orientation: Top | Bottom
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Resizable? No
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Combo Box: cmbDiff
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 1.971"
.head 5 -  Top: 0.073"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 2.271"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? Yes
.head 4 -  String Type: String
.head 4 -  Maximum Data Length: Default
.head 4 -  Sorted? No
.head 4 -  Always Show List? No
.head 4 -  Vertical Scroll? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: White
.head 4 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  AutoFill? Yes
.head 4 +  List Initialization
.head 5 -  Text: 7
.head 5 -  Text: 14
.head 5 -  Text: 21
.head 5 -  Text: 30
.head 5 -  Text: 60
.head 5 -  Text: 90
.head 5 -  Text: 120
.head 4 +  Message Actions
.head 5 +  On SAM_AnyEdit
.head 6 -  Call Diff_Update()
.head 5 +  On SAM_Click
.head 6 -  Call Diff_Update()
.head 3 +  Data Field: dfTime
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: Date/Time
.head 5 -  Editable? Yes
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.938"
.head 6 -  Top: 4.24"
.head 6 -  Width:  1.2"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? Yes
.head 5 -  Border? Yes
.head 5 -  Justify: Left
.head 5 -  Format: hh:mm AMPM
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: White
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 +  ActiveX: axDate
.data CLASSPROPSSIZE
0000: 3200
.enddata
.data CLASSPROPS
0000: 4C00690063004B00 650079004C006500 6E00000000000400 920000004C006900
0020: 63004B0065007900 0000FFFE04003600 0000
.enddata
.data RESOURCE 5 0 1 2721968544
0000: 000A0000CE020000 0000000000000000 020000D0CF11E0A1 B11AE1F8000000FF
0020: 003E000300FEFF09 00F0060000000F01 000000F601000110 0000020000002601
0040: 00FEFFFFF8000000 F8FFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
0060: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
0080: FF40FDFFFFFFFEFF FF66FEFFFEFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
00A0: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
00C0: FFFFFFFFFFFFFFFF FFFFFFFF2052006F 006F740081004500 6E0074C072007900
00E0: 0000FFFFFFFFFFA0 16000500FFFF4F01 0000006A452E23C3 87D111008BE30000
0100: F8754DA1F8000000 0F90D4FDA300FE0A C60103000000ED01 00284A0045455000
0120: 0853007243004F00 C24F4C000000FFFF FFFFFF82160201FF FFFFFFFA0000FFFF
0140: FFFFF0C8000000FF FFFFFFFFFFFFFFAF FFFFFFFA0000FFFF FFFFFFFFFFFFFFFF
0160: FFFFFFAFFFFFFFFA 0000FFFFFFFFFF46 0100020000660300 FEFFFFFFFFFFFFFF
0180: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF
01A0: FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF FFFFFFFFFFFFFFFF 0021433412080000
01C0: 0000CA2A00009624 0000801AB15FC000 0006022000001209 000010D607080000
01E0: 1800FC0000200F27 0C00051F00FC0000 E0D906010001FC00 0020D5070C00021B
0200: 00000B0000001500 5102FCD507E70015 0001000700040000 008001EFCDAB0000
0220: 0524080000021409 0200003000000030 0090F21200000080 00801FDEECBD0100
0240: 0508B5F2120352E3 0B00918FCE119DE3 00AA00004BB85101 000000009001DC7C
0260: 01000541C0726961 6C7269F8000000FF FF071E00A805CE50 00000016A802F00C
0280: 0000000840080019 0000006202000079 00FFFFFFFFFFFFFF 1F0900BA4005AC00
02A0: 000002BAC1013200 00003F1700080000 0200006969F06400 0000FFFFFFFFFFFF
02C0: 7F2021C404CE0000 1665C4020C008FA2 08006619000200DE 6900FFFFFFFFFFFF
.enddata
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class: AX_MSComCtl2_MonthView
.head 4 -  Property Template:
.head 4 -  License Key:
.head 4 -  Display Settings
.head 5 -  GUID:
.head 5 -  Type:
.head 5 -  Activation: Class Default
.head 5 -  Host Name:
.head 5 -  Window Location and Size
.head 6 -  Left: 0.388"
.head 6 -  Top: 0.375"
.head 6 -  Width:  5.986"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 3.698"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Border? Class Default
.head 5 -  Etched Border? Class Default
.head 5 -  Hollow? Class Default
.head 5 -  Tab Stop? Class Default
.head 5 -  Tile To Parent? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 4 +  Message Actions
.head 5 +  On DateClick
.data ONX_INT
0000: 01000000
.enddata
.data CLASSPROPSSIZE
0000: 6400
.enddata
.data CLASSPROPS
0000: 500052004F005000 5F00490049004400 0000FFFE4E007B00 3200330032004500
0020: 3400350036003900 2D00380037004300 33002D0031003100 440031002D003800
0040: 4200450033002D00 3000300030003000 4600380037003500 3400440041003100
0060: 7D000000
.enddata
.head 6 +  Parameters
.head 7 -  Date/Time: DateClicked
.head 6 +  Actions
.head 7 -  Set cmbDiff = SalNumberToStrX( DateClicked - Current_Date( 'Timestamp'  ) + 1, 0 )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 3.857"
.head 5 -  Top: 4.198"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call SalDestroyWindow( hWndForm )
.head 3 +  Pushbutton: pbOK
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: OK
.head 4 -  Window Location and Size
.head 5 -  Left: 5.157"
.head 5 -  Top: 4.198"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.292"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Button Appearance: Standard
.head 4 -  ToolTip:
.head 4 -  Image Alignment: Default
.head 4 -  Text Alignment: Default
.head 4 -  Text Image Relation: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call Set_Date()
.head 3 +  Data Field: dfTimeMask
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Data
.head 5 -  Maximum Data Length: Default
.head 5 -  Data Type: String
.head 5 -  Editable? No
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left: 0.288"
.head 6 -  Top: 4.302"
.head 6 -  Width:  0.65"
.head 6 -  Width Editable? Yes
.head 6 -  Height: 0.25"
.head 6 -  Height Editable? Yes
.head 5 -  Visible? No
.head 5 -  Border? No
.head 5 -  Justify: Left
.head 5 -  Format: Unformatted
.head 5 -  Country: Default
.head 5 -  Font Name: Default
.head 5 -  Font Size: Default
.head 5 -  Font Enhancement: Default
.head 5 -  Text Color: Default
.head 5 -  Background Color: Default
.head 5 -  Input Mask: Unformatted
.head 4 -  ToolTip:
.head 4 -  Message Actions
.head 3 -  Background Text: bkgd2
.head 4 -  Resource Id: 834
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 4.302"
.head 5 -  Width:  0.5"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Time:
.head 3 -  Background Text: bkgd3
.head 4 -  Resource Id: 835
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.388"
.head 5 -  Top: 0.094"
.head 5 -  Width:  1.486"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.167"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Justify: Left
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 -  Title: Days from today:
.head 2 +  Functions
.head 3 +  Function: On_Create
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sDate
.head 5 -  Date/Time: dtDateTime
.head 4 +  Actions
.head 5 +  If SalIsValidDateTime( hField )
.head 6 -  Call SalGetWindowText( hField, sDate, 999 )
.head 6 -  Set dtDateTime = SalStrToDate( sDate )
.head 5 +  Else
.head 6 -  Set dtDateTime = SalDateCurrent(  )
.head 5 -  Call axDate.PropSetValue( dtDateTime )
.head 5 -  Set dfTime = SalStrToDate( "8:30 AM" )
.head 5 +  If Not bShowTime
.head 6 -  Call SalHideWindow( dfTime )
.head 6 -  Call SalDisableWindow( dfTimeMask )
.head 6 -  Call SalShowWindow( dfTimeMask )
.head 5 -  Set cmbDiff = SalNumberToStrX( dtDateTime - Current_Date( 'Timestamp'  ), 0 )
.head 3 +  Function: Set_Date
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  String: sFormat
.head 5 -  Date/Time: dtDate
.head 5 -  Date/Time: dtTime
.head 5 -  Date/Time: dtDateTime
.head 5 -  String: sDate
.head 4 +  Actions
.head 5 -  Call axDate.PropGetValue( dtDate )
.head 5 -  Set dtTime = dfTime
.head 5 -  Set dtDateTime = SalDateConstruct( SalDateYear( dtDate ), SalDateMonth( dtDate ), SalDateDay( dtDate ), SalDateHour( dtTime ), SalDateMinute( dtTime ), SalDateSecond( dtTime ) )
.head 5 +  If bShowTime
.head 6 -  Set sFormat = "M/d/yyyy hh:mm AMPM"
.head 5 +  Else
.head 6 -  Set sFormat = "M/d/yyyy"
.head 5 -  Set sDate = SalFmtFormatDateTime( dtDateTime, sFormat )
.head 5 -  Call SalSetWindowText( hField, sDate )
.head 5 -  Call SalDestroyWindow( hWndForm )
.head 3 +  Function: Diff_Update
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call axDate.PropSetValue( SalDateCurrent() + SalStrToNumber( cmbDiff ) )
.head 2 +  Window Parameters
.head 3 -  Window Handle: hField
.head 3 -  Boolean: bShowTime
.head 2 -  Window Variables
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call On_Create()
