.head 0 +  Application Description: M!Table Properties Library
.head 1 -  Outline Version - 4.0.27
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000020100 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE9FFFFFFFFFFFF FF000000007C0200 004D010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D0100000007436C 6173736573
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left: -0.013"
.head 3 -  Top:    0.0"
.head 3 -  Width:  8.013"
.head 3 -  Height: 4.969"
.head 2 +  Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top:    1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top:    0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 6.388"
.head 3 -  Top:    0.729"
.head 2 -  Fully Qualified External References? Yes
.head 2 -  Reject Multiple Window Instances? No
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 1 +  Libraries
.head 2 -  File Include: cdk.apl
.head 2 -  File Include: mimg.apl
.head 2 -  File Include: mtbl.apl
.head 2 -  File Include: vtarray.apl
.head 2 -  File Include: vtcomm.apl
.head 2 -  File Include: vtstr.apl
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: 3D Face Color
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 3 +  Library name: gdi32.dll
.head 4 +  Function: CreateCompatibleBitmap
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 4 +  Function: CreateCompatibleDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: CreateHatchBrush
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: INT
.head 6 -  Number: DWORD
.head 4 +  Function: CreatePen
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: DWORD
.head 4 +  Function: CreateSolidBrush
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: DWORD
.head 4 +  Function: DeleteDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: DeleteObject
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 4 +  Function: LineTo
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 4 +  Function: MoveToEx
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 4 +  Function: Rectangle
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 6 -  Number: INT
.head 4 +  Function: SelectObject
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 -  Number: HANDLE
.head 3 +  Library name: user32.dll
.head 4 +  Function: FillRect
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 +  structPointer
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 6 -  Number: HANDLE
.head 4 +  Function: FrameRect
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Number: HANDLE
.head 6 +  structPointer
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 7 -  Number: LONG
.head 6 -  Number: HANDLE
.head 4 +  Function: GetClientRect
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Boolean: BOOL
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 +  structPointer
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 7 -  Receive Number: LONG
.head 4 +  Function: GetDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: HANDLE
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 4 +  Function: ReleaseDC
.head 5 -  Description:
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: INT
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 -  Number: HANDLE
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 +  System
.head 4 -  !
.head 4 -  ! *** MTPROPS.APL ***
.head 4 -  ! Common
.head 4 -  String: MTPROP_CRLF = "
"
.head 4 -  ! Types
.head 4 -  Number: MTPROP_TYPE_BOOL = 1
.head 4 -  Number: MTPROP_TYPE_NUMBER = 2
.head 4 -  Number: MTPROP_TYPE_STRING = 3
.head 4 -  Number: MTPROP_TYPE_STRING_ML = 4
.head 4 -  Number: MTPROP_TYPE_COLOR = 5
.head 4 -  Number: MTPROP_TYPE_LINE_STYLE = 6
.head 4 -  Number: MTPROP_TYPE_COLUMN = 7
.head 4 -  Number: MTPROP_TYPE_FONT_NAME = 8
.head 4 -  Number: MTPROP_TYPE_FONT_SIZE = 9
.head 4 -  Number: MTPROP_TYPE_ECPD_ORDER = 10
.head 4 -  Number: MTPROP_TYPE_IMAGE_NAME = 11
.head 4 -  ! Values
.head 4 -  String: MTPROP_VAL_BOOL_NO = "0"
.head 4 -  String: MTPROP_VAL_BOOL_YES = "1"
.head 4 -  !
.head 4 -  String: MTPROP_TEXT_UNDEFINED = "<undefined>"
.head 3 -  User
.head 2 -  Resources
.head 2 +  Variables
.head 2 +  Internal Functions
.head 3 +  Function: MTblPropMakeColorImage
.head 4 -  Description:
.head 4 +  Returns
.head 5 -  Number:
.head 4 +  Parameters
.head 5 -  Number: p_nWid
.head 5 -  Number: p_nHt
.head 5 -  Number: p_nColor
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Number: nDC
.head 5 -  Number: nDCTbl
.head 5 -  Number: nBitmap
.head 5 -  Number: nBitmapOld
.head 5 -  Number: nBrush
.head 5 -  Number: nBrushOld
.head 5 -  Number: nColor
.head 5 -  Number: nHatchBrush
.head 5 -  Number: nImage
.head 5 -  Number: nPen
.head 5 -  Number: nPenOld
.head 4 +  Actions
.head 5 -  Set nDCTbl = GetDC( hWndNULL )
.head 5 +  If nDCTbl
.head 6 -  Set nDC = CreateCompatibleDC( nDCTbl )
.head 6 +  If nDC
.head 7 -  Set nBitmap = CreateCompatibleBitmap( nDCTbl, p_nWid, p_nHt )
.head 7 +  If nBitmap
.head 8 -  Set nBitmapOld =  SelectObject( nDC, nBitmap )
.head 8 -  !
.head 8 +  If p_nColor = MTBL_COLOR_UNDEF
.head 9 -  Set nColor = COLOR_White
.head 8 +  Else
.head 9 -  Set nColor = p_nColor
.head 8 -  Set nBrush = CreateSolidBrush( nColor ) 
.head 8 +  If nBrush
.head 9 -  Set nBrushOld = SelectObject( nDC, nBrush ) 
.head 9 -  !
.head 9 -  Set nPen = CreatePen( 0, 1, COLOR_Black )
.head 9 +  If nPen
.head 10 -  Set nPenOld = SelectObject( nDC, nPen ) 
.head 10 -  !
.head 10 -  Call Rectangle( nDC, 0, 0, p_nWid, p_nHt )
.head 10 -  !
.head 10 +  If p_nColor = MTBL_COLOR_UNDEF
.head 11 -  Set nHatchBrush = CreateHatchBrush( 3, COLOR_Black )
.head 11 +  If nHatchBrush
.head 12 -  Call FillRect( nDC, 1, 1, p_nWid - 1, p_nHt - 1, nHatchBrush )
.head 12 -  Call DeleteObject( nHatchBrush )
.head 10 -  !
.head 10 -  Call SelectObject( nDC, nPenOld )
.head 10 -  Call DeleteObject( nPen )
.head 9 -  !
.head 9 -  Call SelectObject( nDC, nBrushOld )
.head 9 -  Call DeleteObject( nBrush )
.head 8 -  !
.head 8 -  Call SelectObject( nDC, nBitmapOld )
.head 8 -  Set nImage = MImgLoadFromHandle( nBitmap, MIMG_TYPE_BMP )
.head 8 -  Call DeleteObject( nBitmap )
.head 7 -  Call DeleteDC( nDC )
.head 6 -  Call ReleaseDC( hWndNULL, nDCTbl )
.head 5 -  Return nImage
.head 2 -  Named Menus
.head 2 +  Class Definitions
.data RESOURCE 0 0 1 3008137893
0000: 6302000017010000 0000000000000000 0200000200FFFF01 00160000436C6173
0020: 73566172004F7574 6C696E6552006567 496E666F61003C00 000A5F004572726F
0040: 7242617300655601 00000D00001E0001 0D00FD00FF370D00 01F700FF1F1A0000
0060: DC000100FF7F2770 0000000100FFFF01 34000000020400FC 0000FF0738000000
0080: 02F700FF1F3C0000 DC000200FF7F4040 00000003040000FF FFC14400000002FD
00A0: 00FF074800000002 F700FF1F4C0000DC 000300FF7F507000 00000300FFFFC154
00C0: 00000001FD00FF07 0180370000080001 0000076364006B49 74656DC20000E000
00E0: 070000009D020001 FD00838001000000 7F047200000200FF FF8108000000010D
0100: FD00FF0715000000 01F700FF1F220000 DC000300FF7F2670 0000000300FFFFC1
0120: 2A00000001FD00FF 07
.enddata
.head 3 +  Functional Class: MTblProp
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Number: m_nType
.head 5 -  String: m_sName
.head 5 -  String: m_sTitle
.head 5 -  String: m_sCategory
.head 5 -  String: m_sGroup
.head 5 -  String: m_sGroup2
.head 5 -  String: m_sDefVal
.head 5 -  String: m_sUndefVal
.head 5 -  Boolean: m_bUndefVal
.head 5 -  String: m_sAutoVal
.head 5 -  Boolean: m_bAutoVal
.head 5 -  String: m_sVal
.head 5 -  !
.head 5 -  Boolean: m_bEnsureVisible
.head 5 -  Boolean: m_bEnsureInvisible
.head 5 -  Boolean: m_bModified
.head 5 -  !
.head 5 -  String: m_sMinVal
.head 5 -  String: m_sMaxVal
.head 5 -  Number: m_nMaxDecimalPlaces
.head 5 -  !
.head 5 -  Number: m_nMinDataLen
.head 5 -  Number: m_nMaxDataLen
.head 5 -  !
.head 5 -  String: m_saVals[*]
.head 5 -  Number: m_nVals
.head 5 -  String: m_saDispVals[*]
.head 5 -  Number: m_nDispVals
.head 5 -  !
.head 5 -  Number: m_nParentProp
.head 5 -  Number: m_naChildProps[*]
.head 5 -  Number: m_nChildProps
.head 5 -  !
.head 5 -  Number: m_nMutExclGrp
.head 5 -  !
.head 5 -  Number: m_nRowId
.head 4 +  Functions
.head 5 +  Function: AddChildProp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  Number: p_nPropIdx
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_naChildProps[m_nChildProps] = p_nPropIdx
.head 7 -  Set m_nChildProps = m_nChildProps + 1
.head 7 -  !
.head 7 -  Return m_nChildProps
.head 5 +  Function: AddDispVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: p_sDispVal
.head 7 -  String: p_sVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_saDispVals[m_nDispVals] = p_sDispVal
.head 7 -  Set m_nDispVals = m_nDispVals + 1
.head 7 -  !
.head 7 -  Set m_saVals[m_nVals] = p_sVal
.head 7 -  Set m_nVals = m_nVals + 1
.head 7 -  !
.head 7 -  Return m_nDispVals
.head 5 +  Function: AddDispVals_Column
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  : p_CdkItem
.head 8 -  Class: cdkItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  : CdkTable
.head 8 -  Class: cdkBaseTable
.head 7 -  : oColumns[*]
.head 8 -  Class: cdkColumn
.head 7 -  Number: n
.head 7 -  Number: nColCount
.head 7 -  String: sDispVal
.head 7 -  String: sName
.head 7 -  String: sTitle
.head 6 +  Actions
.head 7 -  Call CdkTable.InitializeFromObject( p_CdkItem )
.head 7 -  Set nColCount = CdkTable.EnumColumnObjects( oColumns )
.head 7 +  While n < nColCount
.head 8 -  Set sName = oColumns[n].GetName( )
.head 8 -  Set sTitle = oColumns[n].GetWindowTitle( )
.head 8 -  !
.head 8 -  Set sDispVal = sName || " (" || VisStrSubstitute( sTitle, MTPROP_CRLF, "|" ) || ")"
.head 8 -  Call AddDispVal( sDispVal, sName )
.head 8 -  !
.head 8 -  Set n = n + 1
.head 5 +  Function: AddDispVals_ECPD_Order
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call AddDispVal( "Cell - Column - Row", SalNumberToStrX( MTECPD_ORDER_CELL_COL_ROW, 0 ) )
.head 7 -  Call AddDispVal( "Cell - Row - Column", SalNumberToStrX( MTECPD_ORDER_CELL_ROW_COL, 0 ) )
.head 7 -  Call AddDispVal( "Column - Cell - Row", SalNumberToStrX( MTECPD_ORDER_COL_CELL_ROW, 0 ) )
.head 7 -  Call AddDispVal( "Column - Row - Cell", SalNumberToStrX( MTECPD_ORDER_COL_ROW_CELL, 0 ) )
.head 7 -  Call AddDispVal( "Row - Cell - Column", SalNumberToStrX( MTECPD_ORDER_ROW_CELL_COL, 0 ) )
.head 7 -  Call AddDispVal( "Row - Column - Cell", SalNumberToStrX( MTECPD_ORDER_ROW_COL_CELL, 0 ) )
.head 5 +  Function: AddDispVals_FontName
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 7 -  Number: nFontCount
.head 7 -  String: saFonts[*]
.head 6 +  Actions
.head 7 -  Set nFontCount = MTblEnumFonts( saFonts )
.head 7 +  While n < nFontCount
.head 8 -  Call AddDispVal( saFonts[n], saFonts[n] )
.head 8 -  Set n = n + 1
.head 5 +  Function: AddDispVals_ImageSource
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call AddDispVal( "File", "FILE" )
.head 7 -  Call AddDispVal( "TD Resource", "TDRES" )
.head 5 +  Function: AddDispVals_Linestyle
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call AddDispVal( "Invisible", SalNumberToStrX( MTLS_INVISIBLE, 0 ) )
.head 7 -  Call AddDispVal( "Dotted", SalNumberToStrX( MTLS_DOT, 0 ) )
.head 7 -  Call AddDispVal( "Solid", SalNumberToStrX( MTLS_SOLID, 0 ) )
.head 5 +  Function: AddDispValsToListCtrl
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndList
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 6 +  Actions
.head 7 +  While n < m_nDispVals
.head 8 -  Call SalListAdd( p_hWndList, m_saDispVals[n] )
.head 8 -  Set n = n + 1
.head 5 +  Function: AssignTo
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: MTblProp
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set p.m_nType = m_nType
.head 7 -  Set p.m_sName = m_sName
.head 7 -  Set p.m_sTitle = m_sTitle
.head 7 -  Set p.m_sCategory = m_sCategory
.head 7 -  Set p.m_sGroup = m_sGroup
.head 7 -  Set p.m_sGroup2 = m_sGroup2
.head 7 -  Set p.m_sDefVal = m_sDefVal
.head 7 -  Set p.m_sUndefVal = m_sUndefVal
.head 7 -  Set p.m_bUndefVal = m_bUndefVal
.head 7 -  Set p.m_sAutoVal = m_sAutoVal
.head 7 -  Set p.m_bAutoVal = m_bAutoVal
.head 7 -  Set p.m_sVal = m_sVal
.head 7 -  !
.head 7 -  Set p.m_bEnsureVisible = m_bEnsureVisible
.head 7 -  Set p.m_bEnsureInvisible = m_bEnsureInvisible
.head 7 -  Set p.m_bModified = m_bModified
.head 7 -  !
.head 7 -  Set p.m_sMinVal = m_sMinVal
.head 7 -  Set p.m_sMaxVal = m_sMaxVal
.head 7 -  Set p.m_nMaxDecimalPlaces = m_nMaxDecimalPlaces
.head 7 -  !
.head 7 -  Set p.m_nMinDataLen = m_nMinDataLen
.head 7 -  Set p.m_nMaxDataLen = m_nMaxDataLen
.head 7 -  !
.head 7 -  Set p.m_nVals = m_nVals
.head 7 -  Call VisArrayCopy( m_saVals, p.m_saVals, DT_String )
.head 7 -  Set p.m_nDispVals = m_nDispVals
.head 7 -  Call VisArrayCopy( m_saDispVals, p.m_saDispVals, DT_String )
.head 7 -  !
.head 7 -  Set p.m_nParentProp = m_nParentProp
.head 7 -  Set p.m_nChildProps = m_nChildProps
.head 7 -  Call VisArrayCopy( m_naChildProps, p.m_naChildProps, DT_Number )
.head 7 -  !
.head 7 -  Set p.m_nMutExclGrp = m_nMutExclGrp
.head 7 -  !
.head 7 -  Set p.m_nRowId = m_nRowId
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: GetAutoVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sAutoVal
.head 5 +  Function: GetCategory
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sCategory
.head 5 +  Function: GetChildProp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  Number: p_nIdx
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If p_nIdx >= 0 AND p_nIdx < m_nChildProps
.head 8 -  Return m_naChildProps[p_nIdx]
.head 7 +  Else
.head 8 -  Return -1
.head 5 +  Function: GetChildPropCount
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nChildProps
.head 5 +  Function: GetDefVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sDefVal
.head 5 +  Function: GetDispVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return GetDispValFromVal( GetVal( ) )
.head 5 +  Function: GetDispValCount
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nDispVals
.head 5 +  Function: GetDispValFromVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: p_sVal
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nIndex
.head 7 -  String: sDispVal
.head 6 +  Actions
.head 7 +  If HasUndefVal( ) AND p_sVal = GetUndefVal( )
.head 8 -  Set sDispVal = MTPROP_TEXT_UNDEFINED
.head 7 +  Else
.head 8 -  Set nIndex = VisArrayFindString( m_saVals, p_sVal )
.head 8 +  If nIndex >= 0
.head 9 -  Set sDispVal = m_saDispVals[nIndex]
.head 8 +  Else
.head 9 -  Set sDispVal = p_sVal
.head 7 -  Return sDispVal
.head 5 +  Function: GetEffVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If NOT m_sVal
.head 8 -  Return m_sDefVal
.head 7 +  Else
.head 8 -  Return m_sVal
.head 5 +  Function: GetEnsureInvisible
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_bEnsureInvisible
.head 5 +  Function: GetEnsureVisible
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_bEnsureVisible
.head 5 +  Function: GetFullTitle
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: s
.head 7 -  String: sDelimiter
.head 7 -  String: sFullTitle
.head 6 +  Actions
.head 7 -  Set sDelimiter = "."
.head 7 -  !
.head 7 -  Set s = GetCategory( )
.head 7 +  If s
.head 8 +  If sFullTitle
.head 9 -  Set sFullTitle = sFullTitle || sDelimiter || s
.head 8 +  Else
.head 9 -  Set sFullTitle = s
.head 7 -  !
.head 7 -  Set s = GetGroup( )
.head 7 +  If s
.head 8 +  If sFullTitle
.head 9 -  Set sFullTitle = sFullTitle || sDelimiter || s
.head 8 +  Else
.head 9 -  Set sFullTitle = s
.head 7 -  !
.head 7 -  Set s = GetGroup2( )
.head 7 +  If s
.head 8 +  If sFullTitle
.head 9 -  Set sFullTitle = sFullTitle || sDelimiter || s
.head 8 +  Else
.head 9 -  Set sFullTitle = s
.head 7 -  !
.head 7 -  Set s = GetTitle( )
.head 7 +  If NOT s
.head 8 -  Set s = GetName( )
.head 7 +  If s
.head 8 +  If sFullTitle
.head 9 -  Set sFullTitle = sFullTitle || sDelimiter || s
.head 8 +  Else
.head 9 -  Set sFullTitle = s
.head 7 -  !
.head 7 -  Return sFullTitle
.head 5 +  Function: GetGroup
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sGroup
.head 5 +  Function: GetGroup2
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sGroup2
.head 5 +  Function: GetMaxDataLen
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nMaxDataLen
.head 5 +  Function: GetMaxDecimalPlaces
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nMaxDecimalPlaces
.head 5 +  Function: GetMaxVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sMaxVal
.head 5 +  Function: GetMinDataLen
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nMinDataLen
.head 5 +  Function: GetMinVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sMinVal
.head 5 +  Function: GetModified
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_bModified
.head 5 +  Function: GetMutExclGrp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nMutExclGrp
.head 5 +  Function: GetName
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sName
.head 5 +  Function: GetParentProp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nParentProp
.head 5 +  Function: GetRowId
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nRowId
.head 5 +  Function: GetTitle
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sTitle
.head 5 +  Function: GetType
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_nType
.head 5 +  Function: GetUndefVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sUndefVal
.head 5 +  Function: GetVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sVal
.head 5 +  Function: GetValFromDispVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  String:
.head 6 +  Parameters
.head 7 -  String: p_sDispVal
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nIndex
.head 7 -  String: sVal
.head 6 +  Actions
.head 7 +  If p_sDispVal = MTPROP_TEXT_UNDEFINED
.head 8 -  Set sVal = GetUndefVal( )
.head 7 +  Else
.head 8 -  Set nIndex = VisArrayFindString( m_saDispVals, p_sDispVal )
.head 8 +  If nIndex >= 0
.head 9 -  Set sVal = m_saVals[nIndex]
.head 8 +  Else
.head 9 -  Set sVal = p_sDispVal
.head 7 -  Return sVal
.head 5 +  Function: GetValDecimalPlaces
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nDecimalPlaces
.head 7 -  Number: nMod
.head 6 +  Actions
.head 7 +  If IsNumeric( )
.head 8 -  Set nMod = SalNumberMod( SalStrToNumber( GetVal( ) ), 1 )
.head 8 +  While nMod
.head 9 -  Set nDecimalPlaces = nDecimalPlaces + 1
.head 9 -  Set nMod = nMod * 10
.head 9 -  Set nMod = SalNumberMod( nMod, 1 )
.head 7 -  Return nDecimalPlaces
.head 5 +  Function: GetValAsBool
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If m_sVal = MTPROP_VAL_BOOL_YES
.head 8 -  Return TRUE
.head 7 +  Else
.head 8 -  Return FALSE
.head 5 +  Function: HasAutoVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_bAutoVal
.head 5 +  Function: HasUndefVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_bUndefVal
.head 5 +  Function: Init
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nType = NUMBER_Null
.head 7 -  Set m_sName = STRING_Null
.head 7 -  Set m_sTitle = STRING_Null
.head 7 -  Set m_sCategory = STRING_Null
.head 7 -  Set m_sGroup = STRING_Null
.head 7 -  Set m_sGroup2 = STRING_Null
.head 7 -  Set m_sDefVal = STRING_Null
.head 7 -  Set m_sUndefVal = STRING_Null
.head 7 -  Set m_bUndefVal = FALSE
.head 7 -  Set m_sAutoVal = STRING_Null
.head 7 -  Set m_bAutoVal = FALSE
.head 7 -  Set m_sVal = STRING_Null
.head 7 -  !
.head 7 -  Set m_bEnsureVisible = FALSE
.head 7 -  Set m_bEnsureInvisible = FALSE
.head 7 -  Set m_bModified = FALSE
.head 7 -  !
.head 7 -  Set m_sMinVal = STRING_Null
.head 7 -  Set m_sMaxVal = STRING_Null
.head 7 -  Set m_nMaxDecimalPlaces = NUMBER_Null
.head 7 -  !
.head 7 -  Set m_nMinDataLen = NUMBER_Null
.head 7 -  Set m_nMaxDataLen = NUMBER_Null
.head 7 -  !
.head 7 -  Call InitDispVals( )
.head 7 -  Set m_nParentProp = -1
.head 7 -  Call InitChildProps( )
.head 7 -  !
.head 7 -  Set m_nMutExclGrp = NUMBER_Null
.head 7 -  !
.head 7 -  Set m_nRowId = TBL_Error
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: InitChildProps
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nChildProps = 0
.head 7 +  If NOT SalArrayIsEmpty( m_naChildProps )
.head 8 -  Call SalArraySetUpperBound( m_naChildProps, 1, -1 )
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: InitDispVals
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nVals = 0
.head 7 +  If NOT SalArrayIsEmpty( m_saVals )
.head 8 -  Call SalArraySetUpperBound( m_saVals, 1, -1 )
.head 7 -  Set m_nDispVals = 0
.head 7 +  If NOT SalArrayIsEmpty( m_saDispVals )
.head 8 -  Call SalArraySetUpperBound( m_saDispVals, 1, -1 )
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: IsNumeric
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nType
.head 6 +  Actions
.head 7 -  Set nType = GetType( )
.head 7 -  Return ( nType = MTPROP_TYPE_COLOR OR nType = MTPROP_TYPE_FONT_SIZE OR nType = MTPROP_TYPE_LINE_STYLE OR nType = MTPROP_TYPE_NUMBER )
.head 5 +  Function: IsUndefVal
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  String: p_sVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If NOT HasUndefVal( )
.head 8 -  Return FALSE
.head 7 -  Return p_sVal = GetUndefVal( )
.head 5 +  Function: IsValDef
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return m_sVal = m_sDefVal
.head 5 +  Function: ReadValFromCdkItem
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: cdkItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  : oTbl
.head 8 -  Class: cdkBaseTable
.head 7 -  : oColumns[*]
.head 8 -  Class: cdkColumn
.head 7 -  Number: n
.head 7 -  Number: nColCount
.head 7 -  String: sVal
.head 6 +  Actions
.head 7 +  If NOT m_sName
.head 8 -  Return FALSE
.head 7 -  !
.head 7 -  Set m_sVal = STRING_Null
.head 7 -  Call p.GetStrProperty( m_sName, sVal )
.head 7 -  Set m_sVal = sVal
.head 7 -  !
.head 7 +  If m_sVal
.head 8 +  If m_nType = MTPROP_TYPE_COLUMN AND m_sName = "MT_TREE_COL"
.head 9 -  Set m_sVal = STRING_Null
.head 9 +  If oTbl.InitializeFromObject( p )
.head 10 -  Set nColCount = oTbl.EnumColumnObjects( oColumns )
.head 10 -  Set n = 0
.head 10 +  While n < nColCount
.head 11 +  If oColumns[n].GetStrProperty( "MTC_IS_TREE_COL", sVal ) AND sVal = MTPROP_VAL_BOOL_YES
.head 12 -  Set m_sVal = oColumns[n].GetName( )
.head 12 -  Break
.head 11 -  Set n = n + 1
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: SetAutoVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sAutoVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sAutoVal = p_sAutoVal
.head 7 -  Set m_bAutoVal = TRUE
.head 7 -  Return TRUE
.head 5 +  Function: SetCategory
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sCategory
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sCategory = p_sCategory
.head 7 -  Return TRUE
.head 5 +  Function: SetDefVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sDefVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sDefVal = p_sDefVal
.head 7 -  Return TRUE
.head 5 +  Function: SetEnsureInvisible
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Boolean: p_bSet
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_bEnsureInvisible = p_bSet
.head 7 -  Return TRUE
.head 5 +  Function: SetEnsureVisible
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Boolean: p_bSet
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_bEnsureVisible = p_bSet
.head 7 -  Return TRUE
.head 5 +  Function: SetGroup
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sGroup
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sGroup = p_sGroup
.head 7 -  Return TRUE
.head 5 +  Function: SetGroup2
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sGroup2
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sGroup2 = p_sGroup2
.head 7 -  Return TRUE
.head 5 +  Function: SetMaxDecimalPlaces
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nMaxDecimalPlaces = p_nVal
.head 7 -  Return TRUE
.head 5 +  Function: SetMaxDataLen
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nMaxDataLen= p_nVal
.head 7 -  Return TRUE
.head 5 +  Function: SetMaxVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sMaxVal = p_sVal
.head 7 -  Return TRUE
.head 5 +  Function: SetMinDataLen
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nMinDataLen= p_nVal
.head 7 -  Return TRUE
.head 5 +  Function: SetMinVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sMinVal = p_sVal
.head 7 -  Return TRUE
.head 5 +  Function: SetModified
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Boolean: p_bSet
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_bModified = p_bSet
.head 7 -  Return TRUE
.head 5 +  Function: SetMutExclGrp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nMutExclGrp = p_nVal
.head 7 -  Return TRUE
.head 5 +  Function: SetName
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sName
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sName = p_sName
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: SetParentProp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nIdx
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nParentProp= p_nIdx
.head 7 -  Return TRUE
.head 5 +  Function: SetRowId
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nRowId
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nRowId= p_nRowId
.head 7 -  Return TRUE
.head 5 +  Function: SetTitle
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sTitle
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sTitle = p_sTitle
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: SetType
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nType
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_nType = p_nType
.head 7 -  Return TRUE
.head 5 +  Function: SetUndefVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sUndefVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set m_sUndefVal = p_sUndefVal
.head 7 -  Set m_bUndefVal = TRUE
.head 7 -  Return TRUE
.head 5 +  Function: SetVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sVal
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If p_sVal != m_sVal
.head 8 -  Call SetModified( TRUE )
.head 7 -  Set m_sVal = p_sVal
.head 7 -  Return TRUE
.head 5 +  Function: ValidateVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Receive String: r_sMsg
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bHasUndefVal
.head 7 -  Boolean: bOk
.head 7 -  Number: nMax
.head 7 -  Number: nMin
.head 7 -  Number: nType
.head 7 -  String: sDefVal
.head 7 -  String: sUndefVal
.head 7 -  String: sVal
.head 7 -  String: sMax
.head 7 -  String: sMin
.head 6 +  Actions
.head 7 -  Set bOk = TRUE
.head 7 -  !
.head 7 -  Set nType = GetType( )
.head 7 -  Set sVal = GetVal( )
.head 7 -  Set sDefVal = GetDefVal( )
.head 7 -  Set bHasUndefVal = HasUndefVal( )
.head 7 +  If bHasUndefVal
.head 8 -  Set sUndefVal = GetUndefVal( )
.head 7 -  !
.head 7 +  If NOT ( sVal = sDefVal OR ( bHasUndefVal AND sVal = sUndefVal ) )
.head 8 +  Loop VALIDATE
.head 9 +  If IsNumeric( )
.head 10 +  If NOT SalStrIsValidNumber( sVal )
.head 11 -  Set bOk = FALSE
.head 11 -  Set r_sMsg = "Invalid number"
.head 11 -  Break VALIDATE
.head 10 +  Else
.head 11 -  Set sMin = GetMinVal( )
.head 11 +  If sMin
.head 12 +  If SalStrToNumber( sVal ) < SalStrToNumber( sMin )
.head 13 -  Set bOk = FALSE
.head 13 -  Set r_sMsg = "Value is below minimum"
.head 13 -  Break VALIDATE
.head 11 -  !
.head 11 -  Set sMax = GetMaxVal( )
.head 11 +  If sMax
.head 12 +  If SalStrToNumber( sVal ) > SalStrToNumber( sMax )
.head 13 -  Set bOk = FALSE
.head 13 -  Set r_sMsg = "Value is above maximum"
.head 13 -  Break VALIDATE
.head 11 -  !
.head 11 -  Set nMax = GetMaxDecimalPlaces( )
.head 11 +  If nMax != NUMBER_Null
.head 12 +  If GetValDecimalPlaces( ) > nMax
.head 13 -  Set bOk = FALSE
.head 13 +  If nMax > 0
.head 14 -  Set r_sMsg = "Value has more decimal places than allowed"
.head 13 +  Else
.head 14 -  Set r_sMsg = "Decimal places are not allowed"
.head 13 -  Break VALIDATE
.head 9 -  !
.head 9 -  Set nMin = GetMinDataLen( )
.head 9 +  If nMin > 0
.head 10 +  If SalStrLength( sVal ) < nMin
.head 11 -  Set bOk = FALSE
.head 11 -  Set r_sMsg = "Value's data length is below minimum"
.head 11 -  Break VALIDATE
.head 9 -  !
.head 9 -  Set nMax = GetMaxDataLen( )
.head 9 +  If nMax > 0
.head 10 +  If SalStrLength( sVal ) > nMax
.head 11 -  Set bOk = FALSE
.head 11 -  Set r_sMsg = "Value's data length is above maximum"
.head 11 -  Break VALIDATE
.head 9 -  !
.head 9 -  Break VALIDATE
.head 7 -  !
.head 7 -  Return bOk
.head 5 +  Function: WriteValToCdkItem
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: cdkItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  : oTbl
.head 8 -  Class: cdkBaseTable
.head 7 -  : oColumns[*]
.head 8 -  Class: cdkColumn
.head 7 -  Number: n
.head 7 -  Number: nColCount
.head 7 -  String: sName
.head 6 +  Actions
.head 7 +  If NOT GetModified( )
.head 8 -  Return TRUE
.head 7 +  If NOT m_sName
.head 8 -  Return FALSE
.head 7 -  !
.head 7 +  If m_sVal != m_sDefVal
.head 8 -  Call p.SetStrProperty( m_sName, m_sVal )
.head 7 +  Else
.head 8 -  Call p.RemoveStrProperty( m_sName )
.head 7 -  !
.head 7 +  If m_nType = MTPROP_TYPE_COLUMN AND m_sName = "MT_TREE_COL"
.head 8 +  If oTbl.InitializeFromObject( p )
.head 9 -  Set nColCount = oTbl.EnumColumnObjects( oColumns )
.head 9 -  Set n = 0
.head 9 +  While n < nColCount
.head 10 -  Set sName = oColumns[n].GetName( )
.head 10 +  If sName = m_sVal
.head 11 -  Call oColumns[n].SetStrProperty( "MTC_IS_TREE_COL", MTPROP_VAL_BOOL_YES )
.head 10 +  Else
.head 11 -  Call oColumns[n].RemoveStrProperty( "MTC_IS_TREE_COL" )
.head 10 -  Set n = n + 1
.head 7 -  !
.head 7 -  Call SetModified( FALSE )
.head 7 -  !
.head 7 -  Return TRUE
.head 3 +  Functional Class: MTblPropList
.head 4 -  Description:
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  : m_Props[*]
.head 6 -  Class: MTblProp
.head 5 -  Number: m_nCount
.head 4 +  Functions
.head 5 +  Function: AddColProps
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: cdkItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  : Prop
.head 8 -  Class: MTblProp
.head 7 -  Boolean: bOk
.head 7 -  Number: nChildIdx
.head 7 -  Number: nParentIdx
.head 7 -  String: sCategory
.head 7 -  String: sGroup
.head 7 -  String: sGroup2
.head 6 +  Actions
.head 7 +  Loop DO
.head 8 -  ! Category "(General)"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "(General)"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_OWNERDRAWN" )
.head 9 -  Call Prop.SetTitle( "Ownerdrawn" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_END_ELLIPSIS" )
.head 9 -  Call Prop.SetTitle( "End Ellipsis" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_EXPORT_AS_TEXT" )
.head 9 -  Call Prop.SetTitle( "Export As Text" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "(Header)"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "(Header)"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_HDR_OWNERDRAWN" )
.head 9 -  Call Prop.SetTitle( "Ownerdrawn" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_HDR_TXTALIGN_H" )
.head 9 -  Call Prop.SetTitle( "Text Alignment Horizontal" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 9 -  Call Prop.SetDefVal( STRING_Null )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.AddDispVal( "Left", SalNumberToStrX( MTBL_COLHDR_FLAG_TXTALIGN_LEFT, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Right", SalNumberToStrX( MTBL_COLHDR_FLAG_TXTALIGN_RIGHT, 0 ) )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  ! Group "Font"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Font"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_FONT_NAME" )
.head 10 -  Call Prop.SetTitle( "Name" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_FONT_NAME )
.head 10 -  Call Prop.AddDispVals_FontName( )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Set nParentIdx = AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_FONT_SIZE" )
.head 10 -  Call Prop.SetTitle( "Size" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_FONT_SIZE )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.SetMinVal( "1" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Set nChildIdx = AddProp( Prop )
.head 10 -  !
.head 10 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_FONT_ENH_BOLD" )
.head 10 -  Call Prop.SetTitle( "Bold" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_FONT_ENH_ITALIC" )
.head 10 -  Call Prop.SetTitle( "Italic" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_FONT_ENH_UNDERLINE" )
.head 10 -  Call Prop.SetTitle( "Underline" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_FONT_ENH_STRIKEOUT" )
.head 10 -  Call Prop.SetTitle( "Strikeout" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  ! Group "Colors"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Colors"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_TEXT_COLOR" )
.head 10 -  Call Prop.SetTitle( "Text" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_BACK_COLOR" )
.head 10 -  Call Prop.SetTitle( "Background" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  ! Group "Image"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Image"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_SOURCE" )
.head 10 -  Call Prop.SetTitle( "Source" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.AddDispVals_ImageSource( )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Set nParentIdx = AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_NAME" )
.head 10 -  Call Prop.SetTitle( "Name" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Set nChildIdx = AddProp( Prop )
.head 10 -  !
.head 10 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_TRANSP_COLOR" )
.head 10 -  Call Prop.SetTitle( "Transparent Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_ALIGN_TILE" )
.head 10 -  Call Prop.SetTitle( "Tile" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_ALIGN_H" )
.head 10 -  Call Prop.SetTitle( "Alignment Horizontal" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 10 -  Call Prop.SetDefVal( SalNumberToStrX( MTSI_ALIGN_LEFT, 0 ) )
.head 10 -  Call Prop.AddDispVal( "Left", SalNumberToStrX( MTSI_ALIGN_LEFT, 0 ) )
.head 10 -  Call Prop.AddDispVal( "Center", SalNumberToStrX( MTSI_ALIGN_HCENTER, 0 ) )
.head 10 -  Call Prop.AddDispVal( "Right", SalNumberToStrX( MTSI_ALIGN_RIGHT, 0 ) )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_ALIGN_V" )
.head 10 -  Call Prop.SetTitle( "Alignment Vertical" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 10 -  Call Prop.SetDefVal( SalNumberToStrX( MTSI_ALIGN_VTEXT, 0 ) )
.head 10 -  Call Prop.AddDispVal( "First Text Line", SalNumberToStrX( MTSI_ALIGN_VTEXT, 0 ) )
.head 10 -  Call Prop.AddDispVal( "Top", SalNumberToStrX( MTSI_ALIGN_TOP, 0 ) )
.head 10 -  Call Prop.AddDispVal( "Center", SalNumberToStrX( MTSI_ALIGN_VCENTER, 0 ) )
.head 10 -  Call Prop.AddDispVal( "Bottom", SalNumberToStrX( MTSI_ALIGN_BOTTOM, 0 ) )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_NOLEAD_LEFT" )
.head 10 -  Call Prop.SetTitle( "No Left Leading" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_NOLEAD_TOP" )
.head 10 -  Call Prop.SetTitle( "No Top Leading" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_HDR_IMG_NOSELINV" )
.head 10 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Button"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Button"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_ACTIVE" )
.head 9 -  Call Prop.SetTitle( "Active" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_WIDTH" )
.head 9 -  Call Prop.SetTitle( "Width" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 9 -  Call Prop.SetDefVal( "0" )
.head 9 -  Call Prop.SetAutoVal( "-1" )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_TEXT" )
.head 9 -  Call Prop.SetTitle( "Text" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  ! Group "Image"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Image"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_BTN_IMG_SOURCE" )
.head 10 -  Call Prop.SetTitle( "Source" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.AddDispVals_ImageSource( )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Set nParentIdx = AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_BTN_IMG_NAME" )
.head 10 -  Call Prop.SetTitle( "Name" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Set nChildIdx = AddProp( Prop )
.head 10 -  !
.head 10 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_BTN_IMG_TRANSP_COLOR" )
.head 10 -  Call Prop.SetTitle( "Transparent Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_DISABLED" )
.head 9 -  Call Prop.SetTitle( "Disabled" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_FLAT" )
.head 9 -  Call Prop.SetTitle( "Flat Border" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_LEFT" )
.head 9 -  Call Prop.SetTitle( "Left Aligned" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_NO_BKGND" )
.head 9 -  Call Prop.SetTitle( "No Background" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BTN_SHARE_FONT" )
.head 9 -  Call Prop.SetTitle( "Share Font" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Buttons"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Buttons"
.head 9 -  !
.head 9 -  ! Group "Permanent"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Permanent"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_BUTTONS_PERMANENT" )
.head 10 -  Call Prop.SetTitle( "Show" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_SHOW_PERM_BTN_NE" )
.head 10 -  Call Prop.SetTitle( "Show In Non Editable Cells" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MTC_BTN_HIDE_PERM" )
.head 10 -  Call Prop.SetTitle( "Hide" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Colors"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Colors"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_TEXT_COLOR" )
.head 9 -  Call Prop.SetTitle( "Text" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_BACK_COLOR" )
.head 9 -  Call Prop.SetTitle( "Background" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Edit Mode"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Edit Mode"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_READ_ONLY" )
.head 9 -  Call Prop.SetTitle( "Read Only" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_CLIP_NONEDIT_AREA" )
.head 9 -  Call Prop.SetTitle( "Clip Non Editable Area" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_CLIP_AUTOSIZE_EDIT" )
.head 9 -  Call Prop.SetTitle( "Adjust Height Automatically" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_EDITALIGN" )
.head 9 -  Call Prop.SetTitle( "Horizontal Text Alignment" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 9 -  Call Prop.SetDefVal( STRING_Null )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.AddDispVal( "Left", SalNumberToStrX( MTBL_COL_FLAG_EDITALIGN_LEFT, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Center", SalNumberToStrX( MTBL_COL_FLAG_EDITALIGN_CENTER, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Right", SalNumberToStrX( MTBL_COL_FLAG_EDITALIGN_RIGHT, 0 ) )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Font"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Font"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_FONT_NAME" )
.head 9 -  Call Prop.SetTitle( "Name" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_FONT_NAME )
.head 9 -  Call Prop.AddDispVals_FontName( )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Set nParentIdx =  AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_FONT_SIZE" )
.head 9 -  Call Prop.SetTitle( "Size" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_FONT_SIZE )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.SetMinVal( "1" )
.head 9 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Set nChildIdx = AddProp( Prop )
.head 9 -  !
.head 9 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_FONT_ENH_BOLD" )
.head 9 -  Call Prop.SetTitle( "Bold" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_FONT_ENH_ITALIC" )
.head 9 -  Call Prop.SetTitle( "Italic" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_FONT_ENH_UNDERLINE" )
.head 9 -  Call Prop.SetTitle( "Underline" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_FONT_ENH_STRIKEOUT" )
.head 9 -  Call Prop.SetTitle( "Strikeout" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Lines"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Lines"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_NO_ROWLINES" )
.head 9 -  Call Prop.SetTitle( "No Row Lines" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_NO_COLLINE" )
.head 9 -  Call Prop.SetTitle( "No Column Line" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Selection"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Selection"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_NOSELINV_TEXT" )
.head 9 -  Call Prop.SetTitle( "Don't Invert Text" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_NOSELINV_IMAGE" )
.head 9 -  Call Prop.SetTitle( "Don't Invert Image" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_NOSELINV_BKGND" )
.head 9 -  Call Prop.SetTitle( "Don't Invert Background" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Text Alignment"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Text Alignment"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_TXTALIGN_H" )
.head 9 -  Call Prop.SetTitle( "Horizontal" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 9 -  Call Prop.SetDefVal( STRING_Null )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.AddDispVal( "Left", SalNumberToStrX( MTBL_COL_FLAG_TXTALIGN_LEFT, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Center", SalNumberToStrX( MTBL_COL_FLAG_TXTALIGN_CENTER, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Right", SalNumberToStrX( MTBL_COL_FLAG_TXTALIGN_RIGHT, 0 ) )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MTC_TXTALIGN_V" )
.head 9 -  Call Prop.SetTitle( "Vertical" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 9 -  Call Prop.SetDefVal( STRING_Null )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.AddDispVal( "Top", SalNumberToStrX( MTBL_COL_FLAG_TXTALIGN_TOP, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Center", SalNumberToStrX( MTBL_COL_FLAG_TXTALIGN_VCENTER, 0 ) )
.head 9 -  Call Prop.AddDispVal( "Bottom", SalNumberToStrX( MTBL_COL_FLAG_TXTALIGN_BOTTOM, 0 ) )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  !
.head 8 -  Set bOk = TRUE
.head 8 -  Break
.head 7 -  Return bOk
.head 5 +  Function: AddProp
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: MTblProp
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call p.AssignTo( m_Props[m_nCount] )
.head 7 -  Set m_nCount = m_nCount + 1
.head 7 -  Return m_nCount - 1
.head 5 +  Function: AddTableProps
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: cdkItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  : Prop
.head 8 -  Class: MTblProp
.head 7 -  Boolean: bOk
.head 7 -  Number: nChildIdx
.head 7 -  Number: n
.head 7 -  Number: nMutExclGrp
.head 7 -  Number: nParentIdx
.head 7 -  Number: nRepGrps
.head 7 -  String: sCategory
.head 7 -  String: sGroup
.head 7 -  String: sGroup2
.head 7 -  String: sPrefix
.head 7 -  String: saRepGrp[*,2]
.head 6 +  Actions
.head 7 -  Set nMutExclGrp = 1
.head 7 +  Loop DO
.head 8 -  ! Category "(General)"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "(General)"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_NO_DITHER" )
.head 9 -  Call Prop.SetTitle( "No Dithering" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_PWD_CHAR" )
.head 9 -  Call Prop.SetTitle( "Password Character" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 9 -  Call Prop.SetDefVal( " " )
.head 9 -  Call Prop.SetMinDataLen( 1 )
.head 9 -  Call Prop.SetMaxDataLen( 1 )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Extended Messages"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Extended Messages"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_EXT_MSGS" )
.head 10 -  Call Prop.SetTitle( "Enabled" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_EXT_MSGS_CORNERSEP_EXCL" )
.head 10 -  Call Prop.SetTitle( "Corner Separator Messages Exclusive" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_EXT_MSGS_COLHDRSEP_EXCL" )
.head 10 -  Call Prop.SetTitle( "Column Header Separator Messages Exclusive" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_YES )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_EXT_MSGS_ROWHDRSEP_EXCL" )
.head 10 -  Call Prop.SetTitle( "Row Header Separator Messages Exclusive" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Special Character Handling"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Special Character Handling"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_EXPAND_CRLF" )
.head 10 -  Call Prop.SetTitle( "Expand CRLF Characters" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_EXPAND_TAB" )
.head 10 -  Call Prop.SetTitle( "Expand TAB Characters" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Theme Handling"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Theme Handling"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_IGNORE_TD_THEME_COLORS" )
.head 10 -  Call Prop.SetTitle( "Ignore TD Theme Colors" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_IGNORE_WIN_THEME" )
.head 10 -  Call Prop.SetTitle( "Ignore Windows Theme" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Buttons"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Buttons"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_BUTTONS_FLAT" )
.head 9 -  Call Prop.SetTitle( "Flat Border" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_BUTTONS_OUTSIDE_CELL" )
.head 9 -  Call Prop.SetTitle( "Outside Cell" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Permanent"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Permanent"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_BUTTONS_PERMANENT" )
.head 10 -  Call Prop.SetTitle( "Show" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_HIDE_PERM_BTN_NE" )
.head 10 -  Call Prop.SetTitle( "Hide In Non Editable Cells" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Hotkey"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Hotkey"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_BTN_HOTKEY" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( "Virtual Key" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "0" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_BTN_HOTKEY_CTRL" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( " + [CTRL]" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_BTN_HOTKEY_SHIFT" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( " + [SHIFT]" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_BTN_HOTKEY_ALT" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( " + [ALT]" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Cell Mode"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Cell Mode"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CM_ACTIVE" )
.head 9 -  Call Prop.SetTitle( "Active" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CM_SINGLE_SELECTION" )
.head 9 -  Call Prop.SetTitle( "Single Selection" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call Prop.SetMutExclGrp( nMutExclGrp )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CM_NO_SELECTION" )
.head 9 -  Call Prop.SetTitle( "No Selection" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call Prop.SetMutExclGrp( nMutExclGrp )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Set nMutExclGrp = nMutExclGrp + 1
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CM_AUTO_EDIT_MODE" )
.head 9 -  Call Prop.SetTitle( "Automatic Edit Mode" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Columns"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Columns"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_NO_LINES_FREE_COL_AREA" )
.head 9 -  Call Prop.SetTitle( "No Lines In Free Column Area" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_COLOR_ENTIRE_COLUMN" )
.head 9 -  Call Prop.SetTitle( "Fully Colorized" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Column Lines"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Column Lines"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_COL_LINES_STYLE" )
.head 10 -  Call Prop.SetTitle( "Style" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_LINE_STYLE )
.head 10 -  Call Prop.AddDispVals_Linestyle( )
.head 10 +  If SalStrLeftX( SalNumberToStrX( SalGetVersion( ), 0 ), 1 ) >= "5"
.head 11 -  Call Prop.SetDefVal( SalNumberToStrX( MTLS_SOLID, 0 ) )
.head 10 +  Else
.head 11 -  Call Prop.SetDefVal( SalNumberToStrX( MTLS_DOT, 0 ) )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_COL_LINES_COLOR" )
.head 10 -  Call Prop.SetTitle( "Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Last Locked Column Line"
.head 9 +  If TRUE
.head 10 -  Set sGroup =  "Last Locked Column Line"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_LLCOL_LINE_STYLE" )
.head 10 -  Call Prop.SetTitle( "Style" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_LINE_STYLE )
.head 10 -  Call Prop.AddDispVals_Linestyle( )
.head 10 -  Call Prop.SetDefVal( SalNumberToStrX( MTLS_SOLID, 0 ) )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_LLCOL_LINE_COLOR" )
.head 10 -  Call Prop.SetTitle( "Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Edit Mode"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Edit Mode"
.head 9 -  !
.head 9 -  ! Group "Multiline Edit"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Multiline Edit"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_MOVE_INP_FOCUS_UD_EX" )
.head 10 -  Call Prop.SetTitle( "Move Input Focus Up/Down At Upper/Lower Edge" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Drop Down List"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Drop Down List"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_USE_TBL_FONT_DDL" )
.head 10 -  Call Prop.SetTitle( "Use Table Font" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_ADAPT_LIST_WIDTH" )
.head 10 -  Call Prop.SetTitle( "Adjust Width Automatically" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Popup Edit"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Popup Edit"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_USE_TBL_FONT_PE" )
.head 10 -  Call Prop.SetTitle( "Use Table Font" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Effective Cell Property Determination"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Effective Cell Property Determination"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_ECPD_ORDER" )
.head 9 -  Call Prop.SetTitle( "Order" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_ECPD_ORDER )
.head 9 -  Call Prop.AddDispVals_ECPD_Order( )
.head 9 -  Call Prop.SetDefVal( SalNumberToStrX( MTECPD_ORDER_CELL_COL_ROW, 0 ) )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_ECPD_COMB_FONT_ENH" )
.head 9 -  Call Prop.SetTitle( "Combine Font Enhancements" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Focus"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Focus"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SUPPRESS_FOCUS" )
.head 9 -  Call Prop.SetTitle( "Suppress" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CLIP_ROW_FOCUS_AREA" )
.head 9 -  Call Prop.SetTitle( "Clip Row Focus Area" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Focus Frame"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Focus Frame"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_FOCUS_FRAME_THICKNESS" )
.head 10 -  Call Prop.SetTitle( "Thickness" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "3" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_FOCUS_FRAME_OFFSET_X" )
.head 10 -  Call Prop.SetTitle( "Offset X" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "-1" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_FOCUS_FRAME_OFFSET_Y" )
.head 10 -  Call Prop.SetTitle( "Offset Y" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "-1" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetGroup2( sGroup2 )
.head 10 -  Call Prop.SetName( "MT_FOCUS_FRAME_COLOR" )
.head 10 -  Call Prop.SetTitle( "Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Headers"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Headers"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_GRADIENT_HEADERS" )
.head 9 -  Call Prop.SetTitle( "Gradient Background" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_HEADERS_BACK_COLOR" )
.head 9 -  Call Prop.SetTitle( "Background Color" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_COLHDR_FREE_BACK_COLOR" )
.head 9 -  Call Prop.SetTitle( "Column Header Free Area Background Color" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CORNER_BACK_COLOR" )
.head 9 -  Call Prop.SetTitle( "Corner Background Color" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_CORNER_OWNERDRAWN" )
.head 9 -  Call Prop.SetTitle( "Corner Ownerdrawn" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_NO_COLUMN_HEADER" )
.head 9 -  Call Prop.SetTitle( "No Column Header" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SOLID_ROWHDR" )
.head 9 -  Call Prop.SetTitle( "Row Headers Solid" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Highlighting"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Highlighting"
.head 9 -  !
.head 9 -  Set nRepGrps = 0
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Cell"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_CELL_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Column"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_COL_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Column Header"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_COLHDR_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Column Header Group"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_COLHDRGRP_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Row"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_ROW_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Row Header"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_ROWHDR_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Corner"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_CORNER_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set saRepGrp[nRepGrps, 0] = "Node"
.head 9 -  Set saRepGrp[nRepGrps, 1] = "MT_HILI_NODE_"
.head 9 -  Set nRepGrps = nRepGrps + 1
.head 9 -  !
.head 9 -  Set n = 0
.head 9 +  While n < nRepGrps
.head 10 -  Set sGroup = saRepGrp[n,0]
.head 10 -  Set sPrefix = saRepGrp[n,1]
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( sPrefix || "BACK_COLOR" )
.head 10 -  Call Prop.SetTitle( "Background Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 +  If sPrefix != "MT_HILI_NODE_"
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetName( sPrefix || "TEXT_COLOR" )
.head 11 -  Call Prop.SetTitle( "Text Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 10 -  ! Group 2 "Additional Font Enhancements"
.head 10 +  If sPrefix != "MT_HILI_NODE_"
.head 11 -  Set sGroup2 = "Additional Font Enhancements"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "FONT_ENH_BOLD" )
.head 11 -  Call Prop.SetTitle( "Bold" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "FONT_ENH_ITALIC" )
.head 11 -  Call Prop.SetTitle( "Italic" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "FONT_ENH_UNDERLINE" )
.head 11 -  Call Prop.SetTitle( "Underline" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "FONT_ENH_STRIKEOUT" )
.head 11 -  Call Prop.SetTitle( "Strikeout" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  ! Group 2 "Image"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Image"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  ! Group 2 "Image Expanded"
.head 10 +  If sPrefix != "MT_HILI_COLHDR_" AND sPrefix != "MT_HILI_COLHDRGRP_" AND sPrefix != "MT_HILI_CORNER_"
.head 11 -  Set sGroup2 = "Image Expanded"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "IMGEXP_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "IMGEXP_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( sPrefix || "IMGEXP_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  Set n = n + 1
.head 9 -  !
.head 9 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Hyperlink HOVER"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Hyperlink HOVER"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_HYPHO_ENABLED" )
.head 9 -  Call Prop.SetTitle( "Enabled" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Additional Font Enhancements"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Additional Font Enhancements"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_HYPHO_FONT_ENH_BOLD" )
.head 10 -  Call Prop.SetTitle( "Bold" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_HYPHO_FONT_ENH_ITALIC" )
.head 10 -  Call Prop.SetTitle( "Italic" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_HYPHO_FONT_ENH_UNDERLINE" )
.head 10 -  Call Prop.SetTitle( "Underline" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_HYPHO_FONT_ENH_STRIKEOUT" )
.head 10 -  Call Prop.SetTitle( "Strikeout" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetGroup( sGroup )
.head 9 -  Call Prop.SetName( "MT_HYPHO_TEXT_COLOR" )
.head 9 -  Call Prop.SetTitle( "Text Color" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Rows"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Rows"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_VARIABLE_ROW_HEIGHT" )
.head 9 -  Call Prop.SetTitle( "Variable Row Height" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_NO_LINES_FREE_ROW_AREA" )
.head 9 -  Call Prop.SetTitle( "No Lines In Free Row Area" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_COLOR_ENTIRE_ROW" )
.head 9 -  Call Prop.SetTitle( "Fully Colorized" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Row Lines"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Row Lines"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_ROW_LINES_STYLE" )
.head 10 -  Call Prop.SetTitle( "Style" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_LINE_STYLE )
.head 10 -  Call Prop.AddDispVals_Linestyle( )
.head 10 +  If SalStrLeftX( SalNumberToStrX( SalGetVersion( ), 0 ), 1 ) >= "5"
.head 11 -  Call Prop.SetDefVal( SalNumberToStrX( MTLS_SOLID, 0 ) )
.head 10 +  Else
.head 11 -  Call Prop.SetDefVal( SalNumberToStrX( MTLS_DOT, 0 ) )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_ROW_LINES_COLOR" )
.head 10 -  Call Prop.SetTitle( "Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Row Flag Images"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Row Flag Images"
.head 10 -  !
.head 10 -  ! Group 2 "ROW_Edited"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_Edited"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_EDITED_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_EDITED_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_EDITED_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_EDITED_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_MarkDeleted"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_MarkDeleted"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_MARKDELETED_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_MARKDELETED_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_MARKDELETED_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_MARKDELETED_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_New"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_New"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_NEW_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_NEW_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_NEW_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_NEW_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_UserFlag1"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_UserFlag1"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG1_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG1_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG1_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG1_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_UserFlag2"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_UserFlag2"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG2_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG2_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG2_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG2_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_UserFlag3"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_UserFlag3"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG3_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG3_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG3_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG3_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_UserFlag4"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_UserFlag4"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG4_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG4_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG4_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG4_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "ROW_UserFlag5"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "ROW_UserFlag5"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG5_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG5_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG5_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_ROW_USERFLAG5_IMG_NOSELINV" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Scrolling"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Scrolling"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_THUMBTRACK_HSCROLL" )
.head 9 -  Call Prop.SetTitle( "Scroll Horizontal On Thumbtrack" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_THUMBTRACK_VSCROLL" )
.head 9 -  Call Prop.SetTitle( "Scroll Vertical On Thumbtrack" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  ! Group "Mousewheel"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Mousewheel Scrolling"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_MWSCR_ENABLED" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( "Enabled" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_MWSCR_ROWS_PER_NOTCH" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( "Rows Per Notch" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "3" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_MWSCR_SPLITROWS_PER_NOTCH" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( "Split-Rows Per Notch" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "3" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetName( "MT_MWSCR_PAGE_KEY" )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetTitle( "Page Virtual Key" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "0" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Selections"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Selections"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SEL_BACK_COLOR" )
.head 9 -  Call Prop.SetTitle( "Background Color" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SEL_TEXT_COLOR" )
.head 9 -  Call Prop.SetTitle( "Text Color" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_FULL_OVERLAP_SELECTION" )
.head 9 -  Call Prop.SetTitle( "Full Overlap" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SUPPRESS_ROW_SEL" )
.head 9 -  Call Prop.SetTitle( "Suppress Row Selections" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Sort"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Sort"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SORT_KEEP_ROWMERGE" )
.head 9 -  Call Prop.SetTitle( "Keep Merged Rows Together" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_SORT_RESTORE_TREE" )
.head 9 -  Call Prop.SetTitle( "Restore Tree" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  ! Category "Tooltips"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Tooltips"
.head 9 -  !
.head 9 -  ! Group "Enabled Types"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Enabled Types"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_CELL_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Cell" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_CELL_TEXT_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Cell Text" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_CELL_COMPLETETEXT_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Cell Complete Text" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_CELL_IMAGE_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Cell Image" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_CELL_BTN_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Cell Button" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_COLHDR_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Column Header" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_COLHDR_BTN_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Column Header Button" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_COLHDRGRP_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Column Header Group" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_CORNER_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Corner" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_ROWHDR_ENABLED" )
.head 10 -  Call Prop.SetTitle( "Row Header" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Default Settings"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Default Settings"
.head 10 -  !
.head 10 -  ! Group 2 "Font"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Font"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FONT_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_FONT_NAME )
.head 11 -  Call Prop.AddDispVals_FontName( )
.head 11 -  Call Prop.SetDefVal( "Arial" )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx =  AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FONT_SIZE" )
.head 11 -  Call Prop.SetTitle( "Size" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_FONT_SIZE )
.head 11 -  Call Prop.SetDefVal( "8" )
.head 11 -  Call Prop.SetMinVal( "1" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx =  AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FONT_ENH_BOLD" )
.head 11 -  Call Prop.SetTitle( "Bold" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FONT_ENH_ITALIC" )
.head 11 -  Call Prop.SetTitle( "Italic" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FONT_ENH_UNDERLINE" )
.head 11 -  Call Prop.SetTitle( "Underline" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FONT_ENH_STRIKEOUT" )
.head 11 -  Call Prop.SetTitle( "Strikeout" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group2 "Colors"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Colors"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_TEXT_COLOR" )
.head 11 -  Call Prop.SetTitle( "Text" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetDefVal( "0" )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FRAME_COLOR" )
.head 11 -  Call Prop.SetTitle( "Frame" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetDefVal( "0" )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_BACK_COLOR" )
.head 11 -  Call Prop.SetTitle( "Background" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetDefVal( SalNumberToStrX( SalColorFromRGB( 255, 255, 228 ), 0 ) )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group2 "Times"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Times"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_DELAY" )
.head 11 -  Call Prop.SetTitle( "Delay" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetDefVal( "400" )
.head 11 -  Call Prop.SetMinVal( "0" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_FADE_IN_TIME" )
.head 11 -  Call Prop.SetTitle( "Fade In" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetDefVal( "0" )
.head 11 -  Call Prop.SetMinVal( "0" )
.head 11 -  Call Prop.SetMaxVal( "5000" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group2 "Position"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Position"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_POS_X" )
.head 11 -  Call Prop.SetTitle( "X" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_POS_Y" )
.head 11 -  Call Prop.SetTitle( "Y" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group2 "Offset"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Offset"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_OFFSET_X" )
.head 11 -  Call Prop.SetTitle( "X" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.SetDefVal( "0" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_OFFSET_Y" )
.head 11 -  Call Prop.SetTitle( "Y" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.SetDefVal( "20" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group2 "Size"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Size"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_WIDTH" )
.head 11 -  Call Prop.SetTitle( "Width" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_HEIGHT" )
.head 11 -  Call Prop.SetTitle( "Height" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group2 "Round Corner Ellipse Size"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Round Corner Ellipse Size"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_RCE_WIDTH" )
.head 11 -  Call Prop.SetTitle( "Width" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetDefVal( "0" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TT_DEF_RCE_HEIGHT" )
.head 11 -  Call Prop.SetTitle( "Height" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 11 -  Call Prop.SetDefVal( "0" )
.head 11 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_DEF_GRADIENT" )
.head 10 -  Call Prop.SetTitle( "Gradient" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_DEF_OPACITY" )
.head 10 -  Call Prop.SetTitle( "Opacity" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "255" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxVal( "255" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_DEF_NO_FRAME" )
.head 10 -  Call Prop.SetTitle( "No Frame" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TT_DEF_PERMEABLE" )
.head 10 -  Call Prop.SetTitle( "Permeable" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_YES )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 8 -  ! Category "Tree"
.head 8 +  If TRUE
.head 9 -  Set sCategory = "Tree"
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_COL" )
.head 9 -  Call Prop.SetTitle( "Column" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_COLUMN )
.head 9 -  Call Prop.AddDispVals_Column( p )
.head 9 -  Call Prop.SetUndefVal( STRING_Null )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  ! Group "Node"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Node"
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TREE_NODE_SIZE" )
.head 10 -  Call Prop.SetTitle( "Size" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 10 -  Call Prop.SetDefVal( "0" )
.head 10 -  Call Prop.SetMinVal( "0" )
.head 10 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TREE_NODE_BACK_COLOR" )
.head 10 -  Call Prop.SetTitle( "Background Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TREE_NODE_OUTER_COLOR" )
.head 10 -  Call Prop.SetTitle( "Outer Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TREE_NODE_INNER_COLOR" )
.head 10 -  Call Prop.SetTitle( "Inner Color" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 10 -  Call Prop.SetUndefVal( STRING_Null )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TREE_NOSELINV_NODES" )
.head 10 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  ! Group 2 "Image Collapsed"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Image Collapsed"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NODE_COLL_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NODE_COLL_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NODE_COLL_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  ! Group 2 "Image Expanded"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Image Expanded"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NODE_EXP_IMG_SOURCE" )
.head 11 -  Call Prop.SetTitle( "Source" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_STRING )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.AddDispVals_ImageSource( )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nParentIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NODE_EXP_IMG_NAME" )
.head 11 -  Call Prop.SetTitle( "Name" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_IMAGE_NAME )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Set nChildIdx = AddProp( Prop )
.head 11 -  !
.head 11 -  Call SetPropRelation( nParentIdx, nChildIdx )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NODE_EXP_IMG_TRANSP_COLOR" )
.head 11 -  Call Prop.SetTitle( "Transparent Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  ! Group "Lines"
.head 9 +  If TRUE
.head 10 -  Set sGroup = "Lines"
.head 10 -  !
.head 10 -  ! Group2 "Tree Lines"
.head 10 +  If TRUE
.head 11 -  Set sGroup2 = "Tree Lines"
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_LINES_STYLE" )
.head 11 -  Call Prop.SetTitle( "Style" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_LINE_STYLE )
.head 11 -  Call Prop.AddDispVals_Linestyle( )
.head 11 -  Call Prop.SetDefVal( SalNumberToStrX( MTLS_INVISIBLE, 0 ) )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_LINES_COLOR" )
.head 11 -  Call Prop.SetTitle( "Color" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_COLOR )
.head 11 -  Call Prop.SetUndefVal( STRING_Null )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Call Prop.Init( )
.head 11 -  Call Prop.SetCategory( sCategory )
.head 11 -  Call Prop.SetGroup( sGroup )
.head 11 -  Call Prop.SetGroup2( sGroup2 )
.head 11 -  Call Prop.SetName( "MT_TREE_NOSELINV_LINES" )
.head 11 -  Call Prop.SetTitle( "No Selection Inverting" )
.head 11 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 11 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 11 -  Call Prop.ReadValFromCdkItem( p )
.head 11 -  Call AddProp( Prop )
.head 11 -  !
.head 11 -  Set sGroup2 = STRING_Null
.head 10 -  !
.head 10 -  Call Prop.Init( )
.head 10 -  Call Prop.SetCategory( sCategory )
.head 10 -  Call Prop.SetGroup( sGroup )
.head 10 -  Call Prop.SetName( "MT_TREE_NO_ROWLINES" )
.head 10 -  Call Prop.SetTitle( "No Row Lines" )
.head 10 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 10 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 10 -  Call Prop.ReadValFromCdkItem( p )
.head 10 -  Call AddProp( Prop )
.head 10 -  !
.head 10 -  Set sGroup = STRING_Null
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_AUTO_NORM_HIER" )
.head 9 -  Call Prop.SetTitle( "Auto Normalize Hierarchy" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_INDENT" )
.head 9 -  Call Prop.SetTitle( "Indent Per Level" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_NUMBER )
.head 9 -  Call Prop.SetDefVal( "0" )
.head 9 -  Call Prop.SetMinVal( "0" )
.head 9 -  Call Prop.SetMaxDecimalPlaces( 0 )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_INDENT_ALL" )
.head 9 -  Call Prop.SetTitle( "Indent All Rows On Level 0" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_INDENT_NONE" )
.head 9 -  Call Prop.SetTitle( "Indent No Rows On Level 0" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_BOTTOM_UP" )
.head 9 -  Call Prop.SetTitle( "Bottom-Up" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_SPLIT_BOTTOM_UP" )
.head 9 -  Call Prop.SetTitle( "Bottom-Up Split-Area" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_FLAT_STRUCT" )
.head 9 -  Call Prop.SetTitle( "Flat Structure" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 9 -  !
.head 9 -  Call Prop.Init( )
.head 9 -  Call Prop.SetCategory( sCategory )
.head 9 -  Call Prop.SetName( "MT_TREE_EXP_ROW_ON_DBLCLK" )
.head 9 -  Call Prop.SetTitle( "Expand/Collapse Row On Double Click" )
.head 9 -  Call Prop.SetType( MTPROP_TYPE_BOOL )
.head 9 -  Call Prop.SetDefVal( MTPROP_VAL_BOOL_NO )
.head 9 -  Call Prop.ReadValFromCdkItem( p )
.head 9 -  Call AddProp( Prop )
.head 8 -  !
.head 8 -  Set bOk = TRUE
.head 8 -  Break
.head 7 -  Return bOk
.head 5 +  Function: AnyPropModified
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 6 +  Actions
.head 7 +  While n < m_nCount
.head 8 +  If m_Props[n].GetModified( )
.head 9 -  Return TRUE
.head 8 -  Set n = n + 1
.head 7 -  Return FALSE
.head 5 +  Function: AssignTo
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: MTblPropList
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 6 +  Actions
.head 7 +  While n < m_nCount
.head 8 -  Call m_Props[n].AssignTo( p.m_Props[n] )
.head 8 -  Set n = n + 1
.head 7 -  Set p.m_nCount = m_nCount
.head 7 -  Return TRUE
.head 5 +  Function: EnumCategories
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: p_saCat[*]
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bAdd
.head 7 -  Number: n
.head 7 -  Number: n2
.head 7 -  Number: nCat
.head 7 -  String: sCat
.head 6 +  Actions
.head 7 +  If NOT SalArrayIsEmpty( p_saCat )
.head 8 -  Call SalArraySetUpperBound( p_saCat, 1, -1 )
.head 7 -  Set nCat = 0
.head 7 -  !
.head 7 -  Set n = 0
.head 7 +  While n < m_nCount
.head 8 -  Set sCat = m_Props[n].GetCategory( )
.head 8 +  If sCat
.head 9 -  Set bAdd = TRUE
.head 9 -  !
.head 9 +  If nCat > 0
.head 10 -  Set n2 = 0
.head 10 +  While n2 < nCat
.head 11 +  If p_saCat[n2] = sCat
.head 12 -  Set bAdd = FALSE
.head 12 -  Break
.head 11 -  Set n2 = n2 + 1
.head 9 -  !
.head 9 +  If bAdd
.head 10 -  Set p_saCat[nCat] = sCat
.head 10 -  Set nCat = nCat + 1
.head 8 -  !
.head 8 -  Set n = n + 1
.head 7 -  !
.head 7 -  Return nCat
.head 5 +  Function: FindName
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: p_s
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 6 +  Actions
.head 7 +  While n < m_nCount
.head 8 +  If m_Props[n].GetName( ) = p_s
.head 9 -  Return n
.head 8 -  Set n = n + 1
.head 7 -  Return -1
.head 5 +  Function: FindRowId
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  Number: p_n
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 6 +  Actions
.head 7 +  While n < m_nCount
.head 8 +  If m_Props[n].GetRowId( ) = p_n
.head 9 -  Return n
.head 8 -  Set n = n + 1
.head 7 -  Return -1
.head 5 +  Function: SetPropRelation
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nParentIdx
.head 7 -  Number: p_nChildIdx
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If p_nParentIdx < 0 OR p_nParentIdx >= m_nCount
.head 8 -  Return FALSE
.head 7 +  If p_nChildIdx < 0 OR p_nChildIdx >= m_nCount
.head 8 -  Return FALSE
.head 7 -  Call m_Props[p_nParentIdx].AddChildProp( p_nChildIdx )
.head 7 -  Call m_Props[p_nChildIdx].SetParentProp( p_nParentIdx )
.head 7 -  Return TRUE
.head 5 +  Function: WritePropsToCdkItem
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: cdkItem
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 6 +  Actions
.head 7 -  Set n = 0
.head 7 +  While n < m_nCount
.head 8 -  Call m_Props[n].WriteValToCdkItem( p )
.head 8 -  Set n = n + 1
.head 7 -  Return TRUE
.head 3 -  !
.head 3 +  Child Table Class: MTblPropChildTable
.head 4 -  Display Settings
.head 5 -  Window Location and Size
.head 6 -  Left:
.head 6 -  Top:   
.head 6 -  Width:  7.517"
.head 6 -  Width Editable? Class Default
.head 6 -  Height: 3.976"
.head 6 -  Height Editable? Class Default
.head 5 -  Visible? Class Default
.head 5 -  Font Name: Class Default
.head 5 -  Font Size: Class Default
.head 5 -  Font Enhancement: Class Default
.head 5 -  Text Color: Class Default
.head 5 -  Background Color: Class Default
.head 5 -  View: Class Default
.head 5 -  Allow Row Sizing? Class Default
.head 5 -  Lines Per Row: Class Default
.head 4 -  Memory Settings
.head 5 -  Maximum Rows in Memory: 65536
.head 5 -  Discardable? No
.head 4 -  Next Class Child Key: 4
.head 4 -  List in Tool Palette? Yes
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Description:
.head 4 -  Derived From
.head 4 +  Contents
.head 5 +  Column: colName
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 4
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: N
.head 6 -  Visible? Yes
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  Default
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  List Values
.head 6 -  Message Actions
.head 5 +  Column: colVal
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 2
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Value
.head 6 -  Visible? Yes
.head 6 -  Editable? Yes
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: String
.head 6 -  Justify: Left
.head 6 -  Width:  Default
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  List Values
.head 6 +  Message Actions
.head 7 +  On SAM_AnyEdit
.head 8 -  Call OnValEdit( lParam )
.head 7 +  On SAM_DropDown
.head 8 -  Call OnValDropDown( SalTblQueryContext( hWndForm ) )
.head 5 +  Column: colPropIndex
.head 6 -  Class Child Ref Key: 0
.head 6 -  Class ChildKey: 3
.head 6 -  Class:
.head 6 -  Property Template:
.head 6 -  Class DLL Name:
.head 6 -  Title: Property-Index
.head 6 -  Visible? No
.head 6 -  Editable? No
.head 6 -  Maximum Data Length: Default
.head 6 -  Data Type: Number
.head 6 -  Justify: Left
.head 6 -  Width:  Default
.head 6 -  Width Editable? Yes
.head 6 -  Format: Unformatted
.head 6 -  Country: Default
.head 6 -  Input Mask: Unformatted
.head 6 -  Cell Options
.head 7 -  Cell Type? Standard
.head 7 -  Multiline Cell? No
.head 7 -  Cell DropDownList
.head 8 -  Sorted? Yes
.head 8 -  Vertical Scroll? Yes
.head 8 -  Auto Drop Down? No
.head 8 -  Allow Text Editing? Yes
.head 7 -  Cell CheckBox
.head 8 -  Check Value:
.head 8 -  Uncheck Value:
.head 8 -  Ignore Case? Yes
.head 6 -  List Values
.head 6 -  Message Actions
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  : m_PropList
.head 6 -  Class: MTblPropList
.head 5 -  Number: m_nColorGrayed
.head 4 +  Functions
.head 5 +  Function: OnCreate
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nCols
.head 7 -  Window Handle: hWndaCol[*]
.head 6 +  Actions
.head 7 -  ! Make it a good table ;o)
.head 7 -  Call MTblSubClass( hWndForm )
.head 7 -  ! Get "grayed" color
.head 7 -  Call MTblGetEffColumnHdrBackColor( colName, m_nColorGrayed )
.head 7 -  ! Buttons permanently visible
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_BUTTONS_PERMANENT, TRUE )
.head 7 -  ! Gradient headers
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_GRADIENT_HEADERS, TRUE )
.head 7 -  ! No column header
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_NO_COLUMN_HEADER, TRUE )
.head 7 -  ! No row header
.head 7 -  Call SalTblDefineRowHeader( hWndForm, STRING_Null, 0, 0, hWndNULL )
.head 7 -  ! Columns not movable
.head 7 -  Call SalTblSetTableFlags( hWndForm, TBL_Flag_MovableCols, FALSE )
.head 7 -  ! Enable extended messages
.head 7 -  Call MTblEnableExtMsgs( hWndForm, TRUE )
.head 7 -  ! Enable mousewheel scrolling
.head 7 -  Call MTblEnableMWheelScroll( hWndForm, TRUE )
.head 7 -  ! Enable vertical thumbtrack scrolling
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_THUMBTRACK_VSCROLL, TRUE )
.head 7 -  ! Lines...
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_NO_FREE_COL_AREA_LINES | MTBL_FLAG_NO_FREE_ROW_AREA_LINES, TRUE )
.head 7 -  Call MTblDefineColLines( hWndForm, MTLS_SOLID, m_nColorGrayed )
.head 7 -  Call MTblDefineRowLines( hWndForm, MTLS_SOLID, m_nColorGrayed )
.head 7 -  ! Cell mode
.head 7 -  Call MTblDefineCellMode( hWndForm, TRUE, MTBL_CM_FLAG_NO_SELECTION | MTBL_CM_FLAG_AUTO_EDIT_MODE )
.head 7 -  ! Focus frame
.head 7 -  Call MTblDefineFocusFrame( hWndForm, 2, 0, 0, SalColorFromRGB( 255, 128, 0 ) )
.head 7 -  ! Selection colors
.head 7 -  Call MTblSetSelectionColors( hWndForm, COLOR_White, SalColorFromRGB( 0, 128, 192 ) )
.head 7 -  ! No focus frame
.head 7 -  ! Call MTblSetFlags( hWndForm, MTBL_FLAG_SUPPRESS_FOCUS, TRUE )
.head 7 -  ! No row selection
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_SUPPRESS_ROW_SELECTION, TRUE )
.head 7 -  ! Expand CRLF
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_EXPAND_CRLF, TRUE )
.head 7 -  ! AdaptListWidth
.head 7 -  Call MTblSetFlags( hWndForm, MTBL_FLAG_ADAPT_LIST_WIDTH, TRUE )
.head 7 -  ! Tooltips...
.head 7 -  Call MTblEnableTipType( hWndForm, MTBL_TIP_CELL_COMPLETETEXT, TRUE )
.head 7 -  ! Tree...
.head 7 -  ! Call MTblDefineTree( hWndForm, colNode, 8, 8 )
.head 7 -  ! Call MTblSetTreeFlags( hWndForm, MTBL_TREE_FLAG_FLAT_STRUCT | MTBL_TREE_FLAG_NO_ROWLINES, TRUE )
.head 7 -  Call MTblDefineTree( hWndForm, colName, 8, 12 )
.head 7 -  Call MTblSetTreeNodeColors( hWndForm, COLOR_DarkGray, COLOR_Black, COLOR_White )
.head 7 -  ! Indent
.head 7 -  Call MTblSetIndentPerLevel( hWndForm, 12 )
.head 5 +  Function: OnBeforeEnterEditMode
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndCol
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bButton
.head 7 -  Boolean: bHasUndefVal
.head 7 -  Boolean: bList
.head 7 -  Number: n
.head 7 -  Number: nContext
.head 7 -  Number: nListLines
.head 7 -  Number: nPropIdxSrc
.head 7 -  Number: nResources
.head 7 -  Number: nRet
.head 7 -  Number: nType
.head 7 -  String: sName
.head 7 -  String: sVal
.head 7 -  String: saResources[*]
.head 6 +  Actions
.head 7 -  Set nContext = SalTblQueryContext( hWndForm )
.head 7 -  Call SalTblSetContext( hWndForm, p_nRow )
.head 7 -  !
.head 7 -  Set sName = m_PropList.m_Props[colPropIndex].GetName( )
.head 7 -  Set nType = m_PropList.m_Props[colPropIndex].GetType( )
.head 7 -  Set bHasUndefVal = m_PropList.m_Props[colPropIndex].HasUndefVal( )
.head 7 +  Select Case nType
.head 8 +  Case MTPROP_TYPE_IMAGE_NAME
.head 9 -  Set nPropIdxSrc = m_PropList.m_Props[colPropIndex].GetParentProp( )
.head 9 +  If nPropIdxSrc >= 0
.head 10 -  Call m_PropList.m_Props[colPropIndex].InitDispVals( )
.head 10 -  !
.head 10 -  Set sVal = m_PropList.m_Props[nPropIdxSrc].GetVal( )
.head 10 +  If sVal = "FILE"
.head 11 -  Set bList = FALSE
.head 11 -  Set bButton = TRUE
.head 10 +  Else If sVal = "TDRES"
.head 11 -  Set bList = TRUE
.head 11 -  Set bButton = FALSE
.head 11 -  !
.head 11 -  Set nResources = CDK_App.EnumResources( saResources )
.head 11 -  Set n = 0
.head 11 +  While n < nResources
.head 12 -  Call m_PropList.m_Props[colPropIndex].AddDispVal( saResources[n], saResources[n] )
.head 12 -  Set n = n + 1
.head 10 +  Else
.head 11 -  Set bList = FALSE
.head 11 -  Set bButton = FALSE
.head 10 -  !
.head 10 -  Call MTblDefineButtonCell( colVal, p_nRow, bButton, -1, "...", 0, 0 )
.head 10 +  If bList
.head 11 -  Set nListLines = m_PropList.m_Props[colPropIndex].GetDispValCount( )
.head 11 +  If bHasUndefVal
.head 12 -  Set nListLines = nListLines + 1
.head 11 -  Set nListLines = SalNumberMin( nListLines, 25 )
.head 11 -  Call MTblDefineCellType( colVal, p_nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll | COL_DropDownList_Editable, nListLines, "", "" )
.head 11 -  Call MTblSetDropDownListContext( hWndForm, p_nRow )
.head 11 -  !
.head 11 -  Call SalListClear( colVal )
.head 11 +  If bHasUndefVal
.head 12 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 11 -  Call m_PropList.m_Props[colPropIndex].AddDispValsToListCtrl( colVal )
.head 10 +  Else
.head 11 -  Call MTblDefineCellType( colVal, p_nRow, COL_CellType_Standard, 0, 0, "", "" )
.head 9 -  !
.head 9 -  Break
.head 7 -  !
.head 7 -  Call SalTblSetContext( hWndForm, nContext )
.head 7 -  !
.head 7 -  Return nRet
.head 5 +  Function: OnButtonClick
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndCol
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bNoUndef
.head 7 -  Number: nColorOld
.head 7 -  Number: nColorNew
.head 7 -  Number: nContext
.head 7 -  Number: nIndex
.head 7 -  Number: nFilters
.head 7 -  Number: nPropIdx
.head 7 -  Number: nPos
.head 7 -  Number: nTextColor
.head 7 -  Number: nType
.head 7 -  String: sFile
.head 7 -  String: sName
.head 7 -  String: sNameFind
.head 7 -  String: sPath
.head 7 -  String: sVal
.head 7 -  String: saFilters[*]
.head 6 +  Actions
.head 7 -  Set nContext = SalTblQueryContext( hWndForm )
.head 7 +  If nContext != p_nRow 
.head 8 -  Call SalTblSetContext( hWndForm, p_nRow )
.head 7 -  !
.head 7 -  Set sName = m_PropList.m_Props[colPropIndex].GetName( )
.head 7 -  Set nType = m_PropList.m_Props[colPropIndex].GetType( )
.head 7 +  Select Case nType
.head 8 +  Case MTPROP_TYPE_COLOR
.head 9 +  If colVal = MTPROP_TEXT_UNDEFINED
.head 10 -  Set nColorOld = MTBL_COLOR_UNDEF
.head 9 +  Else
.head 10 -  Set nColorOld = SalStrToNumber( colVal )
.head 9 -  Set nColorNew = nColorOld
.head 9 -  !
.head 9 +  If SalStrLeftX( sName, 9 ) = "MT_TT_DEF"
.head 10 -  Set bNoUndef = TRUE
.head 9 +  If SalModalDialog( dlgMTblPropColor, hWndForm, bNoUndef, nColorNew ) AND nColorNew != nColorOld
.head 10 +  If nColorNew = MTBL_COLOR_UNDEF
.head 11 -  Set sVal = m_PropList.m_Props[colPropIndex].GetUndefVal( )
.head 10 +  Else
.head 11 -  Set sVal = SalNumberToStrX( nColorNew, 0 )
.head 10 -  Call SetVal( sVal, p_nRow )
.head 9 -  !
.head 9 -  Break
.head 8 +  Case MTPROP_TYPE_IMAGE_NAME
.head 9 -  Set nPos = SalStrScan( sName, "IMG_NAME" )
.head 9 +  If nPos >= 0
.head 10 -  Set sNameFind = SalStrLeftX( sName, nPos ) || "IMG_SOURCE"
.head 10 -  Set nPropIdx = m_PropList.FindName( sNameFind )
.head 10 +  If nPropIdx >= 0
.head 11 -  Set sVal = m_PropList.m_Props[nPropIdx].GetVal( )
.head 11 +  If sVal = "FILE"
.head 12 -  Set nFilters = 0
.head 12 -  Set saFilters[nFilters] = "All files (*.*)"
.head 12 -  Set nFilters = nFilters + 1
.head 12 -  Set saFilters[nFilters] = "*.*"
.head 12 -  Set nFilters = nFilters + 1
.head 12 -  !
.head 12 -  Set nIndex = 1
.head 12 -  !
.head 12 -  Set sPath = colVal
.head 12 +  If SalDlgOpenFile( hWndForm, "Image file", saFilters, nFilters, nIndex, sFile, sPath )
.head 13 -  Call SetVal( sPath, p_nRow )
.head 11 +  Else
.head 12 -  Call SalMessageBeep( MB_IconStop )
.head 9 -  !
.head 9 -  Break
.head 8 +  Default
.head 9 +  If m_PropList.m_Props[colPropIndex].HasAutoVal( )
.head 10 -  Call SetVal( m_PropList.m_Props[colPropIndex].GetAutoVal( ), p_nRow )
.head 7 -  !
.head 7 +  If nContext != p_nRow 
.head 8 -  Call SalTblSetContext( hWndForm, nContext )
.head 5 +  Function: OnColHdrSepDoubleClick
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndCol
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Call AutoSizeCol( p_hWndCol )
.head 5 +  Function: OnQueryNoFocusOnCell
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndCol
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If p_hWndCol != GetHandle( colVal )
.head 8 -  Return 1
.head 5 +  Function: OnValDropDown
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 7 -  Number: nPropIdxName
.head 7 -  Number: nValCount
.head 7 -  Number: nType
.head 7 -  Number: naVal[*]
.head 7 -  String: sName
.head 7 -  String: sOldVal
.head 7 -  String: sVal
.head 6 +  Actions
.head 7 -  Set nType = m_PropList.m_Props[colPropIndex].GetType( )
.head 7 +  Select Case nType
.head 8 +  Case MTPROP_TYPE_FONT_SIZE
.head 9 -  Set sOldVal = colVal
.head 9 -  !
.head 9 -  Set sName = m_PropList.m_Props[colPropIndex].GetName( )
.head 9 -  !
.head 9 -  Call MTblSetDropDownListContext( hWndForm, p_nRow )
.head 9 -  Call SalListClear( colVal )
.head 9 -  !
.head 9 +  If m_PropList.m_Props[colPropIndex].HasUndefVal( )
.head 10 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 9 -  !
.head 9 -  Set nPropIdxName = m_PropList.m_Props[colPropIndex].GetParentProp( )
.head 9 +  If nPropIdxName >= 0
.head 10 -  Set sVal = m_PropList.m_Props[nPropIdxName].GetVal( )
.head 10 +  If sVal
.head 11 -  Set nValCount = MTblEnumFontSizes( sVal, naVal )
.head 11 +  If nValCount > 0
.head 12 -  Set n = 0
.head 12 +  While n < nValCount
.head 13 -  Call SalListAdd( colVal, SalNumberToStrX( naVal[n], 0 ) )
.head 13 -  Set n = n + 1
.head 9 -  !
.head 9 -  Set colVal = sOldVal
.head 9 -  !
.head 9 -  Break
.head 5 +  Function: OnValEdit
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 7 -  Boolean: bHasUndefVal
.head 7 -  Number: nIndex
.head 7 -  Number: nKeyState
.head 7 -  Number: nMutExclGrp
.head 7 -  Number: nRow
.head 7 -  Number: nType
.head 7 -  String: sVal
.head 7 -  String: sName
.head 7 -  String: sUndefVal
.head 6 +  Actions
.head 7 -  Set bHasUndefVal = m_PropList.m_Props[colPropIndex].HasUndefVal( )
.head 7 -  Set sUndefVal = m_PropList.m_Props[colPropIndex].GetUndefVal( )
.head 7 -  !
.head 7 +  If NOT colVal AND bHasUndefVal AND sUndefVal = STRING_Null
.head 8 -  Set nKeyState = 0
.head 8 -  Set nKeyState = VisGetKeyState( VK_Delete )
.head 8 -  Set nKeyState = nKeyState | VisGetKeyState( VK_Backspace )
.head 8 +  If NOT nKeyState & KS_Down
.head 9 -  Return TRUE
.head 7 -  !
.head 7 -  Set sVal = m_PropList.m_Props[colPropIndex].GetValFromDispVal( colVal )
.head 7 -  Call SetVal( sVal, p_nRow )
.head 7 -  !
.head 7 -  Set nMutExclGrp = m_PropList.m_Props[colPropIndex].GetMutExclGrp( )
.head 7 -  Set nType = m_PropList.m_Props[colPropIndex].GetType( )
.head 7 +  If nType = MTPROP_TYPE_BOOL AND nMutExclGrp > 0 AND sVal = MTPROP_VAL_BOOL_YES
.head 8 -  Set nIndex = colPropIndex
.head 8 -  Set n = 0
.head 8 +  While n < m_PropList.m_nCount
.head 9 +  If n != nIndex AND m_PropList.m_Props[n].GetMutExclGrp( ) = nMutExclGrp
.head 10 -  Set nRow = MTblGetRowFromID( hWndForm, m_PropList.m_Props[n].GetRowId( ) )
.head 10 +  If nRow != TBL_Error
.head 11 -  Call SetVal( MTPROP_VAL_BOOL_NO, nRow )
.head 9 -  Set n = n + 1
.head 7 -  !
.head 7 -  Return TRUE
.head 5 -  !
.head 5 +  Function: AutoSizeCol
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndCol
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nFlags
.head 6 +  Actions
.head 7 -  Set nFlags = MTASC_BTNSPACE | MTASC_HIDDENROWS | MTASC_ALLROWS
.head 7 -  Return MTblAutoSizeColumn( hWndForm, p_hWndCol, nFlags )
.head 5 +  Function: EnsureRowVisible
.head 6 -  Description:
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 7 -  Number: nParentRows
.head 7 -  Number: nRow
.head 7 -  Number: naParentRows[*]
.head 6 +  Actions
.head 7 +  If SalTblQueryRowFlags( hWndForm, p_nRow, ROW_Hidden )
.head 8 -  Set nRow = p_nRow
.head 8 +  Loop
.head 9 -  Set nRow = MTblGetParentRow( hWndForm, nRow )
.head 9 +  If nRow = TBL_Error
.head 10 -  Break
.head 9 -  Set naParentRows[nParentRows] = nRow
.head 9 -  Set nParentRows = nParentRows + 1
.head 8 +  If nParentRows > 0
.head 9 -  Set n = nParentRows - 1
.head 9 +  While n >= 0
.head 10 +  If NOT MTblQueryRowFlags( hWndForm, naParentRows[n], MTBL_ROW_ISEXPANDED )
.head 11 -  Call MTblExpandRow( hWndForm, naParentRows[n], MTER_REDRAW )
.head 10 -  Set n = n - 1
.head 5 +  Function: FindPropName
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Number:
.head 6 +  Parameters
.head 7 -  String: p_sPropName
.head 7 -  Number: p_nStartRow
.head 7 -  Boolean: p_bNext
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: nContext
.head 7 -  Number: nRow
.head 7 -  Number: nRowFound
.head 6 +  Actions
.head 7 -  Set nContext = SalTblQueryContext( hWndForm )
.head 7 -  !
.head 7 -  Set nRowFound = TBL_Error
.head 7 +  If p_bNext
.head 8 -  Set nRow = p_nStartRow - 1
.head 7 +  Else
.head 8 -  Set nRow = p_nStartRow + 1
.head 7 +  Loop FIND
.head 8 +  If p_bNext
.head 9 +  If NOT SalTblFindNextRow( hWndForm, nRow, 0, 0 )
.head 10 -  Break FIND
.head 8 +  Else
.head 9 +  If NOT SalTblFindPrevRow( hWndForm, nRow, 0, 0 )
.head 10 -  Break FIND
.head 8 -  Call SalTblSetContext( hWndForm, nRow )
.head 8 +  If colPropIndex != NUMBER_Null AND colPropIndex >= 0
.head 9 +  If m_PropList.m_Props[colPropIndex].GetName( ) = p_sPropName
.head 10 -  Set nRowFound = nRow
.head 10 -  Break FIND
.head 7 -  !
.head 7 -  Call SalTblSetContext( hWndForm, nContext )
.head 7 -  !
.head 7 -  Return nRowFound
.head 5 +  Function: GetHandle
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Window Handle:
.head 6 +  Parameters
.head 7 -  Window Handle: p_hWndCol
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Return p_hWndCol
.head 5 +  Function: Populate
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  : oColumns[*]
.head 8 -  Class: cdkColumn
.head 7 -  Boolean: bHasAutoVal
.head 7 -  Boolean: bHasUndefVal
.head 7 -  Number: n
.head 7 -  Number: n2
.head 7 -  Number: nCatCount
.head 7 -  Number: nColCount
.head 7 -  Number: nDispVals
.head 7 -  Number: nFocusRow
.head 7 -  Number: nFontCount
.head 7 -  Number: nGrpCount
.head 7 -  Number: nGrp2Count
.head 7 -  Number: nListLines
.head 7 -  Number: nRow
.head 7 -  Number: nRowCat
.head 7 -  Number: nRowGrp
.head 7 -  Number: nRowGrp2
.head 7 -  Number: nType
.head 7 -  Number: nVal
.head 7 -  Number: naCatRowId[*]
.head 7 -  Number: naGrpRowId[*]
.head 7 -  Number: naGrp2RowId[*]
.head 7 -  String: sCat
.head 7 -  String: sGrp
.head 7 -  String: sGrp2
.head 7 -  String: sName
.head 7 -  String: sTitle
.head 7 -  String: sVal
.head 7 -  String: saCat[*]
.head 7 -  String: saFont[*]
.head 7 -  String: saGrp[*]
.head 7 -  String: saGrp2[*]
.head 6 +  Actions
.head 7 -  Call MTblLockPaint( hWndForm )
.head 7 -  !
.head 7 -  Call SalTblReset( hWndForm )
.head 7 -  !
.head 7 -  Set nFocusRow = TBL_Error
.head 7 -  Set nCatCount = 0
.head 7 -  Set nGrpCount = 0
.head 7 -  !
.head 7 -  Set n = 0
.head 7 +  While n < m_PropList.m_nCount
.head 8 -  ! Category-Row
.head 8 -  Set nRowCat = TBL_Error
.head 8 -  Set sCat = m_PropList.m_Props[n].GetCategory( )
.head 8 +  If sCat
.head 9 -  Set n2 = 0
.head 9 +  While n2 < nCatCount
.head 10 +  If saCat[n2] = sCat
.head 11 -  Set nRowCat = MTblGetRowFromID( hWndForm, naCatRowId[n2] )
.head 11 -  Break
.head 10 -  Set n2 = n2 + 1
.head 9 -  !
.head 9 +  If nRowCat = TBL_Error
.head 10 -  Set nRow = SalTblInsertRow( hWndForm, TBL_MaxRow )
.head 10 +  If nRow = TBL_Error
.head 11 -  Return FALSE
.head 10 -  !
.head 10 -  Call SalTblSetRowFlags( hWndForm, nRow, ROW_New, FALSE )
.head 10 -  ! Call MTblSetRowFlags( hWndForm, nRow, MTBL_ROW_CANEXPAND | MTBL_ROW_ISEXPANDED, TRUE, MTSF_REDRAW )
.head 10 -  Call MTblSetRowFlags( hWndForm, nRow, MTBL_ROW_CANEXPAND, TRUE, MTSF_REDRAW )
.head 10 -  !
.head 10 -  Call MTblSetCellMerge( colName, nRow, 1, MTSM_REDRAW )
.head 10 -  Call MTblSetCellFont( colName, nRow, MTBL_FONT_UNDEF_NAME, MTBL_FONT_UNDEF_SIZE, FONT_EnhBold, MTSF_REDRAW )
.head 10 -  Call MTblSetCellBackColor( colName, nRow, m_nColorGrayed, MTSC_REDRAW )
.head 10 -  !
.head 10 -  Set colName = sCat
.head 10 -  !
.head 10 -  Set nRowCat = nRow
.head 10 -  Set saCat[nCatCount] = sCat
.head 10 -  Set naCatRowId[nCatCount] = MTblGetRowID( hWndForm, nRow )
.head 10 -  Set nCatCount = nCatCount + 1
.head 8 -  ! Group-Row
.head 8 -  Set sGrp = m_PropList.m_Props[n].GetGroup( )
.head 8 -  Set nRowGrp = TBL_Error
.head 8 +  If sGrp
.head 9 -  Set n2 = 0
.head 9 +  While n2 < nGrpCount
.head 10 +  If saGrp[n2] = sGrp
.head 11 -  Set nRowGrp = MTblGetRowFromID( hWndForm, naGrpRowId[n2] )
.head 11 +  If NOT sCat OR ( sCat AND MTblGetParentRow( hWndForm, nRowGrp ) = nRowCat )
.head 12 -  Break
.head 11 +  Else
.head 12 -  Set nRowGrp = TBL_Error
.head 10 -  Set n2 = n2 + 1
.head 9 -  !
.head 9 +  If nRowGrp = TBL_Error
.head 10 +  If nRowCat != TBL_Error
.head 11 -  Set nRow = MTblInsertChildRow( hWndForm, nRowCat, 0 )
.head 10 +  Else
.head 11 -  Set nRow = SalTblInsertRow( hWndForm, TBL_MaxRow )
.head 10 +  If nRow = TBL_Error
.head 11 -  Return FALSE
.head 10 -  !
.head 10 -  Call SalTblSetRowFlags( hWndForm, nRow, ROW_New, FALSE )
.head 10 -  Call MTblSetRowFlags( hWndForm, nRow, MTBL_ROW_CANEXPAND, TRUE, MTSF_REDRAW )
.head 10 -  !
.head 10 -  Call MTblSetCellMerge( colName, nRow, 1, MTSM_REDRAW )
.head 10 -  !
.head 10 -  Set colName = sGrp
.head 10 -  !
.head 10 -  Set nRowGrp = nRow
.head 10 -  Set saGrp[nGrpCount] = sGrp
.head 10 -  Set naGrpRowId[nGrpCount] = MTblGetRowID( hWndForm, nRow )
.head 10 -  Set nGrpCount = nGrpCount + 1
.head 8 -  ! Group2-Row
.head 8 -  Set sGrp2 = m_PropList.m_Props[n].GetGroup2( )
.head 8 -  Set nRowGrp2 = TBL_Error
.head 8 +  If sGrp2
.head 9 -  Set n2 = 0
.head 9 +  While n2 < nGrp2Count
.head 10 +  If saGrp2[n2] = sGrp2
.head 11 -  Set nRowGrp2 = MTblGetRowFromID( hWndForm, naGrp2RowId[n2] )
.head 11 +  If NOT sGrp OR ( sGrp AND MTblGetParentRow( hWndForm, nRowGrp2 ) = nRowGrp )
.head 12 -  Break
.head 11 +  Else
.head 12 -  Set nRowGrp2 = TBL_Error
.head 10 -  Set n2 = n2 + 1
.head 9 -  !
.head 9 +  If nRowGrp2 = TBL_Error
.head 10 +  If nRowGrp != TBL_Error
.head 11 -  Set nRow = MTblInsertChildRow( hWndForm, nRowGrp, 0 )
.head 10 +  Else If nRowCat != TBL_Error
.head 11 -  Set nRow = MTblInsertChildRow( hWndForm, nRowCat, 0 )
.head 10 +  Else
.head 11 -  Set nRow = SalTblInsertRow( hWndForm, TBL_MaxRow )
.head 10 +  If nRow = TBL_Error
.head 11 -  Return FALSE
.head 10 -  !
.head 10 -  Call SalTblSetRowFlags( hWndForm, nRow, ROW_New, FALSE )
.head 10 -  Call MTblSetRowFlags( hWndForm, nRow, MTBL_ROW_CANEXPAND, TRUE, MTSF_REDRAW )
.head 10 -  !
.head 10 +  If nRowGrp != TBL_Error
.head 11 -  Call MTblSetCellIndentLevel( colName, nRow, 1 )
.head 10 -  !
.head 10 -  Call MTblSetCellMerge( colName, nRow, 1, MTSM_REDRAW )
.head 10 -  !
.head 10 -  Set colName = sGrp2
.head 10 -  !
.head 10 -  Set nRowGrp2 = nRow
.head 10 -  Set saGrp2[nGrp2Count] = sGrp2
.head 10 -  Set naGrp2RowId[nGrp2Count] = MTblGetRowID( hWndForm, nRow )
.head 10 -  Set nGrp2Count = nGrp2Count + 1
.head 8 -  ! Property-Row
.head 8 +  If nRowGrp2 != TBL_Error
.head 9 -  Set nRow = MTblInsertChildRow( hWndForm, nRowGrp2, 0 )
.head 8 +  Else If nRowGrp != TBL_Error
.head 9 -  Set nRow = MTblInsertChildRow( hWndForm, nRowGrp, 0 )
.head 8 +  Else If nRowCat != TBL_Error
.head 9 -  Set nRow = MTblInsertChildRow( hWndForm, nRowCat, 0 )
.head 8 +  Else
.head 9 -  Set nRow = SalTblInsertRow( hWndForm, TBL_MaxRow )
.head 8 +  If nRow = TBL_Error
.head 9 -  Return FALSE
.head 8 -  Call SalTblSetRowFlags( hWndForm, nRow, ROW_New, FALSE )
.head 8 -  !
.head 8 +  If sGrp2
.head 9 -  Call MTblSetCellIndentLevel( colName, nRow, 2 )
.head 8 +  Else If sGrp
.head 9 -  Call MTblSetCellIndentLevel( colName, nRow, 1 )
.head 8 -  !
.head 8 -  Set colPropIndex = n
.head 8 -  Call m_PropList.m_Props[n].SetRowId( MTblGetRowID( hWndForm, nRow ) )
.head 8 -  !
.head 8 -  Set sTitle = m_PropList.m_Props[n].GetTitle( )
.head 8 -  Set colName = sTitle
.head 8 -  !
.head 8 -  Set sName = m_PropList.m_Props[n].GetName( )
.head 8 -  Set nType = m_PropList.m_Props[n].GetType( )
.head 8 -  Set sVal = m_PropList.m_Props[n].GetEffVal( )
.head 8 -  Set nDispVals = m_PropList.m_Props[n].GetDispValCount( )
.head 8 -  Set bHasUndefVal = m_PropList.m_Props[n].HasUndefVal( )
.head 8 -  Set bHasAutoVal = m_PropList.m_Props[n].HasAutoVal( )
.head 8 +  Select Case nType
.head 9 +  Case MTPROP_TYPE_BOOL
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_CheckBox, COL_CheckBox_IgnoreCase, 0, MTPROP_VAL_BOOL_YES, MTPROP_VAL_BOOL_NO )
.head 10 -  !
.head 10 -  Break
.head 9 +  Case MTPROP_TYPE_STRING_ML
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_PopupEdit, 0, 5, "", "" )
.head 10 -  !
.head 10 -  Break
.head 9 +  Case MTPROP_TYPE_COLOR
.head 10 -  Call MTblDefineButtonCell( colVal, nRow, TRUE, -1, "...", 0, 0 )
.head 10 -  ! Call MTblSetCellFlags( colVal, nRow, MTBL_CELL_FLAG_READ_ONLY | MTBL_CELL_FLAG_CLIP_NONEDIT, TRUE )
.head 10 -  Call MTblDisableCell( colVal, nRow, TRUE )
.head 10 -  !
.head 10 -  Break
.head 9 +  Case MTPROP_TYPE_FONT_SIZE
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll | COL_DropDownList_Editable, 25, "", "" )
.head 10 -  !
.head 10 -  Break
.head 9 +  ! Case MTPROP_TYPE_LINE_STYLE
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll, 3, "", "" )
.head 10 -  Call MTblSetDropDownListContext( hWndForm, nRow )
.head 10 -  !
.head 10 +  If bHasUndefVal
.head 11 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 10 -  Call m_PropList.m_Props[n].AddDispValsToListCtrl( colVal )
.head 10 -  !
.head 10 -  Break 
.head 9 +  ! Case MTPROP_TYPE_COLUMN
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll, 25, "", "" )
.head 10 -  Call MTblSetDropDownListContext( hWndForm, nRow )
.head 10 -  !
.head 10 +  If bHasUndefVal
.head 11 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 10 -  Call m_PropList.m_Props[n].AddDispValsToListCtrl( colVal )
.head 10 -  !
.head 10 -  Break 
.head 9 +  ! Case MTPROP_TYPE_FONT_NAME
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll, 25, "", "" )
.head 10 -  Call MTblSetDropDownListContext( hWndForm, nRow )
.head 10 -  !
.head 10 +  If bHasUndefVal
.head 11 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 10 -  Call m_PropList.m_Props[n].AddDispValsToListCtrl( colVal )
.head 10 -  !
.head 10 -  Break 
.head 9 +  ! Case MTPROP_TYPE_ECPD_ORDER
.head 10 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll, 6, "", "" )
.head 10 -  Call MTblSetDropDownListContext( hWndForm, nRow )
.head 10 -  !
.head 10 +  If bHasUndefVal
.head 11 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 10 -  Call m_PropList.m_Props[n].AddDispValsToListCtrl( colVal )
.head 10 -  !
.head 10 -  Break 
.head 9 +  Default
.head 10 +  If nDispVals > 0
.head 11 -  Set nListLines = nDispVals
.head 11 +  If bHasUndefVal
.head 12 -  Set nListLines = nListLines + 1
.head 11 -  Set nListLines = SalNumberMin( nListLines, 25 )
.head 11 -  Call MTblDefineCellType( colVal, nRow, COL_CellType_DropDownList, COL_DropDownList_Auto | COL_DropDownList_VScroll, nListLines, "", "" )
.head 11 -  Call MTblSetDropDownListContext( hWndForm, nRow )
.head 11 -  !
.head 11 +  If bHasUndefVal
.head 12 -  Call SalListAdd( colVal, MTPROP_TEXT_UNDEFINED )
.head 11 -  Call m_PropList.m_Props[n].AddDispValsToListCtrl( colVal )
.head 10 +  If bHasAutoVal
.head 11 -  Call MTblDefineButtonCell( colVal, nRow, TRUE, -1, "Automatic", 0, 0 )
.head 8 -  Call SetVal( sVal, nRow )
.head 8 -  !
.head 8 +  If NOT m_PropList.m_Props[n].IsValDef( ) AND NOT m_PropList.m_Props[n].GetEnsureInvisible( )
.head 9 -  Call m_PropList.m_Props[n].SetEnsureVisible( TRUE )
.head 8 +  If m_PropList.m_Props[n].GetEnsureVisible( )
.head 9 -  Call EnsureRowVisible( nRow )
.head 8 -  !
.head 8 -  Set n = n + 1
.head 7 -  !
.head 7 -  Call AutoSizeCol( hWndNULL )
.head 7 -  !
.head 7 +  If MTblUnlockPaint( hWndForm ) < 1
.head 8 -  Call SalInvalidateWindow( hWndForm )
.head 8 -  Call SalUpdateWindow( hWndForm )
.head 7 -  !
.head 7 -  Set nRow = -1
.head 7 +  While SalTblFindNextRow( hWndForm, nRow, 0, ROW_Hidden )
.head 8 +  If MTblGetRowLevel( hWndForm, nRow ) > 0
.head 9 -  Set nFocusRow = nRow
.head 9 -  Break
.head 7 +  If nFocusRow = TBL_Error
.head 8 -  Set nFocusRow = 0
.head 7 -  Call MTblSetFocusCell( hWndForm, nFocusRow, colVal, 0 )
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: SetPropList
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  : p
.head 8 -  Class: MTblPropList
.head 7 -  Boolean: p_bPopulate
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 +  If NOT p.AssignTo( m_PropList )
.head 8 -  Return FALSE
.head 7 -  !
.head 7 +  If p_bPopulate
.head 8 -  Return Populate( )
.head 7 +  Else
.head 8 -  Return TRUE
.head 5 +  Function: SetPropsVisibility
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bVisible
.head 7 -  Number: n
.head 7 -  Number: nRow
.head 7 -  Number: nRowId
.head 6 +  Actions
.head 7 +  While n < m_PropList.m_nCount
.head 8 -  Set nRowId = m_PropList.m_Props[n].GetRowId( )
.head 8 -  Set nRow = MTblGetRowFromID( hWndForm, nRowId )
.head 8 +  If nRow != TBL_Error
.head 9 -  Set bVisible = NOT SalTblQueryRowFlags( hWndForm, nRow, ROW_Hidden )
.head 8 +  Else
.head 9 -  Set bVisible = FALSE
.head 8 -  Call m_PropList.m_Props[n].SetEnsureVisible( bVisible )
.head 8 -  Call m_PropList.m_Props[n].SetEnsureInvisible( NOT bVisible )
.head 8 -  Set n = n + 1
.head 5 +  Function: SetVal
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 +  Parameters
.head 7 -  String: p_sVal
.head 7 -  Number: p_nRow
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Boolean: bIsUndefVal
.head 7 -  Number: nBackColor
.head 7 -  Number: nBackColorCurr
.head 7 -  Number: nContext
.head 7 -  Number: nImg
.head 7 -  Number: nImgColor
.head 7 -  Number: nImgHt
.head 7 -  Number: nImgWid
.head 7 -  Number: nTextColor
.head 7 -  Number: nTextColorCurr
.head 7 -  Number: nType
.head 7 -  Number: nVal
.head 7 -  Number: nX
.head 7 -  Number: nY
.head 7 -  String: sCellText
.head 7 -  String: sValFontNameOld
.head 7 -  String: sVal
.head 6 +  Actions
.head 7 -  Set nContext = SalTblQueryContext( hWndForm )
.head 7 +  If nContext != p_nRow 
.head 8 -  Call SalTblSetContext( hWndForm, p_nRow )
.head 7 -  !
.head 7 -  Set sVal = p_sVal
.head 7 -  Set sCellText = m_PropList.m_Props[colPropIndex].GetDispValFromVal( sVal )
.head 7 -  !
.head 7 -  !
.head 7 -  Set nType = m_PropList.m_Props[colPropIndex].GetType( )
.head 7 -  Set bIsUndefVal = m_PropList.m_Props[colPropIndex].IsUndefVal( sVal )
.head 7 -  !
.head 7 +  Select Case nType
.head 8 +  Case MTPROP_TYPE_COLOR
.head 9 +  If MTblGetCellImage( colVal, p_nRow, nImg )
.head 10 -  Call MTblSetCellImage( colVal, p_nRow, 0, MTSI_REDRAW )
.head 10 -  Call MImgDelete( nImg )
.head 9 -  !
.head 9 +  If bIsUndefVal
.head 10 -  Set nImgColor = MTBL_COLOR_UNDEF
.head 9 +  Else
.head 10 -  Set nVal = SalStrToNumber( p_sVal )
.head 10 -  Set nImgColor = nVal
.head 9 -  !
.head 9 -  Set nImgWid = 24
.head 9 -  Set nImgHt = 13
.head 9 -  Set nImg = MTblPropMakeColorImage( nImgWid, nImgHt, nImgColor )
.head 9 +  If nImg
.head 10 -  Call MTblSetCellImage( colVal, p_nRow, nImg, MTSI_REDRAW )
.head 9 -  !
.head 9 -  Break
.head 7 -  !
.head 7 +  If colVal != sCellText
.head 8 -  Set colVal = sCellText
.head 7 -  !
.head 7 -  Call m_PropList.m_Props[colPropIndex].SetVal( sVal )
.head 7 -  !
.head 7 +  If m_PropList.m_Props[colPropIndex].IsValDef( )
.head 8 -  Set nBackColor = MTBL_COLOR_UNDEF
.head 7 +  Else
.head 8 -  Set nBackColor = SalColorFromRGB( 240, 255, 240 )
.head 8 -  ! Set nBackColor = SalColorFromRGB( 255, 255, 240 )
.head 7 -  Call MTblGetCellBackColor( colVal, p_nRow, nBackColorCurr )
.head 7 +  If nBackColorCurr != nBackColor
.head 8 -  Call MTblSetCellBackColor( colVal, p_nRow, nBackColor, MTSC_REDRAW )
.head 7 -  !
.head 7 -  Else
.head 7 +  If colVal = MTPROP_TEXT_UNDEFINED
.head 8 -  Set nTextColor = COLOR_DarkGray
.head 7 +  Else
.head 8 -  Set nTextColor = MTBL_COLOR_UNDEF
.head 7 -  Call MTblGetCellTextColor( colVal, p_nRow, nTextColorCurr )
.head 7 +  If nTextColorCurr != nTextColor
.head 8 -  Call MTblSetCellTextColor( colVal, p_nRow, nTextColor, MTSC_REDRAW )
.head 7 -  !
.head 7 +  If nContext != p_nRow 
.head 8 -  Call SalTblSetContext( hWndForm, nContext )
.head 7 -  !
.head 7 -  Return TRUE
.head 5 +  Function: Validate
.head 6 -  Description:
.head 6 +  Returns
.head 7 -  Boolean:
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Number: n
.head 7 -  Number: nRowId
.head 7 -  Number: nRowInvalid
.head 7 -  String: sPropTitle
.head 7 -  String: sMsg
.head 7 -  String: sTitle
.head 6 +  Actions
.head 7 -  Set nRowInvalid = TBL_Error
.head 7 +  While n < m_PropList.m_nCount
.head 8 +  If NOT m_PropList.m_Props[n].ValidateVal( sMsg )
.head 9 -  Set nRowId = m_PropList.m_Props[n].GetRowId( )
.head 9 -  Set nRowInvalid = MTblGetRowFromID( hWndForm, nRowId )
.head 9 -  Break
.head 8 -  Set n = n + 1
.head 7 +  If nRowInvalid != TBL_Error
.head 8 -  Call EnsureRowVisible( nRowInvalid )
.head 8 -  Call SalTblSetFocusCell( hWndForm, nRowInvalid, colVal, 0, -1 )
.head 8 -  !
.head 8 -  Set sTitle = "Validation error"
.head 8 +  If NOT sMsg
.head 9 -  Set sMsg = "Invalid value"
.head 8 -  Set sPropTitle = m_PropList.m_Props[n].GetFullTitle( )
.head 8 +  If sPropTitle
.head 9 -  Set sMsg = sPropTitle || ":" || "
" || sMsg
.head 8 -  Call SalMessageBox( sMsg, sTitle, MB_IconStop | MB_Ok )
.head 7 -  Return nRowInvalid = TBL_Error
.head 4 +  Message Actions
.head 5 +  On SAM_Create
.head 6 -  Call OnCreate( )
.head 5 -  !
.head 5 +  On MTM_BeforeEnterEditMode
.head 6 -  Return OnBeforeEnterEditMode( SalNumberToWindowHandle( wParam ), lParam )
.head 5 +  On MTM_BtnClick
.head 6 -  Call OnButtonClick( SalNumberToWindowHandle( wParam ), lParam )
.head 5 +  On MTM_ColHdrSepLBtnDblClk
.head 6 -  Call OnColHdrSepDoubleClick( SalNumberToWindowHandle( wParam ) )
.head 5 +  On MTM_QueryNoFocusOnCell
.head 6 -  Return OnQueryNoFocusOnCell( SalNumberToWindowHandle( wParam ), lParam )
.head 3 -  !
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 3 -  ActiveX:
.head 2 -  Application Actions
.head 1 +  Dialog Box: dlgMTblPropColor
.head 2 -  Class:
.head 2 -  Property Template:
.head 2 -  Class DLL Name:
.head 2 -  Title: Color
.head 2 -  Accesories Enabled? No
.head 2 -  Visible? Yes
.head 2 -  Display Settings
.head 3 -  Display Style? Default
.head 3 -  Visible at Design time? Yes
.head 3 -  Type of Dialog: Modal
.head 3 -  Window Location and Size
.head 4 -  Left: Default
.head 4 -  Top:    Default
.head 4 -  Width:  2.95"
.head 4 -  Width Editable? Yes
.head 4 -  Height: 1.571"
.head 4 -  Height Editable? Yes
.head 3 -  Absolute Screen Location? Yes
.head 3 -  Font Name: Default
.head 3 -  Font Size: Default
.head 3 -  Font Enhancement: Default
.head 3 -  Text Color: Default
.head 3 -  Background Color: Default
.head 2 -  Description:
.head 2 +  Tool Bar
.head 3 -  Display Settings
.head 4 -  Display Style? Default
.head 4 -  Location? Top
.head 4 -  Visible? Yes
.head 4 -  Size: Default
.head 4 -  Size Editable? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 3 -  Contents
.head 2 +  Contents
.head 3 +  Pushbutton: pbChooseColor
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Choose...
.head 4 -  Window Location and Size
.head 5 -  Left: 1.483"
.head 5 -  Top:    0.095"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.286"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: (none)
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call ChooseColor( )
.head 3 +  Check Box: cbUndefColor
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: Undefined
.head 4 -  Window Location and Size
.head 5 -  Left: 1.483"
.head 5 -  Top:    0.452"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.25"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 +  If MyValue
.head 7 -  Set wnColor = MTBL_COLOR_UNDEF
.head 6 +  Else
.head 7 +  If wnLastColor != MTBL_COLOR_UNDEF
.head 8 -  Set wnColor = wnLastColor
.head 7 +  Else
.head 8 -  Set wnColor = COLOR_White
.head 6 -  Call ShowColor( wnColor )
.head 3 +  Picture: picColor
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Window Location and Size
.head 5 -  Left: 0.133"
.head 5 -  Top:    0.095"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.571"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Editable? No
.head 4 -  File Name:
.head 4 -  Storage: External
.head 4 -  Picture Transparent Color: None
.head 4 -  Fit: Scale
.head 4 -  Scaling
.head 5 -  Width:  100
.head 5 -  Height:  100
.head 4 -  Corners: Square
.head 4 -  Border Style: No Border
.head 4 -  Border Thickness: 1
.head 4 -  Tile To Parent? No
.head 4 -  Border Color: Default
.head 4 -  Background Color: Default
.head 4 -  Message Actions
.head 3 +  Pushbutton: pbOk
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Ok
.head 4 -  Window Location and Size
.head 5 -  Left: 0.183"
.head 5 -  Top:    0.881"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.286"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Enter
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call Ok( )
.head 3 +  Pushbutton: pbCancel
.head 4 -  Class Child Ref Key: 0
.head 4 -  Class ChildKey: 0
.head 4 -  Class:
.head 4 -  Property Template:
.head 4 -  Class DLL Name:
.head 4 -  Title: &Cancel
.head 4 -  Window Location and Size
.head 5 -  Left: 1.483"
.head 5 -  Top:    0.881"
.head 5 -  Width:  1.2"
.head 5 -  Width Editable? Yes
.head 5 -  Height: 0.286"
.head 5 -  Height Editable? Yes
.head 4 -  Visible? Yes
.head 4 -  Keyboard Accelerator: Esc
.head 4 -  Font Name: Default
.head 4 -  Font Size: Default
.head 4 -  Font Enhancement: Default
.head 4 -  Picture File Name:
.head 4 -  Picture Transparent Color: None
.head 4 -  Image Style: Single
.head 4 -  Text Color: Default
.head 4 -  Background Color: Default
.head 4 +  Message Actions
.head 5 +  On SAM_Click
.head 6 -  Call Cancel( )
.head 2 +  Functions
.head 3 +  Function: OnCreate
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalCenterWindow( hWndForm )
.head 5 -  !
.head 5 -  Set wnColor = r_nColor
.head 5 -  Set wnLastColor = wnColor
.head 5 -  !
.head 5 +  If p_bNoUndef
.head 6 -  Call SalDisableWindow( cbUndefColor )
.head 5 -  !
.head 5 -  Call ShowColor( wnColor )
.head 3 -  !
.head 3 +  Function: Cancel
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Call SalEndDialog( hWndForm, 0 )
.head 3 +  Function: ChooseColor
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If SalDlgChooseColor( hWndForm, wnColor )
.head 6 -  Set wnLastColor = wnColor
.head 6 -  Call ShowColor( wnColor )
.head 3 +  Function: Ok
.head 4 -  Description:
.head 4 -  Returns
.head 4 -  Parameters
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 -  Set r_nColor = wnColor
.head 5 -  Call SalEndDialog( hWndForm, 1 )
.head 3 +  Function: ShowColor
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  Number: p_nColor
.head 4 -  Static Variables
.head 4 +  Local variables
.head 5 -  Boolean: bUndef
.head 5 -  Number: nBottom
.head 5 -  Number: nImg
.head 5 -  Number: nLeft
.head 5 -  Number: nRight
.head 5 -  Number: nTop
.head 5 -  String: sImg
.head 4 +  Actions
.head 5 +  If GetClientRect( picColor, nLeft, nTop, nRight, nBottom )
.head 6 -  Set nImg = MTblPropMakeColorImage( nRight - nLeft, nBottom - nTop, p_nColor )
.head 6 +  If nImg
.head 7 +  If MImgGetString( nImg, sImg, MIMG_TYPE_BMP )
.head 8 -  Call SalPicSetString( picColor, PIC_FormatBitmap, sImg )
.head 7 -  Call MImgDelete( nImg )
.head 5 -  !
.head 5 -  Set bUndef = ( p_nColor = MTBL_COLOR_UNDEF )
.head 5 -  Set cbUndefColor = bUndef
.head 2 +  Window Parameters
.head 3 -  Boolean: p_bNoUndef
.head 3 -  Receive Number: r_nColor
.head 2 +  Window Variables
.head 3 -  Number: wnColor
.head 3 -  Number: wnLastColor
.head 2 +  Message Actions
.head 3 +  On SAM_Create
.head 4 -  Call OnCreate( )
